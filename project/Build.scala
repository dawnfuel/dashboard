import sbt._
import sbt.Keys._

import com.typesafe.sbt.web.SbtWeb

object LiftProjectBuild extends Build {

  import BuildSettings._

  object Ver {
    val lift = "2.6"
    val lift_edition = "2-6"
    val jetty = "9.2.9.v20150224"
  }

  def compile(deps: ModuleID*): Seq[ModuleID] = deps map (_ % "compile")
  def provided(deps: ModuleID*): Seq[ModuleID] = deps map (_ % "provided")
  def test(deps: ModuleID*): Seq[ModuleID] = deps map (_ % "test")
  def runtime(deps: ModuleID*): Seq[ModuleID] = deps map (_ % "runtime")
  def container(deps: ModuleID*): Seq[ModuleID] = deps map (_ % "container")

  lazy val root = Project("lift-mongo-app", file("."))
    .settings(liftAppSettings: _*)
    .settings(libraryDependencies ++=
    compile(
      "net.liftweb"               %% "lift-webkit"                   % Ver.lift,
      "net.liftweb"               %% "lift-mongodb-record"           % Ver.lift,
      "net.liftmodules"           %% ("extras_"+Ver.lift_edition)    % "0.4",
      "net.liftmodules"           %% ("mongoauth_"+Ver.lift_edition) % "0.6",
      "ch.qos.logback"            % "logback-classic"                % "1.1.2",
      "com.foursquare"            %% "rogue-field"                   % "2.5.0" intransitive(),
      "com.foursquare"            %% "rogue-core"                    % "2.5.1" intransitive(),
      "com.foursquare"            %% "rogue-lift"                    % "2.5.1" intransitive(),
      "com.foursquare"            %% "rogue-index"                   % "2.5.1" intransitive(),
      "org.eclipse.jetty"         % "jetty-server"                   % Ver.jetty,
      "org.eclipse.jetty"         % "jetty-webapp"                   % Ver.jetty,
      "org.apache.kafka"          % "kafka_2.10"                     % "0.8.2.0",
      "org.elasticsearch"         % "elasticsearch"                  % "2.0.0",
      "org.apache.commons"        % "commons-email"                 % "1.4",
      "org.scalaj"                %% "scalaj-http" % "2.2.0",
      "org.quartz-scheduler"      % "quartz" % "2.2.2",
      "com.github.tototoshi"      %% "scala-csv" % "1.3.3",
      "org.apache.httpcomponents" % "httpclient" % "4.5.2",
      "io.github.cloudify"        %% "spdf" % "1.4.0",
      "org.xhtmlrenderer"         % "flying-saucer-pdf" % "9.1.1",
      "com.typesafe"              % "config"            % "1.3.1",
      "com.github.workingDog"     %% "scalaspl" % "1.0"
  ) ++
      provided(
        "org.webjars"       % "jquery"                       % "2.1.3",
        "org.webjars"       % "Semantic-UI"                  % "2.1.4",
        "org.webjars"       % "bootstrap"                    % "3.3.4",
        "org.webjars"       % "d3js"                         % "3.5.6",
        "org.webjars"       % "momentjs"                     % "2.10.6",
        "org.webjars.npm"   % "datatables"                   % "1.10.7",
        //"org.webjars.npm"   % "drmonty-datatables-plugins"   % "1.10.6",
        "org.webjars.npm"   % "d3pie"                        % "0.1.9",
       // "org.webjars"       % "font-awesome"                 % "4.4.0",
        "org.webjars.bower" % "ion.rangeSlider"              % "2.0.10",
        "org.webjars.bower" % "eonasdan-bootstrap-datetimepicker" % "4.15.35",
        "org.webjars.bower" % "datatables-responsive" % "1.0.6",
        "org.webjars" % "elasticsearch-js" % "10.0.1",
        "org.webjars.bower" % "react-d3" % "0.2.2"
      ) ++
      container("org.eclipse.jetty" % "jetty-webapp" % Ver.jetty) ++
      test("org.scalatest" %% "scalatest" % "2.2.6",
        "org.seleniumhq.selenium" % "selenium-java" % "2.35.0",
        "com.github.detro.ghostdriver" % "phantomjsdriver" % "1.0.4")
    )
    .enablePlugins(SbtWeb)
}
