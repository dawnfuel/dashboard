Twitter Bootstrap
-----------------

Bootstrap v3 is included

MongoDB
-------

This app uses MongoDB. Therefore, you will need to either have it installed locally, or use one of the cloud providers and configure it in your props file. See config.MongoConfig for more info.

To give a user admin rights start the sbt console and execute

import code.config._

import code.model._

MongoConfig.init()

import bootstrap.liftweb.Boot

new Boot().boot

import net.liftmodules.mongoauth.MongoAuth

import net.liftmodules.mongoauth.Permission

import net.liftmodules.mongoauth.model.Role

val admin =

Role.createRecord.id("admin").permissions(List(Permission.all)).save()

User.findAll("username","user")(0).roles(List("admin")).save()

Building
--------

This app requires [sbt](http://www.scala-sbt.org/). To build for the first time, run:

    bash$ sbt
    > ~;container:start; container:reload /

The alias `ccr` is provided so you don't have to remember that.

That will start the app and automatically reload it whenever sources are modified. It will be running on http://localhost:8080

sbt-web
-------

[sbt-web](https://github.com/sbt/sbt-web) and associated plugins are used to handle JavaScript, LESS, and font files.

WebJars
-------

WebJars are used to retrieve the jQuery and Twitter bootstrap dependencies.

User Model
----------

This app implements the [Mongoauth Lift Module](https://github.com/eltimn/lift-mongoauth). The registration and login implementation is based on [research done by Google](http://sites.google.com/site/oauthgoog/UXFedLogin) a few years ago and is similar to Amazon.com and Buy.com. It's different than what most people seem to expect, but it can easily be changed to suit your needs since most of the code is part of your project.

BrowserSync
-----------

*Requires: [NodeJS](https://nodejs.org/)*

The sbt build includes a `browserSync` task that will update a file every time the xsbt-web-plugin reloads. This is used in conjuction with the simple node script in the _bs_ folder.

To use, first start the xsbt-web-plugin using the following command:

    sbt> ~ ;container:start ;container:reload / ;browserSync

Or, use the provided alias `ccrs`.

Then, in a separate terminal, go to the bs directory and run:

  bash$ npm install
  bash$ node bs.js

Once that's running it will display the port that you need to point your browser to, default is 3000.

Now, any time any changes are made and the container reloads, the browser will automatically reload as well. It will also reload when any .html files are changed.

Note: this was put in the bs directory because having a package.json file in the top directory caused problems with sbt-web.

Packaging
---------

The `package` task will package the app into a WAR file. The `warPostProcess` function handles creating an SHA-1 digest of each asset file and renames them with the digest. The `Assets` snippet handles figuring out what the digest is for each file.
=======
**        #  **TESTING** #**

- The two styles of testing  used in the testing branch are unit tests and scenario testing  

-Unit testing is used to test the functionality of the different individual components of the system in this case the different classes

-CUD (create,update and delete)operations were carried out on the different classes

-Scenario testing : this style of  testing is used to test the flow of execution of the most common tasks users do on a system or application .This kind of testing requires  that the person writing  the tests to think like the user of the application

-The scenario testing was done in the form of cross browser test(ie the test is executed in two browsers ie chrome and firefox).You would need to get selenium http://www.seleniumhq.org/ and the drivers for the different browsers.

-Scalatest is a library used for writing scala tests  http://www.scalatest.org/

-Two fundamental features to look at  would be the matchers - http://www.scalatest.org/user_guide/using_matchers and the assertions - http://www.scalatest.org/user_guide/using_assertions

-Use sbt test in your project directory to run the different tests at a go.

-Further Notes:A test case should be written for every new feature added to the master branch preferably by developer(s) who did not work on the feature so as to allow for well written test cases(because of biases).



# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Quick summary
* Version
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###


* Summary of set up
* install sbt 0.13.7 http://www.scala-sbt.org/download.html
* git checkout dev
* run sbt "~; container:start ; container:reload /"

* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact