package code.model

import code.BaseMongoSessionWordSpec
import com.sun.corba.se.impl.orbutil.closure.Future
import net.liftweb.common.Full

/**
  * Created by chris on 11/10/16.
  */
class ConfigGroupSpec extends BaseMongoSessionWordSpec{
"ConfigGroup" should{"be created" in{
val testconfiggroup = ConfigGroup.createRecord
    .groupName("testgroup")
    .adminID_FK("test")
    .margin(3)
    .SourceConfigID_FK("testsource")
  /** to ensure it saved*/
  testconfiggroup.save(false)
  val configGroupinDb = ConfigGroup.find(testconfiggroup.id.get)
  configGroupinDb should be ('defined)

  /** to ensure it can be edited*/
  testconfiggroup.groupName("testgroup2").update
  ConfigGroup.find("groupName","testgroup2") match{
    case Full(group) => {
      assert (1 == 1)
    }
    case _ => {
      fail("Failed to update ConfigGroup")
    }
  }
  /** to ensure it can be deleted*/
  testconfiggroup.delete_!
  val configGroupnotinDb = ConfigGroup.find(testconfiggroup.id.get)
configGroupnotinDb should not be ('defined)
   }

 }

}
