package code.model

import code.BaseMongoSessionWordSpec
import net.liftweb.common.Full
import org.scalacheck.Prop
/**
  * Created by chris on 11/14/16.
  */
class UserGroupSpec extends BaseMongoSessionWordSpec {
  "User Group" should {
    "be created, be edited,be deleted" in {
      val Members = List("testuser","testuser2","testuser3")
      val testGroup = UserGroup.createRecord
        .groupName("TestuserGroup")
        .margin(30)
        .adminID_FK("testadmin")
        .members(Members)
      assert(testGroup.groupName.get.length >= 3)/** since groupName cannot be less than 3 characters */


      testGroup.save()

      val testGroupinDB = UserGroup.find(testGroup.id.get)
      testGroupinDB should be ('defined)

      /** to  test if it can be edited*/

      testGroup.groupName("testGroup2").update
      assert(testGroup.groupName.get.length >= 3)
      UserGroup.find("groupName","testGroup2") match {
        case Full(group) =>{
          assert(1==1)
        }
        case _ =>{
          fail("Cannot update userGroup ")
        }
      }

      /** to ensure testGroup can be deleted*/
      testGroup.delete_!
      val testGroupnotinDB = UserGroup.find(testGroup.id.get)
      testGroupnotinDB should not be ('defined)

    }

  }
}