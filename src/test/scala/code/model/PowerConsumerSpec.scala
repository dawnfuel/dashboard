package code.model

import code.BaseMongoSessionWordSpec
import net.liftweb.common.Full
/**
  * Created by chris on 11/7/16.
  */
class PowerConsumerSpec extends BaseMongoSessionWordSpec{
  "Consumer Group" should {
    "be created,be edited and be deleted" in {
      val testConsumerGroup = PowerConsumerGroup.createRecord
        .ConfigID_FK("testConfig")
        .Name("consumerGroup")
        .Type("Estate")

      /** to ensure it saved properly */
      testConsumerGroup.save()
      val groupinDb = PowerConsumerGroup.find(testConsumerGroup.id.get)
      groupinDb should be ('defined)

      /** to test if it can edited */
      testConsumerGroup.Name("consumerGroup2").update
      PowerConsumerGroup.find("Name","consumerGroup2") match {
        case Full(consumer) => {
          assert(1 == 1)
        }
        case _ => {
          fail("Oops, failed to update Group")
        }
      }
      /** to ensure it was deleted */
      testConsumerGroup.delete_!
      val groupnotinDb = PowerConsumerGroup.find(testConsumerGroup.id.get)
      groupnotinDb should not be ('defined)

    }
  }

  /** test for consumer line */
  "Consumer Line" should {
    "be created,be edited and be deleted" in {
      val testconsumerline = PowerConsumerLine.createRecord
        .Name("testconsumerline")
        .NumberOfPhases(1)
          .Type("testprobe")

      /** test for creation */
        testconsumerline.save()
      val lineinDb = PowerConsumerLine.find(testconsumerline.id.get)
      lineinDb should be ('defined)

      /** test for editing*/
      testconsumerline.Name("testconsumerline2").update
      PowerConsumerLine.find("Name","testconsumerline2") match {
        case Full(consumer) => {
          assert(1 == 1)
        }
        case _ => {
          fail("Oops, failed to update line")
        }
      }
      /** test for deleting */
      testconsumerline.delete_!
      val linenotinDb = PowerConsumerLine.find(testconsumerline.id.get)
      linenotinDb should not be ('defined)
    }
  }
  /** test for consumer*/
  "Consumer" should {
    "be created,be edited and be deleted" in {
      val testconsumer = PowerConsumer.createRecord
        .LineID_FK("testline")
        .Name("testmachine")
        .NumberOfPhases(3)

      /** test for creation*/
      testconsumer.save(false)
      val consumerinDb = PowerConsumer.find(testconsumer.id.get)
      consumerinDb should be ('defined)

      /** to test if it can be edited*/
      testconsumer.Name("testmachine2").update
      PowerConsumer.find("Name","testmachine2") match {
        case Full(consumer) => {
          assert(1 == 1)
        }
        case _ => {
          fail("Oops, failed to update line")
        }
      }

      /** tests for deleting*/
      testconsumer.delete_!
      val consumernotinDb = PowerConsumer.find(testconsumer.id.get)
      consumernotinDb should not be ('defined)
    }
  }
}
