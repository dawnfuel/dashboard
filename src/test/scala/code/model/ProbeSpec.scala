package code
package model

import java.nio.channels.Channels

import code.BaseMongoSessionWordSpec
import code.model.Probe.Channels
import net.liftweb.common.Full
import org.scalatest.FlatSpec

/**
  * Created by chris on 10/19/16.
  */
class ProbeSpec extends BaseMongoSessionWordSpec {

  "Probe" should {
    "be created,saved properly,be edited, be deleted" in {

        /**to test if probe can be created*/
      val testprobe = Probe.createRecord
        .probeName("Test-probe")
        .NumberOfChannels(12)
        .ScalingFactorCurrent(0.25)
        .wifi_ssid("Test")
        .wifi_password("password")


      /** to create channels*/
      var channels : List[SignalAssignment] = List()
      val a = 0
      for ( a <- 1 to testprobe.NumberOfChannels.get) {
        channels = SignalAssignment.createRecord
          .sourceChannel(a - 1)
          .SourceName("Grid")
          .ConfigID_FK("test-config") ::channels
      }
      testprobe.Channels(channels).update



      /** to test if it saved properly*/
      testprobe.save()
      /** test for created channels */
      Probe.find("Channels.SourceName","Grid") match {
        case Full(p) =>{
          assert (1==1)
        }
        case _ =>{
          fail("Didnot find channel")
        }
      }
      val newprobeFromDb =Probe.find(testprobe.id.get)
      newprobeFromDb should  be ('defined) /** same as newprobenotFromDb.isDefined should equal (true)*/
      val  p = Probe.findAll("probeName","Test-probe")
      if (p.nonEmpty){
        assert(p.head.wifi_password.toString() == "password")
      }
      else{
        fail("Oops did not save Probe properly")
      }

      /** to test if probe can be edited*/
      testprobe.probeName("Test-probe2").update
      Probe.find("probeName","Test-probe2") match {
        case Full(probe) => {
          assert(1 == 1)
        }
        case _ => {
          fail("Oops, failed to update Probe")
        }
      }

      /** to test if probes can be deleted*/
      testprobe.delete_!
      val newprobenotFromDb=Probe.find(testprobe.id.get)
      newprobenotFromDb should not be ('defined)
      /**same as newprobenotFromDb.isDefined should equal (false)*/

    }

  }
}