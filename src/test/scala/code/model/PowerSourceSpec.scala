package code.model

import code.BaseMongoSessionWordSpec
import net.liftweb.common.Full

/**
  * Created by chris on 11/3/16.
  */
class PowerSourceSpec extends BaseMongoSessionWordSpec {
  /** since phase must be either 1phase or 3phase*/
  def matchphase(a:Int)={ a match {
    case 1 => {
      assert (1 == 1)
    }
    case 3 => {
      assert (1 == 1)
    }
    case  _ => {
      fail()
    }
   }
  }

  /** to ensure source name is not less than 3 characters*/
  def len(any:String)={
    assert(any.length() >= 3)
  }

  /** for Grid */
"Grid" should {
  "be created , be edited ,be deleted" in{
   val testgrid = Grid.createRecord
     .sourceName("test-grid")
     .Type("grid")
     .ProbeID_FK("test-probe")
     .NumberOfPhases(3)
     .MaxChannels(3)
    len(testgrid.sourceName.toString())
    matchphase(testgrid.NumberOfPhases.toString().toInt)

    /** to test if it exists */
    testgrid.save(false)
    val gridFromDb = Grid.find(testgrid.id.get)
    gridFromDb should  be ('defined)

    /** to test if it can be edited */
    testgrid.sourceName("test-grid2").update
    Grid.find("sourceName","test-grid2") match {
      case Full(config) => {
        assert (1 == 1)
      }
      case _ => {
        fail("Oops,failed to update Grid")
      }

    }

    /** to test if deletes */
    testgrid.delete_!
    val gridnotinDb = Grid.find(testgrid.id.get)
    gridnotinDb should not be ('defined)
  }

  }
  /** for generator */
  "Generator" should{ "be created,be edited ,be deleted" in{
    val testgen = Generator.createRecord
      .Type("Generator")
      .sourceName("test-gen")
      .FuelType("Diesel")
      .ProbeID_FK("test-probe")
      .FuelStore(110)
      .FuelUnitPrice(130)
      .NumberOfPhases(1)
    len(testgen.sourceName.toString())
    matchphase(testgen.NumberOfPhases.toString().toInt)

    /** to test if it exists */
    testgen.save(false)
    val geninDb = Generator.find(testgen.id.get)
    geninDb should be ('defined)

    /**to test if it can edited*/
testgen.NumberOfPhases(1).update
    Generator.find("NumberOfPhases",1) match {
      case Full(config) => {
        assert (1 == 1)
      }
      case _ => {
        fail("Oops,failed to update Generator")
      }

    }
    /** to test if it can be deleted */
    testgen.delete_!
    val gennotinDb = Grid.find(testgen.id.get)
    gennotinDb should not be ('defined)
   }
  }

  /** for inverter */
"Inverter" should{"be created ,be edited, be deleted" in {
  val testinv = Inverter.createRecord
    .sourceName("test-inverter")
    .Type("Inverter")
    .depthOfDischarge(50)
    .batteryCapacity(40000)
    .NumberOfPhases(1)
  matchphase(testinv.NumberOfPhases.toString().toInt)
  len(testinv.sourceName.toString())

  /** to ensure it saved */
  testinv.save(false)
  val invinDb = Inverter.find(testinv.id.get)
  invinDb should be ('defined)

  /** to test if it can be edited */
  testinv.depthOfDischarge(40).update
  Inverter.find("depthOfDischarge",40) match {
    case Full(inv) => {
      assert(1 == 1)
    }
    case _ => {
      fail("Failed to update Inverter")
    }
  }

  /** to ensure it can be deleted  */
  testinv.delete_!
  val invnotinDb= Inverter.find(testinv.id.get)
  invnotinDb should not be ('defined)

  }


 }
}
