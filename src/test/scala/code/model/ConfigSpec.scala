package code.model

import code.BaseMongoSessionWordSpec
import net.liftweb.common.Full
import net.liftweb.mongodb.record.field.BsonRecordField
import org.scalatest.Assertions._

/**
  * Created by chris on 10/25/16.
  */
class ConfigSpec extends BaseMongoSessionWordSpec{
 "config" should{"be created, be deleted" in{
   val testAddress =
     Address.createRecord
       .city("testlagos")
       .state("lagos")
       .UserID_FK("test-user")

   val testconfig = Configuration.createRecord
     .configurationName("Test-config")
     .configurationUser("Testuser")
     .ErrorThreshold(3)
     .AdminID_FK("testadminid")
     .ConfigAddress(testAddress)

   assert(testconfig.configurationName.toString().length() >= 3)
   /** to test if it saved properly */
   testconfig.save()
  val configFromDb =Configuration.find(testconfig.id.get)
   configFromDb should  be ('defined)
   val c = Configuration.findAll("configurationName","Test-config")
   if (c.nonEmpty) {
     assert(c.head.AdminID_FK.toString() == "testadminid")
   } else {
     fail("Oops,configuration did not  save properly")
   }

   /** to test if can be edited */
   testconfig.configurationUser("Testuser2").update
   Configuration.find("configurationUser","Testuser2") match {
     case Full(config) => {
       assert (1 == 1)
     }
     case _ => {
       fail("Oops,failed to update configuration")
     }

   }


   /** test if it was deleted properly */
   testconfig.delete_!
   val confignotinDB = Configuration.find(testconfig.id.get)
   confignotinDB should not be  ('defined)

 }


 }
}
