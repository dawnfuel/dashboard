package code.modele

import code.BaseMongoSessionWordSpec
import code.model.Fuel
import net.liftweb.common.Full

/**
  * Created by chris on 11/3/16.
  */
class FuelSpec extends BaseMongoSessionWordSpec{
  "Fuel" should{ "be created, be deleted" in{
    val testfuel = Fuel.createRecord
      .FuelType("Diesel")
      .FuelUnitPrice(96)
      .UserID_FK("testuserid")

    /** to ensure it saved properly*/
    testfuel.save(false)
    val fuelinDb = Fuel.find(testfuel.id.get)
    fuelinDb should be ('defined)

    /** to test if it can be edited */
    testfuel.FuelUnitPrice(150).update
    Fuel.find("FuelUnitPrice","150") match {
      case Full(fuel) => {
        assert(1 == 1)
      }
      case _ => {
        fail("Failed to update")
      }
    }

    /** to ensure it can be deleted*/
    testfuel.delete_!
    val fuelnotinDb = Fuel.find(testfuel.id.get)
    fuelnotinDb should not be ('defined)
   }
  }

}
