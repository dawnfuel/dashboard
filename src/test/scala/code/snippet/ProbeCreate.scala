package code.snippet

import java.util.concurrent.TimeUnit

import code.BaseMongoSessionWordSpec
import org.openqa.selenium.{By, Capabilities, WebDriver}
import org.openqa.selenium.chrome.ChromeDriver
import org.openqa.selenium.firefox.{FirefoxDriver, MarionetteDriver}
import org.openqa.selenium.remote.DesiredCapabilities

/**
  * Created by chris on 11/21/16.
  */
class ProbeCreate extends BaseMongoSessionWordSpec {
  var driver: WebDriver = _

  var browser: Int = _

  var BrowserName: String = _

  val caps = new DesiredCapabilities()
  caps.setCapability("webStorageEnabled",false)

  "User" can {
    "login and create probe " in {
      /** choosing the browser to use */
      browser = 1
      val noofbrowsers = 2
      while (browser < noofbrowsers+1) {
        if (browser == 1) {
          System.setProperty("webdriver.gecko.driver","/usr/bin/geckodriver")
          val capabilities = DesiredCapabilities.firefox()
          capabilities.setCapability("marionette", true)
          driver = new FirefoxDriver(capabilities)
          BrowserName = "Mozilla Firefox"
        }

        if (browser == 2) {
          System.setProperty("webdriver.chrome.driver","/usr/bin/chromedriver")
          driver = new ChromeDriver()
          BrowserName = "Google Chrome"
        }

        val host = "http://localhost:8082"
        val loginURL = host + "/login"
        val home = "GRIT SYSTEMS : Home"
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS)
        /** Navigate to login page */
        driver.get(loginURL)
        /** enter email */
        val email = driver.findElement(By.name("email"))
        email.sendKeys("test@test.com")
        /** enter user password*/
        val password = driver.findElement(By.id("id_password"))
        password.sendKeys("testuser")

        //submit
        val sub = driver.findElement(By.id("submit"))
        sub.click()

        Thread.sleep(6000)

        assert(driver.getTitle() === home)

        /**navigating to settings*/
        val set = driver.findElement(By.partialLinkText("Settings"))
          set.click()
        assert(driver.getCurrentUrl() === "http://localhost:8082/system/fuel/listfuel")
        Thread.sleep(6000)
        /** navigating to probes*/
        driver.findElement(By.linkText("Probes")).click()
        assert(driver.getCurrentUrl() === "http://localhost:8082/system/probe/listprobe.html")
        Thread.sleep(6000)
        /** creating the probes*/
        driver.findElement(By.linkText("Create Probe")).click()
        assert(driver.getCurrentUrl() === "http://localhost:8082/system/probe/createprobe")
        Thread.sleep(6000)
        driver.findElement(By.id("probename")).sendKeys("testprobe")
        Thread.sleep(6000)
        driver.findElement(By.id("submit")).click()
        Thread.sleep(6000)
        assert (driver.getCurrentUrl() === "http://localhost:8082/system/probe/listprobe")
        assert(driver.findElement(By.xpath("//input[@value='testprobe']")).getSize() != 0)/** to check if the probe was added to the list*/
        println("Probe created on " + BrowserName)
        driver.quit()
        browser += 1

      }
    }
  }
}