package code.snippet

import java.util.concurrent.TimeUnit

import code.BaseMongoSessionWordSpec
import org.openqa.selenium.{By, WebDriver}
import org.openqa.selenium.chrome.{ChromeDriver, ChromeOptions}
import org.openqa.selenium.firefox.{FirefoxDriver, MarionetteDriver}
import org.openqa.selenium.remote.DesiredCapabilities
/**
  * Created by chris on 11/22/16.
  */
class NotificationSpec extends BaseMongoSessionWordSpec {
  var driver: WebDriver = _

  var browser: Int = _

  var BrowserName: String = _

  val caps = new DesiredCapabilities()
  caps.setCapability("webStorageEnabled",false)



  "User" can {
    "login and set custom notification" in {
      /** choosing the browser to use */
      browser = 1
      val noofbrowsers = 2
      while (browser < noofbrowsers+1) {

        if (browser == 1) {
          System.setProperty("webdriver.gecko.driver","/usr/bin/geckodriver")
          val capabilities = DesiredCapabilities.firefox()
          capabilities.setCapability("marionette", true)
          driver = new FirefoxDriver(capabilities)
          BrowserName = "Mozilla Firefox"
        }

        if (browser == 2) {
          System.setProperty("webdriver.chrome.driver","/usr/bin/chromedriver")
          driver = new ChromeDriver()
          BrowserName = "Google Chrome"
        }

        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS)

        driver.get("http://localhost:8082/login")
        driver.findElement(By.name("email")).sendKeys("test@test.com")/** to enter email */
        driver.findElement(By.id("id_password")).sendKeys("testuser")/** to enter user password*/
        driver.findElement(By.id("submit")).click

        driver.findElement(By.linkText("Notifications")).click()
        assert(driver.getCurrentUrl() === "http://localhost:8082/system/configuration/notification")
        /** add email*/
        driver.findElement(By.id("addEmail")).sendKeys("kl@gmail.com")
        driver.findElement(By.id("addEmailBttn")).click()
        val emailList = driver.findElement(By.xpath("//Select[@id='emailList']/option[normalize-space(text())='kl@gmail.com']"))
        assert(emailList.getSize != 0)/** ensures email was added*/
        /** add phone .*/
        driver.findElement(By.id("addPhone")).sendKeys("07086311111")
        driver.findElement(By.id("addPhoneBttn")).click()
        val phonelst = driver.findElement(By.xpath(
          "//Select[@id='phoneList']/option[normalize-space(text())='07086311111']"))
        assert(phonelst.getSize() != 0)/**ensures phone no was added*/
        Thread.sleep(100)
        driver.findElement(By.xpath("//Select[@name='dodBelow']/option[@value='10']")).click()
        Thread.sleep(1000)
        driver.findElement(By.xpath("//input[@value='flag'][@name='dodBelow']")).click()/**clicks on elements with value=flag and name=dodBelow*/
        driver.findElement(By.xpath("//input[@value='sms'][@name='fuelBelowX']")).click()
        driver.findElement(By.xpath("//input[@value='email'][@name='oWindow']")).click()
        driver.findElement(By.id("saveNotifConfig")).click()/** saves notification*/
        assert(driver.getTitle() == "GRIT SYSTEMS : Home")
        println("Notification test passed on " + BrowserName)
        driver.quit()
        browser += 1
      }
    }
  }
}