package code.snippet

/**
  * Created by grit on 5/13/16.
  */

import java.util.Date
import java.util.concurrent.TimeUnit

import code.{BaseMongoSessionWordSpec, TestMongo}
import com.mongodb._
import org.openqa.selenium.chrome.ChromeDriver
import org.scalatest.selenium.{Chrome, WebBrowser}
import org.openqa.selenium.By
import org.openqa.selenium.firefox.{FirefoxDriver, MarionetteDriver}
import org.openqa.selenium.WebDriver
import org.openqa.selenium.remote.DesiredCapabilities

import org.scalatest.FlatSpec
import org.openqa.selenium.phantomjs.PhantomJSDriver

class LoginSpec extends BaseMongoSessionWordSpec{

  try{
  val role = new BasicDBList()
  role.add("admin")

    val mongoClient = new MongoClient( "localhost" , 27017 )
    val db = mongoClient.getDB( "lift-mongo-app" )
    val  coll = db.getCollection("user.users")

  /**deletes testuser 1 hour after creation and creates user if the user does not exist*/
  coll.createIndex(new BasicDBObject("time", 1), new BasicDBObject("expireAfterSeconds", 3600))

    val doc = new BasicDBObject( "email","test@test.com").
      append("password","$2a$10$tfEzNZ9o9gojGDnEonKRGu.v.otRQwlG0RqvYkW1oaa0b.JJKQDIm").
      append("roles",role)
    .append("time",new Date())
     coll.save(doc)
    print("User created and saved")
  }
  catch {
    case e: DuplicateKeyException =>{
      println("User is already saved")
    }
  }

  private val GUI_PORT = 8082
  var host = "http://localhost:" + GUI_PORT.toString

    var driver: WebDriver = _

    var browser: Int = _

    var BrowserName: String = _


    "User" should { "login" in {
      browser = 1
      val noofbrowsers = 2
      while (browser < noofbrowsers+1) {

        if (browser == 1) {
        System.setProperty("webdriver.gecko.driver","/usr/bin/geckodriver")
      val capabilities = DesiredCapabilities.firefox()
      capabilities.setCapability("marionette", true)
      driver = new FirefoxDriver(capabilities)
      BrowserName = "Mozilla Firefox"
        }

        if (browser == 2) {
        System.setProperty("webdriver.chrome.driver","/usr/bin/chromedriver")
      driver = new ChromeDriver()
      BrowserName = "Google Chrome"
      }

        val  host = "http://localhost:8082"
        val loginURL = host + "/login"
        val home = "GRIT SYSTEMS : Home"

        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS)

        /**Navigate to login page*/
        driver.get(loginURL)

        /**enter email*/
        val email = driver.findElement(By.name("email"))
        email.sendKeys("test@test.com")


        //enter user password
        val password = driver.findElement(By.id("id_password"))
        password.sendKeys("testuser")
        //submit
        val sub = driver.findElement(By.id("submit"))
        sub.click()

        Thread.sleep(6000)

        println(driver.getCurrentUrl)

        if(driver.getTitle() == home){
          println("Able to login on " + BrowserName)
        }
        else{
          println("Failed to login on "+ BrowserName)

        }
        driver.quit()
        browser = browser + 1
      }
    }
    }
  }

