package bootstrap.liftweb

import java.util.regex.Pattern

import code.comet.MyScheduledTask
import code.snippet.{ScheduleTaskHelper, serveUpdate}
import net.liftweb.proto.ProtoRules
import org.quartz.CronScheduleBuilder._
import org.quartz.JobBuilder._
import org.quartz.TriggerBuilder._
import org.quartz.impl.StdSchedulerFactory

import scala.xml.{Null, UnprefixedAttribute}
import javax.mail.internet.MimeMessage

import net.liftweb.common._
import net.liftweb.http._
import net.liftweb.util._
import net.liftweb.util.Helpers._
import net.liftweb.http.provider.HTTPParam
import code.config._
import code.model.User
import code.lib.BasicExample
import net.liftmodules.extras.{Gravatar, LiftExtras}
import net.liftmodules.mongoauth.MongoAuth
import code.rest.{DBQueryAPI, ESQueryAPI, Realtime, VersionCheck}
/**
 * A class that's instantiated early and run.  It allows the application
 * to modify lift's environment
 */
class Boot extends Loggable {
  def boot {

    ProtoRules.emailRegexPattern.default.set( Pattern.compile("^[a-z0-9._%\\-+]+@(?:[a-z0-9\\-]+\\.)+[a-z]{2,9}") )
    logger.info("Run Mode: "+Props.mode.toString)
    // Force https for Production
    if (Props.mode == Props.RunModes.Production){
      logger.info("Force https for production")
      LiftRules.earlyResponse.append { (req: Req) =>
        if (req.request.scheme != "https") {
          val uriAndQuery = req.uri +
            (req.request.queryString.map(s => "?"+s) openOr "")
          val uri = "https://%s%s".format(req.request.serverName, uriAndQuery)
          Full(PermRedirectResponse(uri, req, req.cookies: _*))
        }
        else Empty
      }
    }
    // init the ElasticSearch query system
    ESQueryAPI.init()
    // init realtime graphs and clocks
    Realtime.init()
    // init mongodb
    MongoConfig.init()
    //API for Database queries
    DBQueryAPI.init()
    //API to Check Versions
    VersionCheck.init()

    System.setProperty("DEBUG.MONGO", "true")
    System.setProperty("DB.TRACE", "true")
    // init auth-mongo
    MongoAuth.authUserMeta.default.set(User)
    MongoAuth.loginTokenAfterUrl.default.set(Site.password.url)
    MongoAuth.siteName.default.set("Lift Mongo App")
    MongoAuth.systemEmail.default.set("help@localhost.com")
    MongoAuth.systemUsername.default.set("Lift Mongo App")


    //MyScheduledTask ! MyScheduledTask.DoIt

    //LiftRules.unloadHooks.append( () => MyScheduledTask ! MyScheduledTask.Stop )

    LiftRules.cometRequestTimeout = Full(1)

    // For S.loggedIn_? and TestCond.loggedIn/Out builtin snippet
    LiftRules.loggedInTest = Full(() => User.isLoggedIn)
    // checks for ExtSession cookie
    LiftRules.earlyInStateful.append(User.testForExtSession)

    // Gravatar
    // Gravatar.defaultImage.default.set("wavatar")

    // config an email sender
    SmtpMailer.init


    //probeUpdates
    serveUpdate.init()
    LiftRules.handleMimeFile = OnDiskFileParamHolder.apply

    // where to search snippet
    LiftRules.addToPackages("code")
    // set the default htmlProperties
    LiftRules.htmlProperties.default.set((r: Req) => new Html5Properties(r.userAgent))

    // Build SiteMap
    LiftRules.setSiteMap(Site.siteMap)

    LiftRules.supplimentalHeaders = s => s.addHeaders(
      List(HTTPParam("X-Lift-Version", LiftRules.liftVersion),
        HTTPParam("Access-Control-Allow-Origin", "*"),
        HTTPParam("Access-Control-Allow-Credentials", "true"),
        HTTPParam("Access-Control-Allow-Methods", "GET, POST, PUT, OPTIONS"),
        HTTPParam("Access-Control-Allow-Headers", "WWW-Authenticate,Keep-Alive,User-Agent,X-Requested-With,Cache-Control,Content-Type")
      ))
    LiftRules.statelessDispatchTable.append(BasicExample.findItem)
    LiftRules.statelessDispatchTable.append(BasicExample.extractFindItem)
    // LiftRules.dispatch.append(BasicExample.findItem)
    // LiftRules.dispatch.append(BasicExample.extractFindItem)
//    LiftRules.onBeginServicing.append {
//      case r => println("Received: "+r)
//    }
    // Error handler
    ErrorHandler.init

    // 404 handler
    LiftRules.uriNotFound.prepend(NamedPF("404handler") {
      case (req, failure) =>
        NotFoundAsTemplate(ParsePath(List("404"), "html", false, false))
    })

    // Show the spinny image when an Ajax call starts
    LiftRules.ajaxStart =
      Full(() => LiftRules.jsArtifacts.show("ajax-spinner").cmd)

    // Make the spinny image go away when it ends
    LiftRules.ajaxEnd =
      Full(() => LiftRules.jsArtifacts.hide("ajax-spinner").cmd)

    // Force the request to be UTF-8
    LiftRules.early.append(_.setCharacterEncoding("UTF-8"))

    // Init Extras
    LiftExtras.init()

    //make notices fade out
    LiftRules.noticesAutoFadeOut.default.set((noticeType: NoticeType.Value) => Full((3 seconds, 3 seconds)))

    // don't include the liftAjax.js code. It's served statically.
    LiftRules.autoIncludeAjaxCalc.default.set(() => () => (session: LiftSession) => false)

    // Mailer
 /*   Mailer.devModeSend.default.set((m: MimeMessage) => logger.info("Dev mode message:\n" + prettyPrintMime(m)))
    Mailer.testModeSend.default.set((m: MimeMessage) => logger.info("Test mode message:\n" + prettyPrintMime(m)))*/
  }

  private def prettyPrintMime(m: MimeMessage): String = {
    val buf = new StringBuilder
    val hdrs = m.getAllHeaderLines
    while (hdrs.hasMoreElements)
      buf ++= hdrs.nextElement.toString + "\n"

    val out =
      """
        |%s
        |====================================
        |%s
      """.format(buf.toString, m.getContent.toString).stripMargin

    out
  }
}
