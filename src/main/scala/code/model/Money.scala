package code.model

import net.liftweb.mongodb.record.{BsonMetaRecord, BsonRecord}
import net.liftweb.record.field.{DecimalField, StringField}


/**
  *  Embedded Money Class
  */
class Money extends BsonRecord[Money] {
  override def meta = Money
  object Amount extends DecimalField(this,0.0)
  object ccode extends StringField(this,30,"NGN")
}

object Money extends Money with BsonMetaRecord[Money]





