package code.model

import code.lib.RogueMetaRecord
import code.model.PowerConsumer.Name
import com.sun.xml.internal.bind.v2.runtime.Coordinator
import net.liftweb.mongodb._
import net.liftweb.mongodb.record._
import net.liftweb.mongodb.record.field._
import net.liftweb.record._
import net.liftweb.record.field._
import net.liftweb.util.FieldError

class Address private () extends MongoRecord[Address] with StringPk[Address]  {
  override def meta = Address
  object UserID_FK extends StringRefField(this, User, 128)
  object postalcode extends IntField(this)
  object zone extends StringField(this,50)
  object subnet extends StringField(this,50)
  object latitude extends DecimalField(this,30)
  object longitude extends DecimalField(this,30)
  object street extends StringField(this, 100)
  object city extends StringField(this, 100)
  object country extends StringField(this, 100)
  object state extends StringField(this, 100)
  object zip extends StringField(this,100)


}
object Address extends Address with RogueMetaRecord[Address]{
  override def collectionName = "address"
}

/** @constructor creates new GeoShape with given coordinates */
class GeoShape{
  var cord = Seq[(Double,Double)]()

  /** @param coordinates sequence of tuple coordinate
    * */
  def setShape(coordinates: Tuple2[Double, Double]*){
    cord = coordinates
  }
  //returns the coordinates that defines a shape
  def getShape() = {cord}
}
object GeoShape extends GeoShape

//Location
class Location extends BsonRecord[Location] {
  override def meta = Location
  object long extends DoubleField(this,0)
  object lat extends DoubleField(this,0)
}
object Location extends Location with BsonMetaRecord[Location]

class Region{
  val Name:String = ""
  /**Sequence of tuple (long,lat) of coordinates that define GeoShape*/
  val GeoShape = Seq[(Double,Double)]()
}