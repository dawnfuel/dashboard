package code.model

/**
 * created by yoda on 03/03/16.
 *
 *  GRIT Systems Engineering Proprietary and Confidential
 * ©2016
 *
 *   This source code is copyrighted. Copying and/or transmitting
 *   portions or all of this work without permission is a violation of applicable law.
 *   No use of this publication may be made for resale or for any other commercial
 *   purpose whatsoever without prior permission in writing from GRIT Systems Engineering .
 *
 */

import java.util.Properties
import java.util.regex.Pattern

import code.lib.RogueMetaRecord
import code.model.Generator.sourceName._
import net.liftweb.mongodb.record._
import net.liftweb.mongodb.record.field._
import net.liftweb.record.field._
import net.liftweb.util.FieldError
import org.apache.kafka.clients.producer._
import org.joda.time.DateTime
import code.model.Probe._

class PowerConsumerGroup private () extends MongoRecord[PowerConsumerGroup] with StringPk[PowerConsumerGroup] {
  override def meta = PowerConsumerGroup
  object Name extends StringField(this, 30)
  object ConfigID_FK extends StringRefField(this, Configuration, 128)
  object Type extends StringField(this, 30)

  def getPowerConsumerGroups(configIDs:List[String]):List[PowerConsumerGroup]={
    var p:List[PowerConsumerGroup] = List()
    for(c <- configIDs){
      p = p ::: PowerConsumerGroup.findAll("ConfigID_FK",c)
    }
    return p
  }

  def getPowerConsumerGroupIDs(configIDs:List[String]):List[String]={
    var p_IDs:List[String] = List()
    for(c <- configIDs){
      p_IDs = p_IDs ::: PowerConsumerGroup.findAll("ConfigID_FK",c).map(p => p.id.toString())
    }
    return p_IDs
  }
  def deleteGroupLinesConsumers ( group : PowerConsumerGroup )  = {
    PowerConsumerLine.findAll("GroupID_FK",group.id.value).map{ line =>
      deleteConsumerLines(line)
    }
    group.delete_!
  }

  def deleteConsumerLines(line : PowerConsumerLine){
    val ConfigID_FK = PowerConsumerGroup.findAll("_id",line.GroupID_FK.toString).head.ConfigID_FK.toString
    val channels = findChannels(List(ConfigID_FK))
    val probes_associated = channels.map{ ch => ch._1}.map{ p_ch => (p_ch.split("_")(0),p_ch.split("_")(1))}.groupBy(_._1)//.map{ p => p._1}
    var counter_channel = 0
    probes_associated.map{  probes =>
      val ch_assigned = probes._2.map{ ch => ch._2.toInt }.toList
      Probe.findAll("_id",probes._1).map{ probe =>
        probe.Channels.get.foreach( a => {
          if(ch_assigned.contains(counter_channel) && a.SourceID_FK.toString == line.id.toString){
            a.SourceID_FK("")
            a.SourceType("")
            a.SourceName("")
            a.phase(0)
          }
          counter_channel += 1
        } )
        probe.save()
      }
    }
    PowerConsumer.findAll("LineID_FK",line.id.toString).map{s =>
      s.delete_!
    }
    line.delete_!
  }

}
object PowerConsumerGroup extends PowerConsumerGroup with  RogueMetaRecord[PowerConsumerGroup]  {
  override def collectionName = "consumer_groups"
}

class PowerConsumerLine private () extends MongoRecord[PowerConsumerLine] with StringPk[PowerConsumerLine] {
  override def meta = PowerConsumerLine
  object Name extends StringField(this, 30){
    override def validations = {
      valMinLen(3, "Name cannot be less than 3 characters.")_  ::
      valRegex(Pattern.compile("^\\w*$"), "Please Remove special Characters and spaces") _:: super.validations
    }
  }
  object ProbeID_FKs extends StringRefListField(this, Probe)
  object GroupID_FK extends StringRefField(this, PowerConsumerGroup, 128)
  object NumberOfPhases extends IntField(this,3)
  object Type extends StringField(this, 30)
  object State extends IntField(this,0)
  object SignalThreshold extends IntField(this, 0) //not added to UI elements yet


  def getPowerConsumerLines(groupIDs:List[String]):List[PowerConsumerLine]={
    var p:List[PowerConsumerLine] = List()
    for(c <- groupIDs){
      p = p ::: PowerConsumerLine.findAll("GroupID_FK",c)
    }
    return p
  }

  def getPowerConsumerLineIDs(groupIDs:List[String]):List[String]={
    var p_IDs:List[String] = List()
    for(c <- groupIDs){
      p_IDs = p_IDs ::: PowerConsumerLine.findAll("GroupID_FK",c).map(p => p.id.toString())
    }
    return p_IDs
  }

}
object PowerConsumerLine extends PowerConsumerLine with  RogueMetaRecord[PowerConsumerLine]  {
  override def collectionName = "consumer_lines"
}

class PowerConsumer private () extends MongoRecord[PowerConsumer] with StringPk[PowerConsumer] {
  override def meta = PowerConsumer
  object Name extends StringField(this, 30)
  object LineID_FK extends StringRefField(this, Configuration, 128)
  object NumberOfPhases extends IntField(this,3)
  object State extends IntField(this,0)


  def getPowerConsumer(groupIDs:List[String]):List[PowerConsumer]={
    var p:List[PowerConsumer] = List()
    for(c <- groupIDs){
      p = p ::: PowerConsumer.findAll("GroupID_FK",c)
    }
    return p
  }

  def getPowerConsumerIDs(groupIDs:List[String]):List[String]={
    var p_IDs:List[String] = List()
    for(c <- groupIDs){
      p_IDs = p_IDs ::: PowerConsumer.findAll("GroupID_FK",c).map(p => p.id.toString())
    }
    return p_IDs
  }

}
object PowerConsumer extends PowerConsumer with  RogueMetaRecord[PowerConsumer]  {
  override def collectionName = "consumers"
}