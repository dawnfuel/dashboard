package code.model

import code.lib.RogueMetaRecord
import net.liftweb.mongodb.record.MongoRecord
import net.liftweb.mongodb.record.field._
import net.liftweb.record.field.{DecimalField, DoubleField, IntField}

class PaymentSettings  extends MongoRecord[PaymentSettings] with StringPk[PaymentSettings] {
  override def meta = PaymentSettings
  object SubscriptionPrice extends DecimalField(this,0.0)
  object exchangeRate extends DoubleField(this)
  object margin extends DoubleField(this)
  object leaseInterest extends  DoubleField(this)
  object VAT extends DoubleField(this)
  object lastReferenceNumber extends IntField(this,0)
}
object PaymentSettings extends PaymentSettings with RogueMetaRecord[PaymentSettings]{
  override def collectionName = "PaymentSettings"
}


