package code.model

/**
 * created by yoda on 7/10/15.
 *
 *  GRIT Systems Engineering Proprietary and Confidential
 * ©2015
 *
 *   This source code is copyrighted. Copying and/or transmitting
 *   portions or all of this work without permission is a violation of applicable law.
 *   No use of this publication may be made for resale or for any other commercial
 *   purpose whatsoever without prior permission in writing from GRIT Systems Engineering .
 *
 */
import java.util.regex.Pattern

import code.lib.RogueMetaRecord
import net.liftweb.common.{Box, Empty, Full}
import net.liftweb.http.S._
import net.liftweb.http.SHtml.{ElemAttr, _}
import net.liftweb.http.js.JE.{AnonFunc, Call, JsRaw}
import net.liftweb.http.js.JsCmds.JsCrVar
import net.liftweb.http.js.{JsCmd, JsExp}
import net.liftweb.http.js.JsCmd._
import net.liftweb.mongodb._
import net.liftweb.mongodb.record._
import net.liftweb.mongodb.record.field._
import net.liftweb.record._
import net.liftweb.record.field._

import scala.xml.{Elem, Null, UnprefixedAttribute}

class Configuration private () extends MongoRecord[Configuration] with StringPk[Configuration] {
  override def meta = Configuration
  object configurationUser extends StringField(this, 30)
  object configurationName extends StringField(this, 30){
    override def validations = {
      valRegex(Pattern.compile("^\\S+$"), "Configuration Name required") _ ::
      valMinLen(3, "Configuration Name cannot be less than 3 characters.") _ :: super.validations
    }
  }
  object UserID_FK extends StringRefField(this, User, 128)
  object AdminID_FK extends StringRefField(this, User, 128)
  object UserID_FKs extends StringRefListField (this, User)
  object ConfigGroupID_FK extends StringRefField(this, ConfigGroup, 128)
  object GroupName extends StringField(this, 30)
  object ErrorThreshold extends IntField(this,0)
  val ConfigAddress = new BsonRecordField(this,Address)
  val GeoPoint = new BsonRecordField(this, Location)
  def getUserConfigIDs(userID:String):List[String]={
     Configuration.findAll("UserID_FKs", userID).map(c => (c.id.toString))
  }
}
object Configuration extends Configuration with RogueMetaRecord[Configuration] {
  override def collectionName = "configurations"
}