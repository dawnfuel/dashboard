package code.model

/**
 * created by yoda on 7/10/15.
 *
 *  GRIT Systems Engineering Proprietary and Confidential
 * ©2015
 *
 *   This source code is copyrighted. Copying and/or transmitting
 *   portions or all of this work without permission is a violation of applicable law.
 *   No use of this publication may be made for resale or for any other commercial
 *   purpose whatsoever without prior permission in writing from GRIT Systems Engineering .
 *
 */

import net.liftweb.mongodb._
import net.liftweb.mongodb.record._
import net.liftweb.mongodb.record.field._
import net.liftweb.record._
import net.liftweb.record.field._

class SignalAssignment extends BsonRecord[SignalAssignment] {
  override def meta = SignalAssignment
  object sourceChannel extends IntField(this)
  object phase extends IntField(this)
  object ConfigID_FK extends StringRefField(this, User, 128)
  object SourceID_FK extends StringRefField(this, User, 128)
  object SensorID_FK extends StringRefField(this, Sensor, 128)
  object SourceType extends StringField(this,30)
  object SourceName extends StringField(this,30)
  object Direction extends StringField(this,30)
  object PowerFactor extends DecimalField(this,0.7)
  object DefaultVoltage extends IntField(this,240)
  object ScalingFactorVoltage extends DecimalField(this,1.0)    // 0.234375   = 240 volts  / 2^10
  object ScalingFactorCurrent extends DecimalField(this,0.1432)  // 0.09765625 = 100 ampere / 2^10
  object ChannelThreshold extends IntField(this,0)
}

object SignalAssignment extends SignalAssignment with BsonMetaRecord[SignalAssignment]