package code.model

import code.lib.RogueMetaRecord
import net.liftweb.mongodb.record.{MongoMetaRecord, MongoRecord}
import net.liftweb.mongodb.record.field.{BsonRecordField, StringPk, StringRefField}
import net.liftweb.record.field.{StringField}
import net.liftweb.record.field.joda.JodaTimeField

/**
  * Payment Model
  */
class Payment extends MongoRecord[Payment] with StringPk[Payment]{
  override def meta: MongoMetaRecord[Payment] = Payment
  object AccountID_FK extends StringRefField(this,Account,128)
  object Date extends JodaTimeField(this)
  object Amount extends BsonRecordField(this,Money)
  object Method extends StringField(this,32)  // Card or Bank payments
  object CardDetails extends StringField(this,128)
  object Reference extends StringField(this,128)
  object Email extends StringField(this,128)
  object Auth_code extends StringField(this,128)
  object Paystack_customerCode extends StringField(this,128)
}

object Payment extends Payment with RogueMetaRecord[Payment] {
  override def collectionName: String = "payments"
}