package code.model

import code.lib.RogueMetaRecord
import net.liftweb.mongodb.record.MongoRecord
import net.liftweb.mongodb.record.field.{StringPk, StringRefField}
import net.liftweb.record.field.{BooleanField, DoubleField, StringField, TextareaField}
import net.liftweb.mongodb.record.field._
import net.liftweb.record.field.joda.JodaTimeField

/** Invoice trait
  * @tparam T
  */
trait Invoice[T<:MongoRecord[T]] {this: T =>
  object AccountID_FK extends StringRefField(this:T,Account,128)
  object Amount extends DoubleField(this:T)
  object DueDate extends JodaTimeField(this:T)
  object reference extends StringField(this:T,128)
  object Status extends StringField(this:T,30,"unpaid")
  object PaymentID_FK extends StringRefField(this:T,Payment,128)
  object Description extends TextareaField(this :T, 160)
  object Sent extends BooleanField(this: T,false)
}

class GRIT_Invoice extends Invoice[GRIT_Invoice] with MongoRecord[GRIT_Invoice] with StringPk[GRIT_Invoice]{
  override def meta = GRIT_Invoice
  object Type extends StringField(this,"grit_invoice")
  object details extends MongoMapField[GRIT_Invoice,String](this)
  object Lines extends BsonRecordListField(this, InvoiceLines)
  object tag extends StringField(this,30)
}

object GRIT_Invoice extends GRIT_Invoice with RogueMetaRecord[GRIT_Invoice] {
  override def collectionName = "invoice"
}

class User_Invoice extends Invoice[User_Invoice] with MongoRecord[User_Invoice] with StringPk[User_Invoice] {
  override  def meta = User_Invoice
  object Type extends StringField(this,"user_invoice")
  object details extends MongoMapField[User_Invoice,String](this)
}

object User_Invoice extends User_Invoice with RogueMetaRecord[User_Invoice]{
  override def collectionName = "invoice"
}
