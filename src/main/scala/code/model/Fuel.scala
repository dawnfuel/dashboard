package code.model

import java.util.regex.Pattern

import code.lib.RogueMetaRecord
import net.liftweb.mongodb.record.MongoRecord
import net.liftweb.mongodb.record.field.{StringRefField, StringPk}
import net.liftweb.record.field.{DecimalField, StringField}
import net.liftweb.util.FieldError

class Fuel extends MongoRecord[Fuel] with StringPk[Fuel] {
    override def meta = Fuel
    object UserID_FK extends StringRefField(this, User, 128)
    object FuelType extends StringField(this, 128) {
      def fuelTypeUnique(msg: String)(value: String): List[FieldError] ={
        if (Generator.find("fuelType",value).isDefined){List(FieldError(this, msg))}
        else Nil }

      override def validations ={
        fuelTypeUnique("you already have the fuel type.")_ ::
          valMinLen(3, "fuel Name cannot be less than 3 characters.")_  ::
          valRegex(Pattern.compile("^\\w*$"), "Please Remove special Characters and spaces") _:: super.validations
      }

  }
    object FuelUnitPrice extends DecimalField(this,0.0)
}
object Fuel extends Fuel with RogueMetaRecord[Fuel] {
  override def collectionName = "Fuel"
}
