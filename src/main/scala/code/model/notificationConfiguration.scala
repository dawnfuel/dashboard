package code.model

/**
 * Created by zezzy on 10/29/15.
 **/
import code.lib.RogueMetaRecord
import net.liftweb.mongodb.record._
import net.liftweb.mongodb.record.field._
import net.liftweb.record.field._

class NotificationConfiguration extends MongoRecord[NotificationConfiguration] with StringPk[NotificationConfiguration] {
  override def meta = NotificationConfiguration
  object userID_FK extends StringRefField(this, User, 128)
  object xBelowDoD extends BsonRecordField(this,xBelowDoDElems)
  object xAboveDoD extends BsonRecordField(this, xBelowDoDElems)
  object aboveDoDWithModulus extends BsonRecordField(this, xBelowDoDElems)
  object thresholdInv extends BsonRecordField(this, xBelowDoDElems)
  object thresholdGrid extends BsonRecordField(this, xBelowDoDElems)
  object thresholdGen extends BsonRecordField(this, thresholdGenElems)
  object operatingWindow extends BsonRecordField(this,noticeMethod)
  object sourceTransitions extends BsonRecordListField(this,sourceTransition)
  //Resources
  object fuelBelowX extends BsonRecordField(this,xBelowDoDElems)
  object InverterTime extends BsonRecordField(this,xBelowDoDElems)
  object batteryBelowX extends BsonRecordField(this,xBelowDoDElems)
  object utilityBelowX extends BsonRecordField(this,xBelowDoDElems)
  //Quality
  object powerfactorBelowThreshold extends BsonRecordField(this,noticeMethod) //power factor below threshold
  object voltageBelowThreshold extends BsonRecordField(this,noticeMethod)
  object voltageXBelowThreshold extends BsonRecordField(this,xBelowDoDElems)
  object voltageAboveThreshold extends BsonRecordField(this,noticeMethod)
  object voltageXAboveThreshold extends BsonRecordField(this,xBelowDoDElems)
  object zeroPhase extends BsonRecordField(this,noticeMethod)
  //System Error
  object signalOverlap extends BsonRecordField(this,noticeMethod)
  object invOutGrtIn extends BsonRecordField(this,noticeMethod) //inverter output greater than input

  object phoneList extends StringRefListField(this,contactList)
  object emailList extends StringRefListField(this,contactList)
}
object NotificationConfiguration  extends NotificationConfiguration with RogueMetaRecord[NotificationConfiguration]{
  override def collectionName = "NotificationConfiguration"
}


//class xBelowDoDElems
class xBelowDoDElems extends BsonRecord[xBelowDoDElems]{
  override def meta = xBelowDoDElems
  object inValue extends IntField(this,5)
  object flag extends BooleanField(this,false)
  object email extends BooleanField(this,false)
  object sms extends BooleanField(this,false)
  object Value extends IntField(this,90)
}
object xBelowDoDElems extends xBelowDoDElems with BsonMetaRecord[xBelowDoDElems]


//class xAboveDoDElems
class thresholdGenElems extends BsonRecord[thresholdGenElems]{
  override def meta = thresholdGenElems
  object high extends BooleanField(this,false)
  object low extends BooleanField(this,false)
  object inValue extends IntField(this,5)
  object flag extends BooleanField(this,false)
  object email extends BooleanField(this,false)
  object sms extends BooleanField(this,false)
  object Value extends IntField(this,90)
}
object thresholdGenElems extends thresholdGenElems with BsonMetaRecord[thresholdGenElems]

// used for operating window but can be reused if the need arise
class noticeMethod extends BsonRecord[noticeMethod]{
  override def meta = noticeMethod
  object flag extends BooleanField(this,false)
  object email extends BooleanField(this,false)
  object sms extends BooleanField(this,false)
}
object noticeMethod extends noticeMethod with BsonMetaRecord[noticeMethod]

//source transition
class sourceTransition extends BsonRecord[sourceTransition]{
  override def meta = sourceTransition
  object sourceName extends StringField(this,128)
  object flag extends BooleanField(this,false)
  object email extends BooleanField(this,false)
  object sms extends BooleanField(this,false)
}
object sourceTransition extends sourceTransition with BsonMetaRecord[sourceTransition]


class contactList extends MongoRecord[contactList]{
  override def id = ""
  override def meta = contactList
  object value extends StringField(this,128)
}
object contactList extends contactList with MongoMetaRecord[contactList]