package code.model


import code.lib.RogueMetaRecord
import net.liftweb.mongodb.record.MongoRecord
import net.liftweb.mongodb.record.field.{StringPk, StringRefField, StringRefListField}
import net.liftweb.record.field.{StringField}

/**
  * Domain Model
  */

class Domains private () extends MongoRecord[Domains] with StringPk[Domains] {

  override def meta = Domains
  object name extends StringField(this, 30)
  object subdomain extends StringField(this, 30)
  object AdminID_FK extends StringRefField(this, User, 128)
  object imageURL extends StringField (this, 128)
  object pageURL extends StringField(this, 128)
  object hostName extends StringField(this, 128)
  object faviconURL extends StringField(this, 128)

}
object Domains extends Domains with RogueMetaRecord[Domains] {
  override def collectionName = "domains"
}