package code.model

import code.lib.RogueMetaRecord
import net.liftweb.mongodb.record.MongoRecord
import net.liftweb.mongodb.record.field._
import net.liftweb.record.field.{DecimalField, DoubleField, IntField, StringField}

/**
  * Products Model
  */

trait Products[T<:MongoRecord[T]] {this: T =>
  object name extends StringField(this:T,64)
  object price extends BsonRecordField(this:T,Money)

}

class GRIT_Products extends Products[GRIT_Products] with MongoRecord[GRIT_Products] with StringPk[GRIT_Products]{
  override def meta = GRIT_Products
  object Type extends StringField(this,"otherProducts")
}

object GRIT_Products extends GRIT_Products with RogueMetaRecord[GRIT_Products]{
  override def collectionName = "products"
}

class Device extends Products[Device] with MongoRecord[Device] with StringPk[Device] {
  override def meta = Device
  object NumberOfChannels extends IntField(this)
  object features extends MongoListField[Device,String](this)
  object Type extends StringField(this,"device")
}

object Device extends Device with RogueMetaRecord[Device] {
  override def collectionName = "products"
}

class Sensor extends Products[Sensor] with MongoRecord[Sensor] with StringPk[Sensor] {
  override def meta = Sensor
  object ScalingFactor extends DoubleField(this)
  object maxCurrent extends DoubleField (this)
  object size extends DoubleField (this)
  object Type extends StringField(this,"sensor")
}

object Sensor extends Sensor with RogueMetaRecord[Sensor] {
  override def collectionName = "products"
}

import net.liftweb.mongodb.record._
import net.liftweb.mongodb.record.field._

class MeterConfigs extends BsonRecord[MeterConfigs] {
  override def meta = MeterConfigs
  object IP extends StringRefField(this, Device, 128)
  object Port extends IntField(this)
}

object MeterConfigs extends MeterConfigs with BsonMetaRecord[MeterConfigs]

class CommunicationDevice extends Products[CommunicationDevice] with MongoRecord[CommunicationDevice] with StringPk[CommunicationDevice] {
  override def meta = CommunicationDevice
  object meterConfigs extends BsonRecordListField(this, MeterConfigs)
  object wifi_ssid extends StringField(this, 40)
  object wifi_password extends StringField(this, 40)
  object Type extends StringField(this,"communicationDevice")
}

object CommunicationDevice extends CommunicationDevice with RogueMetaRecord[CommunicationDevice] {
  override def collectionName = "products"
}
