package code.model

/**
 * created by yoda on 7/10/15.
 *
 *  GRIT Systems Engineering Proprietary and Confidential
 * ©2015
 *
 *   This source code is copyrighted. Copying and/or transmitting
 *   portions or all of this work without permission is a violation of applicable law.
 *   No use of this publication may be made for resale or for any other commercial
 *   purpose whatsoever without prior permission in writing from GRIT Systems Engineering .
 *
 */


import java.util.concurrent.atomic.AtomicIntegerFieldUpdater

import code.lib.RogueMetaRecord
import net.liftweb.common.Box
import net.liftweb.http.S
import net.liftweb.json.JsonAST.{JField, JObject, JString}
import net.liftweb.mongodb.record._
import net.liftweb.mongodb.record.field._
import net.liftweb.record.field._
import net.liftweb.util.{FieldIdentifier, FieldError, True}
import java.util.regex.Pattern

import scala.xml.Text


trait powerSource[T<:MongoRecord[T]] {this: T =>
  val ProbeID_FK = new StringRefField(this:T, Probe, 128){
    override def validations = valRegex(Pattern.compile("^\\S+$"), "probe required")_ :: super.validations
  }

  val ConfigID_FK = new StringRefField(this:T, Configuration, 128){
    override def validations =  valMinLen(3, "config required")_ :: super.validations
      //valRegex(Pattern.compile("^\\S+$"), "config required")_ :: super.validations
  }

  object Type extends StringField(this:T,30)
  object NumberOfPhases extends IntField(this:T,3)
  object MaxChannels extends IntField(this:T,3)
  object State extends IntField(this:T,0)
  object SignalThreshold extends IntField(this:T, 0) //not added to UI elements yet
}

class Generator extends powerSource[Generator] with MongoRecord[Generator] with StringPk[Generator] {
  override def meta = Generator
  object operatingWindow extends BsonRecordField(this, operatingWindowElem)
  object sourceName extends StringField(this, 30) {
    override def validations = {
        valMinLen(3, "sourceName cannot be less than 3 characters.")_  ::
        valRegex(Pattern.compile("^\\w*$"), "Please Remove special Characters and spaces") _:: super.validations
    }
  }
  //Gen specific
  val FuelStore = new IntField(this, 150) {
    override def validations =
      isFuelInteger("enter non zero value for fuel tank capacity") _ ::
        super.validations
  }
  val Rating = new IntField(this, 20) {
    override def validations =
      isRatingInteger("enter non zero value generator rating") _ ::
        super.validations
  }

  private def isRatingInteger(errorMsg: => String)(value: Int): List[FieldError] =
    value match {
      case 0 => FieldError(this.Rating, errorMsg) :: Nil
      case default => Nil
    }


  val GeneratorMap = new BsonRecordField(this, generatorMap) {
    override def validations =
      isGenMapValid("fuel consumption must increase with load") _ ::
        super.validations
  }

  private def isFuelInteger(errorMsg: => String)(value: Int): List[FieldError] =
    value match {
      case 0 => FieldError(this.FuelStore, errorMsg) :: Nil
      case default => Nil
    }

  def manOf[T: Manifest](t: T): Manifest[T] = manifest[T]

  private def isGenMapValid(errorMsg: => String)(map: generatorMap): List[FieldError] = {
    // check that map is ascending
    println("MAPVALIDATION " + map.QuarterLoad)
    if (true) {
      //(map.QuarterLoad < map.HalfLoad)){// && (map.HalfLoad < map.ThreeQuarterLoad) && (map.ThreeQuarterLoad < map.FullLoad)){
      Nil
    } else {
      FieldError(this.GeneratorMap, errorMsg) :: Nil
    }
  }

  val FuelID_FK = new StringRefField(this, Fuel, 128){
    override def validations = valRegex(Pattern.compile("^\\S+$"), "fuel required")_ :: super.validations
  }
  object FuelType extends StringField(this, 128)
  object FuelUnitPrice extends DecimalField(this,0.0)
  object Synchronized extends BooleanField(this,false)

}

object Generator extends Generator with RogueMetaRecord[Generator] {
  override def collectionName = "powersources"
}


//class GeneratorMap
class generatorMap extends BsonRecord[generatorMap] {
  override def meta = generatorMap
  //comment this out and uncomment the below to activate floats.
  // Note: Have to change extractors to reflect floats in kafkaSparkLib.scala
  object QuarterLoad extends IntField(this,10)
  object HalfLoad extends IntField(this,15)
  object ThreeQuarterLoad extends IntField(this,17)
  object FullLoad extends IntField(this,20)
}
object generatorMap extends generatorMap with BsonMetaRecord[generatorMap]



/////////// Inverter ////////////////////////////////////////////
class Inverter extends powerSource[Inverter] with MongoRecord[Inverter] with StringPk[Inverter] {
  override def meta = Inverter
  object operatingWindow extends BsonRecordField(this, operatingWindowElem)
  object sourceName extends StringField(this, 30) {
//    def sourceNameUnique(msg: String)(value: String): List[FieldError] ={
//      if (Inverter.find("sourceName",value).isDefined){List(FieldError(this, msg))}
//      else Nil }

    override def validations ={
//      sourceNameUnique("source name already taken.")_ ::
        valMinLen(3, "sourceName cannot be less than 3 characters.")_ ::
        valRegex(Pattern.compile("^\\w*$"), "Please Remove special Characters and spaces") _:: super.validations
    }
  }
  object Efficiency extends IntField(this,80)
  object batteryLife extends BsonRecordListField(this, BatteryLife)
  object eThreshold extends DoubleField(this)
  val chargeCurrent = new IntField(this, 20) {
    override def validations =
      ChargeCurrent("enter non zero value Depth Of Discharge") _ ::
        super.validations
  }

  val depthOfDischarge = new IntField(this, 50) {
    override def validations =
      DoD("enter non zero value Depth Of Discharge") _ ::
        super.validations
  }

  val batteryCapacity = new IntField(this, 200) {
    override def validations =
      BatteryCapacity("enter non zero value BatteryCapacity") _ ::
        super.validations
  }

  val batteryVoltage = new IntField(this, 12) {
    override def validations =
      BatteryVoltage("enter non zero value BatteryCapacity") _ ::
        super.validations
  }

  val batteryBankVoltage = new IntField(this, 24) {
    override def validations =
      BatteryBankVoltage("enter non zero value Battery bank voltage") _ ::
        super.validations
  }

  val batteryCount = new IntField(this, 4) {
    override def validations =
      BatteryCount("enter non zero value Battery Count") _ ::
        super.validations
  }


  val Rating = new IntField(this, 5) {
    override def validations =
      isRatingInteger("enter non zero value generator rating") _ ::
        super.validations
  }

  val batteryChemistry = new StringField(this, 30) {
    override def validations =
      valMinLen(1, "Battery chemistry required") _ :: super.validations
  }

  object batteryEndOfLife extends IntField(this,0)

  val cost = new IntField(this, 60000) {
    override def validations =
      isCostInteger("enter non zero value BatteryCapacity") _ ::
        super.validations
  }

  private def ChargeCurrent(errorMsg: => String)(value: Int): List[FieldError] =
    value match {
      case 0 => FieldError(this.chargeCurrent, errorMsg) :: Nil
      case default => Nil
    }

  private def DoD(errorMsg: => String)(value: Int): List[FieldError] =
    value match {
      case 0 => FieldError(this.depthOfDischarge, errorMsg) :: Nil
      case default => Nil
    }

  private def BatteryCapacity(errorMsg: => String)(value: Int): List[FieldError] =
    value match {
      case 0 => FieldError(this.batteryCapacity, errorMsg) :: Nil
      case default => Nil
    }

  private def BatteryVoltage(errorMsg: => String)(value: Int): List[FieldError] =
    value match {
      case 0 => FieldError(this.batteryVoltage, errorMsg) :: Nil
      case default => Nil
    }

  private def BatteryBankVoltage(errorMsg: => String)(value: Int): List[FieldError] =
    value match {
      case 0 => FieldError(this.batteryBankVoltage, errorMsg) :: Nil
      case default => Nil
    }

  private def BatteryCount(errorMsg: => String)(value: Int): List[FieldError] =
    value match {
      case 0 => FieldError(this.batteryCount, errorMsg) :: Nil
      case default => Nil
    }

  private def isRatingInteger(errorMsg: => String)(value: Int): List[FieldError] =
    value match {
      case 0 => FieldError(this.Rating, errorMsg) :: Nil
      case default => Nil
    }


  private def isCostInteger(errorMsg: => String)(value: Int): List[FieldError] =
    value match {
      case 0 => FieldError(this.cost, errorMsg) :: Nil
      case default => Nil
    }

  }
object Inverter extends Inverter with RogueMetaRecord[Inverter] {
  override def collectionName = "powersources"
}


//Inverter BatteryLife
class BatteryLife extends BsonRecord[BatteryLife] {
  override def meta = BatteryLife
  object DoD extends IntField(this,100)
  object numberOfCycles extends IntField(this,200)
  object bCapacity extends IntField(this,50)
}
object BatteryLife extends BatteryLife with BsonMetaRecord[BatteryLife]




///////////////////// Grid ///////////////////////////////
class Grid extends powerSource[Grid] with MongoRecord[Grid] with StringPk[Grid] {
  object sourceName extends StringField(this, 30) {
//    def sourceNameUnique(msg: String)(value: String): List[FieldError] ={
//      if (Grid.find("sourceName",value).isDefined){List(FieldError(this, msg))}
//      else Nil }

    override def validations ={
//      sourceNameUnique("source name already taken.")_ ::
        valMinLen(3, "sourceName cannot be less than 3 characters.")_ ::
        valRegex(Pattern.compile("^\\w*$"), "Please Remove special Characters and spaces") _:: super.validations
    }
  }
  override def meta = Grid
  val UtilityMap = new BsonRecordField(this, utilityMap)
  object Rating extends IntField(this)

}
object Grid extends Grid with RogueMetaRecord[Grid] {
  override def collectionName = "powersources"
}

//class GeneratorMap
class utilityMap extends BsonRecord[utilityMap] {
  override def meta = utilityMap
    //comment this out and uncomment the below to activate floats.
    // Note: Have to change extractors to reflect floats in kafkaSparkLib.scala
  object night extends IntField(this,25)
  object morning extends IntField(this,25)
  object afternoon extends IntField(this,25)
  object evening extends IntField(this,25) // 25.79 Naira at last count
//  object night extends DecimalField(this,15.00)
//  object morning extends DecimalField(this,15.00)
//  object afternoon extends DecimalField(this,15.00)
//  object evening extends DecimalField(this,15.00)
}
object utilityMap extends utilityMap with BsonMetaRecord[utilityMap]


//general source query functions
object powerSourceFunctions{
  /**@return returns true if source name already exists in config
    * @param c_id id of probe to which source is assigned
    * @param name the name of source
    * checks uniqueness of sourceName to configuration*/
  def validateSourceName(c_id:String,name:String):Boolean = {
    println("==--==--"+c_id+" : "+name)
    var err = false
    Configuration.find("_id",c_id).map(c => {
      if (Generator.findAll("ConfigID_FK", c.id.toString).filter(_.sourceName._1.toString == name).nonEmpty) err = true
    })
    if(err) {
      S.error(List(FieldError(new FieldIdentifier  {override def uniqueFieldId = Box.legacyNullTest("sourceName_id")}, Text("source name already used in this configuration"))))
    }
    err
  }
}

// used for operating window but can be reused if the need arise
class operatingWindowElem extends BsonRecord[operatingWindowElem]{
  override def meta = operatingWindowElem
  object set extends BooleanField(this,false)
  object from extends StringField(this,4)
  object to extends StringField(this,4)
}
object operatingWindowElem extends operatingWindowElem with BsonMetaRecord[operatingWindowElem]