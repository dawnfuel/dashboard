package code.model

/**
  * Created by yoda on 5/17/16.
  */

import java.util.Properties
import java.util.regex.Pattern

import code.lib.RogueMetaRecord
import net.liftweb.mongodb.record._
import net.liftweb.mongodb.record.field._
import net.liftweb.record.field._
import net.liftweb.util.FieldError
import org.apache.kafka.clients.producer._
import org.joda.time.DateTime
/*
  * Created by yoda on 4/26/16.
  */
class ConfigGroup private() extends MongoRecord[ConfigGroup] with StringPk[ConfigGroup] {
  override def meta = ConfigGroup
  object groupName extends StringField(this, 30){
    override def validations = {
      valMinLen(3, "groupName cannot be less than 3 characters.")_  ::
        valRegex(Pattern.compile("^\\w*$"), "Please Remove special Characters and spaces") _:: super.validations
    }
  }
  object margin extends IntField(this,30){
    //    override def validations = {
    //      valMinLen(1, "margin cannot be empty")_  :: super.validations
    //    }
  }
  object adminID_FK extends StringRefField(this, User, 128){
    override def validations = {
      valMinLen(1, "admin cannot be empty")_  :: super.validations
    }
  }

  object SourceConfigID_FK extends StringRefField(this, User, 128){
    override def validations = {
      valMinLen(1, "SourceConfigID_FK cannot be empty")_  :: super.validations
    }
  }
  def acceptPayment() ={
    //
  }

  //A geoShape is a sequence of Location which is a a tuple object of Longitude and latitude
  //makes sence to use a Seq of tuples.
  /**Sequence of tuple (long,lat) of coordinates that define GeoShape*/
  val GeoShape = Seq[(Double,Double)]()


}

object ConfigGroup extends ConfigGroup with  RogueMetaRecord[ConfigGroup]  {
  override def collectionName = "configgroup"
}