package code.model

import java.util.Properties
import java.util.regex.Pattern

import code.lib.RogueMetaRecord
import net.liftweb.mongodb.record._
import net.liftweb.mongodb.record.field._
import net.liftweb.record.field._
import net.liftweb.util.FieldError
import org.apache.kafka.clients.producer._
import org.joda.time.DateTime
/*
  * Created by yoda on 4/26/16.
  */
class UserGroup private() extends MongoRecord[UserGroup] with StringPk[UserGroup] {
  override def meta = UserGroup
  object groupName extends StringField(this, 30){
    override def validations = {
      valMinLen(3, "groupName cannot be less than 3 characters.")_  ::
      valRegex(Pattern.compile("^\\w*$"), "Please Remove special Characters and spaces") _:: super.validations
    }
  }
  object margin extends IntField(this,30){
  }

  object adminID_FK extends StringRefField(this, User, 128){
    override def validations = {
      valMinLen(1, "admin cannot be empty")_  :: super.validations
    }
  }
  object members extends StringRefListField(this, User){
  }

  object whiteLabel extends BooleanField(this,false)

}

object UserGroup extends UserGroup with  RogueMetaRecord[UserGroup]  {
  override def collectionName = "usergroup"
}