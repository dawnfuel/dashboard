package code.model

/**
  * InvoiceLines  Model
  */

import net.liftweb.mongodb.record._
import net.liftweb.mongodb.record.field._
import net.liftweb.record.field._

class InvoiceLines extends BsonRecord[InvoiceLines] {
  override def meta = InvoiceLines
  object ProductID_FK extends StringRefField(this, Device, 128)
  object Quantity extends IntField(this)
  object UnitPrice extends DecimalField(this,0.0)
}

object InvoiceLines extends InvoiceLines with BsonMetaRecord[InvoiceLines]

