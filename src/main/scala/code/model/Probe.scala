package code.model

/**
 * created by yoda on 7/10/15.
 *
 *  GRIT Systems Engineering Proprietary and Confidential
 * ©2015
 *
 *   This source code is copyrighted. Copying and/or transmitting
 *   portions or all of this work without permission is a violation of applicable law.
 *   No use of this publication may be made for resale or for any other commercial
 *   purpose whatsoever without prior permission in writing from GRIT Systems Engineering .
 *
 */

import java.util.Properties
import java.util.regex.Pattern

import code.lib.RogueMetaRecord
import code.model.Generator.sourceName._
import net.liftweb.mongodb.record._
import net.liftweb.mongodb.record.field._
import net.liftweb.record.field._
import net.liftweb.util.FieldError
import org.apache.kafka.clients.producer._
import org.bson.types.ObjectId
import org.joda.time.DateTime



class Probe private () extends MongoRecord[Probe] with StringPk[Probe] {
  override def meta = Probe
  object probeName extends StringField(this, 30)
  object ConfigID_FK extends StringRefField(this, Configuration, 128)
  object DeviceID_FK extends StringRefField(this, Device, 128)
  object DeviceType extends StringField(this, 40)
  object NumberOfChannels extends IntField(this,8)
  object SignalThreshold extends IntField(this)
  object ErrorThreshold extends IntField(this)
  object ScalingFactorCurrent extends DecimalField(this,0.1432)
  object ScalingFactorVoltage extends DecimalField(this,1)
  object Channels extends BsonRecordListField(this, SignalAssignment)
  object wifi_ssid extends StringField(this, 40)
  object wifi_password extends StringField(this, 40)
  object distributionLevel extends StringField(this, 40)
  object address extends BsonRecordField(this,Address)
  object dataExpiry extends DateField(this,new DateTime().plusMonths(1).toDate)
  def manOf[T: Manifest](t: T): Manifest[T] = manifest[T]
  def getProbes(confIDs:List[String]):List[Probe]={
    var p:List[Probe] = List()
    for(c <- confIDs){
      p = p ::: Probe.findAll("ConfigID_FK",c)
    }
    p
  }

  /** Find Channels within a given probe that have been assigned to a given Configuration
    *
    * @param ProbeID Probe PK identifier
    * @param ConfigID_FK Configuration PK identifier
    * @return a Seq of ProbeID_ChannelNumber and ProbeName_ChannelNumber
    *         tuples for populating a select element.
    */
  def findChannelsForProbe( ProbeID : String, ConfigID_FK : String) = {
    val channel_pairs = Probe.findAll("_id",ProbeID).flatMap { probe =>
      probe.Channels.get.zipWithIndex.map {  case (channel , i) =>
        if (channel.SourceID_FK.toString.isEmpty && channel.ConfigID_FK.toString == ConfigID_FK) {
          (probe.id.toString+"_"+i, probe.probeName.toString+"_"+i)
        }
      }.filter(_ !=(()))
    }
    val channels_distinct = channel_pairs.distinct.asInstanceOf[Seq[(String,String)]]
    channels_distinct
  }

  /** Find Channels assigned to a Configuration that have not been assigned to any sources
    *
    * @param configID_FKs a list of Configuration PK identifiers
    * @return a Seq of ProbeID_ChannelNumber and ProbeName_ChannelNumber
    *         tuples for populating a select element.
    */
  def findChannelsWithNoSources( configID_FKs : List[String]) = {
    val channel_pairs = Probe.findAll.flatMap { probe =>
      probe.Channels.get.zipWithIndex.map {  case (channel , i) =>
        if (configID_FKs.contains(channel.ConfigID_FK.toString) && channel.SourceID_FK.toString.isEmpty) {
          (probe.id.toString+"_"+i, probe.probeName.toString+"_"+i)
        }
      }.filter(_ !=(()))
    }
    val channels_distinct = channel_pairs.distinct.asInstanceOf[Seq[(String,String)]]
    channels_distinct
  }

  def findChannels( configID_FKs : List[String]) = {
    val probes = Probe.findAll.flatMap { probe =>
      probe.Channels.get.zipWithIndex.map {  case (channel , i) =>
        if (configID_FKs.contains(channel.ConfigID_FK.toString)) {
          (probe.id.toString+"_"+i, probe.probeName.toString+"_"+i)
        }
      }.filter(_ !=(()))
    }
    val probes_distinct = probes.distinct.asInstanceOf[List[(String,String)]]
    probes_distinct
  }

  def findChannelsForSource( sourceID_FKs : List[String], configID_FKs : List[String], reset : Boolean ) = {
    val probes = Probe.findAll.flatMap { probe =>
      probe.Channels.get.zipWithIndex.map {  case (channel , i) =>
        if ((sourceID_FKs.contains(channel.SourceID_FK.toString) || channel.SourceID_FK.toString.isEmpty ) && (configID_FKs.contains(channel.ConfigID_FK.toString))) {
          if( reset == true ){
            channel.SourceID_FK("")
            channel.sourceChannel(0)
            channel.SourceType("")
            channel.SourceName("")
            channel.phase(0)
            probe.save()
          }
          (probe.id.toString+"_"+i, probe.probeName.toString+"_"+i)
        }
      }.filter(_ !=(()))
    }
    var counter = -1
    val probeswithphase = Probe.findAll.flatMap { probe =>
      probe.Channels.get.zipWithIndex.map {  case (channel , i) =>
        if (sourceID_FKs.contains(channel.SourceID_FK.toString)) {
          counter += 1
          //println("CHANNEL SCAN : "+probe.id.toString+"_"+i, channel.Direction.value+" DIR "+testvalue+" -> "+counter)
          (probe.id.toString+"_"+i, counter)
        }
      }.filter(_ !=(()))
    }
    val probe_assigned = probeswithphase.distinct.asInstanceOf[List[(String,Int)]].groupBy(_._2)
    val probes_distinct = probes.distinct.asInstanceOf[Seq[(String,String)]]
    (probes_distinct, probe_assigned)
  }

  def findProbesWithAvailableChannels = {
    val probes = Probe.findAll.flatMap { probe =>
      probe.Channels.get.map { channel =>
        if (channel.ConfigID_FK.toString.isEmpty) {
          (probe.id.toString, probe.probeName.toString)
        }
      }.filter(_ !=(()))
    }
    probes.distinct.asInstanceOf[Seq[(String,String)]]
  }

  def findUserProbesWithAvailableChannels(userId : String) = {
    val probes = User.findAll("_id",new ObjectId(userId)).flatMap(u =>
      u.ProbeID_FKs.value.flatMap(p => Probe.findAll("_id",p).flatMap {
        probe =>
          probe.Channels.get.map { channel =>
            if (channel.ConfigID_FK.toString.isEmpty) {
              (probe.id.toString, probe.probeName.toString)
            }
          }.filter(_ !=(()))
      })
  )
    probes.distinct.asInstanceOf[Seq[(String,String)]]
  }
}
object Probe extends Probe with  RogueMetaRecord[Probe]  {
  override def collectionName = "probes"
}