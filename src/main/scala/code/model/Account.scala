package code.model

import code.lib.RogueMetaRecord
import net.liftweb.http.S
import net.liftweb.mongodb.record.MongoRecord
import net.liftweb.mongodb.record.field.{StringPk, StringRefField}
import net.liftweb.record.field.joda.JodaTimeField
import net.liftweb.record.field.{DecimalField, IntField, StringField}
import net.liftweb.util.FieldError

import scala.xml.Text

/**
  * User Account Model
  */
class Account extends MongoRecord[Account] with StringPk[Account]{
  override def meta = Account
  object Name extends StringField(this,32){
    override def displayName = "Username"
    override def setFilter = trim _ :: super.setFilter

    private def valUnique(msg: => String)(value: String): List[FieldError] = {
      if (value.length > 0)
        meta.findAll(name, value).filterNot(_.id.get == owner.id.get).map(u =>
          FieldError(this, Text(msg))
        )
      else
        Nil
    }
    override def validations =
      valUnique(S ? "Selected name already exists") _ ::
        valMinLen(3, S ? "Name must not be less than 3 characters") _ ::
        valMaxLen(32, S ? "Name is above 32 characters") _ ::
        super.validations
  }
  object UserID_FK extends  StringRefField(this,User,128)
  object PaymentPlan extends StringField(this,50)
  object MonthlyInstallment extends DecimalField(this,0.0)
  object TotalInstallment extends DecimalField(this,0.0)
  object BillingDate extends IntField(this)
  object CreationDate extends JodaTimeField(this)
}

object Account extends Account with RogueMetaRecord[Account] {
  override def collectionName = "account"
}
