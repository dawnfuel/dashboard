package code.rest

import code.model.{Configuration, Generator, PowerConsumerGroup, User}
import code.snippet.homepageops
import net.liftweb.common.Full
import net.liftweb.http.LiftRules
import net.liftweb.http.rest.RestHelper
import net.liftweb.json.JsonDSL._

object DBQueryAPI extends RestHelper{

  def init() : Unit = {
    LiftRules.statelessDispatch.append(DBQueryAPI)
  }

  case class userRequest (email : String, passwd: String)
  case class Config (id : String,name : String)

  serve {
    // Rest API to check User
    // returns user registration status and configIDs
    case "loginAPI" :: Nil  JsonPost  ((jsonData, req)) =>  {
      val (email, passwd) = (jsonData.extract[userRequest].email, jsonData.extract[userRequest].passwd)
      val success = checkUser(email, passwd)
      success match {
        case true => {
          val currentUserID = User.find("email", email).headOption.getOrElse(User).id.toString()
          val configs : List[Config]  = Configuration.findAll("UserID_FKs", currentUserID).map(p => Config(p.id.toString,p.configurationName.toString))
          ("success" -> success.toString) ~
            ("userIDFKs" -> currentUserID)~
              ("configs" ->
              configs.map{
                c => ("name" ->c.name)~("id"->c.id)~("accountType"->accountType(consumerOnly(c.id,currentUserID)))~
                  ("consumerGroups"->consumerGroups(c.id))
              }
            )
        }
        case _ => {
          ("success" -> success.toString) ~ ("configs" -> "")~("accountType"->"")
        }
      }
    }
  }

  //check if user is registered
  def checkUser(mail : String, password :String) = {
      val email = mail.toLowerCase.trim
      if ( email.length > 0 && password.length > 0) {
        User.findByEmail(email) match {
          case Full(user) if user.password.isMatch(password) =>
            true
          case _ =>
            false
        }
      }
  }

  def consumerOnly(configId: String,currentUserID: String) : Boolean = {
    val group = Configuration.findAll("_id",configId).headOption.getOrElse(Configuration).ConfigGroupID_FK.value
    val sources = List("inverter", "generator", "grid").toSet
    val userPowerSources = getSources(Configuration.findAll("UserID_FKs", currentUserID).map(p => p.id.toString)).flatMap(p => f(p))
    userPowerSources.toSet.intersect(sources).isEmpty  && group.isEmpty
  }

  def f(v:Generator) = List(v.Type.toString)
  def getSources(configIDs:List[String]):List[Generator]={
    var g:List[Generator] = List()
    for(p <- configIDs){
      g = g ::: Generator.findAll("ConfigID_FK",p)
    }
    g
  }

  def accountType (consumerOnly_ : Boolean): String = {
    if  (consumerOnly_) "consumerOnly" else "generic"
  }

  /**
    *
    *
    * @param configId
    * @return
    */
  def consumerGroups (configId : String) = {
    PowerConsumerGroup.findAll(("ConfigID_FK" -> configId)~("Type" -> "Estate")).map(p => p.id.toString -> p.Name.toString)
  }

}
