package code.rest

import net.liftweb.common.Full
import net.liftweb.http.LiftRules
import net.liftweb.http.rest.RestHelper
import net.liftweb.util.Props
import net.liftweb.json.JsonDSL._
/**
  * API for Mobile APP Version
  */
object VersionCheck extends RestHelper {

  def init() : Unit = {
    LiftRules.statelessDispatch.append(VersionCheck)
  }

  def toInt(s: String): Int = {
    util.Try(s.toInt).getOrElse(0)
  }



  val (version_number,major_revision_number,minor_revision_number)= sys.env.get("APP_VERSION") match {
    case Some(version) => {
      val array = version.split("\\.")
      (toInt(array(0)), toInt(array(1)), toInt(array(2)))

    }
    case None => {
       Props.get("app.version.recent") match {
         case Full(version) => {
            val array = version.toString.split("\\.")
           (toInt(array(0)), toInt(array(1)), toInt(array(2)))
         }
         case _ => {
           (1, 1, 2)
         }
       }
    }
  }

  val download_link = sys.env.get("DOWNLOAD_LINK") match {
    case Some(link) => {
      link
    }
    case None => {
      Props.get("app.downloadLink") match {
        case Full(link) => {
          link.toString
        }
        case _ => {
          ""
        }
      }
    }
  }

  serve {
    // Rest API to return current version of mobile app
    case "versionCheck" :: Nil  JsonGet req =>  {
      ("version_number" -> version_number) ~ ("major_revision_number" -> major_revision_number) ~("minor_revision_number" -> minor_revision_number) ~ ("download_link" -> download_link)
    }
  }


}
