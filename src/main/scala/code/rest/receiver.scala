package code.rest

import java.text.SimpleDateFormat
import java.util.{Calendar, Date}

import code.model._
import net.liftweb.http.rest.RestHelper
import net.liftweb.http.LiftRules
import net.liftweb.json
import net.liftweb.json.JsonAST._
import net.liftweb.json.JsonDSL._
import net.liftweb.util.Helpers._
import org.elasticsearch.action.search.{SearchResponse, SearchType}
import org.elasticsearch.client.Client
import org.elasticsearch.client.transport.TransportClient
import org.elasticsearch.common.transport.InetSocketTransportAddress
import org.elasticsearch.index.query.QueryBuilders
import org.elasticsearch.search.SearchHit
import org.elasticsearch.search.aggregations.AggregationBuilders
import org.elasticsearch.search.aggregations.bucket.histogram.{DateHistogramInterval, Histogram}
import org.elasticsearch.search.sort._
import net.liftweb.common.Full
import net.liftweb.http._
import org.elasticsearch.common.unit.TimeValue
import org.elasticsearch.search.aggregations.bucket.terms.StringTerms
import org.elasticsearch.search.aggregations.metrics.stats.InternalStats

import collection.JavaConversions._


object Realtime extends RestHelper {
  def init() : Unit = {
    LiftRules.statelessDispatch.append(Realtime)
  }

  var MessageCount = scala.collection.mutable.Map[String, Int]()

  def greet(name: String) : JValue =
    "greeting" -> ("HELLO "+name.toUpperCase)
  serve {
    case "shout" :: Nil JsonPost json->request =>
      val V = json.extract[Power_MSG].ConfigID_FK
      val conf = Configuration.findAll("_id", V)
      conf.head.UserID_FKs.value.foreach { currentUserID =>

        //Count messages for each User
        if (MessageCount.contains(currentUserID.toString())) {
          MessageCount(currentUserID.toString()) += 1
          if (MessageCount(currentUserID.toString())==20) {
            val js = net.liftweb.json.parse("{\"Update\" : true }")
            NamedCometListener.getDispatchersFor(Full(currentUserID.toString)).foreach(_.foreach(_ ! Fun(js) ))
            MessageCount(currentUserID.toString())= 0
          }
        } else {
          MessageCount(currentUserID.toString()) = 1
        }
        println (MessageCount)
        NamedCometListener.getDispatchersFor(Full(currentUserID.toString)).foreach(_.foreach(_ ! json))
      }

      for { JString(name) <- (json \\ "name").toOpt }
        yield greet(name)
  }
  // this case class must be identical to the corresponding case class in the spark data engine
  case class Power_MSG(
                    batterycapacity : Double,         // 1
                    SourceID_FK: String,              // 2
                    ConfigID_FK: String,              // 3
                    Time : String,                    // 4
                    SourceType : String,              // 5
                    SourceName : String,              // 6
                    Voltage1 : Double,                // 7
                    Voltage2 : Double,                // 8
                    Voltage3 : Double,                // 9
                    Current1 : Double,                // 10
                    Current2 : Double,                // 11
                    Current3 : Double,                // 12
                    PowerFactor1 : Double,            // 13
                    PowerFactor2 : Double,            // 14
                    PowerFactor3 : Double,            // 15
                    Energy_since_last : Double,       // 16
                    Power_real_total : Double,        // 17
                    Power_apparent_total : Double,    // 18
                    Power_reactive_total : Double,    // 19
                    Fuel_hour_rate : Double,          // 20
                    Fuel_since_last : Double,         // 21
                    Cost_since_last : Double          // 22
                  )

  case class Fun(json: JValue)
}

object ESQueryAPI extends RestHelper {
  def cleantz( time : String ) : String = {
    var sign_builder= new StringBuilder ++= time
    var clean_sign = ""
    if (sign_builder.charAt(23).toString == "-"){
      clean_sign= sign_builder.replace(23,24,"-").toString()
    }else{
      clean_sign = sign_builder.replace(23,24,"+").toString()
    }
    var time_builder= new StringBuilder ++= clean_sign
    if (time_builder.charAt(26).toString == ":"){
      val cleanz = time_builder.deleteCharAt(26)
      cleanz.toString()
    }else{
      time_builder.toString()
    }
  }

  def getMaxDay (): Date = {
    val  dateFormat = new SimpleDateFormat("MM/dd/yyyy");
    var date = new Date()
    //val convertedDate = dateFormat.parse(date)
    val d = Calendar.getInstance();
    d.setTime(date);
    d.set(Calendar.HOUR_OF_DAY, 23);
    d.set(Calendar.MINUTE, 59);
    d.set(Calendar.SECOND, 59);
    d.set(Calendar.MILLISECOND, 0);
    d.set(Calendar.DAY_OF_MONTH, d.getActualMaximum(Calendar.DAY_OF_MONTH));
    return ((d.getTime()))
  }


  def getMinDay (): Date = {
    val  dateFormat = new SimpleDateFormat("MM/dd/yyyy");
    var date = new Date()
    //val convertedDate = dateFormat.parse(date)
    val  c = Calendar.getInstance();
    c.setTime(date);
    c.set(Calendar.HOUR_OF_DAY, 0);
    c.set(Calendar.MINUTE, 0);
    c.set(Calendar.SECOND, 0);
    c.set(Calendar.MILLISECOND, 0);
    c.set(Calendar.DAY_OF_MONTH, c.getActualMinimum(Calendar.DAY_OF_MONTH));
    return ((c.getTime()))
  }

  def manOf[T: Manifest](t: T): Manifest[T] = manifest[T]
  val ISO8601Format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZ")
  //  ISO8601Format.setTimeZone(TimeZone.getTimeZone("UTC"));
  ISO8601Format.setLenient(false)

  def init() : Unit = {
    LiftRules.statelessDispatch.append(ESQueryAPI)
  }
  def getESparams() : (String,Int,Int) = {
    val eshost = try {
      sys.env("ESHOST").toString
    } catch {
      case e: Exception => "localhost"
    }
    val esjavaport = try {
      sys.env("ESJAVAPORT").toInt
    } catch {
      case e: Exception => 9300
    }
    val eshttpport = try {
      sys.env("ESHTTPPORT").toInt
    } catch {
      case e: Exception => 9200
    }
    (eshost,esjavaport,eshttpport)
  }

  //Find ES index to search
  def index(configId : String) : String = {
    val index = Configuration.find("_id", configId) match {
      case Full(c) => {
        //Check if the config is in a configGroup and if the configId is the source
        //of the configGroup
        if (c.ConfigGroupID_FK.value.nonEmpty) {
          val configGroup = ConfigGroup.findAll("_id",c.ConfigGroupID_FK.value).headOption.getOrElse(ConfigGroup)
          if (configGroup.SourceConfigID_FK.value != c.id.value) {
            "consumer"
          } else {
            "grit"
          }
        } else {
          "grit"
        }
      }
      case _ => {
        "grit"
      }
    }
    index
  }

  def exportAggregations(confId: String, startIso: String, stopIso: String, interval: DateHistogramInterval) = {
    val startTime = ISO8601Format.parse(cleantz(startIso))
    val queryStartTime = ISO8601Format.format(startTime)
    val stopTime = ISO8601Format.parse(cleantz(stopIso))
    val queryStopTime = ISO8601Format.format(stopTime)
    val timezone = "+" + stopIso.takeRight(5)

    println("**********************")
    println(startTime)
    println(stopTime)
    println("**********************")

    val (eshost, esjavaport, eshttpport) = getESparams()
    val client: Client = TransportClient.builder().build().addTransportAddress(new InetSocketTransportAddress(java.net.InetAddress.getByName(eshost), esjavaport))
    val matchQuery = QueryBuilders.matchQuery("ConfigID_FK", confId)
    val filter = QueryBuilders.rangeQuery("Time").gte(queryStartTime.toString).lte(queryStopTime.toString)
    val query = QueryBuilders.filteredQuery(matchQuery, filter)
    val agg = AggregationBuilders.dateHistogram("Export")
      .field("Time")
      .interval(interval)
      .timeZone(timezone)
      .subAggregation(AggregationBuilders.terms("SourceType").field("SourceType")
        .subAggregation(AggregationBuilders.terms("SourceName").field("SourceName")
          .subAggregation(AggregationBuilders.stats("costStats").field("Cost_since_last"))
          .subAggregation(AggregationBuilders.stats("energyStats").field("Energy_since_last"))
          .subAggregation(AggregationBuilders.stats("timeStats").field("Cost_since_last"))
          .subAggregation(AggregationBuilders.stats("powerStats").field("Power_real_total"))
        )
      )

    val searchResponse: SearchResponse = client
      .prepareSearch(index(confId))
      .setTypes("docs")
      .setSearchType(SearchType.DFS_QUERY_THEN_FETCH)
      .setQuery(query)
      .addAggregation(agg)
      .addSort("Time", SortOrder.DESC)
      .execute()
      .actionGet()

    client.close()
    json.parse(searchResponse.toString)
  }

  val timing_int = 5

  serve {
    case "recent" :: confId :: stop_iso :: window :: Nil JsonGet req => {
      println("RECENT ARGS WINDOW : "+window)
      println("RECENT ARGS STOP   : "+stop_iso)
      println("RECENT ARGS STOP   : "+cleantz(stop_iso))
      // println("RECENT TIMEZONE   : "+timezone)
      val (eshost,esjavaport,eshttpport) = getESparams()
      val client: Client =TransportClient.builder().build().addTransportAddress(new InetSocketTransportAddress(java.net.InetAddress.getByName(eshost), esjavaport))
      val stop_time = ISO8601Format.parse(cleantz(stop_iso))
      // get start and stop time
      val matchquery = QueryBuilders.matchQuery("ConfigID_FK", confId)
      val start_time = new Date(stop_time.getTime() - (window.toInt*1000))
      println("RECENT COMPUTE START : "+start_time)
      println("RECENT COMPUTE STOP  : "+stop_time)
      val query_start = ISO8601Format.format(start_time)
      val query_stop = ISO8601Format.format(stop_time)
      println("RECENT QUERY START : "+query_start)
      println("RECENT QUERY STOP  : "+query_stop)
      val probesPerConfig = 1
      // construct the query
      val filter1 = QueryBuilders.rangeQuery("Time").gte(query_start.toString).lte(query_stop.toString)
      val query = QueryBuilders.filteredQuery(matchquery,filter1)
      val response_data: SearchResponse = client
        .prepareSearch(index(confId))
        .setTypes("docs")
        .setQuery(query)
        .setFrom(0)
        .setSize(probesPerConfig*window.toInt/timing_int)
        .addSort("Time", SortOrder.ASC)
        .execute()
        .actionGet()
      println("RECENT FILTER IS        : " + filter1)
      println("RECENT GET /grit/search?\n" + query)
      println("RECENT STOP  : "+manOf(stop_time))
      println("RECENT START : "+manOf(start_time))
      client.close()
      json.parse(response_data.toString)
    }

    case "exportToCsv" :: confId :: startIso :: stopIso :: intervalValue_ :: intervalUnit :: Nil JsonGet req => {
      val intervalValue = intervalValue_.toInt
      val dateHistogramInterval = intervalUnit match {
        case "Hour" => {DateHistogramInterval.hours(intervalValue)}
        case "Day" => {DateHistogramInterval.days(intervalValue)}
        case "Week" => {DateHistogramInterval.weeks(intervalValue)}
        case "Month" => {DateHistogramInterval.MONTH}
        case "Year" => {DateHistogramInterval.YEAR}
        case _ => {DateHistogramInterval.HOUR}
       }
      exportAggregations(confId, startIso, stopIso, dateHistogramInterval)
    }

    case "daily" :: confId :: start_iso:: stop_iso :: Nil JsonGet req => {
      println("RECENT ARGS STOP   : "+stop_iso)
      println("RECENT ARGS STOP   : "+cleantz(stop_iso))

      val (eshost,esjavaport,eshttpport) = getESparams()
      val client: Client =TransportClient.builder().build().addTransportAddress(new InetSocketTransportAddress(java.net.InetAddress.getByName(eshost), esjavaport))
      val stop_time = ISO8601Format.parse(cleantz(stop_iso))
      val timezone = "+"+stop_iso.takeRight(5)
      println("RECENT TIMEZONE   : "+timezone)
      // get start and stop time
      val matchquery = QueryBuilders.matchQuery("ConfigID_FK", confId)
      val start_time = ISO8601Format.parse(cleantz(start_iso))
      println("RECENT COMPUTE START : "+start_time)
      println("RECENT COMPUTE STOP  : "+stop_time)
      val query_start = ISO8601Format.format(start_time)
      val query_stop = ISO8601Format.format(stop_time)
      println("RECENT QUERY START : "+query_start)
      println("RECENT QUERY STOP  : "+query_stop)

      val secInFrame = (stop_time.getTime()-start_time.getTime())/1000;
      val maxDataPoints = 900
      println(secInFrame.toInt);
      val histogramInterval = secInFrame match {
        case x if x <= 4500  => 5
        case _ => secInFrame.toInt / maxDataPoints
      }
      println(histogramInterval)
      // construct the query
      val filter1 = QueryBuilders.rangeQuery("Time").gt(query_start.toString).lte(query_stop.toString)
      val query = QueryBuilders.filteredQuery(matchquery,filter1)

      val agg = AggregationBuilders.dateHistogram("Power")
        .field("Time")
        .interval(DateHistogramInterval.seconds(histogramInterval))//.Post_zone("+01:00"))
        .timeZone(timezone)
        .subAggregation(AggregationBuilders.terms("SourceType").field("SourceType")
          .subAggregation(AggregationBuilders.stats("powerStats").field("Power_real_total")))
        .subAggregation(AggregationBuilders.stats("power").field("Power_real_total"))

      val response_data: SearchResponse = client
        .prepareSearch(index(confId))
        .setTypes("docs")
        //.setSearchType(SearchType.DFS_QUERY_THEN_FETCH)
        .setQuery(query)
        .addAggregation(agg)
        //.setSize(probesPerConfig*window.toInt/timing_int)
        .addSort("Time", SortOrder.ASC)
        .execute()
        .actionGet()
      println("RECENT FILTER IS        : " + filter1)
      println("RECENT GET /grit/search?\n" + query)
      println("RECENT STOP  : "+manOf(stop_time))
      println("RECENT START : "+manOf(start_time))
      client.close()
      json.parse(response_data.toString)
    }

    case "data" :: confId :: start_iso :: stop_iso :: Nil JsonGet req => {
      println("DATA ARGS START : "+start_iso)
      println("DATA ARGS STOP  : "+stop_iso)
      val timezone = "+"+stop_iso.takeRight(5)
      println("DATA TIMEZONE   : "+timezone)
      val (eshost,esjavaport,eshttpport) = getESparams()
      val client: Client =TransportClient.builder().build().addTransportAddress(new InetSocketTransportAddress(java.net.InetAddress.getByName(eshost), esjavaport))
      val max_bars = 100
      // convert time to seconds since epoch
      val start = ISO8601Format.parse(cleantz(start_iso))
      val stop = ISO8601Format.parse(cleantz(stop_iso))

      val startDay = ISO8601Format.format(getMinDay())
      val stopDay = ISO8601Format.format(getMaxDay())


      // determine the query key
      val matchquery = QueryBuilders.matchQuery("ConfigID_FK", confId)
      // construct the query
      val filter1 = QueryBuilders.rangeQuery("Time").gte(start).lte(stop)//.timezone("+01:00")
      val filter2 = QueryBuilders.rangeQuery("Time").gte(startDay).lte(stopDay)
      val query = QueryBuilders.filteredQuery(matchquery,filter1)
      val query2 = QueryBuilders.filteredQuery(matchquery,filter2)

      val response_desc: SearchResponse = client
        .prepareSearch(index(confId))
        .setTypes("docs")
        .setQuery(query)
        .setSize(1)
        .addSort("Time", SortOrder.DESC)
        .execute()
        .actionGet()

      val response_asc: SearchResponse = client
        .prepareSearch(index(confId))
        .setTypes("docs")
        .setQuery(query)
        .setSize(1)
        .addSort("Time", SortOrder.ASC)
        .execute()
        .actionGet()

      val hits_desc: Array[SearchHit] = response_desc.getHits().getHits
      val hits_asc: Array[SearchHit] = response_asc.getHits().getHits
      var intervalToUse = 5
      val interval_ = if (hits_asc.length > 0) {
        val start_time = cleantz(hits_asc(0).getSource.get("Time").toString)
        val stop_time = cleantz(hits_desc(0).getSource.get("Time").toString)
        println("BOUNDS START : "+start_time)
        println("BOUNDS STOP  : "+stop_time)
        val start = ISO8601Format.parse(cleantz(start_time))
        val stop = ISO8601Format.parse(cleantz(stop_time))
        val delta = stop.getTime - start.getTime
        val interval = ((delta / max_bars)/1000).toInt match {
          case x if x >= timing_int => x
          case _ => timing_int
        }
        interval
      }else {5}
      //client.close()

      // compute delta between start and stop time
      val delta = stop.getTime - start.getTime
      // interval = divide delta with maxbars
      // interval >= 5 make sure result is at least the size of timing interval ( 5 secs hardcoded at the moment )
      val interval = ((delta / max_bars)/1000).toInt match {
        case x if x >= timing_int => x
        case _ => timing_int
      }
      val checkDate = ISO8601Format.parse(cleantz("2017-04-29T00:00:00.000+0100"))
      if (start.after(checkDate) || start.equals(checkDate)) {
        intervalToUse = interval_
      } else {
        intervalToUse = interval
      }
      val query_interval = interval + "ms"
      // the number of buckets per interval = interval / size of timing interval (default 5 secs)
      val docs_per_bucket = interval / timing_int
      println(delta)
      println(interval)
      println(query_interval)

      //EnergyBySourceName stats Aggregation
      val agg18 = AggregationBuilders.terms("EnergyBySourceName")
        .field("SourceName")
        .subAggregation(AggregationBuilders.stats("Stats").field("Energy_since_last"))

      val agg17 = AggregationBuilders.terms("EnergyBySourceType")
        .field("SourceType")
        .subAggregation(AggregationBuilders.stats("Stats").field("Energy_since_last"))

      val agg16 = AggregationBuilders.dateHistogram("EnergyDay")
        .field("Time")
        .interval(DateHistogramInterval.DAY)//.Post_zone("+01:00"))
        .timeZone(timezone)
        .subAggregation(AggregationBuilders.terms("SourceType").field("SourceType")
          .subAggregation(AggregationBuilders.stats("CostStats").field("Cost_since_last"))
          .subAggregation(AggregationBuilders.stats("energyStats").field("Energy_since_last")))
        .subAggregation(AggregationBuilders.stats("energy").field("Energy_since_last"))
        .subAggregation(AggregationBuilders.stats("TimeStats").field("Cost_since_last"))

      val agg15 = AggregationBuilders.dateHistogram("batteryDOD")
        .field("Time")
        .interval(DateHistogramInterval.seconds(interval))//.Post_zone("+01:00"))
        .timeZone(timezone)
        .subAggregation(AggregationBuilders.stats("DOD").field("dod"))

      val agg14 = AggregationBuilders.dateHistogram("batteryCap")
        .field("Time")
        .interval(DateHistogramInterval.seconds(interval))//.Post_zone("+01:00"))
        .timeZone(timezone)
        .subAggregation(AggregationBuilders.stats("batteryCapacity").field("batterycapacity"))

      val agg11 = AggregationBuilders.dateHistogram("fuelRate")
        .field("Time")
        .interval(DateHistogramInterval.seconds(interval))//.Post_zone("+01:00"))
        .timeZone(timezone)
        .subAggregation(AggregationBuilders.stats("FuelRate").field("Fuel_hour_rate"))

      val agg10 = AggregationBuilders.dateHistogram("fuel")
        .field("Time")
        .interval(DateHistogramInterval.seconds(interval))//.Post_zone("+01:00"))
        .timeZone(timezone)
        .subAggregation(AggregationBuilders.stats("Fuel").field("Fuel_since_last"))


      val agg7 = AggregationBuilders.dateHistogram("Energy")
        .field("Time")
        .interval(DateHistogramInterval.seconds(interval))//.Post_zone("+01:00"))
        .timeZone(timezone)
        .subAggregation(AggregationBuilders.terms("SourceType").field("SourceType")
        .subAggregation(AggregationBuilders.terms("SourceName").field("SourceName")))
        .subAggregation(AggregationBuilders.stats("energy").field("Energy_since_last"))


      val agg6 = AggregationBuilders.terms("costName")
        .field("SourceName")
        .subAggregation(AggregationBuilders.stats("TimeStats").field("Cost_since_last"))

      val agg5 = AggregationBuilders.terms("costType")
        .field("SourceType")
        .subAggregation(AggregationBuilders.stats("TimeStats").field("Cost_since_last"))

      val agg4 = AggregationBuilders.dateHistogram("Cost")
        .field("Time")
        .interval(DateHistogramInterval.seconds(interval))//.Post_zone("+01:00"))
        .timeZone(timezone)
        .subAggregation(AggregationBuilders.terms("SourceType").field("SourceType")
        .subAggregation(AggregationBuilders.terms("SourceName").field("SourceName")))
        .subAggregation(AggregationBuilders.stats("cost").field("Cost_since_last"))


      val agg3 = AggregationBuilders.terms("timingName")
        .field("SourceName")
        .subAggregation(AggregationBuilders.stats("TimeStats").field("Time"))
        .subAggregation(AggregationBuilders.terms("timingType").field("SourceType"))
        .subAggregation(AggregationBuilders.stats("reactive_stats").field("Power_reactive_total"))
        .subAggregation(AggregationBuilders.stats("apparent_stats").field("Power_apparent_total"))
        .subAggregation(AggregationBuilders.stats("real_stats").field("Power_real_total"))

      val agg2 = AggregationBuilders.terms("timingType")
        .field("SourceType")
        .subAggregation(AggregationBuilders.stats("TimeStats").field("Time"))

      val agg20 = AggregationBuilders.dateHistogram("Power")
        .field("Time")
        .interval(DateHistogramInterval.seconds(intervalToUse))//.Post_zone("+01:00"))
        .timeZone(timezone)
        .subAggregation(AggregationBuilders.terms("SourceType").field("SourceType")
        .subAggregation(AggregationBuilders.terms("SourceName").field("SourceName")
        .subAggregation(AggregationBuilders.stats("Power_real_total").field("Power_real_total"))))
        .subAggregation(AggregationBuilders.stats("Power_real").field("Power_real_total"))


      val agg1 = AggregationBuilders.dateHistogram("power")
        .field("Time")
        .interval(DateHistogramInterval.seconds(interval))//.Post_zone("+01:00"))
        .timeZone(timezone)
        .subAggregation(AggregationBuilders.terms("SourceType").field("SourceType")
        .subAggregation(AggregationBuilders.terms("SourceName").field("SourceName")))
        .subAggregation(AggregationBuilders.terms("SourceName").field("SourceName"))
        .subAggregation(AggregationBuilders.stats("Power_real_total").field("Power_real_total"))

      val response_data: SearchResponse = client
        .prepareSearch(index(confId))
        .setTypes("docs")
        .setSearchType(SearchType.DFS_QUERY_THEN_FETCH)
        .setQuery(query)
        .addAggregation(agg1)
        .addAggregation(agg2)
        .addAggregation(agg3)
        .addAggregation(agg4)
        .addAggregation(agg5)
        .addAggregation(agg6)
        .addAggregation(agg7)
        .addAggregation(agg10)
        .addAggregation(agg11)
        .addAggregation(agg14)
        .addAggregation(agg17)
        .addAggregation(agg18)
        .addAggregation(agg20)
        .addSort("Time", SortOrder.DESC)
        .execute()
        .actionGet()

      val response_data_battery: SearchResponse = client
        .prepareSearch("battery")
        .setTypes("docs")
        .setSearchType(SearchType.DFS_QUERY_THEN_FETCH)
        .setQuery(query)
        .addAggregation(agg15)
        .addSort("Time", SortOrder.DESC)
        .execute()
        .actionGet()

      val response_data_Day: SearchResponse = client
        .prepareSearch(index(confId))
        .setTypes("docs")
        .setSearchType(SearchType.DFS_QUERY_THEN_FETCH)
        .setQuery(query2)
        .addAggregation(agg16)
        .addSort("Time", SortOrder.DESC)
        .execute()
        .actionGet()
      client.close()
      val intervalData = ("interval" -> interval_)
      val mergedResponses =    json.parse(response_data_battery.toString) merge json.parse(response_data.toString) merge json.parse(response_data_Day.toString) merge json.parse(json.compact(render(intervalData)))
	println("JSON   :" + json.pretty(render(mergedResponses)))
      val configuration = index(confId) match {
        case "consumer" => {
          val ConfigGroupID_FK = Configuration.findAll("_id",confId).headOption.getOrElse(Configuration).ConfigGroupID_FK.value
          ConfigGroup.findAll("_id",ConfigGroupID_FK).headOption.getOrElse(ConfigGroup).SourceConfigID_FK.value
        }
        case "grit" => confId
      }
      val channels = Probe.findAll.flatMap( p => {
        p.Channels.value.filter(_.ConfigID_FK.value == configuration)
      })
      val groupedChannels = channels.groupBy(_.SourceID_FK.value)
      val generators = groupedChannels.map( x => {Generator.findAll(("_id" -> x._1))})
      val powersources = generators.flatMap(x => x.map( y => "\""+y.sourceName+"\" :"+y.asJSON))
      val sourcedata = "{\"powersources\" :{"+powersources.mkString(",")+"}}"
     mergeJson(mergedResponses,json.parse(sourcedata.toString))
    }

    case "consumerGroup" :: groupId :: start_iso :: stop_iso :: Nil  JsonGet req => {
      println("DATA ARGS START : "+start_iso)
      println("DATA ARGS STOP  : "+stop_iso)
      val timezone = "+"+stop_iso.takeRight(5)
      println("DATA TIMEZONE   : "+timezone)
      val (eshost,esjavaport,eshttpport) = getESparams()
      val client: Client =TransportClient.builder().build().addTransportAddress(new InetSocketTransportAddress(java.net.InetAddress.getByName(eshost), esjavaport))
      //val max_bars = 100
      // convert time to seconds since epoch
      val start = ISO8601Format.parse(cleantz(start_iso))
      val stop = ISO8601Format.parse(cleantz(stop_iso))

      val startDay = ISO8601Format.format(getMinDay())
      val stopDay = ISO8601Format.format(getMaxDay())

      val maxDataPoints = 900
      val secInFrame = (stop.getTime()-start.getTime())/1000;
      val histogramInterval = secInFrame.toInt / maxDataPoints
      println("histogramInterval : " + histogramInterval)

      // determine the query key
      val matchquery = QueryBuilders.matchQuery("GroupID_FK", groupId)

      // construct the query
      val filter = QueryBuilders.rangeQuery("Time").gte(start).lte(stop)
      val query = QueryBuilders.filteredQuery(matchquery,filter)

      val agg = AggregationBuilders.dateHistogram("consumerGroupCost")
        .field("Time")
        .interval(DateHistogramInterval.seconds(histogramInterval))//.Post_zone("+01:00"))
        .timeZone(timezone)
        .subAggregation(AggregationBuilders.terms("consumerCost").field("consumerId")
          .subAggregation(AggregationBuilders.terms("costConsumer").field("ConsumerName")
            .subAggregation(AggregationBuilders.stats("costStats").field("Cost_since_last"))))


      val agg3 = AggregationBuilders.dateHistogram("consumerGroup")
        .field("Time")
        .interval(DateHistogramInterval.seconds(histogramInterval))//.Post_zone("+01:00"))
        .timeZone(timezone)
        .subAggregation(AggregationBuilders.terms("consumerEnergy").field("consumerId")
          .subAggregation(AggregationBuilders.terms("energyConsumer").field("ConsumerName")
          .subAggregation(AggregationBuilders.stats("energyStats").field("Energy_since_last"))))

      val response_data_Day: SearchResponse = client
        .prepareSearch("consumer")
        .setTypes("docs")
        .setSearchType(SearchType.DFS_QUERY_THEN_FETCH)
        //.setQuery(matchquery)
        .setQuery(query)
        .addAggregation(agg)
        .addAggregation(agg3)
        .addSort("Time", SortOrder.DESC)
        .execute()
        .actionGet()
      client.close()
      json.parse(response_data_Day.toString)
    }

    case "consumer" :: confId :: start_iso :: stop_iso :: Nil  JsonGet req => {
      println("DATA ARGS START : "+start_iso)
      println("DATA ARGS STOP  : "+stop_iso)
      val timezone = "+"+stop_iso.takeRight(5)
      println("DATA TIMEZONE   : "+timezone)
      val (eshost,esjavaport,eshttpport) = getESparams()
      val client: Client =TransportClient.builder().build().addTransportAddress(new InetSocketTransportAddress(java.net.InetAddress.getByName(eshost), esjavaport))
      //val max_bars = 100
      // convert time to seconds since epoch
      val start = ISO8601Format.parse(cleantz(start_iso))
      val stop = ISO8601Format.parse(cleantz(stop_iso))

      val startDay = ISO8601Format.format(getMinDay())
      val stopDay = ISO8601Format.format(getMaxDay())

      //get CofigGroupID_FK from confId
      val configGroup = ConfigGroup.find("SourceConfigID_FK", confId)
      val configGroupId : String  = configGroup match {
        case Full(conf) => conf.id.toString
        case _ => ""
      }

      // determine the query key
      val matchquery = QueryBuilders.matchQuery("ConfigGroupID_FK", configGroupId)

      // construct the query
      val filter = QueryBuilders.rangeQuery("Time").gte(start).lte(stop)
      val query = QueryBuilders.filteredQuery(matchquery,filter)

      val agg =  AggregationBuilders.terms("consumerEnergy")
        .field("consumerId")
        .subAggregation(AggregationBuilders.terms("energyConsumer").field("ConsumerName")
        .subAggregation(AggregationBuilders.stats("energyStats").field("Energy_since_last")))

      val agg1 =  AggregationBuilders.terms("consumerCost")
        .field("consumerId")
        .subAggregation(AggregationBuilders.terms("costConsumer").field("ConsumerName")
        .subAggregation(AggregationBuilders.stats("costStats").field("Cost_since_last")))

      val response_data_Day: SearchResponse = client
        .prepareSearch("consumer")
        .setTypes("docs")
        .setSearchType(SearchType.DFS_QUERY_THEN_FETCH)
        //.setQuery(matchquery)
        .setQuery(query)
        .addAggregation(agg)
        .addAggregation(agg1)
        .addSort("Time", SortOrder.DESC)
        .execute()
        .actionGet()
      client.close()
      json.parse(response_data_Day.toString)
    }

    case "monthly" :: confId :: start_iso:: stop_iso :: Nil JsonGet req => {
      println("DATA ARGS START : "+start_iso)
      println("DATA ARGS STOP  : "+stop_iso)
      val timezone = "+"+stop_iso.takeRight(5)
      println("DATA TIMEZONE   : "+timezone)
      val (eshost,esjavaport,eshttpport) = getESparams()
      val client: Client =TransportClient.builder().build().addTransportAddress(new InetSocketTransportAddress(java.net.InetAddress.getByName(eshost), esjavaport))

      val start = ISO8601Format.parse(cleantz(start_iso))
      val stop = ISO8601Format.parse(cleantz(stop_iso))

      val startDay = ISO8601Format.format(getMinDay())
      val stopDay = ISO8601Format.format(getMaxDay())

      // determine the query key
      val matchquery = QueryBuilders.matchQuery("ConfigID_FK", confId)

      // construct the query
      val filter2 = QueryBuilders.rangeQuery("Time").gte(start).lte(stop)
      val query2 = QueryBuilders.filteredQuery(matchquery,filter2)

      val agg16 = AggregationBuilders.dateHistogram("EnergyDay")
        .field("Time")
        .interval(DateHistogramInterval.DAY)//.Post_zone("+01:00"))
        .timeZone(timezone)
        .subAggregation(AggregationBuilders.terms("SourceType").field("SourceType")
          .subAggregation(AggregationBuilders.stats("CostStats").field("Cost_since_last"))
          .subAggregation(AggregationBuilders.stats("energyStats").field("Energy_since_last")))
        .subAggregation(AggregationBuilders.stats("energy").field("Energy_since_last"))
        .subAggregation(AggregationBuilders.stats("TimeStats").field("Cost_since_last"))

      val response_data_Day: SearchResponse = client
        .prepareSearch(index(confId))
        .setTypes("docs")
        .setSearchType(SearchType.DFS_QUERY_THEN_FETCH)
        //.setQuery(matchquery)
        .setQuery(query2)
        .addAggregation(agg16)
        .addSort("Time", SortOrder.DESC)
        .execute()
        .actionGet()
      println("RECENT FILTER IS  MONTH      : " + filter2)
      println("RECENT MONTH GET /grit/search?\n" + query2)
      println("RECENT STOP MONTH  : "+(startDay))
      println("RECENT START MONTH: "+(stopDay))
      println("start" + start_iso + " "+start)
      println("stop" + stop_iso + " " +stop)
      client.close()

      json.parse(response_data_Day.toString)
    }

    case "yearly" :: confId :: start_iso:: stop_iso :: Nil JsonGet req => {
      println("DATA ARGS START : "+start_iso)
      println("DATA ARGS STOP  : "+stop_iso)
      val timezone = "+"+stop_iso.takeRight(5)
      println("DATA TIMEZONE   : "+timezone)
      val (eshost,esjavaport,eshttpport) = getESparams()
      val client: Client =TransportClient.builder().build().addTransportAddress(new InetSocketTransportAddress(java.net.InetAddress.getByName(eshost), esjavaport))

      val start = ISO8601Format.parse(cleantz(start_iso))
      val stop = ISO8601Format.parse(cleantz(stop_iso))

      val startDay = ISO8601Format.format(getMinDay())
      val stopDay = ISO8601Format.format(getMaxDay())

      // determine the query key
      val matchquery = QueryBuilders.matchQuery("ConfigID_FK", confId)

      // construct the query
      val filter2 = QueryBuilders.rangeQuery("Time").gte(start).lte(stop)
      val query2 = QueryBuilders.filteredQuery(matchquery,filter2)

      val agg16 = AggregationBuilders.dateHistogram("EnergyYear")
        .field("Time")
        .interval(DateHistogramInterval.MONTH)//.Post_zone("+01:00"))
        .timeZone(timezone)
        .subAggregation(AggregationBuilders.terms("SourceType").field("SourceType")
          .subAggregation(AggregationBuilders.stats("CostStats").field("Cost_since_last"))
          .subAggregation(AggregationBuilders.stats("energyStats").field("Energy_since_last")))
        .subAggregation(AggregationBuilders.stats("energy").field("Energy_since_last"))
        .subAggregation(AggregationBuilders.stats("TimeStats").field("Cost_since_last"))

      val response_data_Day: SearchResponse = client
        .prepareSearch(index(confId))
        .setTypes("docs")
        .setSearchType(SearchType.DFS_QUERY_THEN_FETCH)
        //.setQuery(matchquery)
        .setQuery(query2)
        .addAggregation(agg16)
        .addSort("Time", SortOrder.DESC)
        .execute()
        .actionGet()
      println("RECENT FILTER IS YEAR      : " + filter2)
      println("RECENT YEAR GET /grit/search?\n" + query2)
      println("RECENT STOP YEAR  : "+(startDay))
      println("RECENT START YEAR: "+(stopDay))
      println("start" + start_iso + " "+start)
      println("stop" + stop_iso + " " +stop)
      client.close()

      json.parse(response_data_Day.toString)
    }

    case "bounds" :: confId :: Nil JsonGet req => {
      val (eshost,esjavaport,eshttpport) = getESparams()
      val client: Client =TransportClient.builder().build().addTransportAddress(new InetSocketTransportAddress(java.net.InetAddress.getByName(eshost), esjavaport))

      // get start and stop time
      val query = QueryBuilders.matchQuery("ConfigID_FK", confId)

      val response_desc: SearchResponse = client
        .prepareSearch(index(confId))
        .setTypes("docs")
        .setQuery(query)
        .setSize(1)
        .addSort("Time", SortOrder.DESC)
        .execute()
        .actionGet()

      val response_asc: SearchResponse = client
        .prepareSearch(index(confId))
        .setTypes("docs")
        .setQuery(query)
        .setSize(1)
        .addSort("Time", SortOrder.ASC)
        .execute()
        .actionGet()

      val hits_desc: Array[SearchHit] = response_desc.getHits().getHits
      val hits_asc: Array[SearchHit] = response_asc.getHits().getHits

      if (hits_asc.length > 0) {
        val start_time = cleantz(hits_asc(0).getSource.get("Time").toString)
        val stop_time = cleantz(hits_desc(0).getSource.get("Time").toString)
        println("BOUNDS START : "+start_time)
        println("BOUNDS STOP  : "+stop_time)
        client.close()
        ("start" -> ISO8601Format.parse(start_time ).getTime ) ~
          ("stop" -> ISO8601Format.parse(stop_time ).getTime): JValue
      }else {
        client.close()
        ("start" -> "") ~
          ("stop" -> ""): JValue
      }
    }
  }
}
