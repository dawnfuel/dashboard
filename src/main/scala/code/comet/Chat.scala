package code.comet

import code.model.Configuration
import net.liftweb._
import http._
import actor._
import util._
import Helpers._
import net.liftweb.json.JsonAST._
import net.liftweb.json.JsonDSL._
import scala.concurrent._
import scala.concurrent.duration._
import scala.concurrent.ExecutionContext.Implicits._
import net.liftweb.common.Full
/**
 * The comet chat component
 */
class Chat extends CometActor with CometListener {
  private var msgs: Vector[String] = Vector() // private state

  // register this component
  def registerWith = ChatServer

  // listen for messages
  override def lowPriority = {
    case v: Vector[String] => {
      println(msgs, v )
      Future { blocking(Thread.sleep(10000L)); "done" }
      msgs = v; reRender()
    }
  }

  // render the component
  def render = ClearClearable & "li *" #> msgs
}


/**
 * The chat server
 */
object ChatServer extends LiftActor with ListenerManager {
  def manOf[T: Manifest](t: T): Manifest[T] = manifest[T]
  implicit val formats = net.liftweb.json.DefaultFormats
  private var msgs = Vector(S ? "Welcome") // the private data

  // what we send to listeners on update
  def createUpdate = msgs

  // handle incoming messages
  override def lowPriority = {
    case s: String => {
      println(" "+s)
      msgs = (msgs :+ s.trim).filter(_.length > 0).takeRight(20)
      updateListeners()
    }
    case s: JValue => {

      val V = s.extract[Power_USER].ConfigID_FK
      val conf = Configuration.findAll("_id", V)
      msgs = (msgs :+ V.trim).filter(_.length > 0).takeRight(20)
      println("CONFIG ID : "+V)
      println(conf)
      val currentUserIDs = conf.head.UserID_FKs.value //""//User.currentUserId.headOption.getOrElse("")
      println("USER ID   : "+currentUserIDs)
      currentUserIDs.foreach { currentUserID =>
        NamedCometListener.getDispatchersFor(Full(currentUserID.toString)).foreach(_.foreach(_ ! s))
      }
      println("======================chatting===================")
      updateListeners()
    }

  }
  case class Probe(ProbeID_FK: String)
  case class Power_USER(
                         ProbeName  : String,
                         ProbeID_FK : String,
                         ConfigID_FK: String,
                         DeviceType : String,
                         Time : String,
                         SourceType : String,
                         SourceName : String,
                         Voltage : Seq[Double],
                         Current : Seq[Double],
                         Power_factor : Seq[Double],
                         Power_real : Seq[Double],
                         Power_apparent : Seq[Double],
                         Power_reactive : Seq[Double],
                         Energy_since_last : Seq[Double],
                         Power_real_total : Double,
                         Power_apparent_total : Double,
                         Power_reactive_total : Double,
                         Energy_since_last_total : Double
                         )
}