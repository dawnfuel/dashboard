//package com.myapp.comet
package code.comet

import java.util.Date
import net.liftweb._
import net.liftweb.http._
import js.{JsCmds, JsCmd, JE}
import http._
import actor._
import util._
import net.liftweb.http.SHtml._
import Helpers._
import scala.concurrent._
import scala.concurrent.duration._
import scala.concurrent.ExecutionContext.Implicits._
import js.JsCmds._
import js.JE._
import _root_.scala.xml.Text
import scala.xml.Node
import common._

class Clock extends CometActor {
  override def defaultPrefix = Full("clk")
  def render = bind("servertime" -> servertimeSpan, "probetime" -> probetimeSpan )
  def servertimeSpan = (<span id="servertime">{timeNow}</span>)
  def probetimeSpan = (<span id="probetime">{timeNow}</span>)
  // schedule a ping every 1 second so we redraw
  ActorPing.schedule(this, Tick, 1000L)
  override def lowPriority : PartialFunction[Any, Unit] = {
    case Tick => {
      //println("Got tick " + new Date());
      partialUpdate(SetHtml("servertime", Text(timeNow.toString)))
      partialUpdate(JE.JsRaw("""document.getElementById("usertime").innerHTML = moment().format('YYYY-MM-DDTHH:mm:ss.SSSZZ');"""))
      // schedule an update in 1 second
      ActorPing.schedule(this, Tick, 1000L)
    }
  }
}
case object Tick