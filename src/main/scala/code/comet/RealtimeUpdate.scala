package code.comet

/**
  * Created by idarlington on 4/22/16.
  */

import java.text.SimpleDateFormat
import java.util.{Calendar, Date}

import net.liftweb.json.JsonAST.JValue
import net.liftweb.{actor}
import net.liftweb.util._
import Helpers._
import org.elasticsearch.action.search.SearchResponse
import org.elasticsearch.client.Client
import org.elasticsearch.client.transport.TransportClient
import org.elasticsearch.common.transport.InetSocketTransportAddress
import org.elasticsearch.index.query.QueryBuilders
import org.elasticsearch.search.aggregations.AggregationBuilders
import org.elasticsearch.search.aggregations.bucket.histogram.DateHistogramInterval
import org.elasticsearch.search.sort.SortOrder

import net.liftweb.json._

import net.liftweb.util.Schedule
import net.liftweb.actor.LiftActor
import net.liftweb.util.Helpers._


object MyScheduledTask extends LiftActor {
  case class DoIt()
  case class Stop()
  private var stopped = false
  def messageHandler = {
    case DoIt if !stopped =>
      Schedule.schedule(this, DoIt, 60*1000L)
    // ... do useful work here

      //RealtimeUpdate.init
      println("=====================performing real time updates==============================")
    case Stop =>
      stopped = true
  }
}



object RealtimeUpdate {

  def cleantz( time : String ) : String = {
    var sign_builder= new StringBuilder ++= time
    var clean_sign = ""
    if (sign_builder.charAt(23).toString == "-"){
      clean_sign= sign_builder.replace(23,24,"-").toString()
    }else{
      clean_sign = sign_builder.replace(23,24,"+").toString()
    }
    var time_builder= new StringBuilder ++= clean_sign
    if (time_builder.charAt(26).toString == ":"){
      val cleanz = time_builder.deleteCharAt(26)
      cleanz.toString()
    }else{
      time_builder.toString()
    }
  }

  def manOf[T: Manifest](t: T): Manifest[T] = manifest[T]



  def getESparams() : (String,Int,Int) = {
    val eshost = try {
      sys.env("ESHOST").toString
    } catch {
      case e: Exception => "localhost"
    }
    val esjavaport = try {
      sys.env("ESJAVAPORT").toInt
    } catch {
      case e: Exception => 9300
    }
    val eshttpport = try {
      sys.env("ESHTTPPORT").toInt
    } catch {
      case e: Exception => 9200
    }
    (eshost,esjavaport,eshttpport)
  }

  def getMaxTime (): String = {
    val  dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZ");
    var date = new Date()
    //val convertedDate = dateFormat.parse(date)
    val d = Calendar.getInstance();
    d.setTime(date);
    d.set(Calendar.HOUR_OF_DAY, 0);
    d.set(Calendar.MINUTE, 0);
    d.set(Calendar.SECOND, 0);
    d.set(Calendar.MILLISECOND, 0);
    return (dateFormat.format(d.getTime()))

  }

  def getMinTime (): String = {
    val  dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZ");
    var date = new Date()
    //val convertedDate = dateFormat.parse(date)
    val  c = Calendar.getInstance();
    c.setTime(date);
    c.set(Calendar.HOUR_OF_DAY, 23);
    c.set(Calendar.MINUTE, 59);
    c.set(Calendar.SECOND, 59);
    c.set(Calendar.MILLISECOND, 0);
    return (dateFormat.format(c.getTime()))
  }

  def init(currentUserID:String,ConfigID:String):JValue = {
    println("===============Starting Elasticsearch AGG =========================")
    val ISO8601Format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZ")
    //  ISO8601Format.setTimeZone(TimeZone.getTimeZone("UTC"));
    ISO8601Format.setLenient(false)

    // println("RECENT TIMEZONE   : "+timezone)
    val (eshost,esjavaport,eshttpport) = getESparams()
    val client: Client =TransportClient.builder().build().addTransportAddress(new InetSocketTransportAddress(java.net.InetAddress.getByName(eshost), esjavaport))
    val stop_time = ISO8601Format.parse((getMinTime().toString))
    val start_time = ISO8601Format.parse((getMaxTime().toString))

      val matchquery = QueryBuilders.matchQuery("ConfigID_FK", ConfigID)

      println("RECENT COMPUTE START : " + start_time)
      println("RECENT COMPUTE STOP  : " + stop_time)
      val query_start = ISO8601Format.format(start_time)
      val query_stop = ISO8601Format.format(stop_time)
      println("RECENT QUERY START : " + query_start)
      println("RECENT QUERY STOP  : " + query_stop)

      // construct the query
      val filter1 = QueryBuilders.rangeQuery("Time").gte(query_start.toString).lte(query_stop.toString)
      val query = QueryBuilders.filteredQuery(matchquery, filter1)

      val agg = AggregationBuilders.dateHistogram("Power")
        .field("Time")
        .interval(DateHistogramInterval.seconds(96)) //.Post_zone("+01:00"))
        .subAggregation(AggregationBuilders.terms("SourceType").field("SourceType")
          .subAggregation(AggregationBuilders.stats("powerStats").field("Power_real_total")))
        .subAggregation(AggregationBuilders.stats("power").field("Power_real_total"))

      val response_data: SearchResponse = client
        .prepareSearch("grit")
        .setTypes("docs")
        //.setSearchType(SearchType.DFS_QUERY_THEN_FETCH)
        .setQuery(query)
        .addAggregation(agg)
        //.setSize(probesPerConfig*window.toInt/timing_int)
        .addSort("Time", SortOrder.ASC)
        .execute()
        .actionGet()
      println("RECENT FILTER IS        : " + filter1)
      println("RECENT GET /grit/search?\n" + query)
      println("RECENT STOP  : " + manOf(stop_time))
      println("RECENT START : " + manOf(start_time))

      val json = parse(response_data.toString)
      println("===============Elasticsearch Update AGG Done=========================")

    client.close()
   return  json
  }

}




