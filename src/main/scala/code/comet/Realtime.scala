package code.comet
import code._
import net.liftweb.http._
import js.{JE, JsCmd, JsCmds}
import net.liftweb.util.Helpers._
import code.model._
import code.rest.Realtime._
import net.liftweb.common.Full
import net.liftweb.json.JsonAST._
import net.liftweb.json.JsonDSL._

import _root_.scala.xml.Text
import net.liftweb.http.js.JsCmds.SetHtml
/**
 * Comet actor that displays the item id and updated price
 */

class RealtimeComet extends NamedCometActorTrait {
  implicit val formats = net.liftweb.json.DefaultFormats
  def manOf[T: Manifest](t: T): Manifest[T] = manifest[T]

  def render = {
    val user = User.currentUserId.headOption.getOrElse("")
    "#price *+"      #> user &
      "#itemName *"   #> user
    }

  def roundTo (n:Double): Double = {

    Math.round( n * 100.0)/100.0;

  }

    override def lifespan = Full(20 seconds)
    override def cometRenderTimeout = 10000L
    override def cometProcessingTimeout = 10000L

    override def lowPriority = {
      case s: String => {
        println(" "+s)
      }
      case s: JValue => {
        val json = compactRender(s)
        val time = s.extract[Power_MSG].Time
        val sourceType = s.extract[Power_MSG].SourceType
        val name = s.extract[Power_MSG].SourceName
        val Voltage1 = roundTo (s.extract[Power_MSG].Voltage1)
        val Voltage2 = roundTo (s.extract[Power_MSG].Voltage2)
        val Voltage3 = roundTo (s.extract[Power_MSG].Voltage3)
        val Current1 = roundTo (s.extract[Power_MSG].Current1)
        val Current2 = roundTo (s.extract[Power_MSG].Current2)
        val Current3 = roundTo (s.extract[Power_MSG].Current3)
        val realPower = roundTo (s.extract[Power_MSG].Power_real_total)
        val apparentPower = roundTo (s.extract[Power_MSG].Power_apparent_total)
        val reactivePower = roundTo (s.extract[Power_MSG].Power_reactive_total)
        val PowerFactor1 = roundTo (s.extract[Power_MSG].PowerFactor1)
        val PowerFactor2 = roundTo (s.extract[Power_MSG].PowerFactor2)
        val PowerFactor3 = roundTo (s.extract[Power_MSG].PowerFactor3)
        partialUpdate(SetHtml("probetime", Text(time.toString)))
        partialUpdate(SetHtml("sourceType", Text(sourceType.toString)))
        partialUpdate(SetHtml("probename", Text(name.toString)))
        partialUpdate(SetHtml("voltage1", Text(Voltage1.toString)))
        partialUpdate(SetHtml("voltage2", Text(Voltage2.toString)))
        partialUpdate(SetHtml("voltage3", Text(Voltage3.toString)))
        partialUpdate(SetHtml("current1", Text(Current1.toString)))
        partialUpdate(SetHtml("current2", Text(Current2.toString)))
        partialUpdate(SetHtml("current3", Text(Current3.toString)))
        partialUpdate(SetHtml("realPower", Text(realPower.toString)))
        partialUpdate(SetHtml("apparentPower", Text(apparentPower.toString)))
        partialUpdate(SetHtml("reactivePower", Text(reactivePower.toString)))
        partialUpdate(SetHtml("pf1", Text(PowerFactor1.toString)))
        partialUpdate(SetHtml("pf2", Text(PowerFactor2.toString)))
        partialUpdate(SetHtml("pf3", Text(PowerFactor3.toString)))

        partialUpdate(JE.JsRaw("""$("#realtime_stuff")[0].dispatchEvent(new CustomEvent('build', { 'detail':  %s }))""".format(json)).cmd)
      }

      case s:Fun => {
        val json = compactRender(s.json)
        partialUpdate(JE.JsRaw("""$("#realtime_stuff")[0].dispatchEvent(new CustomEvent('aggregation', { 'detail':  %s }))""".format(json)).cmd)
      }
      case _ => {
        println("\nDEFAULT")
      }



    }
}




