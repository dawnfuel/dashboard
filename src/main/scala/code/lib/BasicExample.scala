package code
package lib

import code.model._
import net.liftweb._
import common._
import http._
import net.liftweb.http.rest.RestHelper


/**
 * A simple example of a REST style interface
 * using the basic Lift tools
 */

object BasicExample extends RestHelper {
  /*
   * Given a suffix and an item, make a LiftResponse
   */
  private def toResponse(suffix: String, item: Item) =
    suffix match {
      case "xml" => XmlResponse(item)
      case _ => JsonResponse(item)
    }
  /**
   * Find /simple/item/1234.json
   * Find /simple/item/1234.xml
   */
  lazy val findItem: LiftRules.DispatchPF = {
    case Req("simple" :: "item" :: itemId :: Nil, //  path
    suffix, // suffix
    GetRequest) => {
      println("EXEC 1")
      () => Item.find(itemId).map(toResponse(suffix, _))}
  }
  /**
   * Find /simple2/item/1234.json
   */
  lazy val extractFindItem: LiftRules.DispatchPF = {
    // path with extractor
    case Req("simple2" :: "item" :: Item(item) :: Nil,
    suffix, GetRequest) => {
      // a function that returns the response
      println("EXEC 2")
      () => Full(toResponse(suffix, item))
    }
  }
}