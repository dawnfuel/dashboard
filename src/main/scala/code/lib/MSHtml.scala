package code.lib
import net.liftweb._
import http._
import S._
import common._
import util._
import util.Helpers._
import http.js._
import http.js.AjaxInfo
import JE._
import JsCmds._
import scala.xml._
import json._
import scala.xml._
import net.liftweb.http.SHtml._


trait MSHtml extends Loggable {

  private def deferCall(data: JsExp, jsFunc: Call): Call =
    Call(jsFunc.function, (jsFunc.params ++ List(AnonFunc(makeAjaxCall(data)))): _*)

  private def selected(in: Boolean) = if (in) new UnprefixedAttribute("selected", "selected", Null) else Null

  def ajaxMultiSelect(opts: Seq[(String, String)], deflt: Seq[String], func: List[String] => JsCmd, attrs: ElemAttr*): Elem =
    ajaxMultiSelect_*(opts, deflt, Empty, LFuncHolder(func), attrs: _*)

  def ajaxMultiSelect(opts: Seq[(String, String)], deflt: Seq[String], jsFunc: Call, func: List[String] => JsCmd, attrs: ElemAttr*): Elem =
    ajaxMultiSelect_*(opts, deflt, Full(jsFunc), LFuncHolder(func), attrs: _*)

  private def ajaxMultiSelect_*(opts: Seq[(String, String)], deflt: Seq[String], jsFunc: Box[Call], func: AFuncHolder, attrs: ElemAttr*): Elem = {
    val optionSelect =
      """function(funcName, element) {
        |  var postData = ""
        |  var i = 0;
        |  var k = 0;
        |  for (k = 0; k < element.length; k++) {
        |   console.log("M1 "+k+" : "+element[k].selected)
        |   if (element[k].selected) {
        |     if (i == 0)
        |       postData = funcName + '=' + encodeURIComponent(element[k].value);
        |     else {
        |       postData = postData + '&' + funcName + '=' + encodeURIComponent(element[k].value);
        |     }
        |     i++;
        |   }
        |  }
        |  console.log(postData)
        |  return postData;
        |}""".stripMargin

    val raw = (funcName: String, value: String) => JsRaw(optionSelect + "('" + funcName + "'," + value + ")")
    val key = formFuncName

    val vals = opts.map(_._1)
    val testFunc = LFuncHolder(in => in.filter(v => vals.contains(v)) match {case Nil => false case xs => func(xs)}, func.owner)
    fmapFunc((testFunc)) {
      funcName =>
        (attrs.foldLeft(<select multiple="multiple">{opts.flatMap {case (value, text) => (<option value={value}>{text}</option>) % selected(deflt.contains(value))}}</select>)(_ % _)) %
          ("onchange" -> (jsFunc match {
            case Full(f) => {
              println("MSEL T FULL "+testFunc+"  ::  "+func+"  ::  "+f+" : "+funcName )
              JsCrVar(key, JsRaw("this")) & deferCall(raw(funcName, key), f)
            }
            case _ => {
              println("MSEL T DEFAULT "+testFunc+"  ::  "+func+"  ::  ")
              makeAjaxCall(raw(funcName, "this"))
            }
          }))
    }
  }

  def ajaxUntrustedMultiSelect(opts: Seq[(String, String)], deflt: Seq[String], func: List[String] => JsCmd, attrs: ElemAttr*): Elem =
    ajaxUntrustedMultiSelect_*(opts, deflt, Empty, LFuncHolder(func), attrs: _* )

  def ajaxUntrustedMultiSelect(opts: Seq[(String, String)], deflt: Seq[String], jsFunc: Call, func: List[String] => JsCmd, attrs: ElemAttr*): Elem =
    ajaxUntrustedMultiSelect_*(opts, deflt, Full(jsFunc), LFuncHolder(func), attrs: _*)

  private def ajaxUntrustedMultiSelect_*(opts: Seq[(String, String)], deflt: Seq[String], jsFunc: Box[Call], func: AFuncHolder, attrs: ElemAttr*): Elem = {

    val optionSelect =
      """function(funcName, element) {
        |  var postData = ""
        |  var i = 0;
        |  var k = 0;
        |  for (k = 0; k < element.length; k++) {
        |   if (element[k].selected) {
        |     if (i == 0)
        |       postData = funcName + '=' + encodeURIComponent(element[k].value);
        |       // postData = funcName + '=' + this.options(element[k].value);
        |     else {
        |       postData = postData + '&' + funcName + '=' + encodeURIComponent(element[k].value);
        |       // postData = postData + '&' + funcName + '=' + this.options(element[k].value);
        |     }
        |     i++;
        |   }
        |  }
        |  console.log("POST DATA")
        |  console.log(postData)
        |  return postData;
        |}""".stripMargin

    val raw = (funcName: String, value: String) => JsRaw(optionSelect + "('" + funcName + "'," + value + ")")
    val key = formFuncName

    //    val raw = (funcName: String, value: String) => JsRaw("'" + funcName + "=' + encodeURIComponent(" + value + ".options[" + value + ".selectedIndex].value)")
    //    val raw = (funcName: String, value: String) => JsRaw("'" + funcName + "=' + this.options[" + value + ".selectedIndex].value")//untrusted

    val vals = opts.map(_._1)
    val testFunc = LFuncHolder(in => in match {
      case Nil => false
      case xs => func(xs)
    }, func.owner)
    fmapFunc(contextFuncBuilder(testFunc)) {
      import net.liftweb.http.js.JsCmds.JsCrVar
      funcName =>
        (attrs.foldLeft(<select multiple="multiple">
          {opts.flatMap { case (value, text) => (<option value={value}>
            {text}
          </option>) % selected(deflt.contains(value))
          }}
        </select>)(_ % _)) %
          ("onchange" -> (jsFunc match {
            case Full(f) => {
              JsCrVar(key, JsRaw("this")) & deferCall(raw(funcName, key), f)
            }
            case _ => {
              makeAjaxCall(raw(funcName, "this"))
            }
          }))
    }
  }
  case class ReplaceOptionsMultiSelect(select: String, opts: List[(String, String)], dflt: Seq[String]) extends JsCmd {
    def toJsCmd = """var x=document.getElementById(""" + select.encJs + """);
    if (x) {
    while (x.length > 0) {x.remove(0);}
    var y = null;
                                                                        """ +
      opts.map {
        case (value, text) =>
          "y=document.createElement('option'); " +
            "y.text = " + text.encJs + "; " +
            "y.value = " + value.encJs + "; " +
            (if (dflt.contains(value)) "y.selected = true; " else "") +
            " try {x.add(y, null);} catch(e) {if (typeof(e) == 'object' && typeof(e.number) == 'number' && (e.number & 0xFFFF) == 5){ x.add(y,x.options.length); } } "
      }.mkString("\n")+"};"

  }
  def replaceAllChannels( channels : Seq[(String,String)], prefix : String, numberOfPhases : Int): List[JsCmd] = {
    val channel_options = channels.map{ ch => (ch,ch)}
    println()
    channels.foreach(println)

    if(numberOfPhases >1){
      val elements = List("_1","_2","_3").map{ element => prefix+element}
      return List(
        ReplaceOptions(elements(2), channels.toList, Box.legacyNullTest(channels(2)._1.toString)),
        ReplaceOptions(elements(1), channels.toList, Box.legacyNullTest(channels(1)._1.toString)),
        ReplaceOptions(elements(0), channels.toList, Box.legacyNullTest(channels(0)._1.toString))
      )
    }else{
      return List(ReplaceOptions(prefix+"_1", channels.toList, Box.legacyNullTest(channels(0)._1.toString)))
    }
  }
}

object MSHtml extends MSHtml