package code.snippet

import code.model._
import net.liftweb.http.SHtml.ElemAttr._
import net.liftweb.http.{RequestVar, S, SHtml}
import net.liftweb.json.JsonAST._
import net.liftweb.json.Printer._
import net.liftweb.util.Helpers._
import net.liftweb.json.JsonDSL._
import org.apache.kafka.clients.producer._
import net.liftweb.json.Serialization.write
import scala.xml.Text

/**
  * CRUD for Communication Device Class
  */
object CommDeviceOps {
  implicit val formats = net.liftweb.json.DefaultFormats
    object CommDeviceVar extends RequestVar[CommunicationDevice](CommunicationDevice.createRecord)

  def processSubmit() = {
    println(CommDeviceVar)
    CommDeviceVar.is.save()
    S.seeOther("/system/products/list_comm_device")
  }

  def list()  ={
    val commDevices = CommunicationDevice.findAll("Type","communicationDevice")
    "#commDeviceList" #> commDevices.map{ commDevice =>
      ".commDeviceName" #>  commDevice.name.value &
        ".actions *" #> {SHtml.link("/system/products/edit_comm_device?action=View", () => CommDeviceVar(commDevice), Text("View")) ++ Text(" ") ++
          SHtml.link("/system/products/edit_comm_device?action=Edit", () => CommDeviceVar(commDevice), Text("Edit")) ++ Text(" ") ++
          SHtml.link("/system/products/delete_comm_device", () => CommDeviceVar(commDevice), Text("Delete"))}
    }
  }

  def create() = {
    val commDevice = CommDeviceVar.is
    "#hidden" #> SHtml.hidden(() => CommDeviceVar(commDevice)) &
      "#name" #> SHtml.text(CommDeviceVar.is.name.value,name=>{CommDeviceVar.is.name(name)} ) &
     "#price" #> SHtml.number(CommDeviceVar.is.price._1.Amount.value.toDouble ,(p: Double)=>{},0.0,math.pow(2,52),0.001)&
      "#ssid" #> SHtml.text(CommDeviceVar.is.wifi_ssid.value, W_ssid => CommDeviceVar.is.wifi_ssid(W_ssid) ) &
      "#password"  #> SHtml.password(CommDeviceVar.is.wifi_password.value, W_pass => CommDeviceVar.is.wifi_password(W_pass) ) &
      "#submit" #> SHtml.onSubmitUnit(processSubmit)
  }

  def edit() = {

    if ( ! CommDeviceVar.set_?)
      S.redirectTo("/system/products/list_comm_device")

    val params = S.request.map(_.request.param("action")) openOr ("Undefined")
    val action = params.asInstanceOf[List[String]].headOption.getOrElse("Undefined action")
    val vieworedit = action match {
      case "Edit" =>
        pairToBasic("enabled","disabled")
      case _ =>
        pairToBasic("disabled","disabled")
    }
    val commDevice = CommDeviceVar.is
    var Ports : List[String] = List()
    var IP : List[String] = List()

    def addPort (q : String) = {
       Ports = q::Ports
    }

    def addIP(p : String) = {
       IP = p::IP
    }

    def processSubmit() = {
      if (action == "Edit") {
        println(Ports)
        println(IP)
        var meterConfigs_ = List[MeterConfigs]()
        S.params("port").foreach(addPort(_))
        S.params("ip").foreach(addIP(_))
        println(S.params("ip"))
        println(S.params("port"))
        Ports.zip(IP).map{ l =>
          if (l._1.nonEmpty && l._2.nonEmpty){
            val config = MeterConfigs.createRecord.Port(l._1.toInt).IP(l._2)
            meterConfigs_ = config :: meterConfigs_
          }
        }
        CommDeviceVar.is.meterConfigs(meterConfigs_)
        CommDeviceVar.is.save()
        val jsonObject = ("topic"->"meterConfigs") ~("meterConfigs"->CommDeviceVar.is.meterConfigs.value.map{c => c.asJValue}) ~
          ("id" ->CommDeviceVar.is.name.value) ~ ("Wifi_ssid" -> CommDeviceVar.is.wifi_ssid.value) ~ ("Wifi_password" -> CommDeviceVar.is.wifi_password.value)

        println(pretty(render(jsonObject)))
        println("sending configuration")
        try{
          sendConfiguration(jsonObject,(CommDeviceVar.is.name.value))
        }catch {
          case e:Exception => println(" configuration not sent")
        }
      }
      S.redirectTo("/system/products/list_comm_device")
    }
    "#hidden" #> SHtml.hidden(() => CommDeviceVar(commDevice)) &
      ".panel-title"  #> Text(action + " Device " + CommDeviceVar.name.value) &
      "#name" #> SHtml.text(CommDeviceVar.is.name.value, ( name : String )=> CommDeviceVar.is.name(name), vieworedit ) &
      "#ssid" #> SHtml.text(CommDeviceVar.is.wifi_ssid.value, ( W_ssid : String )=> CommDeviceVar.is.wifi_ssid(W_ssid), vieworedit ) &
      "#password"  #> SHtml.password(CommDeviceVar.is.wifi_password.value, ( W_pass : String )=> CommDeviceVar.is.wifi_password(W_pass), vieworedit ) &
      "#lines *" #>  { CommDeviceVar.is.meterConfigs.value.zipWithIndex.map { case(line,i) =>
        ".port" #> SHtml.number(line.Port.value, (r:Int) => {addPort(r.toString)},0,math.pow(2,52).toInt,vieworedit)&
        ".ip" #> SHtml.text(line.IP.value, ( IP : String )=>addIP(IP.toString), vieworedit)
      } } &
      "#submit" #> SHtml.onSubmitUnit(processSubmit)
  }

  def delete () = {
    val commDevice  = CommDeviceVar.is
    def deleteCommDevice(){
      commDevice.delete_!
    }
    "#name" #> CommDeviceVar.is.name &
      "#yes" #> SHtml.link("/system/products/list_comm_device", () =>{
        deleteCommDevice() }, Text("Delete")) &
      "#no" #> SHtml.link("/system/products/list_comm_device", () =>{ }, Text("Cancel"))
  }

  def sendConfiguration(jsonObject:JObject,Dest:String){
    val message = new ProducerRecord[String,String](server.topic,write(jsonObject))
    server.producer.send(message)
    println("configuration has been sent")
  }
}
