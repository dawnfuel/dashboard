package code.snippet


import org.quartz.CronScheduleBuilder._

import org.quartz.JobBuilder.newJob
import org.quartz.TriggerBuilder.newTrigger
import javax.servlet.ServletContextEvent
import javax.servlet.ServletContextListener
import org.quartz.Scheduler
import org.quartz.SchedulerException
import org.quartz.impl.StdSchedulerFactory
/**
  * Created by idarlington on 2/11/16.
  */





class QuartzListener extends ServletContextListener {

  var scheduler: Scheduler = null

  override def contextInitialized(servletContext: ServletContextEvent) {
    println("Context Initialized")
    try {
      val jd = newJob(classOf[ScheduleTaskHelper]).withIdentity("job1","group1").build();
      val tr = newTrigger()
        .withIdentity("trigger3", "group1")
        .withSchedule(cronSchedule("0 0 0 * * ?"))
        .forJob("job1","group1")
        .build();
      scheduler = new StdSchedulerFactory().getScheduler
      scheduler.start()
      scheduler.scheduleJob(jd, tr)
    } catch {
      case e: SchedulerException => e.printStackTrace()
    }
  }

  override def contextDestroyed(servletContext: ServletContextEvent) {
    println("Context Destroyed")
    try {
      scheduler.shutdown()
    } catch {
      case e: SchedulerException => e.printStackTrace()
    }
  }
}
