package code.snippet

import java.util.Date

import code.model._
import com.mongodb.QueryBuilder
import org.bson.types.ObjectId
import org.joda.time.DateTime
import org.quartz.{JobExecutionContext, Job}

import scala.util.parsing.json.JSON
import scalaj.http.{Http, HttpRequest}

/**
  * Created by idarlington on 2/12/16.
  */

class ScheduleTaskHelper extends Job {

  def execute(arg0: JobExecutionContext) {
    println("started subscription payment")
    // function to perform subscription payment

    val fixedAmountPerProbe = 1000000

    // get probes whose subscription has expired
    val  d = new Date(); // or make a date out of a string...
    val dquery = QueryBuilder.start().put("dataExpiry").lessThanEquals(d).get()
    val paidProbes =  Probe.findAll(dquery).map(p => (p.id.toString -> p.ConfigID_FK.toString) )

    if (paidProbes.isEmpty) {
      println("No probes with data expiry")
    } else {

      for (rawWord <- paidProbes) {

        //get userId, Email &  authorization Code from last payment
        val currentUserID = Configuration.findAll("_id", rawWord._2)(0).AdminID_FK.toString()
        val userDetails = User.findAll("_id", new ObjectId(currentUserID)).map(p => (p.email.toString -> p.authorization.toString))

        if (!userDetails.head._2.isEmpty) {
          // if user has paid formerly
          val request: HttpRequest = Http("https://api.paystack.co/transaction/charge_authorization").postForm(Seq("email" -> userDetails.head._1, "authorization_code" -> userDetails.head._2, "amount" -> fixedAmountPerProbe.toString)).header("Authorization", "Bearer sk_live_cab5dd09381c3caa7e88727c338b9a4414d2a0ec").timeout(connTimeoutMs = 50000, readTimeoutMs = 50000)
          val response = request.asString
          println(response)
          val valid = response.isSuccess
          if (valid) {
            //if payment was succesful

            val status = JSON.parseFull(response.body)
            val obj = status.get
            val theMap = obj.asInstanceOf[Map[String, _]]
            val amount = theMap.get("data").get.asInstanceOf[Map[String, _]].get("amount").get.toString.toDouble
            val ref = theMap.get("data").get.asInstanceOf[Map[String, _]].get("reference").get.toString

            if (amount >= fixedAmountPerProbe) {
              GRIT_Invoice.createRecord.Amount(10000).reference(ref).save()
              val date = Probe.findAll("_id", rawWord._1)(0).dataExpiry.toString
              val extended = new DateTime(date).plusMonths(1)
              Probe.findAll("_id", rawWord._1)(0).dataExpiry(extended.toDate).save()
            }
          }
        }
      }
    }
  }
}