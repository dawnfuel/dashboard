package code.snippet

import java.util.{Calendar, Date}
import code.model
import code.model._
import com.mongodb.QueryBuilder
import org.bson.types.ObjectId
import net.liftweb.common.Box
import net.liftweb.http.SHtml._
import net.liftweb.http.js.JsCmds.{After, ReplaceOptions}
import net.liftweb.http.js.{JE, JsCmd}
import net.liftweb.http.{NamedCometActorSnippet, S, SHtml}
import scala.xml.{Text, Elem, NodeSeq}
import net.liftweb._
import http._
import js._
import JsCmds._
import net.liftweb._
import util._
import Helpers._

import net.liftweb.json.JsonDSL._
import net.liftweb.json.JsonAST._
import net.liftweb.json.Printer._
import net.liftweb.json.JObject

class homepageops {

  var (selectedConf,selectedProbe,selectedUserID,selectedSource)=("","","","")

  var start_time = ""
  var stop_time = ""
  def start : NodeSeq = { <span id="start">{start_time}</span> }
  def stop : NodeSeq = { <span id="stop">{stop_time}</span> }
  def configid : NodeSeq = { <span id="confid">{selectedconf}</span> }
  def probeid : NodeSeq = { <span id="probeid">{selectedprobe}</span> }

  def manOf[T: Manifest](t: T): Manifest[T] = manifest[T]

  var selectedconf = ""
  var selectedprobe = ""
  var selectedConsumerGroup = ""
  var selectedConfGroup = ""



///Bind to quick time filter an triger reload on select
  def todayT():JsCmd={
    changeDefaultWindowTime("today")
    JE.JsRaw("""$("#quick").trigger('winChange',["%s"]);""".format(timeAndMsg("today"))).cmd
  }
  def this_monthT():JsCmd = {
    changeDefaultWindowTime("thismonth")
    JE.JsRaw("""$("#quick").trigger('winChange',["%s"]);""".format(timeAndMsg("thismonth"))).cmd
  }
  def this_yearT():JsCmd = {
    changeDefaultWindowTime("thisyear")
    JE.JsRaw("""$("#quick").trigger('winChange',["%s"]);""".format(timeAndMsg("thisyear"))).cmd
  }
  def this_weekT():JsCmd = {
    changeDefaultWindowTime("thisweek")
    JE.JsRaw("""$("#quick").trigger('winChange',["%s"]);""".format(timeAndMsg("thisweek"))).cmd
  }
  def foreverT():JsCmd={
    changeDefaultWindowTime("forever")
    JE.JsRaw("""$("#quick").trigger('winChange',["%s"]);""".format(timeAndMsg("forever"))).cmd
  }
  def last_15_minT():JsCmd = {
    changeDefaultWindowTime("last15min")
    JE.JsRaw("""$("#quick").trigger('winChange',["%s"]);""".format(timeAndMsg("last15min"))).cmd
  }
  def last_30_minT():JsCmd = {
    changeDefaultWindowTime("last30min")
    JE.JsRaw("""$("#quick").trigger('winChange',["%s"]);""".format(timeAndMsg("last30min"))).cmd
  }
  def last_1_hrT():JsCmd = {
    changeDefaultWindowTime("last1hr")
    JE.JsRaw("""$("#quick").trigger('winChange',["%s"]);""".format(timeAndMsg("last1hr"))).cmd
  }
  def last_4_hrT():JsCmd = {
    changeDefaultWindowTime("last4hr")
    JE.JsRaw("""$("#quick").trigger('winChange',["%s"]);""".format(timeAndMsg("last4hr"))).cmd
  }
  def last_12_hrT():JsCmd = {
    changeDefaultWindowTime("last12hr")
    JE.JsRaw("""$("#quick").trigger('winChange',["%s"]);""".format(timeAndMsg("last12hr"))).cmd
  }
  def last_24_hrT():JsCmd = {
    changeDefaultWindowTime("last24hr")
    JE.JsRaw("""$("#quick").trigger('winChange',["%s"]);""".format(timeAndMsg("last24hr"))).cmd
  }
  def last_7_daysT():JsCmd = {
    changeDefaultWindowTime("last7days")
    JE.JsRaw("""$("#quick").trigger('winChange',["%s"]);""".format(timeAndMsg("last7days"))).cmd
  }
  def last_30_daysT():JsCmd = {
    changeDefaultWindowTime("last30days")
    JE.JsRaw("""$("#quick").trigger('winChange',["%s"]);""".format(timeAndMsg("last30days"))).cmd
  }
  def last_60_daysT():JsCmd = {
    changeDefaultWindowTime("last60days")
    JE.JsRaw("""$("#quick").trigger('winChange',["%s"]);""".format(timeAndMsg("last60days"))).cmd
  }
  def last_90_daysT():JsCmd = {
    changeDefaultWindowTime("last90days")
    JE.JsRaw("""$("#quick").trigger('winChange',["%s"]);""".format(timeAndMsg("last90days"))).cmd
  }
  def last_6_monthT():JsCmd = {
    changeDefaultWindowTime("last6month")
    JE.JsRaw("""$("#quick").trigger('winChange',["%s"]);""".format(timeAndMsg("last6month"))).cmd
  }
  def last_1_yearT():JsCmd = {
    changeDefaultWindowTime("last1year")
    JE.JsRaw("""$("#quick").trigger('winChange',["%s"]);""".format(timeAndMsg("last1year"))).cmd
  }
  def last_2_yearT():JsCmd = {
    changeDefaultWindowTime("last2year")
    JE.JsRaw("""$("#quick").trigger('winChange',["%s"]);""".format(timeAndMsg("last2year"))).cmd
  }
  def last_5_yearT():JsCmd = {
    changeDefaultWindowTime("last5year")
    JE.JsRaw("""$("#quick").trigger('winChange',["%s"]);""".format(timeAndMsg("last5year"))).cmd
  }

  //////////////////// not saved
  def yesterdayT():JsCmd = {
    val (year, month, day, hour, minute,second) = currentTimeElem()
    val startT = new Date().getTime - myCalendarMillsec(year,month,day-1,0,0,0)
    val stopT = myCalendarMillsec(year,month,day,0,0,0)
    println("\n\nstart : "+startT+"\n\nstop : "+stopT)
    JE.JsRaw("""$("#quick").trigger('winChange',["%s","%s"]);""".format(startT,stopT)).cmd
  }
  def day_before_yesterdayT():JsCmd = {
    val (year, month, day, hour, minute,second) = currentTimeElem()
    val startT = new Date().getTime - myCalendarMillsec(year,month,day-2,0,0,0)
    val stopT = myCalendarMillsec(year,month,day-1,0,0,0)
    JE.JsRaw("""$("#quick").trigger('winChange',["%s","%s"]);""".format(startT,stopT)).cmd
  }
  def previous_weekT():JsCmd = {

    val (year, month, day, hour, minute,second) = currentTimeElem()
    val stopT = new Date().getTime - new Date().getTime % (60*60*1000*24*(Calendar.getInstance().get(Calendar.DAY_OF_WEEK)))
    val startT = new Date().getTime - stopT + (60*60*24*7*1000)
    println("\n\n\nweek past  "+startT+" : "+stopT)
    JE.JsRaw("""$("#quick").trigger('winChange',["%s","%s"]);""".format(startT,stopT)).cmd
  }
  def previous_monthT():JsCmd = {
    val (year, month, day, hour, minute,second) = currentTimeElem()
    val startT = new Date().getTime - myCalendarMillsec(year,month-1,1,0,0,0)
    val stopT = myCalendarMillsec(year,month,1,0,0,0)
    JE.JsRaw("""$("#quick").trigger('winChange',["%s","%s"]);""".format(startT,stopT)).cmd
  }
  def previous_yearT():JsCmd = {
    val (year, month, day, hour, minute,second) = currentTimeElem()
    val startT = new Date().getTime - myCalendarMillsec(year-1,1,1,0,0,0)
    val stopT = myCalendarMillsec(year,1,1,0,0,0)
    JE.JsRaw("""$("#quick").trigger('winChange',["%s","%s"]);""".format(startT,stopT)).cmd
  }

  def f(v:Generator) = List(v.Type.toString)
  def getSources(configIDs:List[String]):List[Generator]={
    var g:List[Generator] = List()
    for(p <- configIDs){
      g = g ::: Generator.findAll("ConfigID_FK",p)
    }
    return g
  }

  val currentUserID = User.currentUserId.headOption.getOrElse("")
  val currentUserEmail = User.find("_id",new ObjectId(currentUserID)).headOption.getOrElse(User).email.toString()
  val blockUserAccess = User.find("_id",new ObjectId(currentUserID)).headOption.getOrElse(User).BlockAccess.value
  var configs:List[(String,String)] = Configuration.findAll("UserID_FKs", currentUserID).map(p => (p.id.toString -> p.configurationName.toString))
  val configGroups :List[(String,String)] = ConfigGroup.findAll("adminID_FK",currentUserID).map(p => (p.id.toString -> p.groupName.toString))
  var users:List[(String,String)] = List(("n/a","n/a"))    //to hold users
  var consumerGroups = List[(String,String)]()
  val userPowerSources = getSources(Configuration.findAll("UserID_FKs", currentUserID).map(p => (p.id.toString))).flatMap(p => f(p))

  def getUserStuff = {
    selectedUserID =currentUserID
    var timeWindowString = S.get("timeWindow").toOption.getOrElse("") //get time out of session variable
    var realTimeWindowString = S.get("").toOption.getOrElse("")
    if(timeWindowString.isEmpty)timeWindowString = "today"  //Default time to today if it is not set
    if(realTimeWindowString.isEmpty)realTimeWindowString="300"
    var timeWindowInMil = timeAndMsg(timeWindowString)
    println("set time window")

    /** set users if loggedIn user is admin and put current user first */
    if(userHasRole("admin")){
      println("\n\n C-user : " +currentUserID+" , " +currentUserEmail)
      users = getAllUsers().filter(_ != (currentUserID,currentUserEmail))
      users = (currentUserID,currentUserEmail) :: users
    }

   /** check if the configuration list is empty */
     if (configs.isEmpty) {
      configs = List(("n/a","n/a"))
    }else{
      consumerGroups = PowerConsumerGroup.findAll(("ConfigID_FK" -> configs.head._1)~("Type" -> ("Estate"))).map(p => (p.id.toString -> p.Name.toString))
    }
      println("blockUserAccess: " + blockUserAccess.toString + " " + currentUserEmail.toString)
      selectedconf = configs.head._1
      "#today [onclick]" #> SHtml.ajaxInvoke(todayT)&
      "#this_week [onclick]" #> SHtml.ajaxInvoke(this_weekT)&
      "#this_month [onclick]" #> SHtml.ajaxInvoke(this_monthT)&
      "#this_year [onclick]" #> SHtml.ajaxInvoke(this_yearT)&
      "#forever [onclick]" #> SHtml.ajaxInvoke(foreverT)&
      "#last_15_min [onclick]" #> SHtml.ajaxInvoke(last_15_minT)&
      "#last_30_min [onclick]" #> SHtml.ajaxInvoke(last_30_minT)&
      "#last_1_hr [onclick]" #> SHtml.ajaxInvoke(last_1_hrT)&
      "#last_4_hr [onclick]" #> SHtml.ajaxInvoke(last_4_hrT)&
      "#last_12_hr [onclick]" #> SHtml.ajaxInvoke(last_12_hrT)&
      "#last_24_hr [onclick]" #> SHtml.ajaxInvoke(last_24_hrT)&
      "#last_7_days [onclick]" #> SHtml.ajaxInvoke(last_7_daysT)&
      "#last_30_days [onclick]" #> SHtml.ajaxInvoke(last_30_daysT)&
      "#last_60_days [onclick]" #> SHtml.ajaxInvoke(last_60_daysT)&
      "#last_90_days [onclick]" #> SHtml.ajaxInvoke(last_90_daysT)&
      "#last_6_month [onclick]" #> SHtml.ajaxInvoke(last_6_monthT)&
      "#last_1_year [onclick]" #> SHtml.ajaxInvoke(last_1_yearT)&
      "#last_2_year [onclick]" #> SHtml.ajaxInvoke(last_2_yearT)&
      "#last_5_year [onclick]" #> SHtml.ajaxInvoke(last_5_yearT)&
      "#yesterday [onclick]" #> SHtml.ajaxInvoke(yesterdayT)&
      "#day_before_yesterday [onclick]" #> SHtml.ajaxInvoke(day_before_yesterdayT)&
      "#previous_week [onclick]" #> SHtml.ajaxInvoke(previous_weekT)&
      "#previous_month [onclick]" #> SHtml.ajaxInvoke(previous_monthT)&
      "#previous_year [onclick]" #> SHtml.ajaxInvoke(previous_yearT)&
      "#hiddenTimeWindow" #> SHtml.text(timeWindowInMil, timeWindowInMil= _)&
      "#blockUserAccess [value]" #> blockUserAccess.toString &
      "#realTimeWindow" #> SHtml.text(realTimeWindowString,realTimeWindowString=_ )&
      "#users" #> ajaxUntrustedSelect(users, Box.legacyNullTest(users.head._1), { s => After(0, selectUser(s)) })&
      "#consumerGroup" #> ajaxUntrustedSelect(consumerGroups, Box.legacyNullTest(consumerGroups.headOption.getOrElse(("n/a","n/a"))._1), { s => After(0, selectConsumerGroup(s)) })&
      "#config" #> customSelect() &
      "#configGroup" #>  ajaxSelect(configGroups, Box.legacyNullTest(configGroups.headOption.getOrElse(("n/a","n/a"))._1), { s => After(200, selectConfGroup(s)) })
  }


  def customSelect():Elem = {
    if(userHasRole("admin")){
      return ajaxUntrustedSelect(configs, Box.legacyNullTest(configs.head._1), { s => After(200, selectConf(s)) })
    }else {
      return ajaxSelect(configs, Box.legacyNullTest(configs.head._1), { s => After(200, selectConf(s)) })
    }
  }

  private def selectUser(userID: String): List[JsCmd] = {
    var selectedUserConfigs:List[(String,String)] = Configuration.findAll("UserID_FKs", userID).map(c => (c.id.toString -> c.configurationName.toString))
    if(selectedUserConfigs.isEmpty){
      selectedUserConfigs = List(("n/a","n/a"))
    }
    var selectedUserConfigGroups = ConfigGroup.findAll("adminID_FK",userID).map(p => (p.id.toString(),p.groupName.toString()))
    if(selectedUserConfigGroups.isEmpty) selectedUserConfigGroups = List(("n/a","n/a"))
    var selectedConsumerGroups =  PowerConsumerGroup.findAll(("ConfigID_FK" -> selectedUserConfigs.head._1)~("Type" -> ("Estate"))).map(p => (p.id.toString, p.Name.toString))
    if (selectedConsumerGroups.isEmpty) selectedConsumerGroups = List(("n/a","n/a"))
    println("\n\n\n")
    println("selectedConsumerGroups  : "+selectedConsumerGroups.head._1.toString)
    println ()
    return List(
      ReplaceOptions("config", selectedUserConfigs, Box.legacyNullTest(selectedUserConfigs.head._1)),
      ReplaceOptions("configGroup", selectedUserConfigGroups, Box.legacyNullTest(selectedUserConfigGroups.head._1)),
      ReplaceOptions("consumerGroup", selectedConsumerGroups, Box.legacyNullTest(selectedConsumerGroups.head._1)),
      JE.JsRaw("""$('#config').trigger('change');""").cmd
    )
  }

  private def selectConsumerGroup (consGroup : String): JsCmd = {
    selectedConsumerGroup = consGroup
  }

  private def selectConf(config: String) : JsCmd = {
    selectedconf = config
    var consumer_groups =  PowerConsumerGroup.findAll(("ConfigID_FK" -> config)~("Type" -> ("Estate"))).map(p => (p.id.toString , p.Name.toString))
    if (consumer_groups.isEmpty ) consumer_groups  = List(("n/a","n/a"))
    return ReplaceOptions("consumerGroup", consumer_groups, Box.legacyNullTest(consumer_groups.head._1))
  }

  private def selectConfGroup (configGroup: String) : JsCmd = {
    selectedConfGroup = configGroup
  }

  private def changeDefaultWindowTime(t : String){
    S.set("timeWindow",t)
  }

  /**
    * Calculate time delta and appropriate display msg
    * @param timeWindowString
    * @return
    */
  def timeAndMsg(timeWindowString:String):String = {
    val (year, month, day, hour, minute,second) = currentTimeElem()
    var timeWindowInMil = ""
    timeWindowString match {

      case "today" => {
        timeWindowInMil = (new Date().getTime - myCalendarMillsec(year,month,day,0,0,0)).toString
      }
      case "thisweek" => {
        timeWindowInMil =  (new Date().getTime % (60*60*1000*24*(Calendar.getInstance().get(Calendar.DAY_OF_WEEK)))).toString
      }
      case "thismonth" => {
        timeWindowInMil = (new Date().getTime - myCalendarMillsec(year,month,1,0,0,0)).toString
      }
      case "thisyear" => {
        timeWindowInMil = (new Date().getTime - myCalendarMillsec(year,1,1,0,0,0)).toString
      }
      case "forever" =>{
        timeWindowInMil = new Date().getTime.toString
      }
      case "last15min" => {
        timeWindowInMil = "900000"
      }
      case "last30min" => {
        timeWindowInMil = "1800000"
      }
      case "last1hr" => {
        timeWindowInMil = "3600000"
      }
      case "last4hr" => {
        timeWindowInMil = "14400000"
      }
      case "last12hr" => {
        timeWindowInMil = "43200000"
      }
      case "last24hr" => {
        timeWindowInMil = "86400000"
      }
      case "last7days" => {
        timeWindowInMil = "604800000"
      }
      case "last30days" => {
        timeWindowInMil = "2592000000"
      }
      case "last60days" => {
        timeWindowInMil = "5184000000"
      }
      case "last90days" => {
        timeWindowInMil = "7776000000"
      }
      case "last6month" => {
        timeWindowInMil = "15778476000"
      }
      case "last1year" => {
        timeWindowInMil = (new Date().getTime - myCalendarMillsec(year-1,1,1,0,0,0)).toString
      }
      case "last2year" => {
        timeWindowInMil = (new Date().getTime - myCalendarMillsec(year-2,1,1,0,0,0)).toString
      }
      case "last5year" => {
        timeWindowInMil = (new Date().getTime - myCalendarMillsec(year-5,1,1,0,0,0)).toString
      }
      case _ => {
        timeWindowInMil = (new Date().getTime - myCalendarMillsec(year,month,day,0,0,0)).toString
      }
    }
    return timeWindowInMil
  }

  /**
    *
    * @param year
    * @param month
    * @param day
    * @param hour
    * @param minute
    * @param second
    * @return
    */
  def myCalendarMillsec(year:Int, month:Int, day:Int, hour:Int, minute:Int,second:Int):Long={
    val date:Calendar = Calendar.getInstance()
    date.set(Calendar.YEAR, year)
    date.set(Calendar.MONTH, month-1)
    date.set(Calendar.DAY_OF_MONTH, day)
    date.set(Calendar.HOUR_OF_DAY, hour)
    date.set(Calendar.MINUTE, minute)
    date.set(Calendar.SECOND, second)

    return date.getTimeInMillis
  }
  def currentTimeElem():(Int,Int,Int,Int,Int,Int)={
    val now = Calendar.getInstance()
    return (now.get(Calendar.YEAR),(now.get(Calendar.MONTH) + 1),now.get(Calendar.DATE),now.get(Calendar.HOUR),now.get(Calendar.MINUTE),now.get(Calendar.SECOND))
  }

  def getAllUsers():List[(String,String)] = {
        return User.findAll.map(u=> (u.id.toString(), u.email.toString()))
      }

  /**
    *
    * @param configId
    * @return
    */
  def consumerOnly(configId: String) : Boolean = {
    val group = Configuration.findAll("_id",configId).headOption.getOrElse(Configuration).ConfigGroupID_FK.value
    val sources = List("inverter", "generator", "grid").toSet
    (userPowerSources.toSet.intersect(sources).isEmpty  && group.isEmpty)
  }

  def getAllConfigs():List[(String,String)] = {
     return Configuration.findAll.map(c => (c.id.toString(), c.configurationName.toString()))
  }

  def userHasRole(role:String):Boolean ={
    return User.currentUser.openOr(User).roles.names.contains(role)
  }

  def passwordRedirect() : NodeSeq ={
    if (User.currentUser.openOr(User).password.value.isEmpty) {
      S.redirectTo("/settings/profile")
      NodeSeq.Empty
    } else {
      NodeSeq.Empty
    }
  }

  def getAdminConfigs(userID:String):List[(String,String)]={
      return ("all", "all configs") :: Configuration.findAll("UserID_FKs", userID).map(p => (p.id.toString -> p.configurationName.toString))
  }

  def multiple(xhtml: NodeSeq): NodeSeq =
    if (configs.size > 1 || userHasRole("admin")) xhtml else NodeSeq.Empty

  def single(xhtml: NodeSeq): NodeSeq =
    if (configs.size == 1) xhtml else NodeSeq.Empty

  def available(xhtml: NodeSeq): NodeSeq =
    if (configs.size >= 1) xhtml else NodeSeq.Empty

  def isConfigAdmin(xhtml: NodeSeq): NodeSeq =
    if (Configuration.findAll("AdminID_FK",currentUserID).nonEmpty || userHasRole("admin")) xhtml else NodeSeq.Empty

  def hasConfigGroup(xhtml: NodeSeq): NodeSeq =
    if (!configGroups.isEmpty || userHasRole("admin")) xhtml else NodeSeq.Empty

  def hasConsumerGroup(xhtml: NodeSeq): NodeSeq =
    if (!consumerGroups.isEmpty || userHasRole("admin")) xhtml else NodeSeq.Empty

  def hasGenerator(xhtml: NodeSeq): NodeSeq =
    if (userPowerSources.exists(_ == "generator") || userHasRole("admin")) xhtml else NodeSeq.Empty

  def hasInverter(xhtml: NodeSeq): NodeSeq =
    if (userPowerSources.exists(_ == "inverter") || userHasRole("admin")) xhtml else NodeSeq.Empty

  def hasConsumerOnly(xhtml: NodeSeq): NodeSeq = {
    val group = Configuration.findAll("_id",selectedconf).headOption.getOrElse(Configuration).ConfigGroupID_FK.value
    val sources = List("inverter", "generator", "grid").toSet
    if(userHasRole("admin")) xhtml
      else if (userPowerSources.toSet.intersect(sources).isEmpty  && group.isEmpty) NodeSeq.Empty
      else xhtml
  }
}

object AddRealtimeUser extends NamedCometActorSnippet {
  def cometClass = "RealtimeComet"
  def name = User.currentUserId.headOption.getOrElse("")
  println("\nNAMED "+name)
}

