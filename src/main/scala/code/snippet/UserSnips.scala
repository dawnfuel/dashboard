package code
package snippet

import code.config.{Site, SmtpMailer}
import code.model.{Address, Configuration, LoginCredentials, User}
import net.liftweb.http.provider.HTTPCookie

import scala.util.parsing.json.JSON
import scala.xml._
import net.liftweb._
import common._
import net.liftweb.http._
import http.js.JsCmd
import http.js.JsCmds._
import util._
import Helpers._
import net.liftmodules.extras.SnippetHelper
import net.liftmodules.mongoauth.LoginRedirect
import net.liftmodules.mongoauth.model.ExtSession
import javax.mail.internet.MimeMessage

import net.liftweb.util.Mailer
import net.liftweb.util.Mailer._
import org.bson.types.ObjectId
import org.joda.time.DateTime

import scalaj.http.{Http, HttpRequest}


sealed trait UserSnippet extends SnippetHelper with Loggable {

  protected def user: Box[User]

  protected def serve(snip: User => NodeSeq): NodeSeq =
    (for {
      u <- user ?~ "User not found"
    } yield {
      snip(u)
    }): NodeSeq

  protected def serve(html: NodeSeq)(snip: User => CssSel): NodeSeq =
    (for {
      u <- user ?~ "User not found"
    } yield {
      snip(u)(html)
    }): NodeSeq

  def header(xhtml: NodeSeq): NodeSeq = serve { user =>
    <div id="user-header">
      <i class="fa fa-user"></i>
      <h3>{name(xhtml)}</h3>
    </div>
  }

  //  def gravatar(xhtml: NodeSeq): NodeSeq = {
  //    val size = S.attr("size").map(toInt) openOr Gravatar.defaultSize.vend
  //
  //    serve { user =>
  //      Gravatar.imgTag(user.email.get, size)
  //    }
  //  }

  def username(xhtml: NodeSeq): NodeSeq = serve { user =>
    Text(user.username.get)
  }

  def name(xhtml: NodeSeq): NodeSeq = serve { user =>
    if (user.name.get.length > 0)
      Text("%s (%s)".format(user.name.get, user.username.get))
    else
      Text(user.username.get)
  }

  def title(xhtml: NodeSeq): NodeSeq = serve { user =>
    <lift:head>
      <title lift="Menu.title">{"Lift Mongo App: %*% - "+user.username.get}</title>
    </lift:head>
  }
}

object CurrentUser extends UserSnippet {
  protected def user = User.currentUser
}

object ProfileLocUser extends UserSnippet {

  protected def user = Site.profileLoc.currentValue

  import java.text.SimpleDateFormat

  val df = new SimpleDateFormat("MMM d, yyyy")

  def profile(html: NodeSeq): NodeSeq = serve(html) { user =>
    val editLink: NodeSeq =
      if (User.currentUser.filter(_.id.get == user.id.get).isDefined)
        <a href={Site.editProfile.url} class="btn btn-info"><i class="icon-edit icon-white"></i> Edit Your Profile</a>
      else
        NodeSeq.Empty

    //    "#id_avatar *" #> Gravatar.imgTag(user.email.get) &
    "#id_name *" #> <h3>{user.name.get}</h3> &
      "#id_location *" #> user.location.get &
      "#id_whencreated" #> df.format(user.whenCreated.toDate).toString &
      "#id_bio *" #> user.bio.get &
      "#id_editlink *" #> editLink
  }
}

object Logout extends Loggable{
  def render = {
    if(!User.currentUser.openOr(User).roles.names.contains("guest")) {
      val user: User = User.currentUser.headOption.getOrElse("").asInstanceOf[User]
      val tm = S.get("timeWindow").toOption.getOrElse("") +":"+ S.get("realTimeWindow").toOption.getOrElse("")

      user.timeWindow(tm)
      user.validate match {
        case Nil => {
          user.save()
        }
        case errors => println(errors)
      }
    }
    User.logUserOut()
    S.redirectTo("/")
    "#waiting" #> "Redirecting..."
  }
}

object UserLogin extends Loggable {

  def render = {
    // form vars
    var password = ""
    var remember = User.loginCredentials.is.isRememberMe

    def doSubmit(): JsCmd = {
      S.param("email").map(e => {
        val email = e.toLowerCase.trim
        // save the email and remember entered in the session var
        User.loginCredentials(LoginCredentials(email, remember))

        if ( email.length > 0 && password.length > 0) {
          User.findByEmail(email) match {
            case Full(user) if (user.password.isMatch(password)) =>
              logger.debug("pwd matched")
              User.logUserIn(user, true)
              var tm = Array("","")
              try{
                var usertm = user.timeWindow.value.split(":")
                if(! usertm.isEmpty) {
                  if (usertm.length < 2) usertm = usertm :+ " "
                  tm=usertm
                }
              }catch {
                case e:Exception => println("User has no user.timeWindow ")
              }

              S.set("timeWindow",tm(0)) //add history graph window time to session
              S.set("realTimeWindow",tm(1))
              if (remember) User.createExtSession(user.id.get)
              else ExtSession.deleteExtCookie()
              S.redirectTo(LoginRedirect.openOr(Site.home.url))
            case _ =>
              S.error("id_invalid_credential","Username or password incorrect")
              Noop
          }
        }
        else if (email.length <= 0 && password.length > 0) {
          S.error("id_email_err", "Please enter an email")
          Noop
        }
        else if ( password.length <= 0 && email.length > 0) {
          S.error("id_password_err", "Please enter a password")
          Noop
        }
        else {
          S.error("id_email_err", "Please enter an email address")
          Noop
        }
      }) openOr {
        S.error("id_email_err", "Please enter an email address")
        Noop
      }
    }

    "#id_email [value]" #> User.loginCredentials.is.email &
      "#id_password" #> SHtml.password(password, password = _) &
      "name=remember" #> SHtml.checkbox(remember, remember = _) &
      "#id_submit" #> SHtml.hidden(doSubmit)
  }
}

object newLogin{
  def render(in: NodeSeq): NodeSeq = {
    User.isLoggedIn match {
      case true => S.redirectTo("dashboard")
      case false => in
    }

  }
}

object UserTopbar {
  def render = {
    User.currentUser match {
      case Full(user) =>
        <ul class="nav navbar-nav navbar-right" id="user">
          <li class="dropdown" data-dropdown="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              <i class="fa fa-user"></i>
              <span>{user.username.get}</span>
              <b class="caret"></b>
            </a>
            <ul class="dropdown-menu">
              <!--<li data-lift="lackRole?name=guest"><a href={Site.profileLoc.calcHref(user)}>
              <i class="icon-user"></i> Profile</a></li>-->
              <li data-lift="lackRole?name=guest"><a href="/settings/account"><i class="icon-user"></i> Profile</a></li>
              <li><lift:Menu.item name="List Fuel" donthide="true" linktoself="true"><i class="icon-cog"></i> Settings</lift:Menu.item></li>
              <li><lift:Menu.item name="upload update" donthide="true"><i class="icon-off upload"></i> upload update</lift:Menu.item></li>
              <!-- li class="divider"></li -->
              <!--ul class="dropdown-menu"-->
              <li><lift:Menu.item name="Charts" donthide="true" linktoself="true"><i class="icon-cog"></i> Charts</lift:Menu.item></li>
              <li><lift:Menu.item name="Reports" donthide="true" linktoself="true"><i class="icon-cog"></i> Reports</lift:Menu.item></li>
              <li><lift:Menu.item name="Notifications" donthide="true" linktoself="true"><i class="icon-cog"></i> Notifications</lift:Menu.item></li>
              <li><lift:Menu.item name="Export" donthide="true" linktoself="true"><i class="icon-cog"></i> Export</lift:Menu.item></li>
              <li><lift:Menu.item name="Automator" donthide="true" linktoself="true"><i class="icon-cog"></i> Automator</lift:Menu.item></li>
              <li class="divider"></li>
              <li><lift:Menu.item name="About" donthide="true" linktoself="true"><i class="icon-cog"></i> About</lift:Menu.item></li>
              <li><a href="https://gritsystems.freshdesk.com/support/solutions"><i class="icon-cog"></i> Help</a></li>
              <!--<li><lift:Menu.item name="Help" donthide="true" linktoself="true"><i class="icon-cog"></i> Help</lift:Menu.item></li>-->
              <li><lift:Menu.item name="Contact" donthide="true" linktoself="true"><i class="icon-cog"></i> Contact</lift:Menu.item></li>
              <li><lift:Menu.item name="Bills" donthide="true" linktoself="true"><i class="icon-cog"></i> Bills &amp; Payment</lift:Menu.item></li>
              <li><lift:Menu.item name="Demo" donthide="true" linktoself="true"><i class="icon-cog"></i>Demo</lift:Menu.item></li>

              <!--/ul-->
              <li class="divider"></li>
              <li><lift:Menu.item name="Logout" donthide="true"><i class="icon-off"></i> Log Out</lift:Menu.item></li>
            </ul>
          </li>
        </ul>
      case _ if (S.request.flatMap(_.location).map(_.name).filterNot(it => List("Login", "Register").contains(it)).isDefined) =>
        <span>
          <a href="/login" class="btn btn-default navbar-btn navbar-right">Sign In</a>
          <a href="/demo" class="btn btn-default navbar-btn navbar-right demo-button">Demo</a>
        </span>
      case _ => NodeSeq.Empty
    }
  }
}


object SendEmail {
  SmtpMailer.init()
  def send_!(from: String,
             recipient: String,
             subject: String,
             body: String) {
    val mailTypes = List(PlainMailBodyType(body),
      To(recipient),BCC("support@grit.systems"))
    val mailReturn =   Mailer.msgSendImpl (
      From(from),
      Subject(subject),
      mailTypes)
    println("MAIL RETURN : "+mailReturn)
  }

  def sendHTML(from: String, recipient: String, subject: String, richBody: NodeSeq) = {
        val mailReturn = Mailer.sendMail(
          From(from),
          Subject(subject),
          To(recipient),
          BCC("support@grit.systems"),
          XHTMLMailBodyType(richBody)
        )
        println("MAIL RETURN : "+mailReturn)
  }
}

object ProfileOps {
  def redirect () = {
    val eml = S.params("q").headOr("")
    val config = S.params("c").headOr("")
    if(!eml.isEmpty){ login(eml,config)}
    "#selDemo" #> Text("Loading ...")
  }

  def login(email:String,config:String){
    println("\n got email : "+email)
    User.findByEmail(email) match {
      case Full(user) => {
        if (config.nonEmpty) {
          Configuration.find("_id",config) match {
            case Full(conf) => {
              conf.UserID_FKs((user.id.value.toString :: conf.UserID_FKs.value).distinct).save()
            }
            case _ => {}
          }
        }
        val linkExpiry = Option(user.profileLinkExpiry.value.isBeforeNow).getOrElse(true)
        println(linkExpiry)
        println(user.roles.value.contains("admin"))
        println((user.password.value.nonEmpty))
        if (user.roles.value.contains("admin") || (user.password.value.nonEmpty) || linkExpiry ) {
          S.redirectTo("/")
        } else {
          println("trying to login")
          User.logUserIn(user, true)
          User.createExtSession(user.id.get)
          S.redirectTo("/settings/profile")
        }
      }
      case _ => {
        if(config.nonEmpty){
          Configuration.find("_id",config) match {
            case Full(conf) => {
              val userName = email.split("@")(0) + scala.util.Random.nextInt(100)
              val linkExpiry = (new DateTime).plusDays(2)
              val user = User.createRecord.email(email).username(userName).roles(List("user")).profileLinkExpiry(linkExpiry).save()
              conf.UserID_FKs((user.id.value.toString :: conf.UserID_FKs.value).distinct).save()
              User.logUserIn(user, true)
              User.createExtSession(user.id.get)
              S.redirectTo("/settings/profile")
            }
            case _ => {
              S.redirectTo("/")
            }
          }
        }
      }
    }
  }
}

object GettingStarted {
  SmtpMailer.init()
  def render = {

    for {
      r <- S.request if r.post_? // make sure it's a post
      name <- S.param("name")
      address <- S.param("address")
      email <- S.param("email")
      number <- S.param("number")
      plan <- S.param("plan")
    } {
      val msg =
        "Name: " + name + "\n" +
          "Address: " + address + "\n" +
          "Email: " + email + "\n" +
          "Number: " + number + "\n" +
          "Plan: "  + plan + "\n"
      println (name)
      println (address)
      println (email)
      println (number)
      SendEmail.send_!(
        "notify@grit.systems",
        "support@grit.systems",
        "Interested Client",
        msg
      )
    }
    PassThru
  }
}

object ContactUs {
  SmtpMailer.init()
  def render = {

    for {
      r <- S.request if r.post_? // make sure it's a post
      name <- S.param("name")
      email <- S.param("email")
      message <- S.param("message")
      title <- S.param("title")

    } {
      SendEmail.send_!(
        email,
        "support@grit.systems",
        title,
        message
      )
    }
    PassThru
  }
}

object addressVar extends RequestVar[Address](Address.createRecord)
object InterestedUser {
  SmtpMailer.init()
  def randomString(length: Int) = scala.util.Random.alphanumeric.take(length).mkString
  def userLocation( API_KEY:String, IP:String ) :Map[String,_] = {
    val request: HttpRequest = Http("http://api.ipinfodb.com/v3/ip-city" + "/?key=" + API_KEY + "&ip=" + IP + "&format=" + "json")
      .timeout(connTimeoutMs = 10000, readTimeoutMs = 50000)

    val response = request.asString
    val status = JSON.parseFull(response.body)
    val theMap = status.get.asInstanceOf[Map[String,_]]
    return theMap;
  }
  val A = "The Smart Meter that will help you spend less on electricity and start saving money"
  val B = "The Smart Meter that will change your life"
  var lastViewed = A

  def AB_Test () = {

    S.findCookie("AB_TEST") match {
      case Full(x) => {
        val lastValue = S.cookieValue("AB_TEST")
        "#leadText" #> lastValue
      }
      case _ => {
        lastViewed = lastViewed match {
          case A => B
          case B => A
        }
      }
        S.addCookie(HTTPCookie("AB_TEST", lastViewed).setDomain(S.hostName).setPath("/"))
        "#leadText" #> lastViewed
      }
    }

  def render = {
    for {
      r <- S.request if r.post_? // make sure it's a post
      email <- S.param("email")
      ipAddress <- S.param("ip")

    } {

      if (email.nonEmpty) {
        val name = email.split("@")(0)
        val userName = name + scala.util.Random.nextInt(100)
        val location = userLocation("4a11cd54e97c151b6d22d63baf9b4d103a1ee0b1fbcd56574767455dc8bb7329",ipAddress)
        val AB_TEST_Val = S.cookieValue("AB_TEST").openOr("") match {
          case A => "A"
          case B => "B"
          case _ => ""
        }
        User.createRecord.name(name).username(userName).email(email).AB_Campaign(AB_TEST_Val).roles(List("interested")).timezone(location.get("timeZone").get.toString).saveBox()  match {
          case Full(x) => {
            println("true")
            val newUserId = User.findByEmail(email).headOption.get.id.toString
            println(newUserId)
            addressVar.is.UserID_FK(newUserId).latitude(location.get("latitude").get.toString.toDouble).longitude(location.get("longitude").get.toString.toDouble)
              .country(location.get("countryName").get.toString)
              .zone(location.get("timeZone").get.toString)
              .state(location.get("regionName").get.toString)
              .city(location.get("cityName").get.toString)
              .zip(location.get("zipCode").get.toString).save()
            println(location.get("zipCode").get.toString)
          }
          case _=>{
            println("false")
          }
        }

        val msg =
          "Name: " + name + "\n" +
            "Email: " + email + "\n"
        try{
          SendEmail.send_!(
            "notify@grit.systems",
            "ikenna@grit.systems",
            "Interested Client",
            msg
          )
          
          SendEmail.send_!(
            "notify@grit.systems",
            "support@grit.systems",
            "Interested Client",
            msg
          )

          val HtmlMsg = "<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional //EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\"><!--[if IE]><html xmlns=\"http://www.w3.org/1999/xhtml\" class=\"ie\"><![endif]--><!--[if !IE]><!--><html style=\"margin: 0;padding: 0;\" xmlns=\"http://www.w3.org/1999/xhtml\"><!--<![endif]--><head>\n    <meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\" />\n    <!--[if !mso]><!--><meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\" /><!--<![endif]-->\n    <meta name=\"viewport\" content=\"width=device-width\" /><style type=\"text/css\">\n@media only screen and (min-width: 620px){* [lang=x-wrapper] h1{}* [lang=x-wrapper] h1{font-size:26px !important;line-height:34px !important}* [lang=x-wrapper] h2{}* [lang=x-wrapper] h2{font-size:20px !important;line-height:28px !important}* [lang=x-wrapper] h3{}* [lang=x-layout__inner] p,* [lang=x-layout__inner] ol,* [lang=x-layout__inner] ul{}* div [lang=x-size-8]{font-size:8px !important;line-height:14px !important}* div [lang=x-size-9]{font-size:9px !important;line-height:16px !important}* div [lang=x-size-10]{font-size:10px !important;line-height:18px !important}* div [lang=x-size-11]{font-size:11px !important;line-height:19px !important}* div [lang=x-size-12]{font-size:12px !important;line-height:19px !important}* div [lang=x-size-13]{font-size:13px !important;line-height:21px !important}* div [lang=x-size-14]{font-size:14px !important;line-height:21px !important}* div \n[lang=x-size-15]{font-size:15px !important;line-height:23px !important}* div [lang=x-size-16]{font-size:16px !important;line-height:24px !important}* div [lang=x-size-17]{font-size:17px !important;line-height:26px !important}* div [lang=x-size-18]{font-size:18px !important;line-height:26px !important}* div [lang=x-size-18]{font-size:18px !important;line-height:26px !important}* div [lang=x-size-20]{font-size:20px !important;line-height:28px !important}* div [lang=x-size-22]{font-size:22px !important;line-height:31px !important}* div [lang=x-size-24]{font-size:24px !important;line-height:32px !important}* div [lang=x-size-26]{font-size:26px !important;line-height:34px !important}* div [lang=x-size-28]{font-size:28px !important;line-height:36px !important}* div [lang=x-size-30]{font-size:30px !important;line-height:38px !important}* div [lang=x-size-32]{font-size:32px \n!important;line-height:40px !important}* div [lang=x-size-34]{font-size:34px !important;line-height:43px !important}* div [lang=x-size-36]{font-size:36px !important;line-height:43px !important}* div [lang=x-size-40]{font-size:40px !important;line-height:47px !important}* div [lang=x-size-44]{font-size:44px !important;line-height:50px !important}* div [lang=x-size-48]{font-size:48px !important;line-height:54px !important}* div [lang=x-size-56]{font-size:56px !important;line-height:60px !important}* div [lang=x-size-64]{font-size:64px !important;line-height:63px !important}}\n</style>\n    <style type=\"text/css\">\nbody {\n  margin: 0;\n  padding: 0;\n}\ntable {\n  border-collapse: collapse;\n  table-layout: fixed;\n}\n* {\n  line-height: inherit;\n}\n[x-apple-data-detectors],\n[href^=\"tel\"],\n[href^=\"sms\"] {\n  color: inherit !important;\n  text-decoration: none !important;\n}\n.wrapper .footer__share-button a:hover,\n.wrapper .footer__share-button a:focus {\n  color: #ffffff !important;\n}\n.btn a:hover,\n.btn a:focus,\n.footer__share-button a:hover,\n.footer__share-button a:focus,\n.email-footer__links a:hover,\n.email-footer__links a:focus {\n  opacity: 0.8;\n}\n.preheader,\n.header,\n.layout,\n.column {\n  transition: width 0.25s ease-in-out, max-width 0.25s ease-in-out;\n}\n.layout,\n.header {\n  max-width: 400px !important;\n  -fallback-width: 95% !important;\n  width: calc(100% - 20px) !important;\n}\ndiv.preheader {\n  max-width: 360px !important;\n  -fallback-width: 90% !important;\n  width: calc(100% - 60px) !important;\n}\n.snippet,\n.webversion {\n  Float: none !important;\n}\n.column {\n  max-width: 400px !important;\n  width: 100% !important;\n}\n.fixed-width.has-border {\n  max-width: 402px !important;\n}\n.fixed-width.has-border .layout__inner {\n  box-sizing: border-box;\n}\n.snippet,\n.webversion {\n  width: 50% !important;\n}\n.ie .btn {\n  width: 100%;\n}\n.ie .column,\n[owa] .column,\n.ie .gutter,\n[owa] .gutter {\n  display: table-cell;\n  float: none !important;\n  vertical-align: top;\n}\n.ie div.preheader,\n[owa] div.preheader,\n.ie .email-footer,\n[owa] .email-footer {\n  max-width: 560px !important;\n  width: 560px !important;\n}\n.ie .snippet,\n[owa] .snippet,\n.ie .webversion,\n[owa] .webversion {\n  width: 280px !important;\n}\n.ie .header,\n[owa] .header,\n.ie .layout,\n[owa] .layout,\n.ie .one-col .column,\n[owa] .one-col .column {\n  max-width: 600px !important;\n  width: 600px !important;\n}\n.ie .fixed-width.has-border,\n[owa] .fixed-width.has-border,\n.ie .has-gutter.has-border,\n[owa] .has-gutter.has-border {\n  max-width: 602px !important;\n  width: 602px !important;\n}\n.ie .two-col .column,\n[owa] .two-col .column {\n  width: 300px !important;\n}\n.ie .three-col .column,\n[owa] .three-col .column,\n.ie .narrow,\n[owa] .narrow {\n  width: 200px !important;\n}\n.ie .wide,\n[owa] .wide {\n  width: 400px !important;\n}\n.ie .two-col.has-gutter .column,\n[owa] .two-col.x_has-gutter .column {\n  width: 290px !important;\n}\n.ie .three-col.has-gutter .column,\n[owa] .three-col.x_has-gutter .column,\n.ie .has-gutter .narrow,\n[owa] .has-gutter .narrow {\n  width: 188px !important;\n}\n.ie .has-gutter .wide,\n[owa] .has-gutter .wide {\n  width: 394px !important;\n}\n.ie .two-col.has-gutter.has-border .column,\n[owa] .two-col.x_has-gutter.x_has-border .column {\n  width: 292px !important;\n}\n.ie .three-col.has-gutter.has-border .column,\n[owa] .three-col.x_has-gutter.x_has-border .column,\n.ie .has-gutter.has-border .narrow,\n[owa] .has-gutter.x_has-border .narrow {\n  width: 190px !important;\n}\n.ie .has-gutter.has-border .wide,\n[owa] .has-gutter.x_has-border .wide {\n  width: 396px !important;\n}\n.ie .fixed-width .layout__inner {\n  border-left: 0 none white !important;\n  border-right: 0 none white !important;\n}\n.ie .layout__edges {\n  display: none;\n}\n.mso .layout__edges {\n  font-size: 0;\n}\n.layout-fixed-width,\n.mso .layout-full-width {\n  background-color: #ffffff;\n}\n@media only screen and (min-width: 620px) {\n  .column,\n  .gutter {\n    display: table-cell;\n    Float: none !important;\n    vertical-align: top;\n  }\n  div.preheader,\n  .email-footer {\n    max-width: 560px !important;\n    width: 560px !important;\n  }\n  .snippet,\n  .webversion {\n    width: 280px !important;\n  }\n  .header,\n  .layout,\n  .one-col .column {\n    max-width: 600px !important;\n    width: 600px !important;\n  }\n  .fixed-width.has-border,\n  .fixed-width.ecxhas-border,\n  .has-gutter.has-border,\n  .has-gutter.ecxhas-border {\n    max-width: 602px !important;\n    width: 602px !important;\n  }\n  .two-col .column {\n    width: 300px !important;\n  }\n  .three-col .column,\n  .column.narrow {\n    width: 200px !important;\n  }\n  .column.wide {\n    width: 400px !important;\n  }\n  .two-col.has-gutter .column,\n  .two-col.ecxhas-gutter .column {\n    width: 290px !important;\n  }\n  .three-col.has-gutter .column,\n  .three-col.ecxhas-gutter .column,\n  .has-gutter .narrow {\n    width: 188px !important;\n  }\n  .has-gutter .wide {\n    width: 394px !important;\n  }\n  .two-col.has-gutter.has-border .column,\n  .two-col.ecxhas-gutter.ecxhas-border .column {\n    width: 292px !important;\n  }\n  .three-col.has-gutter.has-border .column,\n  .three-col.ecxhas-gutter.ecxhas-border .column,\n  .has-gutter.has-border .narrow,\n  .has-gutter.ecxhas-border .narrow {\n    width: 190px !important;\n  }\n  .has-gutter.has-border .wide,\n  .has-gutter.ecxhas-border .wide {\n    width: 396px !important;\n  }\n}\n@media only screen and (-webkit-min-device-pixel-ratio: 2), only screen and (min--moz-device-pixel-ratio: 2), only screen and (-o-min-device-pixel-ratio: 2/1), only screen and (min-device-pixel-ratio: 2), only screen and (min-resolution: 192dpi), only screen and (min-resolution: 2dppx) {\n  .fblike {\n    background-image: url(https://i3.createsend1.com/static/eb/customise/13-the-blueprint-3/images/fblike@2x.png) !important;\n  }\n  .tweet {\n    background-image: url(https://i4.createsend1.com/static/eb/customise/13-the-blueprint-3/images/tweet@2x.png) !important;\n  }\n  .linkedinshare {\n    background-image: url(https://i6.createsend1.com/static/eb/customise/13-the-blueprint-3/images/lishare@2x.png) !important;\n  }\n  .forwardtoafriend {\n    background-image: url(https://i5.createsend1.com/static/eb/customise/13-the-blueprint-3/images/forward@2x.png) !important;\n  }\n}\n@media (max-width: 321px) {\n  .fixed-width.has-border .layout__inner {\n    border-width: 1px 0 !important;\n  }\n  .layout,\n  .column {\n    min-width: 320px !important;\n    width: 320px !important;\n  }\n  .border {\n    display: none;\n  }\n}\n.mso div {\n  border: 0 none white !important;\n}\n.mso .w560 .divider {\n  Margin-left: 260px !important;\n  Margin-right: 260px !important;\n}\n.mso .w360 .divider {\n  Margin-left: 160px !important;\n  Margin-right: 160px !important;\n}\n.mso .w260 .divider {\n  Margin-left: 110px !important;\n  Margin-right: 110px !important;\n}\n.mso .w160 .divider {\n  Margin-left: 60px !important;\n  Margin-right: 60px !important;\n}\n.mso .w354 .divider {\n  Margin-left: 157px !important;\n  Margin-right: 157px !important;\n}\n.mso .w250 .divider {\n  Margin-left: 105px !important;\n  Margin-right: 105px !important;\n}\n.mso .w148 .divider {\n  Margin-left: 54px !important;\n  Margin-right: 54px !important;\n}\n.mso .font-avenir,\n.mso .font-cabin,\n.mso .font-open-sans,\n.mso .font-ubuntu {\n  font-family: sans-serif !important;\n}\n.mso .font-bitter,\n.mso .font-merriweather,\n.mso .font-pt-serif {\n  font-family: Georgia, serif !important;\n}\n.mso .font-lato,\n.mso .font-roboto {\n  font-family: Tahoma, sans-serif !important;\n}\n.mso .font-pt-sans {\n  font-family: \"Trebuchet MS\", sans-serif !important;\n}\n.mso .footer__share-button p {\n  margin: 0;\n}\n@media only screen and (min-width: 620px) {\n  .wrapper .size-8 {\n    font-size: 8px !important;\n    line-height: 14px !important;\n  }\n  .wrapper .size-9 {\n    font-size: 9px !important;\n    line-height: 16px !important;\n  }\n  .wrapper .size-10 {\n    font-size: 10px !important;\n    line-height: 18px !important;\n  }\n  .wrapper .size-11 {\n    font-size: 11px !important;\n    line-height: 19px !important;\n  }\n  .wrapper .size-12 {\n    font-size: 12px !important;\n    line-height: 19px !important;\n  }\n  .wrapper .size-13 {\n    font-size: 13px !important;\n    line-height: 21px !important;\n  }\n  .wrapper .size-14 {\n    font-size: 14px !important;\n    line-height: 21px !important;\n  }\n  .wrapper .size-15 {\n    font-size: 15px !important;\n    line-height: 23px !important;\n  }\n  .wrapper .size-16 {\n    font-size: 16px !important;\n    line-height: 24px !important;\n  }\n  .wrapper .size-17 {\n    font-size: 17px !important;\n    line-height: 26px !important;\n  }\n  .wrapper .size-18 {\n    font-size: 18px !important;\n    line-height: 26px !important;\n  }\n  .wrapper .size-20 {\n    font-size: 20px !important;\n    line-height: 28px !important;\n  }\n  .wrapper .size-22 {\n    font-size: 22px !important;\n    line-height: 31px !important;\n  }\n  .wrapper .size-24 {\n    font-size: 24px !important;\n    line-height: 32px !important;\n  }\n  .wrapper .size-26 {\n    font-size: 26px !important;\n    line-height: 34px !important;\n  }\n  .wrapper .size-28 {\n    font-size: 28px !important;\n    line-height: 36px !important;\n  }\n  .wrapper .size-30 {\n    font-size: 30px !important;\n    line-height: 38px !important;\n  }\n  .wrapper .size-32 {\n    font-size: 32px !important;\n    line-height: 40px !important;\n  }\n  .wrapper .size-34 {\n    font-size: 34px !important;\n    line-height: 43px !important;\n  }\n  .wrapper .size-36 {\n    font-size: 36px !important;\n    line-height: 43px !important;\n  }\n  .wrapper .size-40 {\n    font-size: 40px !important;\n    line-height: 47px !important;\n  }\n  .wrapper .size-44 {\n    font-size: 44px !important;\n    line-height: 50px !important;\n  }\n  .wrapper .size-48 {\n    font-size: 48px !important;\n    line-height: 54px !important;\n  }\n  .wrapper .size-56 {\n    font-size: 56px !important;\n    line-height: 60px !important;\n  }\n  .wrapper .size-64 {\n    font-size: 64px !important;\n    line-height: 63px !important;\n  }\n}\n.mso .size-8,\n.ie .size-8 {\n  font-size: 8px !important;\n  line-height: 14px !important;\n}\n.mso .size-9,\n.ie .size-9 {\n  font-size: 9px !important;\n  line-height: 16px !important;\n}\n.mso .size-10,\n.ie .size-10 {\n  font-size: 10px !important;\n  line-height: 18px !important;\n}\n.mso .size-11,\n.ie .size-11 {\n  font-size: 11px !important;\n  line-height: 19px !important;\n}\n.mso .size-12,\n.ie .size-12 {\n  font-size: 12px !important;\n  line-height: 19px !important;\n}\n.mso .size-13,\n.ie .size-13 {\n  font-size: 13px !important;\n  line-height: 21px !important;\n}\n.mso .size-14,\n.ie .size-14 {\n  font-size: 14px !important;\n  line-height: 21px !important;\n}\n.mso .size-15,\n.ie .size-15 {\n  font-size: 15px !important;\n  line-height: 23px !important;\n}\n.mso .size-16,\n.ie .size-16 {\n  font-size: 16px !important;\n  line-height: 24px !important;\n}\n.mso .size-17,\n.ie .size-17 {\n  font-size: 17px !important;\n  line-height: 26px !important;\n}\n.mso .size-18,\n.ie .size-18 {\n  font-size: 18px !important;\n  line-height: 26px !important;\n}\n.mso .size-20,\n.ie .size-20 {\n  font-size: 20px !important;\n  line-height: 28px !important;\n}\n.mso .size-22,\n.ie .size-22 {\n  font-size: 22px !important;\n  line-height: 31px !important;\n}\n.mso .size-24,\n.ie .size-24 {\n  font-size: 24px !important;\n  line-height: 32px !important;\n}\n.mso .size-26,\n.ie .size-26 {\n  font-size: 26px !important;\n  line-height: 34px !important;\n}\n.mso .size-28,\n.ie .size-28 {\n  font-size: 28px !important;\n  line-height: 36px !important;\n}\n.mso .size-30,\n.ie .size-30 {\n  font-size: 30px !important;\n  line-height: 38px !important;\n}\n.mso .size-32,\n.ie .size-32 {\n  font-size: 32px !important;\n  line-height: 40px !important;\n}\n.mso .size-34,\n.ie .size-34 {\n  font-size: 34px !important;\n  line-height: 43px !important;\n}\n.mso .size-36,\n.ie .size-36 {\n  font-size: 36px !important;\n  line-height: 43px !important;\n}\n.mso .size-40,\n.ie .size-40 {\n  font-size: 40px !important;\n  line-height: 47px !important;\n}\n.mso .size-44,\n.ie .size-44 {\n  font-size: 44px !important;\n  line-height: 50px !important;\n}\n.mso .size-48,\n.ie .size-48 {\n  font-size: 48px !important;\n  line-height: 54px !important;\n}\n.mso .size-56,\n.ie .size-56 {\n  font-size: 56px !important;\n  line-height: 60px !important;\n}\n.mso .size-64,\n.ie .size-64 {\n  font-size: 64px !important;\n  line-height: 63px !important;\n}\n.footer__share-button p {\n  margin: 0;\n}\n</style>\n    \n    <title></title>\n  <!--[if !mso]><!--><style type=\"text/css\">\n@import url(https://fonts.googleapis.com/css?family=Cabin:400,700,400italic,700italic|Open+Sans:400italic,700italic,700,400);\n</style><link href=\"https://fonts.googleapis.com/css?family=Cabin:400,700,400italic,700italic|Open+Sans:400italic,700italic,700,400\" rel=\"stylesheet\" type=\"text/css\" /><!--<![endif]--><style type=\"text/css\">\nbody{background-color:#f5f7fa}.mso h1{}.mso h1{font-family:sans-serif !important}.mso h2{}.mso h3{}.mso .column,.mso .column__background td{}.mso .column,.mso .column__background td{font-family:sans-serif !important}.mso .btn a{}.mso .btn a{font-family:sans-serif !important}.mso .webversion,.mso .snippet,.mso .layout-email-footer td,.mso .footer__share-button p{}.mso .webversion,.mso .snippet,.mso .layout-email-footer td,.mso .footer__share-button p{font-family:sans-serif !important}.mso .logo{}.mso .logo{font-family:Tahoma,sans-serif !important}.logo a:hover,.logo a:focus{color:#859bb1 !important}.mso .layout-has-border{border-top:1px solid #b1c1d8;border-bottom:1px solid #b1c1d8}.mso .layout-has-bottom-border{border-bottom:1px solid #b1c1d8}.mso .border,.ie .border{background-color:#b1c1d8}@media only screen and (min-width: 620px){.wrapper h1{}.wrapper h1{font-size:26px \n!important;line-height:34px !important}.wrapper h2{}.wrapper h2{font-size:20px !important;line-height:28px !important}.wrapper h3{}.column p,.column ol,.column ul{}}.mso h1,.ie h1{}.mso h1,.ie h1{font-size:26px !important;line-height:34px !important}.mso h2,.ie h2{}.mso h2,.ie h2{font-size:20px !important;line-height:28px !important}.mso h3,.ie h3{}.mso .layout__inner p,.ie .layout__inner p,.mso .layout__inner ol,.ie .layout__inner ol,.mso .layout__inner ul,.ie .layout__inner ul{}\n</style><meta name=\"robots\" content=\"noindex,nofollow\" />\n<meta property=\"og:title\" content=\"My First Campaign\" />\n</head>\n<!--[if mso]>\n  <body class=\"mso\">\n<![endif]-->\n<!--[if !mso]><!-->\n  <body class=\"full-padding\" style=\"margin: 0;padding: 0;-webkit-text-size-adjust: 100%;\">\n<!--<![endif]-->\n    <div class=\"wrapper\" style=\"min-width: 320px;background-color: #f5f7fa;\" lang=\"x-wrapper\">\n      <div class=\"preheader\" style=\"Margin: 0 auto;max-width: 560px;min-width: 280px; width: 280px;width: calc(28000% - 173040px);\">\n        <div style=\"border-collapse: collapse;display: table;width: 100%;\">\n        <!--[if (mso)|(IE)]><table align=\"center\" class=\"preheader\" cellpadding=\"0\" cellspacing=\"0\"><tr><td style=\"width: 280px\" valign=\"top\"><![endif]-->\n          <div class=\"snippet\" style='display: table-cell;Float: left;font-size: 12px;line-height: 19px;max-width: 280px;min-width: 140px; width: 140px;width: calc(14000% - 78120px);padding: 10px 0 5px 0;color: #b9b9b9;font-family: \"Open Sans\",sans-serif;'>\n            \n          </div>\n        <!--[if (mso)|(IE)]></td><td style=\"width: 280px\" valign=\"top\"><![endif]-->\n          <div class=\"webversion\" style='display: table-cell;Float: left;font-size: 12px;line-height: 19px;max-width: 280px;min-width: 139px; width: 139px;width: calc(14100% - 78680px);padding: 10px 0 5px 0;text-align: right;color: #b9b9b9;font-family: \"Open Sans\",sans-serif;'>\n            \n          </div>\n        <!--[if (mso)|(IE)]></td></tr></table><![endif]-->\n        </div>\n      </div>\n      <div class=\"header\" style=\"Margin: 0 auto;max-width: 600px;min-width: 320px; width: 320px;width: calc(28000% - 173000px);\" id=\"emb-email-header-container\">\n      <!--[if (mso)|(IE)]><table align=\"center\" class=\"header\" cellpadding=\"0\" cellspacing=\"0\"><tr><td style=\"width: 600px\"><![endif]-->\n        <div class=\"logo emb-logo-margin-box\" style=\"font-size: 26px;line-height: 32px;Margin-top: 6px;Margin-bottom: 20px;color: #c3ced9;font-family: Roboto,Tahoma,sans-serif;Margin-left: 20px;Margin-right: 20px;\" align=\"center\">\n          <div class=\"logo-center\" style=\"font-size:0px !important;line-height:0 !important;\" align=\"center\" id=\"emb-email-header\"><a style=\"text-decoration: none;transition: opacity 0.1s ease-in;color: #c3ced9;\" href=\"https://grit.systems/img/grit-small.png\"><img style=\"height: auto;width: 100%;border: 0;max-width: 165px;\" src=\"https://grit.systems/img/grit-small.png\" alt=\"logo\" width=\"165\" /></a></div>\n        </div>\n      <!--[if (mso)|(IE)]></td></tr></table><![endif]-->\n      </div>\n      <div class=\"layout one-col fixed-width\" style=\"Margin: 0 auto;max-width: 600px;min-width: 320px; width: 320px;width: calc(28000% - 173000px);overflow-wrap: break-word;word-wrap: break-word;word-break: break-word;\">\n        <div class=\"layout__inner\" style=\"border-collapse: collapse;display: table;width: 100%;background-color: #ffffff;\" lang=\"x-layout__inner\" emb-background-style>\n        <!--[if (mso)|(IE)]><table align=\"center\" cellpadding=\"0\" cellspacing=\"0\"><tr class=\"layout-fixed-width\" emb-background-style><td style=\"width: 600px\" class=\"w560\"><![endif]-->\n          <div class=\"column\" style='text-align: left;color: #60666d;font-size: 14px;line-height: 21px;font-family: \"Open Sans\",sans-serif;max-width: 600px;min-width: 320px; width: 320px;width: calc(28000% - 167400px);'>\n        \n            <div style=\"Margin-left: 20px;Margin-right: 20px;Margin-top: 24px;\">\n      <p class=\"size-17\" style=\"Margin-top: 0;Margin-bottom: 0;font-family: cabin,avenir,sans-serif;font-size: 17px;line-height: 26px;\" lang=\"x-size-17\"><span class=\"font-cabin\"><strong>Hello,</strong></span></p><p class=\"size-17\" style=\"Margin-top: 20px;Margin-bottom: 0;font-family: cabin,avenir,sans-serif;font-size: 17px;line-height: 26px;\" lang=\"x-size-17\"><span class=\"font-cabin\">Thank you for your interest in the GRIT Systems G1 energy monitor.</span></p><p class=\"size-17\" style=\"Margin-top: 20px;Margin-bottom: 20px;font-family: cabin,avenir,sans-serif;font-size: 17px;line-height: 26px;\" lang=\"x-size-17\"><span class=\"font-cabin\">Please take the time to fill out the <a style=\"text-decoration: underline;transition: opacity 0.1s ease-in;color: #5c91ad;\" href=\"https://tife1.typeform.com/to/kDrg27?email="+ email + "\">form linked below</a>, this will help us better understand your requirements.</span></p>\n    </div>\n        \n            <div style=\"Margin-left: 20px;Margin-right: 20px;\">\n      <div style=\"line-height:10px;font-size:1px\">&nbsp;</div>\n    </div>\n        \n            <div style=\"Margin-left: 20px;Margin-right: 20px;Margin-bottom: 24px;\">\n      <div class=\"btn btn--flat btn--large\" style=\"text-align:left;\">\n        <![if !mso]><a style='border-radius: 4px;display: inline-block;font-size: 14px;font-weight: bold;line-height: 24px;padding: 12px 24px;text-align: center;text-decoration: none !important;transition: opacity 0.1s ease-in;color: #fff;background-color: #36a60d; color: #f5f7fa; font-family: &#39;Open Sans&#39;, sans-serif !important;font-family: \"Open Sans\",sans-serif;' href=\"https://tife1.typeform.com/to/kDrg27?email=" + email +  "\">Go to Form</a><![endif]>\n      <!--[if mso]><p style=\"line-height:0;margin:0;\">&nbsp;</p><v:roundrect xmlns:v=\"urn:schemas-microsoft-com:vml\" href=\"https://tife1.typeform.com/to/kDrg27?email=" +email + " \" style=\"width:125px\" arcsize=\"9%\" fillcolor=\"#36A60D\" stroke=\"f\"><v:textbox style=\"mso-fit-shape-to-text:t\" inset=\"0px,11px,0px,11px\"><center style=\"font-size:14px;line-height:24px;color:#F5F7FA;font-family:sans-serif;font-weight:bold;mso-line-height-rule:exactly;mso-text-raise:4px\">Go to Form</center></v:textbox></v:roundrect><![endif]--></div>\n    </div>\n        \n          </div>\n        <!--[if (mso)|(IE)]></td></tr></table><![endif]-->\n        </div>\n      </div>\n  \n      <div class=\"layout email-footer\" style=\"Margin: 0 auto;max-width: 600px;min-width: 320px; width: 320px;width: calc(28000% - 173000px);overflow-wrap: break-word;word-wrap: break-word;word-break: break-word;\">\n        <div class=\"layout__inner\" style=\"border-collapse: collapse;display: table;width: 100%;\" lang=\"x-layout__inner\">\n        <!--[if (mso)|(IE)]><table align=\"center\" cellpadding=\"0\" cellspacing=\"0\"><tr class=\"layout-email-footer\"><td style=\"width: 400px;\" valign=\"top\" class=\"w360\"><![endif]-->\n          <div class=\"column wide\" style='text-align: left;font-size: 12px;line-height: 19px;color: #b9b9b9;font-family: \"Open Sans\",sans-serif;Float: left;max-width: 400px;min-width: 320px; width: 320px;width: calc(8000% - 47600px);'>\n            <div style=\"Margin-left: 20px;Margin-right: 20px;Margin-top: 10px;Margin-bottom: 10px;\">\n              <table class=\"email-footer__links emb-web-links\" style=\"border-collapse: collapse;table-layout: fixed;\"><tbody><tr>\n              \n<td class=\"emb-web-links\" style=\"padding: 0;width: 26px;\"><a style=\"text-decoration: underline;transition: opacity 0.1s ease-in;color: #b9b9b9;\" href=\"http://www.facebook.com/grit.systems/\"><img style=\"border: 0;\" src=\"https://i8.createsend1.com/static/eb/customise/13-the-blueprint-3/images/facebook.png\" width=\"26\" height=\"26\" /></a></td><td class=\"emb-web-links\" style=\"padding: 0 0 0 3px;width: 26px;\"><a style=\"text-decoration: underline;transition: opacity 0.1s ease-in;color: #b9b9b9;\" href=\"http://twitter.com/grit_systems\"><img style=\"border: 0;\" src=\"https://i10.createsend1.com/static/eb/customise/13-the-blueprint-3/images/twitter.png\" width=\"26\" height=\"26\" /></a></td><td class=\"emb-web-links\" style=\"padding: 0 0 0 3px;width: 26px;\"><a style=\"text-decoration: underline;transition: opacity 0.1s ease-in;color: #b9b9b9;\" href=\"https://www.linkedin.com/company/grit-systems-engineering\"><img style=\"border: 0;\" src=\"https://i2.createsend1.com/static/eb/customise/13-the-blueprint-3/images/linkedin.png\" width=\"26\" height=\"26\" /></a></td><td class=\"emb-web-links\" style=\"padding: 0 0 0 3px;width: 26px;\"><a style=\"text-decoration: underline;transition: opacity 0.1s ease-in;color: #b9b9b9;\" href=\"https://grit.systems\"><img style=\"border: 0;\" src=\"https://i3.createsend1.com/static/eb/customise/13-the-blueprint-3/images/website.png\" width=\"26\" height=\"26\" /></a></td>\n              </tr></tbody></table>\n              <div style=\"Margin-top: 20px;\">\n                <div>GRIT Systems<br />\n294 Herbert Macauly<br />\n101212 Yaba, Lagos<br />\n<br />\n+234(0)706 555 1430</div>\n              </div>\n              <div style=\"Margin-top: 18px;\">\n                \n              </div>\n            </div>\n          </div>\n        <!--[if (mso)|(IE)]></td><td style=\"width: 200px;\" valign=\"top\" class=\"w160\"><![endif]-->\n          <div class=\"column narrow\" style='text-align: left;font-size: 12px;line-height: 19px;color: #b9b9b9;font-family: \"Open Sans\",sans-serif;Float: left;max-width: 320px;min-width: 200px; width: 320px;width: calc(72200px - 12000%);'>\n            <div style=\"Margin-left: 20px;Margin-right: 20px;Margin-top: 10px;Margin-bottom: 10px;\">\n              \n            </div>\n          </div>\n        <!--[if (mso)|(IE)]></td></tr></table><![endif]-->\n        </div>\n      </div>\n      <div style=\"line-height:40px;font-size:40px;\">&nbsp;</div>\n    \n  </div>\n</body></html>"

          val HtmlSeq = scala.xml.Unparsed(HtmlMsg)
          SendEmail.sendHTML(
            "notify@grit.systems",
            email,
            "GRIT Systems' G1 Energy Monitor",
            HtmlSeq
          )
          User.findByEmail(email).headOption.getOrElse(User).mailSent(true).save()
          S.appendGlobalJs(Run("toastr[\"success\"](\"We have succesfully received your request and will be in touch with you soon.\")"))
        } catch {
          case e: Exception => println(e)
        }


      }
    }
    PassThru
  }
}

















