package code.snippet

import code.model.{GRIT_Invoice, _}
import net.liftweb.http.js.JsCmds.Run
import net.liftweb.http.{RequestVar, S}
import org.bson.types.ObjectId
import org.joda.time.DateTime
import org.joda.time.format.DateTimeFormat

import scala.util.parsing.json.JSON
import scala.xml.Text
import scalaj.http.{Http, HttpRequest}
import net.liftweb.util._
import Helpers._
import code.snippet.AccountOps.totalOwing
import net.liftweb.common.Full
import net.liftweb.mongodb.BsonDSL._


/**
  *Handles invoice processing
  */
class Billing {

  object GRIT_InvoiceVar extends RequestVar[GRIT_Invoice](GRIT_Invoice.createRecord)
  val currentUserID = S.get("selectedUserID").toOption.getOrElse(User.currentUserId.headOption.getOrElse(""))
  val currentUserEmail = User.find("_id",new ObjectId(currentUserID)).headOption.getOrElse(User).email.toString()
  var configs:List[(String,String)] = Configuration.findAll("UserID_FK", currentUserID).map(p => (p.id.toString -> p.configurationName.toString))
  var users:List[(String,String)] = List(("n/a","n/a"))    /**to hold users*/
  val formatter = java.text.NumberFormat.getIntegerInstance
  val account = Account.findAll("UserID_FK",currentUserID).headOption.getOrElse(Account)
  var pp = DateTimeFormat.forPattern("yyyy-MM-dd")

  /** Processes Payments **/
  def render = {
    for {
      r <- S.request if r.post_? /** make sure it's a post*/
      ref <- S.param("hidden") /**payment reference*/
      invoices = S.params("invoices")
    } {
      val request: HttpRequest = Http("https://api.paystack.co/transaction/verify/"+ref).header("Authorization","Bearer sk_live_cab5dd09381c3caa7e88727c338b9a4414d2a0ec").timeout(connTimeoutMs = 10000, readTimeoutMs = 50000)
      val response = request.asString
      val valid = response.isSuccess

      if (valid) {
        val status = JSON.parseFull(response.body)
        val obj = status.get
        val theMap = obj.asInstanceOf[Map[String,_]]
        val authorization =  theMap.get("data").get.asInstanceOf[Map[String,_]].get("authorization").get.asInstanceOf[Map[String,_]].get("authorization_code").get.toString
        val amount  = theMap.get("data").get.asInstanceOf[Map[String,_]].get("amount").get.toString.toDouble/100
        val customer_code = theMap.get("data").get.asInstanceOf[Map[String,_]].get("customer").get.asInstanceOf[Map[String,_]].get("customer_code").get.toString
        val email = theMap.get("data").get.asInstanceOf[Map[String,_]].get("customer").get.asInstanceOf[Map[String,_]].get("email").get.toString
        val cardDetails =  theMap.get("data").get.asInstanceOf[Map[String,_]].get("authorization").get.asInstanceOf[Map[String,_]].get("last4").get.toString


        println(status)
        println("\n")
        println(obj)
        val totalAmount : BigDecimal = invoices.map{invoiceId =>
          val invoice = GRIT_Invoice.findAll("_id",invoiceId).headOption.getOrElse(GRIT_Invoice)
          val fmt = DateTimeFormat forPattern "dd MMM yyyy"
          val formatter = java.text.NumberFormat.getInstance
          val amount = if(!invoice.Lines.value.isEmpty){
            invoice.Lines.value.map{ a =>
              a.UnitPrice.value * BigDecimal(a.Quantity.value)
            }.reduce(_+_)
          } else {
            BigDecimal(0.0)
          }
          amount
        }.sum

        if (amount >= totalAmount)
          {
            val payment = Payment.createRecord.Amount(Money.createRecord.Amount(BigDecimal(amount))).Reference(ref).Auth_code(authorization)
              .Paystack_customerCode(customer_code).Method("Card").Email(email).AccountID_FK(account.id.value).CardDetails(cardDetails)

            for (invoiceId <- invoices) {
              GRIT_Invoice.find("_id",invoiceId) match {
                case Full(invoice) => {
                  invoice.Status("paid").PaymentID_FK(payment.id.value).save()
                }
                case _ => {}
              }
            }
            payment.save();
            S.notice("Payment was Successful!")
            S.redirectTo("/bills")
          }
        else {
          S.error("Your payment was not complete contact support@grit.systems to resolve")
          S.redirectTo("/bills")
        }
      } else {
        //TODO JsCommand to print out payment failure
       S.error("Payment was not successful")
        S.redirectTo("/bills")
      }
    }
   S.appendJs(enhance)
    PassThru
  }

  val enhance =
    Run("$('#payButton').click(function() {\n    if ($(\"input:checked\").length == 0) {\n        $('.alert').show();\n        window.setTimeout(function() {\n            $(\".alert\").fadeOut(\"slow\", \"swing\")\n        }, 5000);\n    } else {\n        payWithPaystack()\n    }\n});\n\nfunction add(a,b) {\n  return a + b;\n}\n\nvar myObject = {\n    get readOnlyProperty() {\n        var number = $(\"input:checked\").length;\n        if (number != 0) {\n            var total = []\n            $(\"input:checked\").each(function(a){\n                var amt = $(this).prev().html().split(\" \")[1].split(\",\").reduce(add);\n                console.log(Number(amt))\n                total.push(Number(amt));\n            })\n            amount = total.reduce(add) * 100;\n            console.log(amount)\n        }\n        return amount\n    }\n};\nvar ref = 'gritSystems' + Math.floor((Math.random() * 1000000000000) + 1);\ndocument.getElementById(\"hidden\").value = ref;\nvar UserEmail = $('#email').val()\n\nfunction payWithPaystack() {\n    var handler = PaystackPop.setup({\n        key: 'pk_live_9ac6a4fb664ea163ea02838d9bc0e0348e03867d',\n        email: UserEmail,\n        amount: myObject.readOnlyProperty,\n        callback: function(response) {\n            document.forms[0].submit()\n        },\n        onClose: function() {\n            window.location.href = \"/bills\";\n        },\n        ref: ref\n    });\n    handler.openIframe();\n}")


  /**
    *
    * @param payment handles card processing
    * @return card details
    */
    def lastCard(payment: Payment) = {
      payment.CardDetails.value match {
        case(a) if (a.nonEmpty) => {Text("************"+a)}
        case _ => {Text("")}
      }
    }

  /** Shows summary of users' payments **/
   def balance = {
     val lastPayment = Payment.findAll("AccountID_FK",account.id.value).sortWith(_.Date.value isAfter _.Date.value).headOption.getOrElse(Payment)
    "#email [value]" #> currentUserEmail &
    "#recentCard *" #> lastCard(lastPayment) &
    "#lastPayment *" #> formatter.format(lastPayment.Amount.value.Amount.value) &
    "#totalOwing *" #> formatter.format(BigDecimal(105.00/100) * totalOwing(account.id.value))
  }

  /** Shows Users' outstanding bills **/
  def outStandingBills () = {
    val invoices = GRIT_Invoice.findAll(("AccountID_FK" -> account.id.value)~("Status" ->"unpaid"))
    invoices.nonEmpty match {
      case true => {
        ".card *" #> invoices.map{invoice =>
          val fmt = DateTimeFormat forPattern "dd MMM yyyy"
          val formatter = java.text.NumberFormat.getInstance
          val amount_  = if(!invoice.Lines.value.isEmpty){
            invoice.Lines.value.map{ a =>
              a.UnitPrice.value * BigDecimal(a.Quantity.value)
            }.reduce(_+_)
          } else {
            BigDecimal(0.0)
          }
          val amount = (BigDecimal(105.00/100) * amount_)
          println(amount_)
          println(amount)
          "#ref *" #>  Text(invoice.reference.value) &
            "#amount" #> formatter.format(amount) &
            ".nn [value] " #> Text(invoice.id.value) &
            "#date *" #> fmt.print(new DateTime(invoice.DueDate.value.toString)) &
          ".tag *" #> Text(invoice.tag.value)
        }
      }
      case _ => {
        "#cover" #> <div><i class="fa fa-info-circle" aria-hidden="true"></i><span> You do not have outstanding bills</span></div>
      }
    }
  }

  /** Shows payment history **/
  def paymentHistory() = {
    val payments = Payment.findAll(("AccountID_FK" -> account.id.value))
    ".lists *" #> payments.map{payment =>
      val fmt = DateTimeFormat forPattern "dd MMM yyyy"
      val formatter = java.text.NumberFormat.getInstance
      val amount = payment.Amount.value.Amount.value

      "#payRef *" #>  Text(payment.Reference.value) &
      "#method *" #>  Text(payment.Method.value) &
      "#payAmount  *" #> formatter.format(amount) &
      "#payDate *" #> fmt.print(new DateTime(payment.Date.value.toString))
    }
  }

}
