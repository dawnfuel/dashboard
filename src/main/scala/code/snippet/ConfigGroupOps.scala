package code.snippet

import code.model.{Configuration, User}
import net.liftweb.http.SHtml.ElemAttr._
import net.liftweb.http.js.JsCmd
import net.liftweb.http.js.JsCmds._

import scala.collection.mutable.ListBuffer
//import code.snippet.ConfigurationOps.Item
import code.lib.MSHtml
import code.model.{Configuration, ConfigGroup}
import net.liftweb.common.{Loggable, _}
import net.liftweb.http.SHtml.ajaxSelect
import net.liftweb.http._
import net.liftweb.http.js.JsCmds.After
import net.liftweb.util.Helpers._
import org.bson.types.ObjectId

import scala.xml.Text

/*
 * This class provides the snippets that back the Source CRUD 
 * pages (List, Create, View, Edit, Delete)
 */
class ConfigGroupOps extends Loggable {

  object ConfigGroupVar extends RequestVar[ConfigGroup](ConfigGroup.createRecord)

  case class Item(id: String, name: String)

  /**
    *
    * @param v an instance of a User
    * @param uname uname sets the username
    * @return
    */
  def gn(v: Configuration , uname:String) = List(Item(v.id.toString, v.configurationName.toString+" ("+uname+")"))

  /**
    *
    * @param v  an instance of a User
    * @return Returns the username
    */
  def gnc(v: Configuration ) = List(Item(v.id.toString, v.configurationName.toString+" ("+User.findAll("_id", new ObjectId(v.AdminID_FK.value)).headOption.getOrElse(User.createRecord).username+")"))

  /**
    *
    * @param v an instance of a User
    * @return Returns configuration name as string
    */
  def g(v: Configuration ) = List(Item(v.id.toString, v.configurationName.toString ))

  def manOf[T: Manifest](t: T): Manifest[T] = manifest[T]

  val currentUserId = S.get("selectedUserID").toOption.getOrElse(User.currentUserId.headOption.getOrElse(""))
  var currentUserEmail = User.find("_id", new ObjectId(currentUserId)).headOption.getOrElse(User).email.toString()
  var configs_to_add: List[String] = List()
  var configs_to_rm: List[String] = List()
  var configs_available: List[(String, String)] = List()
  var existing_configs: List[(String, String)] = List()

  /**
    * Handles submission processing
    * Called by the create form when the group clicks submit
  * If the entered data passes validation then if Nil deletes configGroupID and saves the change else add configuration to the group else redirect to error page*/


  def processSubmit() = {
    ConfigGroupVar.is.validate match {
      case Nil =>
        configs_available.flatMap{ config =>
          Configuration.findAll("_id", config._1).map{ config =>
            logger.info("DELETING ConfigGroupID_FK "+config.ConfigGroupID_FK+" from associated Configuration :  "+config.configurationName)
            config.ConfigGroupID_FK(Empty)
            config.save()
          }
        }
        existing_configs.flatMap{ config => Configuration.findAll("_id",  config._1)}.map{ config =>
          logger.info("ADDING  Configuration "+config.configurationName+" to Configuration Group "+ConfigGroupVar.is.id.toString)
          config.ConfigGroupID_FK(ConfigGroupVar.is.id.toString)
          config.save()
        }
        ConfigGroupVar.is.save()
        S.notice("Source Saved")
        S.seeOther("/system/configuration/listgroup")
      case errors => S.error(errors)
    }
    /** exiting the function here causes the form to reload*/
  }

  /** called from /system/configuration/creategroup.html*/
  def create = {
    logger.info("create user groups")
    /** compile list of all Source/Master Configs for all ConfigGroups */
    val masterconf_ids = ConfigGroup.findAll.map{ group => group.SourceConfigID_FK.value}.distinct

    /** compile list of user ID's for all configs that have been assigned to a user */
    var confswithusers = Configuration.findAll.map{ conf =>
      if(!masterconf_ids.contains(conf.id.value)) {
        conf.AdminID_FK.value
      }
    }.filter(_ !=(())).asInstanceOf[List[String]].distinct
    confswithusers.foreach(println)

    /** comple list of user id and user name pairs for admin select */
    val alluserswithconfs = confswithusers.flatMap{ user => User.findAll("_id", new ObjectId(user))}.flatMap( u => List(Item(u.id.toString, u.username.toString)))
    confswithusers.foreach(println)
    alluserswithconfs.foreach(println)

    /** compile list of all configurations that are unassigned to a config group*/
    val allconfigs = alluserswithconfs.flatMap{ user => Configuration.findAll("AdminID_FK",user.id).map{ conf =>
      if(conf.ConfigGroupID_FK.toString.isEmpty){//} && ! (masterconf_ids.contains(conf.id.value))){
        println("include "+conf.configurationName+" :: "+conf.id+" :: "+masterconf_ids+" ")
        conf
      }
    }.filter(_ !=(())).asInstanceOf[List[Configuration]].flatMap( c => gnc(c))}
    logger.info("list of configs : ")
    allconfigs.foreach(println)
    val optionsuser = alluserswithconfs.map(i => i.id -> i.name).sortWith(_._2 > _._2)
    val optionsconfig = allconfigs.map(i => i.id -> i.name).sortWith(_._2 > _._2)
    val default_admin = List[(String, String)](optionsuser.headOption.getOrElse(("", "")))
    println("da "+default_admin.headOption.getOrElse(("", ""))._1+" "+manOf(default_admin))
    val optionssourceconfig = Configuration.findAll("AdminID_FK",default_admin.headOption.getOrElse(("", ""))._1).map{ conf =>
      if(!masterconf_ids.contains(conf.id.value)) {
        conf
      }
    }.filter(_ !=(())).asInstanceOf[List[Configuration]].flatMap(u => gnc(u)).map(i => i.id -> i.name).sortWith(_._2 > _._2)
    val default_source_config = List[(String, String)](optionssourceconfig.headOption.getOrElse(("", "")))
    logger.info("default user : " + default_admin)
    ConfigGroupVar.is.adminID_FK(default_admin.headOption.getOrElse(("", ""))._1)
    ConfigGroupVar.is.SourceConfigID_FK(default_source_config.headOption.getOrElse(("", ""))._1)
    existing_configs = default_source_config
    configs_available = optionsconfig diff default_source_config
    val source = ConfigGroupVar.is
    "#hidden" #> SHtml.hidden(() => ConfigGroupVar(source)) &
      "#groupName" #> SHtml.text(ConfigGroupVar.is.groupName.value, name => ConfigGroupVar.is.groupName(name)) &
      "#adminID_FK" #> SHtml.ajaxSelect(optionsuser, Box.legacyNullTest(null), set_admin(_) ,  pairToBasic("title", "only users with associated Configs are listed here")) &
      "#source_config" #> SHtml.ajaxSelect(optionssourceconfig, Box.legacyNullTest(null), set_source_config) &
      "#allconfigs" #> MSHtml.ajaxUntrustedMultiSelect(optionsconfig diff default_source_config, Seq[String](), set_add_configs) &
      "#groupmembers" #> MSHtml.ajaxUntrustedMultiSelect(default_source_config, Seq[String](), set_rm_configs) &
      "#margin" #> SHtml.number(ConfigGroupVar.is.margin.value, margin => ConfigGroupVar.is.margin(margin), 1, 99) &
      "#addconfigs" #> SHtml.ajaxButton(">>", () => add_configs()) &
      "#rm_configs" #> SHtml.ajaxButton("<<", () => rm_configs())

      /** This registers a function for Lift to call when the user clicks submit*/
      "#submit" #> SHtml.onSubmitUnit(processSubmit)
  }

  /**
    *
    * @param config Config sets source config
    * @return Returns config
    */
  def set_source_config(config: String): JsCmd = {
    logger.info("SET SOURCE/MASTER CONFIG")
    ConfigGroupVar.is.SourceConfigID_FK(config)
    Noop
  }

  /**
    *
    * @param admin sets a new admin Id
    * @return Returns added source config
    */
  def set_admin(admin: String): JsCmd = {
    println()
    ConfigGroupVar.is.adminID_FK(admin)
    val new_admin = User.findAll("_id", new ObjectId(admin)).flatMap( u => List(Item(u.id.toString, u.username.toString))).map(i => i.id -> i.name)
    val optionssourceconfig = Configuration.findAll("AdminID_FK",admin).flatMap(u => gnc(u)).map(i => i.id -> i.name).sortWith(_._2 > _._2)
    ConfigGroupVar.is.SourceConfigID_FK(optionssourceconfig.headOption.getOrElse(("",""))._1)
    logger.info("SET GROUP ADMIN : " + new_admin)
    configs_to_add = List(optionssourceconfig.headOption.getOrElse(("", ""))._1)
    List(ReplaceOptions("source_config", optionssourceconfig, Box.legacyNullTest(null)),add_configs())
  }


  /**
    *Handles the removal of source config
    * @param configs in the admin
    * @return Returns error message if the condition is not met.
    */
  def set_rm_configs(configs: List[String]): JsCmd = {
    logger.info("SET REMOVE CONFIGS")
    logger.info("CURRENT ADMIN : " + ConfigGroupVar.is.adminID_FK.value)
    logger.info("CURRENT SOURCE CONFIG : " + ConfigGroupVar.is.SourceConfigID_FK.value)
    val optionssourceconfig = Configuration.findAll("AdminID_FK",ConfigGroupVar.is.adminID_FK.value).flatMap(u => gnc(u)).map(i => i.id -> i.name).sortWith(_._2 > _._2)
    configs_to_rm = configs diff ConfigGroupVar.is.SourceConfigID_FK.value
    configs.foreach(println)
    if (configs.contains(ConfigGroupVar.is.SourceConfigID_FK.value)) {
      logger.error("ERROR CANNOT REMOVE CURRENT SOURCE CONFIG : " + ConfigGroupVar.is.adminID_FK.value)
      S.error("ERROR CANNOT REMOVE CURRENT SOURCE / MASTER CONFIG : " + ConfigGroupVar.is.adminID_FK.value)
      val msg: String = "ERROR CANNOT REMOVE CURRENT SOURCE / MASTER CONFIG : " + ConfigGroupVar.is.adminID_FK.value
      S.appendJs(Run(s"addError(\'$msg\')"))
      ReplaceOptions("groupmembers", existing_configs, Box.legacyNullTest(null))
    } else {
      Noop
    }
  }


  /**
    *
    * @param configs adds a config
    */
  def set_add_configs(configs: List[String]) = {
    logger.info("SET ADD CONFIGS")
    configs_to_add = configs
    configs.foreach(println)
  }


  /**
    *Handles the removal of existing configs
    * @return Returns Js command
    */
  def rm_configs(): JsCmd = {
    logger.info("REMOVING CONFIGS")
    logger.info(" existing : ")
    existing_configs.foreach(println)
    logger.info(" removing : ")
    configs_to_rm.foreach(println)
    val configs_removed = configs_to_rm.map { g_config_id => Configuration.findAll("_id",g_config_id) }.flatten.flatMap(u => gnc(u)).map(i => i.id -> i.name)
    existing_configs = (existing_configs diff configs_removed).sortWith(_._2 > _._2)
    configs_available = configs_available ::: configs_removed
    configs_removed.foreach(println)
    println()
    List(ReplaceOptions("allconfigs", configs_available, Box.legacyNullTest(null)), ReplaceOptions("groupmembers", existing_configs, Box.legacyNullTest(null)))
  }

  /**
    *Handles the adding of a config from existing configs
    * @return Returns JsCmd
    */
  def add_configs(): JsCmd = {
    logger.info("ADDING CONFIGS")
    logger.info(" existing : ")
    existing_configs.foreach(println)
    logger.info(" new      : ")
    configs_to_add.foreach(println)
    val new_configs = configs_to_add.map { g_config_id => Configuration.findAll("_id", g_config_id) }.flatten.flatMap(u => gnc(u)).map(i => i.id -> i.name)
    new_configs.foreach(println)
    logger.info(" available      : ")
    configs_available.foreach(println)
    println()
    existing_configs = (existing_configs ::: new_configs).distinct.sortWith(_._2 > _._2)
    configs_available = configs_available diff new_configs
    List(ReplaceOptions("allconfigs", configs_available, Box.legacyNullTest(null)), ReplaceOptions("groupmembers", existing_configs, Box.legacyNullTest(null)))
  }

  /** called from /system/configuration/editgroup.html*/
  def edit = {
    val params = S.request.map(_.request.param("action")) openOr "Undefined"
    val action = params.asInstanceOf[List[String]].headOption.getOrElse("Undefined action")
    val vieworedit = action match {
      case "Edit" =>
        pairToBasic("enabled", "disabled")
      case _ =>
        pairToBasic("disabled", "disabled")
    }
    logger.info("edit or view config group")
    if (! ConfigGroupVar.set_?)
      S.redirectTo("/system/configuration/listgroup")
    println(ConfigGroupVar)

    /** compile list of all Source/Master Configs for all ConfigGroups*/
    val masterconf_ids = ConfigGroup.findAll.map{ group => group.SourceConfigID_FK.value}.distinct
    /** compile list of user ID's for all configs that have been assigned to a user*/
    var confswithusers = Configuration.findAll.map{ conf =>
      //if(!masterconf_ids.contains(conf.id.value)) {
        conf.AdminID_FK.value
      //}
    }.filter(_ !=(())).asInstanceOf[List[String]].distinct

    /** compile list of user id and user name pairs for admin select*/
    val alluserswithconfs = confswithusers.flatMap{ user => User.findAll("_id", new ObjectId(user))}.flatMap( u => List(Item(u.id.toString, u.username.toString)))
    confswithusers.foreach(println)
    alluserswithconfs.foreach(println)
    /** compile list of all configurations that are unassigned to a config group */
    val allconfigs = alluserswithconfs.flatMap{ user => Configuration.findAll("AdminID_FK",user.id).map{ conf =>
      if(conf.ConfigGroupID_FK.toString.isEmpty){
        conf
      }
    }.filter(_ !=(())).asInstanceOf[List[Configuration]].flatMap( c => gnc(c))}

    logger.info("list of configs : ")
    allconfigs.foreach(println)
    val optionsuser = alluserswithconfs.map(i => i.id -> i.name).sortWith(_._2 > _._2)
    val optionsconfig = allconfigs.map(i => i.id -> i.name).sortWith(_._2 > _._2)
    val default_admin = List[(String, String)](optionsuser.filter(option => option._1 == ConfigGroupVar.is.adminID_FK.value).headOption.getOrElse(("", "")))
    println("da "+default_admin.headOption.getOrElse(("", ""))._1+" "+manOf(default_admin))
    val optionssourceconfig = Configuration.findAll("AdminID_FK",default_admin.headOption.getOrElse(("", ""))._1).map{ conf =>
      //if(!masterconf_ids.contains(conf.id.value)) {
        conf
      //}
    }.filter(_ !=(())).asInstanceOf[List[Configuration]].flatMap(u => gnc(u)).map(i => i.id -> i.name).sortWith(_._2 > _._2)

    logger.info("list of configs : ")
    allconfigs.foreach(println)
    val groupmembers = Configuration.findAll("ConfigGroupID_FK", ConfigGroupVar.id.value).flatMap(u => gnc(u)).map(i => i.id -> i.name).sortWith(_._2 > _._2)
    logger.info("existing members : " + groupmembers)
    existing_configs = groupmembers
    configs_available = optionsconfig diff groupmembers
    val source = ConfigGroupVar.is
    "#hidden" #> SHtml.hidden(() => ConfigGroupVar(source)) &
      "#heading_action" #> SHtml.span(<h5 class="alt" id="heading_action">{action} Config Group : {ConfigGroupVar.is.groupName.value} </h5>, (Noop)  ) &
      "#groupName" #> SHtml.text(ConfigGroupVar.is.groupName.value, name => ConfigGroupVar.is.groupName(name), vieworedit) &
      "#adminID_FK" #> SHtml.ajaxSelect(optionsuser, Box.legacyNullTest(default_admin.headOption.getOrElse(("", ""))._1), set_admin(_), vieworedit) &
      "#source_config" #> SHtml.ajaxSelect(optionssourceconfig, Box.legacyNullTest(optionssourceconfig.headOption.getOrElse(("", ""))._1), set_source_config(_), vieworedit ) &
      "#allconfigs" #> MSHtml.ajaxUntrustedMultiSelect(optionsconfig diff groupmembers, Seq[String](), set_add_configs(_), vieworedit) &
      "#groupmembers" #> MSHtml.ajaxUntrustedMultiSelect(groupmembers, Seq[String](), set_rm_configs(_), vieworedit) &
      "#margin" #> SHtml.number(ConfigGroupVar.is.margin.value, (margin: Int) => ConfigGroupVar.is.margin(margin), 1, 99, vieworedit) &
      "#addconfigs" #> SHtml.ajaxButton(">>", () => add_configs()) &
      "#rm_configs" #> SHtml.ajaxButton("<<", () => rm_configs()) &
      // This registers a function for Lift to call when the user clicks submit
      "#submit" #> SHtml.onSubmitUnit(processSubmit)
  }

  /** called from /system/configuration/listgroup.html*/
  def list = {
    logger.info("list configuration groups")
    "#heading_user" #> SHtml.span(<h5 class="alt" id="heading_user">Configuration Groups associated with {User.findAll("_id", new ObjectId(currentUserId)).headOption.getOrElse(User.createRecord).username.value}</h5>, Noop) &
      "#configurationgrouplist *" #> ConfigGroup.findAll("adminID_FK",currentUserId).map(cr => {
        ".groupName *" #> Text(cr.groupName.value) &
        ".groupAdmin *" #> Text( User.findAll("_id", new ObjectId(cr.adminID_FK.value)).headOption.getOrElse(User.createRecord).username.value) &
        ".sourceName *" #> Text( Configuration.findAll("_id", cr.SourceConfigID_FK.value).headOption.getOrElse(Configuration.createRecord).configurationName.value) &
        ".memberNames *" #> Text( ( Configuration.findAll("ConfigGroupID_FK",cr.id.value ).map{ conf => conf.configurationName.value } mkString ", " )  ) &
        ".actions *" #> {SHtml.link("/system/configuration/editgroup?action=View", () => ConfigGroupVar(cr), Text("view")) ++ Text(" ") ++
          SHtml.link("/system/configuration/editgroup?action=Edit", () => ConfigGroupVar(cr), Text("edit")) ++ Text(" ") ++
          SHtml.link("/system/configuration/deletegroup", () => ConfigGroupVar(cr), Text("delete"))}
      } ) &
      "#configurationgrouplistall *" #> ConfigGroup.findAll.map(cr => {
        ".groupName *" #> Text(cr.groupName.value) &
        ".groupAdmin *" #> Text( User.findAll("_id", new ObjectId(cr.adminID_FK.value)).headOption.getOrElse(User.createRecord).username.value) &
        ".sourceName *" #> Text( Configuration.findAll("_id", cr.SourceConfigID_FK.value).headOption.getOrElse(Configuration.createRecord).configurationName.value) &
        ".memberNames *" #> Text( ( Configuration.findAll("ConfigGroupID_FK",cr.id.value ).map{ conf => conf.configurationName.value } mkString ", " )  ) &
        ".actions *" #> {SHtml.link("/system/configuration/editgroup?action=View", () => ConfigGroupVar(cr), Text("view")) ++ Text(" ") ++
          SHtml.link("/system/configuration/editgroup?action=Edit", () => ConfigGroupVar(cr), Text("edit")) ++ Text(" ") ++
          SHtml.link("/system/configuration/deletegroup", () => ConfigGroupVar(cr), Text("delete"))}
      } )
  }

  /**
    *
    * @param confIDs gets config Id in a list
    * @return Returns configId in a list
    */
  def getSources(confIDs: List[String]): List[ConfigGroup] = {
    var g: List[ConfigGroup] = List()
    for (p <- confIDs) {
      g = g ::: ConfigGroup.findAll("ConfigID_FK", p)
    }
    g
  }

  /** called from /system/configuration/deletegroup.html*/
  def deleteConfigGroup(e: ConfigGroup) {
        Configuration.findAll("ConfigGroupID_FK",e.id.value).map{ config =>
          logger.info("DELETING CONFIGGROUP ID_FKs from associated Configuration :  "+config.configurationName)
          val configgroupid_fks = config.ConfigGroupID_FK.value.to[ListBuffer]
          logger.info("pre delete usergroup_id_fks  : "+ configgroupid_fks)
          config.ConfigGroupID_FK("")
          logger.info("post delete configgroupid_fks : "+ configgroupid_fks)
          config.save()
        }
    logger.info("deleting ConfigGroup :  " + e)
    e.delete_!
  }

  /**
    *
    * @return Returns Css selection and redirects to the configuration listgroup
    */
  def delete = {
    /** we're expecting the ConfigGroupVar to have been set when we were linked to from the list page*/
    if (!ConfigGroupVar.set_?)
      S.redirectTo("/system/configuration/listgroup")
    val e = ConfigGroupVar.is
    "#configGroupName" #> ConfigGroupVar.is.groupName &
      "#yes" #> SHtml.link("/system/configuration/listgroup", () => {
        deleteConfigGroup(e); e.delete_!
      }, Text("Yes")) &
      "#no" #> SHtml.link("/system/configuration/listgroup", () => {}, Text("No"))
  }
}