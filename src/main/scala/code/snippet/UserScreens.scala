package code
package snippet

import config.Site
import model._

import scala.xml._
import net.liftweb._
import common._
import http.{LiftScreen, S}
import util.FieldError
import util.Helpers._


/*
 * Use for editing the currently logged in user only.
 */
sealed trait BaseCurrentUserScreen extends BaseScreen {
  object userVar extends ScreenVar(User.currentUser.openOr(User.createRecord))

  override def localSetup {
    Referer(Site.account.url)
  }
}
object SystemScreen extends BaseCurrentUserScreen {
  addFields(() => userVar.is.systemScreenFields)
  def finish() {
    userVar.is.saveBox match {
      case Empty => S.warning("Empty save")
      case Failure(msg, _, _) => S.error(msg)
      case Full(_) => S.notice("Account settings saved")
    }
  }
}
object AccountScreen extends BaseCurrentUserScreen {
  addFields(() => userVar.is.accountScreenFields)

  def finish() {
    userVar.is.saveBox match {
      case Empty => S.warning("Empty save")
      case Failure(msg, _, _) => S.error(msg)
      case Full(_) => S.notice("Account settings saved")
    }
  }
}

sealed trait BasePasswordScreen {
  this: LiftScreen =>

  def pwdName: String = "Password"
  def pwdMinLength: Int = 6
  def pwdMaxLength: Int = 32

  val passwordField = password(pwdName, "", trim,
    valMinLen(pwdMinLength, "Password must be at least "+pwdMinLength+" characters"),
    valMaxLen(pwdMaxLength, "Password must be "+pwdMaxLength+" characters or less"),
    "tabindex" -> "1"
  )
  val confirmPasswordField = password("Confirm Password", "", trim, "tabindex" -> "1")

  def passwordsMustMatch(): Errors = {
    if (passwordField.is != confirmPasswordField.is)
      List(FieldError(confirmPasswordField, "Passwords must match"))
    else Nil
  }
}


object PasswordScreen extends BaseCurrentUserScreen with BasePasswordScreen {
  override def pwdName = "New Password"
  override def validations = passwordsMustMatch _ :: super.validations

  def finish() {
    userVar.is.password(passwordField.is)
    userVar.is.password.hashIt
    userVar.is.saveBox match {
      case Empty => S.warning("Empty save")
      case Failure(msg, _, _) => S.error(msg)
      case Full(_) => S.notice("New password saved")
    }
  }
}

/*
 * Use for editing the currently logged in user only.
 */
sealed trait profileScreen extends BaseCurrentUserScreen {
  addFields(() => userVar.is.profileScreenFields)
  override def localSetup {
    Referer(Site.editProfile.url)
  }
}

object ProfileScreen extends profileScreen with BasePasswordScreen {
  override def pwdName = "New Password"
  override def validations = passwordsMustMatch _ :: super.validations
  
  def finish() {
    userVar.is.password(passwordField.is)
    userVar.is.password.hashIt
    userVar.is.saveBox match {
      case Empty => S.warning("Empty save")
      case Failure(msg, _, _) => S.error(msg)
      case Full(_) => {
        S.notice("Profile settings saved")
        S.redirectTo("/index.html")
        //TODO Inform user profile was updated
      }
    }
  }
}


// this is needed to keep these fields and the password fields in the proper order
trait BaseRegisterScreen extends BaseScreen {
  object userVar extends ScreenVar(User.regUser.is)

  addFields(() => userVar.is.registerScreenFields)
}

/*
 * Use for creating a new user.
 */
object RegisterScreen extends BaseRegisterScreen {
  val homePageOps = new homepageops

  override def localSetup {
    Referer(Site.listusers.url)
  }

  def finish() {
    val userGroupId : List[String] = User.currentUser match {
      case Full(currentUser) => {
        UserGroup.find("adminID_FK",currentUser.id.value.toString) match {
          case Full(userGroup) => {
            if (homePageOps.userHasRole("regionaladmin")) {
              println("in here")
              List(userGroup.id.value)
            }
            else {
              List("")
            }
          }
          case _ => {List("")}
        }
      }
      case _ => {List("")}
    }

    val user = userVar.is
    user.UserGroupID_FKs(userGroupId.filter(_ != "")).saveBox match {
      case Empty => S.warning("Empty save")
      case Failure(msg, _, _) => S.error(msg)
      case Full(u) =>
        S.notice("Thanks for signing up!")
    }
  }
}

