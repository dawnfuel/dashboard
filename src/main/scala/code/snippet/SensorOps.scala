package code.snippet

import code.model.{Probe, Sensor}
import net.liftweb.http.SHtml.ElemAttr._
import net.liftweb.util._
import Helpers._
import net.liftweb.http.{S, RequestVar, SHtml}

import scala.xml.Text

/** Class providing Sensor CRUD snippets
  *
  */
class SensorOps{
  object sensorVar extends RequestVar[Sensor](Sensor.createRecord)

  def processSubmit() = {
    println(sensorVar)
    sensorVar.is.save()
    S.seeOther("/system/products/listsensors")
  }

  def list()  ={
    val sensors = Sensor.findAll("Type","sensor")
    "#sensorList" #> sensors.map{ sensor =>
    ".sensorName" #>  sensor.name.value &
    ".actions *" #> {SHtml.link("/system/products/editsensor?action=View", () => sensorVar(sensor), Text("view")) ++ Text(" ") ++
      SHtml.link("/system/products/editsensor?action=Edit", () => sensorVar(sensor), Text("edit")) ++ Text(" ") ++
      SHtml.link("/system/products/deletesensor", () => sensorVar(sensor), Text("delete"))}
    }
  }

  def create() = {
    val sensor = sensorVar.is
      "#hidden" #> SHtml.hidden(() => sensorVar(sensor)) &
      "#name" #> SHtml.text(sensorVar.is.name.value,name=>{sensorVar.is.name(name)} ) &
      "#ScalingFactor" #> SHtml.number(sensorVar.is.ScalingFactor.value, setSF(_),0.0,100000.0,0.000001) &
      "#maxCurrent" #> SHtml.number(sensorVar.is.maxCurrent.value,setMaxCurrent(_),0.0,math.pow(2,52),0.00001) &
      "#size" #> SHtml.number(sensorVar.is.size.value,setSize(_),0.0,math.pow(2,52),0.00001)&
      "#price" #> SHtml.number(sensorVar.is.price._1.Amount.value.toDouble ,setPrice(_),0.0,math.pow(2,52),0.001)&
      "#submit" #> SHtml.onSubmitUnit(processSubmit)
  }

  def edit() = {
    if ( ! sensorVar.set_? )
      S.redirectTo("/system/products/listsensors")

    def manOf[T: Manifest](t: T): Manifest[T] = manifest[T]
    val params = S.request.map(_.request.param("action")) openOr ("Undefined")
    val action = params.asInstanceOf[List[String]].headOption.getOrElse("Undefined action")
    val vieworedit = action match {
      case "Edit" =>
        pairToBasic("enabled","disabled")
      case _ =>
        pairToBasic("disabled","disabled")
    }
    val sensor = sensorVar.is

    "#hidden" #> SHtml.hidden(() => sensorVar(sensor)) &
      ".panel-title"  #> Text(action + " Sensor") &
      "#name" #> SHtml.text(sensorVar.is.name.value,name => {sensorVar.is.name(name)},vieworedit ) &
      "#ScalingFactor" #> SHtml.number(sensorVar.is.ScalingFactor.value,(sf:Double) => {setSF(sf) },0.0,100000.0,0.001,vieworedit) &
      "#maxCurrent" #> SHtml.number(sensorVar.is.maxCurrent.value,(maxCurrent:Double)=>{setMaxCurrent(maxCurrent)},0.0,math.pow(2,52),0.0,vieworedit) &
      "#size" #> SHtml.number(sensorVar.is.size.value,(size:Double)=>{setSize(size)},0.0,math.pow(2,52),0.0,vieworedit)&
      "#price" #> SHtml.number(sensorVar.is.price.get.Amount.value.toDouble,(price:Double)=>{setPrice(price)},0.0,math.pow(2,52),0.0,vieworedit)&
      "#submit" #> SHtml.onSubmitUnit(processSubmit)
  }

  def delete () = {
    val sensor  = sensorVar.is
    def deleteSensor(){
      Probe.findAll.map{
        p => p.Channels.get.map{
          channel => if(channel.SensorID_FK.value == sensor.id.value){
            channel.SensorID_FK("")
          }
        }
        p.save()
      }
      sensor.delete_!
    }
    "#sensorName" #> sensorVar.is.name &
      "#yes" #> SHtml.link("/system/products/listsensors", () =>{
        deleteSensor() }, Text("Delete")) &
      "#no" #> SHtml.link("/system/products/listsensors", () =>{ }, Text("Cancel"))
  }

  def setSF(value:Double) = {
    sensorVar.is.ScalingFactor(value)
  }
  def setMaxCurrent(value:Double) = {
    sensorVar.is.maxCurrent(value)
  }
  def setSize(value : Double): Unit = {
    sensorVar.is.size(value)
  }
  def setPrice(value:Double): Unit ={
    sensorVar.is.price.get.Amount(value)
  }

}