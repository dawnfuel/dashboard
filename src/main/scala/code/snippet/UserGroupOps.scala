package code.snippet

import code.model.Probe._
import net.liftweb.http.SHtml.ElemAttr._
import net.liftweb.http.js.{JE, JsCmd}
import net.liftweb.http.js.JsCmds._

import scala.collection.mutable.ListBuffer
import scala.util.Try
//import code.snippet.ConfigurationOps.Item
import code.model.{Configuration, UserGroup, Probe, User}
import net.liftweb._
import net.liftweb.common.{Loggable, _}
import net.liftweb.http.SHtml.ajaxSelect
import net.liftweb.http._
import net.liftweb.http.js.JsCmds.After
import net.liftweb.mongodb.BsonDSL._
import net.liftweb.util.Helpers._
import net.liftweb.util._
import org.bson.types.ObjectId
import code.lib.MSHtml
import scala.xml.{Text, _}

/*
 * This class provides the snippets that back the Source CRUD 
 * pages (List, Create, View, Edit, Delete)
 */
class UserGroupOps extends Loggable {
  object UserGroupVar extends RequestVar[UserGroup](UserGroup.createRecord)
  case class Item(id: String, name: String)
  def g(v:User) = List(Item(v.id.toString, v.username.toString))
  def manOf[T: Manifest](t: T): Manifest[T] = manifest[T]
  val currentUserId = S.get("selectedUserID").toOption.getOrElse(User.currentUserId.headOption.getOrElse(""))
  var currentUserEmail = User.find("_id",new ObjectId(currentUserId)).headOption.getOrElse(User).email.toString()
  var users_to_add : List[String] = List()
  var users_to_rm : List[String] = List()
  var users_available : List[(String,String)] = List()
  var existing_users : List[(String,String)] = List()

  def processSubmit() = {
    UserGroupVar.is.validate match {
      case  Nil =>
        users_available.flatMap{ user =>
          User.findAll("_id",new ObjectId(user._1)).map{ user =>
            logger.info("DELETING USERGROUP ID_FKs from associated User :  "+user.username)
            val usergroupid_fks = user.UserGroupID_FKs.value.to[ListBuffer]
            logger.info("pre edit usergroup_id_fks  : "+ usergroupid_fks)
            usergroupid_fks -= UserGroupVar.is.id.toString
            user.UserGroupID_FKs(usergroupid_fks.toList)
            logger.info("post edit usergroup_id_fks : "+ usergroupid_fks)
            user.save()
          }
        }
        existing_users.flatMap{ user => User.findAll("_id", new ObjectId(user._1))}.map{ user =>
          user.UserGroupID_FKs(user.UserGroupID_FKs.value ::: List(UserGroupVar.is.id.toString))
          user.save()
        }
        UserGroupVar.is.save()
        S.notice("User Group Saved")
        S.seeOther("/system/usergroup/listgroup")
      case errors => S.error(errors)
    }
    // exiting the function here causes the form to reload
  }

  // called from /system/usergroup/creategroup.html
  def create = {
    logger.info("create user groups")
    val allusers = User.findAll.flatMap( u => g(u))
    logger.info("list of users : ")
    allusers.foreach(println)
    val optionsuser = allusers.map( i =>  i.id -> i.name ).sortWith(_._2 > _._2)
    val default_admin = List[(String,String)](optionsuser.headOption.getOrElse(("","")))
    logger.info("default user : "+default_admin)
    UserGroupVar.is.adminID_FK(default_admin.headOption.getOrElse(("",""))._1)
    existing_users = default_admin
    users_available = optionsuser diff default_admin
    val source = UserGroupVar.is
    "#hidden" #> SHtml.hidden(() => UserGroupVar(source) ) &
      "#groupName" #> SHtml.text(UserGroupVar.is.groupName.value, name => UserGroupVar.is.groupName(name) ) &
      "#adminID_FK" #> SHtml.ajaxSelect(optionsuser, Box.legacyNullTest(null), set_admin ) &
      "#allusers" #> MSHtml.ajaxUntrustedMultiSelect(optionsuser diff default_admin, Seq[String]() , set_add_users) &
      "#groupmembers" #> MSHtml.ajaxUntrustedMultiSelect(default_admin, Seq[String]() , set_rm_users) &
      "#margin" #> SHtml.number(UserGroupVar.is.margin.value, margin => UserGroupVar.is.margin(margin), 1,99) &
      "#whiteLabel"  #> SHtml.select(List(("true","true"),("false","false")),Box.legacyNullTest(UserGroupVar.is.whiteLabel.value.toString),b =>{UserGroupVar.is.whiteLabel(Try(b.toBoolean).getOrElse(false))}) &
      "#addusers" #> SHtml.ajaxButton(">>", () => add_users() ) &
      "#rm_users" #> SHtml.ajaxButton("<<", () => rm_users() ) &
    // This registers a function for Lift to call when the user clicks submit
      "#submit" #> SHtml.onSubmitUnit(processSubmit)
  }

  def set_admin ( admin : String ) : JsCmd = {
    println()
    UserGroupVar.is.adminID_FK(admin)
    val new_admin = User.findAll("_id",new ObjectId(admin)).flatMap( u => g(u)).map( i =>  i.id -> i.name )
    logger.info("SET GROUP ADMIN : "+new_admin)
    users_to_add = List(new_admin.headOption.getOrElse(("",""))._1)
    add_users()
  }

  def set_rm_users( users : List[String]) : JsCmd = {
    logger.info("SET REMOVE USERS")
    logger.info("CURRENT ADMIN : "+UserGroupVar.is.adminID_FK.value)
    users_to_rm = users diff UserGroupVar.is.adminID_FK.value
    users.foreach(println)
    if (users.contains(UserGroupVar.is.adminID_FK.value)){
      logger.error("ERROR CANNOT REMOVE CURRENT ADMIN : "+UserGroupVar.is.adminID_FK.value)
      S.error("ERROR CANNOT REMOVE CURRENT ADMIN : "+UserGroupVar.is.adminID_FK.value)
      val msg : String = "ERROR CANNOT REMOVE CURRENT ADMIN : "+UserGroupVar.is.adminID_FK.value
      S.appendJs(Run(s"addError(\'$msg\')"))
      ReplaceOptions("groupmembers", existing_users , Box.legacyNullTest(null))
    }else{
      Noop
    }
  }
  def set_add_users( users : List[String]) = {
    logger.info("SET ADD USERS")
    users_to_add = users
    users.foreach(println)
  }
  def rm_users() : JsCmd = {
    logger.info("REMOVING USERS")
    logger.info(" existing : ")
    existing_users.foreach(println)
    logger.info(" removing : ")
    users_to_rm.foreach(println)
    val users_removed = users_to_rm.map{ g_user_id => User.findAll("_id",new ObjectId(g_user_id))}.flatten.flatMap( u => g(u)).map( i =>  i.id -> i.name )
    existing_users = (existing_users diff users_removed).sortWith(_._2 > _._2)
    users_available = users_available ::: users_removed
    users_removed.foreach(println)
    println()
    List(ReplaceOptions("allusers", users_available , Box.legacyNullTest(null)), ReplaceOptions("groupmembers", existing_users , Box.legacyNullTest(null)))
  }
  def add_users() : JsCmd = {
    logger.info("ADDING USERS")
    logger.info(" existing : ")
    existing_users.foreach(println)
    logger.info(" new      : ")
    users_to_add.foreach(println)
    val new_users = users_to_add.map{ g_user_id => User.findAll("_id",new ObjectId(g_user_id))}.flatten.flatMap( u => g(u)).map( i =>  i.id -> i.name )
    new_users.foreach(println)
    println()
    existing_users = (existing_users ::: new_users).distinct.sortWith(_._2 > _._2)
    users_available = users_available diff new_users
    List(ReplaceOptions("allusers", users_available , Box.legacyNullTest(null)), ReplaceOptions("groupmembers", existing_users , Box.legacyNullTest(null)))
  }

  // called from /system/usergroup/editgroup.html
  def edit = {
    val params = S.request.map(_.request.param("action")) openOr "Undefined"
    val action = params.asInstanceOf[List[String]].headOption.getOrElse("Undefined action")
    val vieworedit = action match {
      case "Edit" =>
        pairToBasic("enabled","disabled")
      case _ =>
        pairToBasic("disabled","disabled")
    }
    logger.info("edit user group")
    if ( ! UserGroupVar.set_? )
      S.redirectTo("/system/usergroup/listgroup")

    val allusers = User.findAll.flatMap( u => g(u))
    logger.info("list of users : ")
    allusers.foreach(println)
    val optionsuser = allusers.map( i =>  i.id -> i.name ).sortWith(_._2 > _._2)
    val groupmembers = User.findAll("UserGroupID_FKs",UserGroupVar.id.value).flatMap( u => g(u)).map( i =>  i.id -> i.name ).sortWith(_._2 > _._2)
    logger.info("existing members : "+groupmembers)
    existing_users = groupmembers
    users_available = optionsuser diff groupmembers
    val source = UserGroupVar.is
    "#hidden" #> SHtml.hidden(() => UserGroupVar(source) ) &
      "#heading_action" #> SHtml.span(<h5 class="alt" id="heading_action">{action} User Group : {UserGroupVar.is.groupName.value} </h5>, (Noop)  ) &
      "#groupName" #> SHtml.text(UserGroupVar.is.groupName.value, name => UserGroupVar.is.groupName(name), vieworedit ) &
      "#adminID_FK" #> SHtml.ajaxSelect(optionsuser, Box.legacyNullTest(null), set_admin(_) , vieworedit) &
      "#allusers" #> MSHtml.ajaxUntrustedMultiSelect(optionsuser diff groupmembers, Seq[String]() , set_add_users(_), vieworedit ) &
      "#groupmembers" #> MSHtml.ajaxUntrustedMultiSelect(groupmembers, Seq[String]() , set_rm_users(_), vieworedit) &
      "#margin" #> SHtml.number(UserGroupVar.is.margin.value, (margin:Int) => UserGroupVar.is.margin(margin), 1,99,  vieworedit) &
      "#whiteLabel"  #> SHtml.select(List(("true","true"),("false","false")),Box.legacyNullTest(UserGroupVar.is.whiteLabel.value.toString),b =>{UserGroupVar.is.whiteLabel(Try(b.toBoolean).getOrElse(false))}) &
      "#addusers" #> SHtml.ajaxButton(">>", () => add_users() ) &
      "#rm_users" #> SHtml.ajaxButton("<<", () => rm_users() ) &
      // This registers a function for Lift to call when the user clicks submit
      "#submit" #> SHtml.onSubmitUnit(processSubmit)
  }

  // called from /system/usergroup/listgroup.html
  def list = {
    logger.info("list user groups")

    "#heading_user" #> SHtml.span(<h5 class="alt" id="heading_user">Groups</h5>, Noop  ) &
    // for each UserGroup object in the database render the name along with links to view, edit, and delete
    "#grouplist *" #> UserGroup.findAll.map(t => {
      ".groupName *" #> Text(t.groupName.value) &
      ".groupAdmin *" #> Text(User.findAll("_id",new ObjectId(t.adminID_FK.value)).head.username.toString) &
      ".groupMembers *" #> Text(User.findAll("UserGroupID_FKs",t.id.value).map{ user => user.username.toString} mkString ", ") &
      ".groupMargin *" #> Text(t.margin.value.toString) &
        ".actions *" #> {
          SHtml.link("/system/usergroup/deletegroup", () => UserGroupVar(t), Text("delete")) ++ Text(" ") ++
          SHtml.link("/system/usergroup/editgroup?action=Edit", () => UserGroupVar(t), Text("edit")) ++ Text(" ") ++
          SHtml.link("/system/usergroup/editgroup?action=View", () => UserGroupVar(t), Text("view"))
        }
    } )
  }

  def getSources(confIDs:List[String]):List[UserGroup]={
    var g:List[UserGroup] = List()
    for(p <- confIDs){
      g = g ::: UserGroup.findAll("ConfigID_FK",p)
    }
    g
  }

  // called from /system/usergroup/deletegroup.html
  def deleteUserGroup(e : UserGroup){
    User.findAll("UserGroupID_FKs",e.id.value).map{ user =>
      logger.info("DELETING USERGROUP ID_FKs from associated User :  "+user.username)
      val usergroupid_fks = user.UserGroupID_FKs.value.to[ListBuffer]
      logger.info("pre delete usergroup_id_fks  : "+ usergroupid_fks)
      usergroupid_fks -= e.id.toString
      user.UserGroupID_FKs(usergroupid_fks.toList)
      logger.info("post delete usergroup_id_fks : "+ usergroupid_fks)
      user.save()
    }
    logger.info("deleting UserGroup :  "+e)
    e.delete_!
  }


  def delete = {
    // we're expecting the UserGroupVar to have been set when we were linked to from the list page
    if ( ! UserGroupVar.set_? )
      S.redirectTo("/system/usergroup/listgroup")
    val e = UserGroupVar.is
    "#userGroupName" #> UserGroupVar.is.groupName &
      "#yes" #> SHtml.link("/system/usergroup/listgroup", () =>{deleteUserGroup(e); e.delete_!}, Text("Yes")) &
      "#no" #> SHtml.link("/system/usergroup/listgroup", () =>{ }, Text("No"))
  }
}