package code.snippet

import code.model.{Probe, Device}
import net.liftweb.http.{S, RequestVar, SHtml}
import net.liftweb.http.SHtml.ElemAttr._
import net.liftweb.util._
import Helpers._

import scala.xml.Text

/**
  *  This class provides the snippets that back the DeviceOps CRUD
  *  pages (List, Create, View, Edit, Delete)
  */

class DeviceOps {
  object deviceVar extends RequestVar[Device](Device.createRecord)
  var features : List[String] = List()

  /**
    * Handles the listing of the device actions
    * @return Html link
    */
  def list() = {
    "#deviceList" #> Device.findAll("Type","device").map { device =>
      ".deviceName" #> device.name.value &
      ".actions *" #> {SHtml.link("/system/products/editdevice?action=View", () => deviceVar(device), Text("view")) ++ Text(" ") ++
        SHtml.link("/system/products/editdevice?action=Edit", () => deviceVar(device), Text("edit")) ++ Text(" ") ++
        SHtml.link("/system/products/delete-device", () => deviceVar(device), Text("delete"))}
    }
  }

  /**
    * Handles device creation
    * @return Css selector
    */
  def create() = {
    val device = deviceVar.is

    "#hidden" #> SHtml.hidden(() => deviceVar(device)) &
      "#name" #> SHtml.text(deviceVar.is.name.value,name=>{deviceVar.is.name(name)} ) &
      "#NumberOfChannels" #> SHtml.number(deviceVar.is.NumberOfChannels.value, (n:Double) => {deviceVar.is.NumberOfChannels(n.toInt)},0.0,100000.0,1) &
      "#price" #> SHtml.number(deviceVar.is.price.value.Amount.value.toDouble,(price:Double) => {deviceVar.is.price.get.Amount(price)},0.0,math.pow(2,52),0.001) &
      "#submit" #> SHtml.onSubmitUnit(processSubmit)
  }

  /**
    * Handles form processing
    * @return redirection to 'listdevices' page
    */
  def processSubmit() = {
    S.params("features").foreach(r => addFeature(r))
    deviceVar.is.features(features)
    deviceVar.is.save()
    S.redirectTo("/system/products/listdevices")
  }

  /**
    * Handles the editing
    * @return 'listdevices' if device is set
    */
  def edit() = {
    if ( ! deviceVar.set_? )
      S.redirectTo("/system/products/listdevices")

    def manOf[T: Manifest](t: T): Manifest[T] = manifest[T]
    val params = S.request.map(_.request.param("action")) openOr ("Undefined")
    val action = params.asInstanceOf[List[String]].headOption.getOrElse("Undefined action")
    val vieworedit = action match {
      case "Edit" =>
        pairToBasic("enabled","disabled")
      case _ =>
        pairToBasic("disabled","disabled")
    }

    val device = deviceVar.is

    /**
      * Handles editing if edit is selected and redirects to 'listdevices'
      * @return edited list
      */
    def processEdit() = {
      if (action == "Edit") {
        S.params("features").foreach(r => addFeature(r))
        println(features)
        println(S.params("features"))
        deviceVar.is.features(features)
        deviceVar.is.save()
      }
      S.redirectTo("/system/products/listdevices")
    }

    "#hidden" #> SHtml.hidden(() => deviceVar(device)) &
      ".panel-title"  #> Text(action + " Device") &
      "#name" #> SHtml.text(deviceVar.is.name.value,name=>{deviceVar.is.name(name)},vieworedit ) &
      "#NumberOfChannels" #> SHtml.number(deviceVar.is.NumberOfChannels.value.toDouble, (n:Double) => {deviceVar.is.NumberOfChannels(n.toInt)},0.0,100000.0,1,vieworedit) &
      "#price" #> SHtml.number(deviceVar.is.price.value.Amount.value.toDouble,(price:Double) => {deviceVar.is.price.get.Amount(price)},0.0,math.pow(2,52),0.001,vieworedit) &
      "#inputFields *" #>  { deviceVar.is.features.value.map { feature =>
        ".features" #> SHtml.text(feature, (r:String) => {addFeature(r)},vieworedit)
      } } &
    "#submit" #> SHtml.onSubmitUnit(processEdit)
  }

  /**
    * Handles device deletion
    * @return link
    */
  def delete () = {
    val device  = deviceVar.is

    def deleteDevice(){
      Probe.findAll("DeviceID_FK",device.id.toString()).map{ p =>
          p.DeviceID_FK("")
          p.save()
      }
      device.delete_!
    }

    "#deviceName" #> deviceVar.is.name &
      "#yes" #> SHtml.link("/system/products/listdevices", () =>{
        deleteDevice() }, Text("Delete")) &
      "#no" #> SHtml.link("/system/products/listdevices", () =>{ }, Text("Cancel"))

  }

  /**
    * @param r new feature to add
    */
  def addFeature(r:String){
    if (! r.isEmpty) features = r::features
  }


}
