package code.snippet

import java.util.Properties

import code.model._
import code.model.Probe._
import net.liftweb.common.{Logger, _}
import net.liftweb.http.SHtml.ElemAttr._
import net.liftweb.http.SHtml._
import net.liftweb.http._
import net.liftweb.http.js.JsCmds.{After, Noop, Run}
import net.liftweb.http.js.JsCmd
import net.liftweb.http.js.JsCmds._
import net.liftweb.util.{PassThru, Props}
import net.liftweb.util.Helpers._
import org.apache.kafka.clients.producer._
import org.bson.types.ObjectId

import scala.xml._
/*
 * This class provides the snippets that back the Probe CRUD
 * pages (List, Create, View, Edit, Delete)
 */

object server {
  val props = new Properties()
  val topic = "gritserver"
  val kafkanode = Props.mode match {
    case Props.RunModes.Production => "test.grit.systems:9092"
    case _ => "localhost:9092"
  }
  props.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, kafkanode)
  props.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, "org.apache.kafka.common.serialization.StringSerializer")
  props.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, "org.apache.kafka.common.serialization.StringSerializer")
  props.put(ProducerConfig.CLIENT_ID_CONFIG, "my-producer")
  val producer = new KafkaProducer[String, String](props)
}


class ProbeOps extends Loggable {


  object probeVar extends RequestVar[Probe](Probe.createRecord)
  val currentUserId = S.get("selectedUserID").toOption.getOrElse(User.currentUserId.headOption.getOrElse(""))
  var currentUserEmail = User.find("_id",new ObjectId(currentUserId)).headOption.getOrElse(User).email.toString()
  var conf = new Properties()
  val devices = Device.findAll.map{device => (device.id.value,device.name.value)}
  val sensors = Sensor.findAll.map{s => (s.id.value,s.name.value)}

  var threshold = List[Int]()
  var defaultVoltage = List[Int]()
  var powerFactor = List[Double]()
  


  def updateFirmware(dest:String) = {
    val Src = " "
    val file = " "
    val msg = "update" + "::" + Src + "::" + dest + "::" + file
    val message = new ProducerRecord[String,String](server.topic,msg)
    server.producer.send(message)
  }

  def multiProbeUpdates() ={
    for {
      r <- S.request if r.post_? // make sure it's a post
      probes = S.params("Probes") // list of probes
    } {
      if (probes.nonEmpty) {
        for (probe <- probes) {
          updateFirmware(probe)
        }
        S.redirectTo("/")
      }
    }
    PassThru
  }

  def sendConfiguration(conf:Properties,Dest:List[String]){


    var prop = ""
    var dest = ""

    val keys = conf.keys()
    while(keys.hasMoreElements()){
      val key = keys.nextElement()
      val value = conf.get(key)
      prop += "::" + key + "," + value
    }
    for(d <- Dest){
      dest += ','+ d
    }
    dest = dest.replaceFirst(",","")

    val msg = "configuration" + "::" + " " + "::"+ dest +  prop
    val message = new ProducerRecord[String,String](server.topic,msg)

    server.producer.send(message)

    println("configuration has been sent")

  }

  def setWiFiParams (ssid:String,pwd:String){
    //this.wifi_ssid = ssid
  }


  /**
    * processSubmit is called by the both the create and edit forms when the user clicks submit
    * If the entered data passes validation then the user is redirected to the List page,
    * otherwise the form on which the user clicked submit is reloaded
   **/

  def processSubmit() = {

    /** adding corresponding channel power factor, default voltage and threshold **/
    for(i <- powerFactor.indices){
      probeVar.is.Channels.get(i).PowerFactor(powerFactor(i))
      probeVar.is.Channels.get(i).DefaultVoltage(defaultVoltage(i))
      probeVar.is.Channels.get(i).ChannelThreshold(threshold(i))
    }

    if ( probeVar.set_? ) {
      println("getting settings")
      conf = new Properties()
      conf.put("Wifi_ssid", probeVar.is.wifi_ssid.value.toString)
      conf.put("Wifi_password" ,probeVar.is.wifi_password.value.toString)

      if(probeVar.is.NumberOfChannels.value == 8) {
        var pFactor = ""
        var dVoltage = ""
        for (i <- 0 to 7) {
          try {
            pFactor += probeVar.is.Channels.get(i).PowerFactor.value.toString + "*"
            dVoltage += probeVar.is.Channels.get(i).DefaultVoltage.value.toString + "*"
          } catch {
            case e:Exception => println("Error sending configuration")
          }
        }
        if (pFactor.length > 0){
          pFactor = pFactor.trim.substring(0, pFactor.trim.length - 1)
        }
        if (dVoltage.length > 0){
          dVoltage = dVoltage.trim.substring(0, dVoltage.trim.length - 1)

        }
        conf.put("PowerFactor", pFactor)
        conf.put("DefaultVoltage", dVoltage)
      }

      /** processSubmit is called by the both the create and edit forms **/

      println("sending configuration")
      try{
        sendConfiguration(conf,List(probeVar.is.probeName.value))
      }catch {
        case e:Exception => println(" configuration not sent")
      }
    }


    println("DEBUG BAD")
    probeVar.is.validate match {
      case  Nil => {
        /**
          * note the return boolean from probeVar.is.save is ignored as it does
         not contain useful information. See thread:
         https://groups.google.com/forum/?hl=en#!searchin/liftweb/Mapper.save/liftweb/kcWwaqGamW0/RjWfdOxjShEJ
         **/
        println("CHANNELS : "+probeVar.is.NumberOfChannels)

        /**
          *  loop through NumberOfChannels and create a signal assignment for each one
          *  for loop execution with a range
          */

        if(Probe.find("probeName",probeVar.is.probeName.toString()).isEmpty) {
          var channels : List[SignalAssignment] = List()
          println("\n\nTo create chsnnels \n\n")
          for( a <- 1 to probeVar.is.NumberOfChannels.get){
            println( "Channel" + a )
            channels = SignalAssignment.createRecord.SourceID_FK("").sourceChannel(0) :: channels
          }
          val cha = SignalAssignment.createRecord.SourceID_FK("").sourceChannel(0)
          val chb = SignalAssignment.createRecord.SourceID_FK("").sourceChannel(1)
          val chc = SignalAssignment.createRecord.SourceID_FK("").sourceChannel(2)
          val chlst = cha :: chb :: chc :: Nil
          def manOf[T: Manifest](t: T): Manifest[T] = manifest[T]

          println("CHT : "+manOf(channels))
          println("CH  : "+channels)

          probeVar.Channels((channels))
        }

        probeVar.is.save()
        println("\n\nSaved \n\n")
        S.notice("Probe Saved")
        /**
          * S.seeOther throws an exception to interrupt the flow of execution and redirect
          * the user to the specified URL
          */
        S.seeOther("/system/probe/listprobe")
      }
      case errors => S.error(errors)
        println("\n\nNot saved \n\n")
    }
    // exiting the function here causes the form to reload
  }
  val channelopts = List (("8","8"),("12","12"))
  // called from /system/probe/createprobe.html
  def create = {
    // When create is called on the first GET request, probeVar.is initialises probeVar by calling Probe.create.
    // If create is being called due to a form reload after validation has failed on a PUT request,
    // probeVar.is returns the probe that was previously set by the SHtml.hidden function (see below)
    val probe = probeVar.is
    //  SHtml.hidden registers a function that will be called when the form is submitted
    // In this case it sets the probeVar in the subsequent PUT request to contain the probe that's been
    // created in this request. This line isn't strictly necessary as if it was omitted, on the subsequent
    // request, a new probe instance would be created, and the name member variable on this new instance
    // would be set to contain the submitted name (see comments below). However, including this line
    // avoids creating a new probe object every time, and hence avoid unnecessary GC.
    // Notice that SHtml.hidded calls 'probeVar(probe)' not 'probeVar(probeVar.is)'
    // this is because we want the probeVar.is function to be evaluated in the scope of the initial request
    // not the scope of the subsequent request.
    "#hidden" #> SHtml.hidden(() => probeVar(probe) ) &
      // When the page is rendered:
      // "SHtml.text(probeVar.is.probeName" - this sets the value of the probe name HTML input field to the current
      // value of the Probe.probeName member held in the probeVar.
      // "name => probeVar.is.probeName(name)" - this registers a function that Lift will call
      // when the user clicks submit. This function sets Probe.probeName in the probeVar
      // in the subsequent request to the value the user entered in the HTML name input field
      "#ssid" #> SHtml.text(probeVar.is.wifi_ssid.value, W_ssid => probeVar.is.wifi_ssid(W_ssid) ) &
      "#password"  #> SHtml.password(probeVar.is.wifi_password.value, W_pass => probeVar.is.wifi_password(W_pass) ) &
      "#probename" #> SHtml.text(probeVar.is.probeName.value, name => probeVar.is.probeName(name), pairToBasic("autofocus","autofocus") ) &
      //      "#scalingvoltage" #> SHtml.text(probeVar.ScalingFactorVoltage.value.toString, volt =>  probeVar.ScalingFactorVoltage(BigDecimal(volt)) ) &
      //      "#scalingcurrent" #> SHtml.text(probeVar.ScalingFactorCurrent.value.toString, current =>  probeVar.ScalingFactorCurrent(BigDecimal(current)) ) &
      //      "#errorthreshold" #> SHtml.number(probeVar.is.ErrorThreshold.value, errThreshold => probeVar.is.ErrorThreshold(errThreshold), 0, 100000 ) &
      //      "#signalthreshold" #> SHtml.number(probeVar.is.SignalThreshold.value, sigThreshold => probeVar.is.SignalThreshold(sigThreshold), 0, 100000 ) &
      "#maxprobechannels" #> SHtml.select(channelopts, Box.legacyNullTest("8"),  channels => probeVar.is.NumberOfChannels(channels.toInt) ) &
      // This registers a function for Lift to call when the user clicks submit
      "#deviceType" #> SHtml.select(devices,Box.legacyNullTest(devices.head._1),(device)=>{probeVar.is.DeviceID_FK(device)}) &
      "#scalingvoltage"  #> SHtml.number(probeVar.is.ScalingFactorVoltage.value.toDouble,(sFV : Double)=>probeVar.is.ScalingFactorVoltage(sFV),0.0, Long.MaxValue.toDouble,0.0000001) &
      "#scalingcurrent"  #> SHtml.number(probeVar.is.ScalingFactorCurrent.value.toDouble,(sFC : Double)=>probeVar.is.ScalingFactorCurrent(sFC),0.0, Long.MaxValue.toDouble,0.0000001) &
    // This registers a function for Lift to call when the user clicks submit
      "#submit" #> SHtml.onSubmitUnit(processSubmit)
  }
  //takes a conf and returns the user with the config
  def getProbeChannels(e : Probe): List[(String,String)] ={
    //    val probe = Probe.findAll(("_id",e.NumberOfChannels.toString)).flatMap(probe => g(probe))
    val probe  = List (e.id.toString -> e.NumberOfChannels.toString)
    println("e "+probe)
    probe
  }


  // called from /system/probe/editprobe.html
  def edit = {
    if ( ! probeVar.set_? )
      S.redirectTo("/system/probe/listprobe")

    def manOf[T: Manifest](t: T): Manifest[T] = manifest[T]
    val params = S.request.map(_.request.param("action")) openOr ("Undefined")
    val action = params.asInstanceOf[List[String]].headOption.getOrElse("Undefined action")
    val vieworedit = action match {
      case "Edit" =>
        pairToBasic("enabled","disabled")
      case _ =>
        pairToBasic("disabled","disabled")
    }
    val probe = probeVar.is

    /*** @param t threshold to be added*/
    def addThreshold(t:Int){
      threshold = t :: threshold
    }
    /*** @param dv default voltage to be added*/
    def addDV(dv:Int){
      defaultVoltage = dv :: defaultVoltage
    }
    /*** @param pf power factor to be added*/
    def addPF(pf:Double){
      powerFactor = pf :: powerFactor
    }

    println("DEBUG GOOD")
    println("\n\n\n"+probeVar.is.Channels+"\n\n"+probeVar.is.Channels.get(0).DefaultVoltage.value+"\n\n\n")

    "#hidden" #> SHtml.hidden(() => probeVar(probe) ) &
      "#heading_action" #> SHtml.span(<h5 class="panel-title" id="heading_action">{action} {probeVar.is.NumberOfChannels} channel probe {probeVar.is.probeName}</h5>, (Noop)  ) &
      "#channels" #> probeVar.is.Channels._1.map( ch =>
        ".chName" #> SHtml.text("Channel"+ch.sourceChannel.toString, _ =>{},pairToBasic("disabled","disabled"))&
        ".th" #> SHtml.number(ch.ChannelThreshold.value, addThreshold(_), 0, 100, vieworedit )&
        ".pf" #> SHtml.number(ch.PowerFactor.value.toDouble, addPF(_), 0.5, 1, 0.001, vieworedit )&
        ".dv" #> SHtml.number(ch.DefaultVoltage.value, addDV(_),  0, 100000, vieworedit )&
        ".SFVoltage" #> SHtml.number(ch.ScalingFactorVoltage.value.toDouble, (sFV : Double)=>{probeVar.is.Channels.get(ch.sourceChannel.value).ScalingFactorVoltage(sFV)},0.0, Long.MaxValue.toDouble,0.0000001,vieworedit)&
        ".SFCurrent" #> SHtml.number(ch.ScalingFactorCurrent.value.toDouble, (sFCurrent : Double)=>{probeVar.is.Channels.get(ch.sourceChannel.value).ScalingFactorCurrent(sFCurrent)}, 0.0, Long.MaxValue.toDouble,0.0000001,vieworedit)&
        ".sensor" #> SHtml.select(sensors,Box.legacyNullTest(ch.SensorID_FK.value),(s)=>{probeVar.is.Channels.get(ch.sourceChannel.value).SensorID_FK(s)},vieworedit)
      )&
      "#probename" #> SHtml.text(probeVar.is.probeName.value, ( name : String ) => probeVar.is.probeName(name), vieworedit ) &
      "#ssid" #> SHtml.text(probeVar.is.wifi_ssid.value, ( W_ssid : String )=> probeVar.is.wifi_ssid(W_ssid), vieworedit ) &
      "#password"  #> SHtml.password(probeVar.is.wifi_password.value, ( W_pass : String )=> probeVar.is.wifi_password(W_pass), vieworedit ) &
      "#maxprobechannels" #> SHtml.select(getProbeChannels(probeVar), Box.legacyNullTest("8"),  channels => { println("WTF"+channels)},  vieworedit ) &
      "#scalingvoltage"  #> SHtml.number(probeVar.is.ScalingFactorVoltage.value.toDouble,(sFV : Double)=>probeVar.is.ScalingFactorVoltage(sFV),0.0, Long.MaxValue.toDouble,0.0000001,vieworedit) &
      "#scalingcurrent"  #> SHtml.number(probeVar.is.ScalingFactorCurrent.value.toDouble,(sFC : Double)=>probeVar.is.ScalingFactorCurrent(sFC),0.0, Long.MaxValue.toDouble,0.0000001,vieworedit) &
      "#deviceType" #> SHtml.select(devices,Box.legacyNullTest(probeVar.is.DeviceID_FK.value),(device)=>{probeVar.is.DeviceID_FK(device)},vieworedit) &
      "#submit" #> SHtml.onSubmitUnit(processSubmit)
      /*"#updatefirmware" #> SHtml.onSubmitUnit(updateFirmware())*/
  }


  // called from /system/probe/listprobe.html
  def list = {
    logger.info("List Probes")
    val userprobe_ids = findChannels(Configuration.findAll("UserID_FKs",currentUserId).map{ c => c.id.toString}).map{ p_ch => p_ch._1.split("_")(0)}.distinct
    val userprobes = userprobe_ids.map{ probe_id => Probe.findAll("_id",probe_id)}.flatten
    userprobe_ids.foreach(println)
    val user = User.find("_id",new ObjectId(currentUserId)).head.username
    "#heading_user" #> SHtml.span(<h5 class="alt" id="heading_user">Probes with channels associated with {user}</h5>, (Noop)  ) &
      "#probelistuser *" #> userprobes.map( p => {
        ".probeName *" #> Text(p.probeName.value) &
          ".actions *" #> {SHtml.link("/system/probe/editprobe?action=View", () => probeVar(p), Text("View")) ++ Text(" ") ++
            SHtml.link("/system/probe/editprobe?action=Edit", () => probeVar(p), Text("Edit")) ++ Text(" ") ++
            SHtml.link("/system/probe/deleteprobe", () => probeVar(p), Text("Delete"))}
      } )&
      "#probelist *" #> Probe.findAll.map( p => {
        /*      if (!userHasRole("admin")) {*/
        ".probeCheck *" #> SHtml.checkbox(false, if (_) ()=> {},"value" -> p.probeName.value, "name"->"Probes", "class"->"check") &
          ".probeName *" #> Text(p.probeName.value) &
          ".actions *" #> {SHtml.link("/system/probe/editprobe?action=View", () => probeVar(p), Text("View")) ++ Text(" ") ++
            SHtml.link("/system/probe/editprobe?action=Edit", () => probeVar(p), Text("Edit")) ++ Text(" ") ++
            SHtml.link("/system/probe/deleteprobe", () => probeVar(p), Text("Delete"))}
      } )
  }

  // called from /system/probe/deleteprobe.html
  def delete = {
    var p = probeVar.is
    val delProbe = S.params("delP").headOr("") //get the id if specified
    if(delProbe.nonEmpty) {
      object delProbeVar extends RequestVar[Probe](Probe.findAll("_id", delProbe.toString).headOption.getOrElse(Probe))
      p = delProbeVar.is
    }
    S.set("delprobeID",p.id.value.toString)
    def deleteProbe(){
      S.set("delprobeID","")
      Generator.findAll("ProbeID_FK",p.id.toString()).map{s =>
        s.delete_!
      }
      p.delete_!
    }
    def cancel(){
      S.set("delprobeID","")
    }
    getAssociatedSources(p.id.value.toString)
    "#probename" #> p.probeName &
      "#yes" #> SHtml.link("/system/probe/listprobe", () =>{
        deleteProbe() }, Text("Delete")) &
      "#no" #> SHtml.link("/system/probe/listprobe", () =>{ }, Text("Cancel"))
  }

  def getAssociatedSources(p_id:String):List[String]={
    val sourceIDs = Probe.findAll("_id",p_id).flatMap{ probe =>
      println("probename "+probe.probeName)
      probe.Channels.get.map{ channel =>
        if(!channel.SourceID_FK.toString.isEmpty){
          println(" channels assigned to phase "+channel.phase)
          channel.SourceID_FK.toString
        }
      }.filter(_ !=(()))
    }.distinct
    sourceIDs.foreach(println)
    var sourceNames:List[String] = Nil
    val sources = new StringBuilder
    sourceIDs.map { sourceID =>
      println(sourceID)
      Generator.findAll("_id", sourceID).map { s =>
        sourceNames = s.sourceName.toString() :: sourceNames
        sources ++= """{'name':'%s','id':'%s','type':'%s'},""".format(s.sourceName.toString(), s.id.toString(), s.Type.value.toString)
      }
    }
    val s = sources.take(sources.length-1).toString()
    S.appendJs(Run("""$("#probeSources").trigger('setSources',{'data' :[%s]});""".format(s)))
    sourceNames
  }

  def userHasRole(role:String):Boolean ={
    println(User.find("_id",new ObjectId(currentUserId)).headOption.getOrElse(User).roles)
    return User.find("_id",new ObjectId(currentUserId)).headOption.getOrElse(User).roles.names.contains(role)
  }

}