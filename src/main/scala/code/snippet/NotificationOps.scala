package code.snippet


/**
 * Created by zezzy on 10/30/15.
 */

import net.liftweb.http.js.JsCmds.Run
import net.liftweb.http.{RequestVar, S, SHtml, NamedCometActorTrait}
import net.liftweb.util.Helpers._
import code.model._

/**
  * This class handles notifications
  */
class NotificationOps{
  object noticeVar extends RequestVar[NotificationConfiguration](NotificationConfiguration.createRecord)//notification variable
  val currentUserId = S.get("selectedUserID").toOption.getOrElse(User.currentUserId.headOption.getOrElse(""))

  def processSubmit(sn:List[String]){
    val xBelowDoD = S.params("dodBelow")
    val xAboveDoD = S.params("dodAbove")
    val aboveDoDWithModulus = S.params("dodAboveAnd")
    val thresholdInv = S.params("thresholdInv")
    val thresholdGrid = S.params("thresholdGrid")
    val thresholdGen = S.params("thresholdGen")
    val thresholdGenState = S.params("thresholdGenSt")
    val operatingWindow = S.params("oWindow")
    val sourceTransitions = getTransition(sn)
    val phoneLists:List[String] = S.params("phones")(0).trim.split('\n').toList
    val emailLists:List[String] = S.params("emails")(0).trim.split('\n').toList

println("\nhhi:\n")
    println(xBelowDoD)
    println(xAboveDoD)
    println(aboveDoDWithModulus)
    //set source transition configuration
    var sTR:List[sourceTransition] = Nil
    sourceTransitions.foreach { t =>
      val sourceTransTmp = sourceTransition.createRecord
      sourceTransTmp.sourceName(t._1)
      t._2.foreach {
        case "flag" => sourceTransTmp.flag(true)
        case "email" => sourceTransTmp.email(true)
        case "sms" => sourceTransTmp.sms(true)
      }
      sTR = sourceTransTmp :: sTR
    }
       //add the records to sourceTransitions
    noticeVar.sourceTransitions(sTR)
    //set operatingWindow notification methods
    operatingWindow.foreach {
      case "flag" => noticeVar.operatingWindow._1.flag(true)
      case "email" => noticeVar.operatingWindow._1.email(true)
      case "sms" => noticeVar.operatingWindow._1.sms(true)
    }

    //set x% below DoD value and notification method
    noticeVar.xBelowDoD._1.inValue(xBelowDoD.headOr("5").toInt)
    xBelowDoD.tail foreach {
      case "flag" => noticeVar.xBelowDoD._1.flag(true)
      case "email" => noticeVar.xBelowDoD._1.email(true)
      case "sms" => noticeVar.xBelowDoD._1.sms(true)
    }

    /** set x% above DoD value and notification  */
    noticeVar.xAboveDoD._1.inValue(xAboveDoD.headOr("5").toInt)
    xAboveDoD.tail.foreach {
      case "flag" => noticeVar.xAboveDoD._1.flag(true)
      case "email" => noticeVar.xAboveDoD._1.email(true)
      case "sms" => noticeVar.xAboveDoD._1.sms(true)
    }

    /** set above DoD and multiple of value, and notification method */
    noticeVar.aboveDoDWithModulus._1.inValue(aboveDoDWithModulus.headOr("5").toInt)
    aboveDoDWithModulus.tail.foreach {
      case "flag" => noticeVar.aboveDoDWithModulus._1.flag(true)
      case "email" => noticeVar.aboveDoDWithModulus._1.email(true)
      case "sms" => noticeVar.aboveDoDWithModulus._1.sms(true)
    }

    /** set inverter threshold value, and notification method */
    println("\n\n\nthreshold inverter: " + thresholdInv + "\n\n\n")
    noticeVar.thresholdInv._1.Value(thresholdInv.headOr("90").toInt)
    noticeVar.thresholdInv._1.inValue(thresholdInv.tail.headOr("5").toInt)
    println("\n\n\nthresholdInv: " +noticeVar.thresholdInv._1 + "\n\n\n" )
    println("\n\n\thresholdInv.tail.: " +thresholdInv.tail + "\n\n\n" )
    thresholdInv.tail.foreach {
      case "flag" => noticeVar.thresholdInv._1.flag(true)
      case "email" => noticeVar.thresholdInv._1.email(true)
      case "sms" => noticeVar.thresholdInv._1.sms(true)
      case _ => println("nothing")
    }

    /** set grid threshold value, and notification method */
    noticeVar.thresholdGrid._1.Value(thresholdGrid.headOr("90").toInt)
    noticeVar.thresholdGrid._1.inValue(thresholdGrid.tail.headOr("5").toInt)
    thresholdGrid.tail.foreach {
      case "flag" => noticeVar.thresholdGrid._1.flag(true)
      case "email" => noticeVar.thresholdGrid._1.email(true)
      case "sms" => noticeVar.thresholdGrid._1.sms(true)
      case _ => println("nothing")
    }

    /** set grid threshold value, and notification method */
    thresholdGenState.foreach{
      case "high" => noticeVar.thresholdGen._1.high(true)
      case "low" => noticeVar.thresholdGen._1.low(true)
    }
    noticeVar.thresholdGen._1.Value(thresholdGen.headOr("90").toInt)
    noticeVar.thresholdGen._1.inValue(thresholdGen.tail.headOr("5").toInt)
    thresholdGen.tail.foreach {
      case "flag" => noticeVar.thresholdGen._1.flag(true)
      case "email" => noticeVar.thresholdGen._1.email(true)
      case "sms" => noticeVar.thresholdGen._1.sms(true)
      case _ => println("nothing")
    }

    /** Resources */
    val InverterTime = S.params("InverterTime")
    val fuelBelowX = S.params("fuelBelowX")
    val batteryBelowX = S.params("batteryBelowX")
    val utilityBelowX = S.params("utilityBelowX")


    /** Quality */
    val PfBelowThreshold = S.params("PfBelowT") //power factor below threshold
    val voltageBelowThreshold = S.params("voltBelowT")
    val voltageXBelowThreshold = S.params("voltXBelowT")
    val voltageAboveThreshold = S.params("voltAboveT")
    val voltageXAboveThreshold = S.params("voltXAboveT")
    val zeroPhase = S.params("zeroPhase")

    /** System Error */
    val signalOverlap = S.params("signalOverlap")
    val invOutGrtIn = S.params("invOutGrtIn") //inverter output greater than input

/** Interver remaining time  */
    noticeVar.InverterTime._1.inValue(InverterTime.headOr("1").toInt)
    InverterTime.tail.foreach {
      case "flag" => noticeVar.InverterTime._1.flag(true)
      case "email" => noticeVar.InverterTime._1.email(true)
      case "sms" => noticeVar.InverterTime._1.sms(true)
    }

/** set fuelBelowX */
    noticeVar.fuelBelowX._1.inValue(fuelBelowX.headOr("5").toInt)
    fuelBelowX.tail.foreach {
      case "flag" => noticeVar.fuelBelowX._1.flag(true)
      case "email" => noticeVar.fuelBelowX._1.email(true)
      case "sms" => noticeVar.fuelBelowX._1.sms(true)
    }
/** set batteryBelowX */
    noticeVar.batteryBelowX._1.inValue(batteryBelowX.headOr("5").toInt)
    batteryBelowX.tail.foreach {
      case "flag" => noticeVar.batteryBelowX._1.flag(true)
      case "email" => noticeVar.batteryBelowX._1.email(true)
      case "sms" => noticeVar.batteryBelowX._1.sms(true)
    }
/** set utilityBelowX */
    noticeVar.utilityBelowX._1.inValue(utilityBelowX.headOr("5").toInt)
    utilityBelowX.tail.foreach {
      case "flag" => noticeVar.utilityBelowX._1.flag(true)
      case "email" => noticeVar.utilityBelowX._1.email(true)
      case "sms" => noticeVar.utilityBelowX._1.sms(true)
    }

/** set powerfactor */
    PfBelowThreshold.foreach {
      case "flag" => noticeVar.powerfactorBelowThreshold._1.flag(true)
      case "email" => noticeVar.powerfactorBelowThreshold._1.email(true)
      case "sms" => noticeVar.powerfactorBelowThreshold._1.sms(true)
    }
/** set voltageBelowThreshold */
    voltageBelowThreshold.foreach {
      case "flag" => noticeVar.voltageBelowThreshold._1.flag(true)
      case "email" => noticeVar.voltageBelowThreshold._1.email(true)
      case "sms" => noticeVar.voltageBelowThreshold._1.sms(true)
    }
/** set voltageXBelowThreshold  */
    noticeVar.voltageXBelowThreshold._1.inValue(voltageXBelowThreshold.headOr("5").toInt)
    voltageXBelowThreshold.tail.foreach {
      case "flag" => noticeVar.voltageXBelowThreshold._1.flag(true)
      case "email" => noticeVar.voltageXBelowThreshold._1.email(true)
      case "sms" => noticeVar.voltageXBelowThreshold._1.sms(true)
    }
/** set voltageAboveThreshold  */
    voltageAboveThreshold.foreach {
      case "flag" => noticeVar.voltageAboveThreshold._1.flag(true)
      case "email" => noticeVar.voltageAboveThreshold._1.email(true)
      case "sms" => noticeVar.voltageAboveThreshold._1.sms(true)
    }
/** set voltageXAboveThreshold  */
    noticeVar.voltageXAboveThreshold._1.inValue(voltageXAboveThreshold.headOr("5").toInt)
    voltageXAboveThreshold.tail.foreach {
      case "flag" => noticeVar.voltageXAboveThreshold._1.flag(true)
      case "email" => noticeVar.voltageXAboveThreshold._1.email(true)
      case "sms" => noticeVar.voltageXAboveThreshold._1.sms(true)
    }
/** set zeroPhase */
    zeroPhase.foreach {
      case "flag" => noticeVar.zeroPhase._1.flag(true)
      case "email" => noticeVar.zeroPhase._1.email(true)
      case "sms" => noticeVar.zeroPhase._1.sms(true)
    }
/** set signalOverlap */
    signalOverlap.foreach {
      case "flag" => noticeVar.signalOverlap._1.flag(true)
      case "email" => noticeVar.signalOverlap._1.email(true)
      case "sms" => noticeVar.signalOverlap._1.sms(true)
    }
/** set invOutGrtIn */
    invOutGrtIn.foreach {
      case "flag" => noticeVar.invOutGrtIn._1.flag(true)
      case "email" => noticeVar.invOutGrtIn._1.email(true)
      case "sms" => noticeVar.invOutGrtIn._1.sms(true)
    }

    /** set user id foreign key of configuration */
    noticeVar.userID_FK(currentUserId)

    println("\n\n\nphoneLists: " + noticeVar.phoneList._1+"\n\n\n")
    println("\n\n\nemailLists: " + noticeVar.emailList._1+"\n\n\n")

    var p:List[String] = Nil

    phoneLists.foreach(l => {
      p = """{"value":"%s"}""".format(l.trim)::p
    })
//
    var e:List[String] = Nil

    emailLists.foreach(l => {
      e = """{"value":"%s"}""".format(l.trim)::e
    })

    noticeVar.phoneList(p)
    noticeVar.emailList(e)

    noticeVar.validate match {
      case Nil => {
        NotificationConfiguration.findAll("userID_FK",noticeVar.is.userID_FK.value.toString).map(n => n.delete_!)
        noticeVar.save()
        println("NotificationConfiguration saved")
        S.redirectTo("/")
      }
      case _ => println("Notification not saved")
    }




    println("\n\nValues from Notification Configuration Html:")
    println("xBelowDoD.head : "+xBelowDoD.headOr(""))
    println("xBelowDoD : "+xBelowDoD)
    println("xAboveDoD : "+xAboveDoD)
    println("aboveDoDWithModulus : "+aboveDoDWithModulus)
    println("thresholdInv : "+thresholdInv)
    println("thresholdGrid : "+thresholdGrid)
    println("thresholdGen : "+thresholdGen)
    /*println("Transitions : "+sourceTransition)*/

  }


  /**returns list of tuple, each of source name and list of notification method*/
  def getTransition(sn:List[String]):List[(String,List[String])]={
    var sourceValues:List[(String,List[String])] = List()
    sn.map{s =>
      sourceValues = (s,S.params(s))::sourceValues
      s
    }
    sourceValues.filter(s => s._2.nonEmpty)
  }

  /**The direct point of contact with configuration HTML page*/
  def display={
    val sourceNames = getSources() //get list of source names
    if(NotificationConfiguration.find("userID_FK", currentUserId).isDefined) populateConfig()

    def pSubmit(){
      processSubmit(sourceNames)
    }

  "#saveNotifConfig" #> SHtml.onSubmitUnit(pSubmit)
  }

  /**triggers setSources,javascript,and sends list of sources as parameter(--source transition). Returns source names*/
  def getSources():List[String]={
  var sourceNames:List[String] = Nil
  val sources = new StringBuilder
  Configuration.findAll("UserID_FK",currentUserId).flatMap{c =>
      Probe.findAll("ConfigID_FK",c.id.toString()).map{p =>
          Generator.findAll("ProbeID_FK",p.id.toString()).map{s =>
            sourceNames = s.sourceName.toString() :: sourceNames
            sources ++= """{'name':'%s','id':'%s','type':'%s'},""".format(s.sourceName.toString(),s.id.toString(),s.Type.value)
          }
      }
   }
    val s = sources.take(sources.length-1).toString()
    S.appendJs(Run("""$("#sources").trigger('setSources',{'data' :[%s]});""".format(s)))
    sourceNames
  }

  //check if the user already has notification configuration
  def populateConfig() {
    def extractValues(fieldName:String, itm:xBelowDoDElems): String = {
      """ '%s' : {'Value' : %s,'inVal' : %s, "flag" : %s, "email" : %s, "sms" : %s}""".format(fieldName,itm.Value._1,itm.inValue._1,itm.flag._1,itm.email._1,itm.sms._1)
    }

    S.appendJs(Run("""$("#sources").trigger('setNConfig',{%s});""".format(
    NotificationConfiguration.find("userID_FK", currentUserId).map(c => {
      println("\n\n\nc.thresholdInv._1: " + c.thresholdInv._1 + "\n\n\n")
      extractValues("dodAbove",c.xAboveDoD._1)+","+
        extractValues("dodBelow",c.xBelowDoD._1)+","+
        extractValues("thresholdGrid",c.thresholdGrid._1)+","+
        extractValues("thresholdInv",c.thresholdInv._1)+","+
        extractValues("dodAboveAnd",c.aboveDoDWithModulus._1)+","+
        """ "thresholdGen" : {'Value' : %s, 'inVal' : %s, "flag" : %s, "email" : %s, "sms" : %s}""".format(c.thresholdGen._1.Value._1,c.thresholdGen._1.inValue._1,c.thresholdGen._1.flag._1,c.thresholdGen._1.email._1,c.thresholdGen._1.sms._1)+","+
        """ "oWindow" : {"flag" : %s, "email" : %s, "sms" : %s}""".format(c.operatingWindow._1.flag._1,c.operatingWindow._1.email._1,c.operatingWindow._1.sms._1)+","+
        """ "thresholdGenSt" : {"high" : %s, "low" : %s}""".format(c.thresholdGen._1.high._1,c.thresholdGen._1.low._1)+","+
        """ "sTransition" : [%s] """.format(setTransition(c.sourceTransitions._1))+","+
        extractValues("fuelBelowX",c.fuelBelowX._1)+","+
        extractValues("InverterTime",c.InverterTime._1)+","+
        extractValues("batteryBelowX",c.batteryBelowX._1)+","+
        extractValues("utilityBelowX",c.utilityBelowX._1)+","+
        extractValues("voltXBelowT",c.voltageXBelowThreshold._1)+","+
        extractValues("voltXAboveT",c.voltageXAboveThreshold._1)+","+
        """ "PfBelowT" : {"flag" : %s, "email" : %s, "sms" : %s}""".format(c.powerfactorBelowThreshold._1.flag._1,c.powerfactorBelowThreshold._1.email._1,c.powerfactorBelowThreshold._1.sms._1)+","+
        """ "voltBelowT" : {"flag" : %s, "email" : %s, "sms" : %s}""".format(c.voltageBelowThreshold._1.flag._1,c.voltageBelowThreshold._1.email._1,c.voltageBelowThreshold._1.sms._1)+","+
        """ "voltAboveT" : {"flag" : %s, "email" : %s, "sms" : %s}""".format(c.voltageAboveThreshold._1.flag._1,c.voltageAboveThreshold._1.email._1,c.voltageAboveThreshold._1.sms._1)+","+
        """ "zeroPhase" : {"flag" : %s, "email" : %s, "sms" : %s}""".format(c.zeroPhase._1.flag._1,c.zeroPhase._1.email._1,c.zeroPhase._1.sms._1)+","+
        """ "signalOverlap" : {"flag" : %s, "email" : %s, "sms" : %s}""".format(c.signalOverlap._1.flag._1,c.signalOverlap._1.email._1,c.signalOverlap._1.sms._1)+","+
        """ "invOutGrtIn" : {"flag" : %s, "email" : %s, "sms" : %s}""".format(c.invOutGrtIn._1.flag._1,c.invOutGrtIn._1.email._1,c.invOutGrtIn._1.sms._1)+","+
        """ "phoneList" : [%s] """.format(setList(c.phoneList._1))+","+
        """ "emailList" : [%s] """.format(setList(c.emailList._1))
    }).toOption.headOption.getOrElse(""))))
  }


  def setTransition(items:List[sourceTransition]):String = {
    var str = new StringBuilder()
   for(itm <- items){
     str++= """,{'sourceName': '%s', 'flag' : %s, 'email' : %s, 'sms' : %s}""".format(itm.sourceName._1,itm.flag._1,itm.email._1,itm.sms._1)
   }
    str.toString().replaceFirst(",","")
  }

  def setList(items:List[String]):String = {
    var str = ""//new StringBuilder()
    for(itm <- items){
      if(itm == ""){
        str ++= """,%s""".format("")
      }
      else {
        str ++= """,%s""".format(itm.toString.trim)
      }
      println("list: " + itm)
    }
    println("str: " + str)
    str.toString().replaceFirst(",","")

  }

  def setPhoneList(items:List[String]):String = {
    var str = new StringBuilder()
    for(itm <- items){
      if(itm == ""){
        str ++= """,{'value':%s}""".format("")
      }
      else {
        str ++= """,{'value':%s}""".format(itm.toString.trim())
      }
      println("list: " + itm)
    }
    println("str: " + str)
    str.toString().replaceFirst(",","")

  }

}
