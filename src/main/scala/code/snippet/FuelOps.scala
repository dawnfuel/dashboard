package code.snippet

import java.util.Date

import code.model._
import net.liftweb.common.{Empty, Box}
import net.liftweb.http.SHtml._
import net.liftweb.http.js.JsCmds.After
import code.model.{Fuel,User,Generator}
import net.liftweb.http.{SHtml, S, RequestVar}
import net.liftweb.util.Helpers
import Helpers._
import org.bson.types.ObjectId
import org.elasticsearch.common.transport.InetSocketTransportAddress

import scala.xml.Text
import org.elasticsearch.client.Client
import org.elasticsearch.client.transport.TransportClient
import org.elasticsearch.common.xcontent.XContentFactory.jsonBuilder
import code.rest.ESQueryAPI._
import net.liftweb.json.JsonDSL._
/**
  * Created by zezzy on 9/30/15.
  *
 */
/**
  *A class that handles fuel operations
  *
  *@constructor creates new fuel object
  */

class FuelOps {
  /** id of current user */
  val currentUserId = S.get("selectedUserID").toOption.getOrElse(User.currentUserId.headOption.getOrElse(""))
  /** email of current user*/
  var currentUserEmail = User.find("_id",new ObjectId(currentUserId)).headOption.getOrElse(User).email.toString()
  /** flag, determines what process is performed (create or edit) */
  var process = "create"
  /** fuel record object */
  object fuelVar extends RequestVar[Fuel](Fuel.createRecord)
  /** generator record object */
  object genVar extends RequestVar[Generator](Generator.createRecord)
  var (fuelType,unitPrice )=("",0.0)
  var litre = 0.0


  /**
    * get list of generators in config
    */

  case class Item(id: String, name: String)
  def g(v:Probe) = List(Item(v.id.toString, v.probeName.toString))
  def h(v:Generator) = List(Item(v.id.toString, v.sourceName.toString))
  def manOf[T: Manifest](t: T): Manifest[T] = manifest[T]
  val currentConfs = Configuration.findAll("UserID_FKs",currentUserId)
  println("CONFs of LOGGED IN USER "+currentConfs)
  var probeList : List[Item] = List()
  val probesLoggedIn = currentConfs.map( x => {
    println(x)
    probeList  = probeList  ::: Probe.findAll(("ConfigID_FK" -> x.id.toString)).flatMap(p => g(p))
  })

  var genList : List[Item] = List()
  val generators = probeList.map( x => {
    println(x)
    genList = genList ::: Generator.findAll(("ProbeID_FK"-> x.id.toString)~("Type" -> "generator")).flatMap(p => h(p))
  })



  /** processes form on submit, according to `process` above*/
  def processSubmit(s : String){


    fuelVar.is.UserID_FK(currentUserId)
    fuelVar.is.FuelType(fuelType)
    fuelVar.is.FuelUnitPrice(unitPrice)

    fuelVar.is.validate match {
      case  Nil => {
        fuelVar.is.save()

        if(process == "edit"){
          Generator.findAll("FuelID_FK",fuelVar.is.id.value).map{gen=>
            gen.FuelType(fuelType)
            gen.FuelUnitPrice(unitPrice)
          }
        }

        try {
          val (eshost,esjavaport,eshttpport) = getESparams()
          val client: Client =TransportClient.builder().build().addTransportAddress(new InetSocketTransportAddress(java.net.InetAddress.getByName(eshost), esjavaport))
          val response = client.prepareIndex("grit", "fuel")
            .setSource(jsonBuilder()
              .startObject()
              .field("UserID_FK", fuelVar.is.UserID_FK.value)
              .field("FuelType", fuelVar.is.FuelType.value)
              .field("FuelUnitPrice", fuelVar.is.FuelUnitPrice.value)
              .field("time", new Date())
              .endObject()
            )
            .execute()
            .actionGet()
        }catch{
          case e:Exception => println("fuel, couldn't connect to elasticsearch")
        }

        S.notice("fuel Saved")
        S.seeOther("/system/fuel/listfuel")
        println("\n\nsaved fuel")

      }
      case errors => S.error(errors)
        println("\n\nError!\n")
    }
  }

  /**@param price the price per unit fuel*/
  def setprice(price : Double):Any={
    unitPrice = price
  }

/**  @return Returns a node sequence. */
  def setlitre(size : Double):Any={
    litre = size
  }

  def create = {
    val fuel = fuelVar.is
    "#hidden" #> SHtml.hidden(() => fuelVar(fuel) ) &
      "#unitPrice" #> SHtml.number(75.0, setprice(_),0.0,100000.0,0.5)&
    "#fuelType" #> SHtml.text("",fuelType = _)&
      "#submit" #> SHtml.onSubmit(processSubmit)
  }

  /**  @return Returns a node sequence. */
  def refill = {
    var selected: Box[String]  = Empty
    val options : List[(String,String)] =
      genList.map(i => (i.id -> i.name))
      println (options)

    def selection (list: List[(String,String)]) = options match {
      case s :: rest => selected = Box[String](options.head._1)
      case Nil => ""
    }



      "#powerSource" #> SHtml.select(options,selected, { s => After(200, setFuel(s))}) &
      "#litre" #> SHtml.number(50.0,setlitre(_),0.0,100000.0,0.5)&
      "#submit" #> SHtml.onSubmit(processRefill)
  }

  def processRefill(s : String){
    val availableLitre = Generator.findAll("_id", genVar.is.id.value)(0).FuelStore.get
    val currentLitre = availableLitre + litre
/*    Probe.findAll("_id", rawWord._1)(0).dataExpiry(extended.toDate).save()*/
    Generator.findAll("_id", genVar.is.id.value)(0).FuelStore(currentLitre.toInt).save()
  }

  def setFuel(f_id:String){
    genVar.is.id(f_id)
  }

  def list = {
    /** for each Configuration object in the database render the name along with links to view, edit, and delete **/
    "#fuellist *" #> Fuel.findAll("UserID_FK", currentUserId).map( t => {
      ".fuelType *" #> Text(t.FuelType.value) &
        ".actions *" #> {SHtml.link("/system/fuel/viewfuel", () => fuelVar(t), Text("View")) ++ Text("  ") ++
          SHtml.link("/system/fuel/editfuel", () => fuelVar(t), Text("Edit")) ++ Text("  ") ++
          SHtml.link("/system/fuel/deletefuel", () => fuelVar(t), Text("Delete"))}
    } )
  }

  /**  @return Returns a node sequence. */
  def edit = {
    process = "edit"
    if ( ! fuelVar.set_? )
      S.redirectTo("/system/fuel/listfuel")

    val fuel = fuelVar.is
    fuelVar.is.UserID_FK(currentUserId)
    "#hidden" #> SHtml.hidden(() => fuelVar(fuel) ) &
      "#unitPrice" #> SHtml.number(fuel.FuelUnitPrice.value.toDouble, setprice(_),0.0,100000.0,0.5)&
      "#fuelType" #> SHtml.text(fuel.FuelType.value,fuelType = _)&
      "#submit" #> SHtml.onSubmit(processSubmit)&
    "#cancel" #> SHtml.onSubmitUnit(redirect)
  }

  /**  @return Returns a node sequence. */
  def view = {
    if ( ! fuelVar.set_? )
      S.redirectTo("/system/fuel/listfuel")

    val fuel = fuelVar.is
    "#fuelType *" #> fuelVar.is.FuelType.asHtml &
      "#edit" #> SHtml.link("/system/fuel/editfuel", () => (fuelVar(fuel)), Text("edit"))
  }


  def delete_fuel_gens(e : Fuel) : Any =  {
    println(e.id)
    Generator.findAll("FuelID_FK",e.id.value).map{gen =>
      gen.FuelID_FK("")
      gen.FuelType("")
      gen.FuelUnitPrice(0.0)
      gen.update
    }
  }


/** called from /system/configuration/deleteconfiguration.html */
  /**  @return Returns a node sequence. */
  def delete = {
    // we're expecting the configurationVar to have been set when we were linked to from the list page
    if ( ! fuelVar.set_? )
      S.redirectTo("/system/fuel/listfuel")

    var e = fuelVar.is
    "#fuelType" #> fuelVar.is.FuelType &
      "#yes" #> SHtml.link("/system/fuel/listfuel", () => { delete_fuel_gens(e);e.delete_!}, Text("Yes")) &
      "#no" #> SHtml.link("/system/fuel/listfuel", () =>{ }, Text("No"))
  }

  /**  @return Returns a node sequence. */
  def redirect()={
    S.seeOther("/system/fuel/listfuel")
  }
}
