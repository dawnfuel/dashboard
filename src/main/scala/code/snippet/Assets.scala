package code
package snippet

import net.liftweb.common._
import net.liftweb.http.LiftRules
import net.liftweb.json._
import net.liftweb.http.S
import net.liftweb.util.Helpers._
import net.liftweb.util.Props

/**
  *This snippet maps the property file from the 'Assets' directory to a CSS file in the 'Styles' directory, also JS file in the 'scripts' directory
  *
  *
  */

object Assets {
  private lazy val assetsMap: Map[String, String] = {
    if (Props.mode == Props.RunModes.Development)
      Map.empty
    else {
      (LiftRules
        .loadResourceAsString("/assets.json")
        .flatMap { s => tryo(JsonParser.parse(s)) }
      ) match {
        case Full(jo: JObject) => jo.values.mapValues(_.toString)
        case _ => Map.empty
      }
    }
  }

  /**
    *
    * @param asset gets the asset path
    * @return asset path
    */
  private def assetPath(asset: String): String =
    assetsMap.getOrElse(asset, asset)

  /**
    *
    * @return selector source for css
    */

  def css = {
    "* [href]" #> assetPath(S.attr("src").getOrElse("/styles.min.css"))
  }

  /**
    *
    * @return  selector source for javascript
    */
  def js = {
    "* [src]" #> assetPath(S.attr("src").getOrElse("/scripts.min.js"))
  }
}
