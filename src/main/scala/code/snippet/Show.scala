package code
package snippet
import config.Site
import model._
import scala.xml._

import net.liftweb._
import common._
import http.{LiftScreen, S}
import util.FieldError
import util.Helpers._

import net.liftweb.util._
import net.liftweb.http._
import net.liftweb.http.SHtml._
/**
 * Created by yoda on 7/14/15.
 */
class Show {
  def users(xhtml: NodeSeq): NodeSeq = User.findAll.flatMap(user => bind("f",
    xhtml, "username" -> user.username, "email" -> user.email.toString))
    //User.findAll.flatMap(x => println(x))
}


class MultiSelectStateful extends StatefulSnippet with Loggable {
  def dispatch = {
    case _ => render
  }

  case class Item(id: String, name: String)
  def g(v:User) = List(Item(v.username.toString, v.email.toString))
  val users = User.findAll.flatMap(user => g(user))
  ///print ("\n\n\nUSERS :\n"+users+"\n\n")
  users.map(i => (i.id -> i.name))
  val options : List[(String,String)] =
    users.map(i => (i.id -> i.name))
  var current = users.head.id :: Nil
  print(current)
  def render = {
    def logSelected() =
      //print("\n SELECTED : "+current)
      logger.info("Values selected: "+current)
    "#multi *" #> (
      SHtml.hidden( () => current ) ++
        //multiSelect(opts: Seq[(String, String)], deflt: Seq[String], func: (List[String]) ⇒ Any, attrs: ElemAttr*): Elem
        //     select(opts: Seq[(String, String)], deflt: Box.legacyNullTest, func: (String) ⇒ Any, attrs: ElemAttr*): Elem
        //SHtml.select(options, Box.legacyNullTest(current), current => current)//, ElemAttr.apply("id","confops"))
        SHtml.multiSelect(options, current, current = _)//, ElemAttr.apply("id","confops"))

        //SHtml.select(List(("b","b"),("a","a")), Box.legacyNullTest(current), current => current)//, ElemAttr.apply("id","confops"))
      ) &
      "type=submit" #> SHtml.onSubmitUnit(logSelected)
  }
}