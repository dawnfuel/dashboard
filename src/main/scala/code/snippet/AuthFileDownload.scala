package code.snippet

import java.io.BufferedReader
import java.io.DataOutputStream
import java.io.InputStreamReader
import java.net.CookieHandler
import java.net.CookieManager
import java.net.URL
import java.util.List
import javax.net.ssl.HttpsURLConnection
import scala.collection.JavaConversions._
import java.io._


/**
  *Handles typeform authentication
  *
  */
object AuthFileDownloadObject {

  def load() {
    val authUrl = "https://admin.typeform.com/login_check"
    val downloadUrl = "https://admin.typeform.com/form/1949330/analyze/csv"
    val http = new AuthFileDownload()
    CookieHandler.setDefault(new CookieManager())
    val postParams= "_username=Tife1379%40gmail.com&_password=grit2016&_target_path=%2F%2Fadmin.typeform.com%2F&teamsWorkspaceInvitationToken="
    http.sendPost(authUrl, postParams)
    http.download(downloadUrl)
  }
}

/**
  * Sets params for url get requests
  */
class AuthFileDownload {

  var cookies: List[String] = _
  var conn: HttpsURLConnection = _
  val USER_AGENT = "Mozilla/5.0"
  val BUFFER_SIZE = 4096

  /**
    *
    * @param url post url
    * @param postParams parameter to post
    */
  def sendPost(url: String, postParams: String) {
    val obj = new URL(url)
    conn = obj.openConnection().asInstanceOf[HttpsURLConnection]
    conn.setUseCaches(false)
    conn.setRequestMethod("POST")
    conn.setRequestProperty("Host", "admin.typeform.com")
    conn.setRequestProperty("User-Agent", USER_AGENT)
    conn.setRequestProperty("Accept", "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8")
    conn.setRequestProperty("Accept-Language", "en-US,en;q=0.5")
    conn.setRequestProperty("Connection", "keep-alive")
    conn.setRequestProperty("Content-Type", "application/x-www-form-urlencoded")
    conn.setRequestProperty("Content-Length", java.lang.Integer.toString(postParams.length))
    conn.setDoOutput(true)
    conn.setDoInput(true)
    val wr = new DataOutputStream(conn.getOutputStream)
    wr.writeBytes(postParams)
    wr.flush()
    wr.close()
    val responseCode = conn.getResponseCode
    println("\nSending 'POST' request to URL : " + url)
    println("Post parameters : " + postParams)
    println("Response Code : " + responseCode)
    val in = new BufferedReader(new InputStreamReader(conn.getInputStream))
    in.close()
  }


  /**
    * @param cookies handles cookies set
    */
  def setCookies(cookies: List[String]) {
    this.cookies = cookies
  }

  /**
    * @return cookies
    */
  def getCookies(): List[String] =  {
    return this.cookies;
  }

  /**
    *
    * @param url Url instantiates new url and sets request
    *
    */
  def download (url: String) = {
    val obj = new URL(url)
    conn = obj.openConnection().asInstanceOf[HttpsURLConnection]
    conn.setRequestMethod("GET")
    conn.setUseCaches(false)
    conn.setRequestProperty("User-Agent", USER_AGENT)
    conn.setRequestProperty("Accept", "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8")
    conn.setRequestProperty("Accept-Language", "en-US,en;q=0.5")
    if (cookies != null) {
      for (cookie <- this.cookies) {
        conn.addRequestProperty("Cookie", cookie.split(";", 1)(0))
      }
    }
    val outputStream = new FileOutputStream("output.csv")
    val bytes = new Array[Byte](1024) //1024 bytes - Buffer size
    Iterator
      .continually (conn.getInputStream.read(bytes))
      .takeWhile (-1 !=)
      .foreach (read=>outputStream.write(bytes,0,read))
    outputStream.close()
    conn.getInputStream.close()

    println("File downloaded")
  }
}

