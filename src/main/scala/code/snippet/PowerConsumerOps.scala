package code.snippet

/**
 * created by yoda on 03/03/16.
 *
 *  GRIT Systems Engineering Proprietary and Confidential
 * ©2016
 *
 *   This source code is copyrighted. Copying and/or transmitting
 *   portions or all of this work without permission is a violation of applicable law.
 *   No use of this publication may be made for resale or for any other commercial
 *   purpose whatsoever without prior permission in writing from GRIT Systems Engineering .
 *
 */

import org.bson.types.ObjectId
import net.liftweb._
import net.liftweb.http._
import util.{CssSel, _}
import common._
import js._
import JsCmds._
import JE._
import net.liftweb.http.js.{JsCmd, JsCmds}
import Helpers._
import net.liftweb.common.Logger

import scala.xml._
import net.liftweb.mongodb.BsonDSL._

import scala.xml.Text
import net.liftweb.http.SHtml.ajaxSelect
import net.liftweb.http.js.JsCmds.After
import code.model.{Configuration, Generator, PowerConsumer, PowerConsumerGroup, PowerConsumerLine, Probe, User}
import code.model.Probe._
import code.model.PowerConsumerGroup._
import code.model.PowerConsumerLine._
import code.model.PowerConsumer._
import net.liftweb.http.js.JsCmd._
import net.liftweb.util.Helpers._
import net.liftweb.http.js.JsCmds.Alert
import net.liftweb.http.S

import xml.NodeSeq
import code.lib.MSHtml
import net.liftweb.http.SHtml.ElemAttr._

/**
 * This class provides the snippets that back the Source CRUD
 * pages (List, Create, View, Edit, Delete)
 */
class PowerConsumerOps extends Loggable {
  val currentUserId = S.get("selectedUserID").toOption.getOrElse(User.currentUserId.headOption.getOrElse(""))
  var currentUserEmail = User.find("_id",new ObjectId(currentUserId)).headOption.getOrElse(User).email.toString()
  var probeID : String = ""
  var one, two, three  = "UNASSIGNED"
  case class Item(id: String, name: String)
  def g(v:PowerConsumerGroup) = List(Item(v.id.toString, v.Name.toString))
  def l(v:PowerConsumerLine) = List(Item(v.id.toString, v.Name.toString))
  def p(v:Probe) = List(Item(v.id.toString, v.probeName.toString))
  def manOf[T: Manifest](t: T): Manifest[T] = manifest[T]
  var listgroup : List[Item] = List()
  var listprobe : List[Item] = List()
  var listline : List[Item] = List()
  /**
    * The PowerConsumerGroupVar object is used to pass Source instances between successive HTML requests
    * in the following cases:
    * - when a create or edit form is submitted
    * - from a list page to a delete, view or edit page
    * - from a view page to a edit page
    * Note that Source.create isn't being called here. Rather, we're registering
    * a function for PowerConsumerGroupVar to call to get a default value in the case that the
    * PowerConsumerGroupVar object is accessed before it is set.
    *
    */

  object PowerConsumerGroupVar extends RequestVar[PowerConsumerGroup](PowerConsumerGroup.createRecord)
  object PowerConsumerLineVar extends RequestVar[PowerConsumerLine](PowerConsumerLine.createRecord)
  object PowerConsumerVar extends RequestVar[PowerConsumer](PowerConsumer.createRecord)

  def processSubmitGroup() = {
    logger.info("\nPower Consumer UserGroup Create Form Submitted")
    PowerConsumerGroupVar.is.validate match {
      case  Nil => {
        PowerConsumerGroupVar.is.save()
        S.notice("Consumer Saved")
        S.seeOther("/system/consumer/listconsumers")
      }
      case errors => S.error(errors)
    }
    logger.info("Power Consumer UserGroup Save Success")
    /** exiting the function here causes the form to reload */
  }
  def set_ConfigID_FK( assignedConfigID : String ) : Any =  {
    PowerConsumerGroupVar.is.ConfigID_FK(assignedConfigID)
    logger.info("\nAssigned Config "+assignedConfigID+" to consumer group "+PowerConsumerGroupVar.is.Name.value+" ")
  }
  def set_groupType( assignedType : String ) : Any = {
    PowerConsumerGroupVar.is.Type(assignedType)
    logger.info("\nAssigned type "+assignedType+" to consumer group "+PowerConsumerGroupVar.is.Name.value+" ")
  }

  /** called from /system/source/createsource.html */
  def editGroup = {
    println()
    logger.info("Loaded Edit Power Consumer UserGroup")
    logger.info("Configurations associated with "+currentUserId+" : ")
    val params = S.request.map(_.request.param("groupID")) openOr ("Undefined")
    val g_id = params.asInstanceOf[List[String]](0)
    val groupname = PowerConsumerGroup.findAll("_id",g_id).head.Name.toString
    val configlist = Configuration.findAll("UserID_FKs",currentUserId).map( x => { (x.id.toString,x.configurationName.toString) } )
    configlist.foreach(logger.info(_))
    val typelist = List(("Estate","Estate"),("Structure","Structure"),("Floor","Floor"),("Section","Section"),("Room","Room"),("Subsection","Subsection"))
    val consumerGroup = PowerConsumerGroupVar.is
    "#hidden" #> SHtml.hidden(() => PowerConsumerGroupVar(consumerGroup) ) &
      "#groupName" #> SHtml.text(groupname, name => PowerConsumerGroupVar.is.Name(groupname), pairToBasic("disabled","disabled") ) &
      "#groupType" #> SHtml.select(typelist, Box.legacyNullTest[String](PowerConsumerGroupVar.is.Name.value), set_groupType ) &
      "#groupConfig" #> SHtml.select(configlist, Box.legacyNullTest[String](PowerConsumerGroupVar.is.ConfigID_FK.value), set_ConfigID_FK ) &
      "#submit" #> SHtml.onSubmitUnit(processSubmitGroup)

  }
  /** This is a brief description of what's being documented.
    *
    * This is further documentation of what we're documenting.  It should
    * provide more details as to how this works and what it does.
    */
  def createGroup = {
    val configlist = Configuration.findAll("UserID_FKs",currentUserId).map( x => { (x.id.toString,x.configurationName.toString) } )
    println()
    logger.info("Loaded Create Power Consumer UserGroup")
    logger.info("Configurations associated with "+currentUserId+" : ")
    configlist.foreach(logger.info(_))
    val typelist = List(("Estate","Estate"),("Structure","Structure"),("Floor","Floor"),("Section","Section"),("Room","Room"),("Subsection","Subsection"))
    val consumerGroup = PowerConsumerGroupVar.is
    "#hidden" #> SHtml.hidden(() => PowerConsumerGroupVar(consumerGroup) ) &
      "#groupName" #> SHtml.text(PowerConsumerGroupVar.is.Name.value, name => PowerConsumerGroupVar.is.Name(name) ) &
      "#groupType" #> SHtml.select(typelist, Box.legacyNullTest[String](PowerConsumerGroupVar.is.Name.value), set_groupType ) &
      "#groupConfig" #> SHtml.select(configlist, Box.legacyNullTest[String](PowerConsumerGroupVar.is.ConfigID_FK.value), set_ConfigID_FK ) &
      "#submit" #> SHtml.onSubmitUnit(processSubmitGroup)
  }

  def processSubmitConsumer() = {
    logger.info("\nPower Consumer Create Form Submitted")
    PowerConsumerVar.is.validate match {
      case  Nil => {
        PowerConsumerVar.is.save()
        S.notice("Consumer Saved")
        S.seeOther("/system/consumer/listconsumers")
      }
      case errors => S.error(errors)
    }
    logger.info("Power Consumer Save Success")
  }

  def set_consumerType ( assignedType : String ) : Unit = {//List[JsCmd]  = {
    println()
    logger.info("Set Consumer Type "+assignedType)
    PowerConsumerVar.is.NumberOfPhases(assignedType.toInt)
    //return List(ReplaceOptions("consumerLine", listprobe, Box.legacyNullTest(List(defaultProbe._1))) :: replaceAllChannels(defaultProbe._1))
  }
  def get_consumerPhaseOptions ( assignedLine : String ) : List[(String,String)] = {
    val phaseoptions = PowerConsumerLine.findAll("_id",assignedLine).last.NumberOfPhases.value match {
      case 3 => {
        List(("3","3 Phase"),("1","1 Phase"))
      }
      case 1 => {
        List(("1","1 Phase"))
      }
      case _ => {
        List(("1","1 Phase"))
      }
    }
    phaseoptions
  }
  def set_consumerLine ( assignedLine : String ) : List[JsCmd]  = {
    println()
    logger.info("Set Consumer Line "+assignedLine)
    val phaseoptions = get_consumerPhaseOptions(assignedLine)
    logger.info("Type/Phase Options   : "+phaseoptions.headOption.getOrElse("","")._1.toInt)
    PowerConsumerVar.is.LineID_FK(assignedLine)
    return List(ReplaceOptions("consumerType", phaseoptions, Box.legacyNullTest[String]("")))
  }


  def createConsumer = {
    /**
      * When create is called on the first GET request, PowerConsumerGroupVar.is initialises PowerConsumerGroupVar by calling Source.create.
     If create is being called due to a form reload after validation has failed on a PUT request,
    PowerConsumerGroupVar.is returns the source that was previously set by the SHtml.hidden function (see below)
      */

    val currentConfs = Configuration.findAll("UserID_FKs",currentUserId)
    currentConfs.map( x => {
      listgroup = listgroup ::: PowerConsumerGroup.findAll(("ConfigID_FK" -> x.id.toString)).flatMap(a =>g(a))
    })
    listgroup.map( x => {
      listline = listline ::: PowerConsumerLine.findAll("GroupID_FK", x.id).flatMap(a =>l(a))
    })
    println("ll "+listline+" ")
    if(listline.isEmpty){
      logger.error("No Consumer Lines Available")
      logger.error("Please Create a Consumer Line Before Creating a Consumer")

      S.redirectTo("/system/consumer/listconsumers",()=>S.error("NO CONSUMER LINE AVAILABLE, CREATE LINE BEFORE CREATING CONSUMER"))
    }else {
      println()
      logger.info("Loaded Create Power Consumer")
      logger.info("Configurations associated with " + currentUserId + " : ")
      currentConfs.foreach(logger.info(_))
      logger.info("Groups associated with " + currentUserId + " : ")
      listgroup.foreach(logger.info(_))
      logger.info("Lines associated with " + currentUserId + " : ")
      listline.foreach(logger.info(_))
      val optionsLine =
        listline.map(i => (i.id -> i.name))
      logger.info("LINE OPTIONS associated with " + currentUserId + " : ")
      optionsLine.foreach(println)
      val typelist = get_consumerPhaseOptions(optionsLine.headOption.getOrElse("", "")._1)
      PowerConsumerVar.is.LineID_FK(optionsLine.headOption.getOrElse("", "")._1)
      val consumer = PowerConsumerVar.is

      /**
        *  SHtml.hidden registers a function that will be called when the form is submitted
       *  In this case it sets the PowerConsumerGroupVar in the subsequent PUT request to contain the source that's been
       *  created in this request. This line isn't strictly necessary as if it was omitted, on the subsequent
       *  request, a new source instance would be created, and the name member variable on this new instance
       *  would be set to contain the submitted name (see comments below). However, including this line
       *  avoids creating a new source object every time, and hence avoid unnecessary GC.
       *  Notice that SHtml.hidded calls 'PowerConsumerGroupVar(source)' not 'PowerConsumerGroupVar(PowerConsumerGroupVar.is)'
       *  this is because we want the PowerConsumerGroupVar.is function to be evaluated in the scope of the initial request
       *  not the scope of the subsequent request.
        */

      "#hidden" #> SHtml.hidden(() => PowerConsumerVar(consumer)) 

        /**
          * When the page is rendered:
          * "SHtml.text(PowerConsumerGroupVar.is.Name" - this sets the value of the source name HTML input field to the current
          * value of the Source.Name member held in the PowerConsumerGroupVar.
          * "name => PowerConsumerGroupVar.is.Name(name)" - this registers a function that Lift will call
          * when the user clicks submit. This function sets Source.Name in the PowerConsumerGroupVar
          * in the subsequent request to the value the user entered in the HTML name input field
          */

        "#consumerLine" #> SHtml.ajaxSelect(optionsLine, Box.legacyNullTest(PowerConsumerVar.is.LineID_FK.value), set_consumerLine) &
        "#consumerName" #> SHtml.text(PowerConsumerVar.is.Name.value, name => PowerConsumerVar.is.Name(name)) &
        "#consumerType" #> SHtml.select(typelist, Box.legacyNullTest(PowerConsumerVar.is.NumberOfPhases.value.toString), set_consumerType) &
        // This registers a function for Lift to call when the user clicks submit
        "#submit" #> SHtml.onSubmitUnit(processSubmitConsumer)
    }
  }
  def processSubmitLine() = {
    logger.info("\nPower Consumer Line Create Form Submitted")
    PowerConsumerLineVar.is.validate match {
      case  Nil => {
        /**
          *  note the return boolean from PowerConsumerGroupVar.is.save is ignored as it does
          *  not contain useful information. See thread:
          *  https://groups.google.com/forum/?hl=en#!searchin/liftweb/Mapper.save/liftweb/kcWwaqGamW0/RjWfdOxjShEJ
          */

        configureChannels(PowerConsumerLineVar.is, List(one, two, three)) //set the channels accordingly
        PowerConsumerLineVar.is.save()
        S.notice("Consumer Saved")
        /**
          * S.seeOther throws an exception to interrupt the flow of execution and redirect
          * the user to the specified URL
           */

        S.seeOther("/system/consumer/listconsumers")
      }
      case errors => {
        logger.error(errors)
        S.error(errors)
      }
    }
    logger.info("Power Consumer UserGroup Save Success")
    /** exiting the function here causes the form to reload */
  }

  def set_lineType( assignedType : String ) : Any = {
    PowerConsumerLineVar.is.Type(assignedType)
    logger.info("Assigned type "+assignedType+" to consumer group "+(PowerConsumerLineVar.is.Name.value+" "))
  }

  def set_consumerProbes(probes : List[String]): List[JsCmd] ={
    logger.info("Selected Probes ")
    val ConfigID_FK = PowerConsumerGroup.findAll("_id",PowerConsumerLineVar.is.GroupID_FK.toString).head.ConfigID_FK
    val channels = probes.map{ p =>
      logger.info(" "+p)
      findChannelsForProbe(p,ConfigID_FK.toString)
    }.flatten
    channels.foreach(println)
    return MSHtml.replaceAllChannels(channels.toSeq,"consumerchannel",numberOfPhases)
  }

  /**
    * Update a Select with new Options
    */

  def set_consumerGroup( assignedGroup : String ) : List[JsCmd] = {
    //  def set_consumerGroup( assignedGroup : String ) : Any = {
    PowerConsumerLineVar.is.GroupID_FK(assignedGroup)
    println("")
    logger.info("Assigned group "+assignedGroup+" to consumer line "+PowerConsumerLineVar.is.Name.value+" ")
    val groupConfigID_FK =  PowerConsumerGroup.findAll(("_id" -> assignedGroup)).map( x => x.ConfigID_FK ).last
    println(groupConfigID_FK)
    val channels = findChannelsWithNoSources(List(groupConfigID_FK.toString))
    channels.map{ c => println(c) }
    val probes_associated = channels.map{ c => c._1 }.map{ p_ch => (p_ch.split("_")(0),p_ch.split("_")(1))}.groupBy(_._1).map{ p => p._1}
    val optionsprobe = probes_associated.map{ probe_id => (probe_id,Probe.findAll("_id",probe_id).head.probeName.toString) }.toList
    List(MSHtml.ReplaceOptionsMultiSelect("consumerProbe", optionsprobe, probes_associated.toSeq) :: MSHtml.replaceAllChannels(channels,"consumerchannel",numberOfPhases))
  }

  /** Creates a consumer Line
    *
    */
  def createLine : CssSel = {
    val currentConfs = Configuration.findAll("UserID_FKs",currentUserId)
    PowerConsumerLineVar.is.NumberOfPhases(numberOfPhases)
    println()
    logger.info("Loaded Create Power Consumer Line")
    logger.info("Number of Phases = "+numberOfPhases)
    logger.info("Configurations associated with "+currentUserId+" : ")
    currentConfs.map( x => x.configurationName.toString ).foreach(logger.info(_))
    logger.info("Groups associated with "+currentUserId+" : ")
    // populate Consumer Group Select
    val groupsLoggedIn = currentConfs.map( x => {
      listgroup = listgroup ::: PowerConsumerGroup.findAll(("ConfigID_FK" -> x.id.toString)).flatMap(a => g(a))
    })
    if (listgroup.length == 0){
      logger.error("CANNOT CREATE CONSUMER LINE WITHOUT CONSUMER GROUP")
      //S.appendJs(Run("noConsumerGroupErrorDialog();")) // pop up
      S.redirectTo("/system/consumer/listconsumers",()=>S.error("NO CONSUMER GROUP AVAILABLE, CREATE GROUP BEFORE CREATING LINE"))
      "#submit" #> SHtml.onSubmitUnit(processSubmitLine)

    }else {
      listgroup.foreach(logger.info(_))
      val optionsGroup = listgroup.map(i => (i.id -> i.name))
      // Populate Probe Select based on default Consumer Group -> Config -> Channel -> Probe
      PowerConsumerLineVar.is.GroupID_FK(optionsGroup.headOption.getOrElse(("", ""))._1)
      val selectedConf = Configuration.findAll("_id", PowerConsumerGroup.findAll("_id", optionsGroup.headOption.getOrElse(("", ""))._1).head.ConfigID_FK.toString)
      val channels = findChannelsWithNoSources(selectedConf.map { conf => conf.id.toString })
      val probes_associated = channels.map { c => c._1 }.map { p_ch => (p_ch.split("_")(0), p_ch.split("_")(1)) }.groupBy(_._1).map { p => p._1 }
      val optionsprobe = probes_associated.map { probe_id => (probe_id, Probe.findAll("_id", probe_id).head.probeName.toString) }.toSeq
      val typelist = List(("Estate", "Estate"), ("Structure", "Structure"), ("Floor", "Floor"), ("Section", "Section"), ("Room", "Room"), ("Subsection", "Subsection"))
      val consumerLine = PowerConsumerLineVar.is

      def sel_phase_CssSel(): CssSel = {
        if (channels.length >= numberOfPhases) {
          if (numberOfPhases == 3) {
            return {
              "id=consumerchannel_1" #> SHtml.select(channels, Box.legacyNullTest(channels(0)._1), one = _) &
                "id=consumerchannel_2" #> SHtml.select(channels, Box.legacyNullTest(channels(1)._1), two = _) &
                "id=consumerchannel_3" #> SHtml.select(channels, Box.legacyNullTest(channels(2)._1), three = _) &
                "#lineName" #> SHtml.text(PowerConsumerLineVar.is.Name.value, name => PowerConsumerLineVar.is.Name(name)) &
                "#linePhases" #> SHtml.select(typelist, Box.legacyNullTest(PowerConsumerLineVar.is.NumberOfPhases.value.toString), set_lineType) &
                "#groupType" #> SHtml.select(typelist, Box.legacyNullTest(PowerConsumerLineVar.is.Type.value.toString), set_lineType) &
                "#signalThreshold" #> SHtml.number(PowerConsumerLineVar.is.SignalThreshold.value, (th :Int) => PowerConsumerLineVar.is.SignalThreshold(th),0,1000) &
                "#consumerGroup" #> SHtml.ajaxSelect(optionsGroup, Box.legacyNullTest(PowerConsumerLineVar.is.GroupID_FK.value), set_consumerGroup) &
                "#consumerProbe" #> MSHtml.ajaxMultiSelect(optionsprobe, probes_associated.toSeq, set_consumerProbes) &
                //      "#consumerProbe" #> SHtml.ajaxUntrustedSelect(optionsProbe, Box.legacyNullTest(optionsProbe.head._1), set_consumerProbe ) &
                "#submit" #> SHtml.onSubmitUnit(processSubmitLine)
            }
          }
          else {
            return {
              "id=consumerchannel_1" #> SHtml.select(channels, Box.legacyNullTest(channels(0)._1), one = _) &
                "#lineName" #> SHtml.text(PowerConsumerLineVar.is.Name.value, name => PowerConsumerLineVar.is.Name(name)) &
                "#signalThreshold" #> SHtml.number(PowerConsumerLineVar.is.SignalThreshold.value, (th :Int) => PowerConsumerLineVar.is.SignalThreshold(th),0,1000) &
                "#linePhases" #> SHtml.select(typelist, Box.legacyNullTest(PowerConsumerLineVar.is.NumberOfPhases.value.toString), set_lineType) &
                "#groupType" #> SHtml.select(typelist, Box.legacyNullTest(PowerConsumerLineVar.is.Type.value.toString), set_lineType) &
                "#consumerGroup" #> SHtml.ajaxSelect(optionsGroup, Box.legacyNullTest(PowerConsumerLineVar.is.GroupID_FK.value), set_consumerGroup) &
                "#consumerProbe" #> MSHtml.ajaxMultiSelect(optionsprobe, probes_associated.toSeq, set_consumerProbes) &
                //      "#consumerProbe" #> SHtml.ajaxUntrustedSelect(optionsProbe, Box.legacyNullTest(optionsProbe.head._1), set_consumerProbe ) &
                "#submit" #> SHtml.onSubmitUnit(processSubmitLine)
            }
          }
        } else {
          S.appendJs(Run("insufficientChannelsErrorDialog();"))
          return {
            "#submit" #> SHtml.onSubmitUnit(processSubmitLine)
          }
        }
      }

      "#hidden" #> SHtml.hidden(() => PowerConsumerLineVar(consumerLine)) &
        sel_phase_CssSel
    }
  }




  var numberOfPhases:Int=1
  def numOfPhase:NodeSeq={
    val x = S.attr("phase") openOr "4"
    numberOfPhases = x.toInt
    <span><hr/></span>
  }
  /**  called from /system/source/editsource.html */
  def edit = {
    if ( ! PowerConsumerGroupVar.set_? )
      S.redirectTo("/system/consumer/listconsumers")

    val source = PowerConsumerGroupVar.is
    "#hidden" #> SHtml.hidden(() => PowerConsumerGroupVar(source) ) &
      "#sourcename" #> SHtml.text(PowerConsumerGroupVar.is.Name.value, name => PowerConsumerGroupVar.is.Name(name) ) &
      "#submit" #> SHtml.onSubmitUnit(processSubmitGroup)
  }

  /** called from /system/consumer/listconsumers.html */
  def orELSE(i: String) = i match {
    case null => SHtml.link("/system/consumer/deleteconsumer", () => (), Text("delete"))
    case _=> i
  }
  def list = {
    println()
    logger.info("Loaded List Power Consumer Lines Page")
    // for each Source object in the database render the name along with links to view, edit, and delete
    logger.warn(""+getPowerConsumerLines(getPowerConsumerGroups(Configuration.getUserConfigIDs(currentUserId)).map{ t => t.id.toString}))
    val currentConfs = Configuration.findAll("UserID_FKs",currentUserId)
    currentConfs.map( x => {
      listgroup = listgroup ::: PowerConsumerGroup.findAll(("ConfigID_FK" -> x.id.toString)).flatMap(a =>g(a))
    })
    listgroup.map( x => {
      listline = listline ::: PowerConsumerLine.findAll("GroupID_FK", x.id).flatMap(a =>l(a))
    })
    println("ll "+listline+" ")
    "#consumergrouplist *" #> getPowerConsumerGroups(Configuration.getUserConfigIDs(currentUserId)).map( t => {
      ".consumerGroupName *" #> SHtml.link("/system/consumer/editconsumergroup?groupID="+t.id.value, () => (), Text(t.Name.value), pairToBasic("title","edit consumer "+t.Name.value+" group")) &
        ".actions *" #> {
          SHtml.link("/system/consumer/deleteconsumergroup", () => PowerConsumerGroupVar(t), Text("delete_group\n"), pairToBasic("title","delete "+t.Name.value+" consumer group"))
        }
    }) &
    "#consumerlist *" #> getPowerConsumerLines(getPowerConsumerGroups(Configuration.getUserConfigIDs(currentUserId)).map { t => t.id.toString }).map(t => {
      val consumers = PowerConsumer.findAll("LineID_FK", t.id.value).map(u => u.Name) mkString ",\n"
      val sourceProbe = t.ProbeID_FKs.value.flatMap{ probe => Probe.findAll("_id", probe ) }
      println("Probe IDs "+t.ProbeID_FKs+" ")
      val probenames = sourceProbe.map{ p => p.probeName } mkString ", "
      val consumerChannel = sourceProbe.flatMap { p =>
        p.Channels.get.map { ch =>
          if (ch.SourceID_FK.toString == t.id.toString) {
            //println(manOf(ch.sourceChannel) + " IN " + ch.sourceChannel)
            ch.sourceChannel.toString
          }
        }
      }
      val group = PowerConsumerGroup.findAll("_id", t.GroupID_FK.value).headOption.get
      val channels = consumerChannel filter (_ !=(())) mkString ", "
      logger.info("CHANNEL   : " + (channels))
      logger.info("CONSUMERS : " + consumers)
      ".consumerLineName *" #> Text(t.Name.value) &
        ".consumerGroupName *" #> SHtml.link("/system/consumer/editconsumergroup?groupID="+group.id.value, () => (), Text(group.Name.value), pairToBasic("title","edit "+ group.Name.value+" consumer group") ) &
        ".consumerNames *" #> consumers &
        ".consumerCH *" #> channels  &
        ".consumerPhases *" #> t.NumberOfPhases &
        ".consumerProbeName *" #> probenames &
        ".actions *" #> {
          SHtml.link("/system/consumer/createconsumer?lineID="+t.id.value, () => PowerConsumerLineVar(t), Text("add_consumer\n"), pairToBasic("title","add consumer "+t.Name.value+" consumer line")) ++
            SHtml.link("/system/consumer/createconsumerline?groupID="+group.id.value, () => PowerConsumerLineVar(t), Text("add_line\n"), pairToBasic("title","add consumer line to "+group.Name.value+" consumer group")) ++
            SHtml.link("/system/consumer/deleteconsumerline", () => PowerConsumerLineVar(t), Text("delete_line\n")) ++
            SHtml.link("/system/consumer/deleteconsumergroup", () => PowerConsumerGroupVar(group), Text("delete_group\n"), pairToBasic("title","delete "+group.Name.value+" consumer group"))
        }
    })
  }

  def listGroups = {
    println()
    logger.info("Loaded List Power Consumer Groups Page")
    // for each Source object in the database render the name along with links to view, edit, and delete
    logger.warn(""+getPowerConsumerLines(getPowerConsumerGroups(Configuration.getUserConfigIDs(currentUserId)).map{ t => t.id.toString}))
    val currentConfs = Configuration.findAll("UserID_FKs",currentUserId)
    currentConfs.map( x => {
      listgroup = listgroup ::: PowerConsumerGroup.findAll(("ConfigID_FK" -> x.id.toString)).flatMap(a =>g(a))
    })
    "#consumergrouplist *" #> getPowerConsumerGroups(Configuration.getUserConfigIDs(currentUserId)).map( t => {
      ".consumerGroupName *" #> SHtml.link("/system/consumer/editconsumergroup?groupID="+t.id.value, () => (), Text(t.Name.value), pairToBasic("title","edit consumer "+t.Name.value+" group")) &
        ".actions *" #> {
            SHtml.link("/system/consumer/deleteconsumergroup", () => PowerConsumerGroupVar(t), Text("delete_group\n"), pairToBasic("title","delete "+t.Name.value+" consumer group"))
        }
    })
  }

  /** called from /system/source/viewsource.html */
  def view = {
    /** we're expecting the PowerConsumerGroupVar to have been set when we were linked to from the list page */
    if ( ! PowerConsumerGroupVar.set_? )
      S.redirectTo("/system/consumer/listconsumers")

    val source = PowerConsumerGroupVar.is
    "#sourcename *" #> PowerConsumerGroupVar.is.Name.asHtml &
      "#edit" #> SHtml.link("/system/source/editsource", () => PowerConsumerGroupVar(source), Text("edit"))
  }

  /** called from /system/consumer/deleteconsumergroup.html */

  def deleteGroup () = {
    if ( ! PowerConsumerGroupVar.set_? )
      S.redirectTo("/system/consumer/listconsumers")


    val e = PowerConsumerGroupVar.is
    "#groupName" #> PowerConsumerGroupVar.is.Name &
      "#yes" #> SHtml.link("/system/consumer/listconsumers", () =>  deleteGroupLinesConsumers(e), Text("Yes")) &
      "#no" #> SHtml.link("/system/consumer/listconsumers", () =>{}, Text("No"))
  }



  def deleteConsumer(consumers : List[PowerConsumer]){
    consumers.map { consumer =>
      PowerConsumer.findAll("_id" -> consumer.id.toString ).map{ c => c.delete_!}
    }
  }


  def deleteLine = {
    // we're expecting the GeneratorVar to have been set when we were linked to from the list page
    if ( ! PowerConsumerLineVar.set_? )
      S.redirectTo("/system/consumer/listconsumers")

    val e = PowerConsumerLineVar.is
    "#lineName" #> PowerConsumerLineVar.is.Name &
      "#yes" #> SHtml.link("/system/consumer/listconsumers", () =>{deleteConsumerLines(e)}, Text("Yes")) &
      "#no" #> SHtml.link("/system/consumer/listconsumers", () =>{ }, Text("No"))
  }

  def configureChannels(genobj:PowerConsumerLine,channels : List[String]): Unit ={
    logger.info("\nConfigure Channels")

    val filtered_channels =channels.map{ ch => if(ch != "UNASSIGNED"){ch}}.filter( _ != (()) ).asInstanceOf[List[String]]
    filtered_channels.foreach(println)
    val probes_associated = filtered_channels.map{ p_ch => (p_ch.split("_")(0),p_ch.split("_")(1))}.groupBy(_._1)
    val probeids = probes_associated.map{ p => p._1 }
    PowerConsumerLineVar.is.ProbeID_FKs(probeids.toList)
    println(" PR ID'S "+probeids+"   "+manOf(probeids))
    probes_associated.foreach(println)
    probes_associated.map{  probes =>
      val channels_associated = probes._2.map{ l => l._2.toInt }
      println("PAssociated "+probes)
      Probe.findAll("_id",probes._1).map{ probe =>
        var counter_channel = 0
        probe.Channels.get.foreach(a => {
          if (a.SourceID_FK.toString.isEmpty  && numberOfPhases > 1 && channels_associated.contains(counter_channel)) {
            if (counter_channel == one.split("_")(1).toInt) {
              a.SourceID_FK(genobj.id.toString)
              a.sourceChannel(one.split("_")(1).toInt)
              //a.SourceType(genobj.Type.toString)
              a.SourceType("consumer")
              a.SourceName(genobj.Name.toString)
              a.phase(1)
              logger.info("JUST ASSIGNED : " + probe.probeName + "_"+counter_channel+" to "+PowerConsumerLineVar.is.Name)
            } else if ((counter_channel == two.split("_")(1).toInt) && (PowerConsumerLineVar.is.NumberOfPhases.get == 3) && channels_associated.contains(counter_channel)) {
              a.SourceID_FK(genobj.id.toString)
              a.sourceChannel(two.split("_")(1).toInt)
              //a.SourceType(genobj.Type.toString)
              a.SourceType("consumer")
              a.SourceName(genobj.Name.toString)
              a.phase(2)
              logger.info("JUST ASSIGNED : " + probe.probeName + "_"+counter_channel+" to "+PowerConsumerLineVar.is.Name)
            } else if ((counter_channel == three.split("_")(1).toInt) && (PowerConsumerLineVar.is.NumberOfPhases.get == 3) && channels_associated.contains(counter_channel) ){
              a.SourceID_FK(genobj.id.toString)
              a.sourceChannel(three.split("_")(1).toInt)
              //a.SourceType(genobj.Type.toString)
              a.SourceType("consumer")
              a.SourceName(genobj.Name.toString)
              a.phase(3)
              logger.info("JUST ASSIGNED : " + probe.probeName + "_"+counter_channel+" to "+PowerConsumerLineVar.is.Name)
            }
            //println(" three "+three+" cc "+counter_channel+" NPH "+PowerConsumerLineVar.is.NumberOfPhases.get+" NPHT "+manOf(PowerConsumerLineVar.is.NumberOfPhases.get))
          }else if (a.SourceID_FK.toString.isEmpty){
            if (counter_channel == one.split("_")(1).toInt && channels_associated.contains(counter_channel)) {
              a.SourceID_FK(genobj.id.toString)
              a.sourceChannel(one.split("_")(1).toInt)
              a.SourceType("consumer")
              //a.SourceType(genobj.Type.toString)
              a.SourceName(genobj.Name.toString)
              a.phase(1)
              logger.info("JUST ASSIGNED : " + probe.probeName + "_"+counter_channel+" to "+PowerConsumerLineVar.is.Name)
            }
          } else {
            println("PREVIOUSLY ASSIGNED")
          }
          counter_channel += 1
        } )
        probe.save()
      }
    }
    filtered_channels.map{ p_ch =>
      println(" PROBE : "+p_ch.split("_")(0)+" CH : "+p_ch.split("_")(1))
    }
  }

}