package code.snippet

import java.io.File

import code.model.{Domains, User}

import scala.xml.{NodeSeq, Text}
import com.kodekutters.psl._
import com.typesafe.config.ConfigFactory

import scala.collection.JavaConversions._
import net.liftweb.util._
import Helpers._
import net.liftweb.common.Full
import net.liftweb.http.SHtml.ElemAttr._
import net.liftweb.http.{LiftRules, RequestVar, S, SHtml}

/**
  * White-label Controller.
  */

class DomainOps {
  object domainVar extends RequestVar[Domains](Domains.createRecord)
  val psl_list_path = LiftRules.getResource("/public_suffix_list.dat").get
  val conf =  ConfigFactory.parseMap(Map("psl.url"->psl_list_path.toString,"psl.printChecks"->true,"psl.charset"->""))
  val psl = PublicSuffixList(conf)
  val hostName = S.request.map(_.request.serverName).openOr("")
  val hostNameArray = hostName.split('.')
  val registrable = psl.registrable(hostName).getOrElse("")


  /**
    *
    * @return tuple of type of domain and search string
    */
  def domainType() : (String,String) = {
    hostName match {
      case host if registrable == "grit.systems" && host != "grit.systems" => {
        val subdomains = hostNameArray.filter(d => d != psl.sld(hostName).getOrElse("")  && d != psl.tld(hostName).getOrElse(""))
        val subdomainString = subdomains.reduce(_+"."+_)
        ("subdomain",subdomainString)
      }
      case host if registrable != "grit.systems" && registrable != "" => {
        ("host",host)
      }
      case host if host == "localhost" => {
        ("localhost",host)
      }
      case _  => {("","")}
    }
  }

  def subdomainCheck(in: NodeSeq): NodeSeq = {
    if (User.currentUser.isEmpty){
      domainType() match {
        case _type if _type._1 == "subdomain" => {
          val subdomains = hostNameArray.filter(d => d != psl.sld(hostName).getOrElse("")  && d != psl.tld(hostName).getOrElse(""))
          val subdomainString = subdomains.reduce(_+"."+_)
          Domains.find("subdomain",subdomainString) match {
            case Full(domain) => {
              S.redirectTo("/domains/login",() => domainVar(domain))
            }
            case _ => {
              in
            }
          }
        }
        case _type if _type._1 == "host" => {
          Domains.find("hostName",_type._2) match {
            case Full(domain) => {
              S.redirectTo("/domains/login",() => domainVar(domain))
            }
            case _ => {
              in
            }
          }
        }
        case _type if _type._1 == "localhost" => {
          val domain = Domains.findAll("hostName","localhost").headOption.getOrElse(Domains)
          /*S.redirectTo("/domains/login",() => domainVar(domain))*/
          in
        }
        case _ => {
          in
        }
      }
    }
    else {
      in
    }
  }

  /**
    * Handles login
    * @return login page
    */
  def login() = {
    if ( ! domainVar.set_? )
      S.redirectTo("/")

    println(domainVar.is.imageURL.value)
    "#img [src]" #> domainVar.is.imageURL.value
  }

  def list() = {
    val domains = Domains.findAll
    "#domainList" #> domains.map{ domain =>
      ".name" #>  domain.name.value &
        ".actions *" #> {SHtml.link("/system/usergroup/editdomain?action=View", () => domainVar(domain), Text("view")) ++ Text(" ") ++
          SHtml.link("/system/usergroup/editdomain?action=Edit", () => domainVar(domain), Text("edit")) ++ Text(" ") ++
          SHtml.link("/system/usergroup/deletedomain", () => domainVar(domain), Text("delete"))}
    }
  }

  def processSubmit() = {
    println(domainVar)
    domainVar.is.save()
    S.seeOther("/system/usergroup/listdomain")
  }

  def create() = {
    val domain = domainVar.is
    "#hidden" #> SHtml.hidden(() => domainVar(domain)) &
      "#name" #> SHtml.text(domainVar.is.name.value,name=>{domainVar.is.name(name)} ) &
      "#admin" #> SHtml.text(domainVar.is.name.value,id=>{domainVar.is.AdminID_FK(id)} ) &
      "#subdomain" #> SHtml.text(domainVar.is.subdomain.value, sd=>{domainVar.is.subdomain(sd)}) &
      "#imageURL" #> SHtml.text(domainVar.is.imageURL.value, img => {domainVar.is.imageURL(img)}) &
      "#pageURL" #> SHtml.text(domainVar.is.pageURL.value, page => {})&
      "#hostname" #> SHtml.text(domainVar.is.hostName.value , hostName => {domainVar.is.hostName(hostName)})&
      "#submit" #> SHtml.onSubmitUnit(processSubmit)
  }

  /**
    * Edit page
    * @return redirect to link
    */
  def edit() = {
    if ( ! domainVar.set_? )
      S.redirectTo("/system/usergroup/listdomain")

    def manOf[T: Manifest](t: T): Manifest[T] = manifest[T]
    val params = S.request.map(_.request.param("action")) openOr ("Undefined")
    val action = params.asInstanceOf[List[String]].headOption.getOrElse("Undefined action")
    val vieworedit = action match {
      case "Edit" =>
        pairToBasic("enabled","disabled")
      case _ =>
        pairToBasic("disabled","disabled")
    }
    val domain = domainVar.is

    "#hidden" #> SHtml.hidden(() => domainVar(domain)) &
      ".panel-title"  #> Text(action + " Domain") &
      "#name" #> SHtml.text(domainVar.is.name.value,name => {domainVar.is.name(name)},vieworedit ) &
      "#subdomain" #> SHtml.text(domainVar.is.subdomain.value,sd=>{domainVar.is.subdomain(sd)},vieworedit) &
      "#imageURL" #> SHtml.text(domainVar.is.imageURL.value,(img)=>{domainVar.is.imageURL(img)},vieworedit) &
      "#faviconURL" #> SHtml.text(domainVar.is.faviconURL.value,(img)=>{domainVar.is.faviconURL(img)},vieworedit) &
      "#hostname" #> SHtml.text(domainVar.is.hostName.value,hostName => {domainVar.is.hostName(hostName)},vieworedit)&
      "#admin" #> SHtml.text(domainVar.is.AdminID_FK.value,id=>{domainVar.is.AdminID_FK(id)},vieworedit)&
      "#submit" #> SHtml.onSubmitUnit(processSubmit)
  }

  /**
    * Deletes domain
    * @return link
    */
  def delete() = {
    val domain = domainVar.is
    def deleteDomain(){
      domain.delete_!
    }
    "#name" #> domainVar.is.name &
    "#yes" #> SHtml.link("/system/usergroup/listdomain", () =>{
      deleteDomain() }, Text("Delete")) &
      "#no" #> SHtml.link("/system/usergroup/listdomain", () =>{ }, Text("Cancel"))
  }

  /**
    * Handles domain logo
    * @return image url
    */
  def domainLogo = {
    val hostName = S.request.map(_.request.serverName).openOr("")
    val hostNameArray = hostName.split('.')
    val registrable = psl.registrable(hostName).getOrElse("")
    val imageURL = domainType() match {
      case _type if _type._1 == "subdomain" => {
        val subdomains = hostNameArray.filter(d => d != psl.sld(hostName).getOrElse("")  && d != psl.tld(hostName).getOrElse(""))
        val subdomainString = subdomains.reduce(_+"."+_)
        Domains.find("subdomain",subdomainString) match {
          case Full(domain) => {
            domain.imageURL.value
          }
          case _ => {
            "/img/grit-small.png"
          }
        }
      }
      case _type if _type._1 == "host" => {
        Domains.find("hostName",_type._2) match {
          case Full(domain) => {
            domain.imageURL.value
          }
          case _ => {
            "/img/grit-small.png"
          }
        }
      }
      case _type if _type._1 == "localhost" => {
        val domain = Domains.findAll("hostName","localhost").headOption.getOrElse(Domains)
        if (domain.imageURL.value.isEmpty){
          "/img/grit-small.png"
        } else {
          domain.imageURL.value
        }
      }
      case _ => {"/img/grit-small.png"}
    }
    imageURL
  }

  def domainFavicon() = {
    val favicon = domainType() match {
      case _type if _type._1 == "subdomain" => {
        val subdomains = hostNameArray.filter(d => d != psl.sld(hostName).getOrElse("")  && d != psl.tld(hostName).getOrElse(""))
        val subdomainString = subdomains.reduce(_+"."+_)
        Domains.find("subdomain",subdomainString) match {
          case Full(domain) => {
            domain.faviconURL.value
          }
          case _ => {
            "/img/favlogo.ico"
          }
        }
      }
      case _type if _type._1 == "host" => {
        Domains.find("hostName",_type._2) match {
          case Full(domain) => {
            domain.faviconURL.value
          }
          case _ => {
            "/img/favlogo.ico"
          }
        }
      }
      case _type if _type._1 == "localhost" => {
        val domain = Domains.findAll("hostName","localhost").headOption.getOrElse(Domains)
        if (domain.faviconURL.value.isEmpty){
          "/img/favlogo.ico"
        } else {
          domain.faviconURL.value
        }
      }
      case _ => {"/img/favlogo.ico"}
    }
    favicon
  }

  def domainTitle() : String = {
    domainType() match {
      case _type if _type._1 == "" => {
        "GRIT SYSTEMS : Smart Meters that will help you spend less on electricity and start saving money"
      }
      case _ => {
        domainType()._2
      }
    }
  }


  def showLogo() = {
    "#img [src]" #> domainLogo
  }


  def showFavicon() = {
    "rel=icon [href]" #> domainFavicon &
    "#titleSpan *" #> domainTitle
  }

}
