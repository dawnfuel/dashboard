package code.snippet

import code.model.User
import net.liftmodules.mongoauth.Permission
import scala.xml.{Text, NodeSeq}
import net.liftweb.http.{DispatchSnippet,S}

sealed trait ShiroShippet {
  def verification(xhtml: NodeSeq)(f: Boolean): NodeSeq =
    if (f) xhtml else NodeSeq.Empty

  def serve(xhtml: NodeSeq, attribute: String = "name")(f: String => Boolean): NodeSeq =
    if (S.attr(attribute) exists f) xhtml else NodeSeq.Empty
}

trait SubjectSnippet extends DispatchSnippet with ShiroShippet {
  def dispatch = {
    case _ => render _
  }
  def render(xhtml: NodeSeq): NodeSeq
}

object hasRole extends SubjectSnippet {
  def render(xhtml: NodeSeq): NodeSeq = serve(xhtml) {
    User.hasRole(_)
  }
}

  object lackRole extends SubjectSnippet {
    def render(xhtml: NodeSeq): NodeSeq = serve(xhtml){
      !User.hasRole(_)
    }
}

object lackRoles extends SubjectSnippet {
  def render(xhtml: NodeSeq): NodeSeq = serve(xhtml){
    (r:String) => {
      val roles = r.split(",").toList
      var boolValue = true
      roles.map{ r =>
        if (User.hasRole(r)){
          boolValue = false
        }
      }
      boolValue
    }
  }
}

object hasPermission extends SubjectSnippet {
  def render(xhtml: NodeSeq): NodeSeq = serve(xhtml) {
    (s:String) => {
      val a = s.split(":")
      User.hasPermission(Permission(a(0),a(1)))}
  }
}

////////////////////////// Permissions /////////////////////////
//sealed trait ShiroShippet2 {
//  def verification(xhtml: NodeSeq)(f: Boolean): NodeSeq =
//    if (f) xhtml else NodeSeq.Empty
//
//  def serve(xhtml: NodeSeq, attribute: String = "name")(f: String => Boolean): NodeSeq =
//    if (S.attr(attribute) exists f) xhtml else NodeSeq.Empty
//}
//
//
//
//
//
//object hasPermission extends SubjectSnippet {
//  def render(xhtml: NodeSeq): NodeSeq = serve(xhtml) {
//    val permission = Permission(_)
//    User.hasPermission(permission)
//  }
//}