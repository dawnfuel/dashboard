package code.snippet

import com.github.tototoshi.csv._
import java.io._
import java.nio.charset.Charset

import net.liftweb.mongodb.BsonDSL._
import code.model._
import com.lowagie.text.pdf.BaseFont
import net.liftweb.common.Full
import org.bson.types.ObjectId
import org.joda.time.DateTime
import org.joda.time.format.DateTimeFormat
import org.xhtmlrenderer.pdf.ITextRenderer



/**
  * Generates invoices from csv file
  */
object InvoiceGeneration {

  /** Download CSV file **/
  AuthFileDownloadObject.load()

  def Start() = {
    /** Parse CSV file **/
    val reader = CSVReader.open(new File("output.csv"))
    val requirementStream = reader.toStream.tail
    /** Loop through each row  **/
    requirementStream.filter(r => {r(1) == "A Consumer"}).foreach{
      consumer => {
        val email = consumer(118)
        val processed = User.find(("roles" -> List("interested")) ~("email"-> email)) match {
          case Full(u) => {
            u.ReqSubmitted(true).save()
            u.ReqProcessed.value
          }
          case _ => {false}
        }
        var sensor = List[(String,String,BigDecimal,Int)]()
        println(email)
        /** Check if row has been processed **/
        if (!processed) {
          var genCap = BigDecimal(0)
          var synced : List[Map[String,Any]] = List()

          /** Get generator capacity if user has generator source **/
          if(consumer(11)=="Generator"){
            /** Schema of questions was changed **/
            consumer(15).nonEmpty match  {
              case true => {
                genCap  = BigDecimal(consumer(15))
                val phase = (Option(consumer(14).toString.toInt).getOrElse(1))
                val sensorType = capacityToSensor(genCap.toDouble)
                sensor = phase match {
                  case 3 => {
                    sensor ::: List.fill(3)((sensorType._1,sensorType._2,sensorType._3,phase))
                  }
                  case 1 => {
                    sensor ::: List.fill(1)((sensorType._1,sensorType._2,sensorType._3,phase))
                  }
                  case _ => {
                    sensor
                  }
                }
              }
              case _ => {
                consumer(46).isEmpty match {
                  case true => {genCap  = BigDecimal(0)}
                  case false => {
                    val numberofGen = Option(consumer(17).toInt)getOrElse(0)
                    val gen : Map[String,Map[String,Any]] = Map(
                      "gen_1" -> Map("sync"->"0","phase"->consumer(45),"cap"->consumer(46)),
                      "gen_2" -> Map("sync"->consumer(43),"phase"->consumer(42),"cap"->consumer(44)),
                      "gen_3" -> Map("sync"->consumer(40),"phase"->consumer(39),"cap"->consumer(41)),
                      "gen_4" -> Map("sync"->consumer(37),"phase"->consumer(36),"cap"->consumer(38)),
                      "gen_5" -> Map("sync"->consumer(34),"phase"->consumer(33),"cap"->consumer(35)),
                      "gen_6" -> Map("sync"->consumer(31),"phase"->consumer(30),"cap"->consumer(32)),
                      "gen_7" -> Map("sync"->consumer(28),"phase"->consumer(27),"cap"->consumer(29)),
                      "gen_8" -> Map("sync"->consumer(25),"phase"->consumer(24),"cap"->consumer(26)),
                      "gen_9" -> Map("sync"->consumer(22),"phase"->consumer(21),"cap"->consumer(23)),
                      "gen_10" -> Map("sync"->consumer(19),"phase"->consumer(18),"cap"->consumer(20))
                    )

                    /** Get number of generators indicated and check synchronicity
                      *  while calculating generator capacity
                      */
                    for (i <- 1 to (numberofGen)) {
                      val phase = Option(gen("gen_" + i).getOrElse("phase","1").toString.toInt).getOrElse(1)
                      val capacity = BigDecimal(gen("gen_" + i).getOrElse("cap",0).toString)
                      val sync =  toBoolean(gen("gen_" + i).getOrElse("sync",0).toString.toInt)
                      if (sync) {
                        synced = gen("gen_" + i) :: synced
                      } else {
                        val sensorType = capacityToSensor(capacity.toDouble)
                        sensor = phase match {
                          case 3 => {
                            sensor ::: List.fill(3)((sensorType._1,sensorType._2,sensorType._3,phase))
                          }
                          case 1 => {
                            sensor ::: List.fill(1)((sensorType._1,sensorType._2,sensorType._3,phase))
                          }
                          case _ => {
                            sensor
                          }
                        }
                        genCap = capacity > genCap match {
                          case true => {capacity}
                          case false => {genCap}
                        }
                      }
                    }

                    val syncedCap = synced.map( sync => BigDecimal(sync.getOrElse("cap",0).toString)).sum
                    synced.foreach{ gen =>
                      val phase = Option(gen.getOrElse("phase","1").toString.toInt).getOrElse(1)
                      val sensorType = capacityToSensor(syncedCap.toDouble)
                      /** Calculate Sensors using generator capacity and synchronous configuration if available **/
                      sensor = phase match {
                        case 3 => {
                          sensor ::: List.fill(3)((sensorType._1,sensorType._2,sensorType._3,phase))
                        }
                        case 1 => {
                          sensor ::: List.fill(1)((sensorType._1,sensorType._2,sensorType._3,phase))
                        }
                        case  _ => {
                          sensor
                        }
                      }
                    }

                    genCap = syncedCap > genCap match {
                      case true => {syncedCap}
                      case false => {genCap}
                    }
                  }
                }
              }
            }
          }

          /** Calculate sensors for grid powersource **/
          if (consumer(10)=="Mains" ) {
            val mainsPhases = (Option(consumer(16).toString.toInt).getOrElse(1))
            val sensorType = capacityToSensor(genCap.toDouble)
            sensor = mainsPhases match  {
              case 3 => {
                sensor ::: List.fill(3)((sensorType._1,sensorType._2,sensorType._3,mainsPhases.toInt))
              }
              case 1 => {
                sensor ::: List.fill(1)((sensorType._1,sensorType._2,sensorType._3,mainsPhases.toInt))
              }
              case _ => {
                sensor
              }
            }
          }

          /** Calculate sensors for inverter powersource **/
          if(consumer(12)=="Inverter") {
            val invPhases = Option(consumer(48).toString.toInt).getOrElse(1)
            val invCapacity = BigDecimal(Str2Int(consumer(47).toString))
            println("inverter :" + invCapacity)
              val sensorType = capacityToSensor(invCapacity.toDouble)
              sensor = invPhases match {
                case 3 => {
                  sensor ::: List.fill(6)((sensorType._1,sensorType._2,sensorType._3,invPhases.toInt))
                }
                case 1 => {
                  sensor ::: List.fill(2)((sensorType._1,sensorType._2,sensorType._3,invPhases.toInt))
                }
                case _ => {
                  sensor
                }
              }
          }

          //TODO Solar Sensors
          println (email + " " + sensor)

          /** Invoice Generation **/
          sensor = sensor.filter(_._1 != "")
          val addedSensors = sensor.filter(_._2 == "100A Sensor")
          var otherSensors = sensor.filterNot(_._2 == "100A Sensor")
          /** Filter 3*100A sensors from being charged **/
          if (addedSensors.length > 3) {
            val additional = addedSensors.length - 3
            for (i <- 1 to additional ) {
              otherSensors ::= addedSensors(i)
            }
          }
          var lines = List[InvoiceLines]()
          val meter = Device.findAll(("Type"->"device")~ ("name"->"G1 Smart Energy Monitor + 3 Sensors")).headOption.getOrElse(Device)
          val fmt = DateTimeFormat forPattern "dd MMM yyyy"
          val formatter = java.text.NumberFormat.getInstance
          val creationDate = fmt.print(new DateTime())
          val due = creationDate //fmt.print(new DateTime().plusDays(31))
          val NGN = "₦"
          val groupedSensors = otherSensors.groupBy(_._1)
          groupedSensors.map { group =>
            val numberOfSensors = group._2.length
            val line = InvoiceLines.createRecord.Quantity(numberOfSensors).UnitPrice(group._2.headOption.getOrElse(("","", BigDecimal("0.0"),0))._3).ProductID_FK(group._1)
            lines = line :: lines
          }
          val (userId : String,userName) = User.findByEmail(email) match {
            case Full(user) => {(user.id.value.toString,user.username.value)}
            case _ => {
                try {
                    val name = email.split("@")(0)
                    val userName = name + scala.util.Random.nextInt(100)
                    val user = User.createRecord.name(name).username(userName).email(email).roles(List("interested"))
                    user.saveBox() match {
                      case Full(x) => {
                        (user.id.value.toString, user.username.value)
                      }
                      case _ => {("","")}
                    }
                  } catch {
                  case e: Exception => {
                    ("","")
                  }
                }
              }
          }

          /** find linked account or create one if unavailable **/
          val account : Account = Account.find("UserID_FK",userId) match {
            case Full(acc) => {acc}
            case _  => {
              var billingDate : Int = new DateTime().getDayOfMonth
              billingDate = (billingDate > 28) match {
                case true => 28
                case false => billingDate
              }
              val account = Account.createRecord.Name(userName).BillingDate(billingDate).CreationDate(new DateTime()).PaymentPlan("buy").UserID_FK(userId).save()
              account
            }
          }
          val totalSensors = groupedSensors.map{group => group._2.length}.sum
          val numberOfMeters = (totalSensors / 8) + 1
          lines ::= InvoiceLines.createRecord.Quantity(numberOfMeters).UnitPrice(meter.price.value.Amount.value).ProductID_FK(meter.id.value)
          val monthlySub = GRIT_Products.findAll("name","G1 Monthly Subscription").headOption.getOrElse(GRIT_Products)
          lines ::= InvoiceLines.createRecord.Quantity(12).UnitPrice(monthlySub.price.value.Amount.value).ProductID_FK(monthlySub.id.value)
          val ref = PaymentSettings.findAll.head.lastReferenceNumber.value + 1
          val dueDate = new DateTime().plusDays(31);
          val Id = PaymentSettings.findAll.head.id.value
          GRIT_Invoice.createRecord.Status("unpaid").reference("E"+ref).tag("installation").DueDate(dueDate).Lines(lines)
            .AccountID_FK(account.id.value).save()
          PaymentSettings.update(("_id" ->Id), ("$inc" -> ("lastReferenceNumber" -> 1)))
          try {
            User.find("_id",new ObjectId(userId.toString)) match {
              case Full(user) => {user.ReqProcessed(true).save()}
              case _ => {}
            }
          } catch {
            case e:Exception => {

            }
          }
          val _totalAmount : BigDecimal = lines.map {
            line => {
              line.Quantity.value * line.UnitPrice.value
            }
          }.sum

          /** generate HTML for invoice lines **/
          val _tax = 0.05 * _totalAmount
          val tax = formatter.format(0.05 * _totalAmount)
          val totalAmount = formatter.format(_totalAmount + _tax)
          val _otherLines = lines.zipWithIndex.filter(_._2 != lines.length-1)
          val otherLines = _otherLines.map(o => o._1)
          val _lastLine = lines.last
          val lastLine =
            <tr class="item last">
              <td>
                {GRIT_Products.findAll("_id",_lastLine.ProductID_FK.value).headOption.getOrElse(GRIT_Products).name.value}
              </td>
              <td>
                {formatter.format(_lastLine.UnitPrice.value)}
              </td>
              <td>
                {_lastLine.Quantity.value}
              </td>
              <td>
                {formatter.format(_lastLine.Quantity.value * _lastLine.UnitPrice.value)}
              </td>
            </tr>
          val td_lines =
            <tbody>
              { otherLines.map(line =>
                <tr class="item">
                  <td>
                    {GRIT_Products.findAll("_id",line.ProductID_FK.value).headOption.getOrElse(GRIT_Products).name.value}
                  </td>
                  <td>
                     {formatter.format(line.UnitPrice.value)}
                  </td>
                  <td>
                    {line.Quantity.value}
                  </td>
                  <td>
                    {formatter.format(line.Quantity.value * line.UnitPrice.value)}
                  </td>
                </tr>
                ) ++ lastLine
               }
            </tbody>
          val html = "<?xml version=\"1.0\" encoding=\"utf-8\"?>\n\n    <!DOCTYPE html\n   PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\"\n   \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\" >\n\n    <html xmlns=\"http://www.w3.org/1999/xhtml\">\n\n    <head>\n        <meta http-equiv=\'Content-Type\' content=\'text/html; charset=UTF-8\'/>\n        <title>\n            Invoice_"+ref+"\n        </title>\n <link href=\"https://fonts.googleapis.com/css?family=Source+Sans+Pro\" rel=\"stylesheet\"/>       <style type=\"text/css\" xml:space=\"preserve\">\n            .invoice-box {\n                max-width: 800px;\n                margin: auto;\n                padding: 30px;\n                border: 1px solid #eee;\n                box-shadow: 0 0 10px rgba(0, 0, 0, .15);\n                font-size: 16px;\n                line-height: 24px;\n                font-family: 'Source Sans Pro',Arial,'Helvetica Neue', 'Helvetica', Helvetica, sans-serif;\n                color: #555;\n            }\n            \n            .invoice-box table {\n                width: 100%;\n                line-height: inherit;\n                text-align: left;\n            }\n            \n            .invoice-box table td {\n                padding: 5px;\n                vertical-align: top;\n            }\n            \n            .invoice-box table tr td:nth-child(4) {\n                text-align: right;\n            }\n            \n            .invoice-box table tr.top table td {\n                padding-bottom: 20px;\n            }\n            \n            .invoice-box table tr.top table td.title {\n                font-size: 45px;\n                line-height: 45px;\n                color: #333;\n            }\n            \n            .invoice-box table tr.information table td {\n                padding-bottom: 40px;\n            }\n            \n            .invoice-box table tr.heading td {\n                background: #eee;\n                border-bottom: 1px solid #ddd;\n                font-weight: bold;\n            }\n            \n            .invoice-box table tr.details td {\n                padding-bottom: 20px;\n            }\n            \n            .invoice-box table tr.item td {\n                border-bottom: 1px solid #eee;\n                padding: auto 0.9em;\n            }\n            \n            .invoice-box table tr.item.last td {\n                border-bottom: none;\n            }\n            \n            .invoice-box table tr.total td:nth-child(4) {\n                border-top: 2px solid #eee;\n                font-weight: bold;\n            }\n            \n            @media only screen and (max-width: 600px) {\n                .invoice-box table tr.top table td {\n                    width: 100%;\n                    display: block;\n                    text-align: center;\n                }\n                .invoice-box table tr.information table td {\n                    width: 100%;\n                    display: block;\n                    text-align: center;\n                }\n            }\n        </style>\n    </head>\n\n    <body>\n        <div class=\"invoice-box\">\n            <table cellpadding=\"0\" cellspacing=\"0\">\n                <tr class=\"top\">\n                    <td colspan=\"4\">\n                        <table>\n                            <tr>\n                                <td class=\"title\">\n                                    <img src=\"http://grit.systems/img/grit-small.png\" style=\"height:1.5em\" alt=\"\" />\n                                </td>\n                                <td></td>\n                                <td></td>\n                                <td>\n                                    Invoice #: E"+{ref}+"\n                                    <br /> Created: "+{creationDate}+"\n                                    <br /> Due: "+{due}+"\n                                </td>\n                            </tr>\n                        </table>\n                    </td>\n                </tr>\n                <tr class=\"information\">\n                    <td colspan=\"4\">\n                        <table>\n                            <tr>\n                                <td>\n                                    GRIT Systems\n                                    <br /> 294 Herbert Macaulay Way\n                                    <br />Yaba, Lagos, 101211\n                                    <br/> Nigeria\n                                </td>\n                                <td></td>\n                                <td></td>\n                                <td>\n                                    "+{email}+"\n                                    <br />\n                                </td>\n                            </tr>\n                        </table>\n                    </td>\n                </tr>\n              \n                <tr class=\"heading\">\n                    <td>\n                        Item\n                    </td>\n                    <td>\n                        Unit Price (&#8358;)\n                    </td>\n                    <td>\n                        Qty\n                    </td>\n                    <td style=\"text-align:left;\">\n                        Total (&#8358;)\n                    </td>\n                </tr>\n                "+{td_lines}+"\n                <tr class=\"total\">\n                    <td></td>\n                    <td></td>\n                    <td></td>\n                    <td style=\"font-weight:normal;\">\n                        VAT : "+NGN+{tax}+"\n                    </td>\n                </tr>\n                <tr class=\"total\">\n                    <td></td>\n                    <td></td>\n                    <td></td>\n                    <td>\n                        Total:  &#x20A6;"+{totalAmount}+"\n                    </td>\n                </tr>\n            </table>\n           <p>\n          <h3 style= \"                background: #eee;\n    margin-bottom:.2em;  border-bottom: 1px solid #ddd;\n                font-weight: bold;\">Details</h3>\n          Payment Method : <i> Electronic Funds Transfer, Payment Terms 100% Upfront</i> <br/> \n          Bank : <i>Guaranty Trust </i><br/>\n          Bank Account name :<i> GRIT SYSTEMS ENGINEERING LTD </i><br/>\n          Account number :<i> 0208721579 </i><br/>\n          </p>  <p style=\"font-size:0.9em\">*<i>This invoice is valid for 5 days after which a new order needs to be placed</i></p>      </div>\n    </body>\n    </html>"
          val HtmlSeq = scala.xml.Unparsed(html)
          val b = HtmlSeq.foldLeft(scala.xml.NodeSeq.Empty){(a,b)=> a ++ b}
          val outputFile = "invoice/Invoice_"+ref+".pdf"
          val os = new FileOutputStream(outputFile)

          /** Convert invoice HTML to PDF **/
          val renderer = new ITextRenderer()
          renderer.setDocumentFromString(b.text)
          renderer.getFontResolver().addFont("SourceSansPro-Regular.ttf", BaseFont.IDENTITY_H, BaseFont.NOT_EMBEDDED);
          renderer.layout()
          renderer.createPDF(os)
          os.close()
          //TODO Send Mail about invoice to Team
        }
      }
    }
  }

  /** Select sensor from capacity of powersource **/
  def capacityToSensor( capacity :Double) : (String,String,BigDecimal) = {
    val current = (capacity * 1000) / (240 * 3)
    val sensor = Sensor.findAll(("Type"->"sensor") ~ ("maxCurrent" -> ("$gte"->current))).sortWith(_.maxCurrent.value < _.maxCurrent.value)
    if (sensor.nonEmpty){
      (sensor.head.id.value,sensor.head.name.value,BigDecimal(sensor.head.price.get.Amount.value.toString))
    }else {
      ("","", BigDecimal("0.0"))
    }
  }

  /** convert 1 and 0 to boolean **/
  def toBoolean(i: Int) : Boolean = i match {
    case i if(i==1) => true
    case _ => false
  }

  def Str2Int(s: String): Int = {
    try {
      s.toInt
    } catch {
      case e: Exception => 0
    }
  }

}
