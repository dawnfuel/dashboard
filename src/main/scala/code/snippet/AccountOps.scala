package code.snippet

import net.liftweb.http.{RequestVar, S, SHtml}
import code.model._
import net.liftweb.util._
import Helpers._
import net.liftweb.common.{Box, Full}
import net.liftweb.http.SHtml.ElemAttr._
import org.joda.time.DateTime
import net.liftweb.mongodb.BsonDSL._
import org.joda.time.format.DateTimeFormat

import scala.xml.Text


/**
  * This object handles Account CRUD
  */
object AccountOps {

  /**
    * Creates account variable
    */
  object accountVar extends RequestVar[Account](Account.createRecord)
  val plans = List(("buy","Buy"),("rent","Rent"))
  val users : List[(String,String)] = User.findAll.map(u => (u.id.value.toString,u.username.value))

  /**
    * Handles the creation of  account for a user
    *
    */

  def create() = {
    val account = accountVar.is
    accountVar.CreationDate(new DateTime())
    "#name" #> SHtml.text("",(n:String)=>{accountVar.Name(n)}) &
    "#user" #> SHtml.select(users,Box.legacyNullTest(users.headOption.getOrElse(("",""))._2),(u:String)=>{accountVar.UserID_FK(u)})&
    "#plan" #> SHtml.select(plans,Box.legacyNullTest(plans.head._2),(plan:String)=>{accountVar.PaymentPlan(plan)})&
    "#monthlyInstall" #> SHtml.number(account.MonthlyInstallment.value.toDouble,(m:Double)=>{accountVar.MonthlyInstallment(BigDecimal(m))},0.0,math.pow(2,52),1)&
    "#totalInstall" #> SHtml.number(account.TotalInstallment.value.toDouble,(t:Double)=>{accountVar.TotalInstallment(BigDecimal(t))},0.0,math.pow(2,52),1)&
    "#billingDate" #> SHtml.number(account.BillingDate.value,(n:Int)=>{accountVar.BillingDate(n)},0,28)&
    "#submit" #> SHtml.onSubmitUnit(processSubmit)
  }

  /**
    * Creates the total amount owing
    * @param accountId Account Id to authenticate as
    * @return Returns sum of the invoicelines
    */
  def totalOwing(accountId : String) : BigDecimal = {
    GRIT_Invoice.findAll(("AccountID_FK" -> accountId)~("Status" ->"unpaid")).map{
      invoice =>
        if(invoice.Lines.value.nonEmpty){
          invoice.Lines.value.map{line =>
            line.UnitPrice.value * BigDecimal(line.Quantity.value)
          }.sum
        } else {
          BigDecimal(0.0)
        }
    }.sum
  }

  /**
    * Handles the display of account owed and paid
    * @return Returns account details in a table
    */

  def list() = {
    val formatter = java.text.NumberFormat.getInstance
    val accounts = Account.findAll
    "#invoiceList" #> accounts.map{ account =>

      val owing : BigDecimal  = totalOwing(account.id.value)
      val paid : BigDecimal = Payment.findAll("AccountID_FK",account.id.value).map{_.Amount.value.Amount.value}.sum

      ".name" #>  account.Name.value &
        ".user" #>  account.Name.value &
        ".owing" #> formatter.format(owing) &
        ".paid" #> formatter.format(paid) &
        ".actions *" #> {SHtml.link("/system/payments/editaccount?action=View", () => accountVar(account), Text("view")) ++ Text(" ") ++
          SHtml.link("/system/payments/editaccount?action=Edit", () => accountVar(account), Text("edit")) ++ Text(" ") ++
          SHtml.link("/system/payments/deleteaccount", () => accountVar(account), Text("delete"))}
   }
  }

  /**
    *Handles the editing of account details
    * @return Returns edited data to the server
    */

  def edit() = {
    if ( ! accountVar.set_? )
      S.redirectTo("/system/payments/listaccounts")

    val params = S.request.map(_.request.param("action")) openOr ("Undefined")
    val action = params.asInstanceOf[List[String]].headOption.getOrElse("Undefined action")
    val vieworedit = action match {
      case "Edit" =>
        pairToBasic("enabled","disabled")
      case _ =>
        pairToBasic("disabled","disabled")
    }
    val account = accountVar.is
    "#hidden" #> SHtml.hidden(() => accountVar(account),vieworedit) &
    ".panel-title"  #> Text(action + " Account ") &
    "#name" #> SHtml.text(accountVar.Name.value,(n:String)=>{accountVar.Name(n)},vieworedit) &
      "#user" #> SHtml.select(users,Box.legacyNullTest(users.filter(u => u._1 == account.UserID_FK.value).headOption.getOrElse(("",""))._1),(u:String)=>{accountVar.UserID_FK(u)},pairToBasic("disabled","disabled"))&
      "#plan" #> SHtml.select(plans,Box.legacyNullTest(account.PaymentPlan.value),(plan:String)=>{accountVar.PaymentPlan(plan)},vieworedit)&
      "#monthlyInstall" #> SHtml.number(account.MonthlyInstallment.value.toDouble,(m:Double)=>{accountVar.MonthlyInstallment(BigDecimal(m))},0.0,math.pow(2,52),1,vieworedit)&
      "#totalInstall" #> SHtml.number(account.TotalInstallment.value.toDouble,(t:Double)=>{accountVar.TotalInstallment(BigDecimal(t))},0.0,math.pow(2,52),1,vieworedit)&
      "#billingDate" #> SHtml.number(account.BillingDate.value,(n:Int)=>{accountVar.BillingDate(n)},0,28,vieworedit)&
      "#submit" #> SHtml.onSubmitUnit(processSubmit)
  }

  /**
    * Saves the edited account and redirects to 'listaccounts'
    *
    */

  def processSubmit() = {
    accountVar.is.validate match {
      case  Nil =>
        accountVar.is.save()
        S.notice("Configuration Saved")
        S.redirectTo("/system/payments/listaccounts")
      case errors => S.error(errors)
    }
  }

  /**
    * Handles accounts delete
    * @return Returns 'listaccounts'
    */

  def delete() = {
    val account  = accountVar.is

    def deleteAccount() = {
      account.delete_!
    }

    "#name" #> account.Name.value &
      "#yes" #> SHtml.link("/system/payments/listaccounts", () => {
        deleteAccount() }, Text("Delete")) &
      "#no" #> SHtml.link("/system/payments/listaccounts", () => { }, Text("Cancel"))
  }
}
