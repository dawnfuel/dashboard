package code.snippet

import code.model._

import scala.util.parsing.json.JSONArray
import scala.xml.{NodeSeq, Text}
import net.liftweb.util.Helpers._
import net.liftweb.util.JsonCmd
import net.liftweb.http.SHtml.jsonForm
import net.liftweb.http.JsonHandler
import net.liftweb.http.js.JsCmd
import net.liftweb.http.js.JsCmds.{ReplaceOptions, Script, SetHtml, _}
import net.liftweb._
import http._
import S._
import SHtml._
import common._
import util._
import Helpers._
import js._
import code.lib.MSHtml
import code.model.Probe._
import com.sun.corba.se.spi.ior.ObjectId
import net.liftweb._
import net.liftweb.http._
import util._
import common._
import common.Loggable
import Helpers._
import TimeHelpers._
import net.liftweb.common.Logger

import scala.xml._
import net.liftweb.record.field._
import net.liftweb.record._
import net.liftweb.mongodb.record._
import net.liftweb.mongodb.BsonDSL._
import org.bson.types._

import scala.collection.mutable.ListBuffer
import scala.xml.{NodeSeq, Text}
import net.liftweb.util.JsonCmd
import net.liftweb.http.SHtml.jsonForm
import net.liftweb.http.JsonHandler
import net.liftweb.http.js.JsCmd
import net.liftweb.http.js.JsCmds.{Script, SetHtml}

import scala.util.control.Breaks._
import JE._
import net.liftweb.http.SHtml.ElemAttr._
import org.elasticsearch.action.search.SearchResponse
import org.elasticsearch.client.Client
import org.elasticsearch.client.transport.TransportClient
import org.elasticsearch.common.transport.InetSocketTransportAddress
import org.elasticsearch.index.query.QueryBuilders
import org.elasticsearch.search.sort.SortOrder
import code.rest.ESQueryAPI.getESparams
import net.liftweb.json.JsonAST.JValue
import org.elasticsearch.action.update.UpdateRequest
import org.elasticsearch.search.SearchHit


/**
  *This class acts as the Controller for the 'Inverter Source''
  */
class InverterOps extends Loggable {

  var oneout, onein, twoout, twoin, threeout, threein  = "UNASSIGNED"
  val currentUserId = S.get("selectedUserID").toOption.getOrElse(User.currentUserId.headOption.getOrElse(""))
  var configID = ""
  object InverterVar extends RequestVar[Inverter](Inverter.createRecord)
  object batteryLifeMap0 extends RequestVar[BatteryLife](BatteryLife.createRecord)
  object batteryLifeMap1 extends RequestVar[BatteryLife](BatteryLife.createRecord)
  object batteryLifeMap2 extends RequestVar[BatteryLife](BatteryLife.createRecord)
  object batteryLifeMap3 extends RequestVar[BatteryLife](BatteryLife.createRecord)
  object oWindowVar extends RequestVar[operatingWindowElem](operatingWindowElem.createRecord)

  var numberOfPhases:Int=1
  var probeID : String = "VALIDATIONFILLER"

  /**
    * @param in inverter source
    * @return link to 'listsource'
    */
  def processSubmit( in : String ) : Any = {
    /** confirm uniqueness of source name to configuration */
    logger.info("saving inverter ")
    if (!powerSourceFunctions.validateSourceName(configID, InverterVar.is.sourceName._1.toString)) {
      InverterVar.ProbeID_FK(probeID)
      InverterVar.ConfigID_FK(configID)
      InverterVar.is.NumberOfPhases(numberOfPhases)
      /** adding operating window */
      InverterVar.is.operatingWindow(oWindowVar)

      val mapList: List[BatteryLife] = List(batteryLifeMap0, batteryLifeMap1, batteryLifeMap2, batteryLifeMap3)
      InverterVar.is.batteryLife(mapList)
      println("Validating 1")
      InverterVar.is.validate match {

        case Nil => {
          println("Validating 2")
          /**  write SourceID_FK of newly created Source and channel assignments to Channels of selected probe */
          println("SELECT1 : " + oneout + " | " + onein)
          println("SELECT2 : " + twoout + " | " + twoin)
          println("SELECT3 : " + threeout + " | " + threein)
          configureChannels( InverterVar.is, List(onein, twoin, threein), List(oneout, twoout, threeout)) //set the channels accordingly
          InverterVar.is.save() /** save the power source */
          S.notice("Power Source Saved")
          /** S.seeOther throws an exception to interrupt the flow of execution and redirect */
          /** the user to the specified URL */
          S.seeOther("/system/source/listsource")
        }

        case errors => {
          println("Validation Failed : " + InverterVar.is.id.toString)
          S.error(errors)
        }
      }
    }
    /** exiting the function here causes the form to reload */
  }

  case class Item(id: String, name: String)
  def g(v:Probe) = List(Item(v.id.toString, v.probeName.toString))

  /**
    * get current user
    *
    * find all configurations that belong to the current user
     */

  val currentConfs = Configuration.findAll("UserID_FKs",currentUserId)
  val optionsconf = currentConfs.map{ conf =>
    if(findChannelsWithNoSources(List(conf.id.toString)).length >= numberOfPhases) {
      /**  only list those configurations with sufficient channels */
      logger.info(conf.configurationName)
      (conf.id.toString, conf.configurationName.toString)
    }
  }.filter(_ !=(())).asInstanceOf[Seq[(String,String)]]


  private def replaceAllChannels( channels : Seq[(String,String)] ): List[JsCmd] = {
    /** return a list of JsCmds to populate the channel <select>s */
    if(numberOfPhases >1){ return List(
      ReplaceOptions("sourcechannel_3in", channels.toList, Box.legacyNullTest(channels(5)._1.toString)),
      ReplaceOptions("sourcechannel_2in", channels.toList, Box.legacyNullTest(channels(4)._1.toString)),
      ReplaceOptions("sourcechannel_1in", channels.toList, Box.legacyNullTest(channels(3)._1.toString)),
      ReplaceOptions("sourcechannel_3out", channels.toList, Box.legacyNullTest(channels(2)._1.toString)),
      ReplaceOptions("sourcechannel_2out", channels.toList, Box.legacyNullTest(channels(1)._1.toString)),
      ReplaceOptions("sourcechannel_1out", channels.toList, Box.legacyNullTest(channels(0)._1.toString))
    )
    }else{
      return List(
        ReplaceOptions("sourcechannel_1out", channels.toList, Box.legacyNullTest(channels(0)._1.toString)),
        ReplaceOptions("sourcechannel_1in", channels.toList, Box.legacyNullTest(channels(1)._1.toString))
      )
    }
  }

  /**
    * @param probes list of probes selected
    * @return channels if condition is met else nothing
    */
  def set_sourceProbes ( probes : List[String] ) : JsCmd = {
    logger.info("Set Probes to ")
    probes.foreach(println)
    InverterVar.is.ProbeID_FK(probes.toString)
    val channels = probes.map{ p =>
      logger.info(" sweeping channels for probe : "+p)
      logger.info(" ConfigID_FK                 : "+configID)
      findChannelsForProbe(p,configID)
    }.flatten
    logger.info("number of channels available : "+channels.length+"")
    logger.info("number of channels required  : "+numberOfPhases+"")
    if(channels.length >= numberOfPhases * 2){
      return replaceAllChannels(channels)
    }else{
      S.appendJs(Run("insufficientChannelsProbeSelectErrorDialog(%s,%s);".format(channels.length,numberOfPhases*2)))
      return Noop
    }
  }

  /**
    * @param config Id of user
    * @return html page
    */
  def set_sourceConfig ( config : String ) : List[JsCmd] = {
    logger.info("Set Config to " + config )
    configID = config
    val channels = findChannelsWithNoSources(List(config))
    channels.foreach(println)
    val probes_associated = channels.map { c => c._1 }.map { p_ch => (p_ch.split("_")(0), p_ch.split("_")(1)) }.groupBy(_._1).map { p => p._1 }
    val optionsprobe = probes_associated.map { probe_id => (probe_id, Probe.findAll("_id", probe_id).head.probeName.toString) }.toList // map the list of all probes for the select probe
    return List(MSHtml.ReplaceOptionsMultiSelect("sourceprobes", optionsprobe, probes_associated.toSeq) :: MSHtml.replaceAllChannels(channels, "sourcechannel", numberOfPhases))
  }

  /**
    * @return html containing source channels
    */
  def render : CssSel = {
    //list of name and id tupules of probes with sufficient channels
    InverterVar.is.NumberOfPhases(numberOfPhases)
    println()
    logger.info("Loaded Create "+numberOfPhases+" phase inverter")
    logger.info("Configurations associated with "+currentUserId+" : ")
    val selectedConf = Configuration.findAll("_id",optionsconf.headOption.getOrElse(("",""))._1)
    configID = optionsconf.headOption.getOrElse(("",""))._1
    println("configID : "+configID)
    val channels = findChannelsWithNoSources(selectedConf.map{ conf => conf.id.toString})
    val probes_associated = channels.map{ c => c._1 }.map{ p_ch => (p_ch.split("_")(0),p_ch.split("_")(1))}.groupBy(_._1).map{ p => p._1}
    val optionsprobe = probes_associated.map{ probe_id => (probe_id,Probe.findAll("_id",probe_id).head.probeName.toString) }.toSeq    // map the list of all probes for the select probe
    println("Channels Length : "+channels.length)
    def channel_cssSel():CssSel = {
      if (numberOfPhases == 3) {
        return {
          "id=sourcechannel_1out" #> SHtml.select(channels, Box.legacyNullTest(channels(0)._1), oneout = _) &
            "id=sourcechannel_1in" #> SHtml.select(channels, Box.legacyNullTest(channels(1)._1), onein = _) &
            "id=sourcechannel_2out" #> SHtml.select(channels, Box.legacyNullTest(channels(2)._1), twoout = _) &
            "id=sourcechannel_2in" #> SHtml.select(channels, Box.legacyNullTest(channels(3)._1), twoin = _) &
            "id=sourcechannel_3out" #> SHtml.select(channels, Box.legacyNullTest(channels(4)._1), threeout = _) &
            "id=sourcechannel_3in" #> SHtml.select(channels, Box.legacyNullTest(channels(5)._1), threein = _)
        }
      }
      else{
        return {
          "id=sourcechannel_1out" #> SHtml.select(channels, Box.legacyNullTest(channels(0)._1), oneout = _) &
          "id=sourcechannel_1in" #> SHtml.select(channels, Box.legacyNullTest(channels(1)._1), onein = _)
        }
      }
    }
      if (channels.length >= numberOfPhases * 2) {
          return {
            "id=sourcetype" #> SHtml.onSubmit(InverterVar.is.Type(_)) &
              "#numberofphases" #> SHtml.onSubmit(phases => numberOfPhases = phases.toInt) &
              "#rating" #> SHtml.number(InverterVar.is.Rating.value, rating => InverterVar.is.Rating(rating), 5,99999999) &
              "#runtime" #> SHtml.number(InverterVar.is.eThreshold.value,(time: Double)=>{InverterVar.is.eThreshold(energyThreshold(time,InverterVar.is.batteryBankVoltage.value,InverterVar.is.depthOfDischarge.value))}, 0,100,0.1) &
              "#sourcename" #> SHtml.text(InverterVar.is.sourceName.value, name => InverterVar.is.sourceName(name), pairToBasic("autofocus","autofocus")) &
              "#signalThreshold" #> SHtml.number(InverterVar.is.SignalThreshold.value, (th :Int) => InverterVar.is.SignalThreshold(th),0,1000) &
              "#sourceprobes" #> MSHtml.ajaxUntrustedMultiSelect(optionsprobe, probes_associated.toSeq, set_sourceProbes) &
              "#sourceconfig" #> SHtml.ajaxSelect(optionsconf, Box.legacyNullTest(optionsconf.headOption.getOrElse(("",""))._1), set_sourceConfig) &
              channel_cssSel() &
              "#operatingWindowF" #> SHtml.text("00:00",oWindowVar.is.from(_)) &
              "#operatingWindowT" #> SHtml.text("24:00",oWindowVar.is.to(_)) &
              "#efficiency" #> SHtml.number(InverterVar.is.Efficiency.value, efficiency => InverterVar.is.Efficiency(efficiency), 0, 99999999 ) &
              "#chargecurrent" #> SHtml.number(InverterVar.is.chargeCurrent.value, current => InverterVar.is.chargeCurrent(current), 5, 99999999 ) &
              "#battcapacity" #> SHtml.number(InverterVar.is.batteryCapacity.value, bCapacity => InverterVar.is.batteryCapacity(bCapacity), 5, 99999999 ) &
              "#battcount" #> SHtml.number(InverterVar.is.batteryCount.value, bCount => InverterVar.is.batteryCount(bCount), 0, 99999999 ) &
              "#battEndOfLife" #> SHtml.number(InverterVar.is.batteryEndOfLife.value, bEOF => InverterVar.is.batteryEndOfLife(bEOF), 0, 100) &
              "#battvoltage" #> SHtml.number(InverterVar.is.batteryVoltage.value, bVolt => InverterVar.is.batteryVoltage(bVolt), 0, 99999999 ) &
              "#battbankvoltage" #> SHtml.number(InverterVar.is.batteryBankVoltage.value, bBVolt => InverterVar.is.batteryBankVoltage(bBVolt), 0, 99999999 ) &
              "#dod" #> SHtml.number(InverterVar.is.depthOfDischarge.value, DoD => InverterVar.is.depthOfDischarge(DoD), 5, 99999999 ) &
              "#cost" #> SHtml.number(InverterVar.is.cost.value, cost => InverterVar.is.cost(cost), 60000, 999999999 ) &
              "#dod0" #> SHtml.number(batteryLifeMap0.is.DoD.value, DoD => batteryLifeMap0.is.DoD(DoD), 5, 99999999 ) &
              "#cycle0" #> SHtml.number(batteryLifeMap0.is.numberOfCycles.value, cycle => batteryLifeMap0.is.numberOfCycles(cycle), 5, 99999999 ) &
              "#bcap0" #> SHtml.number(batteryLifeMap0.is.bCapacity.value, bcap => batteryLifeMap0.is.bCapacity(bcap), 5, 99999999 ) &
              "#dod1" #> SHtml.number(50, DoD => batteryLifeMap1.is.DoD(DoD), 5, 99999999 ) &
              "#cycle1" #> SHtml.number(500, cycle => batteryLifeMap1.is.numberOfCycles(cycle), 5, 99999999 ) &
              "#bcap1" #> SHtml.number(50, bcap => batteryLifeMap1.is.bCapacity(bcap), 5, 99999999 ) &
              "#dod2" #> SHtml.number(30, DoD => batteryLifeMap2.is.DoD(DoD), 5, 99999999 ) &
              "#cycle2" #> SHtml.number(1200, cycle => batteryLifeMap2.is.numberOfCycles(cycle), 5, 99999999 ) &
              "#bcap2" #> SHtml.number(50, bcap => batteryLifeMap2.is.bCapacity(bcap), 5, 99999999 ) &
              "#dod3" #> SHtml.number(10, DoD => batteryLifeMap3.is.DoD(DoD), 5, 99999999 ) &
              "#cycle3" #> SHtml.number(1800, cycle => batteryLifeMap3.is.numberOfCycles(cycle), 5, 99999999 ) &
              "#bcap3" #> SHtml.number(50, bcap => batteryLifeMap3.is.bCapacity(bcap), 5, 99999999 ) &
              "#chemistry" #> SHtml.onSubmit(InverterVar.is.batteryChemistry(_)) &
              "#submit" #> SHtml.onSubmit(processSubmit)
          }

      } else {
        S.appendJs(Run("insufficientChannelsErrorDialog();"))
        return {
          "#submit" #> SHtml.onSubmit(processSubmit)
        }
      }
  }

  def view={
    val params = S.request.map(_.request.param("action")) openOr ("Undefined")
    val action = params.asInstanceOf[List[String]].headOption.getOrElse("Undefined action")
    val vieworedit = action match {
      case "Edit" =>
        pairToBasic("enabled","disabled")
      case _ =>
        pairToBasic("disabled","disabled")
    }
    /** list of name and id tupules of probes with sufficient channels */
    val s_id = S.params("id").headOr("")  /** extract sourceid */
    val source = Inverter.find("_id",s_id ).getOrElse[Inverter](Inverter) //get the source with the id
    numberOfPhases = source.NumberOfPhases.value
    logger.info(action+" "+numberOfPhases+" phase Inverter : "+source.sourceName)
    object inverterVar extends RequestVar[Inverter](source)
    val (channel_available, channel_assigned) = findChannelsForSource(List(inverterVar.is.id.toString),List(inverterVar.is.ConfigID_FK.toString),false)
    val probes_associated = channel_available.map{ c => c._1 }.map{ p_ch => (p_ch.split("_")(0),p_ch.split("_")(1))}.groupBy(_._1).map{ p => p._1}
    val optionsprobe = probes_associated.map{ probe_id => (probe_id,Probe.findAll("_id",probe_id).head.probeName.toString) }.toSeq    /** map the list of all probes for the select probe */
    val current_config = Configuration.findAll("_id", inverterVar.is.ConfigID_FK.toString).head
    val optionsconf = List((current_config.id.value, current_config.configurationName.value))
    configID = optionsconf.headOption.getOrElse(("",""))._1

    /** when submitted */
    def saveEdit(x:String)={
      /** reset signal assignment for this source */
      findChannelsForSource(List(inverterVar.is.id.toString),List(inverterVar.is.ConfigID_FK.toString),true)
      /** update operation window */
      inverterVar.is.operatingWindow(oWindowVar)
      inverterVar.saveTheRecord() /** save the source */
      println("passed channels : "+( onein, oneout, twoin, twoout, threein, threeout ))
      configureChannels(inverterVar.is, List(onein, twoin, threein), List( oneout, twoout, threeout )) /** set the channels accordingly and save probe */
      S.notice("Power Source Saved")
      /** Redirect back to probe if change reassign redirected here */
      val delProbe = S.get("delprobeID").toOption.getOrElse("")
      if(delProbe.nonEmpty)
        S.seeOther("/system/probe/deleteprobe?delP=%s".format(delProbe.toString))
      S.seeOther("/system/source/listsource")
    }
    println
    channel_assigned.foreach(println)
    println
    def custonCSSel():CssSel= {
      if (numberOfPhases > 1) {
        return {
          "id=sourcechannel_1out" #> SHtml.select(channel_available, Box.legacyNullTest(channel_assigned(0)(0)._1), ch => oneout = ch , vieworedit ) &
            "id=sourcechannel_2out" #> SHtml.select(channel_available, Box.legacyNullTest(channel_assigned(1)(0)._1), ch => twoout = ch , vieworedit ) &
            "id=sourcechannel_3out" #> SHtml.select(channel_available, Box.legacyNullTest(channel_assigned(2)(0)._1), ch => threeout = ch , vieworedit ) &
            "id=sourcechannel_1in" #> SHtml.select(channel_available, Box.legacyNullTest(channel_assigned(3)(0)._1), ch => onein = ch , vieworedit ) &
            "id=sourcechannel_2in" #> SHtml.select(channel_available, Box.legacyNullTest(channel_assigned(4)(0)._1), ch => twoin = ch , vieworedit ) &
            "id=sourcechannel_3in" #> SHtml.select(channel_available, Box.legacyNullTest(channel_assigned(5)(0)._1), ch => threein = ch, vieworedit )
        }
      } else {
        S.appendJs(Run("""$("#chnnls").trigger('removeChs');"""))
        return {
          "id=sourcechannel_1out" #> SHtml.select(channel_available, Box.legacyNullTest(channel_assigned(0)(0)._1), ch => oneout = ch , vieworedit ) &
            "id=sourcechannel_1in" #> SHtml.select(channel_available, Box.legacyNullTest(channel_assigned(1)(0)._1), ch => onein = ch ,  vieworedit)
        }
      }
    }
    "#heading_action" #> SHtml.span(<h5 class="alt" id="heading_action">{action} {numberOfPhases} phase Inverter power source : {source.sourceName}</h5>, (Noop)   , vieworedit ) &
    "#hidden" #> SHtml.hidden(() => inverterVar(inverterVar.is)  , vieworedit ) &
      custonCSSel &
      "id=sourcetype" #> SHtml.onSubmit(inverterVar.is.Type(_)  ) &
      "#sourceconfig" #> SHtml.ajaxSelect(optionsconf, Box.legacyNullTest(optionsconf.headOption.getOrElse(("",""))._1), set_sourceConfig(_), pairToBasic("disabled","disabled")) &
      "#operatingWindowF" #> SHtml.text(inverterVar.is.operatingWindow._1.from._1.toString,oWindowVar.is.from(_) , vieworedit ) &
      "#operatingWindowT" #> SHtml.text(inverterVar.is.operatingWindow._1.to._1.toString,oWindowVar.is.to(_) , vieworedit ) &
      "#rating" #> SHtml.number(inverterVar.is.Rating.value, (rating : Int) => inverterVar.is.Rating(rating), 0,99999999 , vieworedit ) &
      "#runtime" #> SHtml.number(runTime(inverterVar.is.eThreshold.value,inverterVar.is.batteryBankVoltage.value,inverterVar.is.depthOfDischarge.value),(time: Double)=>{inverterVar.is.eThreshold(energyThreshold(time,inverterVar.is.batteryBankVoltage.value,inverterVar.is.depthOfDischarge.value))} , 0,100,0.1) &
      "#efficiency" #> SHtml.number(inverterVar.is.Efficiency.value, (efficiency: Int) => inverterVar.is.Efficiency(efficiency), 0, 99999999  , vieworedit ) &
      "#chargecurrent" #> SHtml.number(inverterVar.is.chargeCurrent.value, (current: Int) => inverterVar.is.chargeCurrent(current), 5, 99999999  , vieworedit ) &
      "#battcapacity" #> SHtml.number(inverterVar.is.batteryCapacity.value, (bCapacity: Int) => inverterVar.is.batteryCapacity(bCapacity), 5, 99999999  , vieworedit ) &
      "#battcount" #> SHtml.number(inverterVar.is.batteryCount.value, (bCount: Int) => inverterVar.is.batteryCount(bCount), 0, 99999999  , vieworedit ) &
      "#battEndOfLife" #> SHtml.number(inverterVar.is.batteryEndOfLife.value, (bEOF: Int) => inverterVar.is.batteryEndOfLife(bEOF), 0, 100 , vieworedit ) &
      "#battvoltage" #> SHtml.number(inverterVar.is.batteryVoltage.value, (bVolt: Int) => inverterVar.is.batteryVoltage(bVolt), 0, 99999999  , vieworedit ) &
      "#battbankvoltage" #> SHtml.number(inverterVar.is.batteryBankVoltage.value, (bBVolt: Int) => inverterVar.is.batteryBankVoltage(bBVolt), 0, 99999999  , vieworedit ) &
      "#dod" #> SHtml.number(inverterVar.is.depthOfDischarge.value, (DoD: Int) => inverterVar.is.depthOfDischarge(DoD), 5, 99999999  , vieworedit ) &
      "#cost" #> SHtml.number(inverterVar.is.cost.value, (cost: Int) => inverterVar.is.cost(cost), 60000, 999999999  , vieworedit ) &
      "#dod0" #> SHtml.number(inverterVar.is.batteryLife._1(0).DoD.value, (DoD: Int) => inverterVar.is.batteryLife._1(0).DoD(DoD), 5, 99999999  , vieworedit ) &
      "#cycle0" #> SHtml.number(inverterVar.is.batteryLife._1(0).numberOfCycles.value, (cycle: Int) => inverterVar.is.batteryLife._1(0).numberOfCycles(cycle), 5, 99999999  , vieworedit ) &
      "#bcap0" #> SHtml.number(inverterVar.is.batteryLife._1(0).bCapacity.value, (bcap: Int) => inverterVar.is.batteryLife._1(0).bCapacity(bcap), 5, 99999999  , vieworedit ) &
      "#dod1" #> SHtml.number(inverterVar.is.batteryLife._1(1).DoD.value, (DoD: Int) => inverterVar.is.batteryLife._1(1).DoD(DoD), 5, 99999999  , vieworedit ) &
      "#cycle1" #> SHtml.number(inverterVar.is.batteryLife._1(1).numberOfCycles.value, (cycle: Int) => inverterVar.is.batteryLife._1(1).numberOfCycles(cycle), 5, 99999999  , vieworedit ) &
      "#bcap1" #> SHtml.number(inverterVar.is.batteryLife._1(1).bCapacity.value, (bcap: Int) => inverterVar.is.batteryLife._1(1).bCapacity(bcap), 5, 99999999  , vieworedit ) &
      "#dod2" #> SHtml.number(inverterVar.is.batteryLife._1(2).DoD.value, (DoD: Int) => inverterVar.is.batteryLife._1(2).DoD(DoD), 5, 99999999  , vieworedit ) &
      "#cycle2" #> SHtml.number(inverterVar.is.batteryLife._1(2).numberOfCycles.value, (cycle: Int) => inverterVar.is.batteryLife._1(2).numberOfCycles(cycle), 5, 99999999  , vieworedit ) &
      "#bcap2" #> SHtml.number(inverterVar.is.batteryLife._1(2).bCapacity.value, (bcap: Int) => inverterVar.is.batteryLife._1(2).bCapacity(bcap), 5, 99999999  , vieworedit ) &
      "#dod3" #> SHtml.number(inverterVar.is.batteryLife._1(3).DoD.value, (DoD: Int) => inverterVar.is.batteryLife._1(3).DoD(DoD), 5, 99999999  , vieworedit ) &
      "#cycle3" #> SHtml.number(inverterVar.is.batteryLife._1(3).numberOfCycles.value, (cycle: Int) => inverterVar.is.batteryLife._1(3).numberOfCycles(cycle), 5, 99999999  , vieworedit ) &
      "#bcap3" #> SHtml.number(inverterVar.is.batteryLife._1(3).bCapacity.value, (bcap: Int) => inverterVar.is.batteryLife._1(3).bCapacity(bcap), 5, 99999999  , vieworedit ) &
      "#chemistry" #> SHtml.onSubmit(inverterVar.is.batteryChemistry(_)  ) &
      "#sourcename" #> SHtml.text(inverterVar.is.sourceName.value, name => inverterVar.is.sourceName(name) , vieworedit ) &
      "#signalThreshold" #> SHtml.number(InverterVar.is.SignalThreshold.value, (th :Int) => InverterVar.is.SignalThreshold(th),0,1000) &
      "#sourceprobes" #> MSHtml.ajaxUntrustedMultiSelect(optionsprobe, probes_associated.toSeq, set_sourceProbes(_), vieworedit ) &
      "#resetBttn" #>  SHtml.ajaxButton("Reset",() => {resetBattCap(configID);Noop}) &
//      "#sourceprobes" #> ajaxSelect(optionsprobe, Box.legacyNullTest(optionsprobe.head._1), { s => After(200, MSHtml.replaceAllChannels(s))} , vieworedit ) &
    "id=saveEdit" #> SHtml.onSubmit(x => saveEdit(x))
  }

  def resetBattCap (configID :String) = {
    println("Config : " + configID)
    val (eshost,esjavaport,eshttpport) = getESparams()
    val client: Client = TransportClient.builder().build().addTransportAddress(new InetSocketTransportAddress(java.net.InetAddress.getByName(eshost), esjavaport))

    val query = QueryBuilders.matchQuery("ConfigID_FK", configID)

    val response_desc: SearchResponse = client
      .prepareSearch("grit")
      .setTypes("docs")
      .setQuery(query)
      .setSize(1)
      .addSort("Time", SortOrder.DESC)
      .execute()
      .actionGet()

    val hits_desc: Array[SearchHit] = response_desc.getHits().getHits

    if (hits_desc.length > 0) {
      val lastID = hits_desc(0).getId
      println("esID : " + lastID)
      val updateRequest = new UpdateRequest()
      updateRequest.index("grit")
      updateRequest.`type`("docs")
      updateRequest.id(lastID)
      updateRequest.doc("batterycapacity",100)
      client.update(updateRequest).get

      client.close()
    }
  }

  def configureChannels(genobj:Inverter,chIn:List[String],chOut:List[String]){
    val filtered_channels = (chIn:::chOut).map{ ch => if(ch != "UNASSIGNED"){ch}}.filter(_ !=(())).asInstanceOf[List[String]]
    val probes_associated = filtered_channels.map{ p_ch => (p_ch.split("_")(0),p_ch.split("_")(1))}.groupBy(_._1)
    logger.info("Configure Channels : "+chIn+chOut)
    val (List(onein,twoin,threein),List(oneout,twoout,threeout)) = (chIn,chOut)
    // loop through Channels of selected probe
    probes_associated.map{  probes =>
      val channels_associated = probes._2.map{ l => l._2.toInt }
      println("probes "+probes)
      println("channels associated "+channels_associated)
      Probe.findAll("_id",probes._1).map { probe =>
        var counter_channel = 0
        probe.Channels.get.foreach(a => {
          if (a.SourceID_FK.toString.isEmpty && numberOfPhases > 1 && channels_associated.contains(counter_channel)) {
            if (counter_channel == oneout.split("_")(1).toInt) {
              a.SourceID_FK(genobj.id.toString)
              a.sourceChannel(oneout.split("_")(1).toInt)
              a.SourceType(genobj.Type.toString)
              a.SourceName(genobj.sourceName.toString)
              a.Direction("OUT")
              a.phase(1)
              logger.info("JUST ASSIGNED : " + probe.probeName + "_"+counter_channel+" to "+InverterVar.is.sourceName )
            } else if (counter_channel == onein.split("_")(1).toInt && channels_associated.contains(counter_channel) )  {
              a.SourceID_FK(genobj.id.toString)
              a.sourceChannel(onein.split("_")(1).toInt)
              a.SourceType(genobj.Type.toString)
              a.SourceName(genobj.sourceName.toString)
              a.Direction("IN")
              a.phase(1)
              logger.info("JUST ASSIGNED : " + probe.probeName + "_"+counter_channel+" to "+InverterVar.is.sourceName )
            } else if ((counter_channel.toInt == twoout.split("_")(1).toInt) && (genobj.NumberOfPhases.get == 3) && channels_associated.contains(counter_channel)) {
              a.SourceID_FK(genobj.id.toString)
              a.sourceChannel(twoout.split("_")(1).toInt)
              a.SourceType(genobj.Type.toString)
              a.SourceName(genobj.sourceName.toString)
              a.Direction("OUT")
              a.phase(2)
              logger.info("JUST ASSIGNED : " + probe.probeName + "_"+counter_channel+" to "+InverterVar.is.sourceName )
            } else if ((counter_channel.toInt == twoin.split("_")(1).toInt) && (genobj.NumberOfPhases.get == 3) && channels_associated.contains(counter_channel)) {
              a.SourceID_FK(genobj.id.toString)
              a.sourceChannel(twoin.split("_")(1).toInt)
              a.SourceType(genobj.Type.toString)
              a.SourceName(genobj.sourceName.toString)
              a.Direction("IN")
              a.phase(2)
              logger.info("JUST ASSIGNED : " + probe.probeName + "_"+counter_channel+" to "+InverterVar.is.sourceName )
            } else if ((counter_channel.toInt == threeout.split("_")(1).toInt) && (genobj.NumberOfPhases.get == 3) && channels_associated.contains(counter_channel)) {
              a.SourceID_FK(genobj.id.toString)
              a.sourceChannel(threeout.split("_")(1).toInt)
              a.SourceType(genobj.Type.toString)
              a.SourceName(genobj.sourceName.toString)
              a.Direction("OUT")
              a.phase(3)
              logger.info("JUST ASSIGNED : " + probe.probeName + "_"+counter_channel+" to "+InverterVar.is.sourceName )
            } else if ((counter_channel.toInt == threein.split("_")(1).toInt) && (genobj.NumberOfPhases.get == 3) && channels_associated.contains(counter_channel)) {
              a.SourceID_FK(genobj.id.toString)
              a.sourceChannel(threein.split("_")(1).toInt)
              a.SourceType(genobj.Type.toString)
              a.SourceName(genobj.sourceName.toString)
              a.Direction("IN")
              a.phase(3)
              logger.info("JUST ASSIGNED : " + probe.probeName + "_"+counter_channel+" to "+InverterVar.is.sourceName )
            }
          } else if (a.SourceID_FK.toString.isEmpty) {
            if (counter_channel == oneout.split("_")(1).toInt && channels_associated.contains(counter_channel) ) {
              a.SourceID_FK(genobj.id.toString)
              a.sourceChannel(oneout.split("_")(1).toInt)
              a.SourceType(genobj.Type.toString)
              a.SourceName(genobj.sourceName.toString)
              a.Direction("OUT")
              a.phase(1)
              logger.info("JUST ASSIGNED : " + probe.probeName + "_"+counter_channel+" to "+InverterVar.is.sourceName )
            } else if (counter_channel == onein.split("_")(1).toInt && channels_associated.contains(counter_channel)) {
              a.SourceID_FK(genobj.id.toString)
              a.sourceChannel(onein.split("_")(1).toInt)
              a.SourceType(genobj.Type.toString)
              a.SourceName(genobj.sourceName.toString)
              a.Direction("IN")
              a.phase(1)
              logger.info("JUST ASSIGNED : " + probe.probeName + "_"+counter_channel+" to "+InverterVar.is.sourceName )
            }
          } else {
            logger.info("PREVIOUSLY ASSIGNED : " + probe.probeName + "_"+counter_channel+" to "+InverterVar.is.sourceName )
          }
          counter_channel += 1
        })
        probe.save() /** save the probe */
      }
    }
    filtered_channels.map{ p_ch =>
      println(" PROBE : "+p_ch.split("_")(0)+" CH : "+p_ch.split("_")(1))
    }
  }

  def numOfPhase:NodeSeq={
    val x = S.attr("phase") openOr "4"
    numberOfPhases = x.toInt
    <span><hr/></span>
  }

  /**
    *
    * @param runTime run time figure
    * @param battVoltage battery voltage
    * @param DOD selected battery depth of discharge
    * @return  energy threshold
    */
  def energyThreshold(runTime : Double, battVoltage : Double, DOD : Double) = {
    if (runTime > 0.0) {
      val invEfficiency = 0.8
      val cableEfficiency = 0.98
      val eThreshold = (battVoltage * (DOD/100) * invEfficiency * cableEfficiency) / runTime
      eThreshold
      Math.round(eThreshold*100.0)/100.0
    } else {
      0.0
    }
  }

  /**
    *
    * @param eThreshold energy threshod
    * @param battVoltage battery voltage
    * @param DOD depth of battery discharge
    * @return
    */
  def runTime(eThreshold : Double, battVoltage : Double, DOD : Double ) = {
    if(eThreshold > 0.0){
      val runTime = ((DOD/100) * battVoltage * 0.98 * 0.8 )/ eThreshold
      Math.round(runTime*100.0)/100.0
    }else {
      0.0
    }
  }
}