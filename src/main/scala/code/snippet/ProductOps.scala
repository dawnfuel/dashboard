package code.snippet

import net.liftweb.http.{RequestVar, S, SHtml}
import code.model._
import net.liftweb.util._
import Helpers._
import net.liftweb.http.SHtml.ElemAttr._

import scala.xml.Text

/**
  * Product CRUD
  */

object ProductOps {
  object productVar extends RequestVar[GRIT_Products](GRIT_Products.createRecord)

  def processSubmit() = {
    println(productVar)
    productVar.is.save()
    S.seeOther("/system/products/listproducts")
  }

  def create() = {
    val product = productVar.is
    "#hidden" #> SHtml.hidden(() => productVar(product)) &
      "#name" #> SHtml.text(productVar.is.name.value,name=>{productVar.is.name(name)} ) &
      "#price" #> SHtml.number(productVar.is.price._1.Amount.value.toDouble ,setPrice(_),0.0,math.pow(2,52),0.001)&
      "#submit" #> SHtml.onSubmitUnit(processSubmit)
  }

  def edit() = {
    if ( ! productVar.set_? )
      S.redirectTo("/system/products/listproducts")
    val params = S.request.map(_.request.param("action")) openOr ("Undefined")
    val action = params.asInstanceOf[List[String]].headOption.getOrElse("Undefined action")
    val vieworedit = action match {
      case "Edit" =>
        pairToBasic("enabled","disabled")
      case _ =>
        pairToBasic("disabled","disabled")
    }
    val product = productVar.is

    "#hidden" #> SHtml.hidden(() => productVar(product)) &
      ".panel-title"  #> Text(action + " Product") &
      "#name" #> SHtml.text(productVar.is.name.value,name => {productVar.is.name(name)},vieworedit ) &
      "#price" #> SHtml.number(productVar.is.price.get.Amount.value.toDouble,(price:Double)=>{setPrice(price)},0.0,math.pow(2,52),0.0,vieworedit)&
      "#submit" #> SHtml.onSubmitUnit(processSubmit)
  }

  def list() = {
    val products = GRIT_Products.findAll("Type","otherProducts")
    "#sensorList" #> products.map{ product =>
      ".sensorName" #>  product.name.value &
        ".actions *" #> {SHtml.link("/system/products/editproduct?action=View", () => productVar(product), Text("view")) ++ Text(" ") ++
          SHtml.link("/system/products/editproduct?action=Edit", () => productVar(product), Text("edit")) ++ Text(" ") ++
          SHtml.link("/system/products/deleteproduct", () => productVar(product), Text("delete"))}
    }
  }

  def delete() = {
    val product = productVar.is
    def deleteProduct(){
      product.delete_!
    }
    "#name" #> product.name &
      "#yes" #> SHtml.link("/system/products/listproducts", () =>{
        deleteProduct() }, Text("Delete")) &
      "#no" #> SHtml.link("/system/products/listproducts", () =>{ }, Text("Cancel"))
  }
  def setPrice(value:Double): Unit ={
    productVar.is.price.get.Amount(value)
  }
}
