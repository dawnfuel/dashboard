package code.snippet

import code.model.{PaymentSettings}

import net.liftweb.http.{RequestVar, SHtml}
import net.liftweb.util._
import Helpers._


/** Class performing Create, Read, Update on Payment settings.
  *
  */
class PaymentSettingsOps  {
  object paymentVar extends RequestVar[PaymentSettings](PaymentSettings.createRecord)
  def settings() = {
    val payment = paymentVar.is
    val setting = PaymentSettings.findAll.headOption.getOrElse(PaymentSettings)
    "#hidden" #> SHtml.hidden(() => paymentVar(setting) ) &
    "#exRateValue" #> SHtml.number(setting.exchangeRate.value ,setExchangeRate(_) ,0.0,math.pow(2,52),0.05) &
    "#marginValue" #> SHtml.number(setting.margin.value ,setMargin(_) ,0.0,math.pow(2,52),0.05) &
    "#LiValue" #> SHtml.number(setting.leaseInterest.value ,setLeaseInterest(_) ,0.0,math.pow(2,52),0.05) &
    "#vatValue" #> SHtml.number(setting.VAT.value ,setVAT(_) ,0.0,math.pow(2,52),0.05)&
    "#subPrice" #> SHtml.number(setting.SubscriptionPrice.value.toDouble ,setSubPrice(_) ,0.0,math.pow(2,52),0.05)
  }
  def setExchangeRate(value : Double)={
    paymentVar.is.exchangeRate(value).save
  }
  def setMargin(value : Double)={
    paymentVar.is.margin(value).save
  }
  def setLeaseInterest(value : Double)={
    paymentVar.is.leaseInterest(value).save
  }
  def setVAT(value : Double)={
    paymentVar.is.VAT(value).save
  }
  def setSubPrice(value : Double)={
    paymentVar.is.SubscriptionPrice(value).save
  }
}