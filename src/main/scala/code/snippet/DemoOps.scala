package code.snippet

import code.model.User
import net.liftweb.common.Full
import net.liftweb.http.S
import net.liftweb.util.Helpers._
import scala.xml.NodeSeq
import xml.Text


/**
  * This class provides the snippets that handles the Demo user login
  */
class DemoOps {
  def login(email:String){
    println("\n got email : "+email)
    User.findByEmail(email) match {
      case Full(user) => {
        println("trying to login")
        if(user.roles.names.contains("guest")) {
          User.logUserIn(user, true)
          User.createExtSession(user.id.get)
        }
        S.redirectTo("/index.html")
      }
      case _ => { println("Error finding demo user")}
    }
  }

  /**
    *Handles  the type information of the typeclass
    */
  def manOf[T: Manifest](t: T): Manifest[T] = manifest[T]

  /**
    *
    * @return user details
    */

  def demoUsers={
    var demoDetails:List[(String,String,String)] = List(("select","grit-small.png","select demo user"))
    val users = User.findAll("roles","guest").map(u=> (u.email.toString(),u.image.toString, u.username.toString()))
    demoDetails = demoDetails ::: users

    var elem:NodeSeq = NodeSeq.Empty
    for(u <- users){
      val lnk:String = "https://dashboard.grit.systems/demo?q="+u._1+""
      if(!u._2.isEmpty) {
        val img: String = "/img/" + u._2
        elem ++= <div style="margin-bottom:2em"><a href={lnk}><img class="flex-item-img" id={u._1} src={img}>{u._2}</img></a></div>
      }else{
        elem ++= <div style="margin-bottom:2em"><a href={lnk} ><button class="ui teal massive button flex-item" id={u._1}>{u._3}</button></a></div>
      }
    }

    "#selDemo" #> elem
  }

  /**
    *
    * @param email user email
    */
  def selectUser(email:String){
    println("\n\n\n"+email)
    //val user:User = User.currentUser.headOption.getOrElse("").asInstanceOf[User]
    User.logUserOut()
    login(email)
  }

  /**
    *
    * @return redirection to demo
    */
  def demoRedirect={
    val eml = S.params("q").headOr("")
    if(!eml.isEmpty){ login(eml)}
    "#selDemo" #> Text("Loading ...")
  }

}
