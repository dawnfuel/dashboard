package code.snippet

import net.liftweb.http.js.JsCmds.After
import org.bson.types.ObjectId
import net.liftweb._
import net.liftweb.http.SHtml._
import net.liftweb.http._
import net.liftweb.http.js.{JsCmd, JsCmds}
import net.liftweb.http.js.JE._
import net.liftweb.http.js.JE
import util._
import common._
import Helpers._
import TimeHelpers._
import net.liftweb.common.Logger

import scala.xml._
import net.liftweb.record.field._
import net.liftweb.record._
import net.liftweb.mongodb.record._
import net.liftweb.mongodb.BsonDSL._
import org.bson.types._
import code.model._
import code.model.Probe._
import code.model.PowerConsumerGroup._
import S._
import code.config.Site._
import net.liftweb.http.SHtml.ElemAttr._
//import code.snippet.PowerConsumerOps.Item
import net.liftweb.http.js.JsCmds._
import net.liftweb.http.js.JsExp
import code.lib.MSHtml


/**
 * This class provides the snippets that back the Configuration CRUD 
 * pages (List, Create, View, Edit, Delete)
 */
class ConfigurationOps extends Loggable {

  def manOf[T: Manifest](t: T): Manifest[T] = manifest[T]
  var channels_to_add : List[String] = List()
  var channels_to_rm : List[String] = List()
  var channels_available : List[(String,String)] = List()
  var existing_channels : List[(String,String)] = List()
  val currentUserId = S.get("selectedUserID").toOption.getOrElse(User.currentUserId.headOption.getOrElse(""))
  var currentUserEmail = User.find("_id",new ObjectId(currentUserId)).headOption.getOrElse(User).email.toString()
  var users_to_add : List[String] = List()
  var users_to_rm : List[String] = List()
  var users_available : List[(String,String)] = List()
  var existing_users : List[(String,String)] = List()

  /** The configurationVar object is used to pass Configuration instances between successive HTML requests
  * in the following cases:
  * - when a create or edit form is submitted
  * - from a list page to a delete, view or edit page
  * - from a view page to a edit page
  * Note that Configuration.create isn't being called here. Rather, we're registering
  * a function for configurationVar to call to get a default value in the case that the
  * configurationVar object is accessed before it is set.
  */
  object configurationVar extends RequestVar[Configuration](Configuration.createRecord)
  object ConfigGroupVar extends RequestVar[ConfigGroup](ConfigGroup.createRecord)
  object userVar extends RequestVar[User](User.createRecord)

/** Called by the create form when the user clicks submit
* If the entered data passes validation then the user is redirected to the List page,
* otherwise the form on which the user clicked submit is reloaded
*/
  def processSubmitCreate() = {
    configurationVar.is.validate match {
      case  Nil =>
        configurationVar.is.UserID_FKs(existing_users.map(_._1).distinct)
        logger.info("validation passed, save configuration "+configurationVar.is.configurationName+" and assign to user "+configurationVar.is.configurationUser)
        select_channels(existing_channels.map{ ch => ch._1})
        configurationVar.is.save()
        S.notice("Configuration Saved")
        /** S.seeOther throws an exception to interrupt the flow of execution and redirect
        * the user to the specified URL
        */
        S.seeOther("/system/configuration/listconfiguration")

      case errors => S.error(errors)
    }
  }

/** Called by the edit form when the user clicks submit
* If the entered data passes validation then the user is redirected to the List page,
* otherwise the form on which the user clicked submit is reloaded
*/
  def processSubmitEdit() = {
    configurationVar.is.validate match {
      case  Nil =>
        configurationVar.is.UserID_FKs(existing_users.map(_._1).distinct)
        logger.info("validation passed, save configuration "+configurationVar.is.configurationName+" and assign to user "+configurationVar.is.configurationUser)
        delete_channels(channels_available.map{ ch => ch._1},configurationVar.is.id.toString)
        select_channels(existing_channels.map{ ch => ch._1})
        configurationVar.is.save()
        S.notice("Configuration Saved")
        S.seeOther("/system/configuration/listconfiguration")

      case errors => S.error(errors)
    }
  }

/**creates a list of Users, containing email and username
* @param v an instance of a User
*/
  def g(v:User) = List(Item(v.email.toString, v.username.toString))

/**creates a list of Users, containing User ID and username
* @param v an instance of a User
*/
  def f(v:User) = List(Item(v.id.toString, v.username.toString))
  case class Item(id: String, name: String)
  val users = User.findAll.flatMap(user => g(user))
  users.map(i => i.id -> i.name)
  val optionsuser : List[(String,String)] =
    users.map(i => i.id -> i.name)

  def set_user( id:String ) : Any =  {
    val u = User.findAll("_id",new ObjectId(id))
    u.headOption.map(usr => {
      logger.info("User set to : "+usr.username+", user ID is : "+usr.id )
      configurationVar.is.configurationUser(usr.username.toString)
    })
  }

/** When create is called on the first GET request, configurationVar.is initialises configurationVar by calling Configuration.create.
* If create is being called due to a form reload after validation has failed on a PUT request,
* configurationVar.is returns the configuration that was previously set by the SHtml.hidden function (see below)
*/
  def create = {
    logger.info("Create New Configuration")
    val allusers = User.findAll.flatMap( u => f(u))
    val optionsuser = User.currentUser.openOr(User).roles.names.contains("admin") match {
      case true => allusers.map( i =>  i.id -> i.name ).sortWith(_._2 > _._2)
      case _ => User.findAll("_id",new ObjectId(currentUserId)).flatMap( u => f(u)).map( i =>  i.id -> i.name ).sortWith(_._2 > _._2)
    }
    val configurationProbes = User.currentUser.openOr(User).roles.names.contains("admin") match {
      case true => MSHtml.ajaxMultiSelect(findProbesWithAvailableChannels, Seq[String]() , select_probes(_) )
      case _ => MSHtml.ajaxMultiSelect(findUserProbesWithAvailableChannels(currentUserId), Seq[String]() , select_probes(_) )
    }
    val default_admin = List[(String,String)](optionsuser.headOption.getOrElse(("","")))
    configurationVar.is.AdminID_FK(default_admin.headOption.getOrElse(("",""))._1)
    existing_users = default_admin
    users_available = optionsuser diff default_admin
    val configuration = configurationVar.is
    "#hidden" #> SHtml.hidden(() => configurationVar(configuration) ) &
      "#configurationuser" #> SHtml.ajaxSelect(optionsuser, Box.legacyNullTest(configurationVar.is.configurationUser.value), set_admin ) &
      "#configurationname" #> SHtml.text(configurationVar.is.configurationName.value, name => configurationVar.is.configurationName(name)) &
      "#allusers" #> MSHtml.ajaxUntrustedMultiSelect(optionsuser diff default_admin, Seq[String]() , set_add_users) &
      "#groupmembers" #> MSHtml.ajaxUntrustedMultiSelect(default_admin, Seq[String]() , set_rm_users) &
      "#addusers" #> SHtml.ajaxButton(">>", () => add_users() ) &
      "#rm_users" #> SHtml.ajaxButton("<<", () => rm_users() ) &
      "#errorthreshold" #> SHtml.number(configurationVar.is.ErrorThreshold.value, (th:Int) => configurationVar.is.ErrorThreshold(th), 0, 1000)&
      "#configurationprobes" #> configurationProbes &
      "#configurationchannels" #> SHtml.multiSelect(List(), Seq[String]() , set_add_channels ) &
      // This registers a function for Lift to call when the user clicks submit
      "#submit" #> SHtml.onSubmitUnit(() => {add_channels();processSubmitCreate})
  }

  /**
    *
    * @param admin sets admin as user
    * @return new users
    */
  def set_admin ( admin : String ) : JsCmd = {
    println("adimin : " +   admin)
    set_user(admin)
    configurationVar.is.AdminID_FK(admin)
    val new_admin = User.findAll("_id",new ObjectId(admin)).flatMap( u => f(u)).map( i =>  i.id -> i.name )
    logger.info("SET GROUP ADMIN : "+new_admin)
    users_to_add = List(new_admin.headOption.getOrElse(("",""))._1)
    add_users()
  }

  /**
    *
    * @param user to be removed
    * @return error message
    */
  def set_rm_users( users : List[String]) : JsCmd = {
    logger.info("SET REMOVE USERS")
    logger.info("CURRENT ADMIN : "+configurationVar.is.AdminID_FK.value)
    users_to_rm = users diff configurationVar.is.AdminID_FK.value
    users.foreach(println)
    if (users.contains(configurationVar.is.AdminID_FK.value)){
      logger.error("ERROR CANNOT REMOVE CURRENT ADMIN : "+configurationVar.is.AdminID_FK.value)
      S.error("ERROR CANNOT REMOVE CURRENT ADMIN : "+configurationVar.is.AdminID_FK.value)
      val msg : String = "ERROR CANNOT REMOVE CURRENT ADMIN : "+configurationVar.is.AdminID_FK.value
      S.appendJs(Run(s"addError(\'$msg\')"))
      ReplaceOptions("groupmembers", existing_users , Box.legacyNullTest(null))
    }else{
      Noop
    }
  }

  /**
    *
    * @param user to add
    */
  def set_add_users( users : List[String]) = {
    logger.info("SET ADD USERS")
    users_to_add = users
    users.foreach(println)
  }

  /**
    *
    * @return available user
    */
  def rm_users() : JsCmd = {
    logger.info("REMOVING USERS")
    logger.info(" existing : ")
    existing_users.foreach(println)
    logger.info(" removing : ")
    users_to_rm.foreach(println)
    val users_removed = users_to_rm.map{ g_user_id => User.findAll("_id",new ObjectId(g_user_id))}.flatten.flatMap( u => f(u)).map( i =>  i.id -> i.name )
    existing_users = (existing_users diff users_removed).sortWith(_._2 > _._2)
    users_available = users_available ::: users_removed
    users_removed.foreach(println)
    println()
    List(ReplaceOptions("allusers", users_available , Box.legacyNullTest(null)), ReplaceOptions("groupmembers", existing_users , Box.legacyNullTest(null)))
  }

  /**
    *
    * @return added user
    */
  def add_users() : JsCmd = {
    logger.info("ADDING USERS")
    logger.info(" existing : ")
    existing_users.foreach(println)
    logger.info(" new      : ")
    users_to_add.foreach(println)
    val new_users = users_to_add.map{ g_user_id => User.findAll("_id",new ObjectId(g_user_id))}.flatten.flatMap( u => f(u)).map( i =>  i.id -> i.name )
    new_users.foreach(println)
    println()
    existing_users = (existing_users ::: new_users).distinct.sortWith(_._2 > _._2)
    users_available = users_available diff new_users
    List(ReplaceOptions("allusers", users_available , Box.legacyNullTest(null)), ReplaceOptions("groupmembers", existing_users , Box.legacyNullTest(null)))
  }

/**
* This function is called when the edit form is open
*/
  def edit = {
    val params = S.request.map(_.request.param("action")) openOr "Undefined"
    val action = params.asInstanceOf[List[String]].headOption.getOrElse("Undefined action")

    /**checks if the user requested to view the configuration or edit it
      check [[ code.snippets.ConfigurationOps.f ]]
    */
    val vieworedit = action match {
      case "Edit" =>
        pairToBasic("enabled","disabled")
      case _ =>
        pairToBasic("disabled","disabled")
    }

    if ( ! configurationVar.set_? )
      S.redirectTo("/system/configuration/listconfiguration")

    /** gets all the users and places them in a list
    */
    val allusers = User.findAll.flatMap( u => f(u))
    logger.info("list of users : ")
    val optionsuser = User.currentUser.openOr(User).roles.names match {
      case roles if roles.contains("admin") => allusers.map( i =>  i.id -> i.name ).sortWith(_._2 > _._2)
      case roles if roles.contains("regionaladmin") => {
        UserGroup.findAll("adminID_FK",currentUserId).flatMap(userGroup =>
            User.findAll("UserGroupID_FKs",userGroup.id.value).flatMap( u => f(u)).map( i =>  i.id -> i.name ).sortWith(_._2 > _._2)
        )
      }
      case _ => User.findAll("_id",new ObjectId(currentUserId)).flatMap( u => f(u)).map( i =>  i.id -> i.name ).sortWith(_._2 > _._2)
    }
    println("OPTIONS USER")
    println(User.currentUser.openOr(User).roles.names)
    println(optionsuser)
    val groupmembers = configurationVar.is.UserID_FKs.value.flatMap{id => {
      User.findAll("_id",new ObjectId(id)).flatMap( u => f(u)).map( i =>  i.id -> i.name )
    }}.sortWith(_._2 > _._2)
    val configurationProbes = User.currentUser.openOr(User).roles.names.contains("admin") match {
      case true => MSHtml.ajaxMultiSelect(findProbesWithAvailableChannels, Seq[String]() , select_probes(_), vieworedit )
      case _ => MSHtml.ajaxMultiSelect(findUserProbesWithAvailableChannels(currentUserId), Seq[String]() , select_probes(_), vieworedit )
    }
    logger.info("existing members : "+groupmembers)
    existing_users = groupmembers
    users_available = optionsuser diff groupmembers
    val configuration = configurationVar.is
    existing_channels = findChannels(List(configurationVar.is.id.value))
    "#hidden" #> SHtml.hidden(() => configurationVar(configuration) ) &
      "#configurationuser" #> SHtml.select(optionsuser, Box.legacyNullTest(configurationVar.is.AdminID_FK.value), set_admin(_), vieworedit ) &
      "#configurationname" #> SHtml.text(configurationVar.is.configurationName.value, name => configurationVar.is.configurationName(name), vieworedit ) &
      "#allusers" #> MSHtml.ajaxUntrustedMultiSelect(optionsuser diff groupmembers, Seq[String]() , set_add_users(_), vieworedit ) &
      "#groupmembers" #> MSHtml.ajaxUntrustedMultiSelect(groupmembers, Seq[String]() , set_rm_users(_), vieworedit) &
      "#addusers" #> SHtml.ajaxButton(">>", () => add_users() ) &
      "#rm_users" #> SHtml.ajaxButton("<<", () => rm_users() ) &
      "#errorthreshold" #> SHtml.number(configurationVar.is.ErrorThreshold.value, (th:Int) => configurationVar.is.ErrorThreshold(th), 0, 1000, vieworedit)&
      "#configurationprobes" #> configurationProbes &
      "#configurationchannels" #> MSHtml.ajaxUntrustedMultiSelect(List(), Seq[String]() , set_add_channels(_), vieworedit ) &
      "#configurationchannelsremove" #> MSHtml.ajaxUntrustedMultiSelect(existing_channels, Nil , set_rm_channels(_), vieworedit ) &
      "#addchannels" #> SHtml.ajaxButton(">>",()=>add_channels()) &
      "#rm_channels" #> SHtml.ajaxButton("<<",()=>rm_channels) &
      "#submit" #> SHtml.onSubmitUnit(processSubmitEdit)
  }

  /**
    *
    * @param action to be performed
    * @return Js Cmd if 'edit' is selected
    */
  def add_channels(action:String = "edit") : JsCmd = {
    logger.info("ADDING CHANNELS")
    logger.info(" existing : ")
    existing_channels.foreach(println)
    logger.info(" new      : ")
    val channels_by_probe = channels_to_add.map{ p_ch => (p_ch.split("_")(0),p_ch.split("_")(1))}.groupBy(_._1)
    val new_channels = channels_by_probe.flatMap{ probe =>
      val probeName = Probe.findAll("_id",probe._1).head.probeName
      probe._2.map{ channel =>
        (channel._1+"_"+channel._2,probeName+"_"+channel._2)
      }
    }.toList
    new_channels.foreach(println)
    println()
    existing_channels = (existing_channels ::: new_channels).sortWith(_._2 > _._2)
    channels_available = channels_available diff new_channels
    if(action == "edit"){
      List(ReplaceOptions("configurationchannels", channels_available , Box.legacyNullTest(null)), ReplaceOptions("configurationchannelsremove", existing_channels , Box.legacyNullTest(null)))
    } else {
      JsCmds.Noop
    }
  }

  /**
    * Handles channel removal
    * @return remaining channels in a list
    */
  def rm_channels : JsCmd = {
    logger.info("REMOVING CHANNELS")
    logger.info(" existing : ")
    existing_channels.foreach(println)
    val channels_by_probe = channels_to_rm.map{ p_ch => (p_ch.split("_")(0),p_ch.split("_")(1))}.groupBy(_._1)
    val channels_removed = channels_by_probe.flatMap{ probe =>
      val probeName = Probe.findAll("_id",probe._1).head.probeName
      probe._2.map{ channel =>
        (channel._1+"_"+channel._2,probeName+"_"+channel._2)
      }
    }.toList

    existing_channels = (existing_channels diff channels_removed).sortWith(_._2 > _._2)
    channels_available = channels_available ::: channels_removed
    println(channels_to_rm)
    println()
    List(ReplaceOptions("configurationchannels", channels_available , Box.legacyNullTest(null)), ReplaceOptions("configurationchannelsremove", existing_channels , Box.legacyNullTest(null)))
  }

  /**
    *
    * @param add_channels to config
    * @return added channels
    */
  def set_add_channels( add_channels : List[String] ) : JsCmd = {
    logger.info("SET ADD CHANNELS")
    channels_to_add = add_channels
    add_channels.foreach(println)
  }

  /**
    *
    * @param rm_channels removes channels from config
    * @return removed channels
    */
  def set_rm_channels( rm_channels : List[String] ) : JsCmd = {
    logger.info("SET RM CHANNELS")
    channels_to_rm = rm_channels
    rm_channels.foreach(println)
  }

  /**
    *
    * @param probes selected
    * @return available channels in the probe
    */
  def select_probes( probes : List[String] ) : JsCmd =  {
    logger.info("Populate channel multiselect with channels from the selected probes")
    val channels = probes.flatMap{ p =>
      val probe = Probe.findAll("_id",p).head
      val channels = probe.Channels.get.zipWithIndex.map{ case (e , i) =>
        if (e.ConfigID_FK.toString.isEmpty){
          (probe.id.toString+"_"+i.toString,probe.probeName.toString+"_"+i.toString)
        }
      }
      channels
    }
    val ch_filtered =  channels.filter(_ !=(())).asInstanceOf[List[(String,String)]]
    ch_filtered.foreach(logger.info(_))
    channels_available = ch_filtered
    ReplaceOptions("configurationchannels", ch_filtered , Box.legacyNullTest(null))
  }

  /**
    *
    * @param assignedchannels to user config
    * @return selected Js Cmd
    */
  def select_channels( assignedchannels : List[String] ) : JsCmd =  {
    logger.info("assign selected channels to User Configurations via ConfigID_FK")
    val ch_map = assignedchannels.map{ p_ch => (p_ch.split("_")(0),p_ch.split("_")(1))}.groupBy(_._1)
    //    println(ch_map)
    ch_map.map{ probe =>
      val ch_list = probe._2.map{ ch => ch._2.toInt}
      logger.info("probe "+probe._1+" "+ch_list+" "+manOf(ch_list))
      Probe.findAll("_id",probe._1).map{ probeobj =>
        probeobj.Channels.get.zipWithIndex.map{ case (e , i) =>
          if (ch_list contains i) {
            e.ConfigID_FK(configurationVar.id.toString)
            logger.info(" channel" + i)
          }
          e
        }
        probeobj.save()
      }
    }
    println("")
  }

  /**
    *
    * @param deletedchannels in the probe
    * @param configID for the channels
    * @return
    */
  def delete_channels( deletedchannels : List[String],configID:String ) : JsCmd =  {
    logger.info("removing channels from "+configurationVar.id.toString)
    deletedchannels.foreach(println)
    val ch_map = deletedchannels.map{ p_ch => (p_ch.split("_")(0),p_ch.split("_")(1))}.groupBy(_._1)
    //    println(ch_map)
    ch_map.map{ probe =>
      val ch_list = probe._2.map{ ch => ch._2.toInt}
      logger.info("probe "+probe._1+" "+ch_list+" "+manOf(ch_list))
      Probe.findAll("_id",probe._1).map{ probeobj =>
        probeobj.Channels.get.zipWithIndex.map{ case (e , i) =>
          if (ch_list contains i) {
            if(e.ConfigID_FK.toString == configID){
              e.SourceID_FK(Empty) // disassociate sources
              e.ConfigID_FK(Empty)
              logger.info(" channel" + i+" "+e.ConfigID_FK)
            }
          }
          e
        }
        probeobj.save()
      }
    }
    println("")
  }


  /**called from /system/configuration/listconfiguration.html
  */
  def list = {
    logger.info("list Configurations")
    // for each Configuration object in the database render the name along with links to view, edit, and delete
    "#heading_user" #> SHtml.span(<h5 class="alt" id="heading_user">Configurations associated with {User.findAll("_id", new ObjectId(currentUserId)).headOption.getOrElse(User.createRecord).username.value}</h5>, Noop) &
    "#configurationlist *" #> Configuration.findAll("AdminID_FK",currentUserId).map(c => {
      ".configurationName *" #> Text(c.configurationName.value) &
      ".configOwner *" #> Text(User.findAll("_id", new ObjectId(c.AdminID_FK.value)).headOption.getOrElse(User.createRecord).username.value) &
      ".parentGroup *" #> Text(ConfigGroup.findAll("_id",c.ConfigGroupID_FK.value).headOption.getOrElse(ConfigGroup.createRecord).groupName.value) &
      ".actions *" #> {SHtml.link("/system/configuration/editconfiguration?action=View", () => configurationVar(c), Text("View")) ++ Text(" ") ++
        SHtml.link("/system/configuration/editconfiguration?action=Edit", () => configurationVar(c), Text("Edit")) ++ Text(" ") ++
        SHtml.link("/system/configuration/deleteconfiguration", () => configurationVar(c), Text("Delete"))}
    } ) &
    "#configurationlistall *" #> Configuration.findAll.map(c => {
      ".configurationName *" #> Text(c.configurationName.value) &
        ".parentGroup *" #> Text(ConfigGroup.findAll("_id",c.ConfigGroupID_FK.value).headOption.getOrElse(ConfigGroup.createRecord).groupName.value) &
        ".configOwner *" #> Text(User.findAll("_id", new ObjectId(c.AdminID_FK.value)).headOption.getOrElse(User.createRecord).username.value) &
        ".actions *" #> {SHtml.link("/system/configuration/editconfiguration?action=View", () => configurationVar(c), Text("View")) ++ Text(" ") ++
          SHtml.link("/system/configuration/editconfiguration?action=Edit", () => configurationVar(c), Text("Edit")) ++ Text(" ") ++
          SHtml.link("/system/configuration/deleteconfiguration", () => configurationVar(c), Text("Delete"))}
    } )
  }
/** takes a conf and deletes all channels associated with it
*/
  def delete_configuration_channels(e : Configuration) : Any =  {
    println(" deleting configuration : "+e)
    PowerConsumerGroup.findAll("ConfigID_FK",e.id.toString).map{ consumergroup => deleteGroupLinesConsumers(consumergroup) }
    Generator.findAll("ConfigID_FK",e.id.toString).map{ source => source.delete_! }
    val associated_channels = findChannels(List(e.id.toString))
    associated_channels.foreach(println)
    delete_channels(associated_channels.map{ ch => ch._1},e.id.toString())
    e.delete_!
  }

  /** called from /system/configuration/deleteconfiguration.html*/
  def delete = {
    if ( ! configurationVar.set_? )
      S.redirectTo("/system/configuration/listconfiguration")
    val users = List()
    val e = configurationVar.is
    "#configurationname" #> configurationVar.is.configurationName &
      "#yes" #> SHtml.link("/system/configuration/listconfiguration", () => { delete_configuration_channels(e)}, Text("Yes")) &
      "#no" #> SHtml.link("/system/configuration/listconfiguration", () =>{ }, Text("No"))
  }


  /**
    * Handles addition of config member
    * @return Css selection
    */
  def addConfMember() = {
    object conf extends RequestVar("")
    val configurations = Configuration.findAll("AdminID_FK",currentUserId).map{conf => (conf.id.value,conf.configurationName.value)}
    def confProcess() = {
      S.param("email") match {
        case  Full(email) => if(email.nonEmpty){
          User.findByEmail(email) match {
            case Full(user) => {
              println(user.id.value)
              println(conf)
              Configuration.find("_id",conf.toString) match {
                case Full(config) => {
                  config.UserID_FKs((user.id.value.toString :: config.UserID_FKs.value).distinct).save()
                }
                case _ => {}
              }
            }
            case _ => {}
          }
          val e = new UserOps
          e.sendProfileMail(email,conf)
        }
        case  _ => {}
      }
    }

    "#conf" #> SHtml.select(configurations,Box.legacyNullTest(configurations.headOption.getOrElse("","")._2),(c : String)=>{conf.set(c)})&
    "#submit" #> SHtml.onSubmitUnit(confProcess) &
    ".config *" #> Configuration.findAll("AdminID_FK",currentUserId).map{ config =>
      ".configName" #> Text(config.configurationName.value) &
      "#userList *" #> config.UserID_FKs.value.filter(_ != config.AdminID_FK.value).map{u =>
        ".userName *" #> Text(User.findByStringId(u).openOr(User).username.value) &
        ".email *" #> Text(User.findByStringId(u).openOr(User).email.value) &
        ".actions *" #> SHtml.link("/system/configuration/delconfigmember", () => {configurationVar(config);userVar(User.findByStringId(u).openOr(User))}, Text("remove"))
      }
    }
  }

  /**
    * Handles deletion of config member
    * @return Css selection
    */

  def delConfigMember() ={
    if ( ! configurationVar.set_? )
      S.redirectTo("/system/configuration/configmember")
    val e = configurationVar.is
    val user = userVar.is
    val userIds = e.UserID_FKs.value.filter(_ != user.id.value.toString)
    println(user.id.value.toString)
    println(userIds)
    "#configurationname" #> configurationVar.is.configurationName &
    "#user" #> Text(userVar.is.username + " ("+ userVar.is.email + ") ") &
      "#yes" #> SHtml.link("/system/configuration/configmember", () => {e.UserID_FKs(userIds).save()}, Text("Yes")) &
      "#no" #> SHtml.link("/system/configuration/configmember", () =>{ }, Text("No"))

  }
}