package code.snippet

import code.model._

import scala.xml.{NodeSeq, Text}
import net.liftweb.util.Helpers._
import net.liftweb.util.JsonCmd
import net.liftweb.http.SHtml.jsonForm
import net.liftweb.http.JsonHandler
import net.liftweb.http.js.JsCmd
import net.liftweb.http.js.JsCmds.{Script, SetHtml}

import scala.xml._
import net.liftweb._
import http._
import S._
import SHtml._
import common._
import util._
import Helpers._
import js._
import JsCmds._
import code.model.Probe._
import com.sun.corba.se.spi.ior.ObjectId
import net.liftweb._
import net.liftweb.http._
import util._
import common._
import Helpers._
import TimeHelpers._
import net.liftweb.common.Logger

import scala.xml._
import net.liftweb.record.field._
import net.liftweb.record._
import net.liftweb.mongodb.record._
import net.liftweb.mongodb.BsonDSL._
import org.bson.types._

import scala.collection.mutable.ListBuffer
import scala.xml.{NodeSeq, Text}
import net.liftweb.util.JsonCmd
import net.liftweb.http.SHtml.jsonForm
import net.liftweb.http.JsonHandler
import net.liftweb.http.js.JsCmd
import net.liftweb.http.js.JsCmds.{Script, SetHtml}

import scala.util.control.Breaks._
import JE._
import code.lib.MSHtml
import net.liftweb.http.SHtml.ElemAttr._

/**
  *This class acts as the Controller for the ''Grid Source''
  */
class GridOps extends  Loggable {
  var probeID : String = ""
  var configID : String = ""
  var one, two,three  = "UNASSIGNED"
  val currentUserId = S.get("selectedUserID").toOption.getOrElse(User.currentUserId.headOption.getOrElse(""))

  /**
    * Handles grid variable
    */
  object GridVar extends RequestVar[Grid](Grid.createRecord)

  /**
    * Handles utility mapping
    */
  object utilityMapVar extends RequestVar[utilityMap](utilityMap.createRecord)

  /**
    * @param t type set
    * @tparam T type tag formatter
    * @return formatted  number of phases
    */
  def manOf[T: Manifest](t: T): Manifest[T] = manifest[T]
  var numberOfPhases:Int=1

  /**
    * @param in inverter
    * @return link to 'listsource'
    */
  def processSubmit( in : String ) : Any = {
    println("in args "+in)
    /** confirm uniqueness of source name to configuration */
    if (!powerSourceFunctions.validateSourceName(configID, GridVar.is.sourceName._1.toString)) {
      /** set form variables to GridVar */
      GridVar.is.ProbeID_FK("VALIDATIONFILLER")
      GridVar.is.ConfigID_FK(configID)
      GridVar.is.NumberOfPhases(numberOfPhases)
      println("Validating 1")
      println("ConfigID_FK "+GridVar.is.ConfigID_FK)
      GridVar.is.validate match {
        case Nil => {
          println("Generator ID3 : " + GridVar.is.id.toString)
          /** get phase to channel assignment from <select> */
          /** write SourceID_FK of newly created Source and channel assignments to Channels of selected probe */
          println("SELECT1 : " + one)
          println("SELECT2 : " + two)
          println("SELECT3 : " + three)
          configureChannels(GridVar.is, List(one, two, three)) /** set the channels accordingly */
          GridVar.is.save() /** save the power source */
          S.notice("Power Source Saved")
          /** S.seeOther throws an exception to interrupt the flow of execution and redirect */
          /** the user to the specified URL */
          S.seeOther("/system/source/listsource")
        }
        case errors => {
          println("Validation Failed : " + GridVar.is.id.toString)
          println("ConfigID_FK "+GridVar.is.ConfigID_FK)
          S.error(errors)
        }
      }
    }
    /** exiting the function here causes the form to reload */
  }

  case class Item(id: String, name: String)
  def g(v:Probe) = List(Item(v.id.toString, v.probeName.toString))
  /** find all configurations that belong to the current user */
  val currentConfs = Configuration.findAll("UserID_FKs",currentUserId)
  val optionsconf = currentConfs.map{ conf =>
    if(findChannelsWithNoSources(List(conf.id.toString)).length >= numberOfPhases) {
      /** only list those configurations with sufficient channels */
      logger.info(conf.configurationName)
      (conf.id.toString, conf.configurationName.toString)
    }
  }.filter(_ !=(())).asInstanceOf[Seq[(String,String)]]

  /**
    *
    * @param probes selected
    * @return html containing source channels
    */
  def set_sourceProbes ( probes : List[String] ) : JsCmd = {
    logger.info("Set Probes to ")
    probes.foreach(println)
    GridVar.is.ProbeID_FK(probes.toString)
    val channels = probes.map{ p =>
      logger.info(" sweeping channels for probe : "+p)
      logger.info(" ConfigID_FK                 : "+configID)
      findChannelsForProbe(p,configID)
    }.flatten
    if(channels.length >= numberOfPhases * 2){
      return MSHtml.replaceAllChannels(channels,"sourcechannel",numberOfPhases)
    }else{
      S.appendJs(Run("insufficientChannelsProbeSelectErrorDialog(%s,%s);".format(channels.length,numberOfPhases)))
      return Noop
    }
  }

  /**
    *
    * @param config Id of user
    * @return list source channel
    */
  def set_sourceConfig ( config : String ) : List[JsCmd] = {
    logger.info("Set Config to " + config )
    configID = config
    val channels = findChannelsWithNoSources(List(config))
    channels.foreach(println)
    val probes_associated = channels.map { c => c._1 }.map { p_ch => (p_ch.split("_")(0), p_ch.split("_")(1)) }.groupBy(_._1).map { p => p._1 }
    val optionsprobe = probes_associated.map { probe_id => (probe_id, Probe.findAll("_id", probe_id).head.probeName.toString) }.toList // map the list of all probes for the select probe
    return List(MSHtml.ReplaceOptionsMultiSelect("sourceprobes", optionsprobe, probes_associated.toSeq) :: MSHtml.replaceAllChannels(channels, "sourcechannel", numberOfPhases))
  }

  def render : CssSel = {
    /** list of name and id tupules of probes with sufficient channels */
    GridVar.is.NumberOfPhases(numberOfPhases)
    println()
    logger.info("Loaded Create "+numberOfPhases+" phase GRID")
    logger.info("Configurations associated with "+currentUserId+" : ")
    val selectedConf = Configuration.findAll("_id",optionsconf.headOption.getOrElse(("",""))._1)
    configID = optionsconf.headOption.getOrElse(("",""))._1
    val channels = findChannelsWithNoSources(selectedConf.map{ conf => conf.id.toString})
    val probes_associated = channels.map{ c => c._1 }.map{ p_ch => (p_ch.split("_")(0),p_ch.split("_")(1))}.groupBy(_._1).map{ p => p._1}
    val optionsprobe = probes_associated.map{ probe_id => (probe_id,Probe.findAll("_id",probe_id).head.probeName.toString) }.toSeq    // map the list of all probes for the select probe
    def channel_cssSel():CssSel = {
      if (numberOfPhases == 3) {
        return {
          "id=sourcechannel_1" #> SHtml.select(channels, Box.legacyNullTest(channels(0)._1), one = _) &
            "id=sourcechannel_2" #> SHtml.select(channels, Box.legacyNullTest(channels(1)._1), two = _) &
            "id=sourcechannel_3" #> SHtml.select(channels, Box.legacyNullTest(channels(2)._1), three = _)
        }
      }
      else{
        return {
          "id=sourcechannel_1" #> SHtml.select(channels, Box.legacyNullTest(channels(0)._1), one = _)
        }
      }
    }
    if (channels.length >= numberOfPhases) {
      return {
          channel_cssSel() &
          "id=sourcetype" #> SHtml.onSubmit(GridVar.is.Type(_)) &
          "#numberofphases" #> SHtml.onSubmit(phases => numberOfPhases = phases.toInt) &
          "#rating" #> SHtml.number(GridVar.is.Rating.value, rating => GridVar.is.Rating(rating), 5,99999999) &
          "#night" #> SHtml.number(utilityMapVar.is.night.value, load => GridVar.is.UtilityMap.get.night(load), 5, 2000 ) &
          "#morning" #> SHtml.number(utilityMapVar.is.morning.value, load => GridVar.is.UtilityMap.get.morning(load), 5, 2000 ) &
          "#afternoon" #> SHtml.number(utilityMapVar.is.afternoon.value, load =>GridVar.is.UtilityMap.get.afternoon(load), 5, 2000 ) &
          "#evening" #> SHtml.number(utilityMapVar.is.evening.value, load => GridVar.is.UtilityMap.get.evening(load), 5, 2000 ) &
          "#sourcename" #> SHtml.text(GridVar.is.sourceName.value, name => GridVar.is.sourceName(name), pairToBasic("autofocus","autofocus")) &
          "#signalThreshold" #> SHtml.number(GridVar.is.SignalThreshold.value, (th :Int) => GridVar.is.SignalThreshold(th),0,1000) &
          "#sourceprobes" #> MSHtml.ajaxUntrustedMultiSelect(optionsprobe, probes_associated.toSeq, set_sourceProbes) &
          "#sourceconfig" #> SHtml.ajaxSelect(optionsconf, Box.legacyNullTest(optionsconf.headOption.getOrElse(("",""))._1), set_sourceConfig) &
          "type=submit" #> SHtml.onSubmit(processSubmit)
      }

    } else {
      S.appendJs(Run("insufficientChannelsErrorDialog();"))
      return {
        "#submit" #> SHtml.onSubmit(processSubmit)
      }
    }
  }

  def view={
    val params = S.request.map(_.request.param("action")) openOr ("Undefined")
    val action = params.asInstanceOf[List[String]].headOption.getOrElse("Undefined action")
    val vieworedit = action match {
      case "Edit" =>
        pairToBasic("enabled","disabled")
      case _ =>
        pairToBasic("disabled","disabled")
    }
    /** list of name and id tupules of probes with sufficient channels */
    val s_id = S.params("id").headOr("")  /** extract sourceid */
    val source = Grid.find("_id",s_id ).getOrElse[Grid](Grid) /** get the source with the id */
    numberOfPhases = source.NumberOfPhases.value
    logger.info(action+" "+numberOfPhases+" phase Generator : "+source.sourceName)
    object gridVar extends RequestVar[Grid](source)
    val (channel_available, channel_assigned) = findChannelsForSource(List(gridVar.is.id.toString),List(gridVar.is.ConfigID_FK.toString),false)
    val probes_associated = channel_available.map{ c => c._1 }.map{ p_ch => (p_ch.split("_")(0),p_ch.split("_")(1))}.groupBy(_._1).map{ p => p._1}
    val optionsprobe = probes_associated.map{ probe_id => (probe_id,Probe.findAll("_id",probe_id).head.probeName.toString) }.toSeq    // map the list of all probes for the select probe
    val current_config = Configuration.findAll("_id", gridVar.is.ConfigID_FK.toString).head
    val optionsconf = List((current_config.id.value, current_config.configurationName.value))
    configID = optionsconf.headOption.getOrElse(("",""))._1

    /** when submitted */
    def saveEdit(x:String)={
      /** reset signal assignment for this source */
      findChannelsForSource(List(gridVar.is.id.toString),List(gridVar.is.ConfigID_FK.toString),true)
      /** update operation window */
      gridVar.saveTheRecord() /** save the source */
      println("passed channels : "+(one, two, three))
      configureChannels(gridVar.is, List(one, two, three)) /** set the channels accordingly and save probe */
      S.notice("Power Source Saved")
      /**  Redirect back to probe if change reassign redirected here */
      val delProbe = S.get("delprobeID").toOption.getOrElse("")
      if(delProbe.nonEmpty)
        S.seeOther("/system/probe/deleteprobe?delP=%s".format(delProbe.toString))
      S.seeOther("/system/source/listsource")
    }
    def custonCSSel():CssSel= {
      if (numberOfPhases > 1) {
        return {
          "id=sourcechannel_1" #> SHtml.select(channel_available, Box.legacyNullTest(channel_assigned(0)(0)._1), ch => one = ch ,  vieworedit) &
            "id=sourcechannel_2" #> SHtml.select(channel_available, Box.legacyNullTest(channel_assigned(1)(0)._1), ch => two = ch ,  vieworedit) &
            "id=sourcechannel_3" #> SHtml.select(channel_available, Box.legacyNullTest(channel_assigned(2)(0)._1), ch => three = ch ,  vieworedit)
        }
      } else {
        S.appendJs(Run("""$("#chnnls").trigger('removeChs');"""))
        return {
          "id=sourcechannel_1" #> SHtml.select(channel_available, Box.legacyNullTest(channel_assigned(0)(0)._1), ch => one = ch ,  vieworedit)
        }
      }
    }
    "#heading_action" #> SHtml.span(<h5 class="alt" id="heading_action">{action} {numberOfPhases} phase Grid power source : {source.sourceName}</h5>, (Noop)  ) &
      "#hidden" #> SHtml.hidden(() => gridVar(gridVar.is) ) &
      custonCSSel &
      "id=sourcetype" #> SHtml.onSubmit(gridVar.is.Type(_)) &
      "#rating" #> SHtml.number(gridVar.is.Rating.value, (rating : Int)=> GridVar.is.Rating(rating), 5,99999999, vieworedit) &
      "#night" #> SHtml.number(gridVar.is.UtilityMap._1.night.value, (load : Int)=> gridVar.is.UtilityMap.get.night(load), 5, 2000, vieworedit ) &
      "#morning" #> SHtml.number(gridVar.is.UtilityMap._1.morning.value, (load : Int) => gridVar.is.UtilityMap.get.morning(load), 5, 2000, vieworedit ) &
      "#afternoon" #> SHtml.number(gridVar.is.UtilityMap._1.afternoon.value, (load: Int) =>gridVar.is.UtilityMap.get.afternoon(load), 5, 2000, vieworedit ) &
      "#evening" #> SHtml.number(gridVar.is.UtilityMap._1.evening.value, (load: Int) => gridVar.is.UtilityMap.get.evening(load), 5, 2000, vieworedit ) &
      "#sourcename" #> SHtml.text(gridVar.is.sourceName.value, name => gridVar.is.sourceName(name), vieworedit) &
      "#signalThreshold" #> SHtml.number(gridVar.is.SignalThreshold.value, (th :Int) => gridVar.is.SignalThreshold(th),0,1000) &
      "#sourceprobes" #> MSHtml.ajaxUntrustedMultiSelect(optionsprobe, probes_associated.toSeq, set_sourceProbes(_), vieworedit ) &
      "#sourceconfig" #> SHtml.ajaxSelect(optionsconf, Box.legacyNullTest(optionsconf.headOption.getOrElse(("",""))._1), set_sourceConfig(_),  pairToBasic("disabled","disabled")) &
      "id=saveEdit" #> SHtml.onSubmit(x => saveEdit(x))
  }

  /**
    *
    * @param genobj generator object
    * @param channels channels in the probe
    */
  def configureChannels(genobj:Grid,channels : List[String]){

    val filtered_channels =channels.map{ ch => if(ch != "UNASSIGNED"){ch}}.filter(_ !=(())).asInstanceOf[List[String]]
    println(channels)
    val probes_associated = filtered_channels.map{ p_ch => (p_ch.split("_")(0),p_ch.split("_")(1))}.groupBy(_._1)
    logger.info("\nConfigure Channels")
    var counter_channel = 0
    probes_associated.map{  probes =>
      val channels_associated = probes._2.map{ l => l._2.toInt }
      Probe.findAll("_id",probes._1).map{ probe =>
        var counter_channel = 0
         /** loop through Channels of selected probe */
        probe.Channels.get.foreach(a => {
          if (a.SourceID_FK.toString.isEmpty  && numberOfPhases > 1 && channels_associated.contains(counter_channel)) {
            if (counter_channel == one.split("_")(1).toInt) {
              a.SourceID_FK(genobj.id.toString)
              a.sourceChannel(one.split("_")(1).toInt)
              a.SourceType(genobj.Type.toString)
              a.SourceName(genobj.sourceName.toString)
              a.phase(1)
              logger.info("JUST ASSIGNED : " + probe.probeName + "_"+counter_channel+" to "+GridVar.is.sourceName )
            } else if ((counter_channel == two.split("_")(1).toInt) && (GridVar.is.NumberOfPhases.get == 3) && channels_associated.contains(counter_channel) ) {
              a.SourceID_FK(genobj.id.toString)
              a.sourceChannel(two.split("_")(1).toInt)
              a.SourceType(genobj.Type.toString)
              a.SourceName(genobj.sourceName.toString)
              a.phase(2)
              logger.info("JUST ASSIGNED : " + probe.probeName + "_"+counter_channel+" to "+GridVar.is.sourceName )
            } else if ((counter_channel == three.split("_")(1).toInt) && (GridVar.is.NumberOfPhases.get == 3) && channels_associated.contains(counter_channel)){
              a.SourceID_FK(genobj.id.toString)
              a.sourceChannel(three.split("_")(1).toInt)
              a.SourceType(genobj.Type.toString)
              a.SourceName(genobj.sourceName.toString)
              a.phase(3)
              logger.info("JUST ASSIGNED : " + probe.probeName + "_"+counter_channel+" to "+GridVar.is.sourceName )
            }
          }else if (a.SourceID_FK.toString.isEmpty && channels_associated.contains(counter_channel) ){
            if (counter_channel == one.split("_")(1).toInt) {
              a.SourceID_FK(genobj.id.toString)
              a.sourceChannel(one.split("_")(1).toInt)
              a.SourceType(genobj.Type.toString)
              a.SourceName(genobj.sourceName.toString)
              a.phase(1)
              logger.info("JUST ASSIGNED : " + probe.probeName + "_"+counter_channel+" to "+GridVar.is.sourceName )
            }
          } else {
            println("PREVIOUSLY ASSIGNED")
          }
          counter_channel += 1
        })
        probe.save()
      }
    }
    filtered_channels.map{ p_ch =>
      println(" PROBE : "+p_ch.split("_")(0)+" CH : "+p_ch.split("_")(1))
    }
  }


  def numOfPhase:NodeSeq={
    val x = S.attr("phase") openOr "4"
    numberOfPhases = x.toInt
    <span><hr/></span>
  }

}