package code.snippet

import code.model._
import net.liftweb.common.{Box, Full}
import net.liftweb.http.SHtml._
import net.liftweb._
import http._
import js._
import JsCmds._
import net.liftweb._
import net.liftweb.http._
import util._
import Helpers._
import code.lib.MSHtml
import code.model.Probe._
import net.liftweb.mongodb.BsonDSL._
import net.liftweb.common.Loggable
import net.liftweb.http.SHtml.ElemAttr._

import scala.xml.{NodeSeq, Text}
import net.liftweb.http.js.JsCmd

/**
  * This class acts as the Controller for the ''Generator Source''
  *
  */
class GeneratorOps extends Loggable {
  var probeID : String = ""
  var one, two,three  = "UNASSIGNED"
  var fuelID = ""
  val currentUserId = S.get("selectedUserID").toOption.getOrElse(User.currentUserId.headOption.getOrElse(""))
  object GeneratorVar extends RequestVar[Generator](Generator.createRecord)
  object genmapVar extends RequestVar[generatorMap](generatorMap.createRecord)
  object oWindowVar extends RequestVar[operatingWindowElem](operatingWindowElem.createRecord)
  println("\nFirst UserID : "+currentUserId)


  def manOf[T: Manifest](t: T): Manifest[T] = manifest[T]
  var numberOfPhases:Int=1

  /**
   * This function is called each time the submit button is clicked.
   * The submit button is only available in Create and Update Operations
   */
  def processSubmit( in : String ) : Any = {

    /**confirm uniqueness of source name to configuration*/
    if (!powerSourceFunctions.validateSourceName(configID, GeneratorVar.is.sourceName._1.toString)) {

      /**set form variables to Generatorvar*/
      GeneratorVar.is.ProbeID_FK(probeID)
      GeneratorVar.is.ProbeID_FK("VALIDATIONFILLER")
      GeneratorVar.is.ConfigID_FK(configID)
      GeneratorVar.is.GeneratorMap(genmapVar)
      GeneratorVar.is.NumberOfPhases(numberOfPhases)
      GeneratorVar.is.FuelID_FK(fuelID)
      println("Generator ID : " + GeneratorVar.is.id.toString)

      /**operation time window*/
      GeneratorVar.is.operatingWindow(oWindowVar)
      if (fuelID != "") {
        val fuel = Fuel.find("_id", fuelID).headOption.get
        GeneratorVar.is.FuelType(fuel.FuelType.toString())
        GeneratorVar.is.FuelUnitPrice(fuel.FuelUnitPrice.value)
      }
      GeneratorVar.is.validate match {
        case Nil => {
          println("Generator ID3 : " + GeneratorVar.is.id.toString)
          println("SELECT1 : " + one)
          println("SELECT2 : " + two)
          println("SELECT3 : " + three)
          configureChannels(GeneratorVar.is, List(one, two, three)) //set the channels accordingly
          GeneratorVar.is.save() // save the power source
          S.notice("Power Source Saved")
          S.seeOther("/system/source/listsource")
        }
        case errors => S.error(errors)
      }
    }
    /** exiting the function here causes the form to reload */
  }

  private def channelSelect( channel : String ): JsCmd = {
      println("CH sel "+" "+channel)
  }

  case class Item(id: String, name: String)
  def g(v:Probe) = List(Item(v.id.toString, v.probeName.toString))

  /** get current user*/
  val currentUserID = currentUserId
  /** find all configurations that belong to the current user */
  val currentConfs = Configuration.findAll("UserID_FKs",currentUserID)
  /** find all probes in all configurations for the current user*/
  var probesLoggedIn:List[Probe] =List()
  currentConfs.map( x => {
    probesLoggedIn = probesLoggedIn ::: Probe.findAll(("ConfigID_FK" -> x.id.toString))
  })
  val optionsconf = currentConfs.map{ conf =>
    if(findChannelsWithNoSources(List(conf.id.toString)).length >= numberOfPhases) {
      /**  only list those configurations with sufficient channels */
      logger.info(conf.configurationName)
      (conf.id.toString, conf.configurationName.toString)
    }
  }.filter(_ !=(())).asInstanceOf[Seq[(String,String)]]

  /**get list of user fuels*/
  var fuelList:List[(String,String)] = Fuel.findAll("UserID_FK",currentUserId.toString).map{f => (f.id.toString(),f.FuelType.toString())}
  var configID = ""
  def set_sourceProbes ( probes : List[String] ) : JsCmd = {
    logger.info("Set Probes to "+probes)
    GeneratorVar.is.ProbeID_FK(probes.toString)
    val channels = probes.map{ p =>
      logger.info(" sweeping channels for probe : "+p)
      logger.info(" ConfigID_FK                 : "+configID)
      findChannelsForProbe(p,configID)
    }.flatten
    logger.info("Channels are "+channels)
    logger.info("number of channels available : "+channels.length+"")
    logger.info("number of channels required  : "+numberOfPhases+"")
    if(channels.length >= numberOfPhases * 2){
      return MSHtml.replaceAllChannels(channels,"sourcechannel",numberOfPhases)
    }else{
      S.appendJs(Run("insufficientChannelsProbeSelectErrorDialog(%s,%s);".format(channels.length,numberOfPhases)))
      return Noop
    }
  }

  def set_sourceConfig ( config : String ) : List[JsCmd] = {
    logger.info("Set Config to " + config )
    configID = config
    val channels = findChannelsWithNoSources(List(config))
    channels.foreach(println)
    val probes_associated = channels.map { c => c._1 }.map { p_ch => (p_ch.split("_")(0), p_ch.split("_")(1)) }.groupBy(_._1).map { p => p._1 }
    val optionsprobe = probes_associated.map { probe_id => (probe_id, Probe.findAll("_id", probe_id).head.probeName.toString) }.toList // map the list of all probes for the select probe
    return List(MSHtml.ReplaceOptionsMultiSelect("sourceprobes", optionsprobe, probes_associated.toSeq) :: MSHtml.replaceAllChannels(channels, "sourcechannel", numberOfPhases))
  }

  /** This is responsible for rendering the html pages for CRUD operations with the generator power source.
   *
   */
  def render  : CssSel = {

    /**list of name and id tupules of probes with sufficient channels*/
    GeneratorVar.is.NumberOfPhases(numberOfPhases)
    println()
    val fuel = ("notset","select fuel") ::fuelList
    logger.info("Loaded Create "+numberOfPhases+" phase Generator")
    logger.info("Configurations associated with "+currentUserId+" : ")
    val selectedConf = Configuration.findAll("_id",optionsconf.headOption.getOrElse(("",""))._1)
    configID = optionsconf.headOption.getOrElse(("",""))._1
    val channels = findChannelsWithNoSources(selectedConf.map{ conf => conf.id.toString})
    val probes_associated = channels.map{ c => c._1 }.map{ p_ch => (p_ch.split("_")(0),p_ch.split("_")(1))}.groupBy(_._1).map{ p => p._1}
    val optionsprobe = probes_associated.map{ probe_id => (probe_id,Probe.findAll("_id",probe_id).head.probeName.toString) }.toSeq    // map the list of all probes for the select probe
    def channel_cssSel():CssSel = {
      if (numberOfPhases == 3) {
        return {
          "id=sourcechannel_1" #> SHtml.select(channels, Box.legacyNullTest(channels(0)._1), one = _) &
          "id=sourcechannel_2" #> SHtml.select(channels, Box.legacyNullTest(channels(1)._1), two = _) &
          "id=sourcechannel_3" #> SHtml.select(channels, Box.legacyNullTest(channels(2)._1), three = _)
        }
      }
      else{
        return {
          "id=sourcechannel_1" #> SHtml.select(channels, Box.legacyNullTest(channels(0)._1), one = _)
        }
      }
    }
    if (channels.length >= numberOfPhases) {
      return {
          "#sourcename" #> SHtml.text(GeneratorVar.is.sourceName.value, name => GeneratorVar.is.sourceName(name), pairToBasic("autofocus","autofocus")) &
          "#signalThreshold" #> SHtml.number(GeneratorVar.is.SignalThreshold.value, (th :Int) => GeneratorVar.is.SignalThreshold(th),0,1000) &
          "id=sourcetype" #> SHtml.onSubmit(GeneratorVar.is.Type(_)) &
          "#sourceconfig" #> SHtml.ajaxSelect(optionsconf, Box.legacyNullTest(optionsconf.headOption.getOrElse(("",""))._1), set_sourceConfig) &
          "#sourceprobes" #> MSHtml.ajaxUntrustedMultiSelect(optionsprobe, probes_associated.toSeq, set_sourceProbes) &
          channel_cssSel() &
          "#numberofphases" #> SHtml.onSubmit(phases => numberOfPhases = phases.toInt) &
          "#rating" #> SHtml.number(GeneratorVar.is.Rating.value, rating => GeneratorVar.is.Rating(rating), 5,99999999) &
          "#fuelSelect" #> ajaxSelect(fuel, Box.legacyNullTest(fuel.head._1), { s => After(200, selectFuel(s))}) &
          "#fuelstore" #> SHtml.number(GeneratorVar.is.FuelStore.value, fuel => GeneratorVar.is.FuelStore(fuel), 10, 100000 ) &
          "#operatingWindowF" #> SHtml.text("00:00",oWindowVar.is.from(_)) &
          "#operatingWindowT" #> SHtml.text("24:00",oWindowVar.is.to(_)) &
          "#quarter" #> SHtml.number(genmapVar.is.QuarterLoad.value, load => genmapVar.is.QuarterLoad(load), 5, 2000 ) &
          "#half" #> SHtml.number(genmapVar.is.HalfLoad.value, load => genmapVar.is.HalfLoad(load), 5, 2000 ) &
          "#full" #> SHtml.number(genmapVar.is.FullLoad.value, load => genmapVar.is.FullLoad(load), 5, 2000 ) &
          "#threequarters" #> SHtml.number(genmapVar.is.ThreeQuarterLoad.value, load => genmapVar.is.ThreeQuarterLoad(load), 5, 2000 ) &
        "type=submit" #> SHtml.onSubmit(processSubmit)
      }
    } else {
      S.appendJs(Run("insufficientChannelsErrorDialog();"))
      return {
        "#submit" #> SHtml.onSubmit(processSubmit)
      }
    }
  }

  /**
  * This renders html pages depending on their use. That is if it's a page for viewing or Editing
   */
  def view={
    val params = S.request.map(_.request.param("action")) openOr ("Undefined")
    val action = params.asInstanceOf[List[String]].headOption.getOrElse("Undefined action")
    val vieworedit = action match {
      case "Edit" =>
        pairToBasic("enabled","disabled")
      case _ =>
        pairToBasic("disabled","disabled")
    }

    /**list of name and id tupules of probes with sufficient channels*/
    val s_id = S.params("id").headOr("")  //extract sourceid
    val source = Generator.find("_id",s_id ).getOrElse[Generator](Generator) //get the source with the id
    numberOfPhases = source.NumberOfPhases.value
    logger.info(action+" "+numberOfPhases+" phase Generator : "+source.sourceName)
    object generatorVar extends RequestVar[Generator](source)
    val (channel_available, channel_assigned) = findChannelsForSource(List(generatorVar.is.id.toString),List(generatorVar.is.ConfigID_FK.toString),false)
    //    channel_assigned.map{ a => println(" __ "+a)}    println(" CHA "+channel_assigned+" "+manOf(channel_assigned))
    val probes_associated = channel_available.map{ c => c._1 }.map{ p_ch => (p_ch.split("_")(0),p_ch.split("_")(1))}.groupBy(_._1).map{ p => p._1}
    val optionsprobe = probes_associated.map{ probe_id => (probe_id,Probe.findAll("_id",probe_id).head.probeName.toString) }.toSeq    // map the list of all probes for the select probe
    val current_config = Configuration.findAll("_id", generatorVar.is.ConfigID_FK.toString).head
    val optionsconf = List((current_config.id.value, current_config.configurationName.value))
    configID = optionsconf.headOption.getOrElse(("",""))._1
    val fuel = (source.FuelID_FK.toString(),source.FuelType.toString()) :: fuelList.filter(_._1 != source.FuelID_FK.toString())
    def setFuel(f_id:String){
      generatorVar.is.FuelID_FK(f_id)
      generatorVar.is.FuelType(fuel.filter(_._1 == f_id).map(f => f._2).headOr(""))
    }
    /**when submitted*/
    def saveEdit(x:String)={
      /**reset signal assignment for this source*/
      findChannelsForSource(List(generatorVar.is.id.toString),List(generatorVar.is.ConfigID_FK.toString),true)
      /**update operation window*/
      generatorVar.is.operatingWindow(oWindowVar)
      generatorVar.saveTheRecord() //save the source
      println("passed channels : "+(one, two, three))
      configureChannels(generatorVar.is, List(one, two, three)) //set the channels accordingly and save probe
      S.notice("Power Source Saved")
      /** Redirect back to probe if change reassign redirected here*/
      val delProbe = S.get("delprobeID").toOption.getOrElse("")
      if(delProbe.nonEmpty)
        S.seeOther("/system/probe/deleteprobe?delP=%s".format(delProbe.toString))
      S.seeOther("/system/source/listsource")
    }
    def custonCSSel():CssSel= {
      if (numberOfPhases > 1) {
        return {
          "id=sourcechannel_1" #> SHtml.select(channel_available, Box.legacyNullTest(channel_assigned(0)(0)._1), ch => one = ch ,  vieworedit) &
          "id=sourcechannel_2" #> SHtml.select(channel_available, Box.legacyNullTest(channel_assigned(1)(0)._1), ch => two = ch ,  vieworedit) &
          "id=sourcechannel_3" #> SHtml.select(channel_available, Box.legacyNullTest(channel_assigned(2)(0)._1), ch => three = ch ,  vieworedit)
        }
      } else {
        S.appendJs(Run("""$("#chnnls").trigger('removeChs');"""))
        return {
          "id=sourcechannel_1" #> SHtml.select(channel_available, Box.legacyNullTest(channel_assigned(0)(0)._1), ch => one = ch ,  vieworedit)
        }
      }
    }
    println(current_config.id.toString)
    println(generatorVar.is+"\n")
    println(GeneratorVar.is+"\n")
    println(optionsconf+"\n")
    "#heading_action" #> SHtml.span(<h5 class="alt" id="heading_action">{action} {numberOfPhases} phase Generator power source : {source.sourceName}</h5>, (Noop)  ) &
    "#hidden" #> SHtml.hidden(() => generatorVar(generatorVar.is) ) &
      custonCSSel &
    "id=sourcetype" #> SHtml.onSubmit(GeneratorVar.is.Type(_)) &
    "#sourceconfig" #> SHtml.ajaxSelect(optionsconf, Box.legacyNullTest(current_config.id.toString), set_sourceConfig(_) ,  pairToBasic("disabled","disabled") ) &
    "#sourceprobes" #> MSHtml.ajaxUntrustedMultiSelect(optionsprobe, probes_associated.toSeq, set_sourceProbes(_), vieworedit ) &
    "#operatingWindowF" #> SHtml.text(generatorVar.is.operatingWindow._1.from._1.toString,oWindowVar.is.from(_), vieworedit ) &
    "#operatingWindowT" #> SHtml.text(generatorVar.is.operatingWindow._1.to._1.toString,oWindowVar.is.to(_), vieworedit ) &
    "#signalThreshold" #> SHtml.number(generatorVar.is.SignalThreshold.value, (th :Int) => generatorVar.is.SignalThreshold(th),0,1000) &
    "#fuelstore" #> SHtml.number(generatorVar.is.FuelStore.value, (fuel:Int) => generatorVar.is.FuelStore(fuel), 10, 100000, vieworedit ) &
    "#rating" #> SHtml.number(generatorVar.is.Rating.value, (rating:Int) => generatorVar.is.Rating(rating), 5, 20000, vieworedit  ) &
    "#quarter" #> SHtml.number(generatorVar.is.GeneratorMap._1.QuarterLoad.value,(load:Int) => generatorVar.is.GeneratorMap._1.QuarterLoad(load), 5, 2000, vieworedit ) &
    "#half" #> SHtml.number(generatorVar.is.GeneratorMap._1.HalfLoad.value, (load:Int) => generatorVar.is.GeneratorMap._1.HalfLoad(load), 5, 2000, vieworedit ) &
    "#full" #> SHtml.number(generatorVar.is.GeneratorMap._1.FullLoad.value,  (load:Int) => generatorVar.is.GeneratorMap._1.FullLoad(load), 5, 2000, vieworedit ) &
    "#threequarters" #> SHtml.number(generatorVar.is.GeneratorMap._1.ThreeQuarterLoad.value, (load:Int) => generatorVar.is.GeneratorMap._1.ThreeQuarterLoad(load), 5, 2000, vieworedit ) &
    "#fuelSelect" #> ajaxSelect(fuel, Box.legacyNullTest(fuel.head._1), { s => After(200, setFuel(s))}, vieworedit ) &
    "#sourcename" #> SHtml.text(generatorVar.is.sourceName.toString(),generatorVar.is.sourceName(_), vieworedit ) &
    "id=saveEdit" #> SHtml.onSubmit(x => saveEdit(x))
  }
  /**
    *
    * @param fuel_ID selected for generator
    * @return Json command
    */
  def selectFuel(fuel_ID:String):JsCmd = {
    println("fuelId : "+fuel_ID)
    fuelID =  fuel_ID
  }

  /**
    *
    * @param genobj generator variable
    * @param channels in the probe
    */
  def configureChannels(genobj:Generator,channels : List[String]){
    val filtered_channels =channels.map{ ch => if(ch != "UNASSIGNED"){ch}}.filter(_ !=(())).asInstanceOf[List[String]]
    val probes_associated = filtered_channels.map{ p_ch => (p_ch.split("_")(0),p_ch.split("_")(1))}.groupBy(_._1)
    logger.info("\nConfigure Channels")
    var counter_channel = 0
    probes_associated.map{  probes =>
      val channels_associated = probes._2.map{ l => l._2.toInt }
      Probe.findAll("_id",probes._1).map{ probe =>
        var counter_channel = 0
        // loop through Channels of selected probe
        probe.Channels.get.foreach(a => {
          if (a.SourceID_FK.toString.isEmpty  && numberOfPhases > 1) {
            if (counter_channel == one.split("_")(1).toInt && channels_associated.contains(counter_channel)) {
              a.SourceID_FK(genobj.id.toString)
              a.sourceChannel(one.split("_")(1).toInt)
              a.SourceType(genobj.Type.toString)
              a.SourceName(genobj.sourceName.toString)
              a.phase(1)
              logger.info("JUST ASSIGNED : " + probe.probeName + "_"+counter_channel+" to "+GeneratorVar.is.sourceName)
            } else if ((counter_channel == two.split("_")(1).toInt) && (GeneratorVar.is.NumberOfPhases.get == 3) && channels_associated.contains(counter_channel)) {
              a.SourceID_FK(genobj.id.toString)
              a.sourceChannel(two.split("_")(1).toInt)
              a.SourceType(genobj.Type.toString)
              a.SourceName(genobj.sourceName.toString)
              a.phase(2)
              logger.info("JUST ASSIGNED : " + probe.probeName + "_"+counter_channel+" to "+GeneratorVar.is.sourceName )
            } else if ((counter_channel == three.split("_")(1).toInt) && (GeneratorVar.is.NumberOfPhases.get == 3) && channels_associated.contains(counter_channel)){
              a.SourceID_FK(genobj.id.toString)
              a.sourceChannel(three.split("_")(1).toInt)
              a.SourceType(genobj.Type.toString)
              a.SourceName(genobj.sourceName.toString)
              a.phase(3)
              logger.info("JUST ASSIGNED : " + probe.probeName + "_"+counter_channel+" to "+GeneratorVar.is.sourceName)
            }
          }else if (a.SourceID_FK.toString.isEmpty){
            if (counter_channel == one.split("_")(1).toInt && channels_associated.contains(counter_channel) ) {
              a.SourceID_FK(genobj.id.toString)
              a.sourceChannel(one.split("_")(1).toInt)
              a.SourceType(genobj.Type.toString)
              a.SourceName(genobj.sourceName.toString)
              a.phase(1)
              logger.info("JUST ASSIGNED : " + probe.probeName + "_"+counter_channel+" to "+GeneratorVar.is.sourceName)
            }
          } else {
            logger.info("PREVIOUSLY ASSIGNED : "+ probe.probeName + "_"+counter_channel+" to "+GeneratorVar.is.sourceName)
          }
          counter_channel += 1
        })
        probe.save()
      }
    }
    filtered_channels.map{ p_ch =>
      println(" PROBE : "+p_ch.split("_")(0)+" CH : "+p_ch.split("_")(1))
    }
  }

  /**
    *
    * @return number of phases
    */
  def numOfPhase:NodeSeq={
    val x = S.attr("phase") openOr "4"
    println("NOFPHASES : "+x)
    numberOfPhases = x.toInt
    <span><hr/></span>
  }

}