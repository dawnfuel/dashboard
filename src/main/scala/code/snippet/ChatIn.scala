package code.snippet

import code.comet._
import code.model.User

import net.liftweb._
import util._
import Helpers._
import http._
import js.JsCmds._
import js.JE._

/**
 * Handle input by sending the input line
 * to the ChatServer
 */
object ChatIn {
  // max count per session
  private object lineCnt extends SessionVar(0)

  def render =
    "*" #> SHtml.onSubmit(s => {
      if (s.length < 50 && lineCnt < 20) { // 20 lines per session
        ChatServer ! s // send the message
        lineCnt.set(lineCnt.is + 1)
      }
      SetValById("chat_in", "") // clear the input box
    })

}


import net.liftweb.http.{S, NamedCometActorSnippet}

/**
 * This is the snippet you add on your template (see the item.html file)
 * It will setup a comet actor for the class Itemcomet and set the name to the
 * value of the query parameter item, from an url like:
 * http://127.0.0.1:8080/item?item=chromebook
 */
