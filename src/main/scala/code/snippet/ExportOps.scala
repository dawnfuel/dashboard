package code.snippet

import code.rest.ESQueryAPI._
import org.elasticsearch.action.search.{SearchResponse, SearchType}
import org.elasticsearch.client.Client
import org.elasticsearch.client.transport.TransportClient
import org.elasticsearch.common.transport.InetSocketTransportAddress
import org.elasticsearch.index.query.QueryBuilders
import org.elasticsearch.search.aggregations.AggregationBuilders
import org.elasticsearch.search.aggregations.bucket.histogram.DateHistogramInterval
import org.elasticsearch.search.sort.SortOrder
import org.joda.time.DateTime
import org.elasticsearch.search.aggregations.bucket.histogram.Histogram
import org.elasticsearch.search.aggregations.metrics.sum.Sum
import java.io.{File, FileWriter}

import code.model.{Configuration, User}
import net.liftweb.common.Box
import net.liftweb.http.SHtml._
import net.liftweb.http.js.JsCmds.{After, ReplaceOptions}
import org.bson.types.ObjectId
import net.liftweb._
import http._
import js._
import JsCmds._
import net.liftweb._
import util._
import Helpers._
import code.rest.Realtime.Power_MSG

import scala.collection.JavaConversions._
import scala.xml.Elem
import net.liftweb.json
import org.elasticsearch.common.unit.TimeValue


/**
  * Handles Export of data
  */
object ExportOps {
  implicit val formats = net.liftweb.json.DefaultFormats
  val homePageOps = new homepageops
  val currentUserID = User.currentUserId.headOption.getOrElse("")
  val currentUserEmail = User.find("_id",new ObjectId(currentUserID)).headOption.getOrElse(User).email.toString()
  var configs:List[(String,String)] = Configuration.findAll("UserID_FKs", currentUserID).map(p => (p.id.toString -> p.configurationName.toString))
  var users:List[(String,String)] = List(("n/a","n/a"))
  var selectedUserID = ""
  var selectedconf = ""

  def render()  = {
    /**
      *
      * @return returns a lift response file or redirection
      */
    def processSubmit() : LiftResponse = {
      val stop = S.param("stop").openOr("")
      val start = S.param("start").openOr("")
      val samplePointType = S.param("samplePoint").openOr("")
      val interval = S.param("interval").openOr("hour")
      val phase  = S.param("phase").openOr("all")
      println(phase)
      println(interval)
      val config = selectedconf.isEmpty match {
        case true => configs.head._1
        case false => selectedconf
      }
      val exportData = export(config,start,stop,samplePointType,interval,phase)
      val fileName = config + ".dmd"

      if (exportData.isEmpty){
        return RedirectResponse("/data/export")
      } else {
        InMemoryResponse(
          exportData.mkString("\n").getBytes("UTF-8"),
          "Content-Type" -> "text/plain; charset=utf8" ::
            "Content-Disposition" -> s"attachment; filename=$fileName" :: Nil,
          cookies=Nil, code = 200)
      }
    }

    selectedUserID = currentUserID
    if(homePageOps.userHasRole("admin")){
      println("\n\n C-user : " +currentUserID+" , " +currentUserEmail)
      users = homePageOps.getAllUsers().filter(_ != (currentUserID,currentUserEmail))
      users = (currentUserID,currentUserEmail) :: users
    }

  /** check if the configuration list is empty **/
    if (configs.isEmpty) {
      configs = List(("n/a","n/a"))
    }

    selectedconf = configs.head._1
      "#users" #> ajaxUntrustedSelect(users, Box.legacyNullTest(users.head._1), { s => After(0, selectUser(s)) })&
      "#config" #> customSelect()&
      "#submit" #> SHtml.onSubmitUnit(() => throw new ResponseShortcutException(processSubmit))
  }

  def customSelect():Elem = {
    if(homePageOps.userHasRole("admin")){
      return ajaxUntrustedSelect(configs, Box.legacyNullTest(configs.head._1), { s => After(200, selectConf(s));})
    }else {
      return ajaxSelect(configs, Box.legacyNullTest(configs.head._1), { s => After(200, selectConf(s)) })
    }
  }

  def selectConf(config: String) : JsCmd = {
    selectedconf = config
  }

  def selectUser(userID: String): List[JsCmd] = {
    var selectedUserConfigs:List[(String,String)] = Configuration.findAll("UserID_FKs", userID).map(c => (c.id.toString -> c.configurationName.toString))
    if(selectedUserConfigs.isEmpty){
      selectedUserConfigs = List(("n/a","n/a"))
    }
    return List(
      ReplaceOptions("config", selectedUserConfigs, Box.legacyNullTest(selectedUserConfigs.head._1)),
      JE.JsRaw("""$('#config').trigger('change');""").cmd
    )
  }

  /**
    *
    * @param config user configuration ID
    * @param start_iso start time variable that receives user selected start time
    * @param stop_iso stop time variable that receives user selected stop time
    * @param selTime sample point type selected by user
    * @param interval selected time interval
    * @param phase selected phase
    * @return query response
    */
  def export(config : String,start_iso : String, stop_iso : String,selTime : String,interval : String, phase : String) : List[BigDecimal] = {
    val (eshost,esjavaport,eshttpport) = getESparams()
    val client: Client =TransportClient.builder().build().addTransportAddress(new InetSocketTransportAddress(java.net.InetAddress.getByName(eshost), esjavaport))
    println(start_iso)
    println(stop_iso)
    val start = ISO8601Format.parse(cleantz(start_iso))
    val stop = ISO8601Format.parse(cleantz(stop_iso))

    val timezone = "+"+stop_iso.takeRight(5)

    /** determine the query key **/
    val query_key = QueryBuilders.matchQuery("ConfigID_FK", config)

    /** construct the query **/
    val filter = QueryBuilders.rangeQuery("Time").gte(start).lte(stop)
    val query = QueryBuilders.filteredQuery(query_key,filter)

    /** construct aggregation **/
    val aggs = AggregationBuilders.dateHistogram("Energy")
      .field("Time")
      .interval(DateHistogramInterval.HOUR)
      .minDocCount(0)
      .extendedBounds(new DateTime(start),new DateTime(stop))
      .timeZone(timezone)
      .subAggregation(AggregationBuilders.sum("total_energy").field("Energy_since_last"))

    val energyList : List[BigDecimal] = phase match {
      case "all" => {
        val response: SearchResponse = client
          .prepareSearch("grit")
          .setTypes("docs")
          .setSearchType(SearchType.DFS_QUERY_THEN_FETCH)
          .setQuery(query)
          .addAggregation(aggs)
          .addSort("Time", SortOrder.DESC)
          .execute()
          .actionGet()
        client.close()
        var data = List[BigDecimal]()
        val agg : Histogram = response.getAggregations.get("Energy")

        /** loop through aggregation result and extract sum of energy in a bucket **/
        for (entry : Histogram.Bucket <- agg.getBuckets) {
          val key = entry.getKey().asInstanceOf[DateTime]
          val keyAsString = entry.getKeyAsString
          println(keyAsString)
          val docCount = entry.getDocCount
          val aggs : Sum = entry.getAggregations().get("total_energy")
          val sum  = aggs.getValue()
          data = data ::: List(BigDecimal(sum))
        }
        data
      }
      case _ => {
        var _response: SearchResponse = client
          .prepareSearch("grit")
          .setTypes("docs")
          .setQuery(query)
          .setSearchType(SearchType.SCAN)
          .addSort("Time", SortOrder.DESC)
          .setScroll(new TimeValue(60000))
          .setSize(100)
          .execute().actionGet()
        var voltage_1 = List[(String, BigDecimal)]()
        var voltage_2 = List[(String, BigDecimal)]()
        var voltage_3 = List[(String, BigDecimal)]()
        var current_1 = List[(String, BigDecimal)]()
        var current_2 = List[(String, BigDecimal)]()
        var current_3 = List[(String, BigDecimal)]()
        var pf_1 = List[(String, BigDecimal)]()
        var pf_2 = List[(String, BigDecimal)]()
        var pf_3 = List[(String, BigDecimal)]()
        do {
          for (hit <- _response.getHits.getHits) {
            val time = hit.getSource.get("Time").toString
            println(time)
            val voltage1 =  hit.getSource.get("Voltage1").toString
            val voltage2 = hit.getSource.get("Voltage2").toString
            val voltage3 =  hit.getSource.get("Voltage3").toString
            val current1 =  hit.getSource.get("Current1").toString
            val current2 =  hit.getSource.get("Current2").toString
            val current3 =  hit.getSource.get("Current3").toString
            val powerFactor1 =  hit.getSource.get("PowerFactor1").toString
            val powerFactor2 =  hit.getSource.get("PowerFactor2").toString
            val powerFactor3 =  hit.getSource.get("PowerFactor3").toString
            voltage_1 = (time -> BigDecimal(voltage1)) :: voltage_1
            voltage_2 = (time -> BigDecimal(voltage2)) :: voltage_2
            voltage_3 = (time -> BigDecimal(voltage3)) :: voltage_3
            current_1 = (time -> BigDecimal(current1)) :: current_1
            current_2 = (time -> BigDecimal(current2)) :: current_2
            current_3 = (time -> BigDecimal(current3)) :: current_3
            pf_1 = (time -> BigDecimal(powerFactor1)) :: pf_1
            pf_2 = (time -> BigDecimal(powerFactor2)) :: pf_2
            pf_3 = (time -> BigDecimal(powerFactor3)) :: pf_3
          }
          _response = client
            .prepareSearchScroll(_response.getScrollId).setScroll(new TimeValue(60000))
            .execute()
            .actionGet()
        } while (_response.getHits.getHits.length != 0);
        client.close()
        phase match {
          case "min" => {
            val energy = List(calcEnergy(voltage_1,current_1,pf_1),calcEnergy(voltage_2,current_2,pf_2),calcEnergy(voltage_3,current_3,pf_3))
            val avgEnergy = energy.map{ e => e.sum/e.length }
            val min = avgEnergy.zipWithIndex.minBy(_._1)._2
            energy(min)
          }
          case _ => {
            phase match {
              case "1" => {
                calcEnergy(voltage_1,current_1,pf_1)
              }
              case "2" => {
                calcEnergy(voltage_2,current_2,pf_2)
              }
              case "3" => {
                calcEnergy(voltage_3,current_3,pf_3)
              }
            }
          }
        }
      }
    }

    if (energyList.isEmpty){
       return energyList
    }
    var modifiedData = List[BigDecimal]()
    modifiedData = modifiedData ::: energyList

    /** Pad Energy list  while length < 8760 **/
    while(modifiedData.length < 8760) {
      energyList.iterator.takeWhile((_)=>modifiedData.length < 8760).foreach(energy =>
        {
          val r = scala.util.Random.nextInt(10)
          val num = ((BigDecimal(100) + (sign * r)) / 100) * energy
          modifiedData = modifiedData :::  List(num)
        }
      )
    }

  /** If Energy list is more than 8760 trim excess parts **/
    if (modifiedData.length > 8760) {
      val splitData = modifiedData.splitAt(8760)
      modifiedData = splitData._1
    }

  /** print energy list in a file **/
    val file = new File(config + ".txt")
    using(new FileWriter(file))(writer =>
      modifiedData.foreach{ d =>
        writer.write(d + "\n")
      })
    modifiedData
  }

  /** generate random negative or positive unity **/
  def sign() : BigDecimal = {
    val r = scala.util.Random.nextBoolean()
    r match {
      case true => BigDecimal(1)
      case false => BigDecimal(-1)
    }
  }

  /**
    *
    * @param voltage sum of  voltages
    * @param current sum of current
    * @param pf sum of power factor
    * @return returns a BigDecimal list of calculated energy
    */
  def calcEnergy(voltage: List[(String, BigDecimal)],current : List[(String, BigDecimal)],pf : List[(String, BigDecimal)]) = {
    val power = voltage.zip(current).zip(pf).map{
      case ((v,i),p) => (v._2 * i._2 * p._2,v._1)
    }
    val energyList = power.zipWithIndex.map{ case(p,i) =>
      if(i > 0) {
        val dt = math.abs((BigDecimal(ISO8601Format.parse(power(i-1)._2).getTime - ISO8601Format.parse(power(i)._2).getTime)/(3600 * 1000)).toDouble)
        val dp = math.abs((power(i-1)._1 + power(i)._1).toDouble)/2
        val energy  = dt * dp
        BigDecimal(energy)
      } else  {
        BigDecimal(0)
      }
    }
    energyList.tail
  }

  def using[A <: {def close() : Unit}, B](resource: A)(f: A => B): B =
    try f(resource) finally resource.close()

}
