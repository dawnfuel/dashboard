package code.snippet


import java.io.FileOutputStream

import net.liftweb.http._

import scala.xml.Text
import code.model._
import net.liftweb.util._
import Helpers._
import code.config.SmtpMailer
import com.lowagie.text.pdf.BaseFont
import net.liftweb.common.{Box, Full}
import net.liftweb.http.SHtml.ElemAttr._
import org.joda.time.DateTime
import org.joda.time.format.DateTimeFormat
import net.liftweb.mongodb.BsonDSL._
import net.liftweb.http.js.{JsCmd, JsCmds}
import net.liftweb.http.js.JsCmds.{Run, SetHtml}
import net.liftweb.json.JsonAST._
import net.liftweb.json.Extraction._
import net.liftweb.json.Printer._
import net.liftweb.util.Mailer.{From, PlusImageHolder, Subject, To, XHTMLPlusImages}
import net.liftweb.http.js.JsCmds._
import org.bson.types.ObjectId
import org.xhtmlrenderer.pdf.ITextRenderer

/**
  * CRUD for GRIT Invoices
  */
class GRIT_InvoiceOps {
  object GRIT_InvoiceVar extends RequestVar[GRIT_Invoice](GRIT_Invoice.createRecord)
  object UserVar extends RequestVar[User](User.createRecord)
  implicit val formats = net.liftweb.json.DefaultFormats
  var Qtys : List[Int] = List()
  var products : List[String] = List()
  var prices : List[BigDecimal] = List()
  var amount = Map[String,Double]()

  /**
    *
    * @return link to delete invoice
    */
  def list() = {
    val invoices = GRIT_Invoice.findAll
    "#invoiceList" #> invoices.map{invoice =>
      val fmt = DateTimeFormat forPattern "dd MMM yyyy"
      val formatter = java.text.NumberFormat.getInstance
      val amount = if(!invoice.Lines.value.isEmpty){
        invoice.Lines.value.map{ a =>
          a.UnitPrice.value * BigDecimal(a.Quantity.value)
        }.reduce(_+_)
      } else {
        0.0
      }

      val account = Account.find(invoice.AccountID_FK.value) match {
        case Full(account) => {account}
        case _ => Account
      }
      ".ref" #>  invoice.reference.value &
      ".user" #>  account.Name.value &
      ".amount" #> formatter.format(amount) &
        ".download *" #> SHtml.link("system/payments/listinvoices",
          () => {sendInvoice(invoice.id.value,true)}, scala.xml.XML.loadString("<i class=\"fa fa-download\" aria-hidden=\"true\"></i>")
        ) &
      ".send *" #> SHtml.ajaxButton("Send Mail",()=>{sendInvoice(invoice.id.value)},"class" -> "btn btn-primary")&
      ".date" #> fmt.print(new DateTime(invoice.DueDate.value.toString)) &
        ".actions *" #> {SHtml.link("/system/payments/editinvoice?action=View", () => GRIT_InvoiceVar(invoice), Text("view")) ++ Text(" ") ++
          SHtml.link("/system/payments/editinvoice?action=Edit", () => GRIT_InvoiceVar(invoice), Text("edit")) ++ Text(" ") ++
          SHtml.link("/system/payments/deleteinvoice", () => GRIT_InvoiceVar(invoice), Text("delete"))}
    }
  }

  /**
    *
    * @return edited invoice
    */
  def edit() = {
    val invoiceId = S.params("q").headOption.getOrElse("")
    val invoice_ = GRIT_Invoice.findAll("_id",invoiceId)
    println(invoice_)
    if ( ! GRIT_InvoiceVar.set_? && invoice_.isEmpty ){
      S.redirectTo("/system/payments/listinvoices")
    } else if (invoice_.nonEmpty){
      GRIT_InvoiceVar(invoice_.headOption.getOrElse(GRIT_Invoice))
    }

    val params = S.request.map(_.request.param("action")) openOr ("Undefined")
    val action = params.asInstanceOf[List[String]].headOption.getOrElse("Undefined action")
    val vieworedit = action match {
      case "Edit" =>
        pairToBasic("enabled","disabled")
      case _ =>
        pairToBasic("disabled","disabled")
    }
    val fmt = DateTimeFormat forPattern "MM/dd/yyyy hh:mm a"
    val invoice = GRIT_InvoiceVar.is

    /**
      *
      * @return link to list invoice
      */
    def processSubmit() = {
      if (action == "Edit") {
        var lines = List[InvoiceLines]()
        S.params("qty").foreach(addQty(_))
        S.params("price").foreach(addPrice(_))
        S.params("product").foreach(addProduct(_))
        println(S.param("date"))
        val date = S.param("date") match {
          case Full(d) => {fmt.parseDateTime(d)}
          case _ => {new DateTime()}
        }
        Qtys.zip(products).zip(prices).map{l =>
          println( l._1._1  + " " + l._1._2  +" " +l._2)
          val line = InvoiceLines.createRecord.Quantity(l._1._1).UnitPrice(l._2).ProductID_FK(l._1._2)
          lines = line :: lines
        }
        GRIT_InvoiceVar.is.Lines(lines)
        GRIT_InvoiceVar.is.DueDate(date)
        GRIT_InvoiceVar.is.Sent(false)
        GRIT_InvoiceVar.is.save()
      }
      S.redirectTo("/system/payments/listinvoices")
    }
    val accounts = Account.findAll.map(a => (a.id.value->a.Name.value))
    val Products = GRIT_Products.findAll.map(p =>{
      amount += (p.id.value -> (p.price.get.Amount.value.toDouble))
      (p.id.value,p.name.value)
    })
    S.appendGlobalJs(Run("var amount ="+compact(render(decompose(amount)))))
    "#hidden" #> SHtml.hidden(() => GRIT_InvoiceVar(invoice)) &
      "#hiddenDate" #> SHtml.hidden((_) => {},fmt.print(new DateTime(invoice.DueDate.value.toString))) &
      ".panel-title"  #> Text(action + " Invoice " + invoice.reference.value) &
      "#accounts" #> SHtml.select(accounts,Box.legacyNullTest(invoice.AccountID_FK.value),(acc: String)=>{GRIT_InvoiceVar.AccountID_FK(acc)},vieworedit)&
      "#status" #> SHtml.select(List(("unpaid","Unpaid"),("paid","Paid")),Box.legacyNullTest(GRIT_InvoiceVar.is.Status.value),(status)=>{GRIT_InvoiceVar.is.Status(status)},vieworedit) &
      "#lines *" #>  { GRIT_InvoiceVar.is.Lines.value.zipWithIndex.map { case(line,i) =>
        ".qty" #> SHtml.number(line.Quantity.value, (r:Int) => {addQty(r.toString)},0,math.pow(2,52).toInt,vieworedit)&
        ".product" #> SHtml.ajaxSelect(Products,Box.legacyNullTest(line.ProductID_FK.value),(p: String)=>{selectPrice(p,(i+"s").toString)},vieworedit)&
        ".price" #> SHtml.number(line.UnitPrice.value.toDouble, (r:Double) => {addPrice(r.toString)},0.0,math.pow(2,52),1,vieworedit,pairToBasic("id",(i+"s").toString))
      } } &
     "#submit" #> SHtml.onSubmitUnit(processSubmit)
  }

  /**
    *
    * @return created account
    */
  def create() = {
    val accounts = (Account.findAll.map(a => (a.id.value -> a.Name.value)))//.getOrElse(List(("","")))
    val products = GRIT_Products.findAll.map(p =>{
      amount += (p.id.value -> (p.price.get.Amount.value.toDouble))
      (p.id.value,p.name.value)
    })
    S.appendGlobalJs(Run("var amount ="+compact(render(decompose(amount)))))
    ".product" #> SHtml.ajaxSelect(products,Box.legacyNullTest(products.headOption.getOrElse(("",""))._2),(a: String)=>{selectPrice(a)} )&
    ".price" #> SHtml.number(amount.getOrElse(products.headOption.getOrElse(("", ""))._1,0.0D),(a:Double)=> {addPrice(a.toString)},0.0,math.pow(2,52),1) &
    "#accounts" #> SHtml.select(accounts,Box.legacyNullTest(accounts.headOption.getOrElse(("",""))._2),(acc: String)=>{GRIT_InvoiceVar.AccountID_FK(acc)})&
    "#submit" #> SHtml.onSubmitUnit(createSubmit)
  }

  /**
    *
    * @param productId selected
    * @param id user id
    * @return price
    */
  def selectPrice(productId : String,id:String="") :JsCmd = {
    val price = amount(productId)
    println(id)
    if (id.isEmpty) {
      JsCmds.SetValById("price", price.toDouble)
    }else {
      JsCmds.SetValById(id, price.toDouble)
    }
  }

  /**
    *
    * @return redirect to list invoice
    */
  def createSubmit() = {
    var lines = List[InvoiceLines]()
    S.params("qty").foreach(addQty(_))
    S.params("price").foreach(addPrice(_))
    S.params("product").foreach(addProduct(_))
    S.params("qty").foreach(println(_))
    S.params("price").foreach(println(_))
    S.params("product").foreach(println(_))
    Qtys.zip(products).zip(prices).map{l =>
      println( l._1._1  + " " + l._1._2  +" " +l._2)
      val line = InvoiceLines.createRecord.Quantity(l._1._1).UnitPrice(l._2).ProductID_FK(l._1._2)
      lines = line :: lines
    }
    val ref = PaymentSettings.findAll.head.lastReferenceNumber.value + 1
    val Id = PaymentSettings.findAll.head.id.value
    PaymentSettings.update(("_id" ->Id), ("$inc" -> ("lastReferenceNumber" -> 1)))
    GRIT_InvoiceVar.is.Lines(lines).reference("E"+ref).save()
    S.redirectTo("/system/payments/listinvoices")
  }

  /**
    *
    * @return invoice
    */
  def delete() = {
    val invoice  = GRIT_InvoiceVar.is

    /**
      *
      * @return invoice
      */
    def deleteInvoice() = {
      invoice.delete_!
    }

    "#ref" #> GRIT_InvoiceVar.is.reference.value &
      "#yes" #> SHtml.link("/system/payments/listinvoices", () => {
        deleteInvoice() }, Text("Delete")) &
      "#no" #> SHtml.link("/system/payments/listinvoices", () => { }, Text("Cancel"))
  }

  /**
    *
    * @return redirect to accept invoice html
    */
  def accept_()= {
    val invoiceId = S.params("q").headOr("")
    val email = S.params("email").headOr("")
    val invoice = GRIT_Invoice.findAll("_id",invoiceId).headOption.getOrElse(GRIT_Invoice)
    println(invoiceId)
    println(email)
    val user = User.findByEmail(email) match {
      case Full(u) => {
        User.logUserIn(u, true)
        u
      }
      case _ => User
    }
    S.redirectTo("/system/payments/acceptinvoice.html",()=>{UserVar(user);GRIT_InvoiceVar(invoice)})
  }

  /** Controller for View used in accepting invoices **/
  def accept() = {
    val invoice = GRIT_InvoiceVar.is
    val user = UserVar.is
    val fmt = DateTimeFormat forPattern "MM/dd/yyyy hh:mm a"
    def submit() = {
      val agree = S.param("agree").openOr("") match {
        case "agree" => true
        case _ => false
      }
      user.InstallAgreement(agree).save()
      if (agree) {
        S.appendGlobalJs(Run("$('.successModal').modal()"))
        //S.redirectTo("/bills")
      }
    }
    if (user.email.value.isEmpty || invoice.Lines.value.isEmpty){
      "#cover" #> <div><i class="fa fa-info-circle" aria-hidden="true"></i><span> Sorry, the link is invalid. Send us a mail through the <a href="/contact">contact page </a>to help you resolve this</span></div>
    } else {
      val Products = GRIT_Products.findAll.map(p =>{
        amount += (p.id.value -> (p.price.get.Amount.value.toDouble))
        (p.id.value,p.name.value)
      })
      val account = Account.findAll("_id",invoice.AccountID_FK.value).headOption.getOrElse(Account)
      "#account" #> SHtml.text(user.email.value,(_)=>{},pairToBasic("disabled","disabled"))&
        "#hidden" #> SHtml.hidden(() => GRIT_InvoiceVar(invoice)) &
        "#lines *" #>  { invoice.Lines.value.zipWithIndex.map { case(line,i) =>
          ".qty" #> SHtml.number(line.Quantity.value, (r:Int) => {addQty(r.toString)},0,math.pow(2,52).toInt,pairToBasic("disabled","disabled"))&
            ".product" #> SHtml.ajaxSelect(Products,Box.legacyNullTest(line.ProductID_FK.value),(p: String)=>{selectPrice(p,(i+"s").toString)},pairToBasic("disabled","disabled"))&
            ".price" #> SHtml.number(line.UnitPrice.value.toDouble, (r:Double) => {addPrice(r.toString)},0.0,math.pow(2,52),1,pairToBasic("disabled","disabled"),pairToBasic("id",(i+"s").toString))
        } } &
        "#submit" #> SHtml.onSubmitUnit(submit)
    }
  }

  /**
    *
    * @param q quantity added
    */
  def addQty (q : String) = {
    if (!q.isEmpty) Qtys = BigDecimal(q).toInt::Qtys
  }

  /**
    *
    * @param p price added
    */
  def addPrice(p : String) = {
    if (!p.isEmpty) prices = BigDecimal(p)::prices
  }

  /**
    *
    * @param p product added
    */
  def addProduct(p : String) = {
    if (!p.isEmpty) products = p::products
  }

  /**
    *
    * @param invoiceId
    * @param download flag used to indicate download or email transfer
    * @return generated invoice in PDF or send HTML email with invoice PDF attached
    */
  def sendInvoice(invoiceId : String, download : Boolean = false) : JsCmd ={
    GRIT_Invoice.find("_id",invoiceId) match  {
      case Full(invoice) => {
        val ref = invoice.reference.value
        val account : Account = Account.find("_id",invoice.AccountID_FK.value) match {
          case Full(acc) => {acc}
          case _  => {Account}
        }
        val user = User.findAll("_id",new ObjectId(account.UserID_FK.value)).headOption.getOrElse(User)
        val email = user.email.value
        println(email);
        val fmt = DateTimeFormat forPattern "dd MMM yyyy"
        val formatter = java.text.NumberFormat.getInstance
        val creationDate = fmt.print(new DateTime())
        val due = creationDate //fmt.print(new DateTime().plusDays(31))
        val NGN = "₦"
        val lines = invoice.Lines.value
        val _totalAmount : BigDecimal = lines.map {
          line => {
            line.Quantity.value * line.UnitPrice.value
          }
        }.sum
        val _tax = 0.05 * _totalAmount
        val tax = formatter.format(0.05 * _totalAmount)
        val totalAmount = formatter.format(_totalAmount + _tax)
        val _otherLines = lines.zipWithIndex.filter(_._2 != lines.length-1)
        val otherLines = _otherLines.map(o => o._1)
        val _lastLine = lines.last
        val lastLine =
          <tr class="item last">
            <td>
              {GRIT_Products.findAll("_id",_lastLine.ProductID_FK.value).headOption.getOrElse(GRIT_Products).name.value}
            </td>
            <td>
              {formatter.format(_lastLine.UnitPrice.value)}
            </td>
            <td>
              {_lastLine.Quantity.value}
            </td>
            <td>
              {formatter.format(_lastLine.Quantity.value * _lastLine.UnitPrice.value)}
            </td>
          </tr>
        val td_lines =
          <tbody>
            { otherLines.map(line =>
            <tr class="item">
              <td>
                {GRIT_Products.findAll("_id",line.ProductID_FK.value).headOption.getOrElse(GRIT_Products).name.value}
              </td>
              <td>
                {formatter.format(line.UnitPrice.value)}
              </td>
              <td>
                {line.Quantity.value}
              </td>
              <td>
                {formatter.format(line.Quantity.value * line.UnitPrice.value)}
              </td>
            </tr>
          ) ++ lastLine
            }
          </tbody>
        val html = "<?xml version=\"1.0\" encoding=\"utf-8\"?>\n\n    <!DOCTYPE html\n   PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\"\n   \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\" >\n\n    <html xmlns=\"http://www.w3.org/1999/xhtml\">\n\n    <head>\n        <meta http-equiv=\'Content-Type\' content=\'text/html; charset=UTF-8\'/>\n        <title>\n            Invoice_"+ref+"\n        </title>\n <link href=\"https://fonts.googleapis.com/css?family=Source+Sans+Pro\" rel=\"stylesheet\"/>       <style type=\"text/css\" xml:space=\"preserve\">\n            .invoice-box {\n                max-width: 800px;\n                margin: auto;\n                padding: 30px;\n                border: 1px solid #eee;\n                box-shadow: 0 0 10px rgba(0, 0, 0, .15);\n                font-size: 16px;\n                line-height: 24px;\n                font-family: 'Source Sans Pro',Arial,'Helvetica Neue', 'Helvetica', Helvetica, sans-serif;\n                color: #555;\n            }\n            \n            .invoice-box table {\n                width: 100%;\n                line-height: inherit;\n                text-align: left;\n            }\n            \n            .invoice-box table td {\n                padding: 5px;\n                vertical-align: top;\n            }\n            \n            .invoice-box table tr td:nth-child(4) {\n                text-align: right;\n            }\n            \n            .invoice-box table tr.top table td {\n                padding-bottom: 20px;\n            }\n            \n            .invoice-box table tr.top table td.title {\n                font-size: 45px;\n                line-height: 45px;\n                color: #333;\n            }\n            \n            .invoice-box table tr.information table td {\n                padding-bottom: 40px;\n            }\n            \n            .invoice-box table tr.heading td {\n                background: #eee;\n                border-bottom: 1px solid #ddd;\n                font-weight: bold;\n            }\n            \n            .invoice-box table tr.details td {\n                padding-bottom: 20px;\n            }\n            \n            .invoice-box table tr.item td {\n                border-bottom: 1px solid #eee;\n                padding: auto 0.9em;\n            }\n            \n            .invoice-box table tr.item.last td {\n                border-bottom: none;\n            }\n            \n            .invoice-box table tr.total td:nth-child(4) {\n                border-top: 2px solid #eee;\n                font-weight: bold;\n            }\n            \n            @media only screen and (max-width: 600px) {\n                .invoice-box table tr.top table td {\n                    width: 100%;\n                    display: block;\n                    text-align: center;\n                }\n                .invoice-box table tr.information table td {\n                    width: 100%;\n                    display: block;\n                    text-align: center;\n                }\n            }\n        </style>\n    </head>\n\n    <body>\n        <div class=\"invoice-box\">\n            <table cellpadding=\"0\" cellspacing=\"0\">\n                <tr class=\"top\">\n                    <td colspan=\"4\">\n                        <table>\n                            <tr>\n                                <td class=\"title\">\n                                    <img src=\"http://grit.systems/img/grit-small.png\" style=\"height:1.5em\" alt=\"\" />\n                                </td>\n                                <td></td>\n                                <td></td>\n                                <td>\n                                    Invoice #: "+{ref}+"\n                                    <br /> Created: "+{creationDate}+"\n                                    <br /> Due: "+{due}+"\n                                </td>\n                            </tr>\n                        </table>\n                    </td>\n                </tr>\n                <tr class=\"information\">\n                    <td colspan=\"4\">\n                        <table>\n                            <tr>\n                                <td>\n                                    GRIT Systems\n                                    <br /> 294 Herbert Macaulay Way\n                                    <br />Yaba, Lagos, 101211\n                                    <br/> Nigeria\n                                </td>\n                                <td></td>\n                                <td></td>\n                                <td>\n                                    "+{email}+"\n                                    <br />\n                                </td>\n                            </tr>\n                        </table>\n                    </td>\n                </tr>\n              \n                <tr class=\"heading\">\n                    <td>\n                        Item\n                    </td>\n                    <td>\n                        Unit Price (&#8358;)\n                    </td>\n                    <td>\n                        Qty\n                    </td>\n                    <td style=\"text-align:left;\">\n                        Total (&#8358;)\n                    </td>\n                </tr>\n                "+{td_lines}+"\n                <tr class=\"total\">\n                    <td></td>\n                    <td></td>\n                    <td></td>\n                    <td style=\"font-weight:normal;\">\n                        VAT : "+NGN+{tax}+"\n                    </td>\n                </tr>\n                <tr class=\"total\">\n                    <td></td>\n                    <td></td>\n                    <td></td>\n                    <td>\n                        Total:  &#x20A6;"+{totalAmount}+"\n                    </td>\n                </tr>\n            </table>\n           <p>\n          <h3 style= \"                background: #eee;\n                border-bottom: 1px solid #ddd;\n  margin-bottom:.2em;   font-weight: bold;\">Details</h3>\n          Payment Method : <i> Electronic Funds Transfer, Payment Terms 100% Upfront</i> <br/> \n          Bank : <i>Guaranty Trust </i><br/>\n          Bank Account name :<i> GRIT SYSTEMS ENGINEERING LTD </i><br/>\n          Account number :<i> 0208721579 </i><br/>\n          </p>     <p style=\"font-size:0.9em\">*<i>This invoice is valid for 5 days after which a new order needs to be placed</i></p>   </div>\n    </body>\n    </html>"
        val HtmlSeq = scala.xml.Unparsed(html)
        val b = HtmlSeq.foldLeft(scala.xml.NodeSeq.Empty){(a,b)=> a ++ b}
        val outputFile = "Invoice_"+ref+".pdf"
        val os = new FileOutputStream(outputFile)
        val renderer = new ITextRenderer()
        renderer.setDocumentFromString(b.text)
        renderer.getFontResolver().addFont("SourceSansPro-Regular.ttf", BaseFont.IDENTITY_H, BaseFont.NOT_EMBEDDED);
        renderer.layout()
        renderer.createPDF(os)
        os.close()
        import java.nio.file.{Files, Paths}
        val filename = "Invoice_"+ref+".pdf"
        val byteArray = Files.readAllBytes(Paths.get(filename))
        if (download == true) {
          throw new ResponseShortcutException( InMemoryResponse(
            byteArray,
            "Content-Type" -> "application/pdf; charset=utf8" ::
              "Content-Disposition" -> s"attachment; filename=$outputFile" :: Nil,
            cookies=Nil, code = 200))
        } else {
          val HtmlMsg = "<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional //EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\"><!--[if IE]><html xmlns=\"http://www.w3.org/1999/xhtml\" class=\"ie\"><![endif]--><!--[if !IE]><!--><html style=\"margin: 0;padding: 0;\" xmlns=\"http://www.w3.org/1999/xhtml\"><!--<![endif]--><head>\n    <meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\" />\n    <!--[if !mso]><!--><meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\" /><!--<![endif]-->\n    <meta name=\"viewport\" content=\"width=device-width\" /><style type=\"text/css\">\n@media only screen and (min-width: 620px){* [lang=x-wrapper] h1{}* [lang=x-wrapper] h1{font-size:26px !important;line-height:34px !important}* [lang=x-wrapper] h2{}* [lang=x-wrapper] h2{font-size:20px !important;line-height:28px !important}* [lang=x-wrapper] h3{}* [lang=x-layout__inner] p,* [lang=x-layout__inner] ol,* [lang=x-layout__inner] ul{}* div [lang=x-size-8]{font-size:8px !important;line-height:14px !important}* div [lang=x-size-9]{font-size:9px !important;line-height:16px !important}* div [lang=x-size-10]{font-size:10px !important;line-height:18px !important}* div [lang=x-size-11]{font-size:11px !important;line-height:19px !important}* div [lang=x-size-12]{font-size:12px !important;line-height:19px !important}* div [lang=x-size-13]{font-size:13px !important;line-height:21px !important}* div [lang=x-size-14]{font-size:14px !important;line-height:21px !important}* div \n[lang=x-size-15]{font-size:15px !important;line-height:23px !important}* div [lang=x-size-16]{font-size:16px !important;line-height:24px !important}* div [lang=x-size-17]{font-size:17px !important;line-height:26px !important}* div [lang=x-size-18]{font-size:18px !important;line-height:26px !important}* div [lang=x-size-18]{font-size:18px !important;line-height:26px !important}* div [lang=x-size-20]{font-size:20px !important;line-height:28px !important}* div [lang=x-size-22]{font-size:22px !important;line-height:31px !important}* div [lang=x-size-24]{font-size:24px !important;line-height:32px !important}* div [lang=x-size-26]{font-size:26px !important;line-height:34px !important}* div [lang=x-size-28]{font-size:28px !important;line-height:36px !important}* div [lang=x-size-30]{font-size:30px !important;line-height:38px !important}* div [lang=x-size-32]{font-size:32px \n!important;line-height:40px !important}* div [lang=x-size-34]{font-size:34px !important;line-height:43px !important}* div [lang=x-size-36]{font-size:36px !important;line-height:43px !important}* div [lang=x-size-40]{font-size:40px !important;line-height:47px !important}* div [lang=x-size-44]{font-size:44px !important;line-height:50px !important}* div [lang=x-size-48]{font-size:48px !important;line-height:54px !important}* div [lang=x-size-56]{font-size:56px !important;line-height:60px !important}* div [lang=x-size-64]{font-size:64px !important;line-height:63px !important}}\n</style>\n    <style type=\"text/css\">\nbody {\n  margin: 0;\n  padding: 0;\n}\ntable {\n  border-collapse: collapse;\n  table-layout: fixed;\n}\n* {\n  line-height: inherit;\n}\n[x-apple-data-detectors],\n[href^=\"tel\"],\n[href^=\"sms\"] {\n  color: inherit !important;\n  text-decoration: none !important;\n}\n.wrapper .footer__share-button a:hover,\n.wrapper .footer__share-button a:focus {\n  color: #ffffff !important;\n}\n.btn a:hover,\n.btn a:focus,\n.footer__share-button a:hover,\n.footer__share-button a:focus,\n.email-footer__links a:hover,\n.email-footer__links a:focus {\n  opacity: 0.8;\n}\n.preheader,\n.header,\n.layout,\n.column {\n  transition: width 0.25s ease-in-out, max-width 0.25s ease-in-out;\n}\n.layout,\n.header {\n  max-width: 400px !important;\n  -fallback-width: 95% !important;\n  width: calc(100% - 20px) !important;\n}\ndiv.preheader {\n  max-width: 360px !important;\n  -fallback-width: 90% !important;\n  width: calc(100% - 60px) !important;\n}\n.snippet,\n.webversion {\n  Float: none !important;\n}\n.column {\n  max-width: 400px !important;\n  width: 100% !important;\n}\n.fixed-width.has-border {\n  max-width: 402px !important;\n}\n.fixed-width.has-border .layout__inner {\n  box-sizing: border-box;\n}\n.snippet,\n.webversion {\n  width: 50% !important;\n}\n.ie .btn {\n  width: 100%;\n}\n.ie .column,\n[owa] .column,\n.ie .gutter,\n[owa] .gutter {\n  display: table-cell;\n  float: none !important;\n  vertical-align: top;\n}\n.ie div.preheader,\n[owa] div.preheader,\n.ie .email-footer,\n[owa] .email-footer {\n  max-width: 560px !important;\n  width: 560px !important;\n}\n.ie .snippet,\n[owa] .snippet,\n.ie .webversion,\n[owa] .webversion {\n  width: 280px !important;\n}\n.ie .header,\n[owa] .header,\n.ie .layout,\n[owa] .layout,\n.ie .one-col .column,\n[owa] .one-col .column {\n  max-width: 600px !important;\n  width: 600px !important;\n}\n.ie .fixed-width.has-border,\n[owa] .fixed-width.has-border,\n.ie .has-gutter.has-border,\n[owa] .has-gutter.has-border {\n  max-width: 602px !important;\n  width: 602px !important;\n}\n.ie .two-col .column,\n[owa] .two-col .column {\n  width: 300px !important;\n}\n.ie .three-col .column,\n[owa] .three-col .column,\n.ie .narrow,\n[owa] .narrow {\n  width: 200px !important;\n}\n.ie .wide,\n[owa] .wide {\n  width: 400px !important;\n}\n.ie .two-col.has-gutter .column,\n[owa] .two-col.x_has-gutter .column {\n  width: 290px !important;\n}\n.ie .three-col.has-gutter .column,\n[owa] .three-col.x_has-gutter .column,\n.ie .has-gutter .narrow,\n[owa] .has-gutter .narrow {\n  width: 188px !important;\n}\n.ie .has-gutter .wide,\n[owa] .has-gutter .wide {\n  width: 394px !important;\n}\n.ie .two-col.has-gutter.has-border .column,\n[owa] .two-col.x_has-gutter.x_has-border .column {\n  width: 292px !important;\n}\n.ie .three-col.has-gutter.has-border .column,\n[owa] .three-col.x_has-gutter.x_has-border .column,\n.ie .has-gutter.has-border .narrow,\n[owa] .has-gutter.x_has-border .narrow {\n  width: 190px !important;\n}\n.ie .has-gutter.has-border .wide,\n[owa] .has-gutter.x_has-border .wide {\n  width: 396px !important;\n}\n.ie .fixed-width .layout__inner {\n  border-left: 0 none white !important;\n  border-right: 0 none white !important;\n}\n.ie .layout__edges {\n  display: none;\n}\n.mso .layout__edges {\n  font-size: 0;\n}\n.layout-fixed-width,\n.mso .layout-full-width {\n  background-color: #ffffff;\n}\n@media only screen and (min-width: 620px) {\n  .column,\n  .gutter {\n    display: table-cell;\n    Float: none !important;\n    vertical-align: top;\n  }\n  div.preheader,\n  .email-footer {\n    max-width: 560px !important;\n    width: 560px !important;\n  }\n  .snippet,\n  .webversion {\n    width: 280px !important;\n  }\n  .header,\n  .layout,\n  .one-col .column {\n    max-width: 600px !important;\n    width: 600px !important;\n  }\n  .fixed-width.has-border,\n  .fixed-width.ecxhas-border,\n  .has-gutter.has-border,\n  .has-gutter.ecxhas-border {\n    max-width: 602px !important;\n    width: 602px !important;\n  }\n  .two-col .column {\n    width: 300px !important;\n  }\n  .three-col .column,\n  .column.narrow {\n    width: 200px !important;\n  }\n  .column.wide {\n    width: 400px !important;\n  }\n  .two-col.has-gutter .column,\n  .two-col.ecxhas-gutter .column {\n    width: 290px !important;\n  }\n  .three-col.has-gutter .column,\n  .three-col.ecxhas-gutter .column,\n  .has-gutter .narrow {\n    width: 188px !important;\n  }\n  .has-gutter .wide {\n    width: 394px !important;\n  }\n  .two-col.has-gutter.has-border .column,\n  .two-col.ecxhas-gutter.ecxhas-border .column {\n    width: 292px !important;\n  }\n  .three-col.has-gutter.has-border .column,\n  .three-col.ecxhas-gutter.ecxhas-border .column,\n  .has-gutter.has-border .narrow,\n  .has-gutter.ecxhas-border .narrow {\n    width: 190px !important;\n  }\n  .has-gutter.has-border .wide,\n  .has-gutter.ecxhas-border .wide {\n    width: 396px !important;\n  }\n}\n@media only screen and (-webkit-min-device-pixel-ratio: 2), only screen and (min--moz-device-pixel-ratio: 2), only screen and (-o-min-device-pixel-ratio: 2/1), only screen and (min-device-pixel-ratio: 2), only screen and (min-resolution: 192dpi), only screen and (min-resolution: 2dppx) {\n  .fblike {\n    background-image: url(https://i3.createsend1.com/static/eb/customise/13-the-blueprint-3/images/fblike@2x.png) !important;\n  }\n  .tweet {\n    background-image: url(https://i4.createsend1.com/static/eb/customise/13-the-blueprint-3/images/tweet@2x.png) !important;\n  }\n  .linkedinshare {\n    background-image: url(https://i6.createsend1.com/static/eb/customise/13-the-blueprint-3/images/lishare@2x.png) !important;\n  }\n  .forwardtoafriend {\n    background-image: url(https://i5.createsend1.com/static/eb/customise/13-the-blueprint-3/images/forward@2x.png) !important;\n  }\n}\n@media (max-width: 321px) {\n  .fixed-width.has-border .layout__inner {\n    border-width: 1px 0 !important;\n  }\n  .layout,\n  .column {\n    min-width: 320px !important;\n    width: 320px !important;\n  }\n  .border {\n    display: none;\n  }\n}\n.mso div {\n  border: 0 none white !important;\n}\n.mso .w560 .divider {\n  Margin-left: 260px !important;\n  Margin-right: 260px !important;\n}\n.mso .w360 .divider {\n  Margin-left: 160px !important;\n  Margin-right: 160px !important;\n}\n.mso .w260 .divider {\n  Margin-left: 110px !important;\n  Margin-right: 110px !important;\n}\n.mso .w160 .divider {\n  Margin-left: 60px !important;\n  Margin-right: 60px !important;\n}\n.mso .w354 .divider {\n  Margin-left: 157px !important;\n  Margin-right: 157px !important;\n}\n.mso .w250 .divider {\n  Margin-left: 105px !important;\n  Margin-right: 105px !important;\n}\n.mso .w148 .divider {\n  Margin-left: 54px !important;\n  Margin-right: 54px !important;\n}\n.mso .font-avenir,\n.mso .font-cabin,\n.mso .font-open-sans,\n.mso .font-ubuntu {\n  font-family: sans-serif !important;\n}\n.mso .font-bitter,\n.mso .font-merriweather,\n.mso .font-pt-serif {\n  font-family: Georgia, serif !important;\n}\n.mso .font-lato,\n.mso .font-roboto {\n  font-family: Tahoma, sans-serif !important;\n}\n.mso .font-pt-sans {\n  font-family: \"Trebuchet MS\", sans-serif !important;\n}\n.mso .footer__share-button p {\n  margin: 0;\n}\n@media only screen and (min-width: 620px) {\n  .wrapper .size-8 {\n    font-size: 8px !important;\n    line-height: 14px !important;\n  }\n  .wrapper .size-9 {\n    font-size: 9px !important;\n    line-height: 16px !important;\n  }\n  .wrapper .size-10 {\n    font-size: 10px !important;\n    line-height: 18px !important;\n  }\n  .wrapper .size-11 {\n    font-size: 11px !important;\n    line-height: 19px !important;\n  }\n  .wrapper .size-12 {\n    font-size: 12px !important;\n    line-height: 19px !important;\n  }\n  .wrapper .size-13 {\n    font-size: 13px !important;\n    line-height: 21px !important;\n  }\n  .wrapper .size-14 {\n    font-size: 14px !important;\n    line-height: 21px !important;\n  }\n  .wrapper .size-15 {\n    font-size: 15px !important;\n    line-height: 23px !important;\n  }\n  .wrapper .size-16 {\n    font-size: 16px !important;\n    line-height: 24px !important;\n  }\n  .wrapper .size-17 {\n    font-size: 17px !important;\n    line-height: 26px !important;\n  }\n  .wrapper .size-18 {\n    font-size: 18px !important;\n    line-height: 26px !important;\n  }\n  .wrapper .size-20 {\n    font-size: 20px !important;\n    line-height: 28px !important;\n  }\n  .wrapper .size-22 {\n    font-size: 22px !important;\n    line-height: 31px !important;\n  }\n  .wrapper .size-24 {\n    font-size: 24px !important;\n    line-height: 32px !important;\n  }\n  .wrapper .size-26 {\n    font-size: 26px !important;\n    line-height: 34px !important;\n  }\n  .wrapper .size-28 {\n    font-size: 28px !important;\n    line-height: 36px !important;\n  }\n  .wrapper .size-30 {\n    font-size: 30px !important;\n    line-height: 38px !important;\n  }\n  .wrapper .size-32 {\n    font-size: 32px !important;\n    line-height: 40px !important;\n  }\n  .wrapper .size-34 {\n    font-size: 34px !important;\n    line-height: 43px !important;\n  }\n  .wrapper .size-36 {\n    font-size: 36px !important;\n    line-height: 43px !important;\n  }\n  .wrapper .size-40 {\n    font-size: 40px !important;\n    line-height: 47px !important;\n  }\n  .wrapper .size-44 {\n    font-size: 44px !important;\n    line-height: 50px !important;\n  }\n  .wrapper .size-48 {\n    font-size: 48px !important;\n    line-height: 54px !important;\n  }\n  .wrapper .size-56 {\n    font-size: 56px !important;\n    line-height: 60px !important;\n  }\n  .wrapper .size-64 {\n    font-size: 64px !important;\n    line-height: 63px !important;\n  }\n}\n.mso .size-8,\n.ie .size-8 {\n  font-size: 8px !important;\n  line-height: 14px !important;\n}\n.mso .size-9,\n.ie .size-9 {\n  font-size: 9px !important;\n  line-height: 16px !important;\n}\n.mso .size-10,\n.ie .size-10 {\n  font-size: 10px !important;\n  line-height: 18px !important;\n}\n.mso .size-11,\n.ie .size-11 {\n  font-size: 11px !important;\n  line-height: 19px !important;\n}\n.mso .size-12,\n.ie .size-12 {\n  font-size: 12px !important;\n  line-height: 19px !important;\n}\n.mso .size-13,\n.ie .size-13 {\n  font-size: 13px !important;\n  line-height: 21px !important;\n}\n.mso .size-14,\n.ie .size-14 {\n  font-size: 14px !important;\n  line-height: 21px !important;\n}\n.mso .size-15,\n.ie .size-15 {\n  font-size: 15px !important;\n  line-height: 23px !important;\n}\n.mso .size-16,\n.ie .size-16 {\n  font-size: 16px !important;\n  line-height: 24px !important;\n}\n.mso .size-17,\n.ie .size-17 {\n  font-size: 17px !important;\n  line-height: 26px !important;\n}\n.mso .size-18,\n.ie .size-18 {\n  font-size: 18px !important;\n  line-height: 26px !important;\n}\n.mso .size-20,\n.ie .size-20 {\n  font-size: 20px !important;\n  line-height: 28px !important;\n}\n.mso .size-22,\n.ie .size-22 {\n  font-size: 22px !important;\n  line-height: 31px !important;\n}\n.mso .size-24,\n.ie .size-24 {\n  font-size: 24px !important;\n  line-height: 32px !important;\n}\n.mso .size-26,\n.ie .size-26 {\n  font-size: 26px !important;\n  line-height: 34px !important;\n}\n.mso .size-28,\n.ie .size-28 {\n  font-size: 28px !important;\n  line-height: 36px !important;\n}\n.mso .size-30,\n.ie .size-30 {\n  font-size: 30px !important;\n  line-height: 38px !important;\n}\n.mso .size-32,\n.ie .size-32 {\n  font-size: 32px !important;\n  line-height: 40px !important;\n}\n.mso .size-34,\n.ie .size-34 {\n  font-size: 34px !important;\n  line-height: 43px !important;\n}\n.mso .size-36,\n.ie .size-36 {\n  font-size: 36px !important;\n  line-height: 43px !important;\n}\n.mso .size-40,\n.ie .size-40 {\n  font-size: 40px !important;\n  line-height: 47px !important;\n}\n.mso .size-44,\n.ie .size-44 {\n  font-size: 44px !important;\n  line-height: 50px !important;\n}\n.mso .size-48,\n.ie .size-48 {\n  font-size: 48px !important;\n  line-height: 54px !important;\n}\n.mso .size-56,\n.ie .size-56 {\n  font-size: 56px !important;\n  line-height: 60px !important;\n}\n.mso .size-64,\n.ie .size-64 {\n  font-size: 64px !important;\n  line-height: 63px !important;\n}\n.footer__share-button p {\n  margin: 0;\n}\n</style>\n    \n    <title></title>\n  <!--[if !mso]><!--><style type=\"text/css\">\n@import url(https://fonts.googleapis.com/css?family=Cabin:400,700,400italic,700italic|Open+Sans:400italic,700italic,700,400);\n</style><link href=\"https://fonts.googleapis.com/css?family=Cabin:400,700,400italic,700italic|Open+Sans:400italic,700italic,700,400\" rel=\"stylesheet\" type=\"text/css\" /><!--<![endif]--><style type=\"text/css\">\nbody{background-color:#f5f7fa}.mso h1{}.mso h1{font-family:sans-serif !important}.mso h2{}.mso h3{}.mso .column,.mso .column__background td{}.mso .column,.mso .column__background td{font-family:sans-serif !important}.mso .btn a{}.mso .btn a{font-family:sans-serif !important}.mso .webversion,.mso .snippet,.mso .layout-email-footer td,.mso .footer__share-button p{}.mso .webversion,.mso .snippet,.mso .layout-email-footer td,.mso .footer__share-button p{font-family:sans-serif !important}.mso .logo{}.mso .logo{font-family:Tahoma,sans-serif !important}.logo a:hover,.logo a:focus{color:#859bb1 !important}.mso .layout-has-border{border-top:1px solid #b1c1d8;border-bottom:1px solid #b1c1d8}.mso .layout-has-bottom-border{border-bottom:1px solid #b1c1d8}.mso .border,.ie .border{background-color:#b1c1d8}@media only screen and (min-width: 620px){.wrapper h1{}.wrapper h1{font-size:26px \n!important;line-height:34px !important}.wrapper h2{}.wrapper h2{font-size:20px !important;line-height:28px !important}.wrapper h3{}.column p,.column ol,.column ul{}}.mso h1,.ie h1{}.mso h1,.ie h1{font-size:26px !important;line-height:34px !important}.mso h2,.ie h2{}.mso h2,.ie h2{font-size:20px !important;line-height:28px !important}.mso h3,.ie h3{}.mso .layout__inner p,.ie .layout__inner p,.mso .layout__inner ol,.ie .layout__inner ol,.mso .layout__inner ul,.ie .layout__inner ul{}\n</style><meta name=\"robots\" content=\"noindex,nofollow\" />\n<meta property=\"og:title\" content=\"My First Campaign\" />\n</head>\n<!--[if mso]>\n  <body class=\"mso\">\n<![endif]-->\n<!--[if !mso]><!-->\n  <body class=\"full-padding\" style=\"margin: 0;padding: 0;-webkit-text-size-adjust: 100%;\">\n<!--<![endif]-->\n    <div class=\"wrapper\" style=\"min-width: 320px;background-color: #f5f7fa;\" lang=\"x-wrapper\">\n      <div class=\"preheader\" style=\"Margin: 0 auto;max-width: 560px;min-width: 280px; width: 280px;width: calc(28000% - 173040px);\">\n        <div style=\"border-collapse: collapse;display: table;width: 100%;\">\n        <!--[if (mso)|(IE)]><table align=\"center\" class=\"preheader\" cellpadding=\"0\" cellspacing=\"0\"><tr><td style=\"width: 280px\" valign=\"top\"><![endif]-->\n          <div class=\"snippet\" style='display: table-cell;Float: left;font-size: 12px;line-height: 19px;max-width: 280px;min-width: 140px; width: 140px;width: calc(14000% - 78120px);padding: 10px 0 5px 0;color: #b9b9b9;font-family: \"Open Sans\",sans-serif;'>\n            \n          </div>\n        <!--[if (mso)|(IE)]></td><td style=\"width: 280px\" valign=\"top\"><![endif]-->\n          <div class=\"webversion\" style='display: table-cell;Float: left;font-size: 12px;line-height: 19px;max-width: 280px;min-width: 139px; width: 139px;width: calc(14100% - 78680px);padding: 10px 0 5px 0;text-align: right;color: #b9b9b9;font-family: \"Open Sans\",sans-serif;'>\n            \n          </div>\n        <!--[if (mso)|(IE)]></td></tr></table><![endif]-->\n        </div>\n      </div>\n      <div class=\"header\" style=\"Margin: 0 auto;max-width: 600px;min-width: 320px; width: 320px;width: calc(28000% - 173000px);\" id=\"emb-email-header-container\">\n      <!--[if (mso)|(IE)]><table align=\"center\" class=\"header\" cellpadding=\"0\" cellspacing=\"0\"><tr><td style=\"width: 600px\"><![endif]-->\n        <div class=\"logo emb-logo-margin-box\" style=\"font-size: 26px;line-height: 32px;Margin-top: 6px;Margin-bottom: 20px;color: #c3ced9;font-family: Roboto,Tahoma,sans-serif;Margin-left: 20px;Margin-right: 20px;\" align=\"center\">\n          <div class=\"logo-center\" style=\"font-size:0px !important;line-height:0 !important;\" align=\"center\" id=\"emb-email-header\"><a style=\"text-decoration: none;transition: opacity 0.1s ease-in;color: #c3ced9;\" href=\"https://grit.systems/img/grit-small.png\"><img style=\"height: auto;width: 100%;border: 0;max-width: 165px;\" src=\"https://grit.systems/img/grit-small.png\" alt=\"logo\" width=\"165\" /></a></div>\n        </div>\n      <!--[if (mso)|(IE)]></td></tr></table><![endif]-->\n      </div>\n      <div class=\"layout one-col fixed-width\" style=\"Margin: 0 auto;max-width: 600px;min-width: 320px; width: 320px;width: calc(28000% - 173000px);overflow-wrap: break-word;word-wrap: break-word;word-break: break-word;\">\n        <div class=\"layout__inner\" style=\"border-collapse: collapse;display: table;width: 100%;background-color: #ffffff;\" lang=\"x-layout__inner\" emb-background-style>\n        <!--[if (mso)|(IE)]><table align=\"center\" cellpadding=\"0\" cellspacing=\"0\"><tr class=\"layout-fixed-width\" emb-background-style><td style=\"width: 600px\" class=\"w560\"><![endif]-->\n          <div class=\"column\" style='text-align: left;color: #60666d;font-size: 14px;line-height: 21px;font-family: \"Open Sans\",sans-serif;max-width: 600px;min-width: 320px; width: 320px;width: calc(28000% - 167400px);'>\n        \n            <div style=\"Margin-left: 20px;Margin-right: 20px;Margin-top: 24px;\">\n      <p class=\"size-17\" style=\"Margin-top: 0;Margin-bottom: 0;font-family: cabin,avenir,sans-serif;font-size: 17px;line-height: 26px;\" lang=\"x-size-17\"><span class=\"font-cabin\"><strong>Hello,</strong></span></p><p class=\"size-17\" style=\"Margin-top: 20px;Margin-bottom: 0;font-family: cabin,avenir,sans-serif;font-size: 17px;line-height: 26px;\" lang=\"x-size-17\"><span class=\"font-cabin\">\n Thank you for your interest in the GRIT Systems G1 energy monitor. </span></p><p class=\"size-17\" style=\"Margin-top: 20px;Margin-bottom: 0;font-family: cabin,avenir,sans-serif;font-size: 17px;line-height: 26px;\" lang=\"x-size-17\"><span class=\"font-cabin\">\n Please find attached invoice for setting up the Energy monitor. This invoice was generated with the information you supplied in the request form.</span></p><p class=\"size-17\" style=\"Margin-top: 20px;Margin-bottom: 20px;font-family: cabin,avenir,sans-serif;font-size: 17px;line-height: 26px;\" lang=\"x-size-17\"><span class=\"font-cabin\">\nThe device would be set up by our deployment team after you have accepted this invoice.</span></p>\n    </div>\n        \n            <div style=\"Margin-left: 20px;Margin-right: 20px;\">\n      <div style=\"line-height:10px;font-size:1px\">&nbsp;</div>\n    </div>\n            \n        \n            <div style=\"Margin-left: 20px;Margin-right: 20px;Margin-bottom: 24px;\">\n      <div class=\"btn btn--flat btn--large\" style=\"text-align:left;\">\n        <![if !mso]><a style='border-radius: 4px;display: inline-block;font-size: 14px;font-weight: bold;line-height: 24px;padding: 12px 24px;text-align: center;text-decoration: none !important;transition: opacity 0.1s ease-in;color: #fff;background-color: #36a60d; color: #f5f7fa; font-family: &#39;Open Sans&#39;, sans-serif !important;font-family: \"Open Sans\",sans-serif;' href=\"http://grit.systems/system/payments/accept?q="+invoiceId+"&email="+email+ "\">View Invoice</a><![endif]>\n      <!--[if mso]><p style=\"line-height:0;margin:0;\">&nbsp;</p><v:roundrect xmlns:v=\"urn:schemas-microsoft-com:vml\" href=\"http://grit.systems/system/payments/accept?q="+invoiceId+"&email="+email+ " \" style=\"width:125px\" arcsize=\"9%\" fillcolor=\"#36A60D\" stroke=\"f\"><v:textbox style=\"mso-fit-shape-to-text:t\" inset=\"0px,11px,0px,11px\"><center style=\"font-size:14px;line-height:24px;color:#F5F7FA;font-family:sans-serif;font-weight:bold;mso-line-height-rule:exactly;mso-text-raise:4px\">Go to Form</center></v:textbox></v:roundrect><![endif]--></div>\n    </div>\n        \n   \n          </div>\n        <!--[if (mso)|(IE)]></td></tr></table><![endif]-->\n        </div>\n      </div>\n  \n      <div class=\"layout email-footer\" style=\"Margin: 0 auto;max-width: 600px;min-width: 320px; width: 320px;width: calc(28000% - 173000px);overflow-wrap: break-word;word-wrap: break-word;word-break: break-word;\">\n        <div class=\"layout__inner\" style=\"border-collapse: collapse;display: table;width: 100%;\" lang=\"x-layout__inner\">\n        <!--[if (mso)|(IE)]><table align=\"center\" cellpadding=\"0\" cellspacing=\"0\"><tr class=\"layout-email-footer\"><td style=\"width: 400px;\" valign=\"top\" class=\"w360\"><![endif]-->\n          <div class=\"column wide\" style='text-align: left;font-size: 12px;line-height: 19px;color: #b9b9b9;font-family: \"Open Sans\",sans-serif;Float: left;max-width: 400px;min-width: 320px; width: 320px;width: calc(8000% - 47600px);'>\n            <div style=\"Margin-left: 20px;Margin-right: 20px;Margin-top: 10px;Margin-bottom: 10px;\">\n              <table class=\"email-footer__links emb-web-links\" style=\"border-collapse: collapse;table-layout: fixed;\"><tbody><tr>\n              \n<td class=\"emb-web-links\" style=\"padding: 0;width: 26px;\"><a style=\"text-decoration: underline;transition: opacity 0.1s ease-in;color: #b9b9b9;\" href=\"http://www.facebook.com/grit.systems/\"><img style=\"border: 0;\" src=\"https://i8.createsend1.com/static/eb/customise/13-the-blueprint-3/images/facebook.png\" width=\"26\" height=\"26\" /></a></td><td class=\"emb-web-links\" style=\"padding: 0 0 0 3px;width: 26px;\"><a style=\"text-decoration: underline;transition: opacity 0.1s ease-in;color: #b9b9b9;\" href=\"http://twitter.com/grit_systems\"><img style=\"border: 0;\" src=\"https://i10.createsend1.com/static/eb/customise/13-the-blueprint-3/images/twitter.png\" width=\"26\" height=\"26\" /></a></td><td class=\"emb-web-links\" style=\"padding: 0 0 0 3px;width: 26px;\"><a style=\"text-decoration: underline;transition: opacity 0.1s ease-in;color: #b9b9b9;\" href=\"https://www.linkedin.com/company/grit-systems-engineering\"><img style=\"border: 0;\" src=\"https://i2.createsend1.com/static/eb/customise/13-the-blueprint-3/images/linkedin.png\" width=\"26\" height=\"26\" /></a></td><td class=\"emb-web-links\" style=\"padding: 0 0 0 3px;width: 26px;\"><a style=\"text-decoration: underline;transition: opacity 0.1s ease-in;color: #b9b9b9;\" href=\"https://grit.systems\"><img style=\"border: 0;\" src=\"https://i3.createsend1.com/static/eb/customise/13-the-blueprint-3/images/website.png\" width=\"26\" height=\"26\" /></a></td>\n              </tr></tbody></table>\n              <div style=\"Margin-top: 20px;\">\n                <div>GRIT Systems<br />\n294 Herbert Macauly<br />\n101212 Yaba, Lagos<br />\n<br />\n+234(0)706 555 1430</div>\n              </div>\n              <div style=\"Margin-top: 18px;\">\n                \n              </div>\n            </div>\n          </div>\n        <!--[if (mso)|(IE)]></td><td style=\"width: 200px;\" valign=\"top\" class=\"w160\"><![endif]-->\n          <div class=\"column narrow\" style='text-align: left;font-size: 12px;line-height: 19px;color: #b9b9b9;font-family: \"Open Sans\",sans-serif;Float: left;max-width: 320px;min-width: 200px; width: 320px;width: calc(72200px - 12000%);'>\n            <div style=\"Margin-left: 20px;Margin-right: 20px;Margin-top: 10px;Margin-bottom: 10px;\">\n              \n            </div>\n          </div>\n        <!--[if (mso)|(IE)]></td></tr></table><![endif]-->\n        </div>\n      </div>\n      <div style=\"line-height:40px;font-size:40px;\">&nbsp;</div>\n    \n  </div>\n</body></html>"

          val HtmlMsgSeq = scala.xml.Unparsed(HtmlMsg)
          SmtpMailer.init()
          val msg = XHTMLPlusImages(HtmlMsgSeq,
            PlusImageHolder(filename, "application/pdf", byteArray))

          Mailer.sendMail(
            From("notify@grit.systems"),
            Subject("Invoice for GRIT Energy Monitor"),
            To("idarlington@grit.systems"),
            msg)
          invoice.Sent(true).save()
          S.appendGlobalJs(Run("toastr[\"success\"](\"Mail was Succesfully Sent.\")"))

        }
      }
      case _ =>{}
    }
    Noop
  }


}
