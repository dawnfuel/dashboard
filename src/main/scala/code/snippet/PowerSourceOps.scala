package code.snippet

import code.model.PowerConsumer
import code.model.Probe._
import net.liftweb.http.js.JsCmds._
//import code.snippet.ConfigurationOps.Item
import org.bson.types.ObjectId
import net.liftweb._
import net.liftweb.http._
import util._
import common._
import Helpers._
import net.liftweb.common.Loggable
import scala.xml._
import net.liftweb.mongodb.BsonDSL._
import scala.xml.Text
import net.liftweb.http.SHtml.ajaxSelect
import net.liftweb.http.js.JsCmds.After
import code.model.{User, Configuration, Probe, Generator}
import code.lib.MSHtml
/*
 * This class provides the snippets that back the Source CRUD 
 * pages (List, Create, View, Edit, Delete)
 */
class PowerSourceOps extends Loggable {
  val currentUserId = S.get("selectedUserID").toOption.getOrElse(User.currentUserId.headOption.getOrElse(""))
  var currentUserEmail = User.find("_id",new ObjectId(currentUserId)).headOption.getOrElse(User).email.toString()
  object GeneratorVar extends RequestVar[Generator](Generator.createRecord)

  def processSubmit() = {
    GeneratorVar.is.validate match {
      case  Nil => {
        GeneratorVar.is.save()
        S.notice("Source Saved")
        S.seeOther("/system/source/listsource")
      }
      case errors => S.error(errors)
    }
    // exiting the function here causes the form to reload
  }
  def set_probe( assignedprobe : String ) : Any =  {
    print("\nASSIGNED PROBES :"+assignedprobe+"\n")
  }
  // called from /system/source/createsource.html
  def create = {
    case class Item(id: String, name: String)
    def g(v:Probe) = List(Item(v.id.toString, v.probeName.toString))
    def manOf[T: Manifest](t: T): Manifest[T] = manifest[T]
    val currentConfs = Configuration.findAll("UserID_FKs",currentUserId)
    println("CONFs of LOGGED IN USER "+currentConfs)
    var list : List[Item] = List()
    val probesLoggedIn = currentConfs.map( x => {
      println(x)
      list = list ::: Probe.findAll(("ConfigID_FK" -> x.id.toString)).flatMap(p => g(p))
    })

    println("\n\nlist of probes associated with UserID : "+currentUserId)
    list.foreach(println)
    val optionsprobe =
      list.map(i => (i.id -> i.name))
    println(optionsprobe)
    println(list)
    println()
    val source = GeneratorVar.is
    "#hidden" #> SHtml.hidden(() => GeneratorVar(source) ) &
      "#sourcename" #> SHtml.text(GeneratorVar.is.sourceName.value, name => GeneratorVar.is.sourceName(name) ) &
      "#sourceprobes" #> SHtml.select(optionsprobe, Box.legacyNullTest(GeneratorVar.is.sourceName.value), set_probe ) &
    // This registers a function for Lift to call when the user clicks submit
      "#submit" #> SHtml.onSubmitUnit(processSubmit)
  }

  // called from /system/source/editsource.html
  def edit = {
    if ( ! GeneratorVar.set_? )
      S.redirectTo("/system/source/listsource")

    val source = GeneratorVar.is
    "#hidden" #> SHtml.hidden(() => GeneratorVar(source) ) &
      "#sourcename" #> SHtml.text(GeneratorVar.is.sourceName.value, name => GeneratorVar.is.sourceName(name) ) &
      "#submit" #> SHtml.onSubmitUnit(processSubmit)
  }

  // called from /system/source/listsource.html
  def list = {
    def ViewOrEditLink(sType :String,id:String,action:String):Elem={
      //match the source type and use the right edit link.
      sType match{
      case "generator" => return SHtml.link("/system/source/editgen?id=%s&action=%s".format(id,action),() => GeneratorVar(Generator), Text(action.capitalize))
      case "inverter" => return SHtml.link("/system/source/editinv?id=%s&action=%s".format(id,action), () => GeneratorVar(Generator), Text(action.capitalize))
      case "grid" => return SHtml.link("/system/source/editgrid?id=%s&action=%s".format(id,action), () => GeneratorVar(Generator), Text(action.capitalize))
      case _ => S.redirectTo("/system/source/listsource")
      }
    }
    val user = User.find("_id",new ObjectId(currentUserId)).head.username
    "#heading_user" #> SHtml.span(<h5 class="alt" id="heading_user">Power Sources associated with : {user}</h5>, (Noop)  ) &
    // for each Source object in the database render the name along with links to view, edit, and delete
    "#sourcelist *" #> getSources(Configuration.findAll("UserID_FKs",currentUserId).map{ conf => conf.id.toString }).map( t => {
      ".sourceName *" #> Text(t.sourceName.value) &
        ".sourceType *" #> Text(t.Type.value) &
        ".phaseCount *" #> Text(t.NumberOfPhases.toString) &
        ".actions *" #> {
          ViewOrEditLink(t.Type.toString(),t.id.toString(),"View") ++ Text(" ") ++
          ViewOrEditLink(t.Type.toString(),t.id.toString(),"Edit") ++ Text(" ") ++
          SHtml.link("/system/source/deletesource", () => GeneratorVar(t), Text("Delete"))}
    } )
  }

  def getSources(confIDs:List[String]):List[Generator]={
    var g:List[Generator] = List()
    for(p <- confIDs){
      g = g ::: Generator.findAll("ConfigID_FK",p)
    }
    return g
  }


  // called from /system/source/viewsource.html
  def view = {
    // we're expecting the GeneratorVar to have been set when we were linked to from the list page
    if ( ! GeneratorVar.set_? )
      S.redirectTo("/system/source/listsource")

    val source = GeneratorVar.is
    "#sourcename *" #> GeneratorVar.is.sourceName.asHtml &
      "#edit" #> SHtml.link("/system/source/editsource", () => (GeneratorVar(source)), Text("Edit"))
  }

  // called from /system/source/deletesource.html
  def deleteGenSource(e : Generator){
    logger.info("\n\ndeleting source :  "+e.ConfigID_FK)
    findChannelsForSource(List(e.id.toString),List(e.ConfigID_FK.toString),true)
    e.delete_!
  }


  def delete = {
    // we're expecting the GeneratorVar to have been set when we were linked to from the list page
    if ( ! GeneratorVar.set_? )
      S.redirectTo("/system/source/listsource")

    val e = GeneratorVar.is
    "#sourcename" #> GeneratorVar.is.sourceName &
      "#yes" #> SHtml.link("/system/source/listsource", () =>{deleteGenSource(e); e.delete_!}, Text("Yes")) &
      "#no" #> SHtml.link("/system/source/listsource", () =>{ }, Text("No"))
  }


}