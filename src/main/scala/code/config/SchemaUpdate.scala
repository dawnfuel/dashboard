package code.config

/**
  * Created by idarlington on 5/26/16.
  */
import code.model._
import net.liftweb.common.Empty


object SchemaUpdate {

  def Update() : Unit = {

    // For all Power sources, copy the configID_FK & signal threshold
    // of the associated probe
    Generator.findAll.foreach(c => {
        val probes = Probe.findAll("_id", c.ProbeID_FK.toString)
        if (probes.nonEmpty) {
          val configID = probes.head.ConfigID_FK.toString()
          val signalThreshold = probes.head.SignalThreshold.toString.toInt
          println(signalThreshold)
          c.Type.toString match {
            case "inverter" => Inverter.findAll("_id",c.id.toString()).foreach { inv => {
                inv.ConfigID_FK(configID.toString).SignalThreshold(signalThreshold).save()
              }
            }
            case "generator" => c.ConfigID_FK(configID.toString).SignalThreshold(signalThreshold).save()
            case "grid" => Grid.findAll("_id",c.id.toString()).foreach { grid => {
                grid.ConfigID_FK(configID.toString).SignalThreshold(signalThreshold).save()
              }
            }
            case _ =>  {}
          }
        }
      }
    )

    //add empty ConfigGroupID_FK field to already existing Configurations
    Configuration.findAll.foreach { config =>
      if (config.ConfigGroupID_FK.toString.isEmpty) {
        config.ConfigGroupID_FK("").save()
      }
    }

    // for all signals, copy the configID_FK of the associated power source
    // copy the scalingFactorCurrent and scalingFactorVoltage of the associated probe
    Probe.findAll.foreach { probe =>
      probe.Channels.get.foreach { channel =>
        if (channel.SourceID_FK.toString.nonEmpty) {
          val Source = Generator.findAll("_id", channel.SourceID_FK.toString)
          val scalingFactorVoltage = probe.ScalingFactorVoltage.toString.toDouble
          val scalingFactorCurrent = probe.ScalingFactorCurrent.toString.toDouble
          if (Source.nonEmpty) {
            val ConfigID = Source.head.ConfigID_FK
            channel.ConfigID_FK(ConfigID.toString)
          }
          channel.ScalingFactorCurrent(scalingFactorCurrent)
          channel.ScalingFactorVoltage(scalingFactorVoltage)
        }
      }
      //if (probe.ConfigID_FK.toString.nonEmpty) probe.ConfigID_FK(Empty)
      probe.save()
    }


    //for all Configurations, copy the UserID_FK to the AdminID_FK and UserID_FKs
    Configuration.findAll.foreach { config =>
      val adminId = config.UserID_FK.value
      config.AdminID_FK(adminId)
      config.UserID_FKs((adminId :: config.UserID_FKs.value).distinct)
      config.save()
    }
  }
}
