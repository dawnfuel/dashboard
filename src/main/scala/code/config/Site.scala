package code
package config

import code.model.User
import net.liftmodules.mongoauth.{Locs, Permission}
import net.liftweb.http.{RedirectResponse, S}
import net.liftweb.sitemap.Loc._
import net.liftweb.sitemap._
import net.liftmodules.mongoauth.model.Role

object MenuGroups {
  val SettingsGroup = LocGroup("settings")
  val TopBarGroup = LocGroup("topbar")
}

/*
 * Wrapper for Menu locations
 */
case class MenuLoc(menu: Menu) {
  lazy val url: String = S.contextPath+menu.loc.calcDefaultHref
  lazy val fullUrl: String = S.hostAndPath+menu.loc.calcDefaultHref
}

object Site extends Locs {
  import MenuGroups._
  // locations (menu entries)
  val home = MenuLoc(Menu.i("Home") /  "index" )
  val dashboard = MenuLoc(Menu.i("dashboard") / "dashboard" )

  val loggedIn = If(() => User.isLoggedIn,
    () => RedirectResponse("/dashboard"))

  val profile = MenuLoc(Menu.i("profile") / "profile" )
  val electricalProp = MenuLoc(Menu.i("Electrical Properties") / "electrical-properties" )
  val resourcesPage = MenuLoc(Menu.i("Resources") / "resources" )
  val acceptInvoice = MenuLoc(Menu.i("Accept Invoice") /  "system"  /"payments"/ "acceptinvoice" >> RequireLoggedIn )
  val accept = MenuLoc(Menu.i("Accept") /  "system"  /"payments"/ "accept")
  val loginToken = MenuLoc(buildLoginTokenMenu)
  val logout = MenuLoc(Menu.i("Logout") /  "logout" >> RequireLoggedIn)
  private val profileParamMenu = Menu.param[User]("User", "Profile",
    User.findByUsername _,
    _.username.get
  ) / "user" >> Loc.CalcValue(() => User.currentUser)
  lazy val profileLoc = profileParamMenu.toLoc

  val whiteLabelLogin = MenuLoc(Menu.i("login") /  "domains" /  "login" )

  val createconfiggroup = MenuLoc(Menu.i("Create Configuration Group") /  "system" /  "configuration" / "creategroup" >> LocGroup("configconfig") >> RequireLoggedIn >> HasRole("admin"))
  val listconfiggroup = MenuLoc(Menu.i("List Configuration Group") /  "system" /  "configuration" / "listgroup" >> LocGroup("configconfig") >> RequireLoggedIn >> HasRole("admin"))
  val editconfiggroup = MenuLoc(Menu.i("Edit Configuration Group") /  "system" /  "configuration" / "editgroup" >> RequireLoggedIn >> HasRole("admin"))
  val deleteconfiggroup = MenuLoc(Menu.i("Delete Configuration Group") /  "system" /  "configuration" / "deletegroup" >> RequireLoggedIn >> HasRole("admin"))
  val createconf = MenuLoc(Menu("Create Configuration") /  "system" /  "configuration" / "createconfiguration" >> LocGroup("configconfig") >> HasPermission(Permission("config","create")))//>> RequireLoggedIn >> HasRole("admin"))
  val listconf = MenuLoc(Menu("List Configurations") /  "system"  /  "configuration" / "listconfiguration" >> LocGroup("configconfig") >>  RequireLoggedIn >> HasPermission(Permission("config","view") ))//>> RequireLoggedIn >> HasRole("admin"))
  val editconf = MenuLoc(Menu("Edit Configuration") /  "system"  /  "configuration" / "editconfiguration" >>  RequireLoggedIn >> HasPermission(Permission("config","edit") ))// >> HasRole("admin"))
  val viewconf = MenuLoc(Menu("View Configuration") /  "system"  /  "configuration" / "viewconfiguration" >>  RequireLoggedIn >> HasPermission(Permission("config","view") ))// >> HasRole("admin"))
  val deleteconf = MenuLoc(Menu("Delete Configuration") /  "system"  /  "configuration" / "deleteconfiguration" >>  RequireLoggedIn >> HasPermission(Permission("config","delete") ))// >> HasRole("admin"))
  val configMember = MenuLoc(Menu("Configuration Member") /  "system"  /  "configuration" / "configmember" >> LocGroup("configconfig") >>  RequireLoggedIn >> HasAnyRoles(List("user","superuser","regionaladmin")))
  val delconfigMember = MenuLoc(Menu("Delete Config Member") /  "system"  /  "configuration" / "delconfigmember" >>   RequireLoggedIn >> HasAnyRoles(List("user","superuser","regionaladmin")))

  val createprobe = MenuLoc(Menu.i("Create Probe") /  "system"  /  "probe" / "createprobe" >> LocGroup("configprobe") >> RequireLoggedIn >> HasRole("admin") )
  val listprobe = MenuLoc(Menu.i("List Probes") /  "system"  /  "probe" / "listprobe" >> LocGroup("configprobe") >> RequireLoggedIn >> HasPermission(Permission("probe","view")))
  val editprobe = MenuLoc(Menu.i("Edit Probe") /  "system"  /  "probe" / "editprobe" >> RequireLoggedIn >> HasPermission(Permission("probe","edit") )) //>> Hidden ,
  val viewprobe = MenuLoc(Menu.i("View Probe") /  "system"  /  "probe" / "viewprobe" >>  RequireLoggedIn  >>  RequireLoggedIn >> HasPermission(Permission("probe","view") )) //>> Hidden,
  val deleteprobe = MenuLoc(Menu.i("Delete Probe") /  "system"  /  "probe" / "deleteprobe" >> RequireLoggedIn >> HasRole("admin") ) // >> Hidden,
//
  val createsource = MenuLoc(Menu("Create Source") /  "system"  /  "source" / "createsource" >> LocGroup("configsource") >> RequireLoggedIn >> HasPermission(Permission("powerSource","create")))
  val sourcetype = MenuLoc(Menu("type Source") /  "system"  /  "source" / "newsource" >> LocGroup("configsource") >> RequireLoggedIn )
  val listsource = MenuLoc(Menu("List Sources") /  "system"  /  "source" / "listsource" >> LocGroup("configsource") >> RequireLoggedIn >> HasPermission(Permission("powerSource","view")))
  val editsource = MenuLoc(Menu("Edit Source") /  "system"  /  "source" / "editsource" >> RequireLoggedIn >> HasPermission(Permission("powerSource","edit")) )
  val viewsource = MenuLoc(Menu("View Source") /  "system"  /  "source" / "viewsource" >> RequireLoggedIn >> HasPermission(Permission("powerSource","view")) )
  val deletesource = MenuLoc(Menu("Delete Source") /  "system"  /  "source" / "deletesource" >>  RequireLoggedIn >> HasPermission(Permission("powerSource","delete")))

  val testajax = MenuLoc(Menu("AJAX ....") /  "system"  /  "source" / "ajax" >> LocGroup("configsource") >> RequireLoggedIn >> HasRole("admin"))
  val jsonajax = MenuLoc(Menu("AJAX JSON") /  "system"  /  "source" / "ajaxjson" >> LocGroup("configsource") >> RequireLoggedIn >> HasRole("admin"))

  val creategen3 = MenuLoc(Menu("Create Generator 3 Phase") /  "system"  /  "source" / "creategen3" >> RequireLoggedIn >> HasPermission(Permission("powerSource","create")) )
  val creategrid3 = MenuLoc(Menu("Create Grid 3 Phase") /  "system"  /  "source" / "creategrid3" >> RequireLoggedIn >> HasPermission(Permission("powerSource","create")) )
  val createinv3 = MenuLoc(Menu("Create Inverter 3 Phase") /  "system"  /  "source" / "createinv3" >> RequireLoggedIn >> HasPermission(Permission("powerSource","create")))
  val creategen1 = MenuLoc(Menu("Create Generator Single Phase") /  "system"  /  "source" / "creategen1" >> RequireLoggedIn >> HasPermission(Permission("powerSource","create")))
  val creategrid1 = MenuLoc(Menu("Create Grid Single Phase") /  "system"  /  "source" / "creategrid1" >> RequireLoggedIn >> HasPermission(Permission("powerSource","create")) )
  val createinv1 = MenuLoc(Menu("Create Inverter Single Phase") /  "system"  /  "source" / "createinv1" >> RequireLoggedIn >> HasPermission(Permission("powerSource","create")))

  val editgen = MenuLoc(Menu("edit gen") /  "system"  /  "source" / "editgen" >> RequireLoggedIn >> HasPermission(Permission("powerSource","edit")) )
  val editinv = MenuLoc(Menu("edit inv") /  "system"  /  "source" / "editinv" >> RequireLoggedIn >> HasPermission(Permission("powerSource","edit")) )
  val editgrid = MenuLoc(Menu("edit grid") /  "system"  /  "source" / "editgrid" >> RequireLoggedIn >> HasPermission(Permission("powerSource","edit")) )

  val createconsumergroup = MenuLoc(Menu("Create Consumer Group") /  "system"  /  "consumer" / "createconsumergroup" >> LocGroup("configconsumer") >> RequireLoggedIn >> HasPermission(Permission("powerConsumer","create")))
  val editconsumergroup = MenuLoc(Menu("Edit Consumer Group") /  "system"  /  "consumer" / "editconsumergroup" >>  RequireLoggedIn >> HasPermission(Permission("powerConsumer","create")))
  val createconsumerline = MenuLoc(Menu("Create Consumer Line") /  "system"  /  "consumer" / "createconsumerline" >> LocGroup("configconsumer") >> RequireLoggedIn >> HasPermission(Permission("powerConsumer","create")))
  val createconsumerline1 = MenuLoc(Menu("Create 1 phase Consumer Line") /  "system"  /  "consumer" / "createconsumerline1"  >> RequireLoggedIn >> HasPermission(Permission("powerConsumer","create")))
  val createconsumerline3 = MenuLoc(Menu("Create 3 phase Consumer Line") /  "system"  /  "consumer" / "createconsumerline3"  >> RequireLoggedIn >> HasPermission(Permission("powerConsumer","create")))
  val createconsumer = MenuLoc(Menu("Create Consumer") /  "system"  /  "consumer" / "createconsumer" >> LocGroup("configconsumer") >> RequireLoggedIn >> HasPermission(Permission("powerConsumer","create")))
  val consumertype = MenuLoc(Menu("type Consumer") /  "system"  /  "consumer" / "newconsumer" >> LocGroup("configconsumer") >> RequireLoggedIn )
  val listconsumers = MenuLoc(Menu("List Consumers") /  "system"  /  "consumer" / "listconsumers" >> LocGroup("configconsumer") >> RequireLoggedIn >> HasPermission(Permission("powerConsumer","view")))
  val listconsumergroups = MenuLoc(Menu("List Consumer Groups") /  "system"  /  "consumer" / "listconsumergroups" >> LocGroup("configconsumer") >> RequireLoggedIn >> HasPermission(Permission("powerConsumer","view")))
  val editconsumer = MenuLoc(Menu("Edit Consumer") /  "system"  /  "consumer" / "editconsumer" >> RequireLoggedIn >> HasPermission(Permission("powerConsumer","edit")) )
  val viewconsumer = MenuLoc(Menu("View Consumer") /  "system"  /  "consumer" / "viewconsumer" >> RequireLoggedIn >> HasPermission(Permission("powerConsumer","view")) )
  val deleteconsumer = MenuLoc(Menu("Delete Consumer") /  "system"  /  "consumer" / "deleteconsumer" >>  RequireLoggedIn >> HasPermission(Permission("powerConsumer","delete")))
  val deleteconsumergroup = MenuLoc(Menu("Delete Consumer Group") /  "system"  /  "consumer" / "deleteconsumergroup" >>  RequireLoggedIn >> HasPermission(Permission("powerConsumer","delete")))
  val deleteconsumerline = MenuLoc(Menu("Delete Consumer Line") /  "system"  /  "consumer" / "deleteconsumerline" >>  RequireLoggedIn >> HasPermission(Permission("powerConsumer","delete")))


  val password = MenuLoc(Menu.i("Password") / "settings" / "password" >> RequireLoggedIn >> SettingsGroup >> HasPermission(Permission("view","own","account")))
  val account = MenuLoc(Menu.i("Account") / "settings" / "account" >> SettingsGroup >> RequireLoggedIn >> HasPermission(Permission("view","own","account")))
  val editProfile = MenuLoc(Menu("EditProfile", "Profile") / "settings" / "profile" >> RequireLoggedIn >> HasPermission(Permission("view","own","account")))
  val register = MenuLoc(Menu.i("Register") / "register" >> RequireLoggedIn >> HasPermission(Permission("user","view")) )

  val configurations = MenuLoc(Menu.i("Configurations") /  "system" /  "configurations" >> LocGroup("config") >> RequireLoggedIn >> HasRole("admin"))
  val consumers = MenuLoc(Menu.i("Power Consumers") /  "system" / "powerconsumers" >> LocGroup("config") >> RequireLoggedIn )
  val probes = MenuLoc(Menu.i("Probes") /  "system" / "probes" >> LocGroup("config") >> RequireLoggedIn )
  val sources = MenuLoc(Menu.i("Power Sources") /  "system" / "powersources" >> LocGroup("config") >> RequireLoggedIn )

  val listusergroups = MenuLoc(Menu.i("List User Groups") /  "system" / "usergroup" / "listgroup" >> LocGroup("usergroup") >> RequireLoggedIn >> HasRole("admin")  )
  val createusergroup = MenuLoc(Menu.i("Create New User Group") /  "system" / "usergroup" / "creategroup" >> LocGroup("usergroup") >> RequireLoggedIn >> HasRole("admin")  )
  val deleteusergroup = MenuLoc(Menu.i("Delete User Group") /  "system" / "usergroup" / "deletegroup" >> RequireLoggedIn >> HasRole("admin") )
  val editusergroup = MenuLoc(Menu.i("Edit/View User Group") /  "system" / "usergroup" / "editgroup" >> RequireLoggedIn >> HasRole("admin") )
  val edituser = MenuLoc(Menu.i("Edit/View User") /  "system" / "usergroup" / "edituser" >> RequireLoggedIn >> HasRole("admin") )
  val editrole = MenuLoc(Menu.i("Edit/View Role") /  "system" / "usergroup" / "editrole" >> RequireLoggedIn >> HasRole("admin") )
  val createrole = MenuLoc(Menu.i("Create New Role") /  "system" / "usergroup" / "createrole" >> RequireLoggedIn >> HasRole("admin") )
  val deleteuser = MenuLoc(Menu.i("Delete/View User") /  "system" / "usergroup" / "deleteuser" >> RequireLoggedIn >> HasRole("admin") )
  val deleterole = MenuLoc(Menu.i("Delete/View Role") /  "system" / "usergroup" / "deleterole" >> RequireLoggedIn >> HasRole("admin") )
  val listusers = MenuLoc(Menu.i("List Users") /  "system" / "usergroup" / "listusers" >> LocGroup("usergroup")  >> RequireLoggedIn >> HasPermission(Permission("user","view"))  )
  val listdomain = MenuLoc(Menu.i("List Domains") /  "system" / "usergroup" / "listdomain" >> LocGroup("usergroup") >> RequireLoggedIn >> HasRole("admin")  )
  val createdomain = MenuLoc(Menu.i("Create Domain") /  "system" / "usergroup" / "createdomain" >> LocGroup("usergroup") >> RequireLoggedIn >> HasRole("admin")  )
  val editdomain = MenuLoc(Menu.i("Edit Domain") /  "system" / "usergroup" / "editdomain" >>  RequireLoggedIn >> HasRole("admin") )
  val deletedomain = MenuLoc(Menu.i("Delete Domain") /  "system" / "usergroup" / "deletedomain" >> RequireLoggedIn >> HasRole("admin")  )

  val about = MenuLoc(Menu.i("About ") /  "gritsytems" /  "about"  >> LocGroup("grit") >> RequireNotLoggedIn >> TopBarGroup )
  val help = MenuLoc(Menu.i("Help") /  "gritsytems" /  "help"  >> LocGroup("grit") >> RequireNotLoggedIn >> TopBarGroup )
  val contact = MenuLoc(Menu.i("Contact ") /  "gritsystems" /  "contact"  >> LocGroup("grit") >> RequireNotLoggedIn >> TopBarGroup )
  val preorder = MenuLoc(Menu.i("Bills") /  "gritsystems" /  "bills"  >> LocGroup("grit") >> RequireLoggedIn >> TopBarGroup )
  val demo = MenuLoc(Menu.i("Demo ") /  "grit" /  "demo"  >> LocGroup("grit") >> TopBarGroup >> RequireNotLoggedIn )
  val getGrit = MenuLoc(Menu.i("Get Grit ") / "get-grit" >> RequireNotLoggedIn )
  val gettingStarted = MenuLoc(Menu.i("Getting Started ") / "getting-started" >> RequireNotLoggedIn )
  val careers = MenuLoc(Menu.i("Careers ") / "careers" )


  val about_drop = MenuLoc(Menu.i("About") /  "about"  >> LocGroup("side"))
  val help_drop = MenuLoc(Menu.i("Help") /  "help"  >> LocGroup("side"))
  val contact_drop = MenuLoc(Menu.i("Contact") /  "contact"  >> LocGroup("side")  )
  val preorder_drop = MenuLoc(Menu.i("Bills") /  "bills"  >> LocGroup("side")  >> RequireLoggedIn )
  val demo_drop = MenuLoc(Menu.i("Demo") /  "demo" >> RequireNotLoggedIn)

  val charts = MenuLoc(Menu.i("Charts") /  "data" /  "charts"  >> LocGroup("data") >> RequireLoggedIn >> TopBarGroup )
  val reports = MenuLoc(Menu.i("Reports") /  "data" /  "reports"  >> LocGroup("data") >> RequireLoggedIn >> TopBarGroup )
  val notifier = MenuLoc(Menu.i("Notifications") /  "data" /  "notify"  >> LocGroup("data") >> RequireLoggedIn >> HasRole("admin") >> TopBarGroup)
  val export = MenuLoc(Menu.i("Export") /  "data" /  "export"  >> LocGroup("data") >> RequireLoggedIn >> HasRole("admin"))// >> TopBarGroup)
  val maps = MenuLoc(Menu.i("Maps") /  "data" /  "maps"  >> LocGroup("data") >> RequireLoggedIn >> HasRole("admin") >> TopBarGroup)
  val auto = MenuLoc(Menu.i("Automator") /  "data" /  "auto"  >> LocGroup("data") >> RequireLoggedIn >> HasRole("admin") >> TopBarGroup )

  val power = MenuLoc(Menu("Power") /  "data"  /  "charts" / "power" >> LocGroup("charts") >> RequireLoggedIn )
  val resources = MenuLoc(Menu("Resources") /  "data"  /  "charts" / "resources" >> LocGroup("charts") >> RequireLoggedIn >> HasRole("admin"))
  val triphase = MenuLoc(Menu("Tri-phase") /  "data"  /  "charts" / "triphase" >> LocGroup("charts") >> RequireLoggedIn >> HasRole("admin") )
  val diagnostic = MenuLoc(Menu("Diagnostic") /  "data"  /  "charts" / "diagnostic" >> LocGroup("charts") >> RequireLoggedIn >> HasRole("admin") )
  val trends = MenuLoc(Menu("Trends") /  "data"  /  "charts" / "trends" >> LocGroup("charts") >> RequireLoggedIn >> HasRole("admin") )
  val timeenergy_ch = MenuLoc(Menu("Time / Energy ") /  "data"  /  "charts" / "timeenergy" >> LocGroup("charts") >> RequireLoggedIn  )

  val timeenergy_res = MenuLoc(Menu("Time / Energy ") /  "data"  /  "reports" / "timeenergy" >> LocGroup("reports") >> RequireLoggedIn >> HasRole("admin") )
  val source_log = MenuLoc(Menu("Power Source Log") /  "data"  /  "reports" / "sourcelog" >> LocGroup("reports") >> RequireLoggedIn  )
  val resource_log = MenuLoc(Menu("Resource Source Log") /  "data"  /  "reports" / "resourcelog" >> LocGroup("reports") >> RequireLoggedIn >> HasRole("admin") )
  val maintenance = MenuLoc(Menu("Logs ") /  "data"  /  "reports" / "maintenance" >> LocGroup("reports") >> RequireLoggedIn >> HasRole("admin") )

  val low_voltage = MenuLoc(Menu("Voltage") /  "data"  /  "notify" / "voltage" >> LocGroup("notify") >> RequireLoggedIn >> HasRole("admin") )
  val power_factor = MenuLoc(Menu("Power Factor") /  "data"  /  "notify" / "powerfactor" >> LocGroup("notify") >> RequireLoggedIn >> HasRole("admin") )
  val phase = MenuLoc(Menu("Power Threshold") /  "data"  /  "notify" / "phase" >> LocGroup("notify") >> RequireLoggedIn >> HasRole("admin") )
  val power_threshold = MenuLoc(Menu("Voltage") /  "data"  /  "notify" / "powerthreshold" >> LocGroup("notify") >> RequireLoggedIn >> HasRole("admin") )
  val window = MenuLoc(Menu("Operating Window") /  "data"  /  "notify" / "window" >> LocGroup("notify") >> RequireLoggedIn >> HasRole("admin") )
  val dod = MenuLoc(Menu("Depth of Discharge") /  "data"  /  "notify" / "dod" >> LocGroup("notify") >> RequireLoggedIn >> HasRole("admin") )

  val dmd = MenuLoc(Menu("Data Demand File [HOMER NREL]") /  "data"  /  "export" / "dmd" >> LocGroup("export") >> RequireLoggedIn >> HasRole("admin") )
  val xml = MenuLoc(Menu("XML") /  "data"  /  "export" / "xml" >> LocGroup("export") >> RequireLoggedIn >> HasRole("admin") )
  val csv = MenuLoc(Menu("CSV") /  "data"  /  "export" / "csv" >> LocGroup("export") >> RequireLoggedIn >> HasRole("admin") )

  val simple = MenuLoc(Menu.i("SIMPLE") /  "simple" /  "item" / ":itemId"  >> LocGroup("side") )

  //fuel options menu
  val listfuel = MenuLoc(Menu.i("List Fuel") /  "system"  /"fuel"/  "listfuel" >> LocGroup("fuel") >> RequireLoggedIn >> HasPermission(Permission("fuel","view")) )
  val createfuel = MenuLoc(Menu("Create Fuel") /  "system" / "fuel" /  "createfuel" >> LocGroup("fuel") >> RequireLoggedIn >> HasPermission(Permission("fuel","create")))
  val editfuel = MenuLoc(Menu("Edit Fuel") /  "system"  / "fuel" /  "editfuel" >> RequireLoggedIn >> HasPermission(Permission("fuel","edit")) )
  val delefuel = MenuLoc(Menu("Delete Fuel") /  "system" / "fuel" /  "deletefuel" >> RequireLoggedIn >> HasPermission(Permission("fuel","delete")) )
  val viewfuel = MenuLoc(Menu("View Fuel") /  "system"  / "fuel" /  "viewfuel" >>  RequireLoggedIn >> HasPermission(Permission("fuel","view")) )
  /*val refillfuel = MenuLoc(Menu("Refill Tank") /  "system"  / "fuel" /  "refill" >> LocGroup("fuel") >>  RequireLoggedIn >> HasPermission(Permission("fuel","edit")) )*/

  val configPayments = MenuLoc(Menu.i("Payment Settings") /  "system"  /"payments"/  "paymentsettings" >> LocGroup("payments") >> RequireLoggedIn >> HasPermission(Permission("payment","view")) )
  val listInvoices = MenuLoc(Menu.i("List Invoices") /  "system"  /"payments"/  "listinvoices" >> LocGroup("payments") >> RequireLoggedIn >> HasPermission(Permission("payment","view")) )
  val createInvoice = MenuLoc(Menu.i("Create Invoice") /  "system"  /"payments"/ "createinvoice" >> LocGroup("payments") >> RequireLoggedIn >> HasPermission(Permission("payment","create")) )
  val editInvoice = MenuLoc(Menu.i("Edit Invoice") /  "system"  /"payments"/ "editinvoice" >> RequireLoggedIn >> HasPermission(Permission("payment","edit")) )
  val deleteInvoice = MenuLoc(Menu.i("Delete Invoice") /  "system"  /"payments"/ "deleteinvoice" >> RequireLoggedIn >> HasPermission(Permission("payment","delete")) )
  val listAccounts = MenuLoc(Menu.i("List Accounts") /  "system"  /"payments"/  "listaccounts" >> LocGroup("payments") >> RequireLoggedIn >> HasPermission(Permission("payment","view")) )
  val createAccount = MenuLoc(Menu.i("Create Account") /  "system"  /"payments"/ "createaccount" >> LocGroup("payments") >> RequireLoggedIn >> HasPermission(Permission("payment","create")) )
  val editAccount = MenuLoc(Menu.i("Edit Account") /  "system"  /"payments"/ "editaccount" >> RequireLoggedIn >> HasPermission(Permission("payment","edit")) )
  val deleteAccount = MenuLoc(Menu.i("Delete Account") /  "system"  /"payments"/ "deleteaccount" >> RequireLoggedIn >> HasPermission(Permission("payment","delete")) )

  val listDevices = MenuLoc(Menu.i("Devices") /  "system"  /"products"/  "listdevices" >> LocGroup("products") >> RequireLoggedIn >> HasRole("admin") )
  val createDevice = MenuLoc(Menu.i("Create Device") /  "system"  /"products"/ "createdevice" >> LocGroup("products") >> RequireLoggedIn >> HasRole("admin") )
  val editDevice = MenuLoc(Menu.i("Edit Device") /  "system"  /"products"/ "editdevice" >> RequireLoggedIn >> HasRole("admin") )
  val deleteDevice = MenuLoc(Menu.i("Delete Device") /  "system"  /"products"/ "delete-device" >>  RequireLoggedIn >> HasRole("admin") )
  val sensor = MenuLoc(Menu.i("Sensors") /  "system"  /"products"/  "listsensors" >> LocGroup("products") >> RequireLoggedIn >> HasRole("admin") )
  val createSensor = MenuLoc(Menu.i("Create Sensors") /  "system"  /"products"/ "createsensor" >>  LocGroup("products")  >> RequireLoggedIn >> HasRole("admin") )
  val editSensor = MenuLoc(Menu.i("Edit Sensors") /  "system"  /"products"/ "editsensor" >> RequireLoggedIn >> HasRole("admin") )
  val deleteSensor = MenuLoc(Menu.i("Delete Sensors") /  "system"  /"products"/ "deletesensor" >> RequireLoggedIn >> HasRole("admin") )
  val listProducts = MenuLoc(Menu.i("Products") /  "system"  /"products"/  "listproducts" >> LocGroup("products") >> RequireLoggedIn >> HasRole("admin") )
  val createProduct = MenuLoc(Menu.i("Create Product") /  "system"  /"products"/ "createproduct" >> LocGroup("products") >> RequireLoggedIn >> HasRole("admin") )
  val editProduct = MenuLoc(Menu.i("Edit Product") /  "system"  /"products"/ "editproduct" >> RequireLoggedIn >> HasRole("admin") )
  val deleteProduct = MenuLoc(Menu.i("Delete Product") /  "system"  /"products"/ "deleteproduct" >> RequireLoggedIn >> HasRole("admin") )

  val commDevice = MenuLoc(Menu.i("Communication Device") /  "system"  /"products"/  "list_comm_device" >> LocGroup("products") >> RequireLoggedIn >> HasRole("admin") )
  val createCommDevice = MenuLoc(Menu.i("Create Communication Device") /  "system"  /"products"/ "create_comm_device" >>  LocGroup("products")  >> RequireLoggedIn >> HasRole("admin") )
  val editCommDevice = MenuLoc(Menu.i("Edit Communication Device") /  "system"  /"products"/ "edit_comm_device" >> RequireLoggedIn >> HasRole("admin") )
  val deleteCommDevice = MenuLoc(Menu.i("Delete Communication Device") /  "system"  /"products"/ "delete_comm_device" >> RequireLoggedIn >> HasRole("admin") )

  //for uploading probeUpdate file
  val probeUpdate = MenuLoc(Menu.i("upload update") / "uploadUpdate" >> RequireLoggedIn >> HasPermission(Permission("config","delete") ))

  val services = MenuLoc(Menu.i("Our Services")/ "services")
  //Notification Configuration
  val notification = MenuLoc(Menu.i("notification Configuration") / "system" / "configuration" / "notification" >> RequireLoggedIn)

  private def menus = List(
    whiteLabelLogin.menu,
    home.menu,
    profile.menu,
    electricalProp.menu,
    resourcesPage.menu,
    simple.menu,
    Menu.i("Login") / "login" >> RequireNotLoggedIn,
    export.menu,
    configPayments.menu,
    sensor.menu,
    createSensor.menu,
    editSensor.menu,
    deleteSensor.menu,
    createInvoice.menu,
    listInvoices.menu,
    editInvoice.menu,
    deleteInvoice.menu,
    acceptInvoice.menu,
    accept.menu,
    createAccount.menu,
    listAccounts.menu,
    editAccount.menu,
    deleteAccount.menu,
    listDevices.menu,
    createDevice.menu,
    editDevice.menu,
    deleteDevice.menu,
    listProducts.menu,
    createProduct.menu,
    editProduct.menu,
    deleteProduct.menu,
    commDevice.menu,
    createCommDevice.menu,
    editCommDevice.menu,
    deleteCommDevice.menu,
    about_drop.menu,
    help_drop.menu,
    contact_drop.menu,
    preorder_drop.menu,
    demo_drop.menu,
    register.menu,
    careers.menu,
    loginToken.menu,
    logout.menu,
    profileParamMenu,
    account.menu,
    password.menu,
    editProfile.menu,
    createconfiggroup.menu,
    editconfiggroup.menu,
    deleteconfiggroup.menu,
    listconfiggroup.menu,
    createconf.menu,
    listconf.menu,
    editconf.menu,
    viewconf.menu,
    deleteconf.menu,
    configMember.menu,
    delconfigMember.menu,
    createprobe.menu,
    listprobe.menu,
    editprobe.menu,
    viewprobe.menu,
    deleteprobe.menu,
    listsource.menu,
    createsource.menu,
    creategen3.menu,
    creategrid3.menu,
    createinv3.menu,
    creategen1.menu,
    creategrid1.menu,
    createinv1.menu,
    editsource.menu,
    viewsource.menu,
    deletesource.menu,
    configurations.menu,
    probes.menu,
    sources.menu,
    listusers.menu,
    listusergroups.menu,
    createusergroup.menu,
    deleteusergroup.menu,
    editusergroup.menu,
    createdomain.menu,
    editdomain.menu,
    deletedomain.menu,
    listdomain.menu,
    edituser.menu,
    editrole.menu,
    createrole.menu,
    deleteuser.menu,
    deleterole.menu,
    consumers.menu,
    listconsumers.menu,
    createconsumergroup.menu,
    editconsumergroup.menu,
    createconsumerline.menu,
    createconsumerline1.menu,
    createconsumerline3.menu,
    createconsumer.menu,
    editconsumer.menu,
    viewconsumer.menu,
    deleteconsumer.menu,
    deleteconsumerline.menu,
    deleteconsumergroup.menu,
    listfuel.menu,
    createfuel.menu,
    editfuel.menu,
    delefuel.menu,
    viewfuel.menu,
    /*refillfuel.menu,*/
    probeUpdate.menu,
    getGrit.menu,
    gettingStarted.menu,
    dashboard.menu,
    editgen.menu,editgrid.menu,editinv.menu,
    notification.menu,
    services.menu,

    Menu.i("Error") / "error" >> Hidden,
    Menu.i("404") / "404" >> Hidden,
    Menu.i("Throw") / "throw"  >> EarlyResponse(() => throw new Exception("This is only a test."))
  )

  /*
   * Return a SiteMap needed for Lift
   */
  def siteMap: SiteMap = SiteMap(menus:_*)
}
