$(document).ready(function() {

    /* This section's vars are related to the export-data functionality. */
    var thisDay = new Date();
    var thisTime = new Date(thisDay.getFullYear(), thisDay.getMonth(), thisDay.getDate(), 0, 1, 30);
    var exportStartTime = moment(thisTime).format("YYYY-MM-DDTHH:mm:ss.SSSZ");
    var exportEndTime = moment(new Date(thisTime.getFullYear(), thisTime.getMonth(),thisTime.getDate(), 23, 59, 59)).format("YYYY-MM-DDTHH:mm:ss.SSSZ");
    var isHiddenFetchDataContainer = false;
    var currentCsv = "";

    $('#datetimepickerStart').datetimepicker({
        defaultDate: exportStartTime
    });
    $('#datetimepickerStop').datetimepicker({
        defaultDate: exportEndTime
    });

    /* This subsection is for setting up the client for the Google Sheets API.
    NB: Some functions and variables named here are defined further down or up. */
    var hasGrantedGooglePermissions = false;

    // Client ID and API key from the Developer Console.
    var CLIENT_ID = "1077004075548-rntpjmep485gtdm5mgqd4oeb8lek9rif.apps.googleusercontent.com";

    // Array of API discovery doc URLs for APIs used by the quickstart
    var DISCOVERY_DOCS = ["https://sheets.googleapis.com/$discovery/rest?version=v4"];

    /* Authorization scopes required by the Sheets API; multiple scopes can be included, separated by spaces.
    See https://developers.google.com/sheets/api/guides/authorizing for more options. */
    var SCOPES = "https://www.googleapis.com/auth/spreadsheets";

    /* On load, called to load the auth2 library and API client library. */
    function handleClientLoad() {
        gapi.load('client:auth2', initClient);
    }

    /* Initializes the API client library and sets up sign-in state listeners. */
    function initClient() {
        gapi.client.init({
            discoveryDocs: DISCOVERY_DOCS,
            clientId: CLIENT_ID,
            scope: SCOPES
        }).then(function() {
            // Specify the function to call each time the sign in state changes.
            gapi.auth2.getAuthInstance().isSignedIn.listen(updateGooglePermissionsStatus);

            // Handle the initial sign-in state.
            hasGrantedGooglePermissions = gapi.auth2.getAuthInstance().isSignedIn.get();
        });
    }

    /* Had to call this directly instead of using <script src="https://apis.google.com/js/api.js?onload=handleClientLoad></script> or
     such code because they weren't working. Why? All the code in this file is within a $(document).ready();, so the variables and functions
     declared herein are local and not accessible outside. */
    handleClientLoad();

    /* End */

    $('.alert').on('close.bs.alert', function(e) {
        e.preventDefault();
        $(this).hide();
    });
    //changing the compress and enlarge icons
    $("a i").click(function() {
        if ($(this).hasClass('fa-compress')) {

            $(this).removeClass('fa-compress');
            $(this).addClass('fa-expand');
        } else if ($(this).hasClass('fa-expand')) {
            $(this).removeClass('fa-expand');
            $(this).addClass('fa-compress');
        }
    });

    //color picker
    $(function() {
        $('#cp4').colorpicker().on('changeColor', function(e) {
            $('.grid .tick').css('stroke', e.color.toHex());
            $(' .domain').css('stroke',e.color.toHex());
        });
        $('#fontChange').change(function () {
        var fontSize = $( "#fontChange  option:selected" ).text();
        $('svg').css("font-size", fontSize);
        });
    });

    $('#profileSettings').submit(function(e){
        e.preventDefault();
    });
    $('#text').val($('#colorinput').val());
    $('#text').on ('change  keyup paste' , function() {
      $('#colorinput').val($('#text').val());
      $('.color-picker').attr('data-value', $('#text').val());
      $('.paper-button').css('background-color', $('#text').val());
      $('.graph').each(function () {
            this.style.backgroundColor = $('#text').val();
        });
    });

      $(document).click(function(event){
        var clicked = event.target;
        if (!$(clicked).parents('.color-picker').length  && !$(event.target).is('.ripple')) {
          $('.translate').css('display','none' );
          $('.color-picker').css('transform', 'scale(0,0)');
        }
      });

    $('#colorinput').change(function() {
      $('#text').val($(this).val().toUpperCase());
      $('.color-picker').attr('data-value', $(this).val().toUpperCase());
    });

    $('.paper-button').click(function() {
      var cx = $('.paper-button').position().left;
      var transform = cx;
      $('.translate').css('display','block' );
      $('.translate').css("transform", 'translate(' + transform + 'px)');
      $('.color-picker').css("transform", "scale(1,1)");
      // $(this).addClass('opened');
    });

    $('#close').click(function() {
      $('.translate').css('display','none' );
      $('.color-picker').css('transform', 'scale(0,0)');
    });

    function rgb2hex(rgb) {
        rgb = rgb.match(/^rgb\((\d+),\s*(\d+),\s*(\d+)\)$/);
        function hex(x) {
            return ("0" + parseInt(x).toString(16)).slice(-2);
        }
        return "#" + hex(rgb[1]) + hex(rgb[2]) + hex(rgb[3]);
    }

//    function setExportDateTimePicker(startTime,stopTime) {
//        $('#datetimepickerStart').datetimepicker({
//            defaultDate: startTime
//        });
//        $('#datetimepickerStop').datetimepicker({
//            defaultDate: stopTime
//        });
//    }

    function setExportDateTimePicker (startTime,stopTime) {
        $('#datetimepickerStart').data("DateTimePicker").date(new Date(startTime));
         $('#datetimepickerStop').data("DateTimePicker").date(new Date(stopTime));
    }

    Array.prototype.sortUnique = function() {
        this.sort();
        var last_i;
        for (var i=0;i<this.length;i++)
            if ((last_i = this.lastIndexOf(this[i])) !== i)
                this.splice(i+1, last_i-i);
        return this;
    };

    $('.color-picker > div:not(#close)').click(function() {
      var bg = $(this).css('background-color');
      $('.paper-button').css('background-color', bg);
      $('.graph').each(function () {
            this.style.backgroundColor = bg;
        });
      $('#colorinput').val(rgb2hex($(this).css('background-color')));
      $('#colorinput').change();
    });

    var dayRealtime = false;
    var minRealTime = false;
    var energySwitch = true;
    var powerBrushStatus = false;
    var day;
    var dataDaily = [];
    var DataDaily = [];
    var datearray = [];
    var PowerSources;
    var barWidth;
    var monthGraphView;

    $("#EnergyCharts").on('setSources', function(e, dt) {
        PowerSources = dt.data[0].Sources;
        console.log(PowerSources);
    });

    var margin = {
            top: 10,
            right: 40,
            bottom: 50,
            left: 60
        },
        height = 300 - margin.top - margin.bottom,
        height2 = 500 - margin.top - margin.bottom,
        height3 = 400 - margin.top - margin.bottom;

        try {
            width = parseInt(d3.select('#real_live').style('width'), 10);
            width2 = parseInt(d3.select('#real_live').style('width'), 10);
        } catch (err) {
            width = parseInt(d3.select('#tab-Energy').style('width'), 10);
            width2 = parseInt(d3.select('#tab-Energy').style('width'), 10);
        }

        try {
            widthFuel = ( parseInt(d3.select('#fuel_graph').style('width'), 10) ? parseInt(d3.select('#fuel_graph').style('width'), 10)  : parseInt(d3.select('#capGraph').style('width'), 10));
        }catch(err) {
            console.log(err.message);
            widthFuel = width2/2;
        }

    var margin2 = {
        top: 10,
        right: 80,
        bottom: 50,
        left: 10
    }; //margin2 = {top: 430, right: 80, bottom: 30, left: 40};
    var pieSrc1;
    var pieSrc2, pieSrc3, pieSrc4;
    var ConfInner = {
        header: {
            title: {
                text: "",
                "color": "gray",
                "fontSize": 15,
            },
            location: "top-center"
        },
        size: {
            canvasHeight: 400,
            canvasWidth: 400,
            pieInnerRadius: "10%",
            pieOuterRadius: "35%"
        },
        misc: {
            pieCenterOffset: {
                x: 0,
                y: 0
            }
        },
        data: {
            sortOrder: "none",
            enabled: true,
            valueType: "percentage",
            content: []
        },
        labels: {
            percentage: {
                color: "gray"
            },
            mainLabel: {
                color: "white"
            },
            outer: {
                format: "label",
                hideWhenLessThanPercentage: null,
                pieDistance: 30
            },
            inner: {
                format: "percentage",
                hideWhenLessThanPercentage: null
            }
        },
        effects: {
            pullOutSegmentOnClick: {
                effect: "bounce",
                speed: 300,
                size: 10
            },
        },
        tooltips: {
            enabled: true,
            type: "placeholder", // caption|placeholder,
            string: "Energy Consumption: {value} Watt Hours"
        }
    };
    var Conf = {
        header: {
            title: {
                text: "",
                "color": "gray",
                "fontSize": 15,
            },
            location: "top-center"
        },
        size: {
            canvasHeight: 400,
            canvasWidth: 400,
            pieInnerRadius: "50%",
        },
        data: {
            sortOrder: "none",
            enabled: true,
            valueType: "percentage",
            content: [] //energy_data
        },
        misc: {
            pieCenterOffset: {
                x: 0,
                y: 0
            }
        },
        labels: {
            percentage: {
                color: "gray"
            },
            mainLabel: {
                color: "white"
            }
        },
        effects: {
            pullOutSegmentOnClick: {
                effect: "bounce",
                speed: 300,
                size: 10
            },
        },
        tooltips: {
            enabled: true,
            type: "placeholder", // caption|placeholder,
            string: "Energy Consumption: {value} Watt Hours"
        }
    };

    var ConfConsumer = {
        header: {
            title: {
                text: "",
                "color": "gray",
                "fontSize": 25,
            },
            location: "top-center"
        },
        size: {
            canvasHeight: 600,
            canvasWidth: 600
        },
        data: {
            sortOrder: "none",
            enabled: true,
            valueType: "percentage",
            content: [] //energy_data
        },
        misc: {
            pieCenterOffset: {
                x: 0,
                y: 0
            }
        },
        labels: {
            outer: {
                "pieDistance": 20
                },
            percentage: {
                color: "gray",
                fontSize : 12.5
            },
            mainLabel: {
                color: "white",
                fontSize : 12.5
            }
        },
        effects: {
            pullOutSegmentOnClick: {
                effect: "bounce",
                speed: 300,
                size: 10
            },
        },
        tooltips: {
            enabled: true,
            type: "placeholder", // caption|placeholder,
            string: "Energy Consumption: {value} Watt Hours"
        }
    };

    function getTimeData(typeTime, nameTime, typeColor) {

        var time_type_data = [];
        for (i = 0; i < typeTime.length; i++) {
            time_type_data.push({
                "label": typeTime[i].key,
                "value": roundTo(typeTime[i].doc_count * 5),
                "color": color[typeColor.indexOf(typeTime[i].key)]
            });
        }

        var time_name_data = [];
        for (i = 0; i < nameTime.length; i++) {
            time_name_data.push({
                "label": nameTime[i].key,
                "value": roundTo(nameTime[i].doc_count * 5),
                "color": color[typeColor.indexOf(nameTime[i].key)]
            });
        }

        var output = {};
        output.time_type_data = time_type_data;
        output.time_name_data = time_name_data;

        return output;

    }

    function getConsumerData (consumerEnergy,consumerCost) {
        var consumerEnergyData = [];
        var consumerCostData = [];
        var colorScheme =  d3.scale.category20().range().concat(d3.scale.category20b().range()).sortUnique() ;
        for (i = 0; i < consumerEnergy.length; i++) {
            consumerEnergyData.push({
                "label": consumerEnergy[i].energyConsumer.buckets[0].key,
                "value": roundTo(consumerEnergy[i].energyConsumer.buckets[0].energyStats.sum),
                "color" : colorScheme[i]
            });
        }
        for (i = 0; i < consumerCost.length; i++) {
            consumerCostData.push({
                "label": consumerCost[i].costConsumer.buckets[0].key,
                "value": roundTo(consumerCost[i].costConsumer.buckets[0].costStats.sum),
                "color" : colorScheme[i]
            });
        }
        var consumerOutput = {};
        consumerOutput.cost = consumerCostData;
        consumerOutput.energy = consumerEnergyData;
        return consumerOutput;
    }

    function getConsumerGroupData (consumerGroupEnergy,consumerGroupCost) {
        var consumerEnergyData = [];
        var consumerCostData = [];
        var sum = {};
         consumerGroupEnergy.map(function (data) {

              data.consumerEnergy.buckets.map( function (d) {
                    if (data.doc_count !== 0 && d.energyConsumer.buckets[0].energyStats.sum !== null) {
                        sum[d.energyConsumer.buckets[0].key] = sum[d.energyConsumer.buckets[0].key] ? sum[d.energyConsumer.buckets[0].key] + d.energyConsumer.buckets[0].energyStats.sum : d.energyConsumer.buckets[0].energyStats.sum;
                        consumerEnergyData.push ({
                            "label" : d.energyConsumer.buckets[0].key ,
                            "date"  : parseTimeISO(moment(data.key).format("YYYY-MM-DD HH:mm:ss")),
                            "value" : sum[d.energyConsumer.buckets[0].key]
                        });
                    }
              });
         });

          consumerGroupCost.map(function (data) {
               data.consumerCost.buckets.map( function (d) {
                     if (data.doc_count !== 0 && d.costConsumer.buckets[0].costStats.sum !== null) {
                         consumerCostData.push ({
                             "label" : d.costConsumer.buckets[0].key ,
                             "date"  : parseTimeISO(moment(data.key).format("YYYY-MM-DD HH:mm:ss")),
                             "value" : d.costConsumer.buckets[0].costStats.sum
                         });
                     }
               });
          });

         var consumerOutput = {};
         consumerOutput.cost = consumerCostData;
         consumerOutput.energy = consumerEnergyData;
         return consumerOutput;
    }

    function getCostData(costType, costName, typeColor) {

        var cost_type_data = [];
        for (i = 0; i < costType.length; i++) {
            cost_type_data.push({
                "label": costType[i].key,
                "value": roundTo(costType[i].TimeStats.sum),
                "color": color[typeColor.indexOf(costType[i].key)]
            });
        }

        var cost_name_data = [];
        for (i = 0; i < costName.length; i++) {
            cost_name_data.push({
                "label": costName[i].key,
                "value": roundTo(costName[i].TimeStats.sum),
                "color": color[typeColor.indexOf(costName[i].key)]
            });
        }

        var output = {};
        output.cost_type_data = cost_type_data;
        output.cost_name_data = cost_name_data;

        return output;

    }

    function _getEnergyData (energyType, energyName, typeColor) {
        var energy_type_data = [];
        for (i = 0; i < energyType.length; i++) {
            energy_type_data.push({
                "label": energyType[i].key,
                "value": roundTo(energyType[i].Stats.sum),
                "color": color[typeColor.indexOf(energyType[i].key)]
            });
        }

        var energy_name_data = [];
        for (i = 0; i < energyName.length; i++) {
            energy_name_data.push({
                "label": energyName[i].key,
                "value": roundTo(energyName[i].Stats.sum),
                "color": color[typeColor.indexOf(energyName[i].key)]
            });
        }

        var output = {};
        output.energy_name_data = energy_name_data;
        output.energy_type_data = energy_type_data;
        return output;

    }

    function getEnergyDataJS(energyTypeJS,energyNameJS,typeColor) {
        var energy_type_dataJS = [];
        Object.keys(energyTypeJS).map(function(t){
            energy_type_dataJS.push({
                "label": t,
                "value": roundTo(energyTypeJS[t]),
                "color": color[typeColor.indexOf(t)]
            });
        });

        var energy_name_dataJS = [];
        Object.keys(energyNameJS).map(function(t){
            energy_name_dataJS.push({
                "label": t.toLowerCase(),
                "value": roundTo(energyNameJS[t]),
                "color": color[typeColor.indexOf(t)]
            });
        });
        var output = {};

        output.energy_name_dataJS = energy_name_dataJS;
        output.energy_type_dataJS = energy_type_dataJS;
        return output;
    }

    function getCostPerEnergy(costType,costName,energyType,energyName,typeColor) {
        var cost_energy_type_data = [];
        for (i = 0; i < energyType.length; i++) {
            for (j = 0; j < costType.length; j++) {
                if ((energyType[i].key === costType[j].key) && (energyType[i].Stats.sum > 0)) {
                    cost_energy_type_data.push({
                        "label": energyType[i].key,
                        "value": roundTo(costType[j].TimeStats.sum/(energyType[i].Stats.sum/1000)),
                        "color": color[typeColor.indexOf(energyType[i].key)]
                    });
                }
            }
        }

        var cost_energy_name_data = [];
        for (i = 0; i < energyName.length; i++) {
            for (j = 0; j < costName.length; j++) {
                if ((energyName[i].key === costName[j].key) && (energyName[i].Stats.sum > 0)) {
                    cost_energy_name_data.push({
                        "label": energyName[i].key,
                        "value": roundTo(costName[j].TimeStats.sum/(energyName[i].Stats.sum/1000)),
                        "color": color[typeColor.indexOf(energyName[i].key)]
                    });
                }
            }
        }

        var output = {};
        output.cost_energy_name_data = cost_energy_name_data;
        output.cost_energy_type_data = cost_energy_type_data;
        return output;
    }

    function sortData(SrcNameByType, SrcByType, SrcName_energy_data, energy_data, time_name_data, time_type_data, cost_name_data, cost_type_data,nameTime, typeColor,cost_energy_name_data,cost_energy_type_data,energy_name_data_JS,energy_type_data_JS) {
        var time_name_data_sorted = [];
        var time_type_data_sorted = [];
        var nameTime_sorted = [];
        var cost_type_data_sorted = [];
        var cost_name_data_sorted = [];
        var power_type_data_sorted = [];
        var power_name_data_sorted = [];
        var energy_type_data_sorted = [];
        var energy_name_data_sorted = [];
        var cost_energy_type_data_sorted = [];
        var cost_energy_name_data_sorted = [];
        var energy_name_data_sortedJS = [];
        var energy_type_data_sortedJS = [];

        for (stype = 0; stype < SrcNameByType.length; stype++) {
            col = color[typeColor.indexOf(SrcByType[stype])].toString();
            for (sname = 0; sname < SrcNameByType[stype].length; sname++) {

                for (sname_data = 0; sname_data < time_name_data.length; sname_data++) {
                    if (SrcNameByType[stype][sname] == time_name_data[sname_data].label) {
                        time_name_data[sname_data].color = d3.rgb(col).brighter(sname * 0.3);
                        time_name_data_sorted.push(time_name_data[sname_data]);
                        nameTime_sorted.push(nameTime[sname_data]);
                        break;
                    }
                }
                for (sname_data = 0; sname_data < cost_name_data.length; sname_data++) {
                    if (SrcNameByType[stype][sname] == cost_name_data[sname_data].label) {
                        cost_name_data[sname_data].color = d3.rgb(col).brighter(sname * 0.3);
                        cost_name_data_sorted.push(cost_name_data[sname_data]);
                        break;
                    }
                }
                for (sname_data = 0; sname_data < SrcName_energy_data.length; sname_data++) {
                    if (SrcNameByType[stype][sname] == SrcName_energy_data[sname_data].label) {
                        SrcName_energy_data[sname_data].color = d3.rgb(col).brighter(sname * 0.3);
                        energy_name_data_sorted.push(SrcName_energy_data[sname_data]);
                        break;
                    }
                }
                for (sname_data = 0; sname_data < cost_energy_name_data.length; sname_data++) {
                    if (SrcNameByType[stype][sname] == cost_energy_name_data[sname_data].label) {
                        cost_energy_name_data[sname_data].color = d3.rgb(col).brighter(sname * 0.3);
                        cost_energy_name_data_sorted.push(cost_energy_name_data[sname_data]);
                        break;
                    }
                }
                for (sname_data = 0; sname_data < energy_name_data_JS.length; sname_data++) {
                    if (SrcNameByType[stype][sname] == energy_name_data_JS[sname_data].label) {
                        energy_name_data_JS[sname_data].color = d3.rgb(col).brighter(sname * 0.3);
                        energy_name_data_sortedJS.push(energy_name_data_JS[sname_data]);
                        break;
                    }
                }
            }
        }

        for (stype = 0; stype < SrcByType.length; stype++) {
            for (stype_data = 0; stype_data < time_type_data.length; stype_data++) {
                if (SrcByType[stype] == time_type_data[stype_data].label) {
                    time_type_data_sorted.push(time_type_data[stype_data]);
                    break;
                }
            }
        }

        for (stype = 0; stype < SrcByType.length; stype++) {
            for (stype_data = 0; stype_data < energy_type_data_JS.length; stype_data++) {
                if (SrcByType[stype] == energy_type_data_JS[stype_data].label) {
                    energy_type_data_sortedJS.push(energy_type_data_JS[stype_data]);
                    break;
                }
            }
        }

        for (stype = 0; stype < SrcByType.length; stype++) {
            for (stype_data = 0; stype_data < energy_data.length; stype_data++) {
                if (SrcByType[stype] == energy_data[stype_data].label) {
                    energy_type_data_sorted.push(energy_data[stype_data]);
                    break;
                }
            }
        }

        for (stype = 0; stype < SrcByType.length; stype++) {
            for (stype_data = 0; stype_data < cost_type_data.length; stype_data++) {
                if (SrcByType[stype] == cost_type_data[stype_data].label) {
                    cost_type_data_sorted.push(cost_type_data[stype_data]);
                    break;
                }
            }
        }

        for (stype = 0; stype < SrcByType.length; stype++) {
            for (stype_data = 0; stype_data < cost_energy_type_data.length; stype_data++) {
                if (SrcByType[stype] == cost_energy_type_data[stype_data].label) {
                    cost_energy_type_data_sorted.push(cost_energy_type_data[stype_data]);
                    break;
                }
            }
        }

        var sortedData = {};
        sortedData.SrcName_energy_data_sorted = energy_name_data_sorted;
        sortedData.time_name_data_sorted = time_name_data_sorted;
        sortedData.time_type_data_sorted = time_type_data_sorted;
        sortedData.energy_type_data_sorted = energy_type_data_sorted;
        sortedData.nameTime_sorted = nameTime_sorted;
        sortedData.cost_name_data_sorted = cost_name_data_sorted;
        sortedData.cost_type_data_sorted = cost_type_data_sorted;
        sortedData.power_name_data_sorted = power_name_data_sorted;
        sortedData.power_type_data_sorted = power_type_data_sorted;
        sortedData.cost_energy_type_data_sorted = cost_energy_type_data_sorted;
        sortedData.cost_energy_name_data_sorted = cost_energy_name_data_sorted;
        sortedData.energy_type_data_sortedJS = energy_type_data_sortedJS;
        sortedData.energy_name_data_sortedJS = energy_name_data_sortedJS;

        return sortedData;
    }

    function createPie(Conf, pie_data, title, parent, id, tooltip) {
        Conf.data.content = pie_data;
        Conf.header.title.text = title;
        Conf.tooltips.string = tooltip;
        var pie = new d3pie(parent, Conf);
        document.getElementById(parent).children[0].setAttribute("id", id);
        return pie;
    }

    function prepareDataDaily(d) {
        dataDaily.push({
            "time": parseTimeISO(moment(d.key).format("YYYY-MM-DD HH:mm:ss")),
            "inverter": d.inverter || 0,
            "grid": d.grid || 0,
            "generator": d.generator || 0,
            "total" : (d.inverter|| 0) + (d.grid||0) +(d.generator||0)
        });
    }

    function capitalizeFirstLetter(string) {
        return string[0].toUpperCase() + string.slice(1);
    }

    function secToHrs(time_Data) {

        var t = [];
        for (i = 0; i < time_Data.length; i++) {
            t.push({
                "label": time_Data[i].label,
                "value": time_Data[i].value,
                "color": time_Data[i].color
            });
        }

        for (i = 0; i < t.length; i++) {
            t[i].value = roundTo(t[i].value / 3600.0);
        }
        return t;
    }

    var date = new Date();

    function addMonth() {
        date = new Date(date.getFullYear(), date.getMonth() + 1, 1);
        remove_monthlygraphElements();
        var timeStart = moment(new Date(date.getFullYear(), date.getMonth(), 1)).format("YYYY-MM-DDTHH:mm:ss.SSSZ");
        var timeStop = moment(new Date(date.getFullYear(), date.getMonth() + 1, 0,23,59,59)).format("YYYY-MM-DDTHH:mm:ss.SSSZ");
        exportStartTime = timeStart;
        exportEndTime = timeStop;
        setExportDateTimePicker(exportStartTime,exportEndTime);
        if (isHiddenFetchDataContainer) {
            onPeriodChanged();
        }
        load_monthly("../monthly/" + config + "/" + timeStart + "/" + timeStop);
    }

    function reduceMonth() {
        date = new Date(date.getFullYear(), date.getMonth() - 1, 1);
        remove_monthlygraphElements();
        var timeStart = moment(new Date(date.getFullYear(), date.getMonth(), 1)).format("YYYY-MM-DDTHH:mm:ss.SSSZ");
        var timeStop = moment(new Date(date.getFullYear(), date.getMonth() + 1, 0,23,59,59)).format("YYYY-MM-DDTHH:mm:ss.SSSZ");
        exportStartTime = timeStart;
        exportEndTime = timeStop;
        setExportDateTimePicker(exportStartTime,exportEndTime);
        if (isHiddenFetchDataContainer) {
            onPeriodChanged();
        }
        load_monthly("../monthly/" + config +  "/" + timeStart + "/" + timeStop);
    }

    function addYear(){
        if (monthGraphView === true) {
            date = new Date(date.getFullYear(),0);
        } else {
            date = new Date(date.getFullYear()+1,0);
        }
        remove_monthlygraphElements();
        var timeStart = moment(new Date(date.getFullYear(), date.getMonth())).format("YYYY-MM-DDTHH:mm:ss.SSSZ");
        var timeStop = moment(new Date(date.getFullYear(), 12, 0, 23,59,59)).format("YYYY-MM-DDTHH:mm:ss.SSSZ");
        exportStartTime = timeStart;
        exportEndTime = timeStop;
        if (isHiddenFetchDataContainer) {
            onPeriodChanged();
        }
        load_yearly("../yearly/" + config +  "/" + timeStart + "/" + timeStop);
    }

    function reduceYear(){
        if (monthGraphView === true) {
            date = new Date(date.getFullYear(),0);
        } else {
            date = new Date(date.getFullYear()-1,0);
        }
        remove_monthlygraphElements();
        var timeStart = moment(new Date(date.getFullYear(), date.getMonth())).format("YYYY-MM-DDTHH:mm:ss.SSSZ");
        var timeStop = moment(new Date(date.getFullYear(), 12, 0, 23,59,59)).format("YYYY-MM-DDTHH:mm:ss.SSSZ");
        exportStartTime = timeStart;
        exportEndTime = timeStop;
        setExportDateTimePicker(exportStartTime,exportEndTime);
        if (isHiddenFetchDataContainer) {
            onPeriodChanged();
        }
        load_yearly("../yearly/" + config +  "/" + timeStart + "/" + timeStop);
    }

    var config = document.getElementById('config').options[0].value;
    try {
        var consumerGroup = document.getElementById('consumerGroup').options[0] ? document.getElementById('consumerGroup').options[0].value : "";
    }
    catch (e) {
        console.log(e);
        consumerGroup = '';
    }

    var lastTransmit = 0;

    $("#probe").on('change', function() {
        var timeWindow = $('#hiddenTimeWindow').val();
        config = document.getElementById('config').options[0].value;
        remove_monthlygraphElements();
        remove_livegraphElements();
        query(config, "../bounds/" + config , timeWindow);
        setProbeOnSummary();
    });

    $("#config").change(function() {
        var timeWindow = $('#hiddenTimeWindow').val();
        config = $("#config").val();
        try {
           consumerGroup = document.getElementById('consumerGroup').options[0] ? document.getElementById('consumerGroup').options[0].value : "";
        }
        catch (e) {
            console.log(e);
            consumerGroup = '';
        }
        remove_graphElements();
        remove_monthlygraphElements();
        remove_livegraphElements();
        remove_phaseElements();
        query(config, "../bounds/" + config , timeWindow);
        setConfOnSummary();
    });
    query(config,"../bounds/" + config , $('#hiddenTimeWindow').val());
    setProbeOnSummary();
    setConfOnSummary();

    $('.alert').hide();
    $('.alert').on('close.bs.alert', function(e) {
        e.preventDefault();
        $(this).hide();
    });

    //Alert
    function closeAlert() {
        $('.alert').show();
        $(".alert").fadeTo(1500, "swing").slideUp(500);
    }

    function query(config, url, TimeWindow, user_stop) {
        var blockUserAccess = document.getElementById('blockUserAccess').value;
        if (blockUserAccess === "true"){
            $('#blockAccessModal').modal({
              show :true
            });
            return ;
        }
        var windowSizeMsg = "";
        var today;
        $('#hiddenTimeWindow').val(TimeWindow); //set window to a hidden field so it can be accessible by other functions
        $.get(url, function(data) {
            console.log(data);
            data_stop_realtime = data.stop; // most recent value returned from snippet
            data_stop = typeof user_stop !== 'undefined' ? user_stop : data_stop_realtime;
            data_stop = moment(parseInt(data_stop)).format("YYYY-MM-DDTHH:mm:ss.SSSZ");
            try{
                dp2.data("DateTimePicker").date(moment(data_stop)); //set dateTimePicker2 time
            } catch (err){
                console.log(err);
            }

            // tz awareness pending
            var user_start = new Date().getTime() - parseFloat(TimeWindow);
            var data_start = "";
            if (user_start < data.start) {
                try{
                    dp1.data("DateTimePicker").date(moment(data.start)); //set dateTimePicker1 time
                } catch(err) {
                    console.log(err);
                }
            } else {
                try {
                    dp1.data("DateTimePicker").date(moment(user_start)); //set dateTimePicker1 time
                } catch(err) {
                    console.log(err);
                }
            }

            today = new Date();
            day = new Date(today.getFullYear(), today.getMonth(), today.getDate(), 0, 1, 30);
            // Will be used to avoid some WET-ness.
            var timeStart = "";
            var timeStop = "";

            if (data_start > data_stop) {
                if (sessionStorage.length > 0) {
                    $("#timeWindowSpan").html(sessionStorage.windowSizeMsg);
                    $("#timeWindowSpan").attr("title", sessionStorage.popMsg);
                    load_data("../data/" + config  + "/" + sessionStorage.data_start + "/" + sessionStorage.data_stop);
                    load_recent("../recent/" + config + "/" + moment(parseInt(sessionStorage.data_stop_realtime)).format("YYYY-MM-DDTHH:mm:ss.SSSZ") + "/" + sessionStorage.realTimeWindow);
                    lastTransmit = sessionStorage.data_stop;
                }
                load_daily("../daily/" + config + "/" + moment(new Date(day)).format("YYYY-MM-DDTHH:mm:ss.SSSZ") + "/" + moment(new Date(day.getFullYear(), day.getMonth(), day.getDate(), 23, 59, 59)).format("YYYY-MM-DDTHH:mm:ss.SSSZ"));
                timeStart = moment(new Date(day.getFullYear(), day.getMonth(), 1)).format("YYYY-MM-DDTHH:mm:ss.SSSZ");
                timeStop = moment(new Date(day.getFullYear(), day.getMonth() + 1, 0,23,59,59)).format("YYYY-MM-DDTHH:mm:ss.SSSZ");
                exportStartTime = timeStart;
                exportEndTime = timeStop;
                setExportDateTimePicker(exportStartTime,exportEndTime);
                if (isHiddenFetchDataContainer) {
                    onPeriodChanged();
                }
                load_monthly("../monthly/" + config  + "/" + timeStart + "/" + timeStop);
                loadConsumers("../consumer/" + config  + "/" + moment(new Date(day)).format("YYYY-MM-DDTHH:mm:ss.SSSZ") + "/" + moment(new Date(day.getFullYear(), day.getMonth(), day.getDate(), 23, 59, 59)).format("YYYY-MM-DDTHH:mm:ss.SSSZ"));
                if (consumerGroup) {
                    loadConsumerGroup("../consumerGroup/" + consumerGroup  + "/" + moment(new Date(day)).format("YYYY-MM-DDTHH:mm:ss.SSSZ") + "/" + moment(new Date(day.getFullYear(), day.getMonth(), day.getDate(), 23, 59, 59)).format("YYYY-MM-DDTHH:mm:ss.SSSZ"));
                }
                return closeAlert();
            } else {
                data_start = moment(new Date(day.getFullYear(), day.getMonth(), day.getDate(), 0, 0, 0)).format("YYYY-MM-DDTHH:mm:ss.SSSZ");
                data_stop = moment(new Date(day.getFullYear(), day.getMonth(), day.getDate(), 23, 59, 59)).format("YYYY-MM-DDTHH:mm:ss.SSSZ");
            }

            load_data("../data/" + config + "/" + moment(new Date(day)).format("YYYY-MM-DDTHH:mm:ss.SSSZ") + "/" + moment(new Date(day.getFullYear(), day.getMonth(), day.getDate(), 23, 59, 59)).format("YYYY-MM-DDTHH:mm:ss.SSSZ"));
            dayRealtime  = true;
            load_daily("../daily/" + config  + "/" + moment(new Date(day)).format("YYYY-MM-DDTHH:mm:ss.SSSZ") + "/" + moment(new Date(day.getFullYear(), day.getMonth(), day.getDate(), 23, 59, 59)).format("YYYY-MM-DDTHH:mm:ss.SSSZ"));
            timeStart = moment(new Date(day.getFullYear(), day.getMonth(), 1)).format("YYYY-MM-DDTHH:mm:ss.SSSZ");
            timeStop = moment(new Date(day.getFullYear(), day.getMonth() + 1,0 ,23,59,59)).format("YYYY-MM-DDTHH:mm:ss.SSSZ");
            exportStartTime = timeStart;
            exportEndTime = timeStop;
            setExportDateTimePicker(exportStartTime,exportEndTime);
            load_monthly("../monthly/" + config  + "/" + timeStart + "/" + timeStop);
            load_recent("../recent/" + config + "/" + moment(parseInt(data_stop_realtime)).format("YYYY-MM-DDTHH:mm:ss.SSSZ") + "/" + realTimeWindow);
            loadConsumers("../consumer/" + config  + "/" + moment(new Date(day)).format("YYYY-MM-DDTHH:mm:ss.SSSZ") + "/" + moment(new Date(day.getFullYear(), day.getMonth(),day.getDate(),23,59,59)).format("YYYY-MM-DDTHH:mm:ss.SSSZ"));
            if (consumerGroup) {
                loadConsumerGroup("../consumerGroup/" + consumerGroup  + "/" + moment(new Date(day)).format("YYYY-MM-DDTHH:mm:ss.SSSZ") + "/" + moment(new Date(day.getFullYear(), day.getMonth(),day.getDate(),23,59,59)).format("YYYY-MM-DDTHH:mm:ss.SSSZ"));
            }
            lastTransmit = data.stop;

            //sessions
            sessionStorage.windowSizeMsg = windowSizeMsg;
            sessionStorage.data_stop_realtime = data_stop_realtime;
            sessionStorage.data_start = data_start;
            sessionStorage.data_stop = data_stop;
            sessionStorage.realTimeWindow = realTimeWindow;
        });
    }

    var brush;
    var brush_status = 0;
    var brush0 = 0;
    var brush1 = 0;

    // for real time brush
    var brush_real;
    var brush_real_status = 0;
    var brush0_real = 0;
    var brush1_real = 0;

    var textMonth;
    var linedVoltage = [];//Voltage
    var linedCurrent = [];//Current
    var linedpf = []; //pf

    var opts = {
        lines: 13, // The number of lines to draw
        length: 20, // The length of each line
        width: 5, // The line thickness
        radius: 30, // The radius of the inner circle
        scale: 10, // Scales overall size of the spinner
        color: '#FFFFFF', // #rgb or #rrggbb or array of colors
        speed: 1.9, // Rounds per second
        trail: 40, // Afterglow percentage
        className: 'spinner', // The CSS class to assign to the spinner
    };

    var parseTimeISO = d3.time.format("%Y-%m-%d %H:%M:%S").parse;
    var x = d3.time.scale().range([0, width2]);
    var y = d3.scale.linear().range([height, 0]);

    tickDays = screen.width <= 768 ? 6 : 1;
    //EnergyByDay axes
    var xEnergyDay = d3.time.scale().range([0, width2]);
    var yEnergyDay = d3.scale.linear().range([height, 0]);
    var svgEnergyDay = d3.select("#energyDay_graph").append("svg")
        .attr("id", "energyDayGraph")
        .attr("viewBox", String(0) + " " + String(0) + " " + String(width2 + margin.left + margin.right) + " " + String(height + margin.top + margin.bottom))
        .attr("perserveAspectRatio", "xMinYMid")
        .attr("width", width2 + margin.left + margin.right)
        .attr("height", height + margin.top + margin.bottom)
        .append("g")
        .attr("transform", "translate(" + margin2.left + "," + margin.top + ")");
    var contextEnergyDay = svgEnergyDay.append("g")
        .attr("id", "context")
        .attr("class", "context")
        .attr("transform", "translate(" + margin.left + "," + 0 + ")");

    var xAxisEnergyDay = d3.svg.axis()
        .scale(xEnergyDay)
        .orient("bottom")
        .ticks(d3.time.days, tickDays)
        .tickFormat(d3.time.format(' %d'))
        .tickSize(5);
    var yAxis1EnergyDay = d3.svg.axis()
        .scale(yEnergyDay)
        .orient("left"); //end

    if (screen.width >= 768 ) {
         controls = d3.select("#energyDayGraph").append("svg:g").attr("scale",0.5).attr("transform","translate(0," + 10+ ")");

         controls.append("svg:circle")
            .attr("cx",((0.99 * (width2 + margin.left)-33)))
            .attr("cy",30)
            .attr("r",40)
            .attr("fill", "#595959")
            .attr("fill-opacity", 0.7);

        controls.append("svg:a").attr("xlink:href", "#").append("svg:path").attr("d", "M " + (0.99 *  (width2 +  margin.left)-50) + " 20 L " + (0.99 *  (width2 +  margin.left) - 65) + " 30 L " + (0.99 *  (width2 + margin.left)-50) + " 40 L " + (0.99 *  (width2 + margin.left) -54) + " 30 Z")
            .attr("fill", "#b3b3b3")
            .on('mouseover', function() {
                $(this).attr("fill", "white");
            })
            .on('mouseout', function() {
                $(this).attr("fill", "#b3b3b3");
            })
            .on('click', function() {
                reduceMonth();
            }).append("svg:title").text("Previous Month");

        controls.append("svg:a").attr("xlink:href", "#").append("svg:path").attr("d", "M " + (0.99 * (width2 +  margin.left) -15) + " 20 L " + (0.99 *  (width2 + margin.left)) + " 30 L " + (0.99 *  (width2 + margin.left)-15) + " 40 L  " + (0.99 *  (width2 + margin.left)- 11) +" 30 Z" )
            .attr("fill", "#b3b3b3")
            .on('mouseover', function() {
                $(this).attr("fill", "white");
            })
            .on('mouseout', function() {
                $(this).attr("fill", "#b3b3b3");
            })
            .on('click', function() {
                if (date.getFullYear() < new Date().getFullYear()) {
                    addMonth();
                } else {
                    if (date.getMonth() + 1 <= new Date().getMonth()) {
                        addMonth();
                    } else {

                    }
                }
            }).append("svg:title").text("Next Month");

        controls.append("svg:a").attr("xlink:href", "#").append("svg:path").attr("d", "M " + (0.99 * (width2 + margin.left)-43) + " 15 L " + (0.99 * (width2 + margin.left)-33) + " 0 L " + (0.99 * (width2 + margin.left)-23) + " 15 L " + (0.99 * (width2 + margin.left) -33) +" 10 Z")
            .attr("fill", "#b3b3b3")
            .on('mouseover', function() {
                $(this).attr("fill", "white");
            })
            .on('mouseout', function() {
                $(this).attr("fill", "#b3b3b3");
            })
            .on('click', function() {
                if (date.getFullYear() + 1 <= new Date().getFullYear()) {
                    addYear();
                } else {

                }
            }).append("svg:title").text("Next Year");

        controls.append("svg:a").attr("xlink:href", "#").append("svg:path").attr("d", "M " + (0.99 * (width2 + margin.left)-43) + " 45 L " + (0.99 * (width2 + margin.left)-33) + " 60 L " + (0.99 * (width2 + margin.left)-23) + " 45 L" + (0.99 * (width2 + margin.left) -33) +" 50 Z")
            .attr("fill", "#b3b3b3")
            .on('mouseover', function() {
                $(this).attr("fill", "white");
            })
            .on('mouseout', function() {
                $(this).attr("fill", "#b3b3b3");
            })
            .on('click', function() {
                reduceYear();
            }).append("svg:title").text("Previous Year");

            controls.append("svg:a").attr("xlink:href", "#").append("svg:circle")
                .attr("cx",((0.99 * (width2 + margin.left)-33)))
                .attr("cy",30)
                .attr("r",10)
                .attr("fill", "#b3b3b3")
                .attr("fill-opacity", 1)
                .attr("id","energySwitch")
                .on('mouseover', function() {
                    $(this).attr("fill", "white");
                })
                .on('mouseout', function() {
                    if (energySwitch !== true) {
                        $(this).attr("fill", "#b3b3b3");
                    }
                })
                .on('click', function() {
                    if (!energySwitch) {
                        energySwitch = true;
                        remove_monthlygraphElements();
                        d3.select("#energySwitchTitle").text("Switch to Time Graph");
                        timeToEnergy();
                    } else {
                        energySwitch = false;
                        remove_monthlygraphElements();
                        d3.select("#energySwitchTitle").text("Switch to Energy Graph");
                        timeToEnergy();
                    }
                })
                .append("svg:title")
                    .attr("id","energySwitchTitle")
                    .text(function() {
                        if (!energySwitch){
                          return "Switch to Energy Graph";
                        } else {
                          return "Switch to Time Graph";
                        }
                    });


    }

    function timeToEnergy(){
        var targetEnergy = document.getElementById('energyDay_graph');
        var spinnerEnergy = new Spinner(opts).spin(targetEnergy);
        var axisTitle = energySwitch ? "Energy (Wh)"  : "Time (Hr)";

        if (!monthGraphView) {
            EnergyByYear.forEach(function(d) {
                d.total = energySwitch ? d.energy.sum : (d.doc_count *5)/3600;
                if (d.buckets.length === 0) {
                    console.info("no buckets");
                    d.src = "error";
                    d.counts = bucket_handlerEnergyDay(d.src, d.total, 0, 0);
                } else if (d.buckets.length > 5) {
                    console.error("too many buckets");
                    console.error(d.buckets);
                    d.src = "error";
                } else {
                    d.counts = bucket_handlerEnergyDay(d.src, d.total, bcount, 0);
                }
            });

            xAxisEnergyMonth = d3.svg.axis()
                .scale(xEnergyDay)
                .orient("bottom")
                .ticks(d3.time.month, tickDays)
                .tickFormat(d3.time.format('%B'))
                .tickSize(5);
            xEnergyDay.domain([d3.time.day.offset(new Date(date.getFullYear(), 0),-29), (new Date(date.getFullYear(),12, 0, 23,59,59))]);
            yEnergyDay.domain([0, d3.extent(EnergyByYear, function(d) {
                return d.total;
            })[1]]);
            EnergyByDayElements = createAxis(0, contextEnergyDay, xAxisEnergyMonth, yAxis1EnergyDay, 'Time (Hr)', width2, height,make_x_axis,make_y_axis, 1); // Battery DOD
            bars_energyDay = drawDay(roundTo, contextEnergyDay, EnergyByYear, svgEnergyDay, yEnergyDay, xEnergyDay);
        } else {
            EnergyByDay.forEach(function(d) {
                d.total = energySwitch ? d.energy.sum : (d.doc_count *5)/3600;
                if (d.buckets.length === 0) {
                    console.info("no buckets");
                    d.src = "error";
                    d.counts = bucket_handlerEnergyDay(d.src, d.total, 0, 0);
                } else if (d.buckets.length > 5) {
                    console.error("too many buckets");
                    console.error(d.buckets);
                    d.src = "error";
                } else {
                    d.counts = bucket_handlerEnergyDay(d.src, d.total, bcount, 0);
                }
            });

            xEnergyDay.domain([d3.time.hour.offset(new Date(date.getFullYear(), date.getMonth(), 1), -23), d3.time.hour.offset(new Date(date.getFullYear(), date.getMonth() + 1, 0), 23)]);
            yEnergyDay.domain([0, d3.extent(EnergyByDay, function(d) {
                return d.total;
            })[1]]);
            EnergyByDayElements = createAxis(0, contextEnergyDay, xAxisEnergyDay, yAxis1EnergyDay, axisTitle, width2, height,make_x_axis,make_y_axis, 1); // Battery DOD
            bars_energyDay = drawDay(roundTo, contextEnergyDay, EnergyByDay, svgEnergyDay, yEnergyDay, xEnergyDay);
        }
        spinnerEnergy.stop();
    }

    //Real time
    var xl = d3.time.scale().range([0, width]);
    yl = d3.scale.linear().range([height, 0]);
    var xAxis_realtime = d3.svg.axis()
        .scale(xl)
        .orient("bottom");
    //.tickFormat(d3.time.format("%H:%M:%S"));
    var yAxis_realtime = d3.svg.axis()
        .scale(yl)
        .orient("left");
    var svg_realtime = d3.select("#realpower_live").append("svg")
        .attr("id", "realtimegraph")
        .attr("viewBox", String(0) + " " + String(0) + " " + String(width + margin.left + margin.right) + " " + String(height + margin.top + margin.bottom))
        .attr("width", width + margin.left + margin.right)
        .attr("height", height + margin.top + margin.bottom)
        .append("g")
        .attr("transform", "translate(" + margin2.left + "," + margin2.top + ")");
    var focus = svg_realtime.append("g")
        .attr("id", "focus")
        .attr("class", "focus")
        .attr("transform", "translate(" + margin.left + "," + margin.top + ")");

    realtimeControls = d3.select("#realtimegraph").append("svg:g").attr("scale",0.5).attr("transform","translate(0," + 10+ ")");

    realtimeControls.append("svg:circle")
        .attr("cx",((0.99 * (width2 + margin.left)-33)))
        .attr("cy",42)
        .attr("r",26)
        .attr("fill", "#595959")
        .attr('stroke-width', 0.4)
        .attr("stroke","#A5BDFF")
        .attr("fill-opacity", 0.9);

    realtimeControls.append("svg:a").attr("xlink:href", "#").append("svg:circle")
        .attr("cx",((0.99 * (width2 + margin.left)-33)))
        .attr("cy",42)
        .attr("r",10)
        .attr("fill", "#A5BDFF")
        .attr("fill-opacity", 1)
        .on('mouseover', function() {
            $(this).attr("fill", "#00cc00");
        })
        .on('mouseout', function() {
            if (minRealTime !== true) {
                $(this).attr("fill", "#A5BDFF");
            }
        })
        .on('click', function() {
            if (!minRealTime) {
                $(this).attr("fill", "#00cc00");
                minRealTime = true;
                dayRealtime = false;
                remove_livegraphElements();
                d3.select("#minRealtimetitle").text("Zoom out to the day");
                load_daily("");
            } else {
                today = new Date();
                day = new Date(today.getFullYear(), today.getMonth(), today.getDate(), 0, 1, 30);
                minRealTime = false;
                dayRealtime = true;
                remove_livegraphElements();
                d3.select("#minRealtimetitle").text("Zoom into the present moment");
                load_daily("../daily/" + config + "/" + moment(new Date(day)).format("YYYY-MM-DDTHH:mm:ss.SSSZ") + "/" + moment(new Date(day.getFullYear(), day.getMonth(), day.getDate(), 23, 59, 59)).format("YYYY-MM-DDTHH:mm:ss.SSSZ"));
            }
        })
        .append("svg:title")
            .attr("id","minRealtimetitle")
            .text(function() {
                if (!minRealTime){
                    return "Zoom into the present moment";
                } else {
                    return "Zoom out to the day";
                }
            });


    //consumers
    var xConsumer = d3.time.scale().range([0, width]);
    var yConsumer =  d3.scale.linear().range([height2, 0]);
    var svg_consumer = d3.select("#consumerGroupCharts").append("svg")
        .attr("id", "consumerGroupGraph")
        .attr("viewBox", String(0) + " " + String(0) + " " + String(width2 + margin.left + margin.right) + " " + String(height2 + margin.top + margin.bottom))
        .attr("width", width2 + margin.left + margin.right)
        .attr("height", height2 + margin.top + margin.bottom)
        .append("g")
        .attr("transform", "translate(" + margin.left + "," + margin.top + ")");
    var contextConsumer = svg_consumer.append("g")
        .attr("id", "context")
        .attr("class", "context");
    var xAxis_consumer = d3.svg.axis()
        .scale(xConsumer)
        .orient("bottom");
    var yAxis_consumer = d3.svg.axis()
        .scale(yConsumer)
        .orient("left");
    var consumerLine = d3.svg.line()
        .x(function(d) { return xConsumer(d.date); })
        .y(function(d) { return yConsumer(d.value); })
        .interpolate("basis");

    var xConCost = d3.time.scale().range([0, width]);
    var yConCost =  d3.scale.linear().range([height2, 0]);
    var svg_ConCost = d3.select("#consumerGroupCostCharts").append("svg")
        .attr("id", "consumerGroupCostGraph")
        .attr("viewBox", String(0) + " " + String(0) + " " + String(width2 + margin.left + margin.right) + " " + String(height2 + margin.top + margin.bottom))
        .attr("width", width2 + margin.left + margin.right)
        .attr("height", height2 + margin.top + margin.bottom)
        .append("g")
        .attr("transform", "translate(" + margin.left + "," + margin.top + ")");
    var contextConCost = svg_ConCost.append("g")
        .attr("id", "context")
        .attr("class", "context");
    var xAxis_ConCost = d3.svg.axis()
        .scale(xConCost)
        .orient("bottom");
    var yAxis_ConCost = d3.svg.axis()
        .scale(yConCost)
        .orient("left");
    var consumerCostLine = d3.svg.line()
        .x(function(d) { return xConCost(d.date); })
        .y(function(d) { return yConCost(d.value); })
        .interpolate("basis");

    //fuel axes
    var xf = d3.time.scale().range([0, widthFuel]);
    var yf = d3.scale.linear().range([height3, 0]);
    var svgFuel = d3.select("#fuel_graph").append("svg")
        .attr("id", "fuelgraph")
        .attr("viewBox", String(0) + " " + String(0) + " " + String(widthFuel + margin.left + margin.right) + " " + String(height3 + margin.top + margin.bottom))
        .attr("perserveAspectRatio", "xMinYMid")
        .attr("width", widthFuel + margin.left + margin.right)
        .attr("height", height3 + margin.top + margin.bottom)
        .append("g")
        .attr("transform", "translate(" + margin.left + "," + margin.top + ")");
    var contextFuel = svgFuel.append("g")
        .attr("id", "context")
        .attr("class", "context");
    var xAxisf = d3.svg.axis()
        .scale(xf)
        .orient("bottom");
    var yAxis1f = d3.svg.axis()
        .scale(yf)
        .orient("left"); //end
    var lineFuel = d3.svg.line()
        .x(function(d) {
            return xf(d.time);
        })
        .y(function(d) {
            return yf(d.total);
        })
        .interpolate("linear");
    textFuel = svgFuel.append("text").attr("x", (0.45 * widthFuel))
        .attr("y", -2)
        .attr("font-size", "2em")
        .attr('transform', 'translate(0,0)')
        .attr("fill", "#96989E")
        .text("Fuel Use");

    var xfr = d3.time.scale().range([0, widthFuel]);
    var yfr = d3.scale.linear().range([height3, 0]);
    var svgFuelRate = d3.select("#fuelCharts").append("svg")
        .attr("id", "fuelCharts")
        .attr("viewBox", String(0) + " " + String(0) + " " + String(widthFuel + margin.left + margin.right) + " " + String(height3 + margin.top + margin.bottom))
        .attr("perserveAspectRatio", "xMinYMid")
        .attr("width", widthFuel + margin.left + margin.right)
        .attr("height", height3 + margin.top + margin.bottom)
        .append("g")
        .attr("transform", "translate(" + margin.left + "," + margin.top + ")");
    var contextFuelRate = svgFuelRate.append("g")
        .attr("id", "context")
        .attr("class", "context");
    /*var xAxisfr = d3.svg.axis()
        .scale(xfr)
        .orient("bottom");*/
    var yAxis1fr = d3.svg.axis()
        .scale(yfr)
        .orient("left"); //end
    var lineFuelRate = d3.svg.line()
        .x(function(d) {
            return xfr(d.time);
        })
        .y(function(d) {
            return yfr(d.total);
        })
        .interpolate("linear");
    textFuelRate = svgFuelRate.append("text").attr("x", (0.45 * widthFuel))
        .attr("y", -2)
        .attr("font-size", "2em")
        .attr('transform', 'translate(0,0)')
        .attr("fill", "#96989E")
        .text("Fuel Rate");


    //battery
    var xb = d3.time.scale().range([0, widthFuel]);
    var yb = d3.scale.linear().range([height3, 0]);
    var svgBattery = d3.select("#capGraph").append("svg")
        .attr("id", "capGraph")
        .attr("viewBox", String(0) + " " + String(0) + " " + String(widthFuel + margin.left + margin.right) + " " + String(height3 + margin.top + margin.bottom))
        .attr("perserveAspectRatio", "xMinYMid")
        .attr("width", widthFuel + margin.left + margin.right)
        .attr("height", height3 + margin.top + margin.bottom)
        .append("g")
        .attr("transform", "translate(" + margin.left + "," + margin.top + ")");
    var contextBattery = svgBattery.append("g")
        .attr("id", "context")
        .attr("class", "context");
    var xAxisb = d3.svg.axis()
         .scale(xb)
         .orient("bottom");
    var yAxis1b = d3.svg.axis()
        .scale(yb)
        .orient("left"); //end
    var lineBattery = d3.svg.line()
        .x(function(d) {
            return xb(d.time);
        })
        .y(function(d) {
            return yb(d.total);
        })
        .interpolate("linear");
    batteryCapacity = svgBattery.append("text").attr("x", (0.4 * widthFuel))
        .attr("y", -2)
        .attr("font-size", "2em")
        .attr('transform', 'translate(0,0)')
        .attr("fill", "#96989E")
        .text("Battery Capacity");

    var xDOD = d3.time.scale().range([0, widthFuel]);
    var yDOD = d3.scale.linear().range([height3, 0]);
    var svgDOD = d3.select("#dodGraph").append("svg")
        .attr("id", "dodGraph")
        .attr("viewBox", String(0) + " " + String(0) + " " + String(widthFuel + margin.left + margin.right) + " " + String(height3 + margin.top + margin.bottom))
        .attr("perserveAspectRatio", "xMinYMid")
        .attr("width", widthFuel + margin.left + margin.right)
        .attr("height", height3 + margin.top + margin.bottom)
        .append("g")
        .attr("transform", "translate(" + margin.left + "," + margin.top + ")");
    var contextDOD = svgDOD.append("g")
        .attr("id", "context")
        .attr("class", "context");
    var yAxis1DOD = d3.svg.axis()
        .scale(yDOD)
        .orient("left"); //end
    var lineDOD = d3.svg.line()
        .x(function(d) {
            return xDOD(d.time);
        })
        .y(function(d) {
            return yDOD(d.total);
        })
        .interpolate("linear");
    batteryDODText = svgDOD.append("text").attr("x", (0.4 * widthFuel))
        .attr("y", -2)
        .attr("font-size", "2em")
        .attr('transform', 'translate(0,0)')
        .attr("fill", "#96989E")
        .text("Battery DOD");

    //Voltage axes
    var xv = d3.time.scale().range([0, width2]);
    var yv = d3.scale.linear().range([height3, 0]);
    var svgVoltage = d3.select("#VoltagePhaseGraph").append("svg")
        .attr("id", "voltagegraph")
        .attr("viewBox", String(0) + " " + String(0) + " " + String(width2 + margin.left + margin.right) + " " + String(height3 + margin.top + margin.bottom))
        .attr("perserveAspectRatio", "xMinYMid")
        .attr("width", width2 + margin.left + margin.right)
        .attr("height", height3 + margin.top + margin.bottom)
        .append("g")
        .attr("transform", "translate(" + margin.left + "," + margin.top + ")");
    var contextVoltage = svgVoltage.append("g");
        /*.attr("id", "context")
        .attr("class", "context");*/
    var xAxisv = d3.svg.axis()
        .scale(xv)
        .orient("bottom")
        .ticks(d3.time.second, 30);
    var yAxis1v = d3.svg.axis()
        .scale(yv)
        .orient("left"); //end
    var lineVoltage = d3.svg.line()
        .x(function(d) {
            return xv(d.time);
        })
        .y(function(d) {
            return yv(d.voltage);
        })
        .interpolate("basis");

    //current axes
    var xcurrent = d3.time.scale().range([0, width2]);
    var ycurrent = d3.scale.linear().range([height3, 0]);
    var svgCurrent = d3.select("#CurrentPhaseGraph").append("svg")
        .attr("id", "currentgraph")
        .attr("viewBox", String(0) + " " + String(0) + " " + String(width2 + margin.left + margin.right) + " " + String(height3 + margin.top + margin.bottom))
        .attr("perserveAspectRatio", "xMinYMid")
        .attr("width", width2 + margin.left + margin.right)
        .attr("height", height3 + margin.top + margin.bottom)
        .append("g")
        .attr("transform", "translate(" + margin.left + "," + margin.top + ")");
    var contextCurrent = svgCurrent.append("g")
        .attr("id", "context")
        .attr("class", "context");
    var xAxiscurrent = d3.svg.axis()
        .scale(xcurrent)
        .orient("bottom")
        .ticks(d3.time.second, 30);
    var yAxis1Current = d3.svg.axis()
        .scale(ycurrent)
        .orient("left"); //end
    var lineCurrent = d3.svg.line()
        .x(function(d) {
            return xv(d.time);
        })
        .y(function(d) {
            return ycurrent(d.current);
        })
        .interpolate("basis");

    //pf axes
    var ypf = d3.scale.linear().range([height3, 0]);
    var svgpf = d3.select("#pfPhaseGraph").append("svg")
        .attr("id", "pfgraph")
        .attr("viewBox", String(0) + " " + String(0) + " " + String(width2 + margin.left + margin.right) + " " + String(height3 + margin.top + margin.bottom))
        .attr("perserveAspectRatio", "xMinYMid")
        .attr("width", width2 + margin.left + margin.right)
        .attr("height", height3 + margin.top + margin.bottom)
        .append("g")
        .attr("transform", "translate(" + margin.left + "," + margin.top + ")");
    var contextpf = svgpf.append("g")
        .attr("id", "context")
        .attr("class", "context");
    var yAxis1pf = d3.svg.axis()
        .scale(ypf)
        .orient("left"); //end
    var linepf = d3.svg.line()
        .x(function(d) {
            return xv(d.time);
        })
        .y(function(d) {
            return ypf(d.pf);
        })
        .interpolate("basis");

    function make_x_axis() {
        return d3.svg.axis()
            .scale(x)
            .orient("bottom")
            .ticks(10);
    }

    function make_y_axis() {
        return d3.svg.axis()
            .scale(y)
            .orient("left")
            .ticks(4);
    }

    function make_yConsumer_axis() {
        return d3.svg.axis()
            .scale(yConsumer)
            .orient("left")
            .ticks(8);
    }

    function make_x_axisf() {
        return d3.svg.axis()
            .scale(xf)
            .orient("bottom")
            .ticks(10);
    }

    function make_y_axisf() {
        return d3.svg.axis()
            .scale(yf)
            .orient("left")
            .ticks(6);
    }

    function make_y_axisBattery() {
        return d3.svg.axis()
            .scale(yb)
            .orient("left")
            .ticks(6);
    }

    function make_x_axisBattery() {
        return d3.svg.axis()
            .scale(xb)
            .orient("bottom")
            .ticks(6);
    }


    function resetBrush() {
        brush
            .clear()
            .event(d3.select(".brush"));

        brush_real
             .clear()
              .event(d3.select(".brush"));
    }

    function roundTo(num) {
        return +(Math.round(num + "e+2") + "e-2");
    }

    function powerGraphScale(power) {
        if (+power >= 1000) {
            var scaledPower = (power / 1000);
            return roundTo(scaledPower) + "KW";
        } else {
            return roundTo(power) + "W";
        }
    }

    function remove_monthlygraphElements() {
        try {
            bars_energyDay.remove();
            EnergyByDayElements.forEach(function(d) {
                d.remove();
            });
            textMonth.remove();
            legend.remove();
        } catch (err) {
            console.log(err.message);
        }
    }

    function remove_livegraphElements() {
        try {
            gridlx.remove();
            gridly.remove();
            axislx.remove();
            axisly.remove();
            axislx_label.remove();
            axisly_label.remove();
            Group.remove();
        } catch (err) {
            console.log(err.message);
        }
    }

    function remove_graphElements() {
        try {
            try {
                    ConsumerGroupEnergyElements.forEach(function(d) {
                        d.remove();
                    });
                    ConsumerGroupCostElements.forEach(function(d) {
                        d.remove();
                    });
                    consumerCostGraph.forEach(function(d) {
                        d.remove();
                    });
                    consumerEnergyGraph.forEach(function(d) {
                        d.remove();
                    });
                    legendGroupCost.remove();
                    legendGroup.remove();
                } catch (err) {
                    console.log(err);
                }
            try {
                fuelElements.forEach(function (d){d.remove();});
                fuelRateElements.forEach(function (d){d.remove();});
                lined.remove();
                linedRate.remove();
            } catch (err) {
                console.log(err.message);
            }
            try {
                batteryCapElements.forEach(function (d){d.remove();});
                batteryDODElements.forEach(function (d){d.remove();});
                linedBatteryCap.remove();
                linedBatteryDOD.remove();
            } catch (err) {
                console.log(err.message);
            }
        } catch (err){
            console.log(err.message);
        }
    }

    function remove_phaseElements(){
        try {
            phaselegend.remove();
            voltageElements.forEach(function (d){d.remove();});
            currentElements.forEach(function (d){d.remove();});
            pfElements.forEach(function (d){d.remove();});
            linedCurrent[0].remove();
            linedCurrent[1].remove();
            linedCurrent[2].remove();
            linedVoltage[0].remove();
            linedVoltage[1].remove();
            linedVoltage[2].remove();
            linedpf[0].remove();
            linedpf[1].remove();
            linedpf[2].remove();
        } catch (err) {
        console.error(err);
        }
    }
    //for energyday graph
    function brushend() {
        if (brush.empty()) {
            return 0;
        }
        if (brush_status == 1) {
            if (screen.width >= 768) {
                window_start_unix = moment(new Date(brush0)).unix();
                window_stop_unix = moment(new Date(brush1)).unix();
                window_start =new Date(new Date(brush0).getTime());
                window_stop = new Date(new Date(brush1).getTime());
                remove_livegraphElements();
                remove_graphElements();
                var timeStart = moment(new Date(window_start.getFullYear(), window_start.getMonth(), window_start.getDate(), 0, 1, 30)).format("YYYY-MM-DDTHH:mm:ss.SSSZ");
                var timeStop = moment(new Date(window_stop.getFullYear(), window_stop.getMonth(), window_stop.getDate(), 23, 59, 59)).format("YYYY-MM-DDTHH:mm:ss.SSSZ");
                exportStartTime = timeStart;
                exportEndTime = timeStop;
                setExportDateTimePicker(exportStartTime,exportEndTime);
                if (isHiddenFetchDataContainer) {
                    onPeriodChanged();
                }
                realtime = false; // Can't load real time when view is more than a day
                minRealTime = false;
                dayRealtime = false;
                load_daily("../daily/" + config  + "/" + timeStart + "/" + timeStop);
                load_data("../data/" + config + "/" + timeStart + "/" + timeStop);
                loadConsumers("../consumer/" + config  + "/" + timeStart + "/" + timeStop);
                if (consumerGroup) {
                    loadConsumerGroup("../consumerGroup/" + consumerGroup  + "/" + timeStart + "/" + timeStop);
                }
            } else {
                reduceMonth();
            }
            resetBrush();
            d3.selectAll(".brush").call(brush.clear());
        }
        brush_status = 0;
    }

    //works for brush_real.
    function brushend_real() {
            if (brush_real.empty()) {
                return 0;
            }
            if (brush_real_status == 1) {
                if (screen.width >= 768) {
                    window_start_unix = moment(new Date(brush0_real)).unix();
                    window_stop_unix = moment(new Date(brush1_real)).unix();
                    window_start =new Date(new Date(brush0_real).getTime());
                    window_stop = new Date(new Date(brush1_real).getTime());
                    remove_livegraphElements();
                    remove_graphElements();
                    console.log(window_start);
                    console.log(window_stop);
                    console.log(brush1_real);
                    console.log(brush0_real);
                    console.log(brush_real);
                    var timeStart = moment(window_start).format("YYYY-MM-DDTHH:mm:ss.SSSZ");
                    var timeStop = moment(window_stop).format("YYYY-MM-DDTHH:mm:ss.SSSZ");


                    exportStartTime = timeStart;
                    exportEndTime = timeStop;
                    if (isHiddenFetchDataContainer) {
                        onPeriodChanged();
                    }
                    realtime = false; // Can't load real time when view is more than a day
                    minRealTime = false;
                    dayRealtime = false;
                    //only the real live graph is loaded
                    powerBrushStatus = true;
                    load_daily("../daily/" + config  + "/" + timeStart + "/" + timeStop);
                    load_data("../data/" + config + "/" + timeStart + "/" + timeStop);
                    loadConsumers("../consumer/" + config  + "/" + timeStart + "/" + timeStop);
                    if (consumerGroup) {
                    loadConsumerGroup("../consumerGroup/" + consumerGroup  + "/" + timeStart + "/" + timeStop);
                    }
                } else {
                    reduceMonth();
                }
                resetBrush();
                d3.selectAll(".brush").call(brush.clear());
            }
            brush_real_status = 0;
        }

    function brushed() {
        brush0 = brush.extent()[0];
        brush1 = brush.extent()[1];
        brush_status = 1;

    }

    function brushed_real() {
            brush0_real = brush_real.extent()[0];
            brush1_real = brush_real.extent()[1];
            brush_real_status = 1;
        }

    // brush tool to let us zoom and pan using the overview chart (i.e for energyday graph)
    brush = d3.svg.brush()
        .x(xEnergyDay)
        .on("brush", brushed)
        .on("brushend", brushend);

     //brush tool for real live graph
     brush_real = d3.svg.brush()
             .x(xl)
             .on("brush", brushed_real)
             .on("brushend", brushend_real);

    var color = ["lightblue", "lightgreen", "darkorange", "red"];
    var legend_colors = [
        ["inverter", "lightblue"],
        ["grid", "lightgreen"],
        ["generator", "darkorange"],
        ["error", "red"]
    ];

    var colour = d3.scale.ordinal()
        .range(color);

    function createAxis(xGrids, contextP, xAxisP, yAxis1P, axisYText, f,h, fx, y,brushAxis) {

        if (xGrids) {
            gridxP = contextP.append("g")
                .attr("class", "grid")
                .attr("transform", "translate(0," + h + ")")
                .call(fx()
                    .tickSize(-h, 0, 0)
                    .tickFormat("")
                );
        }

        gridyP = contextP.append("g")
            .attr("class", "grid")
            .call(y()
                .tickSize(-f, 0)
                .tickFormat("")
            );

        axisxP = contextP.append("g")
            .attr("class", "x axis")
            .attr("transform", "translate(0," + h + ")")
            .call(xAxisP);

        axisx_labelP = axisxP.append("text")
            .attr("x", 0)
            .attr("dx", "3.5em")
            .attr("dy", "3.5em")
            .style("text-anchor", "end")
            .attr("class", "axislabel")
            .text("Time GMT +01:00");

        axisyP = contextP.append("g")
            .attr("class", "y axis")
            .call(yAxis1P);
        axisy_labelP = axisyP.append("text")
            .attr("transform", "rotate(-90)")
            .attr("y", 0)
            .attr("dy", "1.5em")
            .style("text-anchor", "end")
            .attr("class", "axislabel")
            .text(axisYText);

        if (brushAxis) {
            contextP.append("g")
                .attr("class", "xc brush")
                .call(brush)
                .selectAll("rect")
                .attr("y", -6)
                .attr("height", h + 7);

        }

        var Axes = [];
        if (xGrids) {
            Axes.push(gridxP, gridyP, axisxP, axisyP, axisx_labelP, axisy_labelP);
        } else {
            Axes.push(gridyP, axisxP, axisyP, axisx_labelP, axisy_labelP);
        }
        return Axes;
    }

    function modifyData(dataObject) {
        var time;
        var counts;
        var i;
        var j;
        for (i = 0; i < dataObject.length; i++) {
            time = dataObject[i].time;
            counts = dataObject[i].counts;
            for (j = 0; j < counts.length; j++) {
                counts[j].time = time;
            }
        }
    }

    function drawDay(f, contextP, dataStore, svgP, yP, xP) {
        //Tip
        function tipCost(cost) {
            var message = '<p style = \"margin-bottom:1px; \"> Cost : &#8358;' + f(cost.inverter + cost.generator + cost.grid) + ' (';
            if (cost.inverter !== 0) {
                message += '<span style=\"color:#6666ff\">' + f(cost.inverter) + ' </span>';
            }
            if (cost.generator !== 0 && cost.inverter !== 0) {
                message += '+ <span style=\"color:darkorange\">  ' + f(cost.generator) + ' </span>';
            }else if (cost.generator !== 0 && cost.inverter === 0) {
                message += '<span style=\"color:darkorange\">  ' + f(cost.generator) + ' </span>';
            }
            if (cost.grid !== 0 && (cost.generator !== 0 || cost.inverter !== 0)) {
                message += '+ <span style=\"color:green\">  ' + f(cost.grid) + '</span>';
            }else if (cost.grid !== 0 && cost.generator === 0 && cost.inverter === 0) {
                message += '<span style=\"color:green\">  ' + f(cost.grid) + '</span>';
            }
            message += ')</p>';
            return message;
        }

        function tipEnergy(energy) {
            var message = '<p  style = \"margin-bottom:1px; \"> Energy : ' + powerGraphScale(energy.inverter + energy.generator + energy.grid) + 'h (';
            if (energy.inverter !== 0) {
                message += '<span style=\"color:#6666ff\">' + powerGraphScale(energy.inverter) + 'h </span>';
            }
            if (energy.generator !== 0) {
                message += '+ <span style=\"color:darkorange\">  ' + powerGraphScale(energy.generator) + 'h </span>';
            }
            if (energy.grid !== 0) {
                message += '+ <span style=\"color:green\">  ' + powerGraphScale(energy.grid) + 'h</span>';
            }
            message += ')</p>';
            return message;
        }

        function tipTime(time) {
            var message = '<p> Time : ' + f(time.inverter + time.generator + time.grid) + ' Hr (';
            if (time.inverter !== 0) {
                message += '<span style=\"color:#6666ff\">' + f(time.inverter) + ' Hr </span>';
            }
            if (time.generator !== 0) {
                message += '+ <span style=\"color:darkorange\">  ' + f(time.generator) + ' Hr </span>';
            }
            if (time.grid !== 0) {
                message += '+ <span style=\"color:green\">  ' + f(time.grid) + ' Hr</span>';
            }
            message += ')</p>';
            return message;
        }

       // Width of each bar in the monthly energy graph
        barWidth = (width2 - margin.left - margin.right) / new Date(date.getFullYear(), date.getMonth() + 1, 0).getDate();
        modifyData(dataStore);
        textMonth = svgEnergyDay.append("text").attr("x", (0.45 * width2))
            .attr("y", -2)
            .attr("font-size", "2em")
            .attr('transform', 'translate(0,0)')
            .attr("fill", "#96989E")
            .text(function () {
                if (monthGraphView) {
                  return  moment(date).format('MMMM YYYY');
                }else {
                  return  moment(date).format('YYYY');
                }
            });

        var tip = d3.tip()
            .attr('class', 'd3-tip')
            .offset([-10, 0])
            .html(function(d) {
                if (monthGraphView) {
                    return "<p style = \"margin-bottom:0; \" >" + moment(d.time).format('D MMMM YYYY') + "</p>" + tipCost(d.cost) + tipEnergy(d.energy) + tipTime(d.Time);
                }else{
                    return "<p style = \"margin-bottom:0; \" >" + moment(d.time).format('MMMM YYYY') + "</p>" + tipCost(d.cost) + tipEnergy(d.energy) + tipTime(d.Time);
                }
            });

        svgP.call(tip);
        barsP = contextP.append("g")
            .attr("class", "bars")
            // a group for each stack of bars, positioned in the correct x position
            .selectAll(".bar.stack")
            .data(dataStore)
        .enter()
        .append("g")
            .attr("class", "bar stack")
            .attr("transform", function(d) {
                return "translate(" + (xP(d.time) - (barWidth / 2)) + ",0)";
            })
            .on("click", function(d) {
                showDay(d.time);
            })
            // a bar for each value in the stack, positioned in the correct y positions
            .selectAll("rect")
            .data(function(d) {
                return d.counts;

            })
            .enter().append("rect")
            .attr("class", "bar")
            .attr("width", barWidth)
            .attr("y", function(d) {
                return yP(d.y1);
            })
            .attr("height", function(d) {
                return yP(d.y0) - yP(d.y1);
            })
            .on('mouseover', tip.show)
            .on('mouseout', tip.hide)
            .on('mouseup', tip.hide)
            .style("fill", function(d) {
                return colour(d.name);
            });

        legend = svgP.append("g")
            .attr("class", "legend")
               .attr("height", 100)
               .attr("width", 100)
               .attr('transform', 'translate('+ ((0.45 * width2)-150) +',40)');

        var legendRect = legend.selectAll('rect').data(legend_colors);

        legendRect.enter()
            .append("rect")
            .attr("y", height - 15)
            .attr("width", 9)
            .attr("height", 9);

        legendRect
            .attr("x", function(d, i) {
                return i * 80;
            })
            .style("fill", function(d) {
                return d[1];
            });

        var legendText = legend.selectAll('text')
            .data(legend_colors);

        legendText.enter()
            .append("text")
            .attr("y", height + 10);

        legendText
            .attr("x", function(d, i) {
                return i * 80 ;
            })
            .style("font-size", "14px")
            .style("fill", "white")
            .text(function(d) {
                return d[0];
            });

        function showDay(selectedDay) {
            day = new Date(selectedDay);
            //Check if the selected day is today and update the real time boolean
            realtime  = (day.getDate() == new Date().getDate()) && (day.getMonth() == new Date().getMonth()) && (day.getFullYear() == new Date().getFullYear()) ? true : false ;
            remove_livegraphElements();
            remove_graphElements();
            var commonTimeStart = moment(new Date(day)).format("YYYY-MM-DDTHH:mm:ss.SSSZ");
            var commonTimeStop = moment(new Date(day.getFullYear(), day.getMonth(), day.getDate(), 23, 59, 59)).format("YYYY-MM-DDTHH:mm:ss.SSSZ");
            exportStartTime = commonTimeStart;
            exportEndTime = commonTimeStop;
            setExportDateTimePicker(exportStartTime,exportEndTime);
            if (isHiddenFetchDataContainer) {
                onPeriodChanged();
            }
            load_daily("../daily/" + config + "/" + moment(new Date(day.getFullYear(), day.getMonth(), day.getDate(),0, 1, 30)).format("YYYY-MM-DDTHH:mm:ss.SSSZ") + "/" + commonTimeStop);
            load_data("../data/" + config + "/" + commonTimeStart + "/" + commonTimeStop);
            loadConsumers("../consumer/" + config  + "/" + commonTimeStart + "/" + commonTimeStop);
            if (consumerGroup) {
                loadConsumerGroup("../consumerGroup/" + consumerGroup  + "/" + commonTimeStart + "/" + commonTimeStop);
            }
        }

        return barsP;

    }

    function bucket_handlerEnergyDay(buckets, total, count, debug) {
        var count0 = {
            name: "inverter",
            y0: 0,
            y1: 0,
            cost: 0
        };
        var count1 = {
            name: "grid",
            y0: 0,
            y1: 0,
            cost: 0
        };
        var count2 = {
            name: "generator",
            y0: 0,
            y1: 0,
            cost: 0
        };
        var count3 = {
            name: "error",
            y0: 0,
            y1: 0,
            cost: 0
        };
        if (buckets == "error") {
            counts = [count0, count1, count2, count3];
            return counts;
        }
        var cost = {
            inverter: 0,
            grid: 0,
            generator: 0
        };
        var energy = {
            inverter: 0,
            grid: 0,
            generator: 0
        };
        var time = {
            inverter: 0,
            grid: 0,
            generator: 0
        };

        var ratio = count;
        var next = 0;
        Object.keys(buckets).forEach(function(key) {
            ratio = buckets[key].count / count;
            if (buckets[key].source == "inverter") {
                count0.y0 = next;
                count0.y1 = next = !energySwitch ? count0.y0 +((buckets[key].count*5)/3600) : count0.y0 + buckets[key].energy;
                cost.inverter = buckets[key].cost;
                energy.inverter = buckets[key].energy;
                time.inverter = ((buckets[key].count*5)/3600);
            }
            if (buckets[key].source == "grid") {
                count1.y0 = next;
                count1.y1 = next = !energySwitch ? count1.y0 + ((buckets[key].count*5)/3600) : count1.y0 + buckets[key].energy;
                cost.grid = buckets[key].cost;
                energy.grid = buckets[key].energy;
                time.grid = ((buckets[key].count*5)/3600);
            }
            if (buckets[key].source == "generator") {
                count2.y0 = next;
                count2.y1 = next = !energySwitch ? count2.y0 + ((buckets[key].count*5)/3600) : count2.y0 + buckets[key].energy ;
                cost.generator = buckets[key].cost;
                energy.generator = buckets[key].energy;
                time.generator = ((buckets[key].count*5)/3600);
            }
            if (buckets[key].source == "zero") {
                count3.y0 = next;
                count3.y1 = next = !energySwitch ? count3.y0 + ((buckets[key].count*5)/3600) : count3.y0 + buckets[key].energy;
                count3.cost = buckets[key].cost;
            }
            counts = [count0, count1, count2, count3];

        });

        if (debug > 0) {
            console.log("return", counts);
        }
        counts.forEach(function(d) {
            d.cost = cost;
            d.energy = energy;
            d.Time = time;
        });

        //
        return counts;
    }

    var last;
    var bcount;
    var elem = document.getElementById("realtime_stuff");
    /* var graph = document.getElementById("real");*/

    function eventHandlerAgg() {   //Event handler for comet updates
        if (dayRealtime) {
        var URL = "../daily/" + config + "/" + moment(new Date(day)).format("YYYY-MM-DDTHH:mm:ss.SSSZ") + "/" + moment(new Date(day.getFullYear(), day.getMonth(), day.getDate(), 23, 59, 59)).format("YYYY-MM-DDTHH:mm:ss.SSSZ");
        d3.json(URL, function(error, json) {
            if (error) return console.warn(error);
            plotdata_daily = json.aggregations.Power.buckets;
            plotdata_daily.forEach(function(d) {
                d.src = [];
            });
            plotdata_daily.forEach(function(d) {
                d.buckets = d.SourceType.buckets;
            });

            dataDaily = [];  // Temporary data store

            plotdata_daily.forEach(function(d) {
                for (i = 0; i < d.buckets.length; i++) {
                    d.src.push({
                        "key": d.buckets[i].key,
                        "value": d.buckets[i].powerStats.avg
                    });
                }
            });

            plotdata_daily.forEach(function(d) {
                for (i = 0; i < d.src.length; i++) {
                    d[d.src[i].key] = d.src[i].value;
                }
            });

            plotdata_daily.forEach(function(d) {
                prepareDataDaily(d);
            });

            // Nesting data
            newDataset = ["inverter", "grid", "generator"].map(function(n) {
                return {
                    key: n,
                    values: dataDaily.map(function(d) {
                        return {
                            x: d.time,
                            y: d[n],
                            y0: 0
                        };
                    })
                };
            });

            axisly.transition().duration(1500).ease("sin-in-out")
               .call(yAxis_realtime);

            yl.domain([0, d3.extent(dataDaily, function(d) {
                              return d.total;
                          })[1]]);

            Group.select("path").data(stack(newDataset)).transition().attr("d", function(d) {
                    return area(d.values);
                }).attr("class", "valgroup")
                .style("fill", function(d, i) {
                    return colorScale(i);
                });

        });
      }
    }

    var voltage1Data = [];
    var voltage2Data = [];
    var voltage3Data = [];
    var current1Data = [];
    var current2Data = [];
    var current3Data = [];
    var pf1Data = [];
    var pf2Data = [];
    var pf3Data = [];
    var voltages = [];
    var currentData = [];
    var pfData = [];

    function eventHandler(e) {

            if (minRealTime){
                e.detail[e.detail.SourceType]= e.detail.Power_real_total;
                var newData = [{
                    "time": parseTimeISO(moment(e.detail.Time).format("YYYY-MM-DD HH:mm:ss")),
                    "inverter": e.detail.inverter || 0,
                    "grid": e.detail.grid || 0,
                    "generator": e.detail.generator || 0,
                    "total" : (e.detail.inverter|| 0) + (e.detail.grid||0) +(e.detail.generator||0)
                }];

                dataDaily.push({
                   "time": parseTimeISO(moment(e.detail.Time).format("YYYY-MM-DD HH:mm:ss")),
                   "inverter": e.detail.inverter || 0,
                   "grid": e.detail.grid || 0,
                   "generator": e.detail.generator || 0,
                   "total" : (e.detail.inverter|| 0) + (e.detail.grid||0) +(e.detail.generator||0)
                });

                var newDataStacked = ["inverter", "grid", "generator"].map(function(n) {
                    return {
                        key: n,
                        values: newData.map(function(d) {
                            return {
                                x: d.time,
                                y: d[n],
                                y0: 0
                            };
                        })
                    };
                });

                newDataset.forEach(function(d){
                  newDataStacked.forEach(function(x){
                    if(x.key === d.key){
                        d.values.push(x.values[0]);
                    }
                  });
                });

                axisly.transition().duration(1500).ease("sin-in-out")
                   .call(yAxis_realtime);

                axislx
                   .transition()
                   .call(d3.svg.axis().scale(xl).orient("bottom"));

                dataDaily.shift();
                newDataset.forEach(function(d){
                    (d.values.shift());
                });

                yl.domain([0, d3.extent(dataDaily, function(d) {
                                  return d.total;
                              })[1]]);

                xl.domain(d3.extent(dataDaily, function(d) {
                    return d.time;
                }));

                Group.select("path").data(stack(newDataset)).transition().attr("d", function(d) {
                        return area(d.values);
                    }).attr("class", "valgroup")
                    .style("fill", function(d, i) {
                        return colorScale(i);
                    });
            }

            //voltage
                voltage1Data.push({
                    "time":  parseTimeISO(moment(e.detail.Time).format("YYYY-MM-DD HH:mm:ss")),
                    "voltage": e.detail.Voltage1,
                    "sourceType": e.detail.SourceType
                });
                voltage2Data.push({
                    "time":  parseTimeISO(moment(e.detail.Time).format("YYYY-MM-DD HH:mm:ss")),
                    "voltage": e.detail.Voltage2,
                    "sourceType": e.detail.SourceType
                });
                voltage3Data.push({
                    "time":  parseTimeISO(moment(e.detail.Time).format("YYYY-MM-DD HH:mm:ss")),
                    "voltage": e.detail.Voltage3,
                    "sourceType":  e.detail.SourceType,
                });

            //current
                current1Data.push({
                    "time":  parseTimeISO(moment(e.detail.Time).format("YYYY-MM-DD HH:mm:ss")),
                    "current":  e.detail.Current1,
                    "sourceType":  e.detail.SourceType
                });
                current2Data.push({
                    "time":  parseTimeISO(moment(e.detail.Time).format("YYYY-MM-DD HH:mm:ss")),
                    "current":  e.detail.Current2,
                    "sourceType":  e.detail.SourceType
                });
                current3Data.push({
                    "time":  parseTimeISO(moment(e.detail.Time).format("YYYY-MM-DD HH:mm:ss")),
                    "current":  e.detail.Current3,
                    "sourceType": e.detail.SourceType,

            //pf
                });
                pf1Data.push({
                    "time":  parseTimeISO(moment(e.detail.Time).format("YYYY-MM-DD HH:mm:ss")),
                    "pf": e.detail.PowerFactor1,
                    "sourceType":  e.detail.SourceType
                });
                pf2Data.push({
                    "time": parseTimeISO(moment(e.detail.Time).format("YYYY-MM-DD HH:mm:ss")),
                    "pf":  e.detail.PowerFactor2,
                    "sourceType":  e.detail.SourceType
                });
                pf3Data.push({
                    "time":  parseTimeISO(moment(e.detail.Time).format("YYYY-MM-DD HH:mm:ss")),
                    "pf": e.detail.PowerFactor3,
                    "sourceType": e.detail.SourceType,
                });

               currentElements[1]
                   .transition()
                   .call(d3.svg.axis().scale(xcurrent).orient("bottom"));
               voltageElements[1]
                   .transition()
                   .call(d3.svg.axis().scale(xv).orient("bottom"));
               pfElements[1]
                   .transition()
                   .call(d3.svg.axis().scale(xv).orient("bottom"));

               currentElements[2].transition().duration(1500).ease("sin-in-out")
                   .call(yAxis1Current);
               voltageElements[2].transition().duration(1500).ease("sin-in-out")
                   .call(yAxis1v);
               pfElements[2].transition().duration(1500).ease("sin-in-out")
                   .call(yAxis1pf);


               // pop the old data point off the front
               voltage1Data.shift();
               voltage2Data.shift();
               voltage3Data.shift();
               current1Data.shift();
               current2Data.shift();
               current3Data.shift();
               pf1Data.shift();
               pf2Data.shift();
               pf3Data.shift();

               voltages = voltage1Data.concat(voltage2Data, voltage3Data);
               currentData = current1Data.concat(current2Data, current3Data);
               pfData = pf1Data.concat(pf2Data, pf3Data);

               voltageBounds = getAxisBounds(voltages,'voltage');
               phaseBounds = getAxisBounds(pfData,'pf');

               xv.domain(d3.extent(voltages, function(d) {
                   return d.time;
               }));
               xcurrent.domain(d3.extent(currentData, function(d) {
                   return d.time;
               }));

               yv.domain([
                   voltageBounds.axisMin,voltageBounds.axisMax
               ]);

               ypf.domain([
                   phaseBounds.axisMin,phaseBounds.axisMax
               ]);

               ycurrent.domain([0, d3.extent(currentData, function(d) {
                   return d.current;
               })[1]]);


        // transition the line
       linedVoltage[0].transition().attr("d", lineVoltage);
       linedVoltage[1].transition().attr("d", lineVoltage);
       linedVoltage[2].transition().attr("d", lineVoltage);

       linedCurrent[0].transition().attr("d", lineCurrent);
       linedCurrent[1].transition().attr("d", lineCurrent);
       linedCurrent[2].transition().attr("d", lineCurrent);

       linedpf[0].transition().attr("d", linepf);
       linedpf[1].transition().attr("d", linepf);
       linedpf[2].transition().attr("d", linepf);

    }

    // Listen for the event.
    try {
        elem.addEventListener('build', function(e) {
            eventHandler(e);
        }, false);

        elem.addEventListener('aggregation', function() {
            eventHandlerAgg();
        }, false);
    } catch(err) {
        console.log(err);
    }


    function load_monthly(URL) {
        var targetEnergy = document.getElementById('energyDay_graph');
        var spinnerEnergy = new Spinner(opts).spin(targetEnergy);
        monthGraphView = true;
        d3.json(URL, function(error, json) {
            if (error) return console.warn(error);

            EnergyByDay = json.aggregations.EnergyDay.buckets;
            EnergyByDay.forEach(function(d) {
                d.time = last = parseTimeISO(moment(d.key).format("YYYY-MM-DD HH:mm:ss"));
                d.total = energySwitch ? d.energy.sum : (d.doc_count *5)/3600;
                d.buckets = d.SourceType.buckets;

                if (d.buckets.length === 0) {
                    console.info("no buckets");
                    d.src = "error";
                    d.counts = bucket_handlerEnergyDay(d.src, d.total, 0, 0);
                } else if (d.buckets.length > 5) {
                    console.error("too many buckets");
                    console.error(d.buckets);
                    d.src = "error";
                } else {
                    if (d.buckets.length == 1) {
                        src0 = {
                            "source": d.buckets[0].key,
                            "count": d.buckets[0].doc_count,
                            "cost": d.buckets[0].CostStats.sum,
                            "energy": d.buckets[0].energyStats.sum
                        };
                        d.src = {
                            "0": src0
                        };
                        bcount = src0.count;
                    } else if (d.buckets.length == 2) {
                        src0 = {
                            "source": d.buckets[0].key,
                            "count": d.buckets[0].doc_count,
                            "cost": d.buckets[0].CostStats.sum,
                            "energy": d.buckets[0].energyStats.sum
                        };
                        src1 = {
                            "source": d.buckets[1].key,
                            "count": d.buckets[1].doc_count,
                            "cost": d.buckets[1].CostStats.sum,
                            "energy": d.buckets[1].energyStats.sum
                        };
                        bcount = src0.count + src1.count;
                        d.src = {
                            "0": src0,
                            "1": src1
                        };
                    } else if (d.buckets.length == 3) {
                        src0 = {
                            "source": d.buckets[0].key,
                            "count": d.buckets[0].doc_count,
                            "cost": d.buckets[0].CostStats.sum,
                            "energy": d.buckets[0].energyStats.sum
                        };
                        src1 = {
                            "source": d.buckets[1].key,
                            "count": d.buckets[1].doc_count,
                            "cost": d.buckets[1].CostStats.sum,
                            "energy": d.buckets[1].energyStats.sum
                        };
                        src2 = {
                            "source": d.buckets[2].key,
                            "count": d.buckets[2].doc_count,
                            "cost": d.buckets[2].CostStats.sum,
                            "energy": d.buckets[2].energyStats.sum
                        };
                        d.src = {
                            "0": src0,
                            "1": src1,
                            "2": src2
                        };
                        bcount = src0.count + src1.count + src2.count;
                    } else if (d.buckets.length == 4) {
                        src0 = {
                            "source": d.buckets[0].key,
                            "count": d.buckets[0].doc_count,
                            "cost": d.buckets[0].CostStats.sum,
                            "energy": d.buckets[0].energyStats.sum
                        };
                        src1 = {
                            "source": d.buckets[1].key,
                            "count": d.buckets[1].doc_count,
                            "cost": d.buckets[1].CostStats.sum,
                            "energy": d.buckets[1].energyStats.sum
                        };
                        src2 = {
                            "source": d.buckets[2].key,
                            "count": d.buckets[2].doc_count,
                            "cost": d.buckets[2].CostStats.sum,
                            "energy": d.buckets[2].energyStats.sum
                        };
                        src3 = {
                            "source": d.buckets[3].key,
                            "count": d.buckets[3].doc_count,
                            "cost": d.buckets[3].CostStats.sum,
                            "energy": d.buckets[3].energyStats.sum
                        };
                        d.src = {
                            "0": src0,
                            "1": src1,
                            "2": src2,
                            "3": src3
                        };
                        bcount = src0.count + src1.count + src2.count + src3.count;
                    } else if (d.buckets.length == 5) {
                        src0 = {
                            "source": d.buckets[0].key,
                            "count": d.buckets[0].doc_count,
                            "cost": d.buckets[0].CostStats.sum,
                            "energy": d.buckets[0].energyStats.sum
                        };
                        src1 = {
                            "source": d.buckets[1].key,
                            "count": d.buckets[1].doc_count,
                            "cost": d.buckets[1].CostStats.sum,
                            "energy": d.buckets[1].energyStats.sum
                        };
                        src2 = {
                            "source": d.buckets[2].key,
                            "count": d.buckets[2].doc_count,
                            "cost": d.buckets[2].CostStats.sum,
                            "energy": d.buckets[2].energyStats.sum
                        };
                        src3 = {
                            "source": d.buckets[3].key,
                            "count": d.buckets[3].doc_count,
                            "cost": d.buckets[3].CostStats.sum,
                            "energy": d.buckets[3].energyStats.sum

                        };
                        src4 = {
                            "source": d.buckets[4].key,
                            "count": d.buckets[4].doc_count,
                            "cost": d.buckets[4].CostStats.sum,
                            "energy": d.buckets[4].energyStats.sum
                        };
                        d.src = {
                            "0": src0,
                            "1": src1,
                            "2": src2,
                            "3": src3,
                            "4": src4
                        };
                        bcount = src0.count + src1.count + src2.count + src3.count + src4.count;
                    }
                    d.counts = bucket_handlerEnergyDay(d.src, d.total, bcount, 0);
                }
            });
            var axisTitle = energySwitch ? "Energy (Wh)"  : "Time (Hr)";
            xEnergyDay.domain([d3.time.hour.offset(new Date(date.getFullYear(), date.getMonth(), 1), -23), d3.time.hour.offset(new Date(date.getFullYear(), date.getMonth() + 1, 0), 23)]);

            yEnergyDay.domain([0, d3.extent(EnergyByDay, function(d) {
                return d.total;
            })[1]]);

            EnergyByDayElements = createAxis(0, contextEnergyDay, xAxisEnergyDay, yAxis1EnergyDay, axisTitle, width2, height,make_x_axis,make_y_axis, 1); // Battery DOD


            bars_energyDay = drawDay(roundTo, contextEnergyDay, EnergyByDay, svgEnergyDay, yEnergyDay, xEnergyDay);
            spinnerEnergy.stop();
        });
    }

    function load_yearly(URL) {
        var targetEnergy = document.getElementById('energyDay_graph');
        var spinnerEnergy = new Spinner(opts).spin(targetEnergy);
        monthGraphView = false;
        d3.json(URL, function(error, json) {
            if (error) return console.warn(error);

            EnergyByYear = json.aggregations.EnergyYear.buckets;
            EnergyByYear.forEach(function(d) {
                d.time = last = parseTimeISO(moment(d.key).format("YYYY-MM-DD HH:mm:ss"));
                d.total = energySwitch ? d.energy.sum : (d.doc_count *5)/3600;
                d.buckets = d.SourceType.buckets;

                if (d.buckets.length === 0) {
                    console.info("no buckets");
                    d.src = "error";
                    d.counts = bucket_handlerEnergyDay(d.src, d.total, 0, 0);
                } else if (d.buckets.length > 5) {
                    console.error("too many buckets");
                    console.error(d.buckets);
                    d.src = "error";
                } else {
                    if (d.buckets.length == 1) {
                        src0 = {
                            "source": d.buckets[0].key,
                            "count": d.buckets[0].doc_count,
                            "cost": d.buckets[0].CostStats.sum,
                            "energy": d.buckets[0].energyStats.sum
                        };
                        d.src = {
                            "0": src0
                        };
                        bcount = src0.count;
                    } else if (d.buckets.length == 2) {
                        src0 = {
                            "source": d.buckets[0].key,
                            "count": d.buckets[0].doc_count,
                            "cost": d.buckets[0].CostStats.sum,
                            "energy": d.buckets[0].energyStats.sum
                        };
                        src1 = {
                            "source": d.buckets[1].key,
                            "count": d.buckets[1].doc_count,
                            "cost": d.buckets[1].CostStats.sum,
                            "energy": d.buckets[1].energyStats.sum
                        };
                        bcount = src0.count + src1.count;
                        d.src = {
                            "0": src0,
                            "1": src1
                        };
                    } else if (d.buckets.length == 3) {
                        src0 = {
                            "source": d.buckets[0].key,
                            "count": d.buckets[0].doc_count,
                            "cost": d.buckets[0].CostStats.sum,
                            "energy": d.buckets[0].energyStats.sum
                        };
                        src1 = {
                            "source": d.buckets[1].key,
                            "count": d.buckets[1].doc_count,
                            "cost": d.buckets[1].CostStats.sum,
                            "energy": d.buckets[1].energyStats.sum
                        };
                        src2 = {
                            "source": d.buckets[2].key,
                            "count": d.buckets[2].doc_count,
                            "cost": d.buckets[2].CostStats.sum,
                            "energy": d.buckets[2].energyStats.sum
                        };
                        d.src = {
                            "0": src0,
                            "1": src1,
                            "2": src2
                        };
                        bcount = src0.count + src1.count + src2.count;
                        //d.counts = bucket_handler(source,d.total,bcount);;
                    } else if (d.buckets.length == 4) {
                        src0 = {
                            "source": d.buckets[0].key,
                            "count": d.buckets[0].doc_count,
                            "cost": d.buckets[0].CostStats.sum,
                            "energy": d.buckets[0].energyStats.sum
                        };
                        src1 = {
                            "source": d.buckets[1].key,
                            "count": d.buckets[1].doc_count,
                            "cost": d.buckets[1].CostStats.sum,
                            "energy": d.buckets[1].energyStats.sum
                        };
                        src2 = {
                            "source": d.buckets[2].key,
                            "count": d.buckets[2].doc_count,
                            "cost": d.buckets[2].CostStats.sum,
                            "energy": d.buckets[2].energyStats.sum
                        };
                        src3 = {
                            "source": d.buckets[3].key,
                            "count": d.buckets[3].doc_count,
                            "cost": d.buckets[3].CostStats.sum,
                            "energy": d.buckets[3].energyStats.sum
                        };
                        d.src = {
                            "0": src0,
                            "1": src1,
                            "2": src2,
                            "3": src3
                        };
                        bcount = src0.count + src1.count + src2.count + src3.count;
                        //d.counts = bucket_handler(source,d.total,bcount);
                    } else if (d.buckets.length == 5) {
                        src0 = {
                            "source": d.buckets[0].key,
                            "count": d.buckets[0].doc_count,
                            "cost": d.buckets[0].CostStats.sum,
                            "energy": d.buckets[0].energyStats.sum
                        };
                        src1 = {
                            "source": d.buckets[1].key,
                            "count": d.buckets[1].doc_count,
                            "cost": d.buckets[1].CostStats.sum,
                            "energy": d.buckets[1].energyStats.sum
                        };
                        src2 = {
                            "source": d.buckets[2].key,
                            "count": d.buckets[2].doc_count,
                            "cost": d.buckets[2].CostStats.sum,
                            "energy": d.buckets[2].energyStats.sum
                        };
                        src3 = {
                            "source": d.buckets[3].key,
                            "count": d.buckets[3].doc_count,
                            "cost": d.buckets[3].CostStats.sum,
                            "energy": d.buckets[3].energyStats.sum

                        };
                        src4 = {
                            "source": d.buckets[4].key,
                            "count": d.buckets[4].doc_count,
                            "cost": d.buckets[4].CostStats.sum,
                            "energy": d.buckets[4].energyStats.sum
                        };
                        d.src = {
                            "0": src0,
                            "1": src1,
                            "2": src2,
                            "3": src3,
                            "4": src4
                        };
                        bcount = src0.count + src1.count + src2.count + src3.count + src4.count;
                        //d.counts = bucket_handler(source,d.total,bcount);
                    }

                    d.counts = bucket_handlerEnergyDay(d.src, d.total, bcount, 0);
                }

            });
            var axisTitle = energySwitch ? "Energy (Wh)"  : "Time (Hr)";
            xEnergyDay.domain([d3.time.day.offset(new Date(date.getFullYear(), 0),-29), (new Date(date.getFullYear(),12, 0, 23,59,59))]);

            yEnergyDay.domain([0, d3.extent(EnergyByYear, function(d) {
                return d.total;
            })[1]]);

            xAxisEnergyMonth = d3.svg.axis()
                .scale(xEnergyDay)
                .orient("bottom")
                .ticks(d3.time.month, tickDays)
                .tickFormat(d3.time.format('%B'))
                .tickSize(5);

            EnergyByDayElements = createAxis(0, contextEnergyDay, xAxisEnergyMonth, yAxis1EnergyDay, axisTitle, width2, height,make_x_axis,make_y_axis, 1); // Battery DOD

            bars_energyDay = drawDay(roundTo, contextEnergyDay, EnergyByYear, svgEnergyDay, yEnergyDay, xEnergyDay);
            spinnerEnergy.stop();
        });
    }

    function loadConsumers (URL) {
        d3.json(URL, function(error, json) {
            if (error) return console.warn(error);
            var consumerEnergy = json.aggregations.consumerEnergy.buckets;
            var consumerCost = json.aggregations.consumerCost.buckets;
            consumerData = getConsumerData(consumerEnergy,consumerCost);
            ConsumerEnergyData = consumerData.energy;
            ConsumerCostData = consumerData.cost;
            var dimension =  ((+width/2 + width/8) < 700 ) ? 700 : (+width/2 + width/8);
            ConfConsumer.size.canvasHeight = dimension;
            ConfConsumer.size.canvasWidth = dimension;
            try {
                $("#consumerCharts").remove();
                $("#consumerCostCharts").remove();
            } catch (err) {
                console.info("first run no previous consumer pies");
            }
            createPie(ConfConsumer,ConsumerEnergyData,"Client's Energy Consumption","distributionCharts","consumerCharts","Energy : {value} Wh ");
            document.getElementById("consumerCharts").setAttribute("viewBox", "0 0 " + String(ConfConsumer.size.canvasWidth) + " " + String(ConfConsumer.size.canvasHeight));
            document.getElementById("consumerCharts").setAttribute("perserveAspectRatio", "xMidYMid");

            createPie(ConfConsumer,ConsumerCostData,"Client's Cost","distributionChartsCost","consumerCostCharts","Cost : {value} Naira ");
            document.getElementById("consumerCostCharts").setAttribute("viewBox", "0 0 " + String(ConfConsumer.size.canvasWidth) + " " + String(ConfConsumer.size.canvasHeight));
            document.getElementById("consumerCostCharts").setAttribute("perserveAspectRatio", "xMidYMid");
        });
    }

    function getAxisBounds(data,index){
        var axisMin = d3.min(data, function(d) {
                          return d[index];
                      });
        var axisMax = d3.max(data, function(d) {
                          return d[index];
                      });
        var bounds = {};
        if (axisMax === axisMin) {
            bounds.axisMin = parseInt(axisMax) - parseInt((0.1 * axisMax));
            bounds.axisMax = parseInt(axisMax) + parseInt(0.1 * axisMax);
            return bounds;
        }
        else {
            bounds.axisMin = axisMin;
            bounds.axisMax = axisMax;
            return bounds;
        }
    }

    function load_recent(URL) {
        d3.json(URL, function(error, json) {
            if (error) return console.warn(error);

           PhaseData = json.hits.hits;
           voltage1Data = [];
           voltage2Data = [];
           voltage3Data = [];
           current1Data = [];
           current2Data = [];
           current3Data = [];
           pf1Data =[];
           pf2Data = [];
           pf3Data = [];

            PhaseData.forEach(function(d) {
            //voltage
                voltage1Data.push({
                    "time": parseTimeISO(moment(d._source.Time).format("YYYY-MM-DD HH:mm:ss")),
                    "voltage": d._source.Voltage1,
                    "sourceType": d._source.SourceType
                });
                voltage2Data.push({
                    "time": parseTimeISO(moment(d._source.Time).format("YYYY-MM-DD HH:mm:ss")),
                    "voltage": d._source.Voltage2,
                    "sourceType": d._source.SourceType
                });
                voltage3Data.push({
                    "time": parseTimeISO(moment(d._source.Time).format("YYYY-MM-DD HH:mm:ss")),
                    "voltage": d._source.Voltage3,
                    "sourceType": d._source.SourceType,
                });

            //current
                current1Data.push({
                    "time": parseTimeISO(moment(d._source.Time).format("YYYY-MM-DD HH:mm:ss")),
                    "current": d._source.Current1,
                    "sourceType": d._source.SourceType
                });
                current2Data.push({
                    "time": parseTimeISO(moment(d._source.Time).format("YYYY-MM-DD HH:mm:ss")),
                    "current": d._source.Current2,
                    "sourceType": d._source.SourceType
                });
                current3Data.push({
                    "time": parseTimeISO(moment(d._source.Time).format("YYYY-MM-DD HH:mm:ss")),
                    "current": d._source.Current3,
                    "sourceType": d._source.SourceType,

            //pf
                });
                pf1Data.push({
                    "time": parseTimeISO(moment(d._source.Time).format("YYYY-MM-DD HH:mm:ss")),
                    "pf": d._source.PowerFactor1,
                    "sourceType": d._source.SourceType
                });
                pf2Data.push({
                    "time": parseTimeISO(moment(d._source.Time).format("YYYY-MM-DD HH:mm:ss")),
                    "pf": d._source.PowerFactor2,
                    "sourceType": d._source.SourceType
                });
                pf3Data.push({
                    "time": parseTimeISO(moment(d._source.Time).format("YYYY-MM-DD HH:mm:ss")),
                    "pf": d._source.PowerFactor3,
                    "sourceType": d._source.SourceType,
                });
            });
            voltages = voltage1Data.concat(voltage2Data, voltage3Data);
            currentData = current1Data.concat(current2Data, current3Data);
            pfData = pf1Data.concat(pf2Data,pf3Data);

            console.log(pfData);
            console.log(pf1Data);
            console.log(pf3Data);
            console.log(pf2Data);

            var phaseColors = [
                ["phase1", "red"],
                ["phase2", "yellow"],
                ["phase3", "blue"]
            ];

            var voltageBounds = getAxisBounds(voltages,'voltage');
            var phaseBounds = getAxisBounds(pfData,'pf');
            console.log(voltageBounds);

            //Voltage drawing
            xv.domain(d3.extent(voltages, function(d) {
                return d.time;
            }));

            yv.domain([
                voltageBounds.axisMin,voltageBounds.axisMax
            ]);

            ypf.domain([
                phaseBounds.axisMin,phaseBounds.axisMax
            ]);

            xcurrent.domain(d3.extent(currentData, function(d) {
                            return d.time;
                        }));

            ycurrent.domain([
                d3.min(currentData, function(d) {
                    return d.current;
                }),
                d3.max(currentData, function(d) {
                    return d.current;
                })
            ]);

            voltageElements = createAxis (0,contextVoltage,xAxisv,yAxis1v,'Voltage (V)',width2,height3,make_x_axis,make_y_axisf,0);
            currentElements = createAxis (0,contextCurrent,xAxiscurrent,yAxis1Current,'Current (A)',width2,height3,make_x_axis,make_y_axisf,0);
            pfElements = createAxis (0,contextpf,xAxisv,yAxis1pf,' ',width2,height3,make_x_axis,make_y_axisf,0);


            linedVoltage[0] = contextVoltage.append('path')
                            .datum(voltage1Data)
                            .attr("d", lineVoltage)
                            .attr('stroke', 'red')
                            .attr('stroke-width', 2)
                            .attr('fill', 'none');

                /*.attr('d', lineVoltage(voltage1Data))
                .attr('stroke', 'red')
                .attr('stroke-width', 2)
                .attr('fill', 'none');*/

            linedVoltage[1]=contextVoltage.append('path')
                           .datum(voltage2Data)
                           .attr("d", lineVoltage)
                           .attr('stroke', 'yellow')
                           .attr('stroke-width', 2)
                           .attr('fill', 'none');


            linedVoltage[2]= contextVoltage.append('path')
                            .datum(voltage3Data)
                            .attr("d", lineVoltage)
                            .attr('stroke', 'blue')
                            .attr('stroke-width', 2)
                            .attr('fill', 'none');

            //Current Drawing

            linedCurrent[0] = contextCurrent .append('path')
                              .datum(current1Data)
                              .attr("d", lineCurrent)
                              .attr('stroke', 'red')
                              .attr('stroke-width', 2)
                              .attr('fill', 'none');

            linedCurrent[1] = contextCurrent.append('path')
                             .datum(current2Data)
                             .attr("d", lineCurrent)
                             .attr('stroke', 'yellow')
                             .attr('stroke-width', 2)
                             .attr('fill', 'none');

            linedCurrent[2] = contextCurrent.append('path')
                             .datum(current3Data)
                             .attr("d", lineCurrent)
                             .attr('stroke', 'blue')
                             .attr('stroke-width', 2)
                             .attr('fill', 'none');

            //pf Drawing
            linedpf[0] = contextpf.append('path')
                            .datum(pf1Data)
                            .attr("d", linepf)
                            .attr('stroke', 'red')
                            .attr('stroke-width', 2)
                            .attr('fill', 'none');

            linedpf[1] = contextpf.append('path')
                           .datum(pf2Data)
                           .attr("d", linepf)
                           .attr('stroke', 'yellow')
                           .attr('stroke-width', 2)
                           .attr('fill', 'none');

            linedpf[2] = contextpf.append('path')
                           .datum(pf3Data)
                           .attr("d", linepf)
                           .attr('stroke', 'blue')
                           .attr('stroke-width', 2)
                           .attr('fill', 'none');

        phaseLegend(svgCurrent);
        phaseLegend(svgVoltage);
        phaseLegend(svgpf);


        function phaseLegend (svgPhase) {

            phaselegend = svgPhase.append("g")
                .attr("class", "legend")
                .attr("height", 100)
                .attr("width", 100)
                .attr('transform', 'translate('+ ((0.45 * width2)-150) +',50)');

            var legendRectPhase = phaselegend.selectAll('rect').data(phaseColors);

            legendRectPhase.enter()
                .append("rect")
                .attr("y", height3 - 18)
                .attr("width", 10)
                .attr("height", 10);

            legendRectPhase
                .attr("x", function(d, i) {
                    return i * 80 ;
                })
                .style("fill", function(d) {
                    return d[1];
                });

            var legendTextPhase = phaselegend.selectAll('text')
                .data(phaseColors);

            legendTextPhase.enter()
                .append("text")
                .attr("y", height3 + 6);

            legendTextPhase
                .attr("x", function(d, i) {
                    return i * 80;
                })
                .style("font-size", "12px")
                .style("fill", "white")
                .text(function(d) {
                    return d[0];
                });

        }

        });
    }

    function loadConsumerGroup (URL) {
        d3.json(URL, function(error, json) {
            if (error) return console.warn(error);
            var consumerGroupEnergy = json.aggregations.consumerGroup.buckets;
            var consumerGroupCost = json.aggregations.consumerGroupCost.buckets;
            consumerGroupData = getConsumerGroupData(consumerGroupEnergy,consumerGroupCost);
            ConsumerGroupEnergyData = consumerGroupData.energy;
            ConsumerGroupCostData = consumerGroupData.cost;
            var colorScheme = d3.scale.category20b().range().concat(d3.scale.category20().range()).sortUnique();
            consumerCostGraph = [];
            consumerEnergyGraph = [];
            xConsumer.domain(d3.extent(ConsumerGroupEnergyData, function(d) { return d.date; }));
            yConsumer.domain([0, d3.max(ConsumerGroupEnergyData, function(d) { return d.value; })]);
            xConCost.domain(d3.extent(ConsumerGroupCostData, function(d) { return d.date; }));
            yConCost.domain([0, d3.max(ConsumerGroupCostData, function(d) { return d.value; })]);

            ConsumerGroupEnergyElements = createAxis(0, contextConsumer, xAxis_consumer, yAxis_consumer, 'Energy (Wh)', width2,height2, make_x_axis,make_yConsumer_axis, 0);
            ConsumerGroupCostElements = createAxis(0, contextConCost, xAxis_ConCost, yAxis_ConCost, 'Cost (Naira)', width2,height2, make_x_axis,make_yConsumer_axis, 0);
            var dataNest = d3.nest()
                .key(function(d) {return d.label;})
                .entries(ConsumerGroupEnergyData);

            var xPosition = (0.45 * width2) - (37.5 * dataNest.length);
            legendGroup= svg_consumer.append("g")
                .attr("class", "legendGroup")
                .attr("height", 100)
                .attr("width", 100)
                .attr('transform', 'translate('+ (xPosition) +',50)');

            legendGroupCost = svg_ConCost.append("g")
                    .attr("class", "legendGroupCost")
                    .attr("height", 100)
                    .attr("width", 100)
                    .attr('transform', 'translate('+ (xPosition) +',50)');

            consumerGraph  = dataNest.forEach(function(d,i) {
                var consumerEnergy =contextConsumer.append("path")
                    .attr("class", "line")
                    .attr("d", consumerLine(d.values))
                    .style("stroke", colorScheme[i]);
                consumerEnergyGraph.push(consumerEnergy);

                // Add the Legend
                legendGroup.append("text")
                    .attr("x", i * 80 ) // spacing
                    .attr("y", height2 + 10)
                    .style("fill","white")
                    .text(d.key);

                legendGroup.append("rect")
                    .attr("x", i * 80 ) // spacing
                    .attr("y", height2 -15)
                    .attr("width", 9)
                    .attr("height", 9)
                    .style("fill",colorScheme[i]);
            });

            var costDataNest = d3.nest()
                .key(function(d) {return d.label;})
                .entries(ConsumerGroupCostData);
            costDataNest.forEach(function(d,i) {
                var consumerCost = contextConCost.append("path")
                    .attr("class", "line")
                    .attr("d", consumerCostLine(d.values))
                    .style("stroke", colorScheme[i]);
                consumerCostGraph.push(consumerCost);
                // Add the Legend
                legendGroupCost.append("text")
                    .attr("x", i * 80 ) // spacing
                    .attr("y", height2 + 10)
                    .style("fill","white")
                    .text(d.key);

                legendGroupCost.append("rect")
                    .attr("x", i * 80 ) // spacing
                    .attr("y", height2 -15)
                    .attr("width", 9)
                    .attr("height", 9)
                    .style("fill",colorScheme[i]);
            });
        });
    }

    function load_daily(URL) {
        var targetDaily = document.getElementById('real_live');
        var spinnerDaily = new Spinner(opts).spin(targetDaily);
        if (minRealTime){
            URL = ("../recent/" + config +  "/" + moment(new Date()).format("YYYY-MM-DDTHH:mm:ss.SSSZ") + "/" + realTimeWindow);
        }
        d3.json(URL, function(error, json) {
            if (error) return console.warn(error);
            var dkey;
            var ddate;
            dataDaily = [];
            var recent_data = json.hits.hits;
            if (!minRealTime){
                plotdata_daily = json.aggregations.Power.buckets;

                plotdata_daily.forEach(function(d) {
                    d.src = [];
                });
                plotdata_daily.forEach(function(d) {
                    d.buckets = d.SourceType.buckets;
                });

                plotdata_daily.forEach(function(d) {
                    for (i = 0; i < d.buckets.length; i++) {
                        d.src.push({
                            "key": d.buckets[i].key,
                            "value": d.buckets[i].powerStats.avg
                        });
                    }
                });

                plotdata_daily.forEach(function(d) {
                    for (i = 0; i < d.buckets.length; i++) {
                        DataDaily.push({
                            "key": d.buckets[i].key,
                            "value": d.buckets[i].powerStats.avg || 0,
                            "time": parseTimeISO(moment(d.key_as_string).format("YYYY-MM-DD HH:mm:ss"))
                        });
                    }
                });

                plotdata_daily.forEach(function(d) {
                    for (i = 0; i < d.src.length; i++) {
                        d[d.src[i].key] = d.src[i].value;
                    }
                });

                plotdata_daily.forEach(function(d) {
                    prepareDataDaily(d);
                });

            } else {
                recent_data.forEach(function(d) {
                    d[d._source.SourceType] = d._source.Power_real_total;
                });

               recent_data.forEach(function(d) {
                    dataDaily.push({
                       "time": parseTimeISO(moment(d._source.Time).format("YYYY-MM-DD HH:mm:ss")),
                       "inverter": d.inverter || 0,
                       "grid": d.grid || 0,
                       "generator": d.generator || 0,
                       "total" : (d.inverter|| 0) + (d.grid||0) +(d.generator||0)
                    });
                });

            }

            colorScale = d3.scale.ordinal()
                .range(["lightblue", "lightgreen", "darkorange"]);

            newDataset = ["inverter", "grid", "generator"].map(function(n) {
                return {
                    key: n,
                    values: dataDaily.map(function(d) {
                        return {
                            x: d.time,
                            y: d[n],
                            y0: 0
                        };
                    })
                };
            });

            yl.domain([0, d3.extent(dataDaily, function(d) {
                              return d.total;
                          })[1]]);

            var minTime = d3.min(dataDaily, function(d) { return d.time; });
            var maxTime = d3.max(dataDaily, function(d) { return d.time; });

             if (!minTime){
                var currentTime = new Date();
                minTime = new Date(currentTime.getFullYear(), currentTime.getMonth(), currentTime.getDate(), 0, 0, 0);
                maxTime = new Date(currentTime.getFullYear(), currentTime.getMonth(), currentTime.getDate(), 23, 59, 59);
             }
            console.log(powerBrushStatus);
            if (minRealTime || powerBrushStatus){
                xl.domain(d3.extent(dataDaily, function(d) {
                    return d.time;
                }));
            } else {
                xl.domain([
                    new Date(minTime.getFullYear(), minTime.getMonth(), minTime.getDate(), 0, 0, 0),
                    new Date(maxTime.getFullYear(), maxTime.getMonth(), maxTime.getDate(), 23, 59, 59)
                ]);
            }

            gridlx = focus.append("g")
                .attr("class", "grid")
                .attr("transform", "translate(0," + height + ")")
                .call(make_x_axis()
                    .tickSize(-height, 0, 0)
                    .tickFormat("")
                );

            gridly = focus.append("g")
                .attr("class", "grid")
                .call(make_y_axis("left")
                    .tickSize(-width, 0, 0)
                    .tickFormat("")
                );


            axislx = focus.append("g")
                .attr("class", "x axis")
                .attr("transform", "translate(0," + height + ")")
                .call(xAxis_realtime);

            axislx_label = axislx.append("text")
                .attr("x", 0)
                .attr("dx", "3.5em")
                .attr("dy", "3.5em")
                .style("text-anchor", "end")
                .attr("class", "axislabel")
                .text("Time GMT +01:00");

            axisly = focus.append("g")
                .attr("class", "y axis")
                .call(yAxis_realtime);

            axisly_label = axisly.append("text")
                .attr("transform", "rotate(-90)")
                .attr("y", 0)
                .attr("dy", "1.5em")
                .style("text-anchor", "end")
                .attr("class", "axislabel")
                .text("Power (W)");

            //appends and calls brush for real time graph
            focus.append("g")
                 .attr("class", "xc brush")
                 .call(brush_real)
                 .selectAll("rect")
                 .attr("y", -6)
                 .attr("height", height + 7);

            var tip = d3.tip()
                .attr('class', 'd3-tip-area')
                .offset([-10, 0])
                .html(function() {
                    return "<strong style='color:red'>" + moment(ddate).format('MMMM Do YYYY HH:mm:ss') + "</strong> <span >" + "<p>" + capitalizeFirstLetter(dkey) + "<br>" + powerGraphScale(pro) + "</p>" + "</span>";
                });

            svg_realtime.call(tip);
            stack = d3.layout.stack()
                .offset("zero")
                .values(function(d) {
                    return d.values;
                });


            area = d3.svg.area()
                .interpolate("basis")

            .x(function(d) {
                    return xl(d.x);
                })
                .y0(function(d) {
                    return yl(d.y0);
                })
                .y1(function(d) {
                    return yl(d.y0 + d.y);
                });


            Group = focus.selectAll(".valgroup")
                .data(stack(newDataset))
                .enter()
                .append("g")
                .attr("class", "valgroup")
                .style("fill", function(d, i) {
                    return colorScale(i);
                });

            Group.append("path")
                .attr("d", function(d) {
                    return area(d.values);
                });


            focus.selectAll(".valgroup")
                .attr("opacity", 1)
                /*                .on("mouseover", function(d, i) {
                                  focus.selectAll(".valgroup").transition()
                                  .duration(250)
                                  .attr("opacity", function(d, j) {
                                    return j != i ? 0.6 : 1;
                                });})*/

            .on("mousemove", function(d) {
                    mousex = d3.mouse(this);
                    var invertedx = xl.invert(mousex[0]);
                    //var invertedy = yl.invert(mousex[1]);
                    invertedx = invertedx.getDate()+""+invertedx.getHours() + "" + invertedx.getMinutes();
                    var selected = (d.values);
                    for (var k = 0; k < selected.length; k++) {
                        datearray[k] = selected[k].x;

                        datearray[k] =datearray[k].getDate()+ "" +datearray[k].getHours() + "" + datearray[k].getMinutes();

                    }
                    mousedate = datearray.indexOf(invertedx);
                    pro = Math.abs(d.values[mousedate].y-d.values[mousedate].y0);
                    dkey = d.key;
                    ddate = xl.invert(mousex[0]);
                    /*                      d3.select(this)
                                          .classed("hover", true)
                                          .attr("stroke", colorScale[0])
                                          .attr("stroke-width", "0.5px");*/
                    tip.show();
                })
                .on("mouseout", function() {
                    focus.selectAll(".layer")
                        .transition()
                        .duration(250)
                        .attr("opacity", "1");
                    tip.hide();
                });
                powerBrushStatus = false;
                spinnerDaily.stop();

        });
    }

    function load_data(URL) {
/*        var target = document.getElementById('graph');
        var targetEnergy = document.getElementById('energy_graph');
        var targetCost = document.getElementById('cost_graph');*/

        /*    var spinner = new Spinner(opts).spin(target);
            var spinnerEnergy = new Spinner(opts).spin(targetEnergy);
            var spinnerCost = new Spinner(opts).spin(targetCost);*/
        d3.json(URL, function(error, json) {
            if (error) return console.warn(error);

            plotdata = json.aggregations.power.buckets;
            fueldata = json.aggregations.fuel.buckets;
            fuelRatedata = json.aggregations.fuelRate.buckets;
            batteryCapData = json.aggregations.batteryCap.buckets;
            batteryDOD = json.aggregations.batteryDOD.buckets;

            try {
                $("#timePieName").remove();
                $("#timePieType").remove();
                $("#energyPieName").remove();
                $("#energyPieType").remove();
                $("#costPieName").remove();
                $("#costPieType").remove();
                $("#powerPieName").remove();
                $("#powerPieType").remove();
                $("#costEnergyPieName").remove();
                $("#costEnergyPieType").remove();
                $("#energyJSChartsPieName").remove();
                $("#energyJSChartsPieType").remove();
            } catch (err) {
                console.info("first run no previous pies and donuts");
            }

            var typeColor = ["inverter", "grid", "generator"];
            var typeTime = json.aggregations.timingType.buckets;
            var nameTime = json.aggregations.timingName.buckets;
            var costName = json.aggregations.costName.buckets;
            var costType = json.aggregations.costType.buckets;
            var energyName = json.aggregations.EnergyBySourceName.buckets;
            var energyType = json.aggregations.EnergyBySourceType.buckets;
            var power_data = json.aggregations.Power.buckets;
            var interval = json.interval;

            _sourceTypes = []; // contains source types
            _sourceNames = []; // contains source names
            _sourceNameByType = {}; // object with key of source types to an array of source names

            // Multilevel array of source names with top level array index representing
            // index of related source type
            _SrcNameByType = [];

            plotdata.forEach(function(d) {
                d.time = last = parseTimeISO(moment(d.key).format("YYYY-MM-DD HH:mm:ss"));
                d.total = d.Power_real_total.avg;
                d.buckets = d.SourceType.buckets;
                if (d.buckets.length > 0) {
                    d.buckets.map(function(_type) {
                        if (!(_type.key in _sourceNameByType)) {
                            _sourceNameByType[_type.key]= [];
                        }
                        if (_sourceTypes.indexOf(_type.key) == -1) {
                            _sourceTypes.push(_type.key);
                            _SrcNameByType.push([]);
                        }
                    _type.SourceName.buckets.map(function(name) {
                        if (_sourceNames.indexOf(name.key) == -1) {
                            _sourceNames.push(name.key);
                            _sourceNameByType[_type.key].push(name.key);
                            _SrcNameByType[_sourceTypes.indexOf(_type.key)].push(name.key);
                        }
                    });
                    });
                }
            });
            console.log(json);
            var sourceTypes = {};
            var sourceNames= {};
            var sourceTypes_ = {};
            var sourceNames_= {};
            var energy_sourceTypes ={};
            var energy_sourceNames = {};

            var max_bars = 100;
            var delta = new Date(URL.split("/")[4]).getTime() - new Date(URL.split("/")[3]).getTime();
            /*var interval = ((delta / max_bars)/1000);
            interval = interval < 5 ? 5 : interval;*/

            power_data.forEach(function(d){
                var time = d.key_as_string;
                d.SourceType.buckets.forEach(function(s){
                    var powers_ = 0;
                    s.SourceName.buckets.forEach(function(n){
                        var name = n.key;
                        var power_ = n.Power_real_total.avg;
                        powers_ += power_;
                        if (sourceNames_[name]) {
                            sourceNames_[name].push({"name":name,"power":power_,"time":time});
                        } else {
                            sourceNames_[name] = [];
                            sourceNames_[name].push({"name":name,"power":power_,"time":time});
                        }
                    });
                    var type = s.key;
                    if (sourceTypes_[type]) {
                        sourceTypes_[type].push({"type":type,"power":powers_,"time":time});
                    } else {
                        sourceTypes_[type] = [];
                        sourceTypes_[type].push({"type":type,"power":powers_,"time":time});
                    }
                });
            });

            Object.keys(sourceNames_).forEach(function(t){
                sourceNames[t] = sourceNames_[t].sort(function(a,b){
                  return new Date(b.time) - new Date(a.time);
                });
            });

            Object.keys(sourceTypes_).forEach(function(t){
                sourceTypes[t] = sourceTypes_[t].sort(function(a,b){
                  return new Date(b.time) - new Date(a.time);
                });
            });

             Object.keys(sourceTypes).forEach(function(t){
                var energy = [];
                for (var i = 1; i < sourceTypes[t].length; i++) {
                    var dp  = (sourceTypes[t][i].power+sourceTypes[t][i-1].power)/2;
                    t1 = new Date(sourceTypes[t][i-1].time).getTime() || new Date(moment(parseInt(sourceTypes[t][i-1].time))).getTime();
                    t2 = new Date(sourceTypes[t][i].time).getTime() || new Date(moment(parseInt(sourceTypes[t][i].time))).getTime();
                    var dt = (Math.abs(t1-t2)/(1000));
                    if (dt <= interval){
                        energy.push(dp * (dt/3600));
                    }
                }
                energySum = energy.reduce(function(a,b){return a+b;},0);
                energy_sourceTypes[t] = energySum;
            });

            Object.keys(sourceNames).forEach(function(t){
                var energy = [];
                for (var i = 1; i < sourceNames[t].length; i++) {
                    var dp  = (sourceNames[t][i].power+sourceNames[t][i-1].power)/2;
                    t1 = new Date(sourceNames[t][i-1].time).getTime() || new Date(moment(parseInt(sourceNames[t][i-1].time))).getTime();
                    t2 = new Date(sourceNames[t][i].time).getTime() || new Date(moment(parseInt(sourceNames[t][i].time))).getTime();
                    var dt = (Math.abs(t1-t2)/(1000));
                    if (dt <= interval) {
                        energy.push(dp * (dt/3600));
                    }
                }
                energySum = energy.reduce(function(a,b){return a+b;},0);
                 Object.defineProperty(energy_sourceNames, t, {
                                  value: energySum,
                                  writable: true,
                                  enumerable: true,
                                  configurable: true
                                });
            });


            try {
                checkDate_ = new Date("2017-04-29");
                if (new Date(URL.split("/")[3]) < checkDate_) {

                    interval = ((delta / max_bars)/1000);
                    interval = interval < 5 ? 5 : interval;


                     Object.keys(sourceTypes).forEach(function(t){
                        var energy = [];
                        for (var i = 1; i < sourceTypes[t].length; i++) {
                            var dp  = (sourceTypes[t][i].power+sourceTypes[t][i-1].power)/2;
                            t1 = new Date(sourceTypes[t][i-1].time).getTime() || new Date(moment(parseInt(sourceTypes[t][i-1].time))).getTime();
                            t2 = new Date(sourceTypes[t][i].time).getTime() || new Date(moment(parseInt(sourceTypes[t][i].time))).getTime();
                            var dt = (Math.abs(t1-t2)/(1000));
                            if (dt <= interval){
                                energy.push(dp * (dt/3600));
                            }
                        }
                        energySum = energy.reduce(function(a,b){return a+b;},0);
                        energy_sourceTypes[t] = energySum;
                    });

                    Object.keys(sourceNames).forEach(function(t){
                        var energy = [];
                        for (var i = 1; i < sourceNames[t].length; i++) {
                            var dp  = (sourceNames[t][i].power+sourceNames[t][i-1].power)/2;
                            t1 = new Date(sourceNames[t][i-1].time).getTime() || new Date(moment(parseInt(sourceNames[t][i-1].time))).getTime();
                            t2 = new Date(sourceNames[t][i].time).getTime() || new Date(moment(parseInt(sourceNames[t][i].time))).getTime();
                            var dt = (Math.abs(t1-t2)/(1000));
                            if (dt <= interval) {
                                energy.push(dp * (dt/3600));
                            }
                        }
                        energySum = energy.reduce(function(a,b){return a+b;},0);
                         Object.defineProperty(energy_sourceNames, t, {
                                          value: energySum,
                                          writable: true,
                                          enumerable: true,
                                          configurable: true
                                        });
                    });
                }

            } catch(err) {
                console.log("errors");
            }



            timeData = getTimeData(typeTime, nameTime, typeColor);
            costData = getCostData(costType, costName, typeColor);
            energyData = _getEnergyData(energyType, energyName, typeColor);
            costPerEnergyData = getCostPerEnergy(costType,costName,energyType,energyName,typeColor);
            energyDataJS = getEnergyDataJS(energy_sourceTypes, energy_sourceNames,typeColor);

            var time_type_data = timeData.time_type_data;
            var time_name_data = timeData.time_name_data;
            var cost_type_data = costData.cost_type_data;
            var cost_name_data = costData.cost_name_data;
            var energy_type_data = energyData.energy_type_data;
            var energy_name_data = energyData.energy_name_data;
            var cost_energy_name_data = costPerEnergyData.cost_energy_name_data;
            var cost_energy_type_data = costPerEnergyData.cost_energy_type_data;
            var energy_type_data_JS = energyDataJS.energy_type_dataJS;
            var energy_name_data_JS = energyDataJS.energy_name_dataJS;

            console.log(energyData);

            var sum = 0;
            fueldata.forEach(function(d) {
                if (d.total !== null) {
                    d.time = last = parseTimeISO(moment(d.key).format("YYYY-MM-DD HH:mm:ss"));
                    sum = sum + d.Fuel.sum;
                    d.total = sum ;
                }
            });

            fuelRatedata.forEach(function(d) {
                if (d.total !== null) {
                    d.time = last = parseTimeISO(moment(d.key).format("YYYY-MM-DD HH:mm:ss"));
                    d.total = d.FuelRate.avg;
                }
            });

            var currentBattery;
            batteryCapData.forEach(function(d) {
               if (d.batteryCapacity.avg !== null) {
                   d.time = last = parseTimeISO(moment(d.key).format("YYYY-MM-DD HH:mm:ss"));
                   d.total = d.batteryCapacity.avg;
                   currentBattery = d.batteryCapacity.avg;
               }else {
               d.time = last = parseTimeISO(moment(d.key).format("YYYY-MM-DD HH:mm:ss"));
               d.total = currentBattery;
               }
           });

           var battDOD;
           batteryDOD.forEach(function(d) {
                 if (d.DOD.avg  !== null) {
                     d.time = last = parseTimeISO(moment(d.key).format("YYYY-MM-DD HH:mm:ss"));
                     d.total = 100 - d.DOD.avg;
                     battDOD = 100 - d.DOD.avg;
                 } else {
                   d.time = last = parseTimeISO(moment(d.key).format("YYYY-MM-DD HH:mm:ss"));
                   d.total = battDOD;
                 }
             });


            sortedData = sortData(_SrcNameByType, _sourceTypes, energy_name_data, energy_type_data, time_name_data, time_type_data, cost_name_data, cost_type_data,nameTime, typeColor,cost_energy_name_data,cost_energy_type_data,energy_name_data_JS,energy_type_data_JS);

            SrcName_energy_data_sorted = sortedData.SrcName_energy_data_sorted;
            time_name_data_sorted = sortedData.time_name_data_sorted;
            time_type_data_sorted = sortedData.time_type_data_sorted;
            cost_name_data_sorted = sortedData.cost_name_data_sorted;
            cost_type_data_sorted = sortedData.cost_type_data_sorted;
            power_name_data_sorted = sortedData.power_name_data_sorted;
            power_type_data_sorted = sortedData.power_type_data_sorted;
            energy_type_data_sorted = sortedData.energy_type_data_sorted;
            nameTime_sorted = sortedData.nameTime_sorted;
            cost_energy_name_data_sorted = sortedData.cost_energy_name_data_sorted;
            cost_energy_type_data_sorted = sortedData.cost_energy_type_data_sorted;
            energy_name_data_sortedJS = sortedData.energy_name_data_sortedJS;
            energy_type_data_sortedJS = sortedData.energy_type_data_sortedJS;

            console.log(energy_name_data_sortedJS);
            console.log(energy_type_data_sortedJS);

            x.domain(d3.extent(plotdata, function(d) {
                return d.time;
            }));
            y.domain([0, d3.extent(plotdata, function(d) {
                return d.total;
            })[1]]);

            /*            spinner.stop();
                        spinnerEnergy.stop();
                        spinnerCost.stop();*/
            try {
                try {
                    $("#timePieName").remove();
                    $("#timePieType").remove();
                    $("#energyPieName").remove();
                    $("#energyPieType").remove();
                    $("#costPieName").remove();
                    $("#costPieType").remove();
                    $("#powerPieName").remove();
                    $("#powerPieType").remove();
                    $("#costEnergyPieName").remove();
                    $("#costEnergyPieType").remove();
                    $("#energyJSChartsPieName").remove();
                    $("#energyJSChartsPieType").remove();
                } catch (err) {
                    console.info("first run no previous pies and donuts");
                }
                try {
                    var canvasHeight = +(document.getElementById("energyDayGraph").getAttribute("height")) + 100;
                    Conf.size.canvasHeight = canvasHeight;
                    ConfInner.size.canvasHeight = canvasHeight;

                    pieSrc10 = createPie(Conf, energy_name_data_sortedJS, "Energy / Source ", "EnergyCharts", "energyJSChartsPieName", "Energy : {value} Wh ");
                    document.getElementById("energyJSChartsPieName").setAttribute("viewBox", "0 0 " + String(Conf.size.canvasWidth) + " " + String(Conf.size.canvasHeight));
                    document.getElementById("energyJSChartsPieName").setAttribute("perserveAspectRatio", "xMidYMid");
                    d3.select("#energyJSChartsPieName").append("svg")
                        .attr("id", "energyJSCharts_svg");

                    pieSrc9 = createPie(ConfInner, energy_type_data_sortedJS, "Energy / Source", "energyJSCharts_svg", "energyJSChartsPieType", "Energy : {value} Wh ");

                    pieSrc8 = createPie(Conf, cost_energy_name_data_sorted, "Cost / Energy", "costPerSourceCharts", "costEnergyPieName", "Cost per Energy : {value} Naira / Kwh ");
                    document.getElementById("costEnergyPieName").setAttribute("viewBox", "0 0 " + String(Conf.size.canvasWidth) + " " + String(Conf.size.canvasHeight));
                    document.getElementById("costEnergyPieName").setAttribute("perserveAspectRatio", "xMidYMid");
                    d3.select("#costEnergyPieName").append("svg")
                        .attr("id", "costEnergyCharts_svg");

                    pieSrc7 = createPie(ConfInner, cost_energy_type_data_sorted, "Cost / Energy", "costEnergyCharts_svg", "costEnergyPieType", "Cost per Energy : {value} Naira / Kwh ");


                    pieSrc6 = createPie(Conf, cost_name_data_sorted, "Cost / Source Type ", "costCharts", "costPieName", "Cost : {value} Naira ");
                    document.getElementById("costPieName").setAttribute("viewBox", "0 0 " + String(Conf.size.canvasWidth) + " " + String(Conf.size.canvasHeight));
                    document.getElementById("costPieName").setAttribute("perserveAspectRatio", "xMidYMid");
                    d3.select("#costPieName").append("svg")
                        .attr("id", "costCharts_svg");

                    pieSrc5 = createPie(ConfInner, cost_type_data_sorted, "Cost / Source Type", "costCharts_svg", "costPieType", "Cost : {value} Naira ");

                    var conv_Time_Name_Data = secToHrs(time_name_data_sorted);

                    pieSrc2 = createPie(Conf, conv_Time_Name_Data, "Time / Source Type", "TimeCharts", "timePieName", "Time: {value} Hrs");
                    document.getElementById("timePieName").setAttribute("viewBox", "0 0 " + String(Conf.size.canvasWidth) + " " + String(Conf.size.canvasHeight));
                    document.getElementById("timePieName").setAttribute("perserveAspectRatio", "xMidYMid");
                    d3.select("#timePieName").append("svg")
                        .attr("id", "timePieType_svg");

                    var conv_Time_Type_Data = secToHrs(time_type_data_sorted);

                    pieSrc4 = createPie(ConfInner, conv_Time_Type_Data, "Time / Source Type", "timePieType_svg", "timePieType", "Time : {value} Hrs");

                    pieSrc3 = createPie(Conf, SrcName_energy_data_sorted, "Energy / Source Type", "energyJSCharts", "energyPieName", "Energy: {value} Watt Hours");
                    document.getElementById("energyPieName").setAttribute("viewBox", "0 0 " + String(Conf.size.canvasWidth) + " " + String(Conf.size.canvasHeight));
                    document.getElementById("energyPieName").setAttribute("perserveAspectRatio", "xMidYMid");
                    d3.select("#energyPieName").append("svg")
                        .attr("id", "energyPieType_svg");

                    pieSrc1 = createPie(ConfInner, energy_type_data_sorted, "Energy / Source Type", "energyPieType_svg", "energyPieType", "Energy: {value} Watt Hours");
                } catch (err) {
                    console.log("Data is not available");
                }
            } catch (err) {
                console.log("Source Type pie chart is not working");
            }

            xf.domain(d3.extent(fueldata, function(d) {
                return d.time;
            }));
            yf.domain([
                d3.min(fueldata, function(d) {
                    return d.total;
                }),
                d3.max(fueldata, function(d) {
                    return d.total;
                })
            ]);

            xfr.domain(d3.extent(fuelRatedata, function(d) {
                return d.time;
            }));
            yfr.domain([0, d3.extent(fuelRatedata, function(d) {
                return d.total;
            })[1]]);

            xDOD.domain(d3.extent(batteryDOD, function(d) {
                return d.time;
            }));
            yDOD.domain([
                d3.min(batteryDOD, function(d) {
                    return d.total;
                }),
                d3.max(batteryDOD, function(d) {
                    return d.total;
                })
            ]);

            xb.domain(d3.extent( batteryCapData, function(d) {
                    return d.time;
                }));
            yb.domain([
                d3.min( batteryCapData, function(d) {
                    return d.total;
                }),
                d3.max( batteryCapData, function(d) {
                    return d.total;
                })
            ]);

           fuelElements = createAxis (0,contextFuel,xAxisf,yAxis1f,'Fuel (L)',widthFuel,height3+1,make_x_axisf,make_y_axisf,0); // fuel
           fuelRateElements = createAxis (0,contextFuelRate,xAxisf,yAxis1fr,'Fuel Rate (L/H)',widthFuel,height3+1,make_x_axisf,make_y_axisf,0); // fuel Rate
           batteryCapElements = createAxis (0,contextBattery,xAxisb,yAxis1b,'Battery Capacity (%)',widthFuel,height3+1,make_x_axisBattery,make_y_axisBattery,0); // Battery Cap
           batteryDODElements = createAxis (0,contextDOD,xAxisb,yAxis1DOD,'Battery DOD (%)',widthFuel,height3+1,make_x_axisBattery,make_y_axisBattery,0); // Battery DOD

           lined = contextFuel.append('path')
                .datum(fueldata)
                .attr("class", "lin")
                .attr("d", lineFuel);

           linedRate = contextFuelRate.append('path')
                .datum(fuelRatedata)
                .attr("class", "lin")
                .attr("d", lineFuelRate);

           linedBatteryCap = contextBattery.append('path')
                 .datum(batteryCapData)
                 .attr("class", "inv")
                 .attr("d", lineBattery);

           linedBatteryDOD = contextDOD.append('path')
                .datum(batteryDOD)
                .attr("class", "inv")
                .attr("d", lineDOD);

        });
    }

    //set Summary and set settings window toggle
    $("#displayWindowLink").click(function() {
        $("#displayWindow").slideToggle();
    });

    //bootstrap datetime picker
    var dpStart = "";
    var dpStop = "";
    var dp1 = $('#datetimepicker1').datetimepicker();
    dp1.data("DateTimePicker").maxDate(new Date()).format("DD-MM-YYYY h:mm:ss a");
    var dp2 = $('#datetimepicker2').datetimepicker({
        useCurrent: false //Important! See issue #1075
    });
    dp2.data("DateTimePicker").maxDate(new Date()).format("DD-MM-YYYY h:mm:ss a");
    dp1.on("dp.change", function(e) {
        dpStart = e.date;
        dp2.data("DateTimePicker").minDate(e.date);

    });
    dp2.on("dp.change", function(e) {
        dp1.data("DateTimePicker").maxDate(e.date);
        dpStop = e.date;
    });

    $("#dPGo").on("click", function() {
        remove_graphElements();
        remove_livegraphElements();
        console.log(typeof dpStart);
        console.log(dpStop);
        console.log((new Date(dpStart)));
        var timeStart = moment(new Date(dpStart)).format("YYYY-MM-DDTHH:mm:ss.SSSZ");
        var timeStop = moment(new Date(dpStop)).format("YYYY-MM-DDTHH:mm:ss.SSSZ");
        exportStartTime = timeStart;
        exportEndTime = timeStop;
        console.log(moment(new Date(dpStart)).format("YYYY-MM-DDTHH:mm:ss.SSSZ"));
        load_data("../data/" + config  + "/" + moment(new Date(dpStart)).format("YYYY-MM-DDTHH:mm:ss.SSSZ") + "/" + moment(new Date(dpStop)).format("YYYY-MM-DDTHH:mm:ss.SSSZ"));
        load_daily("../daily/" + config + "/" + moment(new Date(dpStart)).format("YYYY-MM-DDTHH:mm:ss.SSSZ") + "/" + moment(new Date(dpStop)).format("YYYY-MM-DDTHH:mm:ss.SSSZ"));
        loadConsumers("../consumer/" + config  + "/" + moment(new Date(dpStart)).format("YYYY-MM-DDTHH:mm:ss.SSSZ") + "/" + moment(new Date(dpStop)).format("YYYY-MM-DDTHH:mm:ss.SSSZ"));
        if (consumerGroup) {
            loadConsumerGroup("../consumerGroup/" + consumerGroup  + "/" + moment(new Date(dpStart)).format("YYYY-MM-DDTHH:mm:ss.SSSZ") + "/" + moment(new Date(dpStop)).format("YYYY-MM-DDTHH:mm:ss.SSSZ"));
        }
    });

    //timeWindow Relative tab
    $("#historyRBtn").click(function() {
        var milSecTime = "";
        var inpt = $('#historyRTxt').val();
        if (inpt < 1) {
            return;
        }
        switch (parseInt($("#historyRS option:selected").val())) {
            case 1:
                milSecTime = inpt * 60 * 1000;
                break;
            case 2:
                milSecTime = parseInt(inpt) * 3600000;
                break;
            case 3:
                milSecTime = inpt * 60 * 60 * 24 * 1000;
                break;
            case 4:
                milSecTime = inpt * 60 * 60 * 24 * 7 * 1000;
                break;
            case 5:
                milSecTime = inpt * 60 * 60 * 24 * 7 * 30 * 1000;
                break;
            case 6:
                milSecTime = inpt * 60 * 60 * 24 * 7 * 30 * 12 * 1000;
                break;
            default:
                milSecTime = inpt * 60 * 60 * 24 * 1000;
        }
        remove_monthlygraphElements();
        remove_livegraphElements();
        remove_graphElements();
        query(config, "../bounds/" + config , milSecTime);
    });

    //listens for custom event (HomePageOps.scala)
    $("#quick").on("winChange", function(e, tm, tm2) {
        remove_monthlygraphElements();
        remove_livegraphElements();
        remove_graphElements();
        if (tm2 !== 'undefined') {
            query(config, "../bounds/" + config , tm, tm2);
        } else {
            query(config, "../bounds/" + config, tm);
        }
    });
    var realTimeWindow = $("#realTimeWindow").val(); //get default (in sec)
    $("#rTimeRange").ionRangeSlider({
        force_edges: false,
        values: [
            "30sec", "45sec", "1min", "2min",
            "3min", "5min", "7min",
            "10min", "15min", "20min", "25min", "30min"
        ],
        from: 5,
        onFinish: function(data) {
            chRealTimeWindowSize(data.from);
        }
    });

    function setProbeOnSummary() {
        if (($("#probe option:selected").html()) != 'all probes') {
            $("#probeval").html('of ' + $("#probe option:selected").html() + ' ');
        } else if (($("#probe option:selected").html()) == 'all probes') {
            $("#probeval").html('');
        }
    }

    function setConfOnSummary() {
        $("#configval").html($("#config option:selected").html());
    }

    function chRealTimeWindowSize(tm) {
        switch (tm) {
            case 0:
                realTimeWindow = "30";
                break;
            case 1:
                realTimeWindow = "45";
                break;
            case 2:
                realTimeWindow = "60";
                break;
            case 3:
                realTimeWindow = "120";
                break;
            case 4:
                realTimeWindow = "180";
                break;
            case 5:
                realTimeWindow = "300";
                break;
            case 6:
                realTimeWindow = "420";
                break;
            case 7:
                realTimeWindow = "600";
                break;
            case 8:
                realTimeWindow = "900";
                break;
            case 9:
                realTimeWindow = "1200";
                break;
            case 10:
                realTimeWindow = "1500";
                break;
            case 11:
                realTimeWindow = "1800";
                break;
            default:
                realTimeWindow = "300";
                break;
        }
        $("#realTimeWindow").val(realTimeWindow);
        remove_phaseElements();
        load_recent("../recent/" + config +  "/" + moment(lastTransmit).format("YYYY-MM-DDTHH:mm:ss.SSSZ") + "/" + realTimeWindow);
    }

    /* The code below is for the functionality of exporting all the data points for the stats being currently viewed. */

    var hasOpenedWithGoogleSheets = false;
    var createdGoogleSheetsUrl = "";
    // Used to sidestep pop-up blockers when opening exported data in a separate Google Sheets tab.
    var newGoogleSheetsTab = null;

    $("#displayExportPanel").click(function() {
        $("#exportPanel").slideToggle();
    });

    function JSONToCSVConvertor(JSONData, ReportTitle, ShowLabel) {
        //If JSONData is not an object then JSON.parse will parse the JSON string in an Object
        var arrData = typeof JSONData != 'object' ? JSON.parse(JSONData) : JSONData;
        var CSV = '';

        //Set Report title in first row or line
        CSV += ReportTitle + '\r\n\n';

        //This condition will generate the Label/Header from 1st index of on array
        if (ShowLabel) {
            var row_ = "";
            for (var _data in arrData[0]) {
                row_ += _data + ',';
            }
            row_ = row_.slice(0, -1);
            CSV += row_ + '\r\n';
        }

        //1st loop is to extract each row
        for (var i = 0; i < arrData.length; i++) {
            var row = "";
            for (var index in arrData[i]) {
                row += arrData[i][index] + ',';
            }
            row.slice(0, row.length - 1);
            //add a line break after each row
            CSV += row + '\r\n';
        }

        if (CSV === '') {
            console.log("Invalid data");
            return;
        }
        return CSV;
    }

    function toHHMMSS(number) {
        var sec_num = parseInt(number, 10);
        var hours   = Math.floor(sec_num / 3600);
        var minutes = Math.floor((sec_num - (hours * 3600)) / 60);
        var seconds = sec_num - (hours * 3600) - (minutes * 60);

        if (hours   < 10) {hours   = "0"+hours;}
        if (minutes < 10) {minutes = "0"+minutes;}
        if (seconds < 10) {seconds = "0"+seconds;}
        return hours+':'+minutes+':'+seconds;
    }

    $("#fetchDataAsCsvBtn").on("click", this, function() {
     if (!$('#exportForm')[0].checkValidity()) {$('#exportForm')[0].reportValidity();return;}
        hideExportErrorMsg("errorMessageContainer");
        showPleaseWait("exportPleaseWaitContainer");
        hideFetchDataContainer("fetchDataAsCsvContainer");
        isHiddenFetchDataContainer = true;
        var intervalUnit = $( "#interval option:selected" ).text();
        var intervalValue = document.getElementById("intervalValue").value;
        var _exportStart = $('#datetimepickerStart input').val();
        var _exportStop = $('#datetimepickerStop input').val();
        var exportStart = moment(_exportStart,'MM/DD/YYYY hh:mm A').format("YYYY-MM-DDTHH:mm:ss.SSSZ");
        var exportStop = moment(_exportStop,'MM/DD/YYYY hh:mm A').format("YYYY-MM-DDTHH:mm:ss.SSSZ");
        exportTitle =  "Exported Data from " + moment(exportStart).format("YYYY-MM-DD HH:mm:ss") + " to " + moment(exportStop).format("YYYY-MM-DD HH:mm:ss");
        console.log(exportStart);
        console.log(exportStop);
        console.log(exportTitle);
        $.ajax({
            url: "../exportToCsv/" + config + "/" + exportStart + "/" + exportStop + "/" + intervalValue  + "/" + intervalUnit ,
            contentType: "application/json; charset=utf-8",
            error: function() {
                exportFailed();
            },
            success: function(data) {
                console.log(data);
                var dataArray = [];
                var exportBuckets = data.aggregations.Export.buckets;
                var dateInterval = {"unit":intervalUnit.toLowerCase(),"value":parseInt(intervalValue)};
                console.log(dateInterval);
                exportBuckets.forEach(function(d){
                    var date = d.key;
                    d.SourceType.buckets.forEach(function(s){
                        var sourceType = s.key;
                        s.SourceName.buckets.forEach(function(n){
                            var sourceName = n.key;
                            var time = toHHMMSS(n.doc_count*5);
                            var energy = n.energyStats.sum;
                            var cost = n.costStats.sum;
                            var power  = n.powerStats;
                            dataArray.push(
                                {
                                    "Start Date": moment(date).format("YYYY-MM-DD HH:mm:ss"),
                                    "Stop Date": moment(date).add(dateInterval.value,dateInterval.unit).format("YYYY-MM-DD HH:mm:ss"),
                                    "Source Name":sourceName,
                                    "Source Type":sourceType,
                                    "Energy (Wh)":roundTo(energy),
                                    "Cost (\u20A6)":roundTo(cost),
                                    "Time (hh:mm:ss)":time,
                                    "Avg Power(W)": roundTo(power.avg),
                                    "Min Power(W)" : roundTo(power.min),
                                    "Max Power(W)":roundTo(power.max)
                                }
                            );
                        });
                    });
                });
                currentCsv = JSONToCSVConvertor(dataArray,exportTitle,true);
                exportSucceeded(currentCsv);
            }
        });
    });

    function exportFailed() {
        showFetchDataContainer("fetchDataAsCsvContainer");
        console.log(showExportErrorMsg("errorMessageContainer"));
        hidePleaseWait("exportPleaseWaitContainer");
        console.log("Failed");
    }

    function exportSucceeded(text) {
        hidePleaseWait("exportPleaseWaitContainer");
        var targetSection = $("#exported-data-container");
        // After the request to export to CSV is successful, initialize and show a download button.
        var link = initExportBtnAndData(text, "exported-data.csv");
        targetSection.append(link);

        // Append the "Open with Google Sheets" button too.
        var googleSheetsSection = initGoogleSheetsSection();
        targetSection.append(googleSheetsSection);
    }

    function showExportErrorMsg(containerId) {
        $("#" + containerId).show();
    }

    function hideExportErrorMsg(containerId) {
        $("#" + containerId).hide();
    }

    function showFetchDataContainer(id) {
        showSomething("#" + id);
    }

    function hideFetchDataContainer(id) {
        hideSomething("#" + id);
    }

    function showPleaseWait(containerId) {
        showSomething("#" + containerId);
    }

    function hidePleaseWait(containerId) {
        hideSomething("#" + containerId);
    }

    function showSomething(cssSelector) {
        $(cssSelector).removeClass("hidden");
    }

    function hideSomething(cssSelector) {
        $(cssSelector).addClass("hidden");
    }

    function initExportBtnAndData(text, filename) {
        var dataToSave = new Blob([text], {type: "text/csv;charset=utf-8;"});
        var urlAddress = window.URL.createObjectURL(dataToSave);
        var downloadBtn = $("<button>").text("Save")
            .attr({
                "id": "exportedDataBtn",
                "class": "btn btn-primary col-lg-12 col-md-12 col-xs-12"});
        var downloadLink = $("<a>")
            .append(downloadBtn)
            .attr({
                "id": "exportedDataLink",
                'Content-Type': 'text/csv; charset=UTF-8',
                "class": "col-lg-2 col-md-2 col-xs-2",
                "href": urlAddress,
                "download": filename
            });
        return downloadLink;
    }

    function initGoogleSheetsSection() {
        var topRow = $("<div>")
            .attr("class", "row row-full");
        console.log("Button click");
        var buttonId = "open-in-google-sheets";
        var button = $("<button>")
            .attr({
                "id": buttonId,
                "class": "btn btn-success"
            })
            .text("Open in Google Sheets");
        button.on("click", function() {
            csvToGoogleSheets(currentCsv, hasOpenedWithGoogleSheets, createdGoogleSheetsUrl, "loading-google-sheets", buttonId);
            var clickKind = "pp";
            if (newGoogleSheetsTab === null) {
                newGoogleSheetsTab = window.open("", "_blank");
                clickKind = "First kind: ";
            }
            else {
                if (newGoogleSheetsTab.location === null) {
                    newGoogleSheetsTab = window.open(createdGoogleSheetsUrl, "_blank");
                    clickKind = "Second kind: ";
                }
            }
            console.log(clickKind, newGoogleSheetsTab);
        });
        var loadingGif = $("<img>")
            .attr({
                "src": "/img/ajax-loader.gif",
                "id": "loading-google-sheets",
                "style": "display: none"
            });
        topRow.append(button).append(loadingGif);

        var bottomRow = $("<div>")
            .attr({
                "id": "permission-denied-google-sheets",
                "class": "row row-full alert alert-danger",
                "style": "display: none"
            });
        var googlePermissionsDenied = $("<span>")
            .text("Error. Google account permissions were denied.");
        bottomRow.append(googlePermissionsDenied);

        var sheetsSection = $("<div>")
            .attr({
                "id": "google-sheets-section",
                "class": "col-lg-4 col-md-4 col-xs-4"
            });
        sheetsSection.append(topRow).append(bottomRow);
        return sheetsSection;
    }

    function onPeriodChanged() {
        showFetchDataContainer("fetchDataAsCsvContainer");
        isHiddenFetchDataContainer = false;
        purgeExportBtnAndData("exportedDataLink");
        purgeGoogleSheetsSection("google-sheets-section");
        hideExportErrorMsg("errorMessageContainer");
        hasOpenedWithGoogleSheets = false;
        createdGoogleSheetsUrl = "";
        newGoogleSheetsTab = null;
    }

    // The previous exported data should be purged immediately after the user selects a new time period.
    function purgeExportBtnAndData(exportLinkId) {
        // Remove the blob using window.URL.revokeObjectURL.
        var exportLinkSelector = "#" + exportLinkId;
        var url = $(exportLinkSelector).attr("href");
        window.URL.revokeObjectURL(url);

        // Remove the download link and all (elements) it contains.
        $(exportLinkSelector).remove();
    }

    function purgeGoogleSheetsSection(sectionId) {
        $("#" + sectionId).remove();
    }

    function updateGooglePermissionsStatus(werePermissionsGranted) {
        hasGrantedGooglePermissions = werePermissionsGranted;
        var errorNoPermissions = $("#permission-denied-google-sheets");
        if (werePermissionsGranted === true) {
            errorNoPermissions.hide();
            csvToGoogleSheets(currentCsv, hasOpenedWithGoogleSheets, createdGoogleSheetsUrl, "loading-google-sheets", "open-in-google-sheets");
        }
        else {
            errorNoPermissions.show();
            // Add a message to the new tab that we opened, wait for 5 seconds and then close that tab.
            newGoogleSheetsTab.document.write("Permissions denied for Google Account. Closing...");
            window.setTimeout(closeNewSheetsTab, 5000);
        }
    }

    function csvToGoogleSheets(csv, alreadyOpened, url, idLoadingImage, idSheetsButton) {
        console.log("csvToGoogleSheets branch 1");
        if (alreadyOpened === true) {
            console.log("csvToGoogleSheets branch 22");
             /* newGoogleSheetsTab.location will be null if the user opened it before and then closed it, without changing the time period. In
             that case, don't run openNewSheetsTab() because a new tab will already have been opened in the initial button's click handler. */
            if (newGoogleSheetsTab.location !== null) {
                openNewSheetsTab(url);
            }
        }
        else {
            console.log("csvToGoogleSheets branch 33");
            $("#" + idLoadingImage).show();
            $("#" + idSheetsButton).addClass("disabled");
            if (hasGrantedGooglePermissions) {
                console.log("csvToGoogleSheets branch 44");
                createAndUpdateSpreadsheet(currentCsv, idLoadingImage, idSheetsButton);
            }
            else {
                console.log("csvToGoogleSheets branch 55");
                grantGooglePermissions();
            }

        }
    }

    function grantGooglePermissions() {
        console.log("csvToGoogleSheets branch 66");
        gapi.auth2.getAuthInstance().signIn();
    }

    // NB: spreadsheet != sheet. A spreadsheet contains one or more sheets.
    function createAndUpdateSpreadsheet(csv, idLoadingImg, idSheetsBtn) {
        console.log("csvToGoogleSheets branch 77");
        gapi.client.sheets.spreadsheets.create({
                properties: {
                    title: exportTitle
                }
            })
            .then(function(response) {
            console.log("csvToGoogleSheets branch 88");
            // Required.
            var spreadsheetId = response.result.spreadsheetId;
            var spreadsheetUrl = response.result.spreadsheetUrl;

            /* Required. The "A1 Notation" is used to specify the sheet and range of cells to modify in a
            spreadsheet. Here, we're modifying the entire spreadsheet. See
            https://developers.google.com/sheets/api/guides/concepts#a1_notation for more info. */
            var a1Notation = "Sheet1";

            /* Optional; added for clarity. A "majorDimension" is either the x axis (rows) or y axis
            (columns). So, we can add the blocks as successive rows or columns. See
            https://developers.google.com/sheets/api/reference/rest/v4/spreadsheets.values#dimension for
            more
            info. */
            var majorDim = "ROWS";

            /* Required. The "valueInputOption" determines whether the incoming data will be parsed to
            dates, numbers (USER_ENTERED) or it will be used raw (RAW). See
            https://developers.google.com/sheets/api/reference/rest/v4/ValueInputOption for more info. */
            var inputOption = "USER_ENTERED";

            var rows = csv.split("\n");
            var cellBlocks = [];
            for (i = 0; i < rows.length; i++) {
                var row = rows[i];
                var blockOfCells = row.split(",");
                cellBlocks.push(blockOfCells);
            }

            gapi.client.sheets.spreadsheets.values.update({
                spreadsheetId: spreadsheetId,
                range: a1Notation,
                majorDimension: majorDim,
                valueInputOption: inputOption,
                values: cellBlocks
            }).then(function() {
                console.log("Successfully updated.");
                hasOpenedWithGoogleSheets = true;
                createdGoogleSheetsUrl = spreadsheetUrl;
                $("#" + idLoadingImg).hide();
                $("#" + idSheetsBtn).removeClass("disabled");
                openNewSheetsTab(spreadsheetUrl);
            });
        });
    }

    /* NB: This function has a special implementation that's designed to evade pop-up blockers in
    browsers. We need this because (especially) for Google Sheets, a sheet's url isn't available until
    a while after the user has clicked the "Open in Google Sheets" button. */
    function openNewSheetsTab(url) {
        newGoogleSheetsTab.location.href = url;
    }

    function closeNewSheetsTab() {
        newGoogleSheetsTab.close();
    }

    /* End of export code. */

});
