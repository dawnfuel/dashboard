$(document).ready(function() {

    var export_start = localStorage.from;
    var export_stop = localStorage.to;
    var probe = localStorage.prb;
    var config = localStorage.cfg;
    var interval = "1h";

    $('#all').prop('checked',true);
    $('#phase_1').prop('disabled',true);
    $('#phase_1').prop('checked',true);
    $('#phase_2').prop('disabled',true);
    $('#phase_2').prop('checked',true);
    $('#phase_3').prop('disabled',true);
    $('#phase_3').prop('checked',true);
    $('#hour').prop('checked',true);
    $('#varHr').prop('disabled',false);
    $('#varHrDy').prop('disabled',true);
    $('#varDay').prop('disabled',true);
    $("#pBar").hide();


    var fromDate = $('#exportFrom').datetimepicker();
    var toDate = $('#exportTo').datetimepicker();

    console.log(export_start);

    fromDate.data("DateTimePicker").defaultDate(new Date(export_start)).format("DD-MM-YYYY h:mm:ss.SSS a");
    toDate.data('DateTimePicker').defaultDate(new Date(export_stop)).format("DD-MM-YYYY h:mm:ss.SSS a");


    fromDate.on('dp.change',function(e){
        export_start = new Date(parseInt(e.date + ""));
        console.log("export start; " + export_start);
        toDate.data("DateTimePicker").minDate(e.date);
    });

    toDate.on('dp.change',function(e){
        export_stop = new Date(parseInt(e.date + ""));
        console.log("export stop: " + export_stop);
        fromDate.data("DateTimePicker").maxDate(e.date);
    });


    function getProbes(Field,probeID_FKs){
        var probes = '';
        while(probeID_FKs.length > 0){
            probes += '{"match":{"'+Field+'":'+probeID_FKs.pop()+'}}';
            if(probeID_FKs.length > 0){
                probes += ',';
            }
        }
        return probes;
    }

    function getDataWithVariability(data){
        console.log("in getDataWithVariability");
        var avg_data = [];
        var new_data = data.split('\n');
        var n;
        var i = 0;
        if($('#varHrDy').prop('checked')){
            n = 24*7;
        }else if($('#varHr').prop('checked')){
            n = 24;
         }else if($('#varDay').prop('checked')){
            n = 7;
        }
        else{return data;}

        var count = [];
        for(j = 0; j < new_data.length; j++){
            if(avg_data[i] === undefined){avg_data[i] = 0;}
            if(count[i] === undefined){count[i] = 0;}
            if(new_data[j] > 0){
                avg_data[i] += parseFloat(new_data[j]);
                count[i] += 1;
            }

            i += 1;
            if(i == n){i = 0;}
        }
        console.log("total data: "+avg_data);
        console.log(avg_data.length);
        console.log("count: "+count);
        console.log(count.length);

        for(i = 0; i < avg_data.length; i++){
            if(count[i] === 0){
                avg_data[i] = 0;
                continue;
            }
            avg_data[i] /= count[i];
        }
        console.log("average data: "+avg_data);
        console.log(avg_data.length);

        /*var var_data = [];*/
        var variability = parseFloat($('#varVal').val())/100;
        var new_string = "";
        i = 0;
        for(j = 0; j < new_data.length; j++){

            if(new_data[j] === "0"){
                new_data[j] = avg_data[(i)] * (1 + 2*(Math.random() - 0.5)*variability);
            }
            i += 1;
            if(i == n){i = 0;}

            new_string += new_data[j].toString() + "\n";
        }
        console.log(new_data);
        console.log(new_data.length);

        return new_string.trim();
    }

    function getExportData(client,probes,year_start,export_start,export_stop/*,tz_string*/){
//        var Size = 5;
        var _phase = "";
        if($('#all').prop('checked')){
//            Size = 100000;
            _phase = "all";
        }else{
            if($("#phase_1").prop('checked')){
                _phase += "phase_1::";
            }
            if($("#phase_2").prop('checked')){
                _phase += "phase_2::";
            }
            if($("#phase_3").prop('checked')){
                _phase += "phase_3::";
            }
//            _phase.replace("::","");
        }
        var start = moment(new Date(export_start)).format("YYYY-MM-DDTHH:mm:ss.SSSZ");
        var stop = moment(new Date(export_stop)).format("YYYY-MM-DDTHH:mm:ss.SSSZ");

        var URL = "../export/" + config + "/" + probe + "/" + start + "/" + stop + "/" + interval + "/" + _phase;

        d3.json(URL, function(error, json) {
            if (error) return console.warn(error);
            console.log("====================returned json=====================");
            console.log(json);
//        });

//        client.search({
//              index: 'grit',
//              type: 'docs',
//              body: {
//                       "query":{
//                                  "bool":{
//                                    "must":[
//                                      probes,
//                                      {
//                                        "range": {
//                                          "Time": {
//                                            "gte": export_start,
//                                            "lte": export_stop,
//                                            "format": "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
//                                          }
//                                        }
//                                      }
//                                    ]
//
//                                  }
//                                },
//                       "size": Size,
//                        "aggs" : {
//
//                            "energies_over_time" : {
//                                "date_histogram" : {
//                                    "field" : "Time",
//                                    "interval" : interval,
//                                    "format" : "yyyy-MM-dd'T'HH:mm:ss.SSSZ",
//                                    "time_zone": tz_string
//                                },
//                                "aggs" : {
//                                    "total_energy" : { "sum" : { "field" : "Energy_since_last" } }
//                                }
//                            }
//
//                        },
//                       "sort" : [
//                            { "Time" : {"order" : "asc"}}
//                        ]
//
//
//                    }
//            }).then(function (resp) {
                console.log("finished aggregation");
                var energy_time_series ="";
                var phase_time_series = "";
                var counter = 0;

                var hits = json.hits.hits;
//                console.log(resp.hits.tota)
                console.log("++++++++++++++++++++++++++ hits +++++++++++++++++++++++");
                console.log(hits);
                var data;
                var aggs;// = json.aggregations.energies_over_time.buckets;
                var start_time;
                var stop_time;

                if(!($('#all').prop('checked'))){
                    console.log("all is not checkeds");
                    data = getPhasePower(hits,interval,1);
                    start_time = new Date(hits[0]._source.Time);
                    stop_time = new Date(hits[hits.length-1]._source.Time);
                }else{
                    aggs = json.aggregations.energies_over_time.buckets;
                    start_time = new Date(aggs[0].key);
                    stop_time = new Date(aggs[aggs.length-1].key);
                    console.log(aggs);
                }
//                console.log("data: " + data);

                console.log("++++++++++++++++++ aggregations ++++++++++++++++++++++");
//                console.log(aggs);
                console.log("show data");
//                var start_time = new Date(aggs[0].key);
//                var stop_time = new Date(aggs[aggs.length-1].key);
                console.log("start_time: " + start_time);
                console.log("stop_time: " + stop_time);

//                var year_end = new Date(new Date(new Date(year_start.getTime()).setMonth(11)).setDate(31));
//                year_end.setHours(23);
//                year_end.setMinutes(59);
//                year_end.setSeconds(59);

                var year_end = new Date(new Date(year_start.getTime()).setFullYear(year_start.getFullYear()+1));

//                console.log("year_start: " + year_start);
//                console.log("year_end: " + year_end);


                var scale = 1;
                if(interval == "1h"){
                    scale = 1;
                }else if(interval == "day"){
                    scale = 24;
                }

                var front_padding = Math.floor(Math.abs(year_start-start_time)/(3600000.0*scale))+1;
                var end_padding = Math.floor(Math.abs(stop_time-year_end)/(3600000.0*scale))-1;


                if(interval == "month"){
                    front_padding = start_time.getMonth();
                    end_padding = 11-stop_time.getMonth();
                }

//                console.log("front_padding_hours: " + front_padding_hours);
//                console.log("end_padding_hours: " + end_padding_hours);


                for(hr = 0; hr < front_padding; hr++){
                    energy_time_series += "0\n";
                    counter += 1;
                }



                if($('#all').prop('checked')){
                    console.log("all is checked");
                    aggs.forEach(function(e){
        //                energy_time_series += e.avg_energy.value.toString + "\n";
                        if(e.total_energy.value === null){
                            energy_time_series += "0\n";
                            counter += 1;
                        }
                        else{
                            energy_time_series += e.total_energy.value.toString() + "\n";
                            counter += 1;
                        }
                    });
                }else{
                    console.log("all is not checked");
                    phase_time_series = energy_time_series + data;
                }


                if($('#all').prop('checked')){
                    console.log("all is checked");
                    for(hr = 0; hr < end_padding; hr++){
                        energy_time_series += "0\n";
                        counter += 1;
                    }
                }else{
                    console.log("all is not checked");
                    for(hr = 0; hr < end_padding; hr++){
                        phase_time_series += "0\n";
                    }
                }


                console.log("total counter: " + counter);

                var a = document.body.appendChild(
                        document.createElement("a")
                    );

                if($('#all').prop('checked')){
                    var e_data = getDataWithVariability(energy_time_series);
                    console.log("all is checked");
                    a.download = "total_energy_data("+year_start.getFullYear()+").txt";
                    a.href = "data:text/plain;base64," + btoa(e_data);

                }else{
                    console.log("all is not checked");
                    var phase = "";
                    if($('#phase_1').prop('checked')){
                        phase += "phase1_";
                    }
                    if($('#phase_2').prop('checked')){
                        phase += "phase2_";
                    }
                    if($('#phase_3').prop('checked')){
                        phase += "phase3_";
                    }
                    a.download = phase + "energy_data("+year_start.getFullYear()+").txt";
                    a.href = "data:text/plain;base64," + btoa(phase_time_series);
                }
                $("#pBar").hide();
                a.click();

    //            hits.forEach(function(hit){
    //            console.log("printing time window");
    //            console.log($('#hiddenTimeWindow').val());
    //            console.log(hit._source.SourceName);

    //            });
//            }, function (err) {
//                console.log("error found");
//                console.trace(err.message);
            });
    }






    $("#btnExport").click(function() {
        console.log("button clicked");
        $("#pBar").show();

        console.log("data period");
        console.log("probe: " + probe);
        console.log("config: " + config);
        console.log(export_start + " ============== " + export_stop);
        console.log("export button click");
        console.log(new Date(export_start));
        console.log(new Date(export_stop));

        var start_time = new Date(export_start);
        var stop_time = new Date(export_stop);

        var tz = start_time.getTimezoneOffset();
        console.log("timezone is " + tz);

        var hh = Math.floor(Math.abs(tz/60));
        var mm = Math.abs((tz%60));
        var tz_string = (tz < 0 ? '+':'-') + (hh < 10 ? '0'+ hh : hh) +':'+ (mm < 10 ? '0'+ mm : mm);
        console.log(tz_string);

        console.log("start_time: " + start_time);
        console.log("stop_time: " + stop_time);

        var year_start = new Date(new Date(new Date(start_time.getTime()).setMonth(0)).setDate(1));

        year_start.setHours(0);
        year_start.setMinutes(0);
        year_start.setSeconds(0);

        var year_end = new Date(new Date(new Date(stop_time.getTime()).setMonth(11)).setDate(31));
        year_end.setHours(23);
        year_end.setMinutes(59);
        year_end.setSeconds(59);

        console.log("-----------------all data---------------");
        console.log("year_start: " + year_start);
        console.log("year_end: " + year_end);

        var client = elasticsearch.Client({
          host: 'localhost:9200',
          log: 'trace'
        });
//        var probes = '';

        if(probe == "all"){
            probes = getProbes("ConfigID_FK",[config]);
        }
        else{
            probes = getProbes("ProbeID_FK",[probe]);
        }

        for(yr = year_start.getFullYear(); yr <= year_end.getFullYear(); yr++){
            var new_export_start;
            var new_export_stop;

            if(yr == year_start.getFullYear()){
                new_export_start = export_start;
                if(year_start.getFullYear() == year_end.getFullYear()){
                    new_export_stop = export_stop;
                }
                else{
                    new_export_stop = new Date(new Date(new Date(year_start.getTime()).setMonth(11)).setDate(31));
                }
                getExportData(client,probes,year_start,new_export_start,new_export_stop,tz_string);
            }
            else if(yr == year_end.getFullYear()){
                new_export_start = new Date(new Date(year_start.getTime()).setFullYear(yr));
                new_export_stop = export_stop;
                getExportData(client,probes,new_export_start,new_export_start,new_export_stop,tz_string);
            }
            else if(yr != year_start.getFullYear()){
                new_export_start = new Date(new Date(year_start.getTime()).setFullYear(yr));
                new_export_stop = new Date(new Date(new Date(new_year_start.getTime()).setMonth(11)).setDate(31));
                getExportData(client,probes,new_export_start,new_export_start,new_export_stop,tz_string);
            }
        }
    });

    $('#all').change(function(){
        if($('#all').prop('checked')) {
            console.log("all checked");
            $('#phase_1').prop('disabled',true);
            $('#phase_1').prop('checked',true);
            $('#phase_2').prop('disabled',true);
            $('#phase_2').prop('checked',true);
            $('#phase_3').prop('disabled',true);
            $('#phase_3').prop('checked',true);
//            document.getElementById("phase_1").disabled = true;

        } else {
            console.log("all unchecked");
            $('#phase_1').prop('disabled',false);
            $('#phase_2').prop('disabled',false);
            $('#phase_3').prop('disabled',false);
//            document.getElementById("phase_1").disabled = false;
        }
    });



    $('#hour').on('click',function(){
        console.log("radio button hour clicked");
        interval = "1h";
        $('#varHr').prop('disabled',false);
        $('#varHrDy').prop('disabled',true);
        $('#varHrDy').prop('checked',false);
        $('#varDay').prop('disabled',true);
        $('#varDay').prop('checked',false);
        console.log("variability" + $('#varVal').val());
    });

    $('#day').on('click',function(){
        console.log("radio button day clicked");
        interval = "day";
        $('#varHr').prop('disabled',true);
        $('#varHr').prop('checked',false);
        $('#varHrDy').prop('disabled',true);
        $('#varHrDy').prop('checked',false);
        $('#varDay').prop('disabled',false);
        console.log("variability" + $('#varVal').val());
    });

    $('#month').on('click',function(){
        console.log("radio button month clicked");
        interval = "month";
        $('#varHr').prop('disabled',true);
        $('#varHr').prop('checked',false);
        $('#varHrDy').prop('checked',false);
        $('#varHrDy').prop('disabled',true);
        $('#varDay').prop('disabled',true);
        $('#varDay').prop('checked',false);
        console.log("variability" + $('#varVal').val());
    });

    $('#varHr').on('change',function(){
        console.log("varHr changed");
        if($('#varHr').prop('checked')){
            console.log("varHrDy enabled");
            $('#varHrDy').prop('disabled',false);
        }else{
            console.log("varHrDy disabled");
            $('#varHrDy').prop('disabled',true);
            $('#varHrDy').prop('checked',false);
        }
    });


    function getPhasePower(query,interval/*,phase*/){
        var P1 = 0;
        var t1 = 0;
        var E = 0;
        var state = 0;
        var xt = 0;
        var newHr = 0;
        var expt = "";
        var change = 0;
        var sum = 0;
        var counter = 0;
        var fill = 0;
        var counter2 = 0;
        var scale = 1;

        if(interval == "1h"){
            scale = 1;
        }else if(interval == "day"){
            scale = 24;
        }

        console.log("length: " + query.length);

        query.forEach(function(data){
            counter2 += 1;
            t = new Date(data._source.Time).getTime();
            m = new Date(data._source.Time).getMonth();


//            if(interval == "day" || interval == "1h"){

            if(interval == "day" || interval == "1h"){
                newHr = parseInt(t/(3600000*scale));
            }
            else if(interval == "month"){
                newHr = m;
            }
            if(state === 0){
                state = 1;
                xt = newHr;             // defines new day
//                    change = 1;
            }

            if(newHr > xt){
                change = 1;
                sum += E;
                fill = (newHr - xt - 1);// intervals with no data
                xt = newHr;             // defines new day
                P1 = 0;
                t1 = 0;
                E = 0;
            }
            var dt = (t-t1)/3600000;
            if(dt > 30){           // interruption in transmission
                P1 = 0;
                t1 = t;
            }
            else{

                var P = 0;
                if($('#phase_1').prop('checked')){
                    P += data._source.Voltage1*data._source.Current1*data._source.PowerFactor1;
                }
                if($('#phase_2').prop('checked')){
                    P += data._source.Voltage2*data._source.Current2*data._source.PowerFactor2;
                }
                if($('#phase_3').prop('checked')){
                    P += data._source.Voltage3*data._source.Current3*data._source.PowerFactor3;
                }

                E += 0.5*(P + P1)*dt;
                P1 = P;
                t1 = t;

            }
            if(change == 1 || query.length == counter2){
                change = 0;
//                console.log("fill: " + fill);

                if(query.length == counter2){
                    expt += E + "\n";
                    counter += 1;
                }
                else{
                    expt += sum + "\n";
                    counter += 1;
                    sum = 0;

                    if(fill > 0){
                        for(d = 0; d < fill; d++){
                            expt += '0\n';
                            counter += 1;
                        }
                    }

                }
            }
//            }
        });
        console.log("current count: " + counter);
//        console.log(expt);
        console.log("counter2: " + counter2);
        return expt;
    }

});