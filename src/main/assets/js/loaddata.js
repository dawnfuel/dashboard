$(document).ready(function() {

    $('.alert').on('close.bs.alert', function(e) {
        e.preventDefault();
        $(this).hide();
    });
    //changing the compress and enlarge icons
    $("a i").click(function() {
        if ($(this).hasClass('fa-compress')) {

            $(this).removeClass('fa-compress');
            $(this).addClass('fa-expand');
        } else if ($(this).hasClass('fa-expand')) {
            $(this).removeClass('fa-expand');
            $(this).addClass('fa-compress');
        }
    });

    //Adding table header in mobile
    if (screen.width < 1024) {
        $('.reactive').each(function() {
            var text = 'Reactive' + ' ' + ' ' + $(this).text();
            $(this).text(text);
        });
        $('.apparent').each(function() {
            var text = 'Apparent' + ' ' + ' ' + $(this).text();
            $(this).text(text);
        });
    }

    var margin = {
            top: 10,
            right: 40,
            bottom: 50,
            left: 60
        }, //margin2 = {top: 430, right: 80, bottom: 30, left: 40},
        width = parseInt(d3.select('#real_live').style('width'), 10),
        width2 = parseInt(d3.select('#CurrentPhaseGraph').style('width'), 10);
        widthFuel = parseInt(d3.select('#fuel_graph').style('width'), 10);
        height = 300 - margin.top - margin.bottom;
        height2 = 500 - margin.top - margin.bottom;
    var margin2 = {
        top: 10,
        right: 80,
        bottom: 70,
        left: 10
    }; //margin2 = {top: 430, right: 80, bottom: 30, left: 40};
    var pieSrc1;
    var pieSrc2, pieSrc3, pieSrc4;
    var ConfInner = {
        header: {
            title: {
                text: ""
            },
            location: "top-center"
        },
        size: {
            canvasHeight: 400,
            canvasWidth: 400,
            pieInnerRadius: "10%",
            pieOuterRadius: "35%"
        },
        misc: {
            pieCenterOffset: {
                x: 0,
                y: 0
            }
        },
        data: {
            sortOrder: "none",
            enabled: true,
            valueType: "percentage",
            content: []
        },
        labels: {
            percentage: {
                color: "gray"
            },
            mainLabel: {
                color: "white"
            },
            outer: {
                format: "label",
                hideWhenLessThanPercentage: null,
                pieDistance: 30
            },
            inner: {
                format: "percentage",
                hideWhenLessThanPercentage: null
            }
        },
        effects: {
            pullOutSegmentOnClick: {
                effect: "bounce",
                speed: 300,
                size: 10
            },
        },
        tooltips: {
            enabled: true,
            type: "placeholder", // caption|placeholder,
            string: "Energy Consumption: {value} Watt Hours"
        }
    };
    var Conf = {
        header: {
            title: {
                text: ""
            },
            location: "top-center"
        },
        size: {
            canvasHeight: 400,
            canvasWidth: 400,
            pieInnerRadius: "50%",
        },
        data: {
            sortOrder: "none",
            enabled: true,
            valueType: "percentage",
            content: [] //energy_data
        },
        misc: {
            pieCenterOffset: {
                x: 0,
                y: 0
            }
        },
        labels: {
            percentage: {
                color: "gray"
            },
            mainLabel: {
                color: "white"
            }
        },
        effects: {
            pullOutSegmentOnClick: {
                effect: "bounce",
                speed: 300,
                size: 10
            },
        },
        tooltips: {
            enabled: true,
            type: "placeholder", // caption|placeholder,
            string: "Energy Consumption: {value} Watt Hours"
        }
    };

    function getEnergyData(d, t1, P1, SrcName_Energy_Total, SrcName_P1, Energy_total, source_types, source_names, SrcName_energy_data, energy_data, typeColor, SrcByType, SrcNameByType, typeTime, nameTime) {

        var docSum1 = 0.0;
        var docSum2 = 0.0;

        for (stype = 0; stype < d.SourceType.buckets.length; stype++) {
            docSum1 += d.SourceType.buckets[stype].doc_count;
            if (source_types.indexOf(d.SourceType.buckets[stype].key) == -1) {
                source_types.push(d.SourceType.buckets[stype].key);
                Energy_total.push(0.0);
                P1.push(0.0);
                SrcNameByType.push([]);
                SrcByType.push(d.SourceType.buckets[stype].key);
            }

        }

        for (sname = 0; sname < d.SourceName.buckets.length; sname++) {
            docSum2 += d.SourceName.buckets[sname].doc_count;
            if (source_names.indexOf(d.SourceName.buckets[sname].key) == -1) {
                source_names.push(d.SourceName.buckets[sname].key);
                SrcName_Energy_Total.push(0.0);
                SrcName_P1.push(0.0);
                for (stype = 0; stype < d.SourceType.buckets.length; stype++) {
                    for (typeName = 0; typeName < d.SourceType.buckets[stype].SourceName.buckets.length; typeName++) {
                        if (d.SourceName.buckets[sname].key == d.SourceType.buckets[stype].SourceName.buckets[typeName].key) {
                            for (index = 0; index < SrcByType.length; index++) {
                                if (SrcByType[index] == d.SourceType.buckets[stype].key) {
                                    SrcNameByType[index].push(d.SourceName.buckets[sname].key);
                                    break;
                                }
                            }
                        }
                    }
                }
            }
        }

        var P2 = new Array(P1.length);
        var SrcName_P2 = new Array(SrcName_P1.length);

        for (stype = 0; stype < source_types.length; stype++) {
            var chk = -1;
            for (index = 0; index < d.buckets.length; index++) {
                if (d.buckets[index].key == source_types[stype]) {
                    chk = stype;
                    fraction = d.buckets[index].doc_count;
                    P2[stype] = d.Power_real_total.avg * fraction / docSum1;
                }
            }
            if (chk == -1) {
                P2[stype] = 0;
            }
        }


        for (sname = 0; sname < source_names.length; sname++) {
            var chk2 = -1;
            for (index = 0; index < d.SourceName.buckets.length; index++) {
                if (d.SourceName.buckets[index].key == source_names[sname]) {
                    chk2 = sname;
                    fraction = d.SourceName.buckets[index].doc_count;
                    SrcName_P2[sname] = d.Power_real_total.avg * fraction / docSum2;
                }
            }
            if (chk2 == -1) {
                SrcName_P2[sname] = 0;
            }
        }

        var t2 = new Date(d.time).getTime() / 3600000.0;
        var time_delta = t2 - t1;

        if (time_delta < 10) {
            for (i = 0; i < source_types.length; i++) {
                Energy_total[i] = Energy_total[i] + (P1[i] + P2[i]) * time_delta / 2;
            }

            for (i = 0; i < source_names.length; i++) {
                SrcName_Energy_Total[i] = SrcName_Energy_Total[i] + (SrcName_P1[i] + SrcName_P2[i]) * time_delta / 2;
            }
        }
        P1 = P2;
        SrcName_P1 = SrcName_P2;
        t1 = t2;

        energy_data = [];
        for (i = 0; i < source_types.length; i++) {
            energy_data.push({
                "label": source_types[i],
                "value": roundTo(Energy_total[i]),
                "color": color[typeColor.indexOf(source_types[i])]
            });
        }

        SrcName_energy_data = [];
        for (i = 0; i < source_names.length; i++) {
            SrcName_energy_data.push({
                "label": source_names[i],
                "value": roundTo(SrcName_Energy_Total[i]),
                "color": color[typeColor.indexOf(source_names[i])]
            });
        }

        var output = {};

        output.t1 = t1;
        output.P1 = P1;
        output.SrcName_Energy_Total = SrcName_Energy_Total;
        output.SrcName_P1 = SrcName_P1;
        output.Energy_total = Energy_total;
        output.source_types = source_types;
        output.source_names = source_names;
        output.SrcName_energy_data = SrcName_energy_data;
        output.energy_data = energy_data;
        output.typeColor = typeColor;
        output.SrcByType = SrcByType;
        output.SrcNameByType = SrcNameByType;
        output.typeTime = typeTime;
        output.nameTime = nameTime;

        return output;
    }

    function getTimeData(typeTime, nameTime, typeColor) {

        var time_type_data = [];
        for (i = 0; i < typeTime.length; i++) {
            time_type_data.push({
                "label": typeTime[i].key,
                "value": roundTo(typeTime[i].doc_count * 5),
                "color": color[typeColor.indexOf(typeTime[i].key)]
            });
        }

        var time_name_data = [];
        for (i = 0; i < nameTime.length; i++) {
            time_name_data.push({
                "label": nameTime[i].key,
                "value": roundTo(nameTime[i].doc_count * 5),
                "color": color[typeColor.indexOf(nameTime[i].key)]
            });
        }

        var output = {};
        output.time_type_data = time_type_data;
        output.time_name_data = time_name_data;

        return output;

    }

    function getCostData(costType, costName, typeColor) {

        var cost_type_data = [];
        for (i = 0; i < costType.length; i++) {
            cost_type_data.push({
                "label": costType[i].key,
                "value": roundTo(costType[i].TimeStats.sum),
                "color": color[typeColor.indexOf(costType[i].key)]
            });
        }

        var cost_name_data = [];
        for (i = 0; i < costName.length; i++) {
            cost_name_data.push({
                "label": costName[i].key,
                "value": roundTo(costName[i].TimeStats.sum),
                "color": color[typeColor.indexOf(costName[i].key)]
            });
        }

        var output = {};
        output.cost_type_data = cost_type_data;
        output.cost_name_data = cost_name_data;

        return output;

    }


    function getPowerData(powerType, powerName, typeColor) {

        var power_type_data = [];
        for (i = 0; i < powerType.length; i++) {
            power_type_data.push({
                "label": powerType[i].key,
                "value": roundTo(powerType[i].TimeStats.avg),
                "color": color[typeColor.indexOf(powerType[i].key)]
            });
        }



        var power_name_data = [];
        for (i = 0; i < powerName.length; i++) {
            power_name_data.push({
                "label": powerName[i].key,
                "value": roundTo(powerName[i].TimeStats.avg),
                "color": color[typeColor.indexOf(powerName[i].key)]
            });
        }

        var output = {};
        output.power_type_data = power_type_data;
        output.power_name_data = power_name_data;

        return output;

    }


    function sortData(SrcNameByType, SrcByType, SrcName_energy_data, energy_data, SrcName_P1, time_name_data, time_type_data, cost_name_data, cost_type_data, power_name_data, power_type_data, nameTime, typeColor) {

        var SrcName_energy_data_sorted = [];
        var time_name_data_sorted = [];
        var time_type_data_sorted = [];
        var SrcNameLoad_sorted = [];
        var nameTime_sorted = [];
        var cost_type_data_sorted = [];
        var cost_name_data_sorted = [];
        var power_type_data_sorted = [];
        var power_name_data_sorted = [];

        for (stype = 0; stype < SrcNameByType.length; stype++) {
            col = color[typeColor.indexOf(SrcByType[stype])].toString();
            for (sname = 0; sname < SrcNameByType[stype].length; sname++) {
                for (sname_data = 0; sname_data < SrcName_energy_data.length; sname_data++) {
                    if (SrcNameByType[stype][sname] == SrcName_energy_data[sname_data].label) {
                        SrcName_energy_data[sname_data].color = d3.rgb(col).brighter(sname * 0.3);
                        SrcName_energy_data_sorted.push(SrcName_energy_data[sname_data]);
                        SrcNameLoad_sorted.push(SrcName_P1[sname_data]);
                        break;
                    }
                }
                for (sname_data = 0; sname_data < time_name_data.length; sname_data++) {
                    if (SrcNameByType[stype][sname] == time_name_data[sname_data].label) {
                        time_name_data[sname_data].color = d3.rgb(col).brighter(sname * 0.3);
                        time_name_data_sorted.push(time_name_data[sname_data]);
                        nameTime_sorted.push(nameTime[sname_data]);
                        break;
                    }
                }
                for (sname_data = 0; sname_data < cost_name_data.length; sname_data++) {
                    if (SrcNameByType[stype][sname] == cost_name_data[sname_data].label) {
                        cost_name_data[sname_data].color = d3.rgb(col).brighter(sname * 0.3);
                        cost_name_data_sorted.push(cost_name_data[sname_data]);
                        break;
                    }
                }

                for (sname_data = 0; sname_data < power_name_data.length; sname_data++) {
                    if (SrcNameByType[stype][sname] == power_name_data[sname_data].label) {
                        power_name_data[sname_data].color = d3.rgb(col).brighter(sname * 0.3);
                        power_name_data_sorted.push(power_name_data[sname_data]);
                        break;
                    }
                }
            }
        }

        for (stype = 0; stype < energy_data.length; stype++) {
            for (stype_data = 0; stype_data < time_type_data.length; stype_data++) {
                if (energy_data[stype].label == time_type_data[stype_data].label) {
                    time_type_data_sorted.push(time_type_data[stype_data]);
                    break;
                }
            }
        }

        for (stype = 0; stype < energy_data.length; stype++) {
            for (stype_data = 0; stype_data < cost_type_data.length; stype_data++) {
                if (energy_data[stype].label == cost_type_data[stype_data].label) {
                    cost_type_data_sorted.push(cost_type_data[stype_data]);
                    break;
                }
            }
        }

        for (stype = 0; stype < energy_data.length; stype++) {
            for (stype_data = 0; stype_data < power_type_data.length; stype_data++) {
                if (energy_data[stype].label == power_type_data[stype_data].label) {
                    power_type_data_sorted.push(power_type_data[stype_data]);
                    break;
                }
            }
        }

        var sortedData = {};

        sortedData.SrcName_energy_data_sorted = SrcName_energy_data_sorted;
        sortedData.time_name_data_sorted = time_name_data_sorted;
        sortedData.time_type_data_sorted = time_type_data_sorted;
        sortedData.SrcNameLoad_sorted = SrcNameLoad_sorted;
        sortedData.nameTime_sorted = nameTime_sorted;
        sortedData.cost_name_data_sorted = cost_name_data_sorted;
        sortedData.cost_type_data_sorted = cost_type_data_sorted;
        sortedData.power_name_data_sorted = power_name_data_sorted;
        sortedData.power_type_data_sorted = power_type_data_sorted;

        return sortedData;
    }

    function createPie(Conf, pie_data, title, parent, id, tooltip) {
        Conf.data.content = pie_data;
        Conf.header.title.text = title;
        Conf.tooltips.string = tooltip;
        var pie = new d3pie(parent, Conf);
        document.getElementById(parent).children[0].setAttribute("id", id);
        return pie;
    }


    function secToHrs(time_Data){

        var t = [];
            for (i = 0; i < time_Data.length; i++) {
                t.push({
                    "label": time_Data[i].label,
                    "value": time_Data[i].value,
                    "color": time_Data[i].color
                });
            }

        for (i = 0; i < t.length; i++){
            t[i].value = roundTo(t[i].value/3600.0);
        }
        return t;
    }


    var date = new Date();

    function addMonth() {
    date = new Date(date.getFullYear(), date.getMonth()+1, 1);
    remove_monthlygraphElements();
    load_monthly("../monthly/" + config + "/" + probe + "/" +  moment(new Date(date.getFullYear(), date.getMonth(), 1)).format("YYYY-MM-DDTHH:mm:ss.SSSZ") + "/" + moment(new Date(date.getFullYear(), date.getMonth() + 1, 0)).format("YYYY-MM-DDTHH:mm:ss.SSSZ"));
    }

    function reduceMonth () {
        date = new Date(date.getFullYear(), date.getMonth()-1, 1);
        remove_monthlygraphElements();
        load_monthly("../monthly/" + config + "/" + probe + "/" +  moment(new Date(date.getFullYear(), date.getMonth(), 1)).format("YYYY-MM-DDTHH:mm:ss.SSSZ") + "/" + moment(new Date(date.getFullYear(), date.getMonth() + 1, 0)).format("YYYY-MM-DDTHH:mm:ss.SSSZ"));
        }

    var config = document.getElementById('config').options[0].value;
    var probe = document.getElementById('probe').options[0].value;
    var lastTransmit = 0;

    $("#probe").on('change', function() {
        var timeWindow = $('#hiddenTimeWindow').val();
        probe = $("#probe").val();
        config = document.getElementById('config').options[0].value;
        remove_graphElements();
        remove_livegraphElements();
        query(config, probe, "../bounds/" + config + "/" + probe, timeWindow);
        setProbeOnSummary();
    });

    $("#config").change(function() {
        var timeWindow = $('#hiddenTimeWindow').val();
        config = $("#config").val();
        probe = document.getElementById('probe').options[0].value;
        remove_graphElements();
        remove_livegraphElements();
        query(config, probe, "../bounds/" + config + "/" + probe, timeWindow);
        setConfOnSummary();
    });
    query(config, probe, "../bounds/" + config + "/" + probe, $('#hiddenTimeWindow').val());
    setProbeOnSummary();
    setConfOnSummary();



    var export_start = "";
    var export_stop = "";
    $("#export").click(function() {

        localStorage.removeItem('from');
        localStorage.removeItem('to');
        localStorage.removeItem('prb');
        localStorage.removeItem('cfg');

        localStorage.from = export_start;
        localStorage.to = export_stop;
        localStorage.prb = probe;
        localStorage.cfg = config;


    });



    var DateDiff = {

        inSeconds: function(d1, d2) {
            var t2 = d2.getTime();
            var t1 = d1.getTime();
            return parseInt((t2 - t1) / (1000));
        },
        inMinutes: function(d1, d2) {
            var t2 = d2.getTime();
            var t1 = d1.getTime();
            return parseInt((t2 - t1) / (60 * 1000));
        },

        inHours: function(d1, d2) {
            var t2 = d2.getTime();
            var t1 = d1.getTime();
            return parseInt((t2 - t1) / (3600 * 1000));
        },

        inDays: function(d1, d2) {
            var t2 = d2.getTime();
            var t1 = d1.getTime();

            return parseInt((t2 - t1) / (24 * 3600 * 1000));
        },

        inWeeks: function(d1, d2) {
            var t2 = d2.getTime();
            var t1 = d1.getTime();

            return parseInt((t2 - t1) / (24 * 3600 * 1000 * 7));
        },

        inMonths: function(d1, d2) {
            var d1Y = d1.getFullYear();
            var d2Y = d2.getFullYear();
            var d1M = d1.getMonth();
            var d2M = d2.getMonth();

            return (d2M + 12 * d2Y) - (d1M + 12 * d1Y);
        },

        inYears: function(d1, d2) {
            return d2.getFullYear() - d1.getFullYear();
        }
    };

    function getDateDiff(d1, d2) {

        if (DateDiff.inSeconds(d1, d2) < 60) {
            if (DateDiff.inSeconds(d1, d2) === 1) {
                return DateDiff.inSeconds(d1, d2) + ' second';
            }
            return DateDiff.inSeconds(d1, d2) + ' seconds';
        } else if (DateDiff.inMinutes(d1, d2) < 60) {
            if (DateDiff.inMinutes(d1, d2) === 1) {
                return DateDiff.inMinutes(d1, d2) + ' minute';
            }
            return DateDiff.inMinutes(d1, d2) + ' minutes';
        } else if (DateDiff.inHours(d1, d2) < 24) {
            if (DateDiff.inHours(d1, d2) === 1) {
                return DateDiff.inHours(d1, d2) + ' hour';
            }
            return DateDiff.inHours(d1, d2) + ' hours';
        } else if (DateDiff.inDays(d1, d2) < 7) {

            if (DateDiff.inDays(d1, d2) === 1) {
                return DateDiff.inDays(d1, d2) + ' day';
            }
            return DateDiff.inDays(d1, d2) + ' days';
        } else if (DateDiff.inWeeks(d1, d2) < 4) {
            if (DateDiff.inWeeks(d1, d2) === 1) {
                return DateDiff.inWeeks(d1, d2) + ' week';
            }
            return DateDiff.inWeeks(d1, d2) + ' weeks';
        } else if (DateDiff.inMonths(d1, d2) < 11) {
            if (DateDiff.inMonths(d1, d2) === 1) {
                return DateDiff.inMonths(d1, d2) + ' month';
            }
            return DateDiff.inMonths(d1, d2) + ' months';
        } else if (DateDiff.inYears(d1, d2) > 0) {
            if (DateDiff.inYears(d1, d2) === 1) {
                return DateDiff.inYears(d1, d2) + ' year';
            }
            return DateDiff.inYears(d1, d2) + ' years';
        } else {
            return 'n/a ';
        }

    }


    function getDateDiff2(d1, d2) {

            if (DateDiff.inDays(d1, d2) < 7) {
                if (DateDiff.inDays(d1, d2) <= 1) {
                    return (moment(d1).format("MMM Do"));
                }
               return ('from ' + moment(d1).format("MMM Do") + ' to ' + moment(d2).format("MMM Do"));
            } else if (DateDiff.inWeeks(d1, d2) < 4) {

                return ('from ' + moment(d1).format("MMM Do") + ' to ' + moment(d2).format("MMM Do"));
            } else if (DateDiff.inMonths(d1, d2) < 11) {
                return ('from ' + moment(d1).format("MMM Do") + ' to ' + moment(d2).format("MMM Do"));
            } else if (DateDiff.inYears(d1, d2) > 0) {
                if (moment(d1).format("YYYY") === moment(d2).format("YYYY")) {
                    return (moment(d1).format("YYYY"));
                }
                return ('from ' + moment(d1).format("YYYY") + ' to ' + moment(d2).format("YYYY"));
            } else {
                return 'n/a ';
            }

        }

    $('.alert').hide();
    $('.alert').on('close.bs.alert', function(e) {
        e.preventDefault();
        $(this).hide();
    });
    //Alert
    function closeAlert() {
        $('.alert').show();
        $(".alert").fadeTo(1500, "swing").slideUp(500);
    }

    function query(config, probe, url, TimeWindow, user_stop) {
        var windowSizeMsg = "";
        $('#hiddenTimeWindow').val(TimeWindow); //set window to a hidden field so it can be accessible by other functions
        $.get(url, function(data) {
            console.log(data);
            data_stop_realtime = data.stop; // most recent value returned from snippet

            data_stop = typeof user_stop !== 'undefined' ? user_stop : data_stop_realtime;
            data_stop = moment(parseInt(data_stop)).format("YYYY-MM-DDTHH:mm:ss.SSSZ");
            dp2.data("DateTimePicker").date(moment(data_stop)); //set dateTimePicker2 time
            // tz awareness pending
            var user_start = new Date().getTime() - parseFloat(TimeWindow);
            var data_start = "";
            var popMSg;


            if (user_start < data.start) {
                data_start = moment(data.start).format("YYYY-MM-DDTHH:mm:ss.SSSZ");
                dp1.data("DateTimePicker").date(moment(data.start)); //set dateTimePicker1 time
                popMSg = "from  " + moment(data.start).format("DD-MM-YYYY HH:mm:ss.SSSZ");

            } else {
                data_start = moment(user_start).format("YYYY-MM-DDTHH:mm:ss.SSSZ");
                data_start = moment(user_start).format("YYYY-MM-DDTHH:mm:ss.SSSZ");
                dp1.data("DateTimePicker").date(moment(user_start)); //set dateTimePicker1 time
                popMSg = "from   " + moment(user_start).format("DD-MM-YYYY HH:mm:ss.SSSZ");
            }



            if (data_start > data_stop) {

                if (sessionStorage.length > 0) {
                    $("#timeWindowSpan").html(sessionStorage.windowSizeMsg);
                    $("#timeWindowSpan").attr("title", sessionStorage.popMsg);
                    load_data("../data/" + config + "/" + probe + "/" + sessionStorage.data_start + "/" + sessionStorage.data_stop);
                    load_recent("../recent/" + config + "/" + probe + "/" + moment(parseInt(sessionStorage.data_stop_realtime)).format("YYYY-MM-DDTHH:mm:ss.SSSZ") + "/" + sessionStorage.realTimeWindow);
                    lastTransmit = sessionStorage.data_stop;
                }
                return closeAlert();

            }

            var timeCheck = Date.now() - 120000 ;
            timeCheck = moment(parseInt(timeCheck)).format("YYYY-MM-DDTHH:mm:ss.SSSZ");
            //windowSizeMsg = 'from Last  '+ getDateDiff (new Date(moment(data_start).format("MM-DD-YYYY HH:mm:ss.SSSZ")), new Date (moment(data_stop).format("MM-DD-YYYY HH:mm:ss.SSSZ")));
            if (data_stop > timeCheck ) {

            windowSizeMsg = 'last ' + getDateDiff(new Date(data_start), new Date(data_stop));
            popMSg += "   to  " + moment(data_stop).format("DD-MM-YYYY HH:mm:ss.SSSZ");

            }else {
            windowSizeMsg = getDateDiff2(new Date(data_start), new Date(data_stop));
            popMSg += "   to  " + moment(data_stop).format("DD-MM-YYYY HH:mm:ss.SSSZ");

            }

            export_start = data_start;
            export_stop = data_stop;


            $("#timeWindowSpan").html(windowSizeMsg);
            $("#timeWindowSpan").attr("title", popMSg);
            load_data("../data/" + config + "/" + probe + "/" + data_start + "/" + data_stop);
            load_recent("../recent/" + config + "/" + probe + "/" + moment(parseInt(data_stop_realtime)).format("YYYY-MM-DDTHH:mm:ss.SSSZ") + "/" + realTimeWindow);
            lastTransmit = data.stop;

            //sessions
            sessionStorage.popMsg = popMSg;
            sessionStorage.windowSizeMsg = windowSizeMsg;
            sessionStorage.data_stop_realtime = data_stop_realtime;
            sessionStorage.data_start = data_start;
            sessionStorage.data_stop = data_stop;
            sessionStorage.realTimeWindow = realTimeWindow;
        });
    }

    var brush;
    var brush_status = 0;
    var brush0 = 0;
    var brush1 = 0;

    var  energyElements;//Energy
    var  costElements;//Cost
    var  powerElements;  // Power
    var  reactiveElements; // Reactive Power
    var  apparentElements; //Apparent Power

     var bars;//Power
     var barsReactive;//Reactive Power
     var barsApparent;//Apparent Power
     var bars_cost;//Cost
     var bars_energy;//Energy
     var lined;//fuel
     var linedRate;//FuelRate
     var linedVoltage = [];//Voltage
     var linedCurrent = [];//Current
     var linedpf = []; //pf

     var pathReal= [];
     var pathReactive= [];
     var pathApparent=[];

      var textMonth;

     var opts = {
       lines: 13, // The number of lines to draw
       length: 20, // The length of each line
       width: 5, // The line thickness
       radius: 30, // The radius of the inner circle
       scale: 10, // Scales overall size of the spinner
       color: '#FFFFFF', // #rgb or #rrggbb or array of colors
       speed: 1.9, // Rounds per second
       trail: 40, // Afterglow percentage
       className: 'spinner', // The CSS class to assign to the spinner
     };

    var parseTimeISO = d3.time.format("%Y-%m-%d %H:%M:%S").parse;

    //Power axes
    var x = d3.time.scale().range([0, width]);
    var y = d3.scale.linear().range([height, 0]);
    var svg = d3.select("#graph").append("svg")
        .attr("id", "powergraph")
        .attr("viewBox", String(0) + " " + String(0) + " " + String(width + margin.left + margin.right) + " " + String(height + margin.top + margin.bottom))
        .attr("perserveAspectRatio", "xMinYMid")
        .attr("width", width + margin.left + margin.right)
        .attr("height", height + margin.top + margin.bottom)
        .append("g")
        .attr("transform", "translate(" + margin.left + "," + margin.top + ")");
    var context = svg.append("g")
        .attr("id", "context")
        .attr("class", "context"); //.attr("transform", "translate(" + margin.left + "," + margin.top + ")");
    var xAxis = d3.svg.axis()
        .scale(x)
        .orient("bottom");
    var yAxis1 = d3.svg.axis()
        .scale(y)
        .orient("left");

    //Reactive power axes
        var yReactive = d3.scale.linear().range([height, 0]);
        var svgReactive = d3.select("#graphreactive").append("svg")
            .attr("id", "powerReactivegraph")
            .attr("viewBox", String(0) + " " + String(0) + " " + String(width + margin.left + margin.right) + " " + String(height + margin.top + margin.bottom))
            .attr("perserveAspectRatio", "xMinYMid")
            .attr("width", width + margin.left + margin.right)
            .attr("height", height + margin.top + margin.bottom)
            .append("g")
            .attr("transform", "translate(" + margin.left + "," + margin.top + ")");
        var contextReactive = svgReactive.append("g")
            .attr("id", "context")
            .attr("class", "context"); //.attr("transform", "translate(" + margin.left + "," + margin.top + ")");
        var yAxis1Reactive = d3.svg.axis()
            .scale(yReactive)
            .orient("left");

    //Apparent power axes
        var yApparent = d3.scale.linear().range([height, 0]);
        var svgApparent = d3.select("#graphapparent").append("svg")
            .attr("id", "powerApparentgraph")
            .attr("viewBox", String(0) + " " + String(0) + " " + String(width + margin.left + margin.right) + " " + String(height + margin.top + margin.bottom))
            .attr("perserveAspectRatio", "xMinYMid")
            .attr("width", width + margin.left + margin.right)
            .attr("height", height + margin.top + margin.bottom)
            .append("g")
            .attr("transform", "translate(" + margin.left + "," + margin.top + ")");
        var contextApparent = svgApparent.append("g")
            .attr("id", "context")
            .attr("class", "context"); //.attr("transform", "translate(" + margin.left + "," + margin.top + ")");
        var yAxis1Apparent = d3.svg.axis()
            .scale(yApparent)
            .orient("left");


    //Cost axes
    var xc = d3.time.scale().range([0, width]);
    var yc = d3.scale.linear().range([height, 0]);
    var svgc = d3.select("#cost_graph").append("svg")
        .attr("id", "costgraph")
        .attr("viewBox", String(0) + " " + String(0) + " " + String(width + margin.left + margin.right) + " " + String(height + margin.top + margin.bottom))
        .attr("perserveAspectRatio", "xMinYMid")
        .attr("width", width + margin.left + margin.right)
        .attr("height", height + margin.top + margin.bottom)
        .attr("data-html", "Use the <i class=\"fa fa-plus\"></i> to zoom into a time range")
        .attr("data-variation", "huge")
        .append("g")
        .attr("transform", "translate(" + margin.left + "," + margin.top + ")");
    var contextc = svgc.append("g")
        .attr("id", "contextc")
        .attr("class", "contextc");
    var xAxisc = d3.svg.axis()
        .scale(xc)
        .orient("bottom");
    var yAxis1c = d3.svg.axis()
        .scale(yc)
        .orient("left"); //end

//EnergyByDay axes
    var xEnergyDay = d3.time.scale().range([0, width2]);
    var yEnergyDay = d3.scale.linear().range([height, 0]);
    var svgEnergyDay = d3.select("#energyDay_graph").append("svg")
        .attr("id", "energyDayGraph")
        .attr("viewBox", String(0) + " " + String(0) + " " + String(width2 + margin.left + margin.right) + " " + String(height + margin.top + margin.bottom))
        .attr("perserveAspectRatio", "xMinYMid")
        .attr("width", width2 + margin.left + margin.right)
        .attr("height", height + margin.top + margin.bottom)
        .append("g")
        .attr("transform", "translate(" + margin.left + "," + margin.top + ")");
    var contextEnergyDay = svgEnergyDay.append("g")
        .attr("id", "context")
        .attr("class", "context");
    var xAxisEnergyDay = d3.svg.axis()
        .scale(xEnergyDay)
        .orient("bottom")
        .ticks(d3.time.days, 1)
        .tickFormat(d3.time.format(' %d'))
        .tickSize(5);
    var yAxis1EnergyDay = d3.svg.axis()
        .scale(yEnergyDay)
        .orient("left"); //end

        svgEnergyDay.append("svg:a").attr("xlink:href", "#0").append("svg:path").attr("d", "M " + (0.955 * width2) + " -10 L " + ((0.94 * width2)) + " 0 L " + (0.955 * width2) + " 10 L " + (0.955 * width2) + " -10")
            .attr("fill", "#96989E")
            /*  .attr("transform", "translate("+(-70) +"," + (-10) + ")")*/
            .on('mouseover', function() {
                $(this).attr("fill", "white");
            })
            .on('mouseout', function() {
                $(this).attr("fill", "#96989E");
            })
            /*  .on( 'mouseup',tip.hide )*/
            .on('click', function() {
                reduceMonth();
            });

        svgEnergyDay.append("svg:a").attr("xlink:href", "#0").append("svg:path").attr("d", "M " + (0.965 * width2) + " -10 L " + ((0.98 * width2)) + " 0 L " + (0.965 * width2) + " 10 L " + (0.965 * width2) + " -10")
            .attr("fill", "#96989E")
            .on('mouseover', function() {
                $(this).attr("fill", "white");
            })
            .on('mouseout', function() {
                $(this).attr("fill", "#96989E");
            })
            .on('click', function() {

            if (date.getFullYear() < new Date().getFullYear()) {
                addMonth();
            } else {
                if (date.getMonth() + 1 <= new Date().getMonth()) {
                    addMonth();
                } else {

                }
            }

            });

    //Energy axes
    var xe = d3.time.scale().range([0, width]);
    var ye = d3.scale.linear().range([height, 0]);
    var svge = d3.select("#energy_graph").append("svg")
        .attr("id", "energygraph")
        .attr("viewBox", String(0) + " " + String(0) + " " + String(width + margin.left + margin.right) + " " + String(height + margin.top + margin.bottom))
        .attr("perserveAspectRatio", "xMinYMid")
        .attr("width", width + margin.left + margin.right)
        .attr("height", height + margin.top + margin.bottom)
        .attr("data-html", "Use the <i class=\"fa fa-plus\"></i> to zoom into a time range")
        .attr("data-variation", "huge")
        .append("g")
        .attr("transform", "translate(" + margin.left + "," + margin.top + ")");
    var contexte = svge.append("g")
        .attr("id", "contexte")
        .attr("class", "context");
    var xAxise = d3.svg.axis()
        .scale(xe)
        .orient("bottom");
    var yAxis1e = d3.svg.axis()
        .scale(ye)
        .orient("left"); //end

    //fuel axes
    var xf = d3.time.scale().range([0, widthFuel]);
    var yf = d3.scale.linear().range([height, 0]);
    var svgFuel = d3.select("#fuel_graph").append("svg")
        .attr("id", "fuelgraph")
        .attr("viewBox", String(0) + " " + String(0) + " " + String(widthFuel + margin.left + margin.right) + " " + String(height + margin.top + margin.bottom))
        .attr("perserveAspectRatio", "xMinYMid")
        .attr("width", widthFuel + margin.left + margin.right)
        .attr("height", height + margin.top + margin.bottom)
        .append("g")
        .attr("transform", "translate(" + margin.left + "," + margin.top + ")");
    var contextFuel = svgFuel.append("g")
        .attr("id", "context")
        .attr("class", "context");
    var xAxisf = d3.svg.axis()
        .scale(xf)
        .orient("bottom");
    var yAxis1f = d3.svg.axis()
        .scale(yf)
        .orient("left"); //end
    var lineFuel = d3.svg.line()
        .x(function(d) {
            return xf(d.time);
        })
        .y(function(d) {
            return yf(d.total);
        })
        .interpolate("linear");

    var xfr = d3.time.scale().range([0, widthFuel]);
    var yfr = d3.scale.linear().range([height, 0]);
    var svgFuelRate = d3.select("#fuelCharts").append("svg")
        .attr("id", "fuelCharts")
        .attr("viewBox", String(0) + " " + String(0) + " " + String(widthFuel + margin.left + margin.right) + " " + String(height + margin.top + margin.bottom))
        .attr("perserveAspectRatio", "xMinYMid")
        .attr("width", widthFuel + margin.left + margin.right)
        .attr("height", height + margin.top + margin.bottom)
        .append("g")
        .attr("transform", "translate(" + margin.left + "," + margin.top + ")");
    var contextFuelRate = svgFuelRate.append("g")
        .attr("id", "context")
        .attr("class", "context");
    /*var xAxisfr = d3.svg.axis()
        .scale(xfr)
        .orient("bottom");*/
    var yAxis1fr = d3.svg.axis()
        .scale(yfr)
        .orient("left"); //end
    var lineFuelRate = d3.svg.line()
        .x(function(d) {
            return xfr(d.time);
        })
        .y(function(d) {
            return yfr(d.total);
        })
        .interpolate("linear");


    //Voltage axes
    var xv = d3.time.scale().range([0, width2]);
    var yv = d3.scale.linear().range([height, 0]);
    var svgVoltage = d3.select("#VoltagePhaseGraph").append("svg")
        .attr("id", "voltagegraph")
        .attr("viewBox", String(0) + " " + String(0) + " " + String(width2 + margin.left + margin.right) + " " + String(height + margin.top + margin.bottom))
        .attr("perserveAspectRatio", "xMinYMid")
        .attr("width", width2 + margin.left + margin.right)
        .attr("height", height + margin.top + margin.bottom)
        .append("g")
        .attr("transform", "translate(" + margin.left + "," + margin.top + ")");
    var contextVoltage = svgVoltage.append("g");
        /*.attr("id", "context")
        .attr("class", "context");*/
    var xAxisv = d3.svg.axis()
        .scale(xv)
        .orient("bottom")
        .ticks(d3.time.second, 30);
    var yAxis1v = d3.svg.axis()
        .scale(yv)
        .orient("left"); //end
    var lineVoltage = d3.svg.line()
        .x(function(d) {
            return xv(d.time);
        })
        .y(function(d) {
            return yv(d.voltage);
        })
        .interpolate("basis");

    //current axes
    var xcurrent = d3.time.scale().range([0, width2]);
    var ycurrent = d3.scale.linear().range([height, 0]);
    var svgCurrent = d3.select("#CurrentPhaseGraph").append("svg")
        .attr("id", "currentgraph")
        .attr("viewBox", String(0) + " " + String(0) + " " + String(width2 + margin.left + margin.right) + " " + String(height + margin.top + margin.bottom))
        .attr("perserveAspectRatio", "xMinYMid")
        .attr("width", width2 + margin.left + margin.right)
        .attr("height", height + margin.top + margin.bottom)
        .append("g")
        .attr("transform", "translate(" + margin.left + "," + margin.top + ")");
    var contextCurrent = svgCurrent.append("g")
        .attr("id", "context")
        .attr("class", "context");
    var xAxiscurrent = d3.svg.axis()
        .scale(xcurrent)
        .orient("bottom")
        .ticks(d3.time.second, 30);
    var yAxis1Current = d3.svg.axis()
        .scale(ycurrent)
        .orient("left"); //end
    var lineCurrent = d3.svg.line()
        .x(function(d) {
            return xv(d.time);
        })
        .y(function(d) {
            return ycurrent(d.current);
        })
        .interpolate("basis");

    //pf axes
    var ypf = d3.scale.linear().range([height, 0]);
    var svgpf = d3.select("#pfPhaseGraph").append("svg")
        .attr("id", "pfgraph")
        .attr("viewBox", String(0) + " " + String(0) + " " + String(width2 + margin.left + margin.right) + " " + String(height + margin.top + margin.bottom))
        .attr("perserveAspectRatio", "xMinYMid")
        .attr("width", width2 + margin.left + margin.right)
        .attr("height", height + margin.top + margin.bottom)
        .append("g")
        .attr("transform", "translate(" + margin.left + "," + margin.top + ")");
    var contextpf = svgpf.append("g")
        .attr("id", "context")
        .attr("class", "context");
    var yAxis1pf = d3.svg.axis()
        .scale(ypf)
        .orient("left"); //end
    var linepf = d3.svg.line()
        .x(function(d) {
            return xv(d.time);
        })
        .y(function(d) {
            return ypf(d.pf);
        })
        .interpolate("basis");

    //battery
        var xb = d3.time.scale().range([0, widthFuel]);
        var yb = d3.scale.linear().range([height, 0]);
        var svgBattery = d3.select("#capGraph").append("svg")
            .attr("id", "capGraph")
            .attr("viewBox", String(0) + " " + String(0) + " " + String(widthFuel + margin.left + margin.right) + " " + String(height + margin.top + margin.bottom))
            .attr("perserveAspectRatio", "xMinYMid")
            .attr("width", widthFuel + margin.left + margin.right)
            .attr("height", height + margin.top + margin.bottom)
            .append("g")
            .attr("transform", "translate(" + margin.left + "," + margin.top + ")");
        var contextBattery = svgBattery.append("g")
            .attr("id", "context")
            .attr("class", "context");
        var xAxisb = d3.svg.axis()
            .scale(xb)
            .orient("bottom");
        var yAxis1b = d3.svg.axis()
            .scale(yb)
            .orient("left"); //end
        var lineBattery = d3.svg.line()
            .x(function(d) {
                return xb(d.time);
            })
            .y(function(d) {
                return yb(d.total);
            })
            .interpolate("linear");

        var xDOD = d3.time.scale().range([0, widthFuel]);
        var yDOD = d3.scale.linear().range([height, 0]);
        var svgDOD = d3.select("#dodGraph").append("svg")
            .attr("id", "dodGraph")
            .attr("viewBox", String(0) + " " + String(0) + " " + String(widthFuel + margin.left + margin.right) + " " + String(height + margin.top + margin.bottom))
            .attr("perserveAspectRatio", "xMinYMid")
            .attr("width", widthFuel + margin.left + margin.right)
            .attr("height", height + margin.top + margin.bottom)
            .append("g")
            .attr("transform", "translate(" + margin.left + "," + margin.top + ")");
        var contextDOD = svgDOD.append("g")
            .attr("id", "context")
            .attr("class", "context");
        /*var xAxisfr = d3.svg.axis()
            .scale(xfr)
            .orient("bottom");*/
        var yAxis1DOD = d3.svg.axis()
            .scale(yDOD)
            .orient("left"); //end
        var lineDOD = d3.svg.line()
            .x(function(d) {
                return xDOD(d.time);
            })
            .y(function(d) {
                return yDOD(d.total);
            })
            .interpolate("linear");


    //realtime
    var xl = d3.time.scale().range([0, width]);
    yl = d3.scale.linear().range([height, 0]);
    var ylApparent = d3.scale.linear().range([height, 0]);
    var ylReactive = d3.scale.linear().range([height, 0]);

    var xAxis_realtime = d3.svg.axis()
        .scale(xl)
        .orient("bottom");
    //.tickFormat(d3.time.format("%H:%M:%S"));

    var yAxis_realtime = d3.svg.axis()
        .scale(yl)
        .orient("left");

    var yAxis_realtimeApparent = d3.svg.axis()
        .scale(ylApparent)
        .orient("left");

    var yAxis_realtimeReactive = d3.svg.axis()
            .scale(ylReactive)
            .orient("left");


     var line = line_ (xl,yl,'');
     var lineRealApparent = line_ (xl,ylApparent,'apparent');
     var lineRealReactive = line_ (xl, ylReactive,'reactive');
    /*var line = d3.svg.line()
        .x(function(d) {
            return xl(d.time);
        })
        .y(function(d) {
            return yl(d.total);
        });*/

        function line_ (x_,y_,d_) {

        realLine_ = d3.svg.line()
        .x(function(d) {
            return x_(d.time);
        })
        .y(function(d) {
        if (d_ == 'reactive'){
         return y_(d.reactive);
        }else if(d_  == 'apparent'){
         return y_(d.apparent);
        } else {
        return y_(d.total);
        }

        });

        return  realLine_;
        }

    /*var areainv = d3.svg.area()
        .defined(function(d) {
            return d.sourceType == 'inverter';
        })
        .x(function(d) {
            return xl(d.time);
        })
        .y1(function(d) {
            return yl(d.total);
        })
        .y0(y(0));*/


    /*var areagen = d3.svg.area()

    .defined(function(d) {
            return d.sourceType == 'generator';
        })
        .x(function(d) {
            return xl(d.time);
        })
        .y1(function(d) {
            return yl(d.total);
        })
        .y0(y(0));*/

    function areagen_(x_,y_,d_) {
       gen_ = d3.svg.area()

        .defined(function(d) {
                return d.sourceType == 'generator';
            })
            .x(function(d) {
                return x_(d.time);
            })
            .y1(function(d) {
                if (d_ == 'reactive'){
                     return y_(d.reactive);
                    }else if(d_  == 'apparent'){
                     return y_(d.apparent);
                    } else {
                    return y_(d.total);
                    }
            })
            .y0(y(0));

            return gen_;
            }

    function areainv_ (x_,y_,d_) {
            inv_ = d3.svg.area()
            .defined(function(d) {
                return d.sourceType == 'inverter';
            })
            .x(function(d) {
                return x_(d.time);
            })
            .y1(function(d) {
                if (d_ == 'reactive'){
                   return y_(d.reactive);
                  }else if(d_  == 'apparent'){
                   return y_(d.apparent);
                  } else {
                  return y_(d.total);
                  }
            })
            .y0(y(0));

            return inv_;

            }

    function areagrd_ (x_,y_,d_) {
         grd_ = d3.svg.area()
         .defined(function(d) {
             return d.sourceType == 'grid';
         })
         .x(function(d) {
             return x_(d.time);
         })
         .y1(function(d) {
              if (d_ == 'reactive'){
                return y_(d.reactive);
               }else if(d_  == 'apparent'){
                return y_(d.apparent);
               } else {
               return y_(d.total);
               }
         })
         .y0(y(0));

        return grd_;

         }


    var areagen = areagen_(xl,yl,'');
    var areainv = areainv_(xl,yl,'');
    var areagrd = areagrd_(xl,yl,'');

    var areagenApparent = areagen_(xl,ylApparent,'apparent');
    var areainvApparent = areainv_(xl,ylApparent,'apparent');
    var areagrdApparent = areagrd_(xl,ylApparent,'apparent');

    var areagenReactive = areagen_(xl,ylReactive,'reactive');
    var areainvReactive = areainv_(xl,ylReactive,'reactive');
    var areagrdReactive = areagrd_(xl,ylReactive,'reactive');

    /*var areagrd = d3.svg.area()
        .defined(function(d) {
            return d.sourceType == 'grid';
        })
        .x(function(d) {
            return xl(d.time);
        })
        .y1(function(d) {
            return yl(d.total);
        })
        .y0(y(0));*/

    var svg_realtime = d3.select("#realpower_live").append("svg")
        .attr("id", "realtimegraph")
        .attr("viewBox", String(0) + " " + String(0) + " " + String(width + margin.left + margin.right) + " " + String(height + margin.top + margin.bottom))
        .attr("width", width + margin.left + margin.right)
        .attr("height", height + margin.top + margin.bottom)
        .append("g")
        .attr("transform", "translate(" + margin2.left + "," + margin2.top + ")");

    var focus = svg_realtime.append("g")
        .attr("id", "focus")
        .attr("class", "focus")
        .attr("transform", "translate(" + margin.left + "," + margin.top + ")");

    var svg_realtimeApparent = d3.select("#apparentpower_live").append("svg")
        .attr("id", "realtimegraph")
        .attr("viewBox", String(0) + " " + String(0) + " " + String(width + margin.left + margin.right) + " " + String(height + margin.top + margin.bottom))
        .attr("width", width + margin.left + margin.right)
        .attr("height", height + margin.top + margin.bottom)
        .append("g")
        .attr("transform", "translate(" + margin2.left + "," + margin2.top + ")");

    var focusApparent = svg_realtimeApparent.append("g")
        .attr("id", "focus")
        .attr("class", "focus")
        .attr("transform", "translate(" + margin.left + "," + margin.top + ")");

    var svg_realtimeReactive = d3.select("#reactivepower_live").append("svg")
        .attr("id", "realtimegraph")
        .attr("viewBox", String(0) + " " + String(0) + " " + String(width + margin.left + margin.right) + " " + String(height + margin.top + margin.bottom))
        .attr("width", width + margin.left + margin.right)
        .attr("height", height + margin.top + margin.bottom)
        .append("g")
        .attr("transform", "translate(" + margin2.left + "," + margin2.top + ")");

    var focusReactive = svg_realtimeReactive.append("g")
        .attr("id", "focus")
        .attr("class", "focus")
        .attr("transform", "translate(" + margin.left + "," + margin.top + ")");


    function make_x_axis() {
        return d3.svg.axis()
            .scale(x)
            .orient("bottom")
            .ticks(10);
    }

    function make_y_axis() {
        return d3.svg.axis()
            .scale(y)
            .orient("left")
            .ticks(4);
    }


    function make_x_axisf() {
        return d3.svg.axis()
            .scale(xf)
            .orient("bottom")
            .ticks(10);
    }

    function resetBrush() {
        brush
            .clear()
            .event(d3.select(".brush"));
    }

    function roundTo(num) {
        return +(Math.round(num + "e+2") + "e-2");
    }


    function powerGraphScale(power) {
        if (+power >= 1000) {
            var scaledPower = (power / 1000);
            return roundTo(scaledPower) + "KW";
        } else {
            return roundTo(power) + "W";
        }
    }

    function remove_graphElements() {
        try {
            bars.remove();
            bars_cost.remove();
            bars_energy.remove();
            barsApparent.remove();
            barsReactive.remove();
            energyElements.forEach(function (d){d.remove();});
            costElements.forEach(function (d){d.remove();});
            powerElements.forEach(function (d){d.remove();});
            apparentElements.forEach(function (d){d.remove();});
            reactiveElements.forEach(function (d){d.remove();});
            fuelElements.forEach(function (d){d.remove();});
            fuelRateElements.forEach(function (d){d.remove();});
            batteryCapElements.forEach(function (d){d.remove();});
            batteryDODElements.forEach(function (d){d.remove();});
            lined.remove();
            linedRate.remove();
            linedBatteryCap.remove();
            linedBatteryDOD.remove();
        } catch (err) {
            console.log(err.message);
        }
    }

    function remove_monthlygraphElements() {
        try {
           bars_energyDay.remove();
           EnergyByDayElements.forEach(function (d){d.remove();});
           textMonth.remove();
        } catch (err) {
            console.log(err.message);
        }
        }

    function remove_livegraphElements() {
        try {
            gridlx.remove();
            gridly.remove();
            axislx.remove();
            axisly.remove();
            axislx_label.remove();
            axisly_label.remove();
           /* pathgen.remove();
            pathgrid.remove();
            pathinv.remove();
            path1.remove();*/
            pathReal.forEach(function (d){d.remove();});
            pathApparent.forEach(function (d){d.remove();});
            pathReactive.forEach(function (d){d.remove();});
            realApparentElements.forEach(function (d){d.remove();});
            realReactiveElements.forEach(function (d){d.remove();});
            voltageElements.forEach(function (d){d.remove();});
            currentElements.forEach(function (d){d.remove();});
            pfElements.forEach(function (d){d.remove();});
            linedCurrent[0].remove();
            linedCurrent[1].remove();
            linedCurrent[2].remove();
            linedVoltage[0].remove();
            linedVoltage[1].remove();
            linedVoltage[2].remove();
            linedpf[0].remove();
            linedpf[1].remove();
            linedpf[2].remove();
        } catch (err) {
            console.log(err.message);
        }
    }

    function brushend() {
        if (brush.empty()) {

            return 0;
        }
        if (brush_status == 1) {
            //update_timefilter();
            window_start = moment(new Date(brush0)).format("YYYY-MM-DDTHH:mm:ss.SSSZ");
            window_stop = moment(new Date(brush1)).format("YYYY-MM-DDTHH:mm:ss.SSSZ");
            window_start_unix = moment(new Date(brush0)).unix();
            window_stop_unix = moment(new Date(brush1)).unix();

            var brushPopMSg;
            var timeCheck = Date.now() - 120000 ;
            timeCheck = moment(parseInt(timeCheck)).format("YYYY-MM-DDTHH:mm:ss.SSSZ");
            if (window_stop > timeCheck ) {

            brushPopMSg = "from  " + moment(new Date(brush0)).format("DD-MM-YYYY HH:mm:ss") + "   to  " + moment(new Date(brush1)).format("DD-MM-YYYY HH:mm:ss");
            $("#timeWindowSpan").html('from ' + getDateDiff(new Date(brush0), new Date(brush1)));
            $("#timeWindowSpan").attr("title", brushPopMSg);

            }else {
            brushPopMSg = "from  " + moment(new Date(brush0)).format("DD-MM-YYYY HH:mm:ss") + "   to  " + moment(new Date(brush1)).format("DD-MM-YYYY HH:mm:ss");
            $("#timeWindowSpan").html(getDateDiff2(new Date(brush0), new Date(brush1)));
//            $("#timeWindowSpan").html('from ' + moment(brush0).format("MMM Do") + ' to ' + moment(brush1).format("MMM Do"));
            $("#timeWindowSpan").attr("title", brushPopMSg);

            }

            //$("#timeWindowSpan").html( "from  "+moment(new Date(brush0)).format("DD-MM-YYYY HH:mm:ss")+"   to  "+moment(new Date(brush1)).format("DD-MM-YYYY HH:mm:ss"));

            dp1.data("DateTimePicker").date(moment(new Date(brush0))); //set dateTimePicker1 time
            dp2.data("DateTimePicker").date(moment(new Date(brush1))); //set dateTimePicker2 time
            remove_graphElements();
            resetBrush();
            d3.selectAll(".brush").remove();
            load_data("../data/" + config + "/" + probe + "/" + window_start + "/" + window_stop);
        }
        brush_status = 0;
    }


    function brushed() {
        brush0 = brush.extent()[0];
        brush1 = brush.extent()[1];
        brush_status = 1;
    }
    // brush tool to let us zoom and pan using the overview chart
    brush = d3.svg.brush()
        .x(x)
        .on("brush", brushed)
        .on("brushend", brushend);
    var color = ["lightblue", "lightgreen", "darkorange", "red"];
    var legend_colors = [
        ["inverter", "lightblue"],
        ["grid", "lightgreen"],
        ["generator", "darkorange"],
        ["error", "red"]
    ];


    var colour = d3.scale.ordinal()
        .range(color);


    function createAxis (xGrids,contextP,xAxisP,yAxis1P,axisYText,f,fx,brushAxis) {

     if (xGrids) {
       gridxP = contextP.append("g")
           .attr("class", "grid")
           .attr("transform", "translate(0," + height + ")")
           .call(fx()
               .tickSize(-height, 0, 0)
               .tickFormat("")
           );
          }

       gridyP = contextP.append("g")
           .attr("class", "grid")
           .call(make_y_axis("left")
               .tickSize(-f, 0, 0)
               .tickFormat("")
           );

       axisxP = contextP.append("g")
           .attr("class", "x axis")
           .attr("transform", "translate(0," + height + ")")
           .call(xAxisP);

       axisx_labelP = axisxP.append("text")
           .attr("x", 0)
           .attr("dx", "3.5em")
           .attr("dy", "3.5em")
           .style("text-anchor", "end")
           .attr("class", "axislabel")
           .text("Time GMT +01:00");

       axisyP = contextP.append("g")
           .attr("class", "y axis")
           .call(yAxis1P);
       axisy_labelP = axisyP.append("text")
           .attr("transform", "rotate(-90)")
           .attr("y", 0)
           .attr("dy", "2.5em")
           .style("text-anchor", "end")
           .attr("class", "axislabel")
           .text(axisYText);

       if (brushAxis) {
       contextP.append("g")
           .attr("class", "xc brush")
           .call(brush)
           .selectAll("rect")
           .attr("y", -6)
           .attr("height", height + 7);

         }

         var Axes = [];

         if (xGrids) {
         Axes.push(gridxP,gridyP,axisxP,axisyP,axisx_labelP,axisy_labelP);
         } else {
         Axes.push(gridyP,axisxP,axisyP,axisx_labelP,axisy_labelP);
         }


        return Axes;

   }

 function modifyData(dataObject) {
     var time;
     var counts;
     var i;
     var j;
     for (i = 0; i < dataObject.length; i++) {
         time = dataObject[i].time;
         counts = dataObject[i].counts;
         for (j = 0; j < counts.length; j++) {
             counts[j].time = time;
         }
     }
 }

   function drawDay (f,contextP,dataStore,svgP,yP,xP) {

             //Tip
                function tipCost(cost) {

                    var message = '<p style = \"margin-bottom:1px; \"> Cost : &#8358;' + f(cost.inverter+cost.generator+cost.grid) + '(';
                    if (cost.inverter !== 0) {
                        message += '<span style=\"color:#6666ff\"> ' + f(cost.inverter) + ' + </span>';
                    }
                    if (cost.generator !== 0) {
                        message += '<span style=\"color:darkorange\">  ' + f(cost.generator) + ' + </span>';
                    }
                    if (cost.grid !== 0) {
                        message += '<span style=\"color:green\">  ' + f(cost.grid) + '</span>';
                    }

                    message += ')</p>';

                    return message;
                }

                function tipEnergy(energy) {

                    var message = '<p> Energy : ' + powerGraphScale(energy.inverter + energy.generator + energy.grid) + 'h (';
                    if (energy.inverter !== 0) {
                        message += '<span style=\"color:#6666ff\">' + powerGraphScale(energy.inverter) + 'h  + </span>';
                    }
                    if (energy.generator !== 0) {
                        message += '<span style=\"color:darkorange\">  ' + powerGraphScale(energy.generator) + 'h  + </span>';
                    }
                    if (energy.grid !== 0) {
                        message += '<span style=\"color:green\">  ' + powerGraphScale(energy.grid) + 'h</span>';
                    }

                    message += ')</p>';

                    return message;
                }

             var barWidth = (width2  - margin.left - margin.right )/ new Date(date.getFullYear(), date.getMonth() + 1, 0).getDate();
             console.log("bar Width:"+ barWidth);

              modifyData (dataStore);

              textMonth = svgEnergyDay.append("text").attr("x", (0.45 * width2))
                          .attr("y",-2)
                          .attr("font-size","2em")
                          .attr('transform', 'translate(0,0)')
                          .attr("fill","#96989E")
                          .text(moment(date).format('MMMM YYYY'));

              var tip = d3.tip()
                  .attr('class', 'd3-tip')
                  .offset([-10, 0])
                  .html(function(d) {
                    return "<p style = \"margin-bottom:0; \" >"+ moment(d.time).format('D MMMM YYYY')+"</p>"+tipCost(d.cost) +tipEnergy(d.energy);
                  });

              svgP.call(tip);

              barsP = contextP.append("g")
                  .attr("class", "bars")
                  // a group for each stack of bars, positioned in the correct x position
                  .selectAll(".bar.stack")
                  .data(dataStore)
                  .enter().append("g")
                  .attr("class", "bar stack")
                  .attr("transform", function(d) {
                      return "translate(" + (xP(d.time)-(barWidth/2)) + ",0)";
                  })
                  // a bar for each value in the stack, positioned in the correct y positions
                  .selectAll("rect")
                  .data(function(d) {
                      return d.counts;

                  })
                  .enter().append("rect")
                  .attr("class", "bar")
                  .attr("width", barWidth)
                  .attr("y", function(d) {
                      return yP(d.y1);
                  })
                  .attr("height", function(d) {
                      return yP(d.y0) - yP(d.y1);
                  })
                  .on('mouseover', tip.show)
                  .on('mouseout', tip.hide)
                  .on( 'mouseup',tip.hide )
                  .style("fill", function(d) {
                      return colour(d.name);
                  });


              var legend = svgP.append("g")
                  .attr("class", "legend")
                  .attr("height", 100)
                  .attr("width", width)
                  .attr('transform', 'translate('+ (0.01* width2)+',40)');

              var legendRect = legend.selectAll('rect').data(legend_colors);

              legendRect.enter()
                  .append("rect")
                  .attr("y", height - 15)
                  .attr("width", 9)
                  .attr("height", 9);

              legendRect
                  .attr("x", function(d, i) {
                      return i * 100 + 400;
                  })
                  .style("fill", function(d) {
                      return d[1];
                  });

              var legendText = legend.selectAll('text')
                  .data(legend_colors);

              legendText.enter()
                  .append("text")
                  .attr("y", height + 10);

              legendText
                  .attr("x", function(d, i) {
                      return i * 100 + 400;
                  })
                  .style("font-size", "14px")
                  .style("fill", "white")
                  .text(function(d) {
                      return d[0];
                  });

              return barsP;

      }



    function bucket_handler(buckets, total, count, debug) {
        var count0 = {
            name: "inverter",
            y0: 0,
            y1: 0
        };
        var count1 = {
            name: "grid",
            y0: 0,
            y1: 0
        };
        var count2 = {
            name: "generator",
            y0: 0,
            y1: 0
        };
        var count3 = {
            name: "error",
            y0: 0,
            y1: 0
        };
        if (buckets == "error") {
            counts = [count0, count1, count2, count3];
            return counts;
        }
        var ratio = count;
        var next = 0;
        Object.keys(buckets).forEach(function(key) {
            ratio = buckets[key].count / count;
            if (buckets[key].source == "inverter") {
                count0.y0 = next;
                count0.y1 = next = count0.y0 + (total * ratio);
            }
            if (buckets[key].source == "grid") {
                count1.y0 = next;
                count1.y1 = next = count1.y0 + (total * ratio);
            }
            if (buckets[key].source == "generator") {
                count2.y0 = next;
                count2.y1 = next = count2.y0 + (total * ratio);
            }

            if (buckets[key].source == "zero") {
                count3.y0 = next;
                count3.y1 = next = count3.y0 + (total * ratio);
            }
            counts = [count0, count1, count2, count3];

        });
        if (debug > 0) {
            console.log("return", counts);
        }
        //
        return counts;
    }


         function bucket_handlerEnergyDay(buckets, total, count, debug) {
                var count0 = {
                    name: "inverter",
                    y0: 0,
                    y1: 0,
                    cost:0
                };
                var count1 = {
                    name: "grid",
                    y0: 0,
                    y1: 0,
                    cost:0
                };
                var count2 = {
                    name: "generator",
                    y0: 0,
                    y1: 0,
                    cost:0
                };
                var count3 = {
                    name: "error",
                    y0: 0,
                    y1: 0,
                    cost:0
                };
                if (buckets == "error") {
                    counts = [count0, count1, count2, count3];
                    return counts;
                }
                var cost = {
                    inverter: 0,
                    grid: 0,
                    generator: 0
                };
                  var energy = {
                      inverter: 0,
                      grid: 0,
                      generator: 0
                  };

                var ratio = count;
                var next = 0;
                Object.keys(buckets).forEach(function(key) {
                    ratio = buckets[key].count / count;
                    if (buckets[key].source == "inverter") {
                        count0.y0 = next;
                        count0.y1 = next = count0.y0 + buckets[key].energy;
                        cost.inverter = buckets[key].cost;
                        energy.inverter = buckets[key].energy;
                    }
                    if (buckets[key].source == "grid") {
                        count1.y0 = next;
                        count1.y1 = next = count1.y0 + buckets[key].energy;
                        cost.grid = buckets[key].cost;
                        energy.grid = buckets[key].energy;
                    }
                    if (buckets[key].source == "generator") {
                        count2.y0 = next;
                        count2.y1 = next = count2.y0 + buckets[key].energy;
                        cost.generator = buckets[key].cost;
                        energy.generator = buckets[key].energy;
                    }

                    if (buckets[key].source == "zero") {
                        count3.y0 = next;
                        count3.y1 = next = count3.y0 + buckets[key].energy;
                        count3.cost = buckets[key].cost;
                    }
                    counts = [count0, count1, count2, count3];

                });

                if (debug > 0) {
                    console.log("return", counts);
                }
                counts.forEach(function (d) { d.cost= cost;d.energy= energy;});

                //
                return counts;
            }



    var last;
    var bcount;
    var plotdata_recent;
    var elem = document.getElementById("realtime_stuff");
    /* var graph = document.getElementById("real");*/
    var voltage1Data = [];
    var voltage2Data = [];
    var voltage3Data = [];
    var current1Data = [];
    var current2Data = [];
    var current3Data = [];
    var pf1Data = [];
    var pf2Data = [];
    var pf3Data = [];
    var voltages = [];
    var currentData = [];
    var pfData = [];




    function eventHandler(e) {
        plotdata_recent.push({
            time: parseTimeISO(moment(e.detail.Time).format("YYYY-MM-DD HH:mm:ss")),
            total: e.detail.Power_real_total,
            apparent : e.detail.Power_apparent_total,
            reactive : e.detail.Power_reactive_total,
            sourceType: e.detail.SourceType
        });

            //voltage
                voltage1Data.push({
                    "time":  parseTimeISO(moment(e.detail.Time).format("YYYY-MM-DD HH:mm:ss")),
                    "voltage": e.detail.Voltage1,
                    "sourceType": e.detail.SourceType
                });
                voltage2Data.push({
                    "time":  parseTimeISO(moment(e.detail.Time).format("YYYY-MM-DD HH:mm:ss")),
                    "voltage": e.detail.Voltage2,
                    "sourceType": e.detail.SourceType
                });
                voltage3Data.push({
                    "time":  parseTimeISO(moment(e.detail.Time).format("YYYY-MM-DD HH:mm:ss")),
                    "voltage": e.detail.Voltage3,
                    "sourceType":  e.detail.SourceType,
                });

            //current
                current1Data.push({
                    "time":  parseTimeISO(moment(e.detail.Time).format("YYYY-MM-DD HH:mm:ss")),
                    "current":  e.detail.Current1,
                    "sourceType":  e.detail.SourceType
                });
                current2Data.push({
                    "time":  parseTimeISO(moment(e.detail.Time).format("YYYY-MM-DD HH:mm:ss")),
                    "current":  e.detail.Current2,
                    "sourceType":  e.detail.SourceType
                });
                current3Data.push({
                    "time":  parseTimeISO(moment(e.detail.Time).format("YYYY-MM-DD HH:mm:ss")),
                    "current":  e.detail.Current3,
                    "sourceType": e.detail.SourceType,

            //pf
                });
                pf1Data.push({
                    "time":  parseTimeISO(moment(e.detail.Time).format("YYYY-MM-DD HH:mm:ss")),
                    "pf": e.detail.PowerFactor1,
                    "sourceType":  e.detail.SourceType
                });
                pf2Data.push({
                    "time": parseTimeISO(moment(e.detail.Time).format("YYYY-MM-DD HH:mm:ss")),
                    "pf":  e.detail.PowerFactor2,
                    "sourceType":  e.detail.SourceType
                });
                pf3Data.push({
                    "time":  parseTimeISO(moment(e.detail.Time).format("YYYY-MM-DD HH:mm:ss")),
                    "pf": e.detail.PowerFactor3,
                    "sourceType": e.detail.SourceType,
                });


               /* currentData.push(current1Data,current2Data, current3Data);
                               pfData.push(pf1Data,pf2Data,pf3Data);
               */
               axislx
                   .transition()
                   //              .duration(500)
                   //              .ease("linear")
                   //.call(d3.svg.axis().scale(x).orient("bottom").tickFormat(d3.time.format("%H:%M:%S"))) // 24 hour format
                   .call(d3.svg.axis().scale(xl).orient("bottom"));

               realApparentElements[1]
                   .transition()
                   .call(d3.svg.axis().scale(xl).orient("bottom"));

               realReactiveElements[1]
                   .transition()
                   .call(d3.svg.axis().scale(xl).orient("bottom"));

               currentElements[1]
                   .transition()
                   .call(d3.svg.axis().scale(xcurrent).orient("bottom"));

               voltageElements[1]
                   .transition()
                   .call(d3.svg.axis().scale(xv).orient("bottom"));

               pfElements[1]
                   .transition()
                   .call(d3.svg.axis().scale(xv).orient("bottom"));


               axisly.transition().duration(1500).ease("sin-in-out")
                   .call(yAxis_realtime);

               realApparentElements[2].transition().duration(1500).ease("sin-in-out")
                   .call(yAxis_realtimeApparent);

               realReactiveElements[2].transition().duration(1500).ease("sin-in-out")
                   .call(yAxis_realtimeReactive);

               currentElements[2].transition().duration(1500).ease("sin-in-out")
                   .call(yAxis1Current);

               voltageElements[2].transition().duration(1500).ease("sin-in-out")
                   .call(yAxis1v);

               pfElements[2].transition().duration(1500).ease("sin-in-out")
                   .call(yAxis1pf);


               // pop the old data point off the front
               plotdata_recent.shift();
               voltage1Data.shift();
               voltage2Data.shift();
               voltage3Data.shift();
               current1Data.shift();
               current2Data.shift();
               current3Data.shift();
               pf1Data.shift();
               pf2Data.shift();
               pf3Data.shift();

               voltages = voltage1Data.concat(voltage2Data, voltage3Data);
               currentData = current1Data.concat(current2Data, current3Data);
               pfData = pf1Data.concat(pf2Data, pf3Data);

               xl.domain(d3.extent(plotdata_recent, function(d) {
                   return d.time;
               }));

               xv.domain(d3.extent(voltages, function(d) {
                   return d.time;
               }));

               xcurrent.domain(d3.extent(currentData, function(d) {
                   return d.time;
               }));

               yl.domain([0, d3.extent(plotdata_recent, function(d) {
                   return d.total;
               })[1]]);

               ylApparent.domain([0, d3.extent(plotdata_recent, function(d) {
                   return d.apparent;
               })[1]]);

               ylReactive.domain([0, d3.extent(plotdata_recent, function(d) {
                   return d.reactive;
               })[1]]);

               yv.domain([
                   d3.min(voltages, function(d) {
                       return d.voltage;
                   }),
                   d3.max(voltages, function(d) {
                       return d.voltage;
                   })
               ]);

               ycurrent.domain([0, d3.extent(currentData, function(d) {
                   return d.current;
               })[1]]);

               ypf.domain([0, d3.extent(pfData, function(d) {
                   return d.pf;
               })[1]]);


        // transition the line
        pathReal[0].transition().attr("d", line);
        pathReal[1].transition().attr("d", areagen);
        pathReal[2].transition().attr("d", areagrd);
        pathReal[3].transition().attr("d", areainv);

        //yl.domain(d3.extent(plotdata_recent, function(d) { return d.total; }));

        pathApparent[0].transition().attr("d", lineRealApparent);
        pathApparent[1].transition().attr("d", areagenApparent);
        pathApparent[2].transition().attr("d", areagrdApparent);
        pathApparent[3].transition().attr("d", areainvApparent);

        pathReactive[0].transition().attr("d", lineRealReactive);
        pathReactive[1].transition().attr("d", areagenReactive);
        pathReactive[2].transition().attr("d", areagrdReactive);
        pathReactive[3].transition().attr("d", areainvReactive);

       linedVoltage[0].transition().attr("d", lineVoltage);
       linedVoltage[1].transition().attr("d", lineVoltage);
       linedVoltage[2].transition().attr("d", lineVoltage);

       linedCurrent[0].transition().attr("d", lineCurrent);
       linedCurrent[1].transition().attr("d", lineCurrent);
       linedCurrent[2].transition().attr("d", lineCurrent);

       linedpf[0].transition().attr("d", linepf);
       linedpf[1].transition().attr("d", linepf);
       linedpf[2].transition().attr("d", linepf);

    }
    // Listen for the event.
    elem.addEventListener('build', function(e) {
        eventHandler(e);
    }, false);
    /* graph.addEventListener('build', function (e) { eventHandler(e); }, false);*/

    function load_recent(URL) {
        d3.json(URL, function(error, json) {
            if (error) return console.warn(error);
            plotdata_recent = json.hits.hits;
            PhaseData = json.hits.hits;


            plotdata_recent.forEach(function(d) {
                d.time = parseTimeISO(moment(d._source.Time).format("YYYY-MM-DD HH:mm:ss"));
                d.total = d._source.Power_real_total;
                d.apparent = d._source.Power_apparent_total;
                d.reactive = d._source.Power_reactive_total;
                d.sourceType = d._source.SourceType;
                d.sourceName = d._source.SourceName;
            });
            xl.domain(d3.extent(plotdata_recent, function(d) {
                return d.time;
            }));
            yl.domain([0, d3.extent(plotdata_recent, function(d) {
                return d.total;
            })[1]]);
            // yl.domain(d3.extent(plotdata_recent, function(d) { return d.total; }));

            ylApparent.domain([0, d3.extent(plotdata_recent, function(d) {
                return d.apparent;
            })[1]]);

            ylReactive.domain([0, d3.extent(plotdata_recent, function(d) {
                return d.reactive;
            })[1]]);

            gridlx = focus.append("g")
                .attr("class", "grid")
                .attr("transform", "translate(0," + height + ")")
                .call(make_x_axis()
                    .tickSize(-height, 0, 0)
                    .tickFormat("")
                );

            gridly = focus.append("g")
                .attr("class", "grid")
                .call(make_y_axis("left")
                    .tickSize(-width, 0, 0)
                    .tickFormat("")
                );

            axislx = focus.append("g")
                .attr("class", "x axis")
                .attr("transform", "translate(0," + height + ")")
                .call(xAxis_realtime);

            axislx_label = axislx.append("text")
                .attr("x", 0)
                .attr("dx", "3.5em")
                .attr("dy", "3.5em")
                .style("text-anchor", "end")
                .attr("class", "axislabel")
                .text("Time GMT +01:00");

            axisly = focus.append("g")
                .attr("class", "y axis")
                .call(yAxis_realtime);

            axisly_label = axisly.append("text")
                .attr("transform", "rotate(-90)")
                .attr("y", 0)
                .attr("dy", "1.5em")
                .style("text-anchor", "end")
                .attr("class", "axislabel")
                .text("Power (W)");

            realApparentElements = createAxis (0,focusApparent,xAxis_realtime,yAxis_realtimeApparent,'Power (VA)',width,make_x_axis,0);
            realReactiveElements = createAxis (0,focusReactive,xAxis_realtime,yAxis_realtimeReactive,'Power (VAR)',width,make_x_axis,0);

            /*pathgen = focus.append("path")
                .datum(plotdata_recent)
                .attr("class", "areagen")
                .attr("d", areagen)
                .attr("data-legend", function() {
                    return 'generator';
                });
            pathgrid = focus.append("path")
                .datum(plotdata_recent)
                .attr("class", "areagrd")
                .attr("d", areagrd)
                .attr("data-legend", function() {
                    return 'grid';
                });
            pathinv = focus.append("path")
                .datum(plotdata_recent)
                .attr("class", "areainv")
                .attr("d", areainv)
                .attr("data-legend", function() {
                    return 'inverter';
                });
            path1 = focus.append("path")
                .datum(plotdata_recent)
                .attr("class", "line")
                .attr("d", line);*/

                function  drawPath (focus_,rline_,gen_,grd_,inv_) {

                    pathgen = focus_.append("path")
                        .datum(plotdata_recent)
                        .attr("class", "areagen")
                        .attr("d", gen_)
                        .attr("data-legend", function() {
                            return 'generator';
                        });
                    pathgrid = focus_.append("path")
                        .datum(plotdata_recent)
                        .attr("class", "areagrd")
                        .attr("d", grd_)
                        .attr("data-legend", function() {
                            return 'grid';
                        });
                    pathinv = focus_.append("path")
                        .datum(plotdata_recent)
                        .attr("class", "areainv")
                        .attr("d", inv_)
                        .attr("data-legend", function() {
                            return 'inverter';
                        });
                    path1 = focus_.append("path")
                        .datum(plotdata_recent)
                        .attr("class", "line")
                        .attr("d", rline_);


                        var path = [path1,pathgen,pathgrid,pathinv];
                        return path;
                        }
            pathReal = drawPath (focus,line,areagen,areagrd,areainv);
            pathApparent =  drawPath (focusApparent,lineRealApparent,areagenApparent,areagrdApparent,areainvApparent);
            pathReactive = drawPath (focusReactive,lineRealReactive,areagenReactive,areagrdReactive,areainvReactive);

            /*legend = focus.append("g")
                .attr("class", "legend")
                .attr("transform", "translate(50,30)")
                .style("font-size", "12px")
                .call(d3.legend);*/
           voltage1Data = [];
           voltage2Data = [];
           voltage3Data = [];
           current1Data = [];
           current2Data = [];
           current3Data = [];
           pf1Data =[];
           pf2Data = [];
           pf3Data = [];

            PhaseData.forEach(function(d) {
            //voltage
                voltage1Data.push({
                    "time": parseTimeISO(moment(d._source.Time).format("YYYY-MM-DD HH:mm:ss")),
                    "voltage": d._source.Voltage1,
                    "sourceType": d._source.SourceType
                });
                voltage2Data.push({
                    "time": parseTimeISO(moment(d._source.Time).format("YYYY-MM-DD HH:mm:ss")),
                    "voltage": d._source.Voltage2,
                    "sourceType": d._source.SourceType
                });
                voltage3Data.push({
                    "time": parseTimeISO(moment(d._source.Time).format("YYYY-MM-DD HH:mm:ss")),
                    "voltage": d._source.Voltage3,
                    "sourceType": d._source.SourceType,
                });

            //current
                current1Data.push({
                    "time": parseTimeISO(moment(d._source.Time).format("YYYY-MM-DD HH:mm:ss")),
                    "current": d._source.Current1,
                    "sourceType": d._source.SourceType
                });
                current2Data.push({
                    "time": parseTimeISO(moment(d._source.Time).format("YYYY-MM-DD HH:mm:ss")),
                    "current": d._source.Current2,
                    "sourceType": d._source.SourceType
                });
                current3Data.push({
                    "time": parseTimeISO(moment(d._source.Time).format("YYYY-MM-DD HH:mm:ss")),
                    "current": d._source.Current3,
                    "sourceType": d._source.SourceType,

            //pf
                });
                pf1Data.push({
                    "time": parseTimeISO(moment(d._source.Time).format("YYYY-MM-DD HH:mm:ss")),
                    "pf": d._source.PowerFactor1,
                    "sourceType": d._source.SourceType
                });
                pf2Data.push({
                    "time": parseTimeISO(moment(d._source.Time).format("YYYY-MM-DD HH:mm:ss")),
                    "pf": d._source.PowerFactor2,
                    "sourceType": d._source.SourceType
                });
                pf3Data.push({
                    "time": parseTimeISO(moment(d._source.Time).format("YYYY-MM-DD HH:mm:ss")),
                    "pf": d._source.PowerFactor3,
                    "sourceType": d._source.SourceType,
                });
            });
            voltages = voltage1Data.concat(voltage2Data, voltage3Data);
            currentData = current1Data.concat(current2Data, current3Data);
            pfData = pf1Data.concat(pf2Data,pf3Data);

            var phaseColors = [
                ["phase1", "red"],
                ["phase2", "yellow"],
                ["phase3", "blue"]
            ];


            //Voltage drawing
            xv.domain(d3.extent(voltages, function(d) {
                return d.time;
            }));

            yv.domain([
                d3.min(voltages, function(d) {
                    return d.voltage;
                }),
                d3.max(voltages, function(d) {
                    return d.voltage;
                })
            ]);


            ypf.domain([
                d3.min(pfData, function(d) {
                    return d.pf;
                }),
                d3.max(pfData, function(d) {
                    return d.pf;
                })
            ]);

            xcurrent.domain(d3.extent(currentData, function(d) {
                            return d.time;
                        }));

            ycurrent.domain([
                d3.min(currentData, function(d) {
                    return d.current;
                }),
                d3.max(currentData, function(d) {
                    return d.current;
                })
            ]);

            voltageElements = createAxis (0,contextVoltage,xAxisv,yAxis1v,'Voltage (V)',width2,make_x_axis,0);
            currentElements = createAxis (0,contextCurrent,xAxiscurrent,yAxis1Current,'Current (A)',width2,make_x_axis,0);
            pfElements = createAxis (0,contextpf,xAxisv,yAxis1pf,' ',width2,make_x_axis,0);


            linedVoltage[0] = contextVoltage.append('path')
                            .datum(voltage1Data)
                            .attr("d", lineVoltage)
                            .attr('stroke', 'red')
                            .attr('stroke-width', 2)
                            .attr('fill', 'none');

                /*.attr('d', lineVoltage(voltage1Data))
                .attr('stroke', 'red')
                .attr('stroke-width', 2)
                .attr('fill', 'none');*/

            linedVoltage[1]=contextVoltage.append('path')
                           .datum(voltage2Data)
                           .attr("d", lineVoltage)
                           .attr('stroke', 'yellow')
                           .attr('stroke-width', 2)
                           .attr('fill', 'none');


            linedVoltage[2]= contextVoltage.append('path')
                            .datum(voltage3Data)
                            .attr("d", lineVoltage)
                            .attr('stroke', 'blue')
                            .attr('stroke-width', 2)
                            .attr('fill', 'none');

            //Current Drawing

            linedCurrent[0] = contextCurrent .append('path')
                              .datum(current1Data)
                              .attr("d", lineCurrent)
                              .attr('stroke', 'red')
                              .attr('stroke-width', 2)
                              .attr('fill', 'none');

            linedCurrent[1] = contextCurrent.append('path')
                             .datum(current2Data)
                             .attr("d", lineCurrent)
                             .attr('stroke', 'yellow')
                             .attr('stroke-width', 2)
                             .attr('fill', 'none');

            linedCurrent[2] = contextCurrent.append('path')
                             .datum(current3Data)
                             .attr("d", lineCurrent)
                             .attr('stroke', 'blue')
                             .attr('stroke-width', 2)
                             .attr('fill', 'none');

            //pf Drawing
            linedpf[0] = contextpf.append('path')
                            .datum(pf1Data)
                            .attr("d", linepf)
                            .attr('stroke', 'red')
                            .attr('stroke-width', 2)
                            .attr('fill', 'none');

            linedpf[1] = contextpf.append('path')
                           .datum(pf2Data)
                           .attr("d", linepf)
                           .attr('stroke', 'yellow')
                           .attr('stroke-width', 2)
                           .attr('fill', 'none');

            linedpf[2] = contextpf.append('path')
                           .datum(pf3Data)
                           .attr("d", linepf)
                           .attr('stroke', 'blue')
                           .attr('stroke-width', 2)
                           .attr('fill', 'none');

        phaseLegend(svgCurrent);
        phaseLegend(svgVoltage);
        phaseLegend(svgpf);


        function phaseLegend (svgPhase) {

            var phaselegend = svgPhase.append("g")
                .attr("class", "legend")
                .attr("height", 100)
                .attr("width", 100)
                .attr('transform', 'translate(-100,50)');

            var legendRectPhase = phaselegend.selectAll('rect').data(phaseColors);

            legendRectPhase.enter()
                .append("rect")
                .attr("y", height - 15)
                .attr("width", 15)
                .attr("height", 15);

            legendRectPhase
                .attr("x", function(d, i) {
                    return i * 100 + 400;
                })
                .style("fill", function(d) {
                    return d[1];
                });

            var legendTextPhase = phaselegend.selectAll('text')
                .data(phaseColors);

            legendTextPhase.enter()
                .append("text")
                .attr("y", height + 15);

            legendTextPhase
                .attr("x", function(d, i) {
                    return i * 100 + 400;
                })
                .style("font-size", "14px")
                .style("fill", "white")
                .text(function(d) {
                    return d[0];
                });

        }

        });
    }

     function load_monthly(URL) {
            d3.json(URL, function(error, json) {
                if (error) return console.warn(error);

              EnergyByDay = json.aggregations.EnergyDay.buckets;

              EnergyByDay.forEach(function(d) {
                  d.time = last = parseTimeISO(moment(d.key).format("YYYY-MM-DD HH:mm:ss"));
                  d.total = d.energy.sum;
                  d.buckets = d.SourceType.buckets;

                  if (d.buckets.length === 0) {
                      console.info("no buckets");
                      d.src = "error";
                      d.counts = bucket_handler(d.src, d.total, 0, 0);
                  } else if (d.buckets.length > 5) {
                      console.error("too many buckets");
                      console.error(d.buckets);
                      d.src = "error";
                  } else {
                      if (d.buckets.length == 1) {
                          src0 = {
                              "source": d.buckets[0].key,
                              "count": d.buckets[0].doc_count,
                              "cost" : d.buckets[0].CostStats.sum,
                              "energy" : d.buckets[0].energyStats.sum
                          };
                          d.src = {
                              "0": src0
                          };
                          bcount = src0.count;
                      } else if (d.buckets.length == 2) {
                          src0 = {
                              "source": d.buckets[0].key,
                              "count": d.buckets[0].doc_count,
                              "cost" : d.buckets[0].CostStats.sum,
                              "energy" : d.buckets[0].energyStats.sum
                          };
                          src1 = {
                              "source": d.buckets[1].key,
                              "count": d.buckets[1].doc_count,
                              "cost" : d.buckets[1].CostStats.sum,
                              "energy" : d.buckets[1].energyStats.sum
                          };
                          bcount = src0.count + src1.count;
                          d.src = {
                              "0": src0,
                              "1": src1
                          };
                          //d.counts = bucket_handler(source,d.total,bcount);
                      } else if (d.buckets.length == 3) {
                          src0 = {
                              "source": d.buckets[0].key,
                              "count": d.buckets[0].doc_count,
                              "cost" : d.buckets[0].CostStats.sum,
                              "energy" : d.buckets[0].energyStats.sum
                          };
                          src1 = {
                              "source": d.buckets[1].key,
                              "count": d.buckets[1].doc_count,
                              "cost" : d.buckets[1].CostStats.sum,
                              "energy" : d.buckets[1].energyStats.sum
                          };
                          src2 = {
                              "source": d.buckets[2].key,
                              "count": d.buckets[2].doc_count,
                              "cost" : d.buckets[2].CostStats.sum,
                              "energy" : d.buckets[2].energyStats.sum
                          };
                          d.src = {
                              "0": src0,
                              "1": src1,
                              "2": src2
                          };
                          bcount = src0.count + src1.count + src2.count;
                          //d.counts = bucket_handler(source,d.total,bcount);;
                      } else if (d.buckets.length == 4) {
                          src0 = {
                              "source": d.buckets[0].key,
                              "count": d.buckets[0].doc_count,
                              "cost" : d.buckets[0].CostStats.sum,
                              "energy" : d.buckets[0].energyStats.sum
                          };
                          src1 = {
                              "source": d.buckets[1].key,
                              "count": d.buckets[1].doc_count,
                              "cost" : d.buckets[1].CostStats.sum,
                              "energy" : d.buckets[1].energyStats.sum
                          };
                          src2 = {
                              "source": d.buckets[2].key,
                              "count": d.buckets[2].doc_count,
                              "cost" : d.buckets[2].CostStats.sum,
                              "energy" : d.buckets[2].energyStats.sum
                          };
                          src3 = {
                              "source": d.buckets[3].key,
                              "count": d.buckets[3].doc_count,
                              "cost" : d.buckets[3].CostStats.sum,
                              "energy" : d.buckets[3].energyStats.sum
                          };
                          d.src = {
                              "0": src0,
                              "1": src1,
                              "2": src2,
                              "3": src3
                          };
                          bcount = src0.count + src1.count + src2.count + src3.count;
                          //d.counts = bucket_handler(source,d.total,bcount);
                      } else if (d.buckets.length == 5) {
                          src0 = {
                              "source": d.buckets[0].key,
                              "count": d.buckets[0].doc_count,
                              "cost" : d.buckets[0].CostStats.sum,
                              "energy" : d.buckets[0].energyStats.sum
                          };
                          src1 = {
                              "source": d.buckets[1].key,
                              "count": d.buckets[1].doc_count,
                              "cost" : d.buckets[1].CostStats.sum,
                              "energy" : d.buckets[1].energyStats.sum
                          };
                          src2 = {
                              "source": d.buckets[2].key,
                              "count": d.buckets[2].doc_count,
                              "cost" : d.buckets[2].CostStats.sum,
                              "energy" : d.buckets[2].energyStats.sum
                          };
                          src3 = {
                              "source": d.buckets[3].key,
                              "count": d.buckets[3].doc_count,
                              "cost" : d.buckets[3].CostStats.sum,
                               "energy" : d.buckets[3].energyStats.sum

                          };
                          src4 = {
                              "source": d.buckets[4].key,
                              "count": d.buckets[4].doc_count,
                              "cost" : d.buckets[4].CostStats.sum,
                              "energy" : d.buckets[4].energyStats.sum
                          };
                          d.src = {
                              "0": src0,
                              "1": src1,
                              "2": src2,
                              "3": src3,
                              "4": src4
                          };
                          bcount = src0.count + src1.count + src2.count + src3.count + src4.count;
                          //d.counts = bucket_handler(source,d.total,bcount);
                      }

                      d.counts = bucket_handlerEnergyDay(d.src, d.total, bcount, 0);
                  }

              });

                xEnergyDay.domain([d3.time.hour.offset(new Date(date.getFullYear(), date.getMonth(), 1),-23),d3.time.hour.offset (new Date(date.getFullYear(), date.getMonth() + 1, 0),23)]);

                yEnergyDay.domain([0, d3.extent(EnergyByDay, function(d) {
                    return d.total;
                })[1]]);

                EnergyByDayElements = createAxis (0,contextEnergyDay,xAxisEnergyDay,yAxis1EnergyDay,'Energy (Wh)',width2,make_x_axis,0); // Battery DOD


                bars_energyDay = drawDay (roundTo,contextEnergyDay,EnergyByDay,svgEnergyDay,yEnergyDay,xEnergyDay);
            });
        }




    function load_data(URL) {
    var target = document.getElementById('graph');
    var targetEnergy = document.getElementById('energy_graph');
    var targetCost = document.getElementById('cost_graph');

    var spinner = new Spinner(opts).spin(target);
    var spinnerEnergy = new Spinner(opts).spin(targetEnergy);
    var spinnerCost = new Spinner(opts).spin(targetCost);
        d3.json(URL, function(error, json) {
            if (error) return console.warn(error);

            plotdata = json.aggregations.power.buckets;
            reactivedata = json.aggregations.powerReactive.buckets;
            apparentdata = json.aggregations.powerApparent.buckets;
            costdata = json.aggregations.Cost.buckets;
            energydata = json.aggregations.Energy.buckets;
            fueldata = json.aggregations.fuel.buckets;
            fuelRatedata = json.aggregations.fuelRate.buckets;
            batteryCapData = json.aggregations.batteryCap.buckets;
            batteryDOD = json.aggregations.batteryDOD.buckets;
            EnergyByDay = json.aggregations.EnergyDay.buckets;

            var t1 = 0;
            var P1 = [];
            var SrcName_Energy_Total = [];
            var SrcName_P1 = [];
            var Energy_total = [];
            var source_types = [];
            var source_names = [];
            var SrcName_energy_data = [];
            var energy_data = [];
            //                var time_data = [];
            var typeColor = ["inverter", "grid", "generator"];
            var SrcByType = [];
            var SrcNameByType = [];
            var typeTime = json.aggregations.timingType.buckets;
            var nameTime = json.aggregations.timingName.buckets;
            var costName = json.aggregations.costName.buckets;
            var costType = json.aggregations.costType.buckets;
            var powerName = json.aggregations.powerName.buckets;
            var powerType = json.aggregations.powerType.buckets;


           function prepareData (data) {
           data.forEach(function(d) {
                 if (d.buckets.length === 0) {
                     console.info("no buckets");
                     d.src = "error";
                     d.counts = bucket_handler(d.src, d.total, 0, 0);
                 } else if (d.buckets.length > 5) {
                     console.error("too many buckets");
                     console.error(d.buckets);
                     d.src = "error";
                 } else {
                     if (d.buckets.length == 1) {
                         src0 = {
                             "source": d.buckets[0].key,
                             "count": d.buckets[0].doc_count
                         };
                         d.src = {
                             "0": src0
                         };
                         bcount = src0.count;
                     } else if (d.buckets.length == 2) {
                         src0 = {
                             "source": d.buckets[0].key,
                             "count": d.buckets[0].doc_count
                         };
                         src1 = {
                             "source": d.buckets[1].key,
                             "count": d.buckets[1].doc_count
                         };
                         bcount = src0.count + src1.count;
                         d.src = {
                             "0": src0,
                             "1": src1
                         };
                         //d.counts = bucket_handler(source,d.total,bcount);
                     } else if (d.buckets.length == 3) {
                         src0 = {
                             "source": d.buckets[0].key,
                             "count": d.buckets[0].doc_count
                         };
                         src1 = {
                             "source": d.buckets[1].key,
                             "count": d.buckets[1].doc_count
                         };
                         src2 = {
                             "source": d.buckets[2].key,
                             "count": d.buckets[2].doc_count
                         };
                         d.src = {
                             "0": src0,
                             "1": src1,
                             "2": src2
                         };
                         bcount = src0.count + src1.count + src2.count;
                         //d.counts = bucket_handler(source,d.total,bcount);;
                     } else if (d.buckets.length == 4) {
                         src0 = {
                             "source": d.buckets[0].key,
                             "count": d.buckets[0].doc_count
                         };
                         src1 = {
                             "source": d.buckets[1].key,
                             "count": d.buckets[1].doc_count
                         };
                         src2 = {
                             "source": d.buckets[2].key,
                             "count": d.buckets[2].doc_count
                         };
                         src3 = {
                             "source": d.buckets[3].key,
                             "count": d.buckets[3].doc_count
                         };
                         d.src = {
                             "0": src0,
                             "1": src1,
                             "2": src2,
                             "3": src3
                         };
                         bcount = src0.count + src1.count + src2.count + src3.count;
                         //d.counts = bucket_handler(source,d.total,bcount);
                     } else if (d.buckets.length == 5) {
                         src0 = {
                             "source": d.buckets[0].key,
                             "count": d.buckets[0].doc_count
                         };
                         src1 = {
                             "source": d.buckets[1].key,
                             "count": d.buckets[1].doc_count
                         };
                         src2 = {
                             "source": d.buckets[2].key,
                             "count": d.buckets[2].doc_count
                         };
                         src3 = {
                             "source": d.buckets[3].key,
                             "count": d.buckets[3].doc_count
                         };
                         src4 = {
                             "source": d.buckets[4].key,
                             "count": d.buckets[4].doc_count
                         };
                         d.src = {
                             "0": src0,
                             "1": src1,
                             "2": src2,
                             "3": src3,
                             "4": src4
                         };
                         bcount = src0.count + src1.count + src2.count + src3.count + src4.count;
                         //d.counts = bucket_handler(source,d.total,bcount);
                     }

                     d.counts = bucket_handler(d.src, d.total, bcount, 0);
                 }
             });
           }

            plotdata.forEach(function(d) {
                d.time = last = parseTimeISO(moment(d.key).format("YYYY-MM-DD HH:mm:ss"));
                d.total = d.Power_real_total.avg;
                d.buckets = d.SourceType.buckets;
                if (d.buckets.length === 0) {
                    console.info("no buckets");
                    d.src = "error";
                    d.counts = bucket_handler(d.src, d.total, 0, 0);
                } else if (d.buckets.length > 5) {
                    console.error("too many buckets");
                    console.error(d.buckets);
                    d.src = "error";
                } else {
                    if (d.buckets.length == 1) {
                        src0 = {
                            "source": d.buckets[0].key,
                            "count": d.buckets[0].doc_count
                        };
                        d.src = {
                            "0": src0
                        };
                        bcount = src0.count;
                    } else if (d.buckets.length == 2) {
                        src0 = {
                            "source": d.buckets[0].key,
                            "count": d.buckets[0].doc_count
                        };
                        src1 = {
                            "source": d.buckets[1].key,
                            "count": d.buckets[1].doc_count
                        };
                        bcount = src0.count + src1.count;
                        d.src = {
                            "0": src0,
                            "1": src1
                        };
                        //d.counts = bucket_handler(source,d.total,bcount);
                    } else if (d.buckets.length == 3) {
                        src0 = {
                            "source": d.buckets[0].key,
                            "count": d.buckets[0].doc_count
                        };
                        src1 = {
                            "source": d.buckets[1].key,
                            "count": d.buckets[1].doc_count
                        };
                        src2 = {
                            "source": d.buckets[2].key,
                            "count": d.buckets[2].doc_count
                        };
                        d.src = {
                            "0": src0,
                            "1": src1,
                            "2": src2
                        };
                        bcount = src0.count + src1.count + src2.count;
                        //d.counts = bucket_handler(source,d.total,bcount);;
                    } else if (d.buckets.length == 4) {
                        src0 = {
                            "source": d.buckets[0].key,
                            "count": d.buckets[0].doc_count
                        };
                        src1 = {
                            "source": d.buckets[1].key,
                            "count": d.buckets[1].doc_count
                        };
                        src2 = {
                            "source": d.buckets[2].key,
                            "count": d.buckets[2].doc_count
                        };
                        src3 = {
                            "source": d.buckets[3].key,
                            "count": d.buckets[3].doc_count
                        };
                        d.src = {
                            "0": src0,
                            "1": src1,
                            "2": src2,
                            "3": src3
                        };
                        bcount = src0.count + src1.count + src2.count + src3.count;
                        //d.counts = bucket_handler(source,d.total,bcount);
                    } else if (d.buckets.length == 5) {
                        src0 = {
                            "source": d.buckets[0].key,
                            "count": d.buckets[0].doc_count
                        };
                        src1 = {
                            "source": d.buckets[1].key,
                            "count": d.buckets[1].doc_count
                        };
                        src2 = {
                            "source": d.buckets[2].key,
                            "count": d.buckets[2].doc_count
                        };
                        src3 = {
                            "source": d.buckets[3].key,
                            "count": d.buckets[3].doc_count
                        };
                        src4 = {
                            "source": d.buckets[4].key,
                            "count": d.buckets[4].doc_count
                        };
                        d.src = {
                            "0": src0,
                            "1": src1,
                            "2": src2,
                            "3": src3,
                            "4": src4
                        };
                        bcount = src0.count + src1.count + src2.count + src3.count + src4.count;
                        //d.counts = bucket_handler(source,d.total,bcount);
                    }

                    d.counts = bucket_handler(d.src, d.total, bcount, 0);
                }

                if (d.buckets.length > 0) {
                    var output = getEnergyData(d, t1, P1, SrcName_Energy_Total, SrcName_P1, Energy_total, source_types, source_names, SrcName_energy_data, energy_data, typeColor, SrcByType, SrcNameByType, typeTime, nameTime);

                    t1 = output.t1;
                    P1 = output.P1;
                    SrcName_Energy_Total = output.SrcName_Energy_Total;
                    SrcName_P1 = output.SrcName_P1;
                    Energy_total = output.Energy_total;
                    source_types = output.source_types;
                    source_names = output.source_names;
                    SrcName_energy_data = output.SrcName_energy_data;
                    energy_data = output.energy_data;
                    typeColor = output.typeColor;
                    SrcByType = output.SrcByType;
                    SrcNameByType = output.SrcNameByType;
                    typeTime = output.typeTime;
                    nameTime = output.nameTime;
                }
            });

             reactivedata.forEach(function(d) {
                d.time = last = parseTimeISO(moment(d.key).format("YYYY-MM-DD HH:mm:ss"));
                d.total = d.reactivePower.avg;
                d.buckets = d.SourceType.buckets;
            });

            apparentdata.forEach(function(d) {
                d.time = last = parseTimeISO(moment(d.key).format("YYYY-MM-DD HH:mm:ss"));
                d.total = d.apparentPower.avg;
                d.buckets = d.SourceType.buckets;
            });

            //Cost Data
            costdata.forEach(function(d) {
                d.time = last = parseTimeISO(moment(d.key).format("YYYY-MM-DD HH:mm:ss"));
                d.total = d.cost.sum;
                d.buckets = d.SourceType.buckets;
            });

            energydata.forEach(function(d) {
                d.time = last = parseTimeISO(moment(d.key).format("YYYY-MM-DD HH:mm:ss"));
                d.total = d.energy.sum;
                d.buckets = d.SourceType.buckets;
            });


        EnergyByDay.forEach(function(d) {
            d.time = last = parseTimeISO(moment(d.key).format("YYYY-MM-DD HH:mm:ss"));
            d.total = d.energy.sum;
            d.buckets = d.SourceType.buckets;
            d.buckets = d.SourceType.buckets;
            if (d.buckets.length === 0) {
                console.info("no buckets");
                d.src = "error";
                d.counts = bucket_handler(d.src, d.total, 0, 0);
            } else if (d.buckets.length > 5) {
                console.error("too many buckets");
                console.error(d.buckets);
                d.src = "error";
            } else {
                if (d.buckets.length == 1) {
                    src0 = {
                        "source": d.buckets[0].key,
                        "count": d.buckets[0].doc_count,
                        "cost" : d.buckets[0].CostStats.sum,
                        "energy" : d.buckets[0].energyStats.sum
                    };
                    d.src = {
                        "0": src0
                    };
                    bcount = src0.count;
                } else if (d.buckets.length == 2) {
                    src0 = {
                        "source": d.buckets[0].key,
                        "count": d.buckets[0].doc_count,
                        "cost" : d.buckets[0].CostStats.sum,
                        "energy" : d.buckets[0].energyStats.sum
                    };
                    src1 = {
                        "source": d.buckets[1].key,
                        "count": d.buckets[1].doc_count,
                        "cost" : d.buckets[1].CostStats.sum,
                        "energy" : d.buckets[1].energyStats.sum
                    };
                    bcount = src0.count + src1.count;
                    d.src = {
                        "0": src0,
                        "1": src1
                    };
                    //d.counts = bucket_handler(source,d.total,bcount);
                } else if (d.buckets.length == 3) {
                    src0 = {
                        "source": d.buckets[0].key,
                        "count": d.buckets[0].doc_count,
                        "cost" : d.buckets[0].CostStats.sum,
                        "energy" : d.buckets[0].energyStats.sum
                    };
                    src1 = {
                        "source": d.buckets[1].key,
                        "count": d.buckets[1].doc_count,
                        "cost" : d.buckets[1].CostStats.sum,
                        "energy" : d.buckets[1].energyStats.sum
                    };
                    src2 = {
                        "source": d.buckets[2].key,
                        "count": d.buckets[2].doc_count,
                        "cost" : d.buckets[2].CostStats.sum,
                        "energy" : d.buckets[2].energyStats.sum
                    };
                    d.src = {
                        "0": src0,
                        "1": src1,
                        "2": src2
                    };
                    bcount = src0.count + src1.count + src2.count;
                    //d.counts = bucket_handler(source,d.total,bcount);;
                } else if (d.buckets.length == 4) {
                    src0 = {
                        "source": d.buckets[0].key,
                        "count": d.buckets[0].doc_count,
                        "cost" : d.buckets[0].CostStats.sum,
                        "energy" : d.buckets[0].energyStats.sum
                    };
                    src1 = {
                        "source": d.buckets[1].key,
                        "count": d.buckets[1].doc_count,
                        "cost" : d.buckets[1].CostStats.sum,
                        "energy" : d.buckets[1].energyStats.sum
                    };
                    src2 = {
                        "source": d.buckets[2].key,
                        "count": d.buckets[2].doc_count,
                        "cost" : d.buckets[2].CostStats.sum,
                        "energy" : d.buckets[2].energyStats.sum
                    };
                    src3 = {
                        "source": d.buckets[3].key,
                        "count": d.buckets[3].doc_count,
                        "cost" : d.buckets[3].CostStats.sum,
                        "energy" : d.buckets[3].energyStats.sum
                    };
                    d.src = {
                        "0": src0,
                        "1": src1,
                        "2": src2,
                        "3": src3
                    };
                    bcount = src0.count + src1.count + src2.count + src3.count;
                    //d.counts = bucket_handler(source,d.total,bcount);
                } else if (d.buckets.length == 5) {
                    src0 = {
                        "source": d.buckets[0].key,
                        "count": d.buckets[0].doc_count,
                        "cost" : d.buckets[0].CostStats.sum,
                        "energy" : d.buckets[0].energyStats.sum
                    };
                    src1 = {
                        "source": d.buckets[1].key,
                        "count": d.buckets[1].doc_count,
                        "cost" : d.buckets[1].CostStats.sum,
                        "energy" : d.buckets[1].energyStats.sum
                    };
                    src2 = {
                        "source": d.buckets[2].key,
                        "count": d.buckets[2].doc_count,
                        "cost" : d.buckets[2].CostStats.sum,
                        "energy" : d.buckets[2].energyStats.sum
                    };
                    src3 = {
                        "source": d.buckets[3].key,
                        "count": d.buckets[3].doc_count,
                        "cost" : d.buckets[3].CostStats.sum,
                         "energy" : d.buckets[3].energyStats.sum

                    };
                    src4 = {
                        "source": d.buckets[4].key,
                        "count": d.buckets[4].doc_count,
                        "cost" : d.buckets[4].CostStats.sum,
                        "energy" : d.buckets[4].energyStats.sum
                    };
                    d.src = {
                        "0": src0,
                        "1": src1,
                        "2": src2,
                        "3": src3,
                        "4": src4
                    };
                    bcount = src0.count + src1.count + src2.count + src3.count + src4.count;
                    //d.counts = bucket_handler(source,d.total,bcount);
                }

                d.counts = bucket_handlerEnergyDay(d.src, d.total, bcount, 0);
            }

        });


            //Prepare data
            prepareData(reactivedata);
            prepareData(costdata);
            prepareData(apparentdata);
            prepareData(energydata);


            var sum = 0;
            fueldata.forEach(function(d) {
                if (d.total !== null) {
                    d.time = last = parseTimeISO(moment(d.key).format("YYYY-MM-DD HH:mm:ss"));
                    sum = sum + d.Fuel.sum;
                    d.total = sum ;
                }
            });


            fuelRatedata.forEach(function(d) {
                if (d.total !== null) {
                    d.time = last = parseTimeISO(moment(d.key).format("YYYY-MM-DD HH:mm:ss"));
                    d.total = d.FuelRate.avg;
                }
            });

            var currentBattery;
            batteryCapData.forEach(function(d) {
               if (d.batteryCapacity.avg !== null) {
                   d.time = last = parseTimeISO(moment(d.key).format("YYYY-MM-DD HH:mm:ss"));
                   d.total = d.batteryCapacity.avg;
                   currentBattery = d.batteryCapacity.avg;
               }else {
               d.time = last = parseTimeISO(moment(d.key).format("YYYY-MM-DD HH:mm:ss"));
               d.total = currentBattery;
               }
           });

           var battDOD;
           batteryDOD.forEach(function(d) {
                 if (d.DOD.avg  !== null) {
                     d.time = last = parseTimeISO(moment(d.key).format("YYYY-MM-DD HH:mm:ss"));
                     d.total = 100 - d.DOD.avg;
                     battDOD = 100 - d.DOD.avg;
                 } else {
                   d.time = last = parseTimeISO(moment(d.key).format("YYYY-MM-DD HH:mm:ss"));
                   d.total = battDOD;
                 }
             });

            timeData = getTimeData(typeTime, nameTime, typeColor);
            costData = getCostData(costType, costName, typeColor);
            powerData = getPowerData(powerType, powerName, typeColor);

            var time_type_data = timeData.time_type_data;
            var time_name_data = timeData.time_name_data;
            var cost_type_data = costData.cost_type_data;
            var cost_name_data = costData.cost_name_data;
            var power_type_data = powerData.power_type_data;
            var power_name_data = powerData.power_name_data;


            sortedData = sortData(SrcNameByType, SrcByType, SrcName_energy_data, energy_data, SrcName_P1, time_name_data, time_type_data, cost_name_data, cost_type_data, power_name_data, power_type_data, nameTime, typeColor);

            SrcName_energy_data_sorted = sortedData.SrcName_energy_data_sorted;
            time_name_data_sorted = sortedData.time_name_data_sorted;
            time_type_data_sorted = sortedData.time_type_data_sorted;
            cost_name_data_sorted = sortedData.cost_name_data_sorted;
            cost_type_data_sorted = sortedData.cost_type_data_sorted;
            power_name_data_sorted = sortedData.power_name_data_sorted;
            power_type_data_sorted = sortedData.power_type_data_sorted;
            SrcNameLoad_sorted = sortedData.SrcNameLoad_sorted;
            nameTime_sorted = sortedData.nameTime_sorted;

            try {
                try {
                    $("#timePieName").remove();
                    $("#timePieType").remove();
                    $("#energyPieName").remove();
                    $("#energyPieType").remove();
                    $("#costPieName").remove();
                    $("#costPieType").remove();
                    $("#powerPieName").remove();
                    $("#powerPieType").remove();
                } catch (err) {
                    console.info("first run no previous pies and donuts");
                }
                var canvasHeight = document.getElementById("energyDayGraph").getAttribute("height");
                Conf.size.canvasHeight = canvasHeight;
                ConfInner.size.canvasHeight = canvasHeight;

/*                pieSrc8 = createPie(Conf, power_name_data_sorted, " Power Max ", "power_max", "powerPieName", "Power : {value} W ");
                document.getElementById("powerPieName").setAttribute("viewBox", "0 0 " + String(Conf.size.canvasWidth) + " " + String(Conf.size.canvasHeight));
                document.getElementById("powerPieName").setAttribute("perserveAspectRatio", "xMidYMid");
                d3.select("#powerPieName").append("svg")
                    .attr("id", "powerCharts_svg");*/

/*                pieSrc7 = createPie(ConfInner, power_type_data_sorted, "Power Max ", "powerCharts_svg", "powerPieType", "Power : {value} W ");*/

                pieSrc6 = createPie(Conf, cost_name_data_sorted, "Cost / Source Type ", "costCharts", "costPieName", "Cost : {value} Naira ");
                document.getElementById("costPieName").setAttribute("viewBox", "0 0 " + String(Conf.size.canvasWidth) + " " + String(Conf.size.canvasHeight));
                document.getElementById("costPieName").setAttribute("perserveAspectRatio", "xMidYMid");
                d3.select("#costPieName").append("svg")
                    .attr("id", "costCharts_svg");

                pieSrc5 = createPie(ConfInner, cost_type_data_sorted, "Cost / Source Type", "costCharts_svg", "costPieType", "Cost : {value} Naira ");

                var conv_Time_Name_Data = secToHrs(time_name_data_sorted);

                pieSrc2 = createPie(Conf, conv_Time_Name_Data, "Time / Power Source", "TimeCharts", "timePieName", "Time: {value} Hrs");
                document.getElementById("timePieName").setAttribute("viewBox", "0 0 " + String(Conf.size.canvasWidth) + " " + String(Conf.size.canvasHeight));
                document.getElementById("timePieName").setAttribute("perserveAspectRatio", "xMidYMid");
                d3.select("#timePieName").append("svg")
                    .attr("id", "timePieType_svg");

                var conv_Time_Type_Data = secToHrs(time_type_data_sorted);

                pieSrc4 = createPie(ConfInner, conv_Time_Type_Data, "Time / Source Type", "timePieType_svg", "timePieType", "Time : {value} Hrs");

                pieSrc3 = createPie(Conf, SrcName_energy_data_sorted, "Energy / Source Name", "EnergyCharts", "energyPieName", "Energy: {value} Watt Hours");
                document.getElementById("energyPieName").setAttribute("viewBox", "0 0 " + String(Conf.size.canvasWidth) + " " + String(Conf.size.canvasHeight));
                document.getElementById("energyPieName").setAttribute("perserveAspectRatio", "xMidYMid");
                d3.select("#energyPieName").append("svg")
                    .attr("id", "energyPieType_svg");

                pieSrc1 = createPie(ConfInner, energy_data, "Energy / Source Type", "energyPieType_svg", "energyPieType", "Energy: {value} Watt Hours");
            } catch (err) {
                console.log("Source Type pie chart is not working");
            }
            buckets = json.buckets;

            x.domain(d3.extent(plotdata, function(d) {
                return d.time;
            }));
            y.domain([0, d3.extent(plotdata, function(d) {
                return d.total;
            })[1]]);

            yReactive.domain([0, d3.extent(reactivedata, function(d) {
                return d.total;
            })[1]]);


            xEnergyDay.domain([d3.time.hour.offset(new Date(date.getFullYear(), date.getMonth(), 1),-23),d3.time.hour.offset (new Date(date.getFullYear(), date.getMonth() + 1, 0),23)]);

            yEnergyDay.domain([0, d3.extent(EnergyByDay, function(d) {
                return d.total;
            })[1]]);

            yApparent.domain([0, d3.extent(apparentdata, function(d) {
                return d.total;
            })[1]]);

            xc.domain(d3.extent(costdata, function(d) {
                return d.time;
            }));
            yc.domain([0, d3.extent(costdata, function(d) {
                return d.total;
            })[1]]);

            xe.domain(d3.extent(energydata, function(d) {
                return d.time;
            }));
            ye.domain([0, d3.extent(energydata, function(d) {
                return d.total;
            })[1]]);

            xf.domain(d3.extent(fueldata, function(d) {
                return d.time;
            }));
            yf.domain([0, d3.extent(fueldata, function(d) {
                return d.total;
            })[1]]);

            xfr.domain(d3.extent(fuelRatedata, function(d) {
                return d.time;
            }));
            yfr.domain([0, d3.extent(fuelRatedata, function(d) {
                return d.total;
            })[1]]);


            xDOD.domain(d3.extent(batteryDOD, function(d) {
                return d.time;
            }));

            yDOD.domain([
                d3.min(batteryDOD, function(d) {
                    return d.total;
                }),
                d3.max(batteryDOD, function(d) {
                    return d.total;
                })
            ]);

             xb.domain(d3.extent( batteryCapData, function(d) {
                    return d.time;
                }));

            yb.domain([
                d3.min( batteryCapData, function(d) {
                    return d.total;
                }),
                d3.max( batteryCapData, function(d) {
                    return d.total;
                })
            ]);

       energyElements = createAxis (1,contexte,xAxise,yAxis1e,'Energy (Wh)',width,make_x_axis,1);//Energy
       costElements = createAxis (1,contextc,xAxisc,yAxis1c,'Cost (N)',width,make_x_axis,1); //Cost
       powerElements = createAxis (1,context,xAxis,yAxis1,'Power (W)',width,make_x_axis,1); // Power
       reactiveElements = createAxis (1,contextReactive,xAxis,yAxis1Reactive,'Power (VAR)',width,make_x_axis,1); // Reactive Power
       apparentElements =  createAxis (1,contextApparent,xAxis,yAxis1Apparent,'Power (VA)',width,make_x_axis,1); // Apparent Power
       fuelElements = createAxis (1,contextFuel,xAxisf,yAxis1f,'Fuel (L)',widthFuel,make_x_axisf,0); // fuel
       fuelRateElements = createAxis (1,contextFuelRate,xAxisf,yAxis1fr,'Fuel Rate (L/H)',widthFuel,make_x_axisf,0); // fuel Rate
       batteryCapElements = createAxis (1,contextBattery,xAxisb,yAxis1b,'Battery Capacity (%)',widthFuel,make_x_axisf,0); // Battery Cap
       batteryDODElements = createAxis (1,contextDOD,xAxisf,yAxis1DOD,'Battery DOD (%)',widthFuel,make_x_axisf,0); // Battery DOD
       EnergyByDayElements = createAxis (0,contextEnergyDay,xAxisEnergyDay,yAxis1EnergyDay,'Energy (Wh)',width2,make_x_axis,0); // Battery DOD


            //fuel drawing

            lined = contextFuel.append('path')
                    .datum(fueldata)
                    .attr("class", "lin")
                    .attr("d", lineFuel);

            linedRate = contextFuelRate.append('path')
                .datum(fuelRatedata)
                .attr("class", "lin")
                .attr("d", lineFuelRate);

             linedBatteryCap = contextBattery.append('path')
                     .datum(batteryCapData)
                     .attr("class", "inv")
                     .attr("d", lineBattery);

              linedBatteryDOD = contextDOD.append('path')
                       .datum(batteryDOD)
                       .attr("class", "inv")
                       .attr("d", lineDOD);


            spinner.stop();
            spinnerEnergy.stop();
            spinnerCost.stop();
            /*bars = draw (powerGraphScale,context,plotdata,svg,y,x);*/
/*            barsReactive = draw (roundTo,contextReactive,reactivedata,svgReactive,yReactive,x);
            barsApparent = draw (roundTo,contextApparent,apparentdata,svgApparent,yApparent,x);*/
            /*bars_cost = draw (roundTo,contextc,costdata,svgc,yc,xc);
            bars_energy = draw (roundTo,contexte,energydata,svge,ye,xe);*/
            bars_energyDay = drawDay (roundTo,contextEnergyDay,EnergyByDay,svgEnergyDay,yEnergyDay,xEnergyDay);
            resize();
            //populate_tables(SrcByType, SrcNameByType, SrcName_energy_data_sorted, nameTime_sorted, json.powersources, time_name_data_sorted, cost_name_data_sorted);
        });
    }




    function resize() {

        var chart = $("#energyDayGraph"),
            aspect = chart.width() / chart.height(),
            container = chart.parent();
        var pie_chart_en = $("#energyPieName"),
            pie_chart_en_container = pie_chart_en.parent();



        var targetWidth = container.width();
        var pie_chart_en_targetWidth = pie_chart_en_container.width();
        $("#energyDayGraph").attr("width", targetWidth);
        $("#energyDayGraph").attr("height", Math.round(targetWidth / aspect));

        $("#realtimegraph").attr("width", targetWidth);
        $("#realtimegraph").attr("height", Math.round(targetWidth / aspect));
        $("#energyPieName").attr("height", Math.round(targetWidth / aspect));
        $("#energyPieName").attr("width", pie_chart_en_targetWidth);
        $("#timePieName").attr("height", Math.round(targetWidth / aspect));
        $("#timePieName").attr("width", pie_chart_en_targetWidth);
        $("#powerPieName").attr("height", Math.round(targetWidth / aspect));
        $("#powerPieName").attr("width", pie_chart_en_targetWidth);
        // http://jsfiddle.net/shawnbot/BJLe6/
        // http://stackoverflow.com/questions/9400615/whats-the-best-way-to-make-a-d3-js-visualisation-layout-responsive
    }

    //set Summary and set settings window toggle
    $("#displayWindowLink").click(function() {
        $("#displayWindow").slideToggle();
    });

    //bootstrap datetime picker
    var dpStart = "";
    var dpStop = "";
    var dp1 = $('#datetimepicker1').datetimepicker();
    dp1.data("DateTimePicker").maxDate(new Date()).format("DD-MM-YYYY h:mm:ss a");
    var dp2 = $('#datetimepicker2').datetimepicker({
        useCurrent: false //Important! See issue #1075
    });
    dp2.data("DateTimePicker").maxDate(new Date()).format("DD-MM-YYYY h:mm:ss a");
    dp1.on("dp.change", function(e) {
        dpStart = e.date + "";
        dp2.data("DateTimePicker").minDate(e.date);

    });
    dp2.on("dp.change", function(e) {
        dp1.data("DateTimePicker").maxDate(e.date);
        dpStop = e.date + "";
    });

    $("#dPGo").on("click", function() {
        remove_graphElements();
        remove_livegraphElements();
        query(config, probe, "../bounds/" + config + "/" + probe, new Date().getTime() - parseInt(dpStart), dpStop);
    });

    //timeWindow Relative tab
    $("#historyRBtn").click(function() {
        var milSecTime = "";
        var inpt = $('#historyRTxt').val();
        if (inpt < 1) {
            return;
        }
        switch (parseInt($("#historyRS option:selected").val())) {
            case 1:
                milSecTime = inpt * 60 * 1000;
                break;
            case 2:
                milSecTime = parseInt(inpt) * 3600000;
                break;
            case 3:
                milSecTime = inpt * 60 * 60 * 24 * 1000;
                break;
            case 4:
                milSecTime = inpt * 60 * 60 * 24 * 7 * 1000;
                break;
            case 5:
                milSecTime = inpt * 60 * 60 * 24 * 7 * 30 * 1000;
                break;
            case 6:
                milSecTime = inpt * 60 * 60 * 24 * 7 * 30 * 12 * 1000;
                break;
            default:
                milSecTime = inpt * 60 * 60 * 24 * 1000;
        }
        remove_graphElements();
        remove_livegraphElements();
        query(config, probe, "../bounds/" + config + "/" + probe, milSecTime);
    });

    //listens for custom event (HomePageOps.scala)
    $("#quick").on("winChange", function(e, tm, tm2) {
        remove_graphElements();
        remove_livegraphElements();
        if (tm2 !== 'undefined') {
            query(config, probe, "../bounds/" + config + "/" + probe, tm, tm2);
        } else {
            query(config, probe, "../bounds/" + config + "/" + probe, tm);
        }
    });
    var realTimeWindow = $("#realTimeWindow").val(); //get default (in sec)
    $("#rTimeRange").ionRangeSlider({
        force_edges: false,
        values: [
            "30sec", "45sec", "1min", "2min",
            "3min", "5min", "7min",
            "10min", "15min", "20min", "25min", "30min"
        ],
        from: 5,
        onFinish: function(data) {
            chRealTimeWindowSize(data.from);
        }
    });

    function setProbeOnSummary() {
        if (($("#probe option:selected").html()) != 'all probes') {
            $("#probeval").html('of ' + $("#probe option:selected").html() + ' ');
        } else if (($("#probe option:selected").html()) == 'all probes') {
            $("#probeval").html('');
        }
    }

    function setConfOnSummary() {
        $("#configval").html($("#config option:selected").html());
    }

    function chRealTimeWindowSize(tm) {
        switch (tm) {
            case 0:
                realTimeWindow = "30";
                break;
            case 1:
                realTimeWindow = "45";
                break;
            case 2:
                realTimeWindow = "60";
                break;
            case 3:
                realTimeWindow = "120";
                break;
            case 4:
                realTimeWindow = "180";
                break;
            case 5:
                realTimeWindow = "300";
                break;
            case 6:
                realTimeWindow = "420";
                break;
            case 7:
                realTimeWindow = "600";
                break;
            case 8:
                realTimeWindow = "900";
                break;
            case 9:
                realTimeWindow = "1200";
                break;
            case 10:
                realTimeWindow = "1500";
                break;
            case 11:
                realTimeWindow = "1800";
                break;
            default:
                realTimeWindow = "300";
                break;
        }
        $("#realTimeWindow").val(realTimeWindow);
        remove_livegraphElements();
        load_recent("../recent/" + config + "/" + probe + "/" + moment(lastTransmit).format("YYYY-MM-DDTHH:mm:ss.SSSZ") + "/" + realTimeWindow);
    }

});