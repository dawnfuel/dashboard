$(document).ready(function() {

    $('.alert').on('close.bs.alert', function(e) {
        e.preventDefault();
        $(this).hide();
    });
    //changing the compress and enlarge icons
    $("a i").click(function() {
        if ($(this).hasClass('fa-compress')) {

            $(this).removeClass('fa-compress');
            $(this).addClass('fa-expand');
        } else if ($(this).hasClass('fa-expand')) {
            $(this).removeClass('fa-expand');
            $(this).addClass('fa-compress');
        }
    });

    //Adding table header in mobile
    if (screen.width < 1024) {
        $('.reactive').each(function() {
            var text = 'Reactive' + ' ' + ' ' + $(this).text();
            $(this).text(text);
        });
        $('.apparent').each(function() {
            var text = 'Apparent' + ' ' + ' ' + $(this).text();
            $(this).text(text);
        });
    }

    var margin = {
            top: 10,
            right: 80,
            bottom: 70,
            left: 60
        }, //margin2 = {top: 430, right: 80, bottom: 30, left: 40},
        width = parseInt(d3.select('#cost_graph').style('width'), 10),
        widthFuel = parseInt(d3.select('#fuel_graph').style('width'), 10),
        height = 500 - margin.top - margin.bottom; //height2 = 500 - margin2.top - margin2.bottom;


    var ConfInner = {
        header: {
            title: {
                text: ""
            },
            location: "top-center"
        },
        size: {
            canvasHeight: 400,
            canvasWidth: 400,
            pieInnerRadius: "10%",
            pieOuterRadius: "35%"
        },
        misc: {
            pieCenterOffset: {
                x: 0,
                y: 0
            }
        },
        data: {
            sortOrder: "none",
            enabled: true,
            valueType: "percentage",
            content: []
        },
        labels: {
            percentage: {
                color: "gray"
            },
            mainLabel: {
                color: "white"
            },
            outer: {
                format: "label",
                hideWhenLessThanPercentage: null,
                pieDistance: 30
            },
            inner: {
                format: "percentage",
                hideWhenLessThanPercentage: null
            }
        },
        effects: {
            pullOutSegmentOnClick: {
                effect: "bounce",
                speed: 300,
                size: 10
            },
        },
        tooltips: {
            enabled: true,
            type: "placeholder", // caption|placeholder,
            string: "Energy Consumption: {value} Watt Hours"
        }
    };
    var Conf = {
        header: {
            title: {
                text: ""
            },
            location: "top-center"
        },
        size: {
            canvasHeight: 400,
            canvasWidth: 400,
            pieInnerRadius: "50%",
        },
        data: {
            sortOrder: "none",
            enabled: true,
            valueType: "percentage",
            content: [] //energy_data
        },
        misc: {
            pieCenterOffset: {
                x: 0,
                y: 0
            }
        },
        labels: {
            percentage: {
                color: "gray"
            },
            mainLabel: {
                color: "white"
            }
        },
        effects: {
            pullOutSegmentOnClick: {
                effect: "bounce",
                speed: 300,
                size: 10
            },
        },
        tooltips: {
            enabled: true,
            type: "placeholder", // caption|placeholder,
            string: "Energy Consumption: {value} Watt Hours"
        }
    };

    function getEnergyData(d, t1, P1, SrcName_Energy_Total, SrcName_P1, Energy_total, source_types, source_names, SrcName_energy_data, energy_data, typeColor, SrcByType, SrcNameByType) {

        var docSum1 = 0.0;
        var docSum2 = 0.0;

        for (stype = 0; stype < d.SourceType.buckets.length; stype++) {
            docSum1 += d.SourceType.buckets[stype].doc_count;
            if (source_types.indexOf(d.SourceType.buckets[stype].key) == -1) {
                source_types.push(d.SourceType.buckets[stype].key);
                Energy_total.push(0.0);
                P1.push(0.0);
                SrcNameByType.push([]);
                SrcByType.push(d.SourceType.buckets[stype].key);
            }

        }

        for (sname = 0; sname < d.SourceName.buckets.length; sname++) {
            docSum2 += d.SourceName.buckets[sname].doc_count;
            if (source_names.indexOf(d.SourceName.buckets[sname].key) == -1) {
                source_names.push(d.SourceName.buckets[sname].key);
                SrcName_Energy_Total.push(0.0);
                SrcName_P1.push(0.0);
                for (stype = 0; stype < d.SourceType.buckets.length; stype++) {
                    for (typeName = 0; typeName < d.SourceType.buckets[stype].SourceName.buckets.length; typeName++) {
                        if (d.SourceName.buckets[sname].key == d.SourceType.buckets[stype].SourceName.buckets[typeName].key) {
                            for (index = 0; index < SrcByType.length; index++) {
                                if (SrcByType[index] == d.SourceType.buckets[stype].key) {
                                    SrcNameByType[index].push(d.SourceName.buckets[sname].key);
                                    break;
                                }
                            }
                        }
                    }
                }
            }
        }

        var P2 = new Array(P1.length);
        var SrcName_P2 = new Array(SrcName_P1.length);

        for (stype = 0; stype < source_types.length; stype++) {
            var chk = -1;
            for (index = 0; index < d.buckets.length; index++) {
                if (d.buckets[index].key == source_types[stype]) {
                    chk = stype;
                    fraction = d.buckets[index].doc_count;
                    P2[stype] = d.Power_real_total.avg * fraction / docSum1;
                }
            }
            if (chk == -1) {
                P2[stype] = 0;
            }
        }


        for (sname = 0; sname < source_names.length; sname++) {
            var chk2 = -1;
            for (index = 0; index < d.SourceName.buckets.length; index++) {
                if (d.SourceName.buckets[index].key == source_names[sname]) {
                    chk2 = sname;
                    fraction = d.SourceName.buckets[index].doc_count;
                    SrcName_P2[sname] = d.Power_real_total.avg * fraction / docSum2;
                }
            }
            if (chk2 == -1) {
                SrcName_P2[sname] = 0;
            }
        }

        var t2 = new Date(d.time).getTime() / 3600000.0;
        var time_delta = t2 - t1;

        if (time_delta < 10) {
            for (i = 0; i < source_types.length; i++) {
                Energy_total[i] = Energy_total[i] + (P1[i] + P2[i]) * time_delta / 2;
            }

            for (i = 0; i < source_names.length; i++) {
                SrcName_Energy_Total[i] = SrcName_Energy_Total[i] + (SrcName_P1[i] + SrcName_P2[i]) * time_delta / 2;
            }
        }
        P1 = P2;
        SrcName_P1 = SrcName_P2;
        t1 = t2;

        energy_data = [];
        for (i = 0; i < source_types.length; i++) {
            energy_data.push({
                "label": source_types[i],
                "value": roundTo(Energy_total[i]),
                "color": color[typeColor.indexOf(source_types[i])]
            });
        }

        SrcName_energy_data = [];
        for (i = 0; i < source_names.length; i++) {
            SrcName_energy_data.push({
                "label": source_names[i],
                "value": roundTo(SrcName_Energy_Total[i]),
                "color": color[typeColor.indexOf(source_names[i])]
            });
        }

        var output = {};

        output.t1 = t1;
        output.P1 = P1;
        output.SrcName_Energy_Total = SrcName_Energy_Total;
        output.SrcName_P1 = SrcName_P1;
        output.Energy_total = Energy_total;
        output.source_types = source_types;
        output.source_names = source_names;
        output.SrcName_energy_data = SrcName_energy_data;
        output.energy_data = energy_data;
        output.typeColor = typeColor;
        output.SrcByType = SrcByType;
        output.SrcNameByType = SrcNameByType;

        return output;
    }

    function getCostData(costType, costName, typeColor) {

        var cost_type_data = [];
        for (i = 0; i < costType.length; i++) {
            cost_type_data.push({
                "label": costType[i].key,
                "value": roundTo(costType[i].TimeStats.sum),
                "color": color[typeColor.indexOf(costType[i].key)]
            });
        }

        var cost_name_data = [];
        for (i = 0; i < costName.length; i++) {
            cost_name_data.push({
                "label": costName[i].key,
                "value": roundTo(costName[i].TimeStats.sum),
                "color": color[typeColor.indexOf(costName[i].key)]
            });
        }

        var output = {};
        output.cost_type_data = cost_type_data;
        output.cost_name_data = cost_name_data;

        return output;

    }




        function sortData(SrcNameByType, SrcByType, SrcName_energy_data, energy_data, SrcName_P1, cost_name_data, cost_type_data, typeColor) {

            var SrcName_energy_data_sorted = [];
            var SrcNameLoad_sorted = [];
            var cost_type_data_sorted = [];
            var cost_name_data_sorted = [];


            for (stype = 0; stype < SrcNameByType.length; stype++) {
                col = color[typeColor.indexOf(SrcByType[stype])].toString();
                for (sname = 0; sname < SrcNameByType[stype].length; sname++) {
                    for (sname_data = 0; sname_data < SrcName_energy_data.length; sname_data++) {
                        if (SrcNameByType[stype][sname] == SrcName_energy_data[sname_data].label) {
                            SrcName_energy_data[sname_data].color = d3.rgb(col).brighter(sname * 0.3);
                            SrcName_energy_data_sorted.push(SrcName_energy_data[sname_data]);
                            SrcNameLoad_sorted.push(SrcName_P1[sname_data]);
                            break;
                        }
                    }

                    for (sname_data = 0; sname_data < cost_name_data.length; sname_data++) {
                        if (SrcNameByType[stype][sname] == cost_name_data[sname_data].label) {
                            cost_name_data[sname_data].color = d3.rgb(col).brighter(sname * 0.3);
                            cost_name_data_sorted.push(cost_name_data[sname_data]);
                            break;
                        }
                    }

                }
            }


            for (stype = 0; stype < energy_data.length; stype++) {
                for (stype_data = 0; stype_data < cost_type_data.length; stype_data++) {
                    if (energy_data[stype].label == cost_type_data[stype_data].label) {
                        cost_type_data_sorted.push(cost_type_data[stype_data]);
                        break;
                    }
                }
            }



            var sortedData = {};

            sortedData.SrcName_energy_data_sorted = SrcName_energy_data_sorted;
            sortedData.SrcNameLoad_sorted = SrcNameLoad_sorted;
            sortedData.cost_name_data_sorted = cost_name_data_sorted;
            sortedData.cost_type_data_sorted = cost_type_data_sorted;


            return sortedData;
        }


    function createPie(Conf, pie_data, title, parent, id, tooltip) {
        Conf.data.content = pie_data;
        Conf.header.title.text = title;
        Conf.tooltips.string = tooltip;
        var pie = new d3pie(parent, Conf);
        document.getElementById(parent).children[0].setAttribute("id", id);
        return pie;
    }

    var config = document.getElementById('config').options[0].value;
    var probe = document.getElementById('probe').options[0].value;
    var lastTransmit = 0;

    $("#probe").on('change', function() {
        var timeWindow = $('#hiddenTimeWindow').val();
        probe = $("#probe").val();
        config = document.getElementById('config').options[0].value;
        remove_graphElements();
        query(config, probe, "../bounds/" + config + "/" + probe, timeWindow);
        setProbeOnSummary();
    });

    $("#config").change(function() {
        var timeWindow = $('#hiddenTimeWindow').val();
        config = $("#config").val();
        probe = document.getElementById('probe').options[0].value;
        remove_graphElements();
        query(config, probe, "../bounds/" + config + "/" + probe, timeWindow);
        setConfOnSummary();
    });
    query(config, probe, "../bounds/" + config + "/" + probe, $('#hiddenTimeWindow').val());
    setProbeOnSummary();
    setConfOnSummary();


    var export_start = "";
    var export_stop = "";
    $("#export").click(function() {
        console.log("data period");
        console.log(export_start + " ============== " + export_stop);
        console.log("export button click");
        console.log(new Date(export_start));
        console.log(new Date(export_stop));

        var start_time = new Date(export_start);
        var stop_time = new Date(export_stop);

        var tz = start_time.getTimezoneOffset();
        console.log("timezone is " + tz);

        console.log("start_time: " + start_time);
        console.log("stop_time: " + stop_time);

        var year_start = new Date(new Date(new Date(start_time.getTime()).setMonth(0)).setDate(1));
        var year_end = new Date(new Date(new Date(start_time.getTime()).setMonth(11)).setDate(31));

        console.log("year_start: " + year_start);
        console.log("year_end: " + year_end);

//        console.log(day_start);
//        console.log(day_end);

//        var front_padding_hours = Math.abs(Math.floor(year_start-start_time)/3600000);
//        var end_padding_hours = Math.abs(Math.floor(stop_time-year_end)/3600000);
//
//        console.log("front_padding_hours: " + front_padding_hours);
//        console.log("end_padding_hours: " + end_padding_hours);

//        var elasticsearch = require('elasticsearch');
        var client = elasticsearch.Client({
          host: 'localhost:9200',
          log: 'trace'
        });



        client.search({
          index: 'grit',
          type: 'docs',
          body: {
                   "query":{
                              "bool":{
                                "must":[
                                  {
                                    "match": {
                                      "ProbeID_FK": "565DAD26B5EC1977E63B48EBSYSTEM01"

                                    }
                                  },
                                  {
                                    "range": {
                                      "Time": {
                                        "gte": export_start,
                                        "lte": export_stop,
                                        "format": "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
                                      }
                                    }
                                  }
                                ]

                              }
                            },
                            "aggs" : {

                        "energies_over_time" : {
                            "date_histogram" : {
                                "field" : "Time",
                                "interval" : "1h",
                                "format" : "yyyy-MM-dd'T'HH:mm:ss.SSSZ",
                                "time_zone": "+01:00"
                            },
                            "aggs" : {
                                "avg_energy" : { "avg" : { "field" : "Energy_since_last" } }
                            }
                        }

                    },
                   "sort" : [
                        { "Time" : {"order" : "asc"}}
                    ]


                }
        }).then(function (resp) {
            var energy_time_series ="";
            var counter = 0;

//            var hits = resp.hits.hits;
//            console.log(hits);
            var aggs = resp.aggregations.energies_over_time.buckets;
            console.log("++++++++++++++++++ aggregations ++++++++++++++++++++++");
            console.log(aggs[0]);
            console.log("show data");
            start_time = new Date(aggs[0].key);
            stop_time = new Date(aggs[aggs.length-1].key);
            console.log("start_time: " + start_time);
            console.log("stop_time: " + stop_time);

            var front_padding_hours = Math.floor(Math.abs(year_start-start_time)/3600000.0);
            var end_padding_hours = Math.floor(Math.abs(stop_time-year_end)/3600000.0);

            console.log("front_padding_hours: " + front_padding_hours);
            console.log("end_padding_hours: " + end_padding_hours);


            for(hr = 0; hr < front_padding_hours; hr++){
                energy_time_series += "0\n";
                counter += 1;
            }
            aggs.forEach(function(e){
//                energy_time_series += e.avg_energy.value.toString + "\n";
                if(e.avg_energy.value === null){
                    energy_time_series += "0\n";
                    counter += 1;
//                    console.log(0);
                }
                else{
                    energy_time_series += e.avg_energy.value.toString() + "\n";
                    counter += 1;
//                    console.log(e.avg_energy.value.toString());
                }
            });

            for(hr = 0; hr < end_padding_hours; hr++){
                energy_time_series += "0\n";
                counter += 1;
            }

            console.log("total counter: " + counter);

            var a = document.body.appendChild(
                    document.createElement("a")
                );
            a.download = "energy_data.txt";
            a.href = "data:text/plain;base64," + btoa(energy_time_series);
            a.click();
//            hits.forEach(function(hit){
//            console.log("printing time window");
//            console.log($('#hiddenTimeWindow').val());
//            console.log(hit._source.SourceName);

//            });
        }, function (err) {
            console.log("error found");
            console.trace(err.message);
        });
    });


    var DateDiff = {

        inSeconds: function(d1, d2) {
            var t2 = d2.getTime();
            var t1 = d1.getTime();
            return parseInt((t2 - t1) / (1000));
        },
        inMinutes: function(d1, d2) {
            var t2 = d2.getTime();
            var t1 = d1.getTime();
            return parseInt((t2 - t1) / (60 * 1000));
        },

        inHours: function(d1, d2) {
            var t2 = d2.getTime();
            var t1 = d1.getTime();
            return parseInt((t2 - t1) / (3600 * 1000));
        },

        inDays: function(d1, d2) {
            var t2 = d2.getTime();
            var t1 = d1.getTime();

            return parseInt((t2 - t1) / (24 * 3600 * 1000));
        },

        inWeeks: function(d1, d2) {
            var t2 = d2.getTime();
            var t1 = d1.getTime();

            return parseInt((t2 - t1) / (24 * 3600 * 1000 * 7));
        },

        inMonths: function(d1, d2) {
            var d1Y = d1.getFullYear();
            var d2Y = d2.getFullYear();
            var d1M = d1.getMonth();
            var d2M = d2.getMonth();

            return (d2M + 12 * d2Y) - (d1M + 12 * d1Y);
        },

        inYears: function(d1, d2) {
            return d2.getFullYear() - d1.getFullYear();
        }
    };

    function getDateDiff(d1, d2) {

        if (DateDiff.inSeconds(d1, d2) < 60) {
            if (DateDiff.inSeconds(d1, d2) === 1) {
                return DateDiff.inSeconds(d1, d2) + ' second';
            }
            return DateDiff.inSeconds(d1, d2) + ' seconds';
        } else if (DateDiff.inMinutes(d1, d2) < 60) {
            if (DateDiff.inMinutes(d1, d2) === 1) {
                return DateDiff.inMinutes(d1, d2) + ' minute';
            }
            return DateDiff.inMinutes(d1, d2) + ' minutes';
        } else if (DateDiff.inHours(d1, d2) < 24) {
            if (DateDiff.inHours(d1, d2) === 1) {
                return DateDiff.inHours(d1, d2) + ' hour';
            }
            return DateDiff.inHours(d1, d2) + ' hours';
        } else if (DateDiff.inDays(d1, d2) < 7) {

            if (DateDiff.inDays(d1, d2) === 1) {
                return DateDiff.inDays(d1, d2) + ' day';
            }
            return DateDiff.inDays(d1, d2) + ' days';
        } else if (DateDiff.inWeeks(d1, d2) < 4) {
            if (DateDiff.inWeeks(d1, d2) === 1) {
                return DateDiff.inWeeks(d1, d2) + ' week';
            }
            return DateDiff.inWeeks(d1, d2) + ' weeks';
        } else if (DateDiff.inMonths(d1, d2) < 11) {
            if (DateDiff.inMonths(d1, d2) === 1) {
                return DateDiff.inMonths(d1, d2) + ' month';
            }
            return DateDiff.inMonths(d1, d2) + ' months';
        } else if (DateDiff.inYears(d1, d2) > 0) {
            if (DateDiff.inYears(d1, d2) === 1) {
                return DateDiff.inYears(d1, d2) + ' year';
            }
            return DateDiff.inYears(d1, d2) + ' years';
        } else {
            return 'n/a ';
        }

    }


    function getDateDiff2(d1, d2) {

            if (DateDiff.inDays(d1, d2) < 7) {
                if (DateDiff.inDays(d1, d2) <= 1) {
                    return (moment(d1).format("MMM Do"));
                }
               return ('from ' + moment(d1).format("MMM Do") + ' to ' + moment(d2).format("MMM Do"));
            } else if (DateDiff.inWeeks(d1, d2) < 4) {

                return ('from ' + moment(d1).format("MMM Do") + ' to ' + moment(d2).format("MMM Do"));
            } else if (DateDiff.inMonths(d1, d2) < 11) {
                return ('from ' + moment(d1).format("MMM Do") + ' to ' + moment(d2).format("MMM Do"));
            } else if (DateDiff.inYears(d1, d2) > 0) {
                if (moment(d1).format("YYYY") === moment(d2).format("YYYY")) {
                    return (moment(d1).format("YYYY"));
                }
                return ('from ' + moment(d1).format("YYYY") + ' to ' + moment(d2).format("YYYY"));
            } else {
                return 'n/a ';
            }

        }

    $('.alert').hide();
    $('.alert').on('close.bs.alert', function(e) {
        e.preventDefault();
        $(this).hide();
    });
    //Alert
    function closeAlert() {
        $('.alert').show();
        $(".alert").fadeTo(1500, "swing").slideUp(500);
    }

    function query(config, probe, url, TimeWindow, user_stop) {
        var windowSizeMsg = "";
        $('#hiddenTimeWindow').val(TimeWindow); //set window to a hidden field so it can be accessible by other functions
        $.get(url, function(data) {
            console.log(data);
            data_stop_realtime = data.stop; // most recent value returned from snippet

            data_stop = typeof user_stop !== 'undefined' ? user_stop : data_stop_realtime;
            data_stop = moment(parseInt(data_stop)).format("YYYY-MM-DDTHH:mm:ss.SSSZ");
            dp2.data("DateTimePicker").date(moment(data_stop)); //set dateTimePicker2 time
            // tz awareness pending
            var user_start = new Date().getTime() - parseFloat(TimeWindow);
            var data_start = "";
            var popMSg;


            if (user_start < data.start) {
                data_start = moment(data.start).format("YYYY-MM-DDTHH:mm:ss.SSSZ");
                dp1.data("DateTimePicker").date(moment(data.start)); //set dateTimePicker1 time
                popMSg = "from  " + moment(data.start).format("DD-MM-YYYY HH:mm:ss.SSSZ");

            } else {
                data_start = moment(user_start).format("YYYY-MM-DDTHH:mm:ss.SSSZ");
                data_start = moment(user_start).format("YYYY-MM-DDTHH:mm:ss.SSSZ");
                dp1.data("DateTimePicker").date(moment(user_start)); //set dateTimePicker1 time
                popMSg = "from   " + moment(user_start).format("DD-MM-YYYY HH:mm:ss.SSSZ");
            }



            if (data_start > data_stop) {

                if (sessionStorage.length > 0) {
                    $("#timeWindowSpan").html(sessionStorage.windowSizeMsg);
                    $("#timeWindowSpan").attr("title", sessionStorage.popMsg);
                    load_data("../data/" + config + "/" + probe + "/" + sessionStorage.data_start + "/" + sessionStorage.data_stop);
                    lastTransmit = sessionStorage.data_stop;
                }
                return closeAlert();

            }

            var timeCheck = Date.now() - 120000 ;
            timeCheck = moment(parseInt(timeCheck)).format("YYYY-MM-DDTHH:mm:ss.SSSZ");
            //windowSizeMsg = 'from Last  '+ getDateDiff (new Date(moment(data_start).format("MM-DD-YYYY HH:mm:ss.SSSZ")), new Date (moment(data_stop).format("MM-DD-YYYY HH:mm:ss.SSSZ")));
            if (data_stop > timeCheck ) {

            windowSizeMsg = 'last ' + getDateDiff(new Date(data_start), new Date(data_stop));
            popMSg += "   to  " + moment(data_stop).format("DD-MM-YYYY HH:mm:ss.SSSZ");

            }else {
            windowSizeMsg = getDateDiff2(new Date(data_start), new Date(data_stop));
            popMSg += "   to  " + moment(data_stop).format("DD-MM-YYYY HH:mm:ss.SSSZ");

            }

            export_start = data_start;
            export_stop = data_stop;


            $("#timeWindowSpan").html(windowSizeMsg);
            $("#timeWindowSpan").attr("title", popMSg);
            load_data("../data/" + config + "/" + probe + "/" + data_start + "/" + data_stop);
            lastTransmit = data.stop;

            //sessions
            sessionStorage.popMsg = popMSg;
            sessionStorage.windowSizeMsg = windowSizeMsg;
            sessionStorage.data_stop_realtime = data_stop_realtime;
            sessionStorage.data_start = data_start;
            sessionStorage.data_stop = data_stop;
            sessionStorage.realTimeWindow = realTimeWindow;
        });
    }

    var brush;
    var brush_status = 0;
    var brush0 = 0;
    var brush1 = 0;

    var  costElements;//Cost


     var bars_cost;//Cost
     var lined;//fuel
     var linedRate;//FuelRate

     var opts = {
       lines: 13, // The number of lines to draw
       length: 20, // The length of each line
       width: 5, // The line thickness
       radius: 30, // The radius of the inner circle
       scale: 10, // Scales overall size of the spinner
       color: '#FFFFFF', // #rgb or #rrggbb or array of colors
       speed: 1.9, // Rounds per second
       trail: 40, // Afterglow percentage
       className: 'spinner', // The CSS class to assign to the spinner
     };

    var parseTimeISO = d3.time.format("%Y-%m-%d %H:%M:%S").parse;

    var x = d3.time.scale().range([0, width]);
    var y = d3.scale.linear().range([height, 0]);


    //Cost axes
    var xc = d3.time.scale().range([0, width]);
    var yc = d3.scale.linear().range([height, 0]);
    var svgc = d3.select("#cost_graph").append("svg")
        .attr("id", "costgraph")
        .attr("viewBox", String(0) + " " + String(0) + " " + String(width + margin.left + margin.right) + " " + String(height + margin.top + margin.bottom))
        .attr("perserveAspectRatio", "xMinYMid")
        .attr("width", width + margin.left + margin.right)
        .attr("height", height + margin.top + margin.bottom)
        .attr("data-html", "Use the <i class=\"fa fa-plus\"></i> to zoom into a time range")
        .attr("data-variation", "huge")
        .append("g")
        .attr("transform", "translate(" + margin.left + "," + margin.top + ")");
    var contextc = svgc.append("g")
        .attr("id", "contextc")
        .attr("class", "contextc");
    var xAxisc = d3.svg.axis()
        .scale(xc)
        .orient("bottom");
    var yAxis1c = d3.svg.axis()
        .scale(yc)
        .orient("left"); //end

    //fuel axes
    var xf = d3.time.scale().range([0, widthFuel]);
    var yf = d3.scale.linear().range([height, 0]);
    var svgFuel = d3.select("#fuel_graph").append("svg")
        .attr("id", "fuelgraph")
        .attr("viewBox", String(0) + " " + String(0) + " " + String(widthFuel + margin.left + margin.right) + " " + String(height + margin.top + margin.bottom))
        .attr("perserveAspectRatio", "xMinYMid")
        .attr("width", widthFuel + margin.left + margin.right)
        .attr("height", height + margin.top + margin.bottom)
        .append("g")
        .attr("transform", "translate(" + margin.left + "," + margin.top + ")");
    var contextFuel = svgFuel.append("g")
        .attr("id", "context")
        .attr("class", "context");
    var xAxisf = d3.svg.axis()
        .scale(xf)
        .orient("bottom");
    var yAxis1f = d3.svg.axis()
        .scale(yf)
        .orient("left"); //end
    var lineFuel = d3.svg.line()
        .x(function(d) {
            return xf(d.time);
        })
        .y(function(d) {
            return yf(d.total);
        })
        .interpolate("linear");

    var xfr = d3.time.scale().range([0, widthFuel]);
    var yfr = d3.scale.linear().range([height, 0]);
    var svgFuelRate = d3.select("#fuelCharts").append("svg")
        .attr("id", "fuelCharts")
        .attr("viewBox", String(0) + " " + String(0) + " " + String(widthFuel + margin.left + margin.right) + " " + String(height + margin.top + margin.bottom))
        .attr("perserveAspectRatio", "xMinYMid")
        .attr("width", widthFuel + margin.left + margin.right)
        .attr("height", height + margin.top + margin.bottom)
        .append("g")
        .attr("transform", "translate(" + margin.left + "," + margin.top + ")");
    var contextFuelRate = svgFuelRate.append("g")
        .attr("id", "context")
        .attr("class", "context");
    /*var xAxisfr = d3.svg.axis()
        .scale(xfr)
        .orient("bottom");*/
    var yAxis1fr = d3.svg.axis()
        .scale(yfr)
        .orient("left"); //end
    var lineFuelRate = d3.svg.line()
        .x(function(d) {
            return xfr(d.time);
        })
        .y(function(d) {
            return yfr(d.total);
        })
        .interpolate("linear");


    //battery
        var xb = d3.time.scale().range([0, widthFuel]);
        var yb = d3.scale.linear().range([height, 0]);
        var svgBattery = d3.select("#capGraph").append("svg")
            .attr("id", "capGraph")
            .attr("viewBox", String(0) + " " + String(0) + " " + String(widthFuel + margin.left + margin.right) + " " + String(height + margin.top + margin.bottom))
            .attr("perserveAspectRatio", "xMinYMid")
            .attr("width", widthFuel + margin.left + margin.right)
            .attr("height", height + margin.top + margin.bottom)
            .append("g")
            .attr("transform", "translate(" + margin.left + "," + margin.top + ")");
        var contextBattery = svgBattery.append("g")
            .attr("id", "context")
            .attr("class", "context");
        var xAxisb = d3.svg.axis()
            .scale(xb)
            .orient("bottom");
        var yAxis1b = d3.svg.axis()
            .scale(yb)
            .orient("left"); //end
        var lineBattery = d3.svg.line()
            .x(function(d) {
                return xb(d.time);
            })
            .y(function(d) {
                return yb(d.total);
            })
            .interpolate("linear");

        var xDOD = d3.time.scale().range([0, widthFuel]);
        var yDOD = d3.scale.linear().range([height, 0]);
        var svgDOD = d3.select("#dodGraph").append("svg")
            .attr("id", "dodGraph")
            .attr("viewBox", String(0) + " " + String(0) + " " + String(widthFuel + margin.left + margin.right) + " " + String(height + margin.top + margin.bottom))
            .attr("perserveAspectRatio", "xMinYMid")
            .attr("width", widthFuel + margin.left + margin.right)
            .attr("height", height + margin.top + margin.bottom)
            .append("g")
            .attr("transform", "translate(" + margin.left + "," + margin.top + ")");
        var contextDOD = svgDOD.append("g")
            .attr("id", "context")
            .attr("class", "context");
        /*var xAxisfr = d3.svg.axis()
            .scale(xfr)
            .orient("bottom");*/
        var yAxis1DOD = d3.svg.axis()
            .scale(yDOD)
            .orient("left"); //end
        var lineDOD = d3.svg.line()
            .x(function(d) {
                return xDOD(d.time);
            })
            .y(function(d) {
                return yDOD(d.total);
            })
            .interpolate("linear");

    function make_x_axis() {
        return d3.svg.axis()
            .scale(x)
            .orient("bottom")
            .ticks(10);
    }

    function make_y_axis() {
        return d3.svg.axis()
            .scale(y)
            .orient("left")
            .ticks(6);
    }


    function make_x_axisf() {
        return d3.svg.axis()
            .scale(xf)
            .orient("bottom")
            .ticks(10);
    }

    function resetBrush() {
        brush
            .clear()
            .event(d3.select(".brush"));
    }

    function roundTo(num) {
        return +(Math.round(num + "e+2") + "e-2");
    }

/*    function powerGraphScale(power) {
        if (+power >= 1000) {
            var scaledPower = (power / 1000);
            return roundTo(scaledPower) + "KW";
        } else {
            return roundTo(power) + "W";
        }
    }*/

    function remove_graphElements() {
        try {
            bars_cost.remove();
            costElements.forEach(function (d){d.remove();});
            fuelElements.forEach(function (d){d.remove();});
            fuelRateElements.forEach(function (d){d.remove();});
            batteryCapElements.forEach(function (d){d.remove();});
            batteryDODElements.forEach(function (d){d.remove();});
            lined.remove();
            linedRate.remove();
            linedBatteryCap.remove();
            linedBatteryDOD.remove();
        } catch (err) {
            console.log(err.message);
        }
    }

    function brushend() {
        if (brush.empty()) {

            return 0;
        }
        if (brush_status == 1) {
            //update_timefilter();
            window_start = moment(new Date(brush0)).format("YYYY-MM-DDTHH:mm:ss.SSSZ");
            window_stop = moment(new Date(brush1)).format("YYYY-MM-DDTHH:mm:ss.SSSZ");
            window_start_unix = moment(new Date(brush0)).unix();
            window_stop_unix = moment(new Date(brush1)).unix();

            var brushPopMSg;
            var timeCheck = Date.now() - 120000 ;
            timeCheck = moment(parseInt(timeCheck)).format("YYYY-MM-DDTHH:mm:ss.SSSZ");
            if (window_stop > timeCheck ) {

            brushPopMSg = "from  " + moment(new Date(brush0)).format("DD-MM-YYYY HH:mm:ss") + "   to  " + moment(new Date(brush1)).format("DD-MM-YYYY HH:mm:ss");
            $("#timeWindowSpan").html('from ' + getDateDiff(new Date(brush0), new Date(brush1)));
            $("#timeWindowSpan").attr("title", brushPopMSg);

            }else {
            brushPopMSg = "from  " + moment(new Date(brush0)).format("DD-MM-YYYY HH:mm:ss") + "   to  " + moment(new Date(brush1)).format("DD-MM-YYYY HH:mm:ss");
            $("#timeWindowSpan").html(getDateDiff2(new Date(brush0), new Date(brush1)));
//            $("#timeWindowSpan").html('from ' + moment(brush0).format("MMM Do") + ' to ' + moment(brush1).format("MMM Do"));
            $("#timeWindowSpan").attr("title", brushPopMSg);

            }

            //$("#timeWindowSpan").html( "from  "+moment(new Date(brush0)).format("DD-MM-YYYY HH:mm:ss")+"   to  "+moment(new Date(brush1)).format("DD-MM-YYYY HH:mm:ss"));

            dp1.data("DateTimePicker").date(moment(new Date(brush0))); //set dateTimePicker1 time
            dp2.data("DateTimePicker").date(moment(new Date(brush1))); //set dateTimePicker2 time
            remove_graphElements();
            resetBrush();
            d3.selectAll(".brush").remove();
            load_data("../data/" + config + "/" + probe + "/" + window_start + "/" + window_stop);
        }
        brush_status = 0;
    }


    function brushed() {
        brush0 = brush.extent()[0];
        brush1 = brush.extent()[1];
        brush_status = 1;
    }
    // brush tool to let us zoom and pan using the overview chart
    brush = d3.svg.brush()
        .x(x)
        .on("brush", brushed)
        .on("brushend", brushend);
    var color = ["lightblue", "lightgreen", "darkorange", "red"];
    var legend_colors = [
        ["inverter", "lightblue"],
        ["grid", "lightgreen"],
        ["generator", "darkorange"],
        ["error", "red"]
    ];


    var colour = d3.scale.ordinal()
        .range(color);


    function createAxis (xGrids,contextP,xAxisP,yAxis1P,axisYText,f,fx,brushAxis) {

     if (xGrids) {
       gridxP = contextP.append("g")
           .attr("class", "grid")
           .attr("transform", "translate(0," + height + ")")
           .call(fx()
               .tickSize(-height, 0, 0)
               .tickFormat("")
           );
          }

       gridyP = contextP.append("g")
           .attr("class", "grid")
           .call(make_y_axis("left")
               .tickSize(-f, 0, 0)
               .tickFormat("")
           );

       axisxP = contextP.append("g")
           .attr("class", "x axis")
           .attr("transform", "translate(0," + height + ")")
           .call(xAxisP);

       axisx_labelP = axisxP.append("text")
           .attr("x", 0)
           .attr("dx", "3.5em")
           .attr("dy", "3.5em")
           .style("text-anchor", "end")
           .attr("class", "axislabel")
           .text("Time GMT +01:00");

       axisyP = contextP.append("g")
           .attr("class", "y axis")
           .call(yAxis1P);
       axisy_labelP = axisyP.append("text")
           .attr("transform", "rotate(-90)")
           .attr("y", 0)
           .attr("dy", "2.5em")
           .style("text-anchor", "end")
           .attr("class", "axislabel")
           .text(axisYText);

       if (brushAxis) {
       contextP.append("g")
           .attr("class", "xc brush")
           .call(brush)
           .selectAll("rect")
           .attr("y", -6)
           .attr("height", height + 7);

         }

         var Axes = [];

         if (xGrids) {
         Axes.push(gridxP,gridyP,axisxP,axisyP,axisx_labelP,axisy_labelP);
         } else {
         Axes.push(gridyP,axisxP,axisyP,axisx_labelP,axisy_labelP);
         }


        return Axes;

   }

   function draw (f,contextP,dataStore,svgP,yP,xP) {
   //Tip
           var tip = d3.tip()
               .attr('class', 'd3-tip')
               .offset([-10, 0])
               .html(function(d) {
                   return "<strong style=\"text-transform:capitalize\">" + d.name + " " + ": </strong> <span style='color:red'>" + f((d.y1) - (d.y0)) + "</span>";
               });

           svgP.call(tip);

           barsP = contextP.append("g")
               .attr("class", "bars")
               // a group for each stack of bars, positioned in the correct x position
               .selectAll(".bar.stack")
               .data(dataStore)
               .enter().append("g")
               .attr("class", "bar stack")
               .attr("transform", function(d) {
                   return "translate(" + xP(d.time) + ",0)";
               })
               // a bar for each value in the stack, positioned in the correct y positions
               .selectAll("rect")
               .data(function(d) {
                   return d.counts;
               })
               .enter().append("rect")
               .attr("class", "bar")
               .attr("width", 6)
               .attr("y", function(d) {
                   return yP(d.y1);
               })
               .attr("height", function(d) {
                   return yP(d.y0) - yP(d.y1);
               })
               .on('mouseover', tip.show)
               .on('mouseout', tip.hide)
               .on( 'mouseup',tip.hide )
               .style("fill", function(d) {
                   return colour(d.name);
               });



           var legend = svgP.append("g")
               .attr("class", "legend")
               .attr("height", 100)
               .attr("width", 100)
               .attr('transform', 'translate(-200,50)');

           var legendRect = legend.selectAll('rect').data(legend_colors);

           legendRect.enter()
               .append("rect")
               .attr("y", height - 15)
               .attr("width", 15)
               .attr("height", 15);

           legendRect
               .attr("x", function(d, i) {
                   return i * 100 + 400;
               })
               .style("fill", function(d) {
                   return d[1];
               });

           var legendText = legend.selectAll('text')
               .data(legend_colors);

           legendText.enter()
               .append("text")
               .attr("y", height + 15);

           legendText
               .attr("x", function(d, i) {
                   return i * 100 + 400;
               })
               .style("font-size", "14px")
               .style("fill", "white")
               .text(function(d) {
                   return d[0];
               });

           return barsP;

   }





    function bucket_handler(buckets, total, count, debug) {
        var count0 = {
            name: "inverter",
            y0: 0,
            y1: 0
        };
        var count1 = {
            name: "grid",
            y0: 0,
            y1: 0
        };
        var count2 = {
            name: "generator",
            y0: 0,
            y1: 0
        };
        var count3 = {
            name: "error",
            y0: 0,
            y1: 0
        };
        if (buckets == "error") {
            counts = [count0, count1, count2, count3];
            return counts;
        }
        var ratio = count;
        var next = 0;
        Object.keys(buckets).forEach(function(key) {
            ratio = buckets[key].count / count;
            if (buckets[key].source == "inverter") {
                count0.y0 = next;
                count0.y1 = next = count0.y0 + (total * ratio);
            }
            if (buckets[key].source == "grid") {
                count1.y0 = next;
                count1.y1 = next = count1.y0 + (total * ratio);
            }
            if (buckets[key].source == "generator") {
                count2.y0 = next;
                count2.y1 = next = count2.y0 + (total * ratio);
            }

            if (buckets[key].source == "zero") {
                count3.y0 = next;
                count3.y1 = next = count3.y0 + (total * ratio);
            }
            counts = [count0, count1, count2, count3];

        });
        if (debug > 0) {
            console.log("return", counts);
        }
        //
        return counts;
    }


    var last;
    var bcount;

    function load_data(URL) {
    var target = document.getElementById('graph');
    var targetCost = document.getElementById('cost_graph');

    var spinner = new Spinner(opts).spin(target);
    var spinnerCost = new Spinner(opts).spin(targetCost);
        d3.json(URL, function(error, json) {
            if (error) return console.warn(error);

            plotdata = json.aggregations.power.buckets;
            energydata = json.aggregations.Energy.buckets;
            costdata = json.aggregations.Cost.buckets;
            fueldata = json.aggregations.fuel.buckets;
            fuelRatedata = json.aggregations.fuelRate.buckets;
            batteryCapData = json.aggregations.batteryCap.buckets;
            batteryDOD = json.aggregations.batteryDOD.buckets;

            var t1 = 0;
            var P1 = [];
            var SrcName_Energy_Total = [];
            var SrcName_P1 = [];
            var Energy_total = [];
            var source_types = [];
            var source_names = [];
            var SrcName_energy_data = [];
            var energy_data = [];
            //                var time_data = [];
            var typeColor = ["inverter", "grid", "generator"];
            var SrcByType = [];
            var SrcNameByType = [];

            var costName = json.aggregations.costName.buckets;
            var costType = json.aggregations.costType.buckets;

           function prepareData (data) {
           data.forEach(function(d) {
                 if (d.buckets.length === 0) {
                     console.info("no buckets");
                     d.src = "error";
                     d.counts = bucket_handler(d.src, d.total, 0, 0);
                 } else if (d.buckets.length > 5) {
                     console.error("too many buckets");
                     console.error(d.buckets);
                     d.src = "error";
                 } else {
                     if (d.buckets.length == 1) {
                         src0 = {
                             "source": d.buckets[0].key,
                             "count": d.buckets[0].doc_count
                         };
                         d.src = {
                             "0": src0
                         };
                         bcount = src0.count;
                     } else if (d.buckets.length == 2) {
                         src0 = {
                             "source": d.buckets[0].key,
                             "count": d.buckets[0].doc_count
                         };
                         src1 = {
                             "source": d.buckets[1].key,
                             "count": d.buckets[1].doc_count
                         };
                         bcount = src0.count + src1.count;
                         d.src = {
                             "0": src0,
                             "1": src1
                         };
                         //d.counts = bucket_handler(source,d.total,bcount);
                     } else if (d.buckets.length == 3) {
                         src0 = {
                             "source": d.buckets[0].key,
                             "count": d.buckets[0].doc_count
                         };
                         src1 = {
                             "source": d.buckets[1].key,
                             "count": d.buckets[1].doc_count
                         };
                         src2 = {
                             "source": d.buckets[2].key,
                             "count": d.buckets[2].doc_count
                         };
                         d.src = {
                             "0": src0,
                             "1": src1,
                             "2": src2
                         };
                         bcount = src0.count + src1.count + src2.count;
                         //d.counts = bucket_handler(source,d.total,bcount);;
                     } else if (d.buckets.length == 4) {
                         src0 = {
                             "source": d.buckets[0].key,
                             "count": d.buckets[0].doc_count
                         };
                         src1 = {
                             "source": d.buckets[1].key,
                             "count": d.buckets[1].doc_count
                         };
                         src2 = {
                             "source": d.buckets[2].key,
                             "count": d.buckets[2].doc_count
                         };
                         src3 = {
                             "source": d.buckets[3].key,
                             "count": d.buckets[3].doc_count
                         };
                         d.src = {
                             "0": src0,
                             "1": src1,
                             "2": src2,
                             "3": src3
                         };
                         bcount = src0.count + src1.count + src2.count + src3.count;
                         //d.counts = bucket_handler(source,d.total,bcount);
                     } else if (d.buckets.length == 5) {
                         src0 = {
                             "source": d.buckets[0].key,
                             "count": d.buckets[0].doc_count
                         };
                         src1 = {
                             "source": d.buckets[1].key,
                             "count": d.buckets[1].doc_count
                         };
                         src2 = {
                             "source": d.buckets[2].key,
                             "count": d.buckets[2].doc_count
                         };
                         src3 = {
                             "source": d.buckets[3].key,
                             "count": d.buckets[3].doc_count
                         };
                         src4 = {
                             "source": d.buckets[4].key,
                             "count": d.buckets[4].doc_count
                         };
                         d.src = {
                             "0": src0,
                             "1": src1,
                             "2": src2,
                             "3": src3,
                             "4": src4
                         };
                         bcount = src0.count + src1.count + src2.count + src3.count + src4.count;
                         //d.counts = bucket_handler(source,d.total,bcount);
                     }

                     d.counts = bucket_handler(d.src, d.total, bcount, 0);
                 }
             });
           }

            plotdata.forEach(function(d) {
                d.time = last = parseTimeISO(moment(d.key).format("YYYY-MM-DD HH:mm:ss"));
                d.total = d.Power_real_total.avg;
                d.buckets = d.SourceType.buckets;
                if (d.buckets.length === 0) {
                    console.info("no buckets");
                    d.src = "error";
                    d.counts = bucket_handler(d.src, d.total, 0, 0);
                } else if (d.buckets.length > 5) {
                    console.error("too many buckets");
                    console.error(d.buckets);
                    d.src = "error";
                } else {
                    if (d.buckets.length == 1) {
                        src0 = {
                            "source": d.buckets[0].key,
                            "count": d.buckets[0].doc_count
                        };
                        d.src = {
                            "0": src0
                        };
                        bcount = src0.count;
                    } else if (d.buckets.length == 2) {
                        src0 = {
                            "source": d.buckets[0].key,
                            "count": d.buckets[0].doc_count
                        };
                        src1 = {
                            "source": d.buckets[1].key,
                            "count": d.buckets[1].doc_count
                        };
                        bcount = src0.count + src1.count;
                        d.src = {
                            "0": src0,
                            "1": src1
                        };
                        //d.counts = bucket_handler(source,d.total,bcount);
                    } else if (d.buckets.length == 3) {
                        src0 = {
                            "source": d.buckets[0].key,
                            "count": d.buckets[0].doc_count
                        };
                        src1 = {
                            "source": d.buckets[1].key,
                            "count": d.buckets[1].doc_count
                        };
                        src2 = {
                            "source": d.buckets[2].key,
                            "count": d.buckets[2].doc_count
                        };
                        d.src = {
                            "0": src0,
                            "1": src1,
                            "2": src2
                        };
                        bcount = src0.count + src1.count + src2.count;
                        //d.counts = bucket_handler(source,d.total,bcount);;
                    } else if (d.buckets.length == 4) {
                        src0 = {
                            "source": d.buckets[0].key,
                            "count": d.buckets[0].doc_count
                        };
                        src1 = {
                            "source": d.buckets[1].key,
                            "count": d.buckets[1].doc_count
                        };
                        src2 = {
                            "source": d.buckets[2].key,
                            "count": d.buckets[2].doc_count
                        };
                        src3 = {
                            "source": d.buckets[3].key,
                            "count": d.buckets[3].doc_count
                        };
                        d.src = {
                            "0": src0,
                            "1": src1,
                            "2": src2,
                            "3": src3
                        };
                        bcount = src0.count + src1.count + src2.count + src3.count;
                        //d.counts = bucket_handler(source,d.total,bcount);
                    } else if (d.buckets.length == 5) {
                        src0 = {
                            "source": d.buckets[0].key,
                            "count": d.buckets[0].doc_count
                        };
                        src1 = {
                            "source": d.buckets[1].key,
                            "count": d.buckets[1].doc_count
                        };
                        src2 = {
                            "source": d.buckets[2].key,
                            "count": d.buckets[2].doc_count
                        };
                        src3 = {
                            "source": d.buckets[3].key,
                            "count": d.buckets[3].doc_count
                        };
                        src4 = {
                            "source": d.buckets[4].key,
                            "count": d.buckets[4].doc_count
                        };
                        d.src = {
                            "0": src0,
                            "1": src1,
                            "2": src2,
                            "3": src3,
                            "4": src4
                        };
                        bcount = src0.count + src1.count + src2.count + src3.count + src4.count;
                        //d.counts = bucket_handler(source,d.total,bcount);
                    }

                    d.counts = bucket_handler(d.src, d.total, bcount, 0);
                }

                if (d.buckets.length > 0) {
                    var output = getEnergyData(d, t1, P1, SrcName_Energy_Total, SrcName_P1, Energy_total, source_types, source_names, SrcName_energy_data, energy_data, typeColor, SrcByType, SrcNameByType);

                    t1 = output.t1;
                    P1 = output.P1;
                    SrcName_Energy_Total = output.SrcName_Energy_Total;
                    SrcName_P1 = output.SrcName_P1;
                    Energy_total = output.Energy_total;
                    source_types = output.source_types;
                    source_names = output.source_names;
                    SrcName_energy_data = output.SrcName_energy_data;
                    energy_data = output.energy_data;
                    typeColor = output.typeColor;
                    SrcByType = output.SrcByType;
                    SrcNameByType = output.SrcNameByType;

                }
            });


            //Cost Data
            costdata.forEach(function(d) {
                d.time = last = parseTimeISO(moment(d.key).format("YYYY-MM-DD HH:mm:ss"));
                d.total = d.cost.sum;
                d.buckets = d.SourceType.buckets;
            });


            //Prepare data
            prepareData(costdata);

            var sum = 0;
            fueldata.forEach(function(d) {
                if (d.total !== null) {
                    d.time = last = parseTimeISO(moment(d.key).format("YYYY-MM-DD HH:mm:ss"));
                    sum = sum + d.Fuel.sum;
                    d.total = sum ;
                }
            });

            fuelRatedata.forEach(function(d) {
                if (d.total !== null) {
                    d.time = last = parseTimeISO(moment(d.key).format("YYYY-MM-DD HH:mm:ss"));
                    d.total = d.FuelRate.avg;
                }
            });

            var currentBattery;
            batteryCapData.forEach(function(d) {
               if (d.batteryCapacity.avg !== null) {
                   d.time = last = parseTimeISO(moment(d.key).format("YYYY-MM-DD HH:mm:ss"));
                   d.total = d.batteryCapacity.avg;
                   currentBattery = d.batteryCapacity.avg;
               }else {
               d.time = last = parseTimeISO(moment(d.key).format("YYYY-MM-DD HH:mm:ss"));
               d.total = currentBattery;
               }
           });

           var battDOD;
           batteryDOD.forEach(function(d) {
                 if (d.DOD.avg  !== null) {
                     d.time = last = parseTimeISO(moment(d.key).format("YYYY-MM-DD HH:mm:ss"));
                     d.total = 100 - d.DOD.avg;
                     battDOD = 100 - d.DOD.avg;
                 } else {
                   d.time = last = parseTimeISO(moment(d.key).format("YYYY-MM-DD HH:mm:ss"));
                   d.total = battDOD;
                 }
             });

            costData = getCostData(costType, costName, typeColor);

            var cost_type_data = costData.cost_type_data;
            var cost_name_data = costData.cost_name_data;


            sortedData = sortData(SrcNameByType, SrcByType, SrcName_energy_data, energy_data, SrcName_P1,cost_name_data,cost_type_data, typeColor);

            SrcName_energy_data_sorted = sortedData.SrcName_energy_data_sorted;
            cost_name_data_sorted = sortedData.cost_name_data_sorted;
            cost_type_data_sorted = sortedData.cost_type_data_sorted;
            SrcNameLoad_sorted = sortedData.SrcNameLoad_sorted;


            try {
                try {
                    $("#costPieName").remove();
                    $("#costPieType").remove();
                } catch (err) {
                    console.info("first run no previous pies and donuts");
                }
                var canvasHeight = document.getElementById("costgraph").getAttribute("height");
                Conf.size.canvasHeight = canvasHeight;
                ConfInner.size.canvasHeight = canvasHeight;

                pieSrc6 = createPie(Conf, cost_name_data_sorted, "Cost / Source Type ", "costCharts", "costPieName", "Cost : {value} Naira ");
                document.getElementById("costPieName").setAttribute("viewBox", "0 0 " + String(Conf.size.canvasWidth) + " " + String(Conf.size.canvasHeight));
                document.getElementById("costPieName").setAttribute("perserveAspectRatio", "xMidYMid");
                d3.select("#costPieName").append("svg")
                    .attr("id", "costCharts_svg");

                pieSrc5 = createPie(ConfInner, cost_type_data_sorted, "Cost / Source Type", "costCharts_svg", "costPieType", "Cost : {value} Naira ");

            } catch (err) {
                console.log("Source Type pie chart is not working");
            }
            buckets = json.buckets;

            x.domain(d3.extent(plotdata, function(d) {
                return d.time;
            }));
            y.domain([0, d3.extent(plotdata, function(d) {
                return d.total;
            })[1]]);


            xc.domain(d3.extent(costdata, function(d) {
                return d.time;
            }));
            yc.domain([0, d3.extent(costdata, function(d) {
                return d.total;
            })[1]]);

            xf.domain(d3.extent(fueldata, function(d) {
                return d.time;
            }));
            yf.domain([0, d3.extent(fueldata, function(d) {
                return d.total;
            })[1]]);

            xfr.domain(d3.extent(fuelRatedata, function(d) {
                return d.time;
            }));
            yfr.domain([0, d3.extent(fuelRatedata, function(d) {
                return d.total;
            })[1]]);


            xDOD.domain(d3.extent(batteryDOD, function(d) {
                return d.time;
            }));

            yDOD.domain([
                d3.min(batteryDOD, function(d) {
                    return d.total;
                }),
                d3.max(batteryDOD, function(d) {
                    return d.total;
                })
            ]);

             xb.domain(d3.extent( batteryCapData, function(d) {
                    return d.time;
                }));

            yb.domain([
                d3.min( batteryCapData, function(d) {
                    return d.total;
                }),
                d3.max( batteryCapData, function(d) {
                    return d.total;
                })
            ]);


       costElements = createAxis (1,contextc,xAxisc,yAxis1c,'Cost (N)',width,make_x_axis,1); //Cost
       fuelElements = createAxis (1,contextFuel,xAxisf,yAxis1f,'Fuel (L)',widthFuel,make_x_axisf,0); // fuel
       fuelRateElements = createAxis (1,contextFuelRate,xAxisf,yAxis1fr,'Fuel Rate (L/H)',widthFuel,make_x_axisf,0); // fuel Rate
       batteryCapElements = createAxis (1,contextBattery,xAxisb,yAxis1b,'Battery Capacity (%)',widthFuel,make_x_axisf,0); // Battery Cap
       batteryDODElements = createAxis (1,contextDOD,xAxisf,yAxis1DOD,'Battery DOD (%)',widthFuel,make_x_axisf,0); // Battery DOD


            //fuel drawing

            lined = contextFuel.append('path')
                    .datum(fueldata)
                    .attr("class", "lin")
                    .attr("d", lineFuel);

            linedRate = contextFuelRate.append('path')
                .datum(fuelRatedata)
                .attr("class", "lin")
                .attr("d", lineFuelRate);

             linedBatteryCap = contextBattery.append('path')
                     .datum(batteryCapData)
                     .attr("class", "inv")
                     .attr("d", lineBattery);

              linedBatteryDOD = contextDOD.append('path')
                       .datum(batteryDOD)
                       .attr("class", "inv")
                       .attr("d", lineDOD);


            spinner.stop();
            spinnerCost.stop();
            bars_cost = draw (roundTo,contextc,costdata,svgc,yc,xc);
            //resize();
        });
    }

    //set Summary and set settings window toggle
    $("#displayWindowLink").click(function() {
        $("#displayWindow").slideToggle();
    });

    //bootstrap datetime picker
    var dpStart = "";
    var dpStop = "";
    var dp1 = $('#datetimepicker1').datetimepicker();
    dp1.data("DateTimePicker").maxDate(new Date()).format("DD-MM-YYYY h:mm:ss a");
    var dp2 = $('#datetimepicker2').datetimepicker({
        useCurrent: false //Important! See issue #1075
    });
    dp2.data("DateTimePicker").maxDate(new Date()).format("DD-MM-YYYY h:mm:ss a");
    dp1.on("dp.change", function(e) {
        dpStart = e.date + "";
        dp2.data("DateTimePicker").minDate(e.date);

    });
    dp2.on("dp.change", function(e) {
        dp1.data("DateTimePicker").maxDate(e.date);
        dpStop = e.date + "";
    });

    $("#dPGo").on("click", function() {
        remove_graphElements();
        query(config, probe, "../bounds/" + config + "/" + probe, new Date().getTime() - parseInt(dpStart), dpStop);
    });

    //timeWindow Relative tab
    $("#historyRBtn").click(function() {
        var milSecTime = "";
        var inpt = $('#historyRTxt').val();
        if (inpt < 1) {
            return;
        }
        switch (parseInt($("#historyRS option:selected").val())) {
            case 1:
                milSecTime = inpt * 60 * 1000;
                break;
            case 2:
                milSecTime = parseInt(inpt) * 3600000;
                break;
            case 3:
                milSecTime = inpt * 60 * 60 * 24 * 1000;
                break;
            case 4:
                milSecTime = inpt * 60 * 60 * 24 * 7 * 1000;
                break;
            case 5:
                milSecTime = inpt * 60 * 60 * 24 * 7 * 30 * 1000;
                break;
            case 6:
                milSecTime = inpt * 60 * 60 * 24 * 7 * 30 * 12 * 1000;
                break;
            default:
                milSecTime = inpt * 60 * 60 * 24 * 1000;
        }
        remove_graphElements();
        query(config, probe, "../bounds/" + config + "/" + probe, milSecTime);
    });

    //listens for custom event (HomePageOps.scala)
    $("#quick").on("winChange", function(e, tm, tm2) {
        remove_graphElements();
        if (tm2 !== 'undefined') {
            query(config, probe, "../bounds/" + config + "/" + probe, tm, tm2);
        } else {
            query(config, probe, "../bounds/" + config + "/" + probe, tm);
        }
    });
    var realTimeWindow = $("#realTimeWindow").val(); //get default (in sec)
    $("#rTimeRange").ionRangeSlider({
        force_edges: false,
        values: [
            "30sec", "45sec", "1min", "2min",
            "3min", "5min", "7min",
            "10min", "15min", "20min", "25min", "30min"
        ],
        from: 5,
        onFinish: function(data) {
            chRealTimeWindowSize(data.from);
        }
    });

    function setProbeOnSummary() {
        if (($("#probe option:selected").html()) != 'all probes') {
            $("#probeval").html('of ' + $("#probe option:selected").html() + ' ');
        } else if (($("#probe option:selected").html()) == 'all probes') {
            $("#probeval").html('');
        }
    }

    function setConfOnSummary() {
        $("#configval").html($("#config option:selected").html());
    }


});