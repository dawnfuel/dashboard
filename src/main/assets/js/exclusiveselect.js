$(function(){
    $('select[id^=sourcechannel_]').change(function()
    {
        exclusive_select();
    });
    exclusive_select();
});

function exclusive_select(){
    console.log("exclusive select called");
    // List of ids that are selected in all select elements
    var selected = []; //new Array();

    // Get a list of the ids that are selected
    $('[id^=sourcechannel_] option:selected').each(function()
    {
        selected.push($(this).val());
    });

    // Walk through every select option and enable if not
    // in the list and not already selected
    $('[id^=sourcechannel_] option').each(function()
    {
        if (!$(this).is(':selected') && $(this).val() !== '')
        {
            var shouldDisable = false;
            for (var i = 0; i < selected.length; i++)
            {
                if (selected[i] == $(this).val())
                    shouldDisable = true;
            }

            $(this).css('text-decoration', '');
            $(this).removeAttr('disabled', 'disabled');
            if (shouldDisable)
            {
                $(this).css('text-decoration', 'line-through');
                $(this).attr('disabled', 'disabled');
            }
        }
    });
}