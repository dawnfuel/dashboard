$(document).ready(function() {

    $('.alert').on('close.bs.alert', function(e) {
        e.preventDefault();
        $(this).hide();
    });
    //changing the compress and enlarge icons
    $("a i").click(function() {
        if ($(this).hasClass('fa-compress')) {

            $(this).removeClass('fa-compress');
            $(this).addClass('fa-expand');
        } else if ($(this).hasClass('fa-expand')) {
            $(this).removeClass('fa-expand');
            $(this).addClass('fa-compress');
        }
    });


    var margin = {
            top: 10,
            right: 40,
            bottom: 50,
            left: 60
        }, //margin2 = {top: 430, right: 80, bottom: 30, left: 40},
        width = parseInt(d3.select('#allpower_live').style('width'), 10),
        width2 = parseInt(d3.select('#CurrentPhaseGraph').style('width'), 10);
        height = 500 - margin.top - margin.bottom;
    var margin2 = {
        top: 10,
        right: 80,
        bottom: 70,
        left: 10
    }; //margin2 = {top: 430, right: 80, bottom: 30, left: 40};

    var config = document.getElementById('config').options[0].value;
    var probe = document.getElementById('probe').options[0].value;
    var lastTransmit = 0;

    $("#probe").on('change', function() {
        var timeWindow = $('#hiddenTimeWindow').val();
        probe = $("#probe").val();
        config = document.getElementById('config').options[0].value;
        remove_livegraphElements();
        query(config, probe, "../bounds/" + config + "/" + probe, timeWindow);
        setProbeOnSummary();
    });

    $("#config").change(function() {
        var timeWindow = $('#hiddenTimeWindow').val();
        config = $("#config").val();
        probe = document.getElementById('probe').options[0].value;
        remove_livegraphElements();
        query(config, probe, "../bounds/" + config + "/" + probe, timeWindow);
        setConfOnSummary();
    });
    query(config, probe, "../bounds/" + config + "/" + probe, $('#hiddenTimeWindow').val());
    setProbeOnSummary();
    setConfOnSummary();

    var export_start = "";
    var export_stop = "";
    $("#export").click(function() {
        console.log("data period");
        console.log(export_start + " ============== " + export_stop);
        console.log("export button click");
        console.log(new Date(export_start));
        console.log(new Date(export_stop));

        var start_time = new Date(export_start);
        var stop_time = new Date(export_stop);

        var tz = start_time.getTimezoneOffset();
        console.log("timezone is " + tz);

        console.log("start_time: " + start_time);
        console.log("stop_time: " + stop_time);

        var year_start = new Date(new Date(new Date(start_time.getTime()).setMonth(0)).setDate(1));
        var year_end = new Date(new Date(new Date(start_time.getTime()).setMonth(11)).setDate(31));

        console.log("year_start: " + year_start);
        console.log("year_end: " + year_end);

//        console.log(day_start);
//        console.log(day_end);

//        var front_padding_hours = Math.abs(Math.floor(year_start-start_time)/3600000);
//        var end_padding_hours = Math.abs(Math.floor(stop_time-year_end)/3600000);
//
//        console.log("front_padding_hours: " + front_padding_hours);
//        console.log("end_padding_hours: " + end_padding_hours);

//        var elasticsearch = require('elasticsearch');
        var client = elasticsearch.Client({
          host: 'localhost:9200',
          log: 'trace'
        });



        client.search({
          index: 'grit',
          type: 'docs',
          body: {
                   "query":{
                              "bool":{
                                "must":[
                                  {
                                    "match": {
                                      "ProbeID_FK": "565DAD26B5EC1977E63B48EBSYSTEM01"

                                    }
                                  },
                                  {
                                    "range": {
                                      "Time": {
                                        "gte": export_start,
                                        "lte": export_stop,
                                        "format": "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
                                      }
                                    }
                                  }
                                ]

                              }
                            },
                            "aggs" : {

                        "energies_over_time" : {
                            "date_histogram" : {
                                "field" : "Time",
                                "interval" : "1h",
                                "format" : "yyyy-MM-dd'T'HH:mm:ss.SSSZ",
                                "time_zone": "+01:00"
                            },
                            "aggs" : {
                                "avg_energy" : { "avg" : { "field" : "Energy_since_last" } }
                            }
                        }

                    },
                   "sort" : [
                        { "Time" : {"order" : "asc"}}
                    ]


                }
        }).then(function (resp) {
            var energy_time_series ="";
            var counter = 0;

//            var hits = resp.hits.hits;
//            console.log(hits);
            var aggs = resp.aggregations.energies_over_time.buckets;
            console.log("++++++++++++++++++ aggregations ++++++++++++++++++++++");
            console.log(aggs[0]);
            console.log("show data");
            start_time = new Date(aggs[0].key);
            stop_time = new Date(aggs[aggs.length-1].key);
            console.log("start_time: " + start_time);
            console.log("stop_time: " + stop_time);

            var front_padding_hours = Math.floor(Math.abs(year_start-start_time)/3600000.0);
            var end_padding_hours = Math.floor(Math.abs(stop_time-year_end)/3600000.0);

            console.log("front_padding_hours: " + front_padding_hours);
            console.log("end_padding_hours: " + end_padding_hours);


            for(hr = 0; hr < front_padding_hours; hr++){
                energy_time_series += "0\n";
                counter += 1;
            }
            aggs.forEach(function(e){
//                energy_time_series += e.avg_energy.value.toString + "\n";
                if(e.avg_energy.value === null){
                    energy_time_series += "0\n";
                    counter += 1;
//                    console.log(0);
                }
                else{
                    energy_time_series += e.avg_energy.value.toString() + "\n";
                    counter += 1;
//                    console.log(e.avg_energy.value.toString());
                }
            });

            for(hr = 0; hr < end_padding_hours; hr++){
                energy_time_series += "0\n";
                counter += 1;
            }

            console.log("total counter: " + counter);

            var a = document.body.appendChild(
                    document.createElement("a")
                );
            a.download = "energy_data.txt";
            a.href = "data:text/plain;base64," + btoa(energy_time_series);
            a.click();
//            hits.forEach(function(hit){
//            console.log("printing time window");
//            console.log($('#hiddenTimeWindow').val());
//            console.log(hit._source.SourceName);

//            });
        }, function (err) {
            console.log("error found");
            console.trace(err.message);
        });
    });


    var DateDiff = {

        inSeconds: function(d1, d2) {
            var t2 = d2.getTime();
            var t1 = d1.getTime();
            return parseInt((t2 - t1) / (1000));
        },
        inMinutes: function(d1, d2) {
            var t2 = d2.getTime();
            var t1 = d1.getTime();
            return parseInt((t2 - t1) / (60 * 1000));
        },

        inHours: function(d1, d2) {
            var t2 = d2.getTime();
            var t1 = d1.getTime();
            return parseInt((t2 - t1) / (3600 * 1000));
        },

        inDays: function(d1, d2) {
            var t2 = d2.getTime();
            var t1 = d1.getTime();

            return parseInt((t2 - t1) / (24 * 3600 * 1000));
        },

        inWeeks: function(d1, d2) {
            var t2 = d2.getTime();
            var t1 = d1.getTime();

            return parseInt((t2 - t1) / (24 * 3600 * 1000 * 7));
        },

        inMonths: function(d1, d2) {
            var d1Y = d1.getFullYear();
            var d2Y = d2.getFullYear();
            var d1M = d1.getMonth();
            var d2M = d2.getMonth();

            return (d2M + 12 * d2Y) - (d1M + 12 * d1Y);
        },

        inYears: function(d1, d2) {
            return d2.getFullYear() - d1.getFullYear();
        }
    };

    function getDateDiff(d1, d2) {

        if (DateDiff.inSeconds(d1, d2) < 60) {
            if (DateDiff.inSeconds(d1, d2) === 1) {
                return DateDiff.inSeconds(d1, d2) + ' second';
            }
            return DateDiff.inSeconds(d1, d2) + ' seconds';
        } else if (DateDiff.inMinutes(d1, d2) < 60) {
            if (DateDiff.inMinutes(d1, d2) === 1) {
                return DateDiff.inMinutes(d1, d2) + ' minute';
            }
            return DateDiff.inMinutes(d1, d2) + ' minutes';
        } else if (DateDiff.inHours(d1, d2) < 24) {
            if (DateDiff.inHours(d1, d2) === 1) {
                return DateDiff.inHours(d1, d2) + ' hour';
            }
            return DateDiff.inHours(d1, d2) + ' hours';
        } else if (DateDiff.inDays(d1, d2) < 7) {

            if (DateDiff.inDays(d1, d2) === 1) {
                return DateDiff.inDays(d1, d2) + ' day';
            }
            return DateDiff.inDays(d1, d2) + ' days';
        } else if (DateDiff.inWeeks(d1, d2) < 4) {
            if (DateDiff.inWeeks(d1, d2) === 1) {
                return DateDiff.inWeeks(d1, d2) + ' week';
            }
            return DateDiff.inWeeks(d1, d2) + ' weeks';
        } else if (DateDiff.inMonths(d1, d2) < 11) {
            if (DateDiff.inMonths(d1, d2) === 1) {
                return DateDiff.inMonths(d1, d2) + ' month';
            }
            return DateDiff.inMonths(d1, d2) + ' months';
        } else if (DateDiff.inYears(d1, d2) > 0) {
            if (DateDiff.inYears(d1, d2) === 1) {
                return DateDiff.inYears(d1, d2) + ' year';
            }
            return DateDiff.inYears(d1, d2) + ' years';
        } else {
            return 'n/a ';
        }

    }


    function getDateDiff2(d1, d2) {

            if (DateDiff.inDays(d1, d2) < 7) {
                if (DateDiff.inDays(d1, d2) <= 1) {
                    return (moment(d1).format("MMM Do"));
                }
               return ('from ' + moment(d1).format("MMM Do") + ' to ' + moment(d2).format("MMM Do"));
            } else if (DateDiff.inWeeks(d1, d2) < 4) {

                return ('from ' + moment(d1).format("MMM Do") + ' to ' + moment(d2).format("MMM Do"));
            } else if (DateDiff.inMonths(d1, d2) < 11) {
                return ('from ' + moment(d1).format("MMM Do") + ' to ' + moment(d2).format("MMM Do"));
            } else if (DateDiff.inYears(d1, d2) > 0) {
                if (moment(d1).format("YYYY") === moment(d2).format("YYYY")) {
                    return (moment(d1).format("YYYY"));
                }
                return ('from ' + moment(d1).format("YYYY") + ' to ' + moment(d2).format("YYYY"));
            } else {
                return 'n/a ';
            }

        }



    function query(config, probe, url, TimeWindow, user_stop) {
        var windowSizeMsg = "";
        $('#hiddenTimeWindow').val(TimeWindow); //set window to a hidden field so it can be accessible by other functions
        $.get(url, function(data) {
            console.log(data);
            data_stop_realtime = data.stop; // most recent value returned from snippet

            data_stop = typeof user_stop !== 'undefined' ? user_stop : data_stop_realtime;
            data_stop = moment(parseInt(data_stop)).format("YYYY-MM-DDTHH:mm:ss.SSSZ");
            //dp2.data("DateTimePicker").date(moment(data_stop)); //set dateTimePicker2 time
            // tz awareness pending
            var user_start = new Date().getTime() - parseFloat(TimeWindow);
            var data_start = "";
            var popMSg;


            if (user_start < data.start) {
                data_start = moment(data.start).format("YYYY-MM-DDTHH:mm:ss.SSSZ");
                //dp1.data("DateTimePicker").date(moment(data.start)); //set dateTimePicker1 time
                popMSg = "from  " + moment(data.start).format("DD-MM-YYYY HH:mm:ss.SSSZ");

            } else {
                data_start = moment(user_start).format("YYYY-MM-DDTHH:mm:ss.SSSZ");
                data_start = moment(user_start).format("YYYY-MM-DDTHH:mm:ss.SSSZ");
                //dp1.data("DateTimePicker").date(moment(user_start)); //set dateTimePicker1 time
                popMSg = "from   " + moment(user_start).format("DD-MM-YYYY HH:mm:ss.SSSZ");
            }



            if (data_start > data_stop) {

                if (sessionStorage.length > 0) {
                    $("#timeWindowSpan").html(sessionStorage.windowSizeMsg);
                    $("#timeWindowSpan").attr("title", sessionStorage.popMsg);
                    load_recent("../recent/" + config + "/" + probe + "/" + moment(parseInt(sessionStorage.data_stop_realtime)).format("YYYY-MM-DDTHH:mm:ss.SSSZ") + "/" + sessionStorage.realTimeWindow);
                    lastTransmit = sessionStorage.data_stop;
                }
                return closeAlert();

            }

            var timeCheck = Date.now() - 120000 ;
            timeCheck = moment(parseInt(timeCheck)).format("YYYY-MM-DDTHH:mm:ss.SSSZ");
            //windowSizeMsg = 'from Last  '+ getDateDiff (new Date(moment(data_start).format("MM-DD-YYYY HH:mm:ss.SSSZ")), new Date (moment(data_stop).format("MM-DD-YYYY HH:mm:ss.SSSZ")));
            if (data_stop > timeCheck ) {

            windowSizeMsg = 'last ' + getDateDiff(new Date(data_start), new Date(data_stop));
            popMSg += "   to  " + moment(data_stop).format("DD-MM-YYYY HH:mm:ss.SSSZ");

            }else {
            windowSizeMsg = getDateDiff2(new Date(data_start), new Date(data_stop));
            popMSg += "   to  " + moment(data_stop).format("DD-MM-YYYY HH:mm:ss.SSSZ");

            }

            export_start = data_start;
            export_stop = data_stop;


            $("#timeWindowSpan").html(windowSizeMsg);
            $("#timeWindowSpan").attr("title", popMSg);
            load_recent("../recent/" + config + "/" + probe + "/" + moment(parseInt(data_stop_realtime)).format("YYYY-MM-DDTHH:mm:ss.SSSZ") + "/" + realTimeWindow);
            lastTransmit = data.stop;

            //sessions
            sessionStorage.popMsg = popMSg;
            sessionStorage.windowSizeMsg = windowSizeMsg;
            sessionStorage.data_stop_realtime = data_stop_realtime;
            sessionStorage.data_start = data_start;
            sessionStorage.data_stop = data_stop;
            sessionStorage.realTimeWindow = realTimeWindow;
        });
    }


     var linedVoltage = [];//Voltage
     var linedCurrent = [];//Current
     var linedpf = []; //pf

     var pathReal;
     var pathReactive;
     var pathApparent;

/*     var opts = {
       lines: 13, // The number of lines to draw
       length: 20, // The length of each line
       width: 5, // The line thickness
       radius: 30, // The radius of the inner circle
       scale: 10, // Scales overall size of the spinner
       color: '#FFFFFF', // #rgb or #rrggbb or array of colors
       speed: 1.9, // Rounds per second
       trail: 40, // Afterglow percentage
       className: 'spinner', // The CSS class to assign to the spinner
     };*/

    var parseTimeISO = d3.time.format("%Y-%m-%d %H:%M:%S").parse;

    //Power axes
    var x = d3.time.scale().range([0, width]);
    var y = d3.scale.linear().range([height, 0]);


    //Voltage axes
    var xv = d3.time.scale().range([0, width2]);
    var yv = d3.scale.linear().range([height, 0]);
    var svgVoltage = d3.select("#VoltagePhaseGraph").append("svg")
        .attr("id", "voltagegraph")
        .attr("viewBox", String(0) + " " + String(0) + " " + String(width2 + margin.left + margin.right) + " " + String(height + margin.top + margin.bottom))
        .attr("perserveAspectRatio", "xMinYMid")
        .attr("width", width2 + margin.left + margin.right)
        .attr("height", height + margin.top + margin.bottom)
        .append("g")
        .attr("transform", "translate(" + margin.left + "," + margin.top + ")");
    var contextVoltage = svgVoltage.append("g");
        /*.attr("id", "context")
        .attr("class", "context");*/
    var xAxisv = d3.svg.axis()
        .scale(xv)
        .orient("bottom")
        .ticks(d3.time.second, 30);
    var yAxis1v = d3.svg.axis()
        .scale(yv)
        .orient("left"); //end
    var lineVoltage = d3.svg.line()
        .x(function(d) {
            return xv(d.time);
        })
        .y(function(d) {
            return yv(d.voltage);
        })
        .interpolate("basis");

    //current axes
    var xcurrent = d3.time.scale().range([0, width2]);
    var ycurrent = d3.scale.linear().range([height, 0]);
    var svgCurrent = d3.select("#CurrentPhaseGraph").append("svg")
        .attr("id", "currentgraph")
        .attr("viewBox", String(0) + " " + String(0) + " " + String(width2 + margin.left + margin.right) + " " + String(height + margin.top + margin.bottom))
        .attr("perserveAspectRatio", "xMinYMid")
        .attr("width", width2 + margin.left + margin.right)
        .attr("height", height + margin.top + margin.bottom)
        .append("g")
        .attr("transform", "translate(" + margin.left + "," + margin.top + ")");
    var contextCurrent = svgCurrent.append("g")
        .attr("id", "context")
        .attr("class", "context");
    var xAxiscurrent = d3.svg.axis()
        .scale(xcurrent)
        .orient("bottom")
        .ticks(d3.time.second, 30);
    var yAxis1Current = d3.svg.axis()
        .scale(ycurrent)
        .orient("left"); //end
    var lineCurrent = d3.svg.line()
        .x(function(d) {
            return xv(d.time);
        })
        .y(function(d) {
            return ycurrent(d.current);
        })
        .interpolate("basis");

    //pf axes
    var ypf = d3.scale.linear().range([height, 0]);
    var svgpf = d3.select("#pfPhaseGraph").append("svg")
        .attr("id", "pfgraph")
        .attr("viewBox", String(0) + " " + String(0) + " " + String(width2 + margin.left + margin.right) + " " + String(height + margin.top + margin.bottom))
        .attr("perserveAspectRatio", "xMinYMid")
        .attr("width", width2 + margin.left + margin.right)
        .attr("height", height + margin.top + margin.bottom)
        .append("g")
        .attr("transform", "translate(" + margin.left + "," + margin.top + ")");
    var contextpf = svgpf.append("g")
        .attr("id", "context")
        .attr("class", "context");
    var yAxis1pf = d3.svg.axis()
        .scale(ypf)
        .orient("left"); //end
    var linepf = d3.svg.line()
        .x(function(d) {
            return xv(d.time);
        })
        .y(function(d) {
            return ypf(d.pf);
        })
        .interpolate("basis");


    //realtime
    var xl = d3.time.scale().range([0, width]);
    yl = d3.scale.linear().range([height, 0]);
    var ylApparent = d3.scale.linear().range([height, 0]);
    var ylReactive = d3.scale.linear().range([height, 0]);

    var xAxis_realtime = d3.svg.axis()
        .scale(xl)
        .orient("bottom");
    //.tickFormat(d3.time.format("%H:%M:%S"));

    var yAxis_realtime = d3.svg.axis()
        .scale(yl)
        .orient("left");

    var yAxis_realtimeApparent = d3.svg.axis()
        .scale(ylApparent)
        .orient("left");

    var yAxis_realtimeReactive = d3.svg.axis()
            .scale(ylReactive)
            .orient("left");


     var line = line_ (xl,yl,'');
     var lineRealApparent = line_ (xl,ylApparent,'apparent');
     var lineRealReactive = line_ (xl, ylReactive,'reactive');


        function line_ (x_,y_,d_) {

        realLine_ = d3.svg.line()
        .x(function(d) {
            return x_(d.time);
        })
        .y(function(d) {
        if (d_ == 'reactive'){
         return y_(d.reactive);
        }else if(d_  == 'apparent'){
         return y_(d.apparent);
        } else {
        return y_(d.total);
        }
        });



        return  realLine_;
        }


    var svg_All = d3.select("#allpower_live").append("svg")
        .attr("id", "realtimegraph")
        .attr("viewBox", String(0) + " " + String(0) + " " + String(width + margin.left + margin.right) + " " + String(height + margin.top + margin.bottom))
        .attr("width", width + margin.left + margin.right)
        .attr("height", height + margin.top + margin.bottom)
        .append("g")
        .attr("transform", "translate(" + margin2.left + "," + margin2.top + ")");

    var focusAll = svg_All.append("g")
        .attr("id", "focus")
        .attr("class", "focus")
        .attr("transform", "translate(" + margin.left + "," + margin.top + ")");

    function make_x_axis() {
        return d3.svg.axis()
            .scale(x)
            .orient("bottom")
            .ticks(10);
    }

    function make_y_axis() {
        return d3.svg.axis()
            .scale(y)
            .orient("left")
            .ticks(4);
    }

/*    function roundTo(num) {
        return +(Math.round(num + "e+2") + "e-2");
    }*/

/*    function powerGraphScale(power) {
        if (+power >= 1000) {
            var scaledPower = (power / 1000);
            return roundTo(scaledPower) + "KW";
        } else {
            return roundTo(power) + "W";
        }
    }*/


    function remove_livegraphElements() {
        try {
            axislAllx.remove();
            gridlAllx.remove();
            gridlAlly.remove();
            pathReal.remove();
            pathApparent.remove();
            pathReactive.remove();
            voltageElements.forEach(function (d){d.remove();});
            currentElements.forEach(function (d){d.remove();});
            pfElements.forEach(function (d){d.remove();});
            linedCurrent[0].remove();
            linedCurrent[1].remove();
            linedCurrent[2].remove();
            linedVoltage[0].remove();
            linedVoltage[1].remove();
            linedVoltage[2].remove();
            linedpf[0].remove();
            linedpf[1].remove();
            linedpf[2].remove();
        } catch (err) {
            console.log(err.message);
        }
    }


    function createAxis (xGrids,contextP,xAxisP,yAxis1P,axisYText,f,fx,brushAxis) {

     if (xGrids) {
       gridxP = contextP.append("g")
           .attr("class", "grid")
           .attr("transform", "translate(0," + height + ")")
           .call(fx()
               .tickSize(-height, 0, 0)
               .tickFormat("")
           );
          }

       gridyP = contextP.append("g")
           .attr("class", "grid")
           .call(make_y_axis("left")
               .tickSize(-f, 0, 0)
               .tickFormat("")
           );

       axisxP = contextP.append("g")
           .attr("class", "x axis")
           .attr("transform", "translate(0," + height + ")")
           .call(xAxisP);

       axisx_labelP = axisxP.append("text")
           .attr("x", 0)
           .attr("dx", "3.5em")
           .attr("dy", "3.5em")
           .style("text-anchor", "end")
           .attr("class", "axislabel")
           .text("Time GMT +01:00");

       axisyP = contextP.append("g")
           .attr("class", "y axis")
           .call(yAxis1P);
       axisy_labelP = axisyP.append("text")
           .attr("transform", "rotate(-90)")
           .attr("y", 0)
           .attr("dy", "2.5em")
           .style("text-anchor", "end")
           .attr("class", "axislabel")
           .text(axisYText);

       if (brushAxis) {
       contextP.append("g")
           .attr("class", "xc brush")
           .call(brush)
           .selectAll("rect")
           .attr("y", -6)
           .attr("height", height + 7);

         }

         var Axes = [];

         if (xGrids) {
         Axes.push(gridxP,gridyP,axisxP,axisyP,axisx_labelP,axisy_labelP);
         } else {
         Axes.push(gridyP,axisxP,axisyP,axisx_labelP,axisy_labelP);
         }


        return Axes;

   }

    var plotdata_recent;
    var elem = document.getElementById("realtime_stuff");
    /* var graph = document.getElementById("real");*/
    var voltage1Data = [];
    var voltage2Data = [];
    var voltage3Data = [];
    var current1Data = [];
    var current2Data = [];
    var current3Data = [];
    var pf1Data = [];
    var pf2Data = [];
    var pf3Data = [];
    var voltages = [];
    var currentData = [];
    var pfData = [];


    function eventHandler(e) {
        plotdata_recent.push({
            time: parseTimeISO(moment(e.detail.Time).format("YYYY-MM-DD HH:mm:ss")),
            total: e.detail.Power_real_total,
            apparent : e.detail.Power_apparent_total,
            reactive : e.detail.Power_reactive_total,
            sourceType: e.detail.SourceType
        });

            //voltage
                voltage1Data.push({
                    "time":  parseTimeISO(moment(e.detail.Time).format("YYYY-MM-DD HH:mm:ss")),
                    "voltage": e.detail.Voltage1,
                    "sourceType": e.detail.SourceType
                });
                voltage2Data.push({
                    "time":  parseTimeISO(moment(e.detail.Time).format("YYYY-MM-DD HH:mm:ss")),
                    "voltage": e.detail.Voltage2,
                    "sourceType": e.detail.SourceType
                });
                voltage3Data.push({
                    "time":  parseTimeISO(moment(e.detail.Time).format("YYYY-MM-DD HH:mm:ss")),
                    "voltage": e.detail.Voltage3,
                    "sourceType":  e.detail.SourceType,
                });

            //current
                current1Data.push({
                    "time":  parseTimeISO(moment(e.detail.Time).format("YYYY-MM-DD HH:mm:ss")),
                    "current":  e.detail.Current1,
                    "sourceType":  e.detail.SourceType
                });
                current2Data.push({
                    "time":  parseTimeISO(moment(e.detail.Time).format("YYYY-MM-DD HH:mm:ss")),
                    "current":  e.detail.Current2,
                    "sourceType":  e.detail.SourceType
                });
                current3Data.push({
                    "time":  parseTimeISO(moment(e.detail.Time).format("YYYY-MM-DD HH:mm:ss")),
                    "current":  e.detail.Current3,
                    "sourceType": e.detail.SourceType,

            //pf
                });
                pf1Data.push({
                    "time":  parseTimeISO(moment(e.detail.Time).format("YYYY-MM-DD HH:mm:ss")),
                    "pf": e.detail.PowerFactor1,
                    "sourceType":  e.detail.SourceType
                });
                pf2Data.push({
                    "time": parseTimeISO(moment(e.detail.Time).format("YYYY-MM-DD HH:mm:ss")),
                    "pf":  e.detail.PowerFactor2,
                    "sourceType":  e.detail.SourceType
                });
                pf3Data.push({
                    "time":  parseTimeISO(moment(e.detail.Time).format("YYYY-MM-DD HH:mm:ss")),
                    "pf": e.detail.PowerFactor3,
                    "sourceType": e.detail.SourceType,
                });


               /* currentData.push(current1Data,current2Data, current3Data);
                               pfData.push(pf1Data,pf2Data,pf3Data);
               */
               axislAllx
                   .transition()
                   //              .duration(500)
                   //              .ease("linear")
                   //.call(d3.svg.axis().scale(x).orient("bottom").tickFormat(d3.time.format("%H:%M:%S"))) // 24 hour format
                   .call(d3.svg.axis().scale(xl).orient("bottom"));


               currentElements[1]
                   .transition()
                   .call(d3.svg.axis().scale(xcurrent).orient("bottom"));

               voltageElements[1]
                   .transition()
                   .call(d3.svg.axis().scale(xv).orient("bottom"));

               pfElements[1]
                   .transition()
                   .call(d3.svg.axis().scale(xv).orient("bottom"));

/*
               axisly.transition().duration(1500).ease("sin-in-out")
                   .call(yAxis_realtime);*/


               currentElements[2].transition().duration(1500).ease("sin-in-out")
                   .call(yAxis1Current);

               voltageElements[2].transition().duration(1500).ease("sin-in-out")
                   .call(yAxis1v);

               pfElements[2].transition().duration(1500).ease("sin-in-out")
                   .call(yAxis1pf);


               // pop the old data point off the front
               plotdata_recent.shift();
               voltage1Data.shift();
               voltage2Data.shift();
               voltage3Data.shift();
               current1Data.shift();
               current2Data.shift();
               current3Data.shift();
               pf1Data.shift();
               pf2Data.shift();
               pf3Data.shift();

               voltages = voltage1Data.concat(voltage2Data, voltage3Data);
               currentData = current1Data.concat(current2Data, current3Data);
               pfData = pf1Data.concat(pf2Data, pf3Data);

               xl.domain(d3.extent(plotdata_recent, function(d) {
                   return d.time;
               }));

               xv.domain(d3.extent(voltages, function(d) {
                   return d.time;
               }));

               xcurrent.domain(d3.extent(currentData, function(d) {
                   return d.time;
               }));

               yl.domain([0, d3.extent(plotdata_recent, function(d) {
                   return d.total;
               })[1]]);

               ylApparent.domain([0, d3.extent(plotdata_recent, function(d) {
                   return d.apparent;
               })[1]]);

               ylReactive.domain([0, d3.extent(plotdata_recent, function(d) {
                   return d.reactive;
               })[1]]);

               yv.domain([
                   d3.min(voltages, function(d) {
                       return d.voltage;
                   }),
                   d3.max(voltages, function(d) {
                       return d.voltage;
                   })
               ]);

               ycurrent.domain([0, d3.extent(currentData, function(d) {
                   return d.current;
               })[1]]);

               ypf.domain([0, d3.extent(pfData, function(d) {
                   return d.pf;
               })[1]]);


        // transition the line
        pathReal.transition().attr("d", line);
        pathApparent.transition().attr("d", lineRealApparent);
        pathReactive.transition().attr("d", lineRealReactive);


       linedVoltage[0].transition().attr("d", lineVoltage);
       linedVoltage[1].transition().attr("d", lineVoltage);
       linedVoltage[2].transition().attr("d", lineVoltage);

       linedCurrent[0].transition().attr("d", lineCurrent);
       linedCurrent[1].transition().attr("d", lineCurrent);
       linedCurrent[2].transition().attr("d", lineCurrent);

       linedpf[0].transition().attr("d", linepf);
       linedpf[1].transition().attr("d", linepf);
       linedpf[2].transition().attr("d", linepf);

    }
    // Listen for the event.
    elem.addEventListener('build', function(e) {
        eventHandler(e);
    }, false);
    /* graph.addEventListener('build', function (e) { eventHandler(e); }, false);*/

    function load_recent(URL) {
        d3.json(URL, function(error, json) {
            if (error) return console.warn(error);
            plotdata_recent = json.hits.hits;
            PhaseData = json.hits.hits;


            plotdata_recent.forEach(function(d) {
                d.time = parseTimeISO(moment(d._source.Time).format("YYYY-MM-DD HH:mm:ss"));
                d.total = d._source.Power_real_total;
                d.apparent = d._source.Power_apparent_total;
                d.reactive = d._source.Power_reactive_total;
                d.sourceType = d._source.SourceType;
                d.sourceName = d._source.SourceName;
            });
            xl.domain(d3.extent(plotdata_recent, function(d) {
                return d.time;
            }));
            yl.domain([0, d3.extent(plotdata_recent, function(d) {
                return d.total;
            })[1]]);
            // yl.domain(d3.extent(plotdata_recent, function(d) { return d.total; }));

            ylApparent.domain([0, d3.extent(plotdata_recent, function(d) {
                return d.apparent;
            })[1]]);

            ylReactive.domain([0, d3.extent(plotdata_recent, function(d) {
                return d.reactive;
            })[1]]);


            gridlAllx = focusAll.append("g")
                .attr("class", "grid")
                .attr("transform", "translate(0," + height + ")")
                .call(make_x_axis()
                    .tickSize(-height, 0, 0)
                    .tickFormat("")
                );

            gridlAlly = focusAll.append("g")
                .attr("class", "grid")
                .call(make_y_axis("left")
                    .tickSize(-width, 0, 0)
                    .tickFormat("")
                );

            axislAllx = focusAll.append("g")
                .attr("class", "x axis")
                .attr("transform", "translate(0," + height + ")")
                .call(xAxis_realtime);

            axislAllx_label = axislAllx.append("text")
                .attr("x", 0)
                .attr("dx", "3.5em")
                .attr("dy", "3.5em")
                .style("text-anchor", "end")
                .attr("class", "axislabel")
                .text("Time GMT +01:00");


             function axis_(yAxis_,text_) {
                axisName = focusAll.append("g")
                .attr("class", "y axis")
                .call(yAxis_);

                axisly_label = axisName.append("text")
                .attr("transform", "rotate(-90)")
                .attr("y", 0)
                .attr("dy", "-5em")
                .style("text-anchor", "end")
                .attr("class", "axislabel")
                .text(text_);
                 return axisName;
                 }

           pathReal =  focusAll.append("path")
             .datum(plotdata_recent)
             .attr("class", "lineNew")
             .attr('stroke', 'blue')
             .attr('stroke-width', 2)
             .attr('fill', 'none')
             .attr("d", line)
             .on ("mouseover",function () {axislyReal=axis_(yAxis_realtime,"Power (W)");})
             .on("mouseout",function () {axislyReal.remove(); });

           pathApparent =  focusAll.append("path")
             .datum(plotdata_recent)
             .attr('stroke', 'yellow')
              .attr('stroke-width', 2)
              .attr('fill', 'none')
             .attr("d", lineRealApparent)
             .on ("mouseover",function () {axislyApparent=axis_(yAxis_realtimeApparent,"Apparent Power (VA)");})
             .on("mouseout",function () {axislyApparent.remove(); });

           pathReactive = focusAll.append("path")
             .datum(plotdata_recent)
             .attr('stroke', 'red')
              .attr('stroke-width', 2)
              .attr('fill', 'none')
             .attr("d", lineRealReactive)
              .on ("mouseover",function () {axislyReactive=axis_(yAxis_realtimeReactive,"Reactive Power (VAR)");})
              .on("mouseout",function () {axislyReactive.remove(); });

                 var powerColors = [
                      ["Reactive", "red"],
                      ["Apparent", "yellow"],
                      ["Real", "blue"]
                  ];

               var phaselegend = svg_All.append("g")
                    .attr("class", "legend")
                    .attr("height", 100)
                    .attr("width", 100)
                    .attr('transform', 'translate(-70,50)');

                var legendRectPhase = phaselegend.selectAll('rect').data(powerColors);

                legendRectPhase.enter()
                    .append("rect")
                    .attr("y", height - 12)
                    .attr("width", 10)
                    .attr("height", 10);

                legendRectPhase
                    .attr("x", function(d, i) {
                        return i * 100 + 400;
                    })
                    .style("fill", function(d) {
                        return d[1];
                    });

                var legendTextPhase = phaselegend.selectAll('text')
                    .data(powerColors);

                legendTextPhase.enter()
                    .append("text")
                    .attr("y", height + 15);

                legendTextPhase
                    .attr("x", function(d, i) {
                        return i * 100 + 400;
                    })
                    .style("font-size", "12px")
                    .style("fill", "white")
                    .text(function(d) {
                        return d[0];
                    });


           voltage1Data = [];
           voltage2Data = [];
           voltage3Data = [];
           current1Data = [];
           current2Data = [];
           current3Data = [];
           pf1Data =[];
           pf2Data = [];
           pf3Data = [];

            PhaseData.forEach(function(d) {
            //voltage
                voltage1Data.push({
                    "time": parseTimeISO(moment(d._source.Time).format("YYYY-MM-DD HH:mm:ss")),
                    "voltage": d._source.Voltage1,
                    "sourceType": d._source.SourceType
                });
                voltage2Data.push({
                    "time": parseTimeISO(moment(d._source.Time).format("YYYY-MM-DD HH:mm:ss")),
                    "voltage": d._source.Voltage2,
                    "sourceType": d._source.SourceType
                });
                voltage3Data.push({
                    "time": parseTimeISO(moment(d._source.Time).format("YYYY-MM-DD HH:mm:ss")),
                    "voltage": d._source.Voltage3,
                    "sourceType": d._source.SourceType,
                });

            //current
                current1Data.push({
                    "time": parseTimeISO(moment(d._source.Time).format("YYYY-MM-DD HH:mm:ss")),
                    "current": d._source.Current1,
                    "sourceType": d._source.SourceType
                });
                current2Data.push({
                    "time": parseTimeISO(moment(d._source.Time).format("YYYY-MM-DD HH:mm:ss")),
                    "current": d._source.Current2,
                    "sourceType": d._source.SourceType
                });
                current3Data.push({
                    "time": parseTimeISO(moment(d._source.Time).format("YYYY-MM-DD HH:mm:ss")),
                    "current": d._source.Current3,
                    "sourceType": d._source.SourceType,

            //pf
                });
                pf1Data.push({
                    "time": parseTimeISO(moment(d._source.Time).format("YYYY-MM-DD HH:mm:ss")),
                    "pf": d._source.PowerFactor1,
                    "sourceType": d._source.SourceType
                });
                pf2Data.push({
                    "time": parseTimeISO(moment(d._source.Time).format("YYYY-MM-DD HH:mm:ss")),
                    "pf": d._source.PowerFactor2,
                    "sourceType": d._source.SourceType
                });
                pf3Data.push({
                    "time": parseTimeISO(moment(d._source.Time).format("YYYY-MM-DD HH:mm:ss")),
                    "pf": d._source.PowerFactor3,
                    "sourceType": d._source.SourceType,
                });
            });
            voltages = voltage1Data.concat(voltage2Data, voltage3Data);
            currentData = current1Data.concat(current2Data, current3Data);
            pfData = pf1Data.concat(pf2Data,pf3Data);

            var phaseColors = [
                ["phase1", "red"],
                ["phase2", "yellow"],
                ["phase3", "blue"]
            ];


            //Voltage drawing
            xv.domain(d3.extent(voltages, function(d) {
                return d.time;
            }));

            yv.domain([
                d3.min(voltages, function(d) {
                    return d.voltage;
                }),
                d3.max(voltages, function(d) {
                    return d.voltage;
                })
            ]);


            ypf.domain([
                d3.min(pfData, function(d) {
                    return d.pf;
                }),
                d3.max(pfData, function(d) {
                    return d.pf;
                })
            ]);

            xcurrent.domain(d3.extent(currentData, function(d) {
                            return d.time;
                        }));

            ycurrent.domain([
                d3.min(currentData, function(d) {
                    return d.current;
                }),
                d3.max(currentData, function(d) {
                    return d.current;
                })
            ]);

            voltageElements = createAxis (0,contextVoltage,xAxisv,yAxis1v,'Voltage (V)',width2,make_x_axis,0);
            currentElements = createAxis (0,contextCurrent,xAxiscurrent,yAxis1Current,'Current (A)',width2,make_x_axis,0);
            pfElements = createAxis (0,contextpf,xAxisv,yAxis1pf,' ',width2,make_x_axis,0);


            linedVoltage[0] = contextVoltage.append('path')
                            .datum(voltage1Data)
                            .attr("d", lineVoltage)
                            .attr('stroke', 'red')
                            .attr('stroke-width', 2)
                            .attr('fill', 'none');

                /*.attr('d', lineVoltage(voltage1Data))
                .attr('stroke', 'red')
                .attr('stroke-width', 2)
                .attr('fill', 'none');*/

            linedVoltage[1]=contextVoltage.append('path')
                           .datum(voltage2Data)
                           .attr("d", lineVoltage)
                           .attr('stroke', 'yellow')
                           .attr('stroke-width', 2)
                           .attr('fill', 'none');


            linedVoltage[2]= contextVoltage.append('path')
                            .datum(voltage3Data)
                            .attr("d", lineVoltage)
                            .attr('stroke', 'blue')
                            .attr('stroke-width', 2)
                            .attr('fill', 'none');

            //Current Drawing

            linedCurrent[0] = contextCurrent .append('path')
                              .datum(current1Data)
                              .attr("d", lineCurrent)
                              .attr('stroke', 'red')
                              .attr('stroke-width', 2)
                              .attr('fill', 'none');

            linedCurrent[1] = contextCurrent.append('path')
                             .datum(current2Data)
                             .attr("d", lineCurrent)
                             .attr('stroke', 'yellow')
                             .attr('stroke-width', 2)
                             .attr('fill', 'none');

            linedCurrent[2] = contextCurrent.append('path')
                             .datum(current3Data)
                             .attr("d", lineCurrent)
                             .attr('stroke', 'blue')
                             .attr('stroke-width', 2)
                             .attr('fill', 'none');

            //pf Drawing
            linedpf[0] = contextpf.append('path')
                            .datum(pf1Data)
                            .attr("d", linepf)
                            .attr('stroke', 'red')
                            .attr('stroke-width', 2)
                            .attr('fill', 'none');

            linedpf[1] = contextpf.append('path')
                           .datum(pf2Data)
                           .attr("d", linepf)
                           .attr('stroke', 'yellow')
                           .attr('stroke-width', 2)
                           .attr('fill', 'none');

            linedpf[2] = contextpf.append('path')
                           .datum(pf3Data)
                           .attr("d", linepf)
                           .attr('stroke', 'blue')
                           .attr('stroke-width', 2)
                           .attr('fill', 'none');

        phaseLegend(svgCurrent);
        phaseLegend(svgVoltage);
        phaseLegend(svgpf);


        function phaseLegend (svgPhase) {

            var phaselegend = svgPhase.append("g")
                .attr("class", "legend")
                .attr("height", 100)
                .attr("width", 100)
                .attr('transform', 'translate(-100,50)');

            var legendRectPhase = phaselegend.selectAll('rect').data(phaseColors);

            legendRectPhase.enter()
                .append("rect")
                .attr("y", height - 15)
                .attr("width", 15)
                .attr("height", 15);

            legendRectPhase
                .attr("x", function(d, i) {
                    return i * 100 + 400;
                })
                .style("fill", function(d) {
                    return d[1];
                });

            var legendTextPhase = phaselegend.selectAll('text')
                .data(phaseColors);

            legendTextPhase.enter()
                .append("text")
                .attr("y", height + 15);

            legendTextPhase
                .attr("x", function(d, i) {
                    return i * 100 + 400;
                })
                .style("font-size", "14px")
                .style("fill", "white")
                .text(function(d) {
                    return d[0];
                });

        }

        });
    }


    //set Summary and set settings window toggle
    $("#displayWindowLink").click(function() {
        $("#displayWindow").slideToggle();
    });

    //bootstrap datetime picker
    var dpStart = "";
    var dpStop = "";
    var dp1 = $('#datetimepicker1').datetimepicker();
    dp1.data("DateTimePicker").maxDate(new Date()).format("DD-MM-YYYY h:mm:ss a");
    var dp2 = $('#datetimepicker2').datetimepicker({
        useCurrent: false //Important! See issue #1075
    });
    dp2.data("DateTimePicker").maxDate(new Date()).format("DD-MM-YYYY h:mm:ss a");
    dp1.on("dp.change", function(e) {
        dpStart = e.date + "";
        dp2.data("DateTimePicker").minDate(e.date);

    });
    dp2.on("dp.change", function(e) {
        dp1.data("DateTimePicker").maxDate(e.date);
        dpStop = e.date + "";
    });

    $("#dPGo").on("click", function() {
        remove_livegraphElements();
        query(config, probe, "../bounds/" + config + "/" + probe, new Date().getTime() - parseInt(dpStart), dpStop);
    });

    //timeWindow Relative tab
    $("#historyRBtn").click(function() {
        var milSecTime = "";
        var inpt = $('#historyRTxt').val();
        if (inpt < 1) {
            return;
        }
        switch (parseInt($("#historyRS option:selected").val())) {
            case 1:
                milSecTime = inpt * 60 * 1000;
                break;
            case 2:
                milSecTime = parseInt(inpt) * 3600000;
                break;
            case 3:
                milSecTime = inpt * 60 * 60 * 24 * 1000;
                break;
            case 4:
                milSecTime = inpt * 60 * 60 * 24 * 7 * 1000;
                break;
            case 5:
                milSecTime = inpt * 60 * 60 * 24 * 7 * 30 * 1000;
                break;
            case 6:
                milSecTime = inpt * 60 * 60 * 24 * 7 * 30 * 12 * 1000;
                break;
            default:
                milSecTime = inpt * 60 * 60 * 24 * 1000;
        }
        remove_livegraphElements();
        query(config, probe, "../bounds/" + config + "/" + probe, milSecTime);
    });

    //listens for custom event (HomePageOps.scala)
    $("#quick").on("winChange", function(e, tm, tm2) {
        remove_livegraphElements();
        if (tm2 !== 'undefined') {
            query(config, probe, "../bounds/" + config + "/" + probe, tm, tm2);
        } else {
            query(config, probe, "../bounds/" + config + "/" + probe, tm);
        }
    });
    var realTimeWindow = $("#realTimeWindow").val(); //get default (in sec)
    $("#rTimeRange").ionRangeSlider({
        force_edges: false,
        values: [
            "30sec", "45sec", "1min", "2min",
            "3min", "5min", "7min",
            "10min", "15min", "20min", "25min", "30min"
        ],
        from: 5,
        onFinish: function(data) {
            chRealTimeWindowSize(data.from);
        }
    });

    function setProbeOnSummary() {
        if (($("#probe option:selected").html()) != 'all probes') {
            $("#probeval").html('of ' + $("#probe option:selected").html() + ' ');
        } else if (($("#probe option:selected").html()) == 'all probes') {
            $("#probeval").html('');
        }
    }

    function setConfOnSummary() {
        $("#configval").html($("#config option:selected").html());
    }

    function chRealTimeWindowSize(tm) {
        switch (tm) {
            case 0:
                realTimeWindow = "30";
                break;
            case 1:
                realTimeWindow = "45";
                break;
            case 2:
                realTimeWindow = "60";
                break;
            case 3:
                realTimeWindow = "120";
                break;
            case 4:
                realTimeWindow = "180";
                break;
            case 5:
                realTimeWindow = "300";
                break;
            case 6:
                realTimeWindow = "420";
                break;
            case 7:
                realTimeWindow = "600";
                break;
            case 8:
                realTimeWindow = "900";
                break;
            case 9:
                realTimeWindow = "1200";
                break;
            case 10:
                realTimeWindow = "1500";
                break;
            case 11:
                realTimeWindow = "1800";
                break;
            default:
                realTimeWindow = "300";
                break;
        }
        $("#realTimeWindow").val(realTimeWindow);
        remove_livegraphElements();
        load_recent("../recent/" + config + "/" + probe + "/" + moment(lastTransmit).format("YYYY-MM-DDTHH:mm:ss.SSSZ") + "/" + realTimeWindow);
    }

});