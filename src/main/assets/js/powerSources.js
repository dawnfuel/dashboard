//$.noConflict();
$(document).ready(function(){


//ensure battery bulk voltage is multiple of battery voltage
    $("#battvoltage").on("change", function(){
      $("#battbankvoltage").attr("step",$("#battvoltage").val()).val($("#battvoltage").val()*2);
      $(".battBankErr").hide();
    });

    $("#battbankvoltage").on("change", function(){
       if($("#battbankvoltage").val()%$("#battvoltage").val() !== 0 || $("#battbankvoltage").val()<$("#battvoltage").val()){
         $("#battbankvoltage").val("");
         $(".battBankErr").show();
       }else{
       $(".battBankErr").hide();
       }
    });

    $("#chnnls").on("removeChs",function(){
    console.log("removingCh");
      $(".removech").hide();

    });

    //get selected config
    var sel = document.getElementById("sourceconfig");
    var selectedConfig = sel.options[sel.selectedIndex].value;
    console.log(selectedConfig);

    $("#probeSources").on('setSources',function(e,dt){
        console.log(dt.data);

        for(var i in  dt.data){
         var link = "/system/source/editgen?action=Edit&id=" + dt.data[i].id;
         if(dt.data[i].type == "grid"){ link = "/system/source/editgrid?action=Edit&id=" + dt.data[i].id;}else{
           if(dt.data[i].type == "inverter"){ link = "/system/source/editinv?action=Edit&id="+ dt.data[i].id;}
         }
         var name = $('<tr>',{
                      class : "rows"
                    });

        $("#probeSources").append(name).append($('<td>',{
                                                      class: "probeName",
                                                      html: dt.data[i].name+" ( "+dt.data[i].type+" )"
                                                  })).append($('<td>',{
                                                      class: "actions"}).append($('<a>',{
                                                      href:link, html:"reassign"})).append($("<span>",{html:"&nbsp;&nbsp;&nbsp;"})));
          console.log("Name :" + dt.data[i].name);
        }
    });

    $("#").on("click",function(){
        console.log("reset clicked");
        var client = elasticsearch.Client({
          host: 'localhost:9200',
          log: 'trace'
        });

        client.search({
              index: 'grit',
              type: 'docs',
              body:{
                     "query": {

                        "match": {"ConfigID_FK": selectedConfig}
                     },

                        "sort" : [
                           { "Time" : {"order" : "desc"}}
                       ],


                       "size": 1

                     }
                }).then(function (resp) {
                    var id = ''+resp.hits.hits[0]._id.toString()+'';

                    client.update({
                        index: 'grit',
                        type: 'docs',
                        id: id,
                        body: {
                          // put the partial document under the `doc` key
                          doc: {
//                            batterycapacity: parseFloat($("#battcapacity").val())
                            batterycapacity: 100
                          }
                        }
                      }, function (/*response*/) {

                      });

                });
    });

      var dpFrom = $('#operatingWindowF').datetimepicker({
           //useCurrent: false,//Important! See issue #1075
           format: 'HH:mm'
      });
           dpFrom.data("DateTimePicker");

      var dpTo = $('#operatingWindowT').datetimepicker({
               //  useCurrent: false,//Important! See issue #1075
                 format: 'HH:mm'
            });
            dpTo.data("DateTimePicker");

});

