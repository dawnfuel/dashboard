
$(document).ready(function(){

$("#dod").change(function(){
  $("#dodElem").toggle();
});

$("#PThreshold").change(function(){
   $("#PTElem").toggle();
});

$("#PTGen").change(function(){
  $("#PTGenElem").toggle();
});

$("#PTInv").change(function(){
  $("#PTInvElem").toggle();
});

$("#PTGrid").change(function(){
  $("#PTGridElem").toggle();
});

$("#sources").on('setSources',function(e,dt){
    console.log("<><><><><><><><><><><><><><><><><><><><><><><><><><><><>");
    setSourceLoop(dt.data);
    console.log(dt);
});

$("#addPhoneBttn").on("click",function(){
    if(document.getElementById("phoneList").length == 5){
        alert("Sorry! You cannot have more than five phone numbers");
    }else{
        var newPhone = $("#addPhone").val();
        console.log("new phone: " + newPhone);

        var Phone = {
            val : newPhone
        };
        $.each(Phone, function(val, text) {
            var Val = text;
            $('#phoneList').append( new Option(Val,Val) );
        });
        $("#addPhone").val("");
        savePhoneList();
    }
});

$("#rmPhoneBttn").on("click",function(){
    var phone = $('#phoneList :selected').val();
    console.log(phone);
    $("#phoneList option[value='"+phone+"']").remove();
    savePhoneList();
});


$("#addEmailBttn").on("click",function(){

    if(document.getElementById("emailList").length == 5){
        alert("Sorry! You cannot have more than five emails");
    }else{
        var newEmail = $("#addEmail").val();
        console.log("new email: " + newEmail);

        var Email = {
            val : newEmail,
        };
        $.each(Email, function(val, text) {
            var Val = text;
            $('#emailList').append( new Option(Val,Val) );
        });
        $("#addEmail").val("");
        saveEmailList();
    }
});

$("#rmEmailBttn").on("click",function(){
    var email = $('#emailList :selected').val();
    console.log(email);
    $("#emailList option[value='"+email+"']").remove();
    saveEmailList();
});

function savePhoneList(){
    var x = document.getElementById("phoneList");
    var txtPhones = "";
    var i;
    for (i = 0; i < x.length; i++) {
        txtPhones = txtPhones + "\n" + x.options[i].value;
    }
//    console.log("phone list: ");
//    console.log(txtPhones);
    $("#phones").val(txtPhones);
}

function saveEmailList(){

    var y = document.getElementById("emailList");
    var txtEmails = "";
    var j;
    for (j = 0; j < y.length; j++) {
        txtEmails = txtEmails + "\n" + y.options[j].value;
    }
//    console.log("email list: ");
//    console.log(txtEmails);
    $("#emails").val(txtEmails);
}

//$("#saveNotifConfig").on("click",function(){
//    var x = document.getElementById("phoneList");
//    var y = document.getElementById("emailList");
//
//    var txtPhones = "";
//    var i;
//    for (i = 0; i < x.length; i++) {
//        txtPhones = txtPhones + "\n" + x.options[i].value;
//    }
////    console.log("phone list: ");
////    console.log(txtPhones);
//    $("#phones").val(txtPhones);
//
//    var txtEmails = "";
//    var j;
//    for (j = 0; j < y.length; i++) {
//        txtEmails = txtEmails + "\n" + y.options[j].value;
//    }
////    console.log("email list: ");
////    console.log(txtEmails);
//    $("#emails").val(txtEmails);
//});




$("#sources").on('setNConfig',function(e,data){
    console.log("{}{}{}{}{}{}{}{}{}{}{}{}{}{}{}{}{}{}{}{}{}{}{{{}{}{}{}{}{}{}{{}{}{}{}{}{}{}{{}");
   console.log(data);
    $('input[name=dodBelow][value=flag]').attr('checked',data.dodBelow.flag);
    $('input[name=dodBelow][value=email]').attr('checked',data.dodBelow.email);
    $('input[name=dodBelow][value=sms]').attr('checked',data.dodBelow.sms);
    $('select[name=dodBelow]').val(data.dodBelow.inVal);

    $('input[name=dodAbove][value=flag]').attr('checked',data.dodAbove.flag);
    $('input[name=dodAbove][value=email]').attr('checked',data.dodAbove.email);
    $('input[name=dodAbove][value=sms]').attr('checked',data.dodAbove.sms);
    $('select[name=dodAbove]').val(data.dodAbove.inVal);

    $('input[name=dodAboveAnd][value=flag]').attr('checked',data.dodAboveAnd.flag);
    $('input[name=dodAboveAnd][value=email]').attr('checked',data.dodAboveAnd.email);
    $('input[name=dodAboveAnd][value=sms]').attr('checked',data.dodAboveAnd.sms);
    $('select[name=dodAboveAnd]').val(data.dodAboveAnd.inVal);

    $('input[name=oWindow][value=flag]').attr('checked',data.oWindow.flag);
    $('input[name=oWindow][value=email]').attr('checked',data.oWindow.email);
    $('input[name=oWindow][value=sms]').attr('checked',data.oWindow.sms);

    $('input[name=thresholdInv][value=flag]').attr('checked',data.thresholdInv.flag);
    $('input[name=thresholdInv][value=email]').attr('checked',data.thresholdInv.email);
    $('input[name=thresholdInv][value=sms]').attr('checked',data.thresholdInv.sms);
    $('select[name=thresholdInv][value=time]').val(data.thresholdInv.inVal);
    $('select[name=thresholdInv][value=value]').val(data.thresholdInv.Value);

//    $('input[name=thresholdInvVal][value=flag]').attr('checked',data.thresholdInvVal.flag);
//    $('input[name=thresholdInvVal][value=email]').attr('checked',data.thresholdInvVal.email);
//    $('input[name=thresholdInvVal][value=sms]').attr('checked',data.thresholdInvVal.sms);
//    $('select[name=thresholdInvVal]').val(data.thresholdInv.Value);

    $('input[name=thresholdGrid][value=flag]').attr('checked',data.thresholdGrid.flag);
    $('input[name=thresholdGrid][value=email]').attr('checked',data.thresholdGrid.email);
    $('input[name=thresholdGrid][value=sms]').attr('checked',data.thresholdGrid.sms);
    $('select[name=thresholdGrid][value=time]').val(data.thresholdGrid.inVal);
    $('select[name=thresholdGrid][value=value]').val(data.thresholdGrid.Value);

//    $('input[name=thresholdGridVal][value=flag]').attr('checked',data.thresholdGridVal.flag);
//    $('input[name=thresholdGridVal][value=email]').attr('checked',data.thresholdGridVal.email);
//    $('input[name=thresholdGridVal][value=sms]').attr('checked',data.thresholdGridVal.sms);
//    $('select[name=thresholdGridVal]').val(data.thresholdGrid.Value);

    $('input[name=thresholdGen][value=flag]').attr('checked',data.thresholdGen.flag);
    $('input[name=thresholdGen][value=email]').attr('checked',data.thresholdGen.email);
    $('input[name=thresholdGen][value=sms]').attr('checked',data.thresholdGen.sms);
    $('select[name=thresholdGen][value=time]').val(data.thresholdGen.inVal);
    $('select[name=thresholdGen][value=value]').val(data.thresholdGen.Value);

//    $('input[name=thresholdGenVal][value=flag]').attr('checked',data.thresholdGenVal.flag);
//    $('input[name=thresholdGenVal][value=email]').attr('checked',data.thresholdGenVal.email);
//    $('input[name=thresholdGenVal][value=sms]').attr('checked',data.thresholdGenVal.sms);
//    $('select[name=thresholdGenVal]').val(data.thresholdGen.Value);


    $('input[name=thresholdGenSt][value=high]').attr('checked',data.thresholdGenSt.high);
    $('input[name=thresholdGenSt][value=low]').attr('checked',data.thresholdGenSt.low);

    //fuelBelowX
        console.log("jojoihohi9hg9ighi9");
    console.log(data.fuelBelowX.flag);
    console.log("jojoihohi9hg9ighi9");
        $('input[name=fuelBelowX][value=flag]').attr('checked',data.fuelBelowX.flag);
        $('input[name=fuelBelowX][value=email]').attr('checked',data.fuelBelowX.email);
        $('input[name=fuelBelowX][value=sms]').attr('checked',data.fuelBelowX.sms);
        $('select[name=fuelBelowX]').val(data.fuelBelowX.inVal);
    //batteryBelowX
        $('input[name=batteryBelowX][value=flag]').attr('checked',data.batteryBelowX.flag);
        $('input[name=batteryBelowX][value=email]').attr('checked',data.batteryBelowX.email);
        $('input[name=batteryBelowX][value=sms]').attr('checked',data.batteryBelowX.sms);
        $('select[name=batteryBelowX]').val(data.batteryBelowX.inVal);

    //InverterTimeBelowX
        $('input[name=InverterTime][value=flag]').attr('checked',data.InverterTime.flag);
        $('input[name=InverterTime][value=email]').attr('checked',data.InverterTime.email);
        $('input[name=InverterTime][value=sms]').attr('checked',data.InverterTime.sms);
        $('select[name=InverterTime]').val(data.InverterTime.inVal);

    //utilityBelowX
        $('input[name=utilityBelowX][value=flag]').attr('checked',data.utilityBelowX.flag);
        $('input[name=utilityBelowX][value=email]').attr('checked',data.utilityBelowX.email);
        $('input[name=utilityBelowX][value=sms]').attr('checked',data.utilityBelowX.sms);
        $('select[name=utilityBelowX]').val(data.utilityBelowX.inVal);
    //voltXBelowT
        $('input[name=voltXBelowT][value=flag]').attr('checked',data.voltXBelowT.flag);
        $('input[name=voltXBelowT][value=email]').attr('checked',data.voltXBelowT.email);
        $('input[name=voltXBelowT][value=sms]').attr('checked',data.voltXBelowT.sms);
        $('select[name=voltXBelowT]').val(data.voltXBelowT.inVal);
    //voltXAboveT
        $('input[name=voltXAboveT][value=flag]').attr('checked',data.voltXAboveT.flag);
        $('input[name=voltXAboveT][value=email]').attr('checked',data.voltXAboveT.email);
        $('input[name=voltXAboveT][value=sms]').attr('checked',data.voltXAboveT.sms);
        $('select[name=voltXAboveT]').val(data.voltXAboveT.inVal);
    //PfBelowT
        $('input[name=PfBelowT][value=flag]').attr('checked',data.PfBelowT.flag);
        $('input[name=PfBelowT][value=email]').attr('checked',data.PfBelowT.email);
        $('input[name=PfBelowT][value=sms]').attr('checked',data.PfBelowT.sms);
    //voltBelowT
        $('input[name=voltBelowT][value=flag]').attr('checked',data.voltBelowT.flag);
        $('input[name=voltBelowT][value=email]').attr('checked',data.voltBelowT.email);
        $('input[name=voltBelowT][value=sms]').attr('checked',data.voltBelowT.sms);
    //voltAboveT
        $('input[name=voltAboveT][value=flag]').attr('checked',data.voltAboveT.flag);
        $('input[name=voltAboveT][value=email]').attr('checked',data.voltAboveT.email);
        $('input[name=voltAboveT][value=sms]').attr('checked',data.voltAboveT.sms);
    //zeroPhase
        $('input[name=zeroPhase][value=flag]').attr('checked',data.zeroPhase.flag);
        $('input[name=zeroPhase][value=email]').attr('checked',data.zeroPhase.email);
        $('input[name=zeroPhase][value=sms]').attr('checked',data.zeroPhase.sms);
    //signalOverlap
        $('input[name=signalOverlap][value=flag]').attr('checked',data.signalOverlap.flag);
        $('input[name=signalOverlap][value=email]').attr('checked',data.signalOverlap.email);
        $('input[name=signalOverlap][value=sms]').attr('checked',data.signalOverlap.sms);
    //invOutGrtIn
        $('input[name=invOutGrtIn][value=flag]').attr('checked',data.invOutGrtIn.flag);
        $('input[name=invOutGrtIn][value=email]').attr('checked',data.invOutGrtIn.email);
        $('input[name=invOutGrtIn][value=sms]').attr('checked',data.invOutGrtIn.sms);

     //phoneLise
     data.phoneList.forEach(function(e){
        console.log(e);
        var Phone = {
            val : e.value
        };
        $.each(Phone, function(val, text) {
            var Val = text;
            $('#phoneList').append( new Option(Val,Val) );
        });
        savePhoneList();
     });


     data.emailList.forEach(function(e){
         var Email = {
             val : e.value
         };
         $.each(Email, function(val, text) {
             var Val = text;
             $('#emailList').append( new Option(Val,Val) );
         });
         saveEmailList();
      });

    //loop through sources and set checkboxes
    for (var i in data.sTransition){
      console.log(i+" : "+data.sTransition[i].sourceName+" : "+data.sTransition[i].flag);
//      $('select[name='+data.sTransition[i].sourceName+']option[text='+data.sTransition[i].inVal+']').attr("selected","selected");
      $('input[name='+data.sTransition[i].sourceName+'][value=flag]').attr('checked',data.sTransition[i].flag);
      $('input[name='+data.sTransition[i].sourceName+'][value=email]').attr('checked',data.sTransition[i].email);
      $('input[name='+data.sTransition[i].sourceName+'][value=sms]').attr('checked',data.sTransition[i].sms);
    }
   console.log(data);
});

function setSourceLoop(data){
    for(var i in data){
     var name = $('<div>',{
                  class : "col-sm-6",
                  html : data[i].name
                });

    $("#sources").append(name).append($('<span>',{
                                                  class : "col-sm-2",
                                                  html : "Flag &nbsp;"
                                              }).append($('<input >',{type:"checkbox", name:data[i].name, value:"flag", checked:data[i].flag}))).append(
                                              $('<span>',{
                                                  class : "col-sm-2",
                                                  html : "Email &nbsp;"
                                              }).append($('<input>',{type:"checkbox", name:data[i].name, value:"email", checked:data[i].email}))).append(
                                              $('<span>',{
                                                  class : "col-sm-2",
                                                  html : "Sms &nbsp;"
                                                }).append($('<input>',{type:"checkbox", name:data[i].name, value:"sms", checked:data[i].sms})));

    console.log("Name :" + data[i].name);
    }
  }

}); //end of document